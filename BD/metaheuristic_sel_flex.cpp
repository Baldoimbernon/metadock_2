#include <thrust/sequence.h>
#include <omp.h>
#include "energy_common.h"
#include "energy_cuda.h"
#include "vector_types.h"

extern void seleccionar_flex (unsigned int nlig, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param)
{
	unsigned int stride_s, stride_e, th1;
	unsigned int total_selected = metaheuristic.NEMSel_selec + metaheuristic.NEPSel_selec;
	//printf("metaheuristic.NEFIni = %d\n",metaheuristic.NEFIni);
	stride_s = metaheuristic.NEMSel_selec;
	stride_e = metaheuristic.NEFIni - metaheuristic.NEPSel_selec;
	//printf("combined_init %d, combined_end %d, elements_copia_end %d\n",combined_init,combined_end,elements_copia_end);

	vectors_s->num_surface = vectors_e->num_surface;
	vectors_s->nconformations = vectors_e->num_surface * total_selected;	
	//printf("Número de conformaciones: %d\n",vectors_mejora.nconformations);
	vectors_s->conformations_x = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);
	vectors_s->conformations_y = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);
	vectors_s->conformations_z = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);
	
	vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nconformations);
	vectors_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
 	vectors_s->weights = (type_data *)malloc(SCORING_TERMS*sizeof(type_data));	
	//exit(0);	
	memcpy(vectors_s->f_params, vectors_e->f_params,MAXTYPES * sizeof(struct force_field_param_t));
	memcpy(vectors_s->weights, vectors_e->weights,SCORING_TERMS * sizeof(type_data));
	//exit(0);
	//memcpy(vectors_s->vdw_params, vectors_e->vdw_params,MAXTYPES * sizeof(struct vdw_param_t));
	//memcpy(vectors_s->sasa_params, vectors_e->sasa_params,MAXTYPES * sizeof(struct sasa_param_t));
	//memcpy(vectors_s->hbond_params, vectors_e->hbond_params,MAXTYPES * sizeof(struct hbond_param_t));
	
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 0:
		case 3:
		case 2:
			th1 = metaheuristic.Threads1Inc;
			break;
	}
	//exit(0);			
	omp_set_num_threads (th1);
	#pragma omp parallel for 	
	for (unsigned int i = 0;i < vectors_s->num_surface;i++)
	{	
		memcpy(vectors_s->conformations_x + i*nlig*total_selected, vectors_e->conformations_x + i*nlig*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * nlig * sizeof(type_data));  	
		memcpy(vectors_s->conformations_y + i*nlig*total_selected, vectors_e->conformations_y + i*nlig*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * nlig * sizeof(type_data)); 
		memcpy(vectors_s->conformations_z + i*nlig*total_selected, vectors_e->conformations_z + i*nlig*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * nlig * sizeof(type_data));
				
		memcpy(vectors_s->conformations_x + i*nlig*total_selected + stride_s*nlig, vectors_e->conformations_x + i*nlig*metaheuristic.NEFIni + stride_e*nlig, metaheuristic.NEPSel_selec * nlig * sizeof(type_data));  	
		memcpy(vectors_s->conformations_y + i*nlig*total_selected + stride_s*nlig, vectors_e->conformations_y + i*nlig*metaheuristic.NEFIni + stride_e*nlig, metaheuristic.NEPSel_selec * nlig * sizeof(type_data)); 
		memcpy(vectors_s->conformations_z + i*nlig*total_selected + stride_s*nlig, vectors_e->conformations_z + i*nlig*metaheuristic.NEFIni + stride_e*nlig, metaheuristic.NEPSel_selec * nlig * sizeof(type_data));
				
		memcpy(vectors_s->move_x + i*total_selected, vectors_e->move_x + i*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * sizeof(type_data));  	
		memcpy(vectors_s->move_y + i*total_selected, vectors_e->move_y + i*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * sizeof(type_data)); 
		memcpy(vectors_s->move_z + i*total_selected, vectors_e->move_z + i*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * sizeof(type_data));
		memcpy(vectors_s->quat_x + i*total_selected, vectors_e->quat_x + i*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * sizeof(type_data));
		memcpy(vectors_s->quat_y + i*total_selected, vectors_e->quat_y + i*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * sizeof(type_data));
		memcpy(vectors_s->quat_z + i*total_selected, vectors_e->quat_z + i*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * sizeof(type_data));
		memcpy(vectors_s->quat_w + i*total_selected, vectors_e->quat_w + i*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * sizeof(type_data));
		memcpy(vectors_s->energy.energy + i*total_selected, vectors_e->energy.energy + i*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * sizeof(type_data));										
						
		memcpy(vectors_s->move_x + i*total_selected + stride_s, vectors_e->move_x + i*metaheuristic.NEFIni + stride_e, metaheuristic.NEPSel_selec * sizeof(type_data));  	
		memcpy(vectors_s->move_y + i*total_selected + stride_s, vectors_e->move_y + i*metaheuristic.NEFIni + stride_e, metaheuristic.NEPSel_selec * sizeof(type_data)); 
		memcpy(vectors_s->move_z + i*total_selected + stride_s, vectors_e->move_z + i*metaheuristic.NEFIni + stride_e, metaheuristic.NEPSel_selec * sizeof(type_data));
		memcpy(vectors_s->quat_x + i*total_selected + stride_s, vectors_e->quat_x + i*metaheuristic.NEFIni + stride_e, metaheuristic.NEPSel_selec * sizeof(type_data));
		memcpy(vectors_s->quat_y + i*total_selected + stride_s, vectors_e->quat_y + i*metaheuristic.NEFIni + stride_e, metaheuristic.NEPSel_selec * sizeof(type_data));
		memcpy(vectors_s->quat_z + i*total_selected + stride_s, vectors_e->quat_z + i*metaheuristic.NEFIni + stride_e, metaheuristic.NEPSel_selec * sizeof(type_data));
		memcpy(vectors_s->quat_w + i*total_selected + stride_s, vectors_e->quat_w + i*metaheuristic.NEFIni + stride_e, metaheuristic.NEPSel_selec * sizeof(type_data));
		memcpy(vectors_s->energy.energy + i*total_selected + stride_s, vectors_e->energy.energy + i*metaheuristic.NEFIni + stride_e, metaheuristic.NEPSel_selec * sizeof(type_data));	
	}
	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
	
	//for(int k=0;k<64;k++)
	//	printf("e-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",k,vectors_e->conformations_x[k],vectors_e->conformations_y[k],vectors_e->conformations_z[k],vectors_e->move_x[k/nlig],vectors_e->move_y[k/nlig],vectors_e->move_z[k/nlig],vectors_e->energy.energy[k/nlig]);
	//printf("***\n");
//for(int k=0;k<vectors_e->nconformations;k++)
  //              printf("e-number %d, m_x %f, m_y %f, m_z %f, energy %f\n",k,vectors_e->move_x[k],vectors_e->move_y[k],vectors_e->move_z[k],vectors_e->energy.energy[k]);
//for(int k=0;k<vectors_s->nconformations;k++)
  //              printf("s-number %d, m_x %f, m_y %f, m_z %f, energy %f\n",k,vectors_s->move_x[k],vectors_s->move_y[k],vectors_s->move_z[k],vectors_s->energy.energy[k]);
//	for(int k=0;k<64*vectors_s->nconformations;k++)
//		printf("s-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",k,vectors_s->conformations_x[k],vectors_s->conformations_y[k],vectors_s->conformations_z[k],vectors_s->move_x[k/nlig],vectors_s->move_y[k/nlig],vectors_s->move_z[k/nlig],vectors_s->energy.energy[k/nlig]);
	
	//exit(0);
}


