#include "energy_common.h"
#include "energy_cuda.h"
#include "energy_flexibility.h"
#include "energy_CpuSolver.h"
#include "energy_CpuSolver_common.h"
#include "energy_CpuSolver_flex.h"
#include "energy_GpuSolver.h"
#include "flexibility.h"
#include "rotation.h"
#include "metaheuristic_mgpu_flex.h"
#include "metaheuristic_inc_flex.h"
#include "metaheuristic_comb.h"
#include "wtime.h"
#include <thrust/sequence.h>
#include <thrust/sort.h>
#include <thrust/functional.h>
#include <omp.h>

void combinar_individual_flex (unsigned int nlig, type_data *e_conformations_x, type_data *e_conformations_y, type_data *e_conformations_z, type_data *e_move_x, type_data *e_move_y, type_data *e_move_z, type_data *e_quat_x, type_data *e_quat_y, type_data *e_quat_z, type_data *e_quat_w, type_data *s_conformations_x, type_data *s_conformations_y, type_data *s_conformations_z, type_data *s_move_x, type_data *s_move_y, type_data *s_move_z, type_data *s_quat_x, type_data *s_quat_y, type_data *s_quat_z, type_data *s_quat_w , struct param_t param, unsigned int j, unsigned int k, unsigned int l)
{
	type_data local_x, local_y, local_z, local_w, quat_tmp_x, quat_tmp_y, quat_tmp_z, quat_tmp_w;
	type_data angle,eje[3];
	
	unsigned int pos_o = j;	
	unsigned int pos_d = k;
	unsigned int pos_s = l;

	memcpy (s_conformations_x + (pos_s*nlig), e_conformations_x + (pos_o*nlig), nlig*sizeof(type_data));
	memcpy (s_conformations_y + (pos_s*nlig), e_conformations_y + (pos_o*nlig), nlig*sizeof(type_data));
	memcpy (s_conformations_z + (pos_s*nlig), e_conformations_z + (pos_o*nlig), nlig*sizeof(type_data));
	/*for (int m = 0;m < nlig; m++)
	{
		vectors_s->conformations_x[pos_s*nlig + m] = vectors_e->conformations_x[pos_o*nlig + m];
		vectors_s->conformations_y[pos_s*nlig + m] = vectors_e->conformations_y[pos_o*nlig + m];
		vectors_s->conformations_z[pos_s*nlig + m] = vectors_e->conformations_z[pos_o*nlig + m];
	}*/
		
	s_move_x[pos_s] = (e_move_x[pos_o] + e_move_x[pos_d])/2;
	s_move_y[pos_s] = (e_move_y[pos_o] + e_move_y[pos_d])/2;
	s_move_z[pos_s] = (e_move_z[pos_o] + e_move_z[pos_d])/2;
	
	quat_tmp_x = e_quat_x[pos_o];
	quat_tmp_y = e_quat_y[pos_o];
	quat_tmp_z = e_quat_z[pos_o];
	quat_tmp_w = e_quat_w[pos_o];
	angle = getRealRandomNumber(param.rotation);
	eje[0] = getRealRandomNumber (1);
	eje[1] = getRealRandomNumber (1);
	eje[2] = getRealRandomNumber (1);
	setRotation_cpp (&local_x, &local_y, &local_z, &local_w, angle, eje);				
	composeRotation_cpp (&local_x, &local_y, &local_z, &local_w, &quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w, &quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w);
	normalize_cpp(&quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w);
	s_quat_x[pos_s] = quat_tmp_x;
	s_quat_y[pos_s] = quat_tmp_y;
	s_quat_z[pos_s] = quat_tmp_z;
	s_quat_w[pos_s] = quat_tmp_w;
}


extern void combinar_group_flex (unsigned int nlig, type_data *e_conformations_x, type_data *e_conformations_y, type_data *e_conformations_z, type_data *e_move_x, type_data *e_move_y, type_data *e_move_z, type_data *e_quat_x, type_data *e_quat_y, type_data *e_quat_z, type_data *e_quat_w, type_data *s_conformations_x, type_data *s_conformations_y, type_data *s_conformations_z, type_data *s_move_x, type_data *s_move_y, type_data *s_move_z, type_data *s_quat_x, type_data *s_quat_y, type_data *s_quat_z, type_data *s_quat_w , struct metaheuristic_t metaheuristic, unsigned int mejores, unsigned int peores, unsigned int mejorespeores, unsigned int stride_e, unsigned int stride_s, struct param_t param)
{	

	//int l = 0;
	unsigned int base = 0;
	unsigned int ind_m,ind_p,k,j;
	unsigned int div_m = metaheuristic.NMMCom_selec - 1;
	unsigned int div_p = metaheuristic.NPPCom_selec - 1;
	unsigned int div_mp = metaheuristic.NMPCom_selec - 1;
	unsigned int h2;	
	switch (param.mode) {
		
		case 1:
			h2 = 1;
			break;
		case 0:
		case 2:
		case 3:	
			h2 = 1;//metaheuristic.Threads2Com;
			break;
	}
	//omp_set_num_threads(h2);
	//#pragma omp parallel for private(j,k)
	if ((metaheuristic.NEMSel_selec > 0) && (mejores > 2))
	{
	//omp_set_num_threads(h2);
	//#pragma omp parallel for private(j,k)
		for (unsigned int m = 0; m < mejores; m++)
		{
			j = m / div_m;
			k = (div_m * (j+1)) - m;
			if (k <= j) k--;
			//printf("l = %d, j = %d, k = %d\n",m,j,k);
			combinar_individual_flex (nlig,e_conformations_x,e_conformations_y,e_conformations_z,e_move_x,e_move_y,e_move_z,e_quat_x,e_quat_y,e_quat_z,e_quat_w,s_conformations_x,s_conformations_y,s_conformations_z,s_move_x,s_move_y,s_move_z,s_quat_x,s_quat_y,s_quat_z,s_quat_w,param,j,k,m);
		}
	}
				
	//peores con peores
	if ((metaheuristic.NEPSel_selec > 0) && (peores > 2))
	{
		base =  metaheuristic.NMMCom_selec * ( metaheuristic.NMMCom_selec - 1);
		//#pragma omp parallel for private (j,k)
		for (unsigned int j = 0; j < metaheuristic.NPPCom_selec; j++)
			 for (unsigned int k = 0; k < metaheuristic.NPPCom_selec; k++)
				if (k != j)
				{
					combinar_individual_flex (nlig,e_conformations_x,e_conformations_y,e_conformations_z,e_move_x,e_move_y,e_move_z,e_quat_x,e_quat_y,e_quat_z,e_quat_w,s_conformations_x,s_conformations_y,s_conformations_z,s_move_x,s_move_y,s_move_z,s_quat_x,s_quat_y,s_quat_z,s_quat_w,param,j + metaheuristic.NEMSel_selec,k + metaheuristic.NEMSel_selec,base);
					base++;
				}	
	}
			
	//mejores con peores
	
	if ((metaheuristic.NEMSel_selec > 0) && (metaheuristic.NEPSel_selec > 0) && (mejorespeores > 2))
	{
		base = (metaheuristic.NMMCom_selec * ( metaheuristic.NMMCom_selec - 1)) + (metaheuristic.NPPCom_selec * ( metaheuristic.NPPCom_selec - 1));
		ind_m = 0;//getRandomNumber_modified(metaheuristic.NEMSel_selec-1,0);
		ind_p = 0;//getRandomNumber_modified(metaheuristic.NEPSel_selec-1,0);

		//#pragma omp parallel for private(ind_m,ind_p)
		for (unsigned int m = base; m < (mejorespeores + base); m++)
		{					
			//combinar_individual_vs_flex (nlig,vectors_e,vectors_s,metaheuristic,param,stride_e,stride_s,i,ind_m,ind_p+metaheuristic.NEMSel_selec,m);
			//printf("ind_m %d, ind_p %d, m %d\n",ind_m,ind_p,m);
			combinar_individual_flex(nlig,e_conformations_x,e_conformations_y,e_conformations_z,e_move_x,e_move_y,e_move_z,e_quat_x,e_quat_y,e_quat_z,e_quat_w,s_conformations_x,s_conformations_y,s_conformations_z,s_move_x,s_move_y,s_move_z,s_quat_x,s_quat_y,s_quat_z,s_quat_w,param,ind_m,ind_p+metaheuristic.NEMSel_selec,m);			
			ind_m = (ind_m + 1) % metaheuristic.NMMCom_selec;
			ind_p = (ind_p + 1) % metaheuristic.NPPCom_selec;
			if ((ind_p % metaheuristic.NEPSel_selec) == 0) ind_p++;
			//printf("ind_m %d, ind_p %d m %d\n",ind_m,ind_p,m);
			//l++;						
		}
	}				

}



extern void combinar_flex (struct receptor_t proteina, struct ligand_t ligando, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param, struct flexibility_data_t * flexibility_conformations,struct flexibility_params_t *flexibility_params )
{
	unsigned int mejores = 0, peores = 0, mejorespeores = 0;
	unsigned int stride_s, stride_e;	
	type_data tcombinargroupcpu, tenergycombinarcpu;
	type_data tcombinargroupcpu_t, tenergycombinarcpu_t;
	type_data *internal_energy;
	unsigned int th1;
	//omp_set_nested(1);
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 0:
		case 3:
		case 2:
			th1 = metaheuristic.Threads1Ini;
			break;
	}				
	
	vectors_s->num_surface = vectors_e->num_surface;
	
	mejores = metaheuristic.NMMCom_selec * (metaheuristic.NMMCom_selec - 1);	
	mejorespeores = metaheuristic.NMPCom_selec * (metaheuristic.NMPCom_selec - 1);	
	peores = metaheuristic.NPPCom_selec * (metaheuristic.NPPCom_selec - 1);
	stride_s = mejores + mejorespeores + peores;
	//stride_e = metaheuristic.NEMSel_selec + metaheuristic.NEPSel_selec;			
	stride_e = vectors_e->nconformations / vectors_e->num_surface;
	vectors_s->nconformations = vectors_e->num_surface * stride_s;
	vectors_s->conformations_x = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_s->nconformations);
	vectors_s->conformations_y = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_s->nconformations);
	vectors_s->conformations_z = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_s->nconformations);
			
	vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nconformations);
	vectors_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->weights = (type_data *)malloc(SCORING_TERMS*sizeof(type_data));	

	//memcpy(vectors_s->vdw_params, vectors_e->vdw_params,MAXTYPES * sizeof(struct vdw_param_t));
	//memcpy(vectors_s->sasa_params, vectors_e->sasa_params,MAXTYPES * sizeof(struct sasa_param_t));
	//memcpy(vectors_s->hbond_params, vectors_e->hbond_params,MAXTYPES * sizeof(struct hbond_param_t));
	memcpy(vectors_s->f_params, vectors_e->f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_s->weights, vectors_e->weights,SCORING_TERMS * sizeof(type_data));

	internal_energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	flexibility_conformations = (flexibility_data_t *)malloc(sizeof(flexibility_data_t)*vectors_s->nconformations);

	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);			
			
	tcombinargroupcpu = wtime();
	//omp_set_nested(1);
	omp_set_num_threads (th1);
	#pragma omp parallel for 		 			
	for (int i = 0; i < vectors_s->num_surface;i++)	
	{				
		combinar_group_flex(ligando.nlig, (vectors_e->conformations_x + i*stride_e*ligando.nlig),  (vectors_e->conformations_y + i*stride_e*ligando.nlig),  (vectors_e->conformations_z + i*stride_e*ligando.nlig),  (vectors_e->move_x + i*stride_e), (vectors_e->move_y + i*stride_e), (vectors_e->move_z + i*stride_e), (vectors_e->quat_x + i*stride_e), (vectors_e->quat_y + i*stride_e), (vectors_e->quat_z + i*stride_e), (vectors_e->quat_w + i*stride_e), (vectors_s->conformations_x + i*stride_s*ligando.nlig),  (vectors_s->conformations_y + i*stride_s*ligando.nlig),  (vectors_s->conformations_z + i*stride_s*ligando.nlig),  (vectors_s->move_x + i*stride_s), (vectors_s->move_y + i*stride_s), (vectors_s->move_z + i*stride_s), (vectors_s->quat_x + i*stride_s), (vectors_s->quat_y + i*stride_s), (vectors_s->quat_z + i*stride_s), (vectors_s->quat_w + i*stride_s), metaheuristic, mejores, peores, mejorespeores, stride_e, stride_s, param);							
	}
	tcombinargroupcpu_t += wtime() - tcombinargroupcpu;			
	//for (int i=0;i<64;i++) printf("conf %d  x %f  y %f  z %f\n",i,vectors_e->conformations_x[i],vectors_e->conformations_y[i],vectors_e->conformations_z[i]);
					
	flexibility_function_cpp (ligando,param,metaheuristic,vectors_s->conformations_x,vectors_s->conformations_y,vectors_s->conformations_z,flexibility_conformations,flexibility_params,vectors_s->nconformations);	
	//exit(0);
	tenergycombinarcpu = wtime();	
	ForcesCpuSolver_flex(param,ligando,proteina,vectors_s->conformations_x,vectors_s->conformations_y,vectors_s->conformations_z,vectors_s->move_x,vectors_s->move_y,vectors_s->move_z,vectors_s->quat_x, vectors_s->quat_y, vectors_s->quat_z, vectors_s->quat_w,vectors_s->energy.energy,vectors_s->energy.n_conformation,vectors_s->weights,vectors_s->f_params,vectors_s->nconformations,metaheuristic,flexibility_params->n_links);	
	//exit(0);
	energy_internal_conformations_cpu (vectors_s->nconformations,param,metaheuristic,vectors_s->conformations_x,vectors_s->conformations_y,vectors_s->conformations_z,&ligando,vectors_s->weights,vectors_s->f_params,internal_energy,flexibility_params->individual_bonds_VDW,flexibility_params->individual_bonds_ES);
	//exit(0);
	energy_total (vectors_s->nconformations,param,metaheuristic,vectors_s->energy.energy,internal_energy);					
	//exit(0);
	tenergycombinarcpu_t += wtime() - tenergycombinarcpu;

	#pragma omp parallel for			
	for (unsigned int i = 0; i < vectors_s->nconformations; i += stride_s)							
		thrust::stable_sort_by_key((vectors_s->energy.energy + i),(vectors_s->energy.energy + i + stride_s), (vectors_s->energy.n_conformation + i), thrust::less<type_data>());						
	//colocar_flex(ligando.nlig,param,vectors_s,metaheuristic);		
	#pragma omp parallel for
	for (unsigned int i = 0; i < vectors_s->num_surface; i++)
		colocar_flex_by_step (param,metaheuristic,ligando.nlig,vectors_s->conformations_x + (stride_s*i*ligando.nlig),vectors_s->conformations_y + (stride_s*i*ligando.nlig),vectors_s->conformations_z + (stride_s*i*ligando.nlig),vectors_s->move_x + (stride_s*i),vectors_s->move_y + (stride_s*i),vectors_s->move_z + (stride_s*i),vectors_s->quat_x + (stride_s*i),vectors_s->quat_y + (stride_s*i),vectors_s->quat_z + (stride_s*i),vectors_s->quat_w + (stride_s*i),vectors_s->energy.energy + (stride_s*i),vectors_s->energy.n_conformation + (stride_s*i),stride_s,i);
	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
	//exit(0);
	free(flexibility_conformations);
	free(internal_energy);
}

extern void combinar_gpu_flex (struct receptor_t proteina, struct ligand_t ligando, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param, struct device_t *devices,struct flexibility_params_t *flexibility_params, unsigned int orden_device)
{
	unsigned int mejores, peores, mejorespeores, mejores_tmp, peores_tmp, mejorespeores_tmp,stride_e,stride_s;
	type_data kernels = 0;
	//exit(0);
	//cudaError_t cudaStatus;	
	//cudaSetDevice(devices->id[orden_device]);
	
	mejores_tmp = metaheuristic.NMMCom_selec;
	peores_tmp = metaheuristic.NPPCom_selec;
	mejorespeores_tmp = metaheuristic.NMPCom_selec;

	mejores = mejores_tmp * (mejores_tmp - 1);	
	mejorespeores = mejorespeores_tmp * (mejorespeores_tmp - 1);	
	peores = peores_tmp * (peores_tmp - 1);
	//printf("mejores %d, peores %d, mejorespeores %d\n",mejores,peores,mejorespeores);
			
	stride_s = mejores + mejorespeores + peores;
	//stride_e = metaheuristic.NEMSel_selec + metaheuristic.NEPSel_selec;
	stride_e = vectors_e->nconformations / vectors_e->num_surface;
	
	vectors_s->num_surface = vectors_e->num_surface;			
	vectors_s->nconformations = vectors_e->num_surface * stride_s;
	vectors_s->conformations_x = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_s->nconformations);
	vectors_s->conformations_y = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_s->nconformations);
	vectors_s->conformations_z = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_s->nconformations);
			
	vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nconformations);
	vectors_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);	
	vectors_s->weights = (type_data *)malloc(SCORING_TERMS*sizeof(type_data));

	//memcpy(vectors_s->vdw_params, vectors_e->vdw_params,MAXTYPES * sizeof(struct vdw_param_t));						    memcpy(vectors_s->sasa_params, vectors_e->sasa_params,MAXTYPES * sizeof(struct sasa_param_t));					
	//memcpy(vectors_s->hbond_params, vectors_e->hbond_params,MAXTYPES * sizeof(struct hbond_param_t));																			
	memcpy(vectors_s->f_params, vectors_e->f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_s->weights, vectors_e->weights,SCORING_TERMS * sizeof(type_data));
	
	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
		
	omp_set_num_threads(metaheuristic.Threads1Com);
	#pragma omp parallel for	
	for (unsigned int i = 0; i < vectors_s->num_surface;i++)	
	{				
		combinar_group_flex(ligando.nlig, (vectors_e->conformations_x + i*stride_e*ligando.nlig),  (vectors_e->conformations_y + i*stride_e*ligando.nlig),  (vectors_e->conformations_z + i*stride_e*ligando.nlig),  (vectors_e->move_x + i*stride_e), (vectors_e->move_y + i*stride_e), (vectors_e->move_z + i*stride_e), (vectors_e->quat_x + i*stride_e), (vectors_e->quat_y + i*stride_e), (vectors_e->quat_z + i*stride_e), (vectors_e->quat_w + i*stride_e), (vectors_s->conformations_x + i*stride_s*ligando.nlig),  (vectors_s->conformations_y + i*stride_s*ligando.nlig),  (vectors_s->conformations_z + i*stride_s*ligando.nlig),  (vectors_s->move_x + i*stride_s), (vectors_s->move_y + i*stride_s), (vectors_s->move_z + i*stride_s), (vectors_s->quat_x + i*stride_s), (vectors_s->quat_y + i*stride_s), (vectors_s->quat_z + i*stride_s), (vectors_s->quat_w + i*stride_s), metaheuristic, mejores, peores, mejorespeores, stride_e, stride_s, param);				
	}
	
	unsigned int max_conformaciones = ((devices->propiedades[orden_device].maxGridSize[0] * devices->hilos[orden_device*metaheuristic.num_kernels]) / WARP_SIZE) * 8;
	//exit(0);
	if (vectors_s->nconformations < max_conformaciones)
	{
		flexibility_function(ligando,param,metaheuristic,devices,vectors_s->conformations_x,vectors_s->conformations_y,vectors_s->conformations_z,flexibility_params,vectors_s->weights,vectors_s->f_params,vectors_s->nconformations,orden_device);
	}
	else
	{
		//DIVIDIR EN TROZOS
		while((vectors_s->nconformations % max_conformaciones) != 0) max_conformaciones --;		
		//printf("va a trocear\n");		
		for (unsigned int desplazamiento=0;desplazamiento<vectors_s->nconformations;desplazamiento+=max_conformaciones)
		{
			flexibility_function(ligando,param,metaheuristic,devices,vectors_s->conformations_x+desplazamiento,vectors_s->conformations_y+desplazamiento,vectors_s->conformations_z+desplazamiento,flexibility_params,vectors_s->weights,vectors_s->f_params,vectors_s->nconformations,orden_device);
			//printf("Device %d con trozo %d\n",devices->id[0],i);			
		}
	}
	//exit(0);
	max_conformaciones = ((devices->propiedades[orden_device].maxGridSize[0] * devices->hilos[orden_device*metaheuristic.num_kernels]) / WARP_SIZE) * 8;
	if (vectors_s->nconformations < max_conformaciones)
	{			
			gpuSolver_flex (ligando,proteina,param,metaheuristic,devices,vectors_s->conformations_x, vectors_s->conformations_y, vectors_s->conformations_z,vectors_s->move_x,vectors_s->move_y,vectors_s->move_z,vectors_s->quat_x, vectors_s->quat_y, vectors_s->quat_z, vectors_s->quat_w, vectors_s->energy.energy,vectors_s->energy.n_conformation,vectors_s->weights,vectors_s->f_params,flexibility_params->n_links,vectors_s->nconformations,0,0,flexibility_params->individual_bonds_VDW,flexibility_params->individual_bonds_ES,&kernels);
	}
	else
	{
		//DIVIDIR EN TROZOS
		while((vectors_s->nconformations % max_conformaciones) != 0) max_conformaciones --;		
		//printf("va a trocear\n");		
		for (unsigned int desplazamiento=0;desplazamiento<vectors_s->nconformations;desplazamiento+=max_conformaciones)
		{			
			gpuSolver_flex (ligando,proteina,param,metaheuristic,devices,vectors_s->conformations_x+(desplazamiento*ligando.nlig), vectors_s->conformations_y+(desplazamiento*ligando.nlig),vectors_s->conformations_z+(desplazamiento*ligando.nlig), vectors_s->move_x+desplazamiento,vectors_s->move_y+desplazamiento,vectors_s->move_z+desplazamiento,vectors_s->quat_x+desplazamiento, vectors_s->quat_y+desplazamiento, vectors_s->quat_z+desplazamiento, vectors_s->quat_w+desplazamiento, vectors_s->energy.energy+desplazamiento,vectors_s->energy.n_conformation+desplazamiento,vectors_s->weights,vectors_s->f_params,flexibility_params->n_links,max_conformaciones,0,desplazamiento,flexibility_params->individual_bonds_VDW,flexibility_params->individual_bonds_ES,&kernels);
			//printf("Device %d con trozo %d\n",devices->id[0],i);
			
		}
	}

	
	omp_set_num_threads (metaheuristic.Threads1Com);
	#pragma omp parallel for			
	for (unsigned int i = 0; i < vectors_s->nconformations; i += stride_s)				
		thrust::stable_sort_by_key((vectors_s->energy.energy + i),(vectors_s->energy.energy + i + stride_s), (vectors_s->energy.n_conformation + i), thrust::less<type_data>());			
	//colocar_flex(ligando.nlig,param,vectors_s,metaheuristic);	
	#pragma omp parallel for
        for (unsigned int i = 0; i < vectors_s->num_surface; i++)
                colocar_flex_by_step (param,metaheuristic,ligando.nlig,vectors_s->conformations_x + (stride_s*i*ligando.nlig),vectors_s->conformations_y + (stride_s*i*ligando.nlig),vectors_s->conformations_z + (stride_s*i*ligando.nlig),vectors_s->move_x + (stride_s*i),vectors_s->move_y + (stride_s*i),vectors_s->move_z + (stride_s*i),vectors_s->quat_x + (stride_s*i),vectors_s->quat_y + (stride_s*i),vectors_s->quat_z + (stride_s*i),vectors_s->quat_w + (stride_s*i),vectors_s->energy.energy + (stride_s*i),vectors_s->energy.n_conformation + (stride_s*i),stride_s,i);
        thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
	
}	


extern void combinar_multigpu_flex (struct receptor_t proteina, struct ligand_t ligando, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param, struct device_t *devices, struct flexibility_params_t *flexibility_params )
{
	struct receptor_t *proteina_d;
	struct ligand_t *ligando_d;
	unsigned int n_devices,k;
	unsigned int orden_device;
	unsigned int mejores, peores, mejorespeores, mejores_tmp, peores_tmp, mejorespeores_tmp,stride_e,stride_s;	
	//cudaError_t cudaStatus;	
	
	mejores_tmp = metaheuristic.NMMCom_selec;
	peores_tmp = metaheuristic.NPPCom_selec;
	mejorespeores_tmp = metaheuristic.NMPCom_selec;

	mejores = mejores_tmp * (mejores_tmp - 1);	
	mejorespeores = mejorespeores_tmp * (mejorespeores_tmp - 1);	
	peores = peores_tmp * (peores_tmp - 1);
	//printf("mejores %d, peores %d, mejorespeores %d\n",mejores,peores,mejorespeores);
			
	stride_s = mejores + mejorespeores + peores;
	//stride_e = metaheuristic.NEMSel_selec + metaheuristic.NEPSel_selec;
	stride_e = vectors_e->nconformations / vectors_e->num_surface;
	
	//for (int i=0;i<64;i++) printf("conf %d  x %f  y %f  z %f\n",i,vectors_e->conformations_x[i],vectors_e->conformations_y[i],vectors_e->conformations_z[i]);
	//printf("conformations entrada: %d %d %d %d\n",vectors_e->nconformations,vectors_e->num_surface,stride_e,stride_s); 	
	vectors_s->num_surface = vectors_e->num_surface;			
	vectors_s->nconformations = vectors_e->num_surface * stride_s;
	vectors_s->conformations_x = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_s->nconformations);
	vectors_s->conformations_y = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_s->nconformations);
	vectors_s->conformations_z = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_s->nconformations);
	
	vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	
	vectors_s->energy.n_conformation = (unsigned int *)calloc(sizeof(unsigned int),vectors_s->nconformations);
	vectors_s->energy.energy = (type_data *)calloc(sizeof(type_data),vectors_s->nconformations);	
	vectors_s->weights = (type_data *)malloc(SCORING_TERMS*sizeof(type_data));

	//memcpy(vectors_s->vdw_params, vectors_e->vdw_params,MAXTYPES * sizeof(struct vdw_param_t));					
	//memcpy(vectors_s->sasa_params, vectors_e->sasa_params,MAXTYPES * sizeof(struct sasa_param_t));
    //    memcpy(vectors_s->hbond_params, vectors_e->hbond_params,MAXTYPES * sizeof(struct hbond_param_t)); 
	memcpy(vectors_s->f_params, vectors_e->f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_s->weights, vectors_e->weights,SCORING_TERMS * sizeof(type_data));

	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
	//exit(0);	
	omp_set_num_threads(metaheuristic.Threads1Com);
	#pragma omp parallel for	
	for (int i = 0; i < vectors_s->num_surface;i++)	
	{				
		combinar_group_flex(ligando.nlig, (vectors_e->conformations_x + i*stride_e*ligando.nlig),  (vectors_e->conformations_y + i*stride_e*ligando.nlig),  (vectors_e->conformations_z + i*stride_e*ligando.nlig),  (vectors_e->move_x + i*stride_e), (vectors_e->move_y + i*stride_e), (vectors_e->move_z + i*stride_e), (vectors_e->quat_x + i*stride_e), (vectors_e->quat_y + i*stride_e), (vectors_e->quat_z + i*stride_e), (vectors_e->quat_w + i*stride_e), (vectors_s->conformations_x + i*stride_s*ligando.nlig),  (vectors_s->conformations_y + i*stride_s*ligando.nlig),  (vectors_s->conformations_z + i*stride_s*ligando.nlig),  (vectors_s->move_x + i*stride_s), (vectors_s->move_y + i*stride_s), (vectors_s->move_z + i*stride_s), (vectors_s->quat_x + i*stride_s), (vectors_s->quat_y + i*stride_s), (vectors_s->quat_z + i*stride_s), (vectors_s->quat_w + i*stride_s), metaheuristic, mejores, peores, mejorespeores, stride_e, stride_s, param);							
	}
	
	orden_device = devices->id[0];
	unsigned int max_conformaciones = ((devices->propiedades[orden_device].maxGridSize[0] * devices->hilos[orden_device*metaheuristic.num_kernels]) / WARP_SIZE) * 8;
	
	if (vectors_s->nconformations < max_conformaciones)
	{
		flexibility_function(ligando,param,metaheuristic,devices,vectors_s->conformations_x,vectors_s->conformations_y,vectors_s->conformations_z,flexibility_params,vectors_s->weights,vectors_s->f_params,vectors_s->nconformations,orden_device);
	}
	else
	{
		//DIVIDIR EN TROZOS
		while((vectors_s->nconformations % max_conformaciones) != 0) max_conformaciones --;		
		//printf("va a trocear\n");		
		for (unsigned int desplazamiento=0;desplazamiento<vectors_s->nconformations;desplazamiento+=max_conformaciones)
		{
			flexibility_function(ligando,param,metaheuristic,devices,vectors_s->conformations_x+desplazamiento,vectors_s->conformations_y+desplazamiento,vectors_s->conformations_z+desplazamiento,flexibility_params,vectors_s->weights,vectors_s->f_params,vectors_s->nconformations,orden_device);
			//printf("Device %d con trozo %d\n",devices->id[0],i);			
		}
	}
	multigpuSolver_flex (ligando,proteina,vectors_s->num_surface, vectors_s->conformations_x,vectors_s->conformations_y,vectors_s->conformations_z,vectors_s->move_x,vectors_s->move_y,vectors_s->move_z,vectors_s->quat_x, vectors_s->quat_y,vectors_s->quat_z,vectors_s->quat_w,vectors_s->energy,vectors_s->weights,vectors_s->f_params,vectors_s->nconformations, param, metaheuristic, devices,flexibility_params);
	
	omp_set_num_threads (metaheuristic.Threads1Com);
	#pragma omp parallel for			
	for (unsigned int i = 0; i < vectors_s->nconformations; i += stride_s)					
		thrust::stable_sort_by_key((vectors_s->energy.energy + i),(vectors_s->energy.energy + i + stride_s), (vectors_s->energy.n_conformation + i), thrust::less<type_data>());						
	//colocar_flex(ligando.nlig,param,vectors_s,metaheuristic);	
	#pragma omp parallel for
        for (unsigned int i = 0; i < vectors_s->num_surface; i++)
                colocar_flex_by_step (param,metaheuristic,ligando.nlig,vectors_s->conformations_x + (stride_s*i*ligando.nlig),vectors_s->conformations_y + (stride_s*i*ligando.nlig),vectors_s->conformations_z + (stride_s*i*ligando.nlig),vectors_s->move_x + (stride_s*i),vectors_s->move_y + (stride_s*i),vectors_s->move_z + (stride_s*i),vectors_s->quat_x + (stride_s*i),vectors_s->quat_y + (stride_s*i),vectors_s->quat_z + (stride_s*i),vectors_s->quat_w + (stride_s*i),vectors_s->energy.energy + (stride_s*i),vectors_s->energy.n_conformation + (stride_s*i),stride_s,i);
        thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
}	

