#ifndef __METAHEU_INC_FLEX_H__
#define __METAHEU_INC_FLEX_H__

extern void incluir_flex (unsigned int nlig, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param, type_data *max, unsigned int *improve, unsigned int *conformacion);
extern void colocar_flex (unsigned int nlig, struct param_t param, struct vectors_t *vectors_e_s,struct metaheuristic_t metaheuristic);
extern void colocar_flex_by_step (struct param_t param, struct metaheuristic_t metaheuristic, unsigned int nlig,  type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int stride, unsigned int pos);
extern void incluir_warm_up_flex (unsigned int nlig, struct vectors_t *vectors_e, struct param_t param, unsigned int hilos);

#endif
