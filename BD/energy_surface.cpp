
#include "energy_surface.h"
#include "energy_common.h"
using namespace std;


extern void write_surface_file(char *filename, type_data *surface_x, type_data *surface_y, type_data *surface_z, unsigned int nTot) {

	ofstream surface_file(filename, ios::out | ios::trunc);

	if (!surface_file.good()) {
		cout << "can't open file " << filename << endl;
		cout << "Will not write surface file" << endl;
		return;
	}
	surface_file << nTot << endl;
	for (unsigned int i = 0; i <  nTot; i++)
		surface_file << surface_x[i] << " " << surface_y[i] << " " << surface_z[i] << endl;
	surface_file.close();	
}

extern void count_surface (char *input, int nAtom, struct vectors_t* vectors) {

        unsigned int count, countBond;

        unsigned int dum1;
	unsigned int i3 = 0;
        char header1[FILENAME];
	type_data fx,fy,fz;
        


        FILE *fp;
        if ((fp =  fopen(input,"r")) == NULL) {
                printf("ReadInput(): Can't open file \"%s\"\n", input);
                exit(1);
        }
        if (skip_to_token(fp,"@<TRIPOS>MOLECULE")) 
	{
                exit(1);
                return;
        }
	if (skip_to_token(fp,"@<TRIPOS>ATOM")) {
                exit(1);
                return;
        }
        
        for (unsigned int i2 = 0; i2 < nAtom; i2++) 
	{
                fscanf(fp, "%d %s %f %f %f",&dum1,header1,&fx,&fy,&fz);
                if (strcmp(header1,"CA") == 0) 
		{
                        i3++;
                }
		 if (skip_to_eol(fp)) 
		{
                        exit(1);
                        return;
                }
        }
        fclose(fp);
	//printf("i3 %d\n",i3);
	vectors->num_surface = i3;
}

extern void energy_surface (char *input, struct vectors_t* vectors, int scoring_function) {

	unsigned int count, countBond;

	unsigned int nAtom,nBond;
	char header1[FILENAME];
	mystring type;


	FILE *fp;
	if ((fp =  fopen(input,"r")) == NULL) {
		printf("ReadInput(): Can't open file \"%s\"\n", input);
		exit(1);
	}
	if (skip_to_token(fp,"@<TRIPOS>MOLECULE")) {
		exit(1);
		return;
	}	
	fscanf(fp,"%s", header1);
	unsigned int dum1, dum2, dum3;
	fscanf(fp,"%u %u %u %u %u", &nAtom, &nBond, &dum1, &dum2,&dum3);
		
	vectors->surface_x = (type_data *)calloc((vectors->num_surface+1),sizeof(type_data));
	vectors->surface_y = (type_data *)calloc((vectors->num_surface+1),sizeof(type_data));
	vectors->surface_z = (type_data *)calloc((vectors->num_surface+1),sizeof(type_data));

	if (skip_to_token(fp,"@<TRIPOS>ATOM")) {
		exit(1);
		return;
	}
	unsigned int i3 = 0;
	for (unsigned int i2 = 0; i2 < nAtom; i2++) {		
		fscanf(fp, "%u %s %f %f %f",&dum2,type,&vectors->surface_x[i3],&vectors->surface_y[i3],&vectors->surface_z[i3]);		
		if (strcmp(type,"CA") == 0) {
			if (scoring_function != S_AD4)
			{
				vectors->surface_x[i3] /= 10.0;
				vectors->surface_y[i3] /= 10.0;
				vectors->surface_z[i3] /= 10.0;
			}
			i3++;
		}
		if (skip_to_eol(fp)) {
			exit(1);
			return;
		}		
	}
	//printf("x %f y %f z %f\n",vectors->surface_x[0],vectors->surface_y[0],vectors->surface_z[0]);
	//exit(0);
	fclose(fp);
}


