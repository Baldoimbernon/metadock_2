#ifndef __METAHEU_COMB_H__
#define __METAHEU_COMB_H__

extern unsigned int getRandomNumber_modified (unsigned int n, unsigned int act); 
extern void combinar_group (unsigned int nlig, type_data *e_move_x, type_data *e_move_y, type_data *e_move_z, type_data *e_quat_x, type_data *e_quat_y, type_data *e_quat_z, type_data *e_quat_w, type_data *s_move_x, type_data *s_move_y, type_data *s_move_z, type_data *s_quat_x, type_data *s_quat_y, type_data *s_quat_z, type_data *s_quat_w , struct metaheuristic_t metaheuristic, unsigned int mejores, unsigned int peores, unsigned int mejorespeores, unsigned int stride_e, unsigned int stride_s, struct param_t param);
extern void combinar (struct receptor_t proteina, struct ligand_t ligando, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param);
extern void combinar_gpu (struct receptor_t proteina, struct ligand_t ligando, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param, struct device_t *devices, unsigned int orden_device);
extern void combinar_multigpu (struct receptor_t proteina, struct ligand_t ligando, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param, struct device_t *devices);

#endif
