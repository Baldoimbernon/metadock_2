#ifndef __ENERGY_GPUSOLVER_H
#define __ENERGY_GPUSOLVER_H

#include "type.h"

extern void gpuSolver (struct ligand_t ligando, struct receptor_t proteina, struct param_t params, struct metaheuristic_t metaheuristic, struct device_t *devices, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconf, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations,unsigned int orden_device, unsigned int stride_conformations,type_data *time_kernels);

extern void gpuSolver_flex (struct ligand_t ligando, struct receptor_t proteina, struct param_t params, struct metaheuristic_t metaheuristic, struct device_t *devices, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconf, type_data *weights, struct force_field_param_t *f_params, unsigned int tor, unsigned int nconformations,unsigned int orden_device, unsigned int stride_conformations,bool *individual_bonds_VDW, bool *individual_bonds_ES, type_data *time_kernels);

extern void gpuSolver_calculation (struct ligand_t ligando, struct receptor_t proteina, struct param_t params, struct metaheuristic_t metaheuristic, struct device_t *devices, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconf, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations,unsigned int orden_device, unsigned int stride_conformations, type_data *time_kernels);

extern void gpuSolver_calculation_flex (struct ligand_t ligando, struct receptor_t proteina, struct param_t params, struct metaheuristic_t metaheuristic, struct device_t *devices, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconf, type_data *weights, struct force_field_param_t *f_params, unsigned int tor, unsigned int nconformations,unsigned int orden_device, unsigned int stride_conformations,bool *individual_bonds_VDW, bool *individual_bonds_ES, type_data *time_kernels);

#endif
