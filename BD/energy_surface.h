#ifndef _ENERGY_SURFACE_H_
#define _ENERGY_SURFACE_H_
#include "type.h"

extern void write_surface_file(char *file, type_data *surface_x, type_data *surface_y, type_data *surface_z, unsigned int nTot);
extern void energy_surface (char *input, struct vectors_t* vectors, int scoring_function);
extern void count_surface (char *input, int nAtom, struct vectors_t* vectors);

#endif

