#ifndef __CPUSOLVER_FLEX_H
#define __CPUSOLVER_FLEX_H
//#include "energy_common.h"
//#include "vector_types.h"


extern void generate_positions_cpp_flex (unsigned int nlig, struct param_t param, struct vectors_t *vectors, struct metaheuristic_t metaheuristic,struct flexibility_params_t *flexibility_params,struct flexibility_data_t *flexibility_conformations);
extern void generate_positions_cpp_warm_up_flex (unsigned int nlig, struct param_t param, struct vectors_t *vectors, struct metaheuristic_t metaheuristic, struct flexibility_params_t *flexibility_params,struct flexibility_data_t *flexibility_conformations);

extern void forces_CPU_flex (struct param_t param, unsigned int atoms_r, unsigned int atoms_l, unsigned int nlig, type_data *rec_x, type_data *rec_y, type_data *rec_z, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *moves_x, type_data *moves_y, type_data *moves_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, char* rectype, char* ligtype, type_data *ql ,type_data *qr, unsigned int *bonds_r, unsigned int *bonds_l, type_data *energy, unsigned int *nconfs, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct metaheuristic_t metaheuristic);

extern void forces_CPU_flex_type1 (struct param_t param, unsigned int atoms_r, unsigned int atoms_l,unsigned  int nlig, type_data *rec_x, type_data *rec_y, type_data *rec_z, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *moves_x, type_data *moves_y, type_data *moves_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, char* rectype, char* ligtype, type_data *ql ,type_data *qr, unsigned int *bonds_r, unsigned int *bonds_l, type_data *energy, unsigned int *nconfs, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct metaheuristic_t metaheuristic,unsigned int tor);

extern void forces_CPU_flex_type2 (struct param_t param, unsigned int atoms_r, unsigned int atoms_l,unsigned  int nlig, type_data *rec_x, type_data *rec_y, type_data *rec_z, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *moves_x, type_data *moves_y, type_data *moves_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, char* rectype, char* ligtype, type_data *ql ,type_data *qr, unsigned int *bonds_r, unsigned int *bonds_l, type_data *energy, unsigned int *nconfs, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct metaheuristic_t metaheuristic, unsigned int tor);

extern void ForcesCpuSolver_flex(struct param_t param, struct ligand_t ligando, struct receptor_t proteina, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z,type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct metaheuristic_t metaheuristic, unsigned int tor);

extern void energy_internal_conformations_cpu (unsigned int n, struct param_t param, struct metaheuristic_t metaheuristic, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, struct ligand_t *ligando, type_data *weights, struct force_field_param_t *f_parmas, type_data *energy, bool *individual_bonds_VDW, bool *individual_bonds_ES);

extern void energy_internal_conformations_cpu_type2 (unsigned int n, struct param_t param, struct metaheuristic_t metaheuristic, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, struct ligand_t *ligando, type_data *weights, struct force_field_param_t *f_params, type_data *energy, bool *individual_bonds_VDW, bool *individual_bonds_ES);

extern void energy_total (unsigned int n, struct param_t param, struct metaheuristic_t metaheuristic, type_data *energy_sig, type_data *internal_energy);
#endif
