#ifndef __CPUSOLVER_H
#define __CPUSOLVER_H
#include "definitions.h"

extern void generate_positions_cpp (struct param_t param, struct vectors_t *vectors, struct metaheuristic_t metaheuristic);
extern void generate_positions_cpp_warm_up (struct param_t param, struct vectors_t *vectors, struct metaheuristic_t metaheuristic);
extern void move (unsigned int num_surface, type_data *moves_x_h, type_data *moves_y_h, type_data *moves_z_h, type_data *surface_x_h, type_data *surface_y_h, type_data *surface_z_h, struct param_t param, struct metaheuristic_t metaheuristic,unsigned int num);
extern void move_cpp_warm_up (unsigned int num_surface, type_data *moves_x_h, type_data *moves_y_h, type_data *moves_z_h, type_data *surface_x_h, type_data *surface_y_h, type_data *surface_z_h, struct param_t param, unsigned int num, unsigned int hilos);
extern void rotate (type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, struct param_t param, unsigned int nconformations,struct metaheuristic_t metaheuristic );
extern void move_mejora (unsigned int num_surface, type_data *moves_x_h, type_data *moves_y_h, type_data *moves_z_h, struct param_t param, unsigned int NEIIni_selec, struct metaheuristic_t metaheuristic);
extern void move_mejora_cpp_warm_up (unsigned int num_surface, type_data *moves_x_h, type_data *moves_y_h, type_data *moves_z_h,  struct param_t param, unsigned int NEIIni_selec,unsigned int hilos);
extern void vdwforces_warm_up (struct param_t param, unsigned int atoms_r, unsigned int atoms_l, unsigned int nlig, type_data *rec_x, type_data *rec_y, type_data *rec_z, type_data *lig_x, type_data *lig_y, type_data *lig_z, type_data *moves_x, type_data *moves_y, type_data *moves_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, char* rectype, char* ligtype, struct energy_t energy, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations,unsigned int n1, unsigned int n2);
extern void ForcesCpuSolver(struct param_t param, struct ligand_t ligando, struct receptor_t proteina, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *weights, struct force_field_param_t * f_params, unsigned int nconformations, struct metaheuristic_t metaheuristic);


#endif
