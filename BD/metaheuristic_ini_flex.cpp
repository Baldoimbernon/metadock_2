#include "energy_common.h"
#include "energy_cuda.h"
#include "energy_positions.h"
#include "energy_positions_flex.h"
#include "energy_GpuSolver.h"
#include "energy_CpuSolver.h"
#include "energy_CpuSolver_flex.h"
#include "energy_montecarlo.h"
#include "flexibility.h"
#include "vector_types.h"
#include "metaheuristic_ini_flex.h"
#include "metaheuristic_inc_flex.h"
#include "metaheuristic_mgpu_flex.h"
#include "montecarlo.h"
#include "wtime.h"
#include <thrust/sequence.h>
#include <thrust/sort.h>
#include <thrust/functional.h>
#include <omp.h>

extern void fill_conformations (unsigned int N, struct param_t param, struct metaheuristic_t metaheuristic, struct ligand_t ligando,  struct vectors_t *vectors_e) 
{
	unsigned int th1;
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 0:
		case 3:
		case 2:
			th1 = metaheuristic.Threads1Inc;
			break;
	}		
	omp_set_num_threads (th1);
	#pragma omp parallel for 	
	for (unsigned int i = 0;i < (vectors_e->nconformations * ligando.nlig);i+=ligando.nlig)
	{
		memcpy(vectors_e->conformations_x+i,ligando.lig_x,ligando.nlig*sizeof(type_data));
		memcpy(vectors_e->conformations_y+i,ligando.lig_y,ligando.nlig*sizeof(type_data));
		memcpy(vectors_e->conformations_z+i,ligando.lig_z,ligando.nlig*sizeof(type_data));
	}
}

extern void incluir_gpu_flex_by_step (unsigned int nlig, type_data *e_conformations_x, type_data *e_conformations_y, type_data *e_conformations_z, type_data *e_move_x, type_data *e_move_y, type_data *e_move_z, type_data *e_quat_x, type_data *e_quat_y, type_data *e_quat_z, type_data *e_quat_w, type_data *e_energy, unsigned int *e_nconfs, type_data *s_conformations_x, type_data *s_conformations_y, type_data *s_conformations_z, type_data *s_move_x, type_data *s_move_y, type_data *s_move_z, type_data *s_quat_x, type_data *s_quat_y, type_data *s_quat_z, type_data *s_quat_w, type_data *s_energy, unsigned int *s_nconfs, unsigned int stride_e, unsigned int stride_s, unsigned int pos,struct param_t param, struct metaheuristic_t metaheuristic)
{

        struct vectors_t *vtmp = (struct vectors_t *)malloc(sizeof(struct vectors_t));
        vtmp->nconformations = stride_e + stride_s;
        vtmp->conformations_x = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations*nlig);
        vtmp->conformations_y = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations*nlig);
        vtmp->conformations_z = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations*nlig);
        vtmp->move_x = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->move_y = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->move_z = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->quat_x = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->quat_y = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->quat_z = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->quat_w = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->energy.energy = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vtmp->nconformations);
	thrust::sequence(vtmp->energy.n_conformation,vtmp->energy.n_conformation + vtmp->nconformations);

        thrust::stable_sort_by_key(e_energy,e_energy + stride_e, e_nconfs, thrust::less<type_data>());

        colocar_flex_by_step (param,metaheuristic,nlig,e_conformations_x,e_conformations_y,e_conformations_z,e_move_x,e_move_y,e_move_z,e_quat_x,e_quat_y,e_quat_z,e_quat_w,e_energy,e_nconfs,stride_e,pos);

        memcpy(vtmp->conformations_x, e_conformations_x, stride_e * nlig * sizeof(type_data));
        memcpy(vtmp->conformations_x + (stride_e * nlig), s_conformations_x, stride_s * nlig * sizeof(type_data));

        memcpy(vtmp->conformations_y, e_conformations_y, stride_e * nlig * sizeof(type_data));
        memcpy(vtmp->conformations_y + (stride_e * nlig), s_conformations_y, stride_s * nlig * sizeof(type_data));

        memcpy(vtmp->conformations_z, e_conformations_z, stride_e * nlig * sizeof(type_data));
        memcpy(vtmp->conformations_z + (stride_e * nlig), s_conformations_z, stride_s * nlig * sizeof(type_data));

        memcpy(vtmp->move_x, e_move_x, stride_e * sizeof(type_data));
        memcpy(vtmp->move_x + stride_e, s_move_x, stride_s * sizeof(type_data));

        memcpy(vtmp->move_y, e_move_y, stride_e * sizeof(type_data));
        memcpy(vtmp->move_y + stride_e, s_move_y, stride_s * sizeof(type_data));

        memcpy(vtmp->move_z, e_move_z, stride_e * sizeof(type_data));
        memcpy(vtmp->move_z + stride_e, s_move_z, stride_s * sizeof(type_data));

        memcpy(vtmp->quat_x, e_quat_x, stride_e * sizeof(type_data));
        memcpy(vtmp->quat_x + stride_e, s_quat_x, stride_s * sizeof(type_data));

	 memcpy(vtmp->quat_y, e_quat_y, stride_e * sizeof(type_data));
        memcpy(vtmp->quat_y + stride_e, s_quat_y, stride_s * sizeof(type_data));

        memcpy(vtmp->quat_z, e_quat_z, stride_e * sizeof(type_data));
        memcpy(vtmp->quat_z + stride_e, s_quat_z, stride_s * sizeof(type_data));

        memcpy(vtmp->quat_w, e_quat_w, stride_e * sizeof(type_data));
        memcpy(vtmp->quat_w + stride_e, s_quat_w, stride_s * sizeof(type_data));

        memcpy(vtmp->energy.energy, e_energy, stride_e * sizeof(type_data));
        memcpy(vtmp->energy.energy + stride_e, s_energy, stride_s * sizeof(type_data));

        thrust::stable_sort_by_key(vtmp->energy.energy,vtmp->energy.energy + vtmp->nconformations, vtmp->energy.n_conformation, thrust::less<type_data>());
        colocar_flex_by_step (param,metaheuristic,nlig,vtmp->conformations_x,vtmp->conformations_y,vtmp->conformations_z,vtmp->move_x,vtmp->move_y,vtmp->move_z,vtmp->quat_x,vtmp->quat_y,vtmp->quat_z,vtmp->quat_w,vtmp->energy.energy,vtmp->energy.n_conformation,vtmp->nconformations,0);

        memmove(s_conformations_x, vtmp->conformations_x, stride_s * nlig * sizeof(type_data));
        memmove(s_conformations_y, vtmp->conformations_y, stride_s * nlig * sizeof(type_data));
        memmove(s_conformations_z, vtmp->conformations_z, stride_s * nlig * sizeof(type_data));

        memmove(s_move_x, vtmp->move_x, stride_s * sizeof(type_data));
        memmove(s_move_y, vtmp->move_y, stride_s * sizeof(type_data));
        memmove(s_move_z, vtmp->move_z, stride_s * sizeof(type_data));

        memmove(s_quat_x, vtmp->quat_x, stride_s * sizeof(type_data));
        memmove(s_quat_y, vtmp->quat_y, stride_s * sizeof(type_data));
        memmove(s_quat_z, vtmp->quat_z, stride_s * sizeof(type_data));
        memmove(s_quat_w, vtmp->quat_w, stride_s * sizeof(type_data));

        memmove(s_energy, vtmp->energy.energy, stride_s * sizeof(type_data));

        free(vtmp->conformations_x);
        free(vtmp->conformations_y);
        free(vtmp->conformations_z);
        free(vtmp->move_x);
        free(vtmp->move_y);
        free(vtmp->move_z);
        free(vtmp->quat_x);
        free(vtmp->quat_y);
        free(vtmp->quat_z);
        free(vtmp->quat_w);
        free(vtmp->energy.energy);
        free(vtmp->energy.n_conformation);
}


extern void seleccionar_NEFIni_flex (unsigned int nlig, struct vectors_t *vectors_e, type_data *&conformations_x, type_data *&conformations_y, type_data *&conformations_z, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param)
{	
	unsigned int stride_s, stride_e, th1;

	stride_s = metaheuristic.NEFMIni_selec;
	stride_e = metaheuristic.NEIIni - metaheuristic.NEFPIni_selec;	
	
	vectors_s->num_surface = vectors_e->num_surface;
	vectors_s->nconformations = vectors_e->num_surface * metaheuristic.NEFIni;
	//printf("Número de conformaciones: %d\n",vectors_mejora.nconformations);
	//vectors_s->conformations_x = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);
	//vectors_s->conformations_y = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);
	//vectors_s->conformations_z = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);
	conformations_x = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);
        conformations_y = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);
        conformations_z = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);
	vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->surface_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->surface_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->surface_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nconformations);
	vectors_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);	
	vectors_s->weights = (type_data *)malloc(SCORING_TERMS*sizeof(type_data));
	
	memcpy(vectors_s->surface_x,vectors_e->surface_x,sizeof(type_data)*vectors_s->num_surface);
	memcpy(vectors_s->surface_y,vectors_e->surface_y,sizeof(type_data)*vectors_s->num_surface);
	memcpy(vectors_s->surface_z,vectors_e->surface_z,sizeof(type_data)*vectors_s->num_surface);
	
	//memcpy(vectors_s->vdw_params, vectors_e->vdw_params,MAXTYPES * sizeof(struct vdw_param_t));
	//memcpy(vectors_s->sasa_params, vectors_e->sasa_params,MAXTYPES * sizeof(struct sasa_param_t));
	//memcpy(vectors_s->hbond_params, vectors_e->hbond_params,MAXTYPES * sizeof(struct hbond_param_t));
	
	memcpy(vectors_s->f_params, vectors_e->f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_s->weights, vectors_e->weights,SCORING_TERMS * sizeof(type_data));
	
	
	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);

	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 0:
		case 3:
		case 2:
			th1 = metaheuristic.Threads1Ini;
			break;
	}			
	omp_set_num_threads (th1);
	#pragma omp parallel for 	
	for (unsigned int i = 0;i < vectors_s->num_surface;i++)
	{
		//memcpy(vectors_s->conformations_x + i*nlig*metaheuristic.NEFIni, vectors_e->conformations_x + i*nlig*metaheuristic.NEIIni, metaheuristic.NEFMIni_selec * nlig * sizeof(type_data));  	
		//memcpy(vectors_s->conformations_y + i*nlig*metaheuristic.NEFIni, vectors_e->conformations_y + i*nlig*metaheuristic.NEIIni, metaheuristic.NEFMIni_selec * nlig * sizeof(type_data)); 
		//memcpy(vectors_s->conformations_z + i*nlig*metaheuristic.NEFIni, vectors_e->conformations_z + i*nlig*metaheuristic.NEIIni, metaheuristic.NEFMIni_selec * nlig * sizeof(type_data));
		memcpy(conformations_x + i*nlig*metaheuristic.NEFIni, vectors_e->conformations_x + i*nlig*metaheuristic.NEIIni, metaheuristic.NEFMIni_selec * nlig * sizeof(type_data));
                memcpy(conformations_y + i*nlig*metaheuristic.NEFIni, vectors_e->conformations_y + i*nlig*metaheuristic.NEIIni, metaheuristic.NEFMIni_selec * nlig * sizeof(type_data));
                memcpy(conformations_z + i*nlig*metaheuristic.NEFIni, vectors_e->conformations_z + i*nlig*metaheuristic.NEIIni, metaheuristic.NEFMIni_selec * nlig * sizeof(type_data));
	
		memcpy(vectors_s->move_x + i*metaheuristic.NEFIni, (vectors_e->move_x + i*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * sizeof(type_data));  	
		memcpy(vectors_s->move_y + i*metaheuristic.NEFIni, (vectors_e->move_y + i*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * sizeof(type_data)); 
		memcpy(vectors_s->move_z + i*metaheuristic.NEFIni, (vectors_e->move_z + i*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * sizeof(type_data));
		memcpy(vectors_s->quat_x + i*metaheuristic.NEFIni, (vectors_e->quat_x + i*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * sizeof(type_data));
		memcpy(vectors_s->quat_y + i*metaheuristic.NEFIni, (vectors_e->quat_y + i*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * sizeof(type_data));
		memcpy(vectors_s->quat_z + i*metaheuristic.NEFIni, (vectors_e->quat_z + i*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * sizeof(type_data));
		memcpy(vectors_s->quat_w + i*metaheuristic.NEFIni, (vectors_e->quat_w + i*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * sizeof(type_data));
		memcpy(vectors_s->energy.energy + i*metaheuristic.NEFIni, (vectors_e->energy.energy + i*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * sizeof(type_data));
		//memcpy((vectors_s->energy.n_conformation + i*metaheuristic.NEFIni), (vectors_e->energy.n_conformation + i*metaheuristic.NEIIni),metaheuristic.NEFMIni_selec * sizeof(int));				
						
		//memcpy(vectors_s->conformations_x + i*nlig*metaheuristic.NEFIni + stride_s*nlig, (vectors_e->conformations_x + i*nlig*metaheuristic.NEIIni + stride_e*nlig), metaheuristic.NEFPIni_selec * nlig * sizeof(type_data));  	
		//memcpy(vectors_s->conformations_y + i*nlig*metaheuristic.NEFIni + stride_s*nlig, (vectors_e->conformations_y + i*nlig*metaheuristic.NEIIni + stride_e*nlig), metaheuristic.NEFPIni_selec * nlig * sizeof(type_data)); 
		//memcpy(vectors_s->conformations_z + i*nlig*metaheuristic.NEFIni + stride_s*nlig, (vectors_e->conformations_z + i*nlig*metaheuristic.NEIIni + stride_e*nlig), metaheuristic.NEFPIni_selec * nlig * sizeof(type_data));
		memcpy(conformations_x + i*nlig*metaheuristic.NEFIni + stride_s*nlig, (vectors_e->conformations_x + i*nlig*metaheuristic.NEIIni + stride_e*nlig), metaheuristic.NEFPIni_selec * nlig * sizeof(type_data));
                memcpy(conformations_y + i*nlig*metaheuristic.NEFIni + stride_s*nlig, (vectors_e->conformations_y + i*nlig*metaheuristic.NEIIni + stride_e*nlig), metaheuristic.NEFPIni_selec * nlig * sizeof(type_data));
                memcpy(conformations_z + i*nlig*metaheuristic.NEFIni + stride_s*nlig, (vectors_e->conformations_z + i*nlig*metaheuristic.NEIIni + stride_e*nlig), metaheuristic.NEFPIni_selec * nlig * sizeof(type_data));
						
		memcpy(vectors_s->move_x + i*metaheuristic.NEFIni + stride_s, (vectors_e->move_x + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(type_data));  	
		memcpy(vectors_s->move_y + i*metaheuristic.NEFIni + stride_s, (vectors_e->move_y + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(type_data)); 
		memcpy(vectors_s->move_z + i*metaheuristic.NEFIni + stride_s, (vectors_e->move_z + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(type_data));
		memcpy(vectors_s->quat_x + i*metaheuristic.NEFIni + stride_s, (vectors_e->quat_x + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(type_data));
		memcpy(vectors_s->quat_y + i*metaheuristic.NEFIni + stride_s, (vectors_e->quat_y + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(type_data));
		memcpy(vectors_s->quat_z + i*metaheuristic.NEFIni + stride_s, (vectors_e->quat_z + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(type_data));
		memcpy(vectors_s->quat_w + i*metaheuristic.NEFIni + stride_s, (vectors_e->quat_w + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(type_data));
		memcpy(vectors_s->energy.energy + i*metaheuristic.NEFIni + stride_s, (vectors_e->energy.energy + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(type_data));
		//memcpy((vectors_s->energy.n_conformation + i*metaheuristic.NEFIni + stride_s), (vectors_e->energy.n_conformation + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(int));									
	}
	//for(int k=0;k<64;k++)
	//	printf("SelEF e-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",k,vectors_e->conformations_x[k],vectors_e->conformations_y[k],vectors_e->conformations_z[k],vectors_e->move_x[k/nlig],vectors_e->move_y[k/nlig],vectors_e->move_z[k/nlig],vectors_e->energy.energy[k/nlig]);
	//printf("***\n");
	//for(int k=0;k<64;k++)
	//	printf("SelEF s-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",k,vectors_s->conformations_x[k],vectors_s->conformations_y[k],vectors_s->conformations_z[k],vectors_s->move_x[k/nlig],vectors_s->move_y[k/nlig],vectors_s->move_z[k/nlig],vectors_s->energy.energy[k/nlig]);
	
	//exit(0);
}

extern void incluir_mejorar_inicializar_flex (unsigned int nlig, struct vectors_t vectors_e, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int num_surface, unsigned int nconformations, struct metaheuristic_t metaheuristic, struct param_t param)
{	
	unsigned int id_conformation, nc_conf_d,nc_conf_o;
	unsigned int th1;
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 0:
		case 3:
		case 2:
			th1 = metaheuristic.Threads1Inc;
			break;
	}		
	
	omp_set_num_threads (th1);
	#pragma omp parallel for private (id_conformation, nc_conf_o, nc_conf_d) 				
	for (unsigned int i = 0;i < (vectors_e.nconformations);i++)
	{
		id_conformation = vectors_e.energy.n_conformation[i];
		//printf("i %d, id_conformation = %d\n",i,vectors_e.energy.n_conformation[i]);
		if (vectors_e.energy.energy[i] < energy[id_conformation])
		{
			nc_conf_o = id_conformation * nlig;
			nc_conf_d = id_conformation * nlig;
										
			memcpy(conformations_x + nc_conf_o,vectors_e.conformations_x + nc_conf_d, nlig * sizeof(type_data));
			memcpy(conformations_y + nc_conf_o,vectors_e.conformations_y + nc_conf_d, nlig * sizeof(type_data));
			memcpy(conformations_z + nc_conf_o,vectors_e.conformations_z + nc_conf_d, nlig * sizeof(type_data));					
									
			move_x[id_conformation] = vectors_e.move_x[id_conformation];
			move_y[id_conformation] = vectors_e.move_y[id_conformation];
			move_z[id_conformation] = vectors_e.move_z[id_conformation];
			quat_x[id_conformation] = vectors_e.quat_x[id_conformation];
			quat_y[id_conformation] = vectors_e.quat_y[id_conformation];
			quat_z[id_conformation] = vectors_e.quat_z[id_conformation];
			quat_w[id_conformation] = vectors_e.quat_w[id_conformation];
			energy[i] = vectors_e.energy.energy[i];
		}
		else
		{
			nc_conf_o = id_conformation * nlig;
			nc_conf_d = id_conformation * nlig;
										
			memcpy(vectors_e.conformations_x + nc_conf_o,conformations_x + nc_conf_d, nlig * sizeof(type_data));
			memcpy(vectors_e.conformations_y + nc_conf_o,conformations_y + nc_conf_d, nlig * sizeof(type_data));
			memcpy(vectors_e.conformations_z + nc_conf_o,conformations_z + nc_conf_d, nlig * sizeof(type_data));					
					
			vectors_e.move_x[id_conformation] = move_x[id_conformation];
			vectors_e.move_y[id_conformation] = move_y[id_conformation];
			vectors_e.move_z[id_conformation] = move_z[id_conformation];
			vectors_e.quat_x[id_conformation] = quat_x[id_conformation];
			vectors_e.quat_y[id_conformation] = quat_y[id_conformation];
			vectors_e.quat_z[id_conformation] = quat_z[id_conformation];
			vectors_e.quat_w[id_conformation] = quat_w[id_conformation];
			vectors_e.energy.energy[id_conformation] = energy[id_conformation];
		}
	}			
}

extern void incluir_mejorar_inicializar_environment_flex (unsigned int nlig, struct vectors_t vectors_e, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int nconformations, struct metaheuristic_t metaheuristic, struct param_t param)
{
        unsigned int id_conformation,th1;
        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 0:
                case 3:
                case 2:
                        th1 = metaheuristic.Threads1Inc;
                        break;
        }
        omp_set_num_threads (th1);
        #pragma omp parallel for //private (id_conformation)
        for (unsigned int i = 0;i < nconformations;i++)
        {

                //id_conformation = vectors_e.energy.n_conformation[i];
                for (unsigned int j = 0; j < metaheuristic.NEEImp; j++)
                {
			//if (i==0) printf("energy[0] %f  ****  vectors_e.energy.energy[i*metaheuristic.NEEImp + j] %f\n", energy[i],vectors_e.energy.energy[i*metaheuristic.NEEImp + j]);
                        if (vectors_e.energy.energy[i*metaheuristic.NEEImp + j] < energy[i])
                        {
				memcpy(conformations_x + i*nlig,vectors_e.conformations_x + i*metaheuristic.NEEImp*nlig + j*nlig, nlig * sizeof(type_data));
                        	memcpy(conformations_y + i*nlig,vectors_e.conformations_y + i*metaheuristic.NEEImp*nlig + j*nlig, nlig * sizeof(type_data));
				memcpy(conformations_z + i*nlig,vectors_e.conformations_z + i*metaheuristic.NEEImp*nlig + j*nlig, nlig * sizeof(type_data));
                                move_x[i] = vectors_e.move_x[i*metaheuristic.NEEImp + j];
                                move_y[i] = vectors_e.move_y[i*metaheuristic.NEEImp + j];
                                move_z[i] = vectors_e.move_z[i*metaheuristic.NEEImp + j];
                                quat_x[i] = vectors_e.quat_x[i*metaheuristic.NEEImp + j];
                                quat_y[i] = vectors_e.quat_y[i*metaheuristic.NEEImp + j];
                                quat_z[i] = vectors_e.quat_z[i*metaheuristic.NEEImp + j];
                                quat_w[i] = vectors_e.quat_w[i*metaheuristic.NEEImp + j];
                                energy[i] = vectors_e.energy.energy[i*metaheuristic.NEEImp + j];
                        }
                        else
                        {
				memcpy(vectors_e.conformations_x + i*metaheuristic.NEEImp*nlig + j*nlig,conformations_x + i*nlig, nlig * sizeof(type_data));
                                memcpy(vectors_e.conformations_y + i*metaheuristic.NEEImp*nlig + j*nlig,conformations_y + i*nlig, nlig * sizeof(type_data));
				memcpy(vectors_e.conformations_z + i*metaheuristic.NEEImp*nlig + j*nlig,conformations_z + i*nlig, nlig * sizeof(type_data));
                                vectors_e.move_x[i*metaheuristic.NEEImp + j] = move_x[i];
                                vectors_e.move_y[i*metaheuristic.NEEImp + j] = move_y[i];
                                vectors_e.move_z[i*metaheuristic.NEEImp + j] = move_z[i];
                                vectors_e.quat_x[i*metaheuristic.NEEImp + j] = quat_x[i];
                                vectors_e.quat_y[i*metaheuristic.NEEImp + j] = quat_y[i];
                                vectors_e.quat_z[i*metaheuristic.NEEImp + j] = quat_z[i];
                                vectors_e.quat_w[i*metaheuristic.NEEImp + j] = quat_w[i];
                                vectors_e.energy.energy[i*metaheuristic.NEEImp + j] = energy[i];
                        }
                }
        }
}

extern void mejorar_flex (struct receptor_t proteina, struct ligand_t ligando, struct param_t param,  type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int num_surface, unsigned int nconformations, struct metaheuristic_t metaheuristic, struct flexibility_params_t *flexibility_params, struct flexibility_data_t *flexibility_conformations, unsigned int opt)
{
	struct vectors_t vectors_mejora;
	unsigned int steps,moveType=MOVE,bucle,fase,simulations,total;	
	double tenergymejora;
	double tenergymejora_t;
	unsigned int tam = nconformations / num_surface;
	type_data *internal_energy = (type_data *)malloc(sizeof(type_data)*nconformations);
	type_data max_angle_flex;
	flexibility_conformations = (flexibility_data_t *)malloc(sizeof(flexibility_data_t)*nconformations);
	vectors_mejora.num_surface = num_surface;
	vectors_mejora.nconformations = nconformations;
	vectors_mejora.conformations_x = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_mejora.nconformations);
	vectors_mejora.conformations_y = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_mejora.nconformations);
	vectors_mejora.conformations_z = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_mejora.nconformations);

	vectors_mejora.move_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.move_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.move_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations);
	vectors_mejora.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);	
	vectors_mejora.weights = (type_data *)malloc(sizeof(type_data)*SCORING_TERMS);
 	//printf("Memoria para mejora creada....\n");
	
	memcpy(vectors_mejora.conformations_x, conformations_x, vectors_mejora.nconformations * ligando.nlig * sizeof(type_data));  
	memcpy(vectors_mejora.conformations_y, conformations_y, vectors_mejora.nconformations * ligando.nlig * sizeof(type_data));  
	memcpy(vectors_mejora.conformations_z, conformations_z, vectors_mejora.nconformations * ligando.nlig * sizeof(type_data));  

	memcpy(vectors_mejora.move_x, move_x, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.move_y, move_y, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.move_z, move_z, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_x, quat_x, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_y, quat_y, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_z, quat_z, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_w, quat_w, vectors_mejora.nconformations * sizeof(type_data));  
	
	//memcpy(vectors_mejora.energy.energy, vectors_s->energy.energy, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.energy.n_conformation, nconfs,vectors_mejora.nconformations * sizeof(int));	

	memcpy(vectors_mejora.f_params, f_params, MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_mejora.weights, weights, SCORING_TERMS * sizeof(type_data));
	//memset(vectors_mejora.energy.energy,0,vectors_s->nconformations * sizeof(type_data));
	
	//for(int k=0;k<64;k++)
	//	printf("Mejorar VM-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",k,vectors_mejora.conformations_x[k],vectors_mejora.conformations_y[k],vectors_mejora.conformations_z[k],vectors_mejora.move_x[k/ligando.nlig],vectors_mejora.move_y[k/ligando.nlig],vectors_mejora.move_z[k/ligando.nlig],vectors_mejora.energy.energy[k/ligando.nlig]);
	
	//exit(0);
	steps = 0;
	if (opt == 0)
        {
                total = metaheuristic.IMEIni + metaheuristic.IMEFlex;
                simulations = metaheuristic.IMEIni;
        }
        if (opt == 1)
        {
                total = metaheuristic.IMEImp + metaheuristic.IMEFlex;
                simulations = metaheuristic.IMEImp;
        }
        if (opt == 2)
        {
                total = metaheuristic.IMEMUCom + metaheuristic.IMEFlex;
                simulations = metaheuristic.IMEMUCom;
        }

        steps = 0;
	moveType = MOVE;
	//printf("Voy a mejorar....\n");
	while (steps < total){	

		if (steps >=simulations)
                {
                        //FLEXIBILIDAD
                        angulations_conformations_cpp (vectors_mejora.nconformations,metaheuristic,param,flexibility_conformations,flexibility_params->n_links,param.flex_angle);
                        flexibility_cpp (vectors_mejora.nconformations,metaheuristic,param,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,flexibility_conformations,ligando.nlig,flexibility_params->n_links,flexibility_params->n_fragments,flexibility_params->links,flexibility_params->links_fragments,flexibility_params->fragments,flexibility_params->fragments_tam,param.flex_angle);
                        max_angle_flex /= 1.1;
                }
		else
		{	
			if (moveType == MOVE){
				move_mejora(vectors_mejora.num_surface,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,param,tam,metaheuristic);
				//moveType = ROTATE;
				//printf("He movido\n");
			}
			if (moveType == ROTATE) {
				rotate(vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,param,vectors_mejora.nconformations,metaheuristic);
				//moveType = FLEX;
			}				
			if (moveType == FLEX) {
				angulations_conformations_cpp (vectors_mejora.nconformations,metaheuristic,param,flexibility_conformations,flexibility_params->n_links,param.flex_angle);
				flexibility_cpp (vectors_mejora.nconformations,metaheuristic,param,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,flexibility_conformations,ligando.nlig,flexibility_params->n_links,flexibility_params->n_fragments,flexibility_params->links,flexibility_params->links_fragments,flexibility_params->fragments,flexibility_params->fragments_tam,param.flex_angle);							
				//moveType = MOVE;
			}		
		}
		//printf("Voy a calcular la energia...\n");
		tenergymejora = wtime();		
		ForcesCpuSolver_flex(param,ligando,proteina,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,vectors_mejora.energy.energy,vectors_mejora.energy.n_conformation,vectors_mejora.weights,vectors_mejora.f_params,vectors_mejora.nconformations,metaheuristic,flexibility_params->n_links);		
		energy_internal_conformations_cpu (vectors_mejora.nconformations,param,metaheuristic,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,&ligando,vectors_mejora.weights,vectors_mejora.f_params,internal_energy,flexibility_params->individual_bonds_VDW,flexibility_params->individual_bonds_ES);
		energy_total (vectors_mejora.nconformations,param,metaheuristic,vectors_mejora.energy.energy,internal_energy);		
		tenergymejora_t += wtime() - tenergymejora;
		incluir_mejorar_inicializar_flex(ligando.nlig,vectors_mejora,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,num_surface,nconformations,metaheuristic,param);	
		steps++;
		if (moveType == MOVE) moveType = ROTATE;
		else if (moveType == ROTATE) moveType = FLEX;
		else if (moveType == FLEX) moveType = MOVE;
		//for(int k=0;k<64;k++)
		//	printf("STP %d Mejorar VS-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",steps,k,vectors_s->conformations_x[k],vectors_s->conformations_y[k],vectors_s->conformations_z[k],vectors_s->move_x[k/ligando.nlig],vectors_s->move_y[k/ligando.nlig],vectors_s->move_z[k/ligando.nlig],vectors_s->energy.energy[k/ligando.nlig]);
	
	}
	//if (opt==0)
	//	printf("\tCALCULO ENERGIA L-J EN FASE MEJORA INICIALIZAR: %lf seg\n",tenergymejora_t);
	//else
	//	printf("\tCALCULO ENERGIA L-J EN FASE MEJORAR: %lf seg\n",tenergymejora_t);
	//for(int k=0;k<64;k++)
		//	printf("STP %d Mejorar VM-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",steps,k,vectors_mejora.conformations_x[k],vectors_mejora.conformations_y[k],vectors_mejora.conformations_z[k],vectors_mejora.move_x[k/ligando.nlig],vectors_mejora.move_y[k/ligando.nlig],vectors_mejora.move_z[k/ligando.nlig],vectors_mejora.energy.energy[k/ligando.nlig]);
		

	free(vectors_mejora.conformations_x);
	free(vectors_mejora.conformations_y);
	free(vectors_mejora.conformations_z);
	free(vectors_mejora.move_x);
	free(vectors_mejora.move_y);
	free(vectors_mejora.move_z);
	free(vectors_mejora.quat_x);
	free(vectors_mejora.quat_y);
	free(vectors_mejora.quat_z);
	free(vectors_mejora.quat_w);
	free(vectors_mejora.energy.energy);
	free(vectors_mejora.energy.n_conformation);
	free(internal_energy);
	free(flexibility_conformations);
	free(vectors_mejora.weights);
	//for(int k=0;k<64;k++)
	//	printf("STP %d Mejorar VS-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",steps,k,vectors_s->conformations_x[k],vectors_s->conformations_y[k],vectors_s->conformations_z[k],vectors_s->move_x[k/ligando.nlig],vectors_s->move_y[k/ligando.nlig],vectors_s->move_z[k/ligando.nlig],vectors_s->energy.energy[k/ligando.nlig]);
	//exit(0);		
	
}

extern void mejorar_flex_environment (struct receptor_t proteina, struct ligand_t ligando, struct param_t param,  type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int num_surface, unsigned int nconformations, struct metaheuristic_t metaheuristic, struct flexibility_params_t *flexibility_params, struct flexibility_data_t *flexibility_conformations, unsigned int opt)
{
        struct vectors_t vectors_mejora;
        unsigned int steps,moveType=MOVE,bucle,fase,simulations,total;
        double tenergymejora;
        double tenergymejora_t;
        unsigned int tam = (nconformations *  metaheuristic.NEEImp) / num_surface;
        type_data *internal_energy = (type_data *)malloc(sizeof(type_data)*nconformations* metaheuristic.NEEImp);
	type_data max_angle_flex;
        flexibility_conformations = (flexibility_data_t *)malloc(sizeof(flexibility_data_t)*nconformations* metaheuristic.NEEImp);
        vectors_mejora.num_surface = num_surface;
        vectors_mejora.nconformations = nconformations * metaheuristic.NEEImp;
        vectors_mejora.conformations_x = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_mejora.nconformations);
        vectors_mejora.conformations_y = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_mejora.nconformations);
        vectors_mejora.conformations_z = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_mejora.nconformations);

        vectors_mejora.move_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.move_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.move_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations);
        vectors_mejora.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.weights = (type_data *)malloc(sizeof(type_data)*SCORING_TERMS);
        //printf("Memoria para mejora creada....\n");
	memcpy(vectors_mejora.f_params, f_params, MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_mejora.weights, weights, SCORING_TERMS * sizeof(type_data));
	thrust::sequence(vectors_mejora.energy.n_conformation,vectors_mejora.energy.n_conformation + vectors_mejora.nconformations);

	fill_conformations_environment_flex (ligando.nlig,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w, vectors_mejora.conformations_x, vectors_mejora.conformations_y, vectors_mejora.conformations_z, vectors_mejora.move_x, vectors_mejora.move_y, vectors_mejora.move_z, vectors_mejora.quat_x, vectors_mejora.quat_y, vectors_mejora.quat_z, vectors_mejora.quat_w, nconformations, param, metaheuristic);

	steps = 0;
        if (opt == 0)
        {
                total = metaheuristic.IMEIni + metaheuristic.IMEFlex;
                simulations = metaheuristic.IMEIni;
        }
        if (opt == 1)
        {
                total = metaheuristic.IMEImp + metaheuristic.IMEFlex;
                simulations = metaheuristic.IMEImp;
        }
        if (opt == 2)
        {
                total = metaheuristic.IMEMUCom + metaheuristic.IMEFlex;
                simulations = metaheuristic.IMEMUCom;
        }
        moveType = MOVE;
	//for (int l=0;l<(ligando.nlig*metaheuristic.NEEImp);l++) printf("conf %d, cx %f cy %f cz %f mx %f my %f mz %f qx %f qy %f qz %f qw %f\n",l/ligando.nlig,vectors_mejora.conformations_x[l],vectors_mejora.conformations_y[l],vectors_mejora.conformations_z[l],vectors_mejora.move_x[l/ligando.nlig],vectors_mejora.move_y[l/ligando.nlig],vectors_mejora.move_z[l/ligando.nlig],vectors_mejora.quat_x[l/ligando.nlig],vectors_mejora.quat_y[l/ligando.nlig],vectors_mejora.quat_z[l/ligando.nlig],vectors_mejora.quat_w[l/ligando.nlig]);
	//exit(0);
        //printf("Voy a mejorar....\n");
        while (steps < total){
		
		//for (int l=0;l<metaheuristic.NEEImp;l++) printf("ANTES conf %d, mx %f my %f mz %f qx %f qy %f qz %f qw %f energy %f\n",l,vectors_mejora.move_x[l],vectors_mejora.move_y[l],vectors_mejora.move_z[l],vectors_mejora.quat_x[l],vectors_mejora.quat_y[l],vectors_mejora.quat_z[l],vectors_mejora.quat_w[l],vectors_mejora.energy.energy[l]);

		if (steps >=simulations)
                {
			angulations_conformations_cpp (vectors_mejora.nconformations,metaheuristic,param,flexibility_conformations,flexibility_params->n_links,param.flex_angle);
                        flexibility_cpp (vectors_mejora.nconformations,metaheuristic,param,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,flexibility_conformations,ligando.nlig,flexibility_params->n_links,flexibility_params->n_fragments,flexibility_params->links,flexibility_params->links_fragments,flexibility_params->fragments,flexibility_params->fragments_tam,param.flex_angle);
		}
		else
		{
                	if (moveType == MOVE){
                        	move_mejora(vectors_mejora.num_surface,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,param,tam,metaheuristic);
                        	//moveType = ROTATE;
                        	//printf("He movido\n");
                	}
                	if (moveType == ROTATE) {
                        	rotate(vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,param,vectors_mejora.nconformations,metaheuristic);
                        	//moveType = FLEX;
                	}
                	if (moveType == FLEX) {
                        	angulations_conformations_cpp (vectors_mejora.nconformations,metaheuristic,param,flexibility_conformations,flexibility_params->n_links,param.flex_angle);
                        	flexibility_cpp (vectors_mejora.nconformations,metaheuristic,param,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,flexibility_conformations,ligando.nlig,flexibility_params->n_links,flexibility_params->n_fragments,flexibility_params->links,flexibility_params->links_fragments,flexibility_params->fragments,flexibility_params->fragments_tam,param.flex_angle);
                        	//moveType = MOVE;
                	}
		}
                	//printf("Voy a calcular la energia...\n");
                tenergymejora = wtime();
                ForcesCpuSolver_flex(param,ligando,proteina,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,vectors_mejora.energy.energy,vectors_mejora.energy.n_conformation,vectors_mejora.weights,vectors_mejora.f_params,vectors_mejora.nconformations,metaheuristic,flexibility_params->n_links);
		energy_internal_conformations_cpu (vectors_mejora.nconformations,param,metaheuristic,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,&ligando,vectors_mejora.weights,vectors_mejora.f_params,internal_energy,flexibility_params->individual_bonds_VDW,flexibility_params->individual_bonds_ES);
                energy_total (vectors_mejora.nconformations,param,metaheuristic,vectors_mejora.energy.energy,internal_energy);
                tenergymejora_t += wtime() - tenergymejora;
                
		//for (int l=0;l<metaheuristic.NEEImp;l++) printf("DESPUES conf %d, mx %f my %f mz %f qx %f qy %f qz %f qw %f energy %f\n",l,vectors_mejora.move_x[l],vectors_mejora.move_y[l],vectors_mejora.move_z[l],vectors_mejora.quat_x[l],vectors_mejora.quat_y[l],vectors_mejora.quat_z[l],vectors_mejora.quat_w[l],vectors_mejora.energy.energy[l]);
        	//exit(0);

	//	incluir_mejorar_inicializar_environment_flex(vectors_mejora, move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,nconformations,metaheuristic, param);
		incluir_mejorar_inicializar_environment_flex(ligando.nlig,vectors_mejora,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,nconformations,metaheuristic,param);

		fill_conformations_environment_flex (ligando.nlig,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w, vectors_mejora.conformations_x, vectors_mejora.conformations_y, vectors_mejora.conformations_z, vectors_mejora.move_x, vectors_mejora.move_y, vectors_mejora.move_z, vectors_mejora.quat_x, vectors_mejora.quat_y, vectors_mejora.quat_z, vectors_mejora.quat_w, nconformations, param, metaheuristic);
                steps++;
                if (moveType == MOVE) moveType = ROTATE;
                else if (moveType == ROTATE) moveType = FLEX;
                else if (moveType == FLEX) moveType = MOVE;
		
                //for(int k=0;k<64;k++)
                //      printf("STP %d Mejorar VS-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",steps,k,vectors_s->conformations_x[k],vectors_s->conformations_y[k],vectors_s->conformations_z[k],vectors_s->move_x[k/ligando.nlig],vectors_s->move_y[k/ligando.nlig],vectors_s->move_z[k/ligando.nlig],vectors_s->energy.energy[k/ligando.nlig]);

        }
	//exit(0);
	free(vectors_mejora.conformations_x);
        free(vectors_mejora.conformations_y);
        free(vectors_mejora.conformations_z);
        free(vectors_mejora.move_x);
        free(vectors_mejora.move_y);
        free(vectors_mejora.move_z);
        free(vectors_mejora.quat_x);
        free(vectors_mejora.quat_y);
        free(vectors_mejora.quat_z);
        free(vectors_mejora.quat_w);
        free(vectors_mejora.energy.energy);
        free(vectors_mejora.energy.n_conformation);
        free(internal_energy);
        free(flexibility_conformations);
        free(vectors_mejora.weights);
        //for(int k=0;k<64;k++)
        //      printf("STP %d Mejorar VS-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",steps,k,vectors_s->conformations_x[k],vectors_s->conformations_y[k],vectors_s->conformations_z[k],vectors_s->move_x[k/ligando.nlig],vectors_s->move_y[k/ligando.nlig],vectors_s->move_z[k/ligando.nlig],vectors_s->energy.energy[k/ligando.nlig]); 
	//exit(0);

}


extern void seleccionar_inicializar_flex (unsigned int nlig, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic)
{
	unsigned int v1_tmp,l;
	type_data v2_tmp, v3_tmp;
	unsigned int th1;
		
	v1_tmp = vectors_e->nconformations / vectors_e->num_surface;
	metaheuristic.NEIIni_selec = (v1_tmp * metaheuristic.PEMIni)/100;
	//v3_tmp = getPadding(metaheuristic.NEIIni_selec,16);
	//metaheuristic.NEIIni_selec += v3_tmp; 
	
	vectors_s->num_surface = vectors_e->num_surface;
	vectors_s->nconformations = vectors_e->num_surface * metaheuristic.NEIIni_selec;
	vectors_s->conformations_x = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);
	vectors_s->conformations_y = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);
	vectors_s->conformations_z = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);	
	vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nconformations);
	vectors_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);	
	vectors_s->weights = (type_data *)malloc(SCORING_TERMS*sizeof(type_data));

	//memcpy(vectors_s->vdw_params, vectors_e->vdw_params,MAXTYPES * sizeof(struct vdw_param_t)); 
	//memcpy(vectors_s->sasa_params, vectors_e->sasa_params,MAXTYPES * sizeof(struct sasa_param_t)); 
	//memcpy(vectors_s->hbond_params, vectors_e->hbond_params,MAXTYPES * sizeof(struct hbond_param_t)); 
	memcpy(vectors_s->f_params, vectors_e->f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_s->weights, vectors_e->weights,SCORING_TERMS * sizeof(type_data));

	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 0:
		case 3:
		case 2:
			th1 = metaheuristic.Threads1Sel;
			break;
	}	
	omp_set_num_threads (th1);
	#pragma omp parallel for 	
	for (unsigned int i = 0; i < vectors_e->num_surface; i++)
	{
		memcpy(vectors_s->conformations_x + i*nlig*metaheuristic.NEIIni_selec , vectors_e->conformations_x + i*nlig*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * nlig * sizeof(type_data));  	
		memcpy(vectors_s->conformations_y + i*nlig*metaheuristic.NEIIni_selec, vectors_e->conformations_y + i*nlig*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * nlig * sizeof(type_data)); 
		memcpy(vectors_s->conformations_z + i*nlig*metaheuristic.NEIIni_selec, vectors_e->conformations_z + i*nlig*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * nlig * sizeof(type_data));  
				
		memcpy(vectors_s->move_x + i*metaheuristic.NEIIni_selec , vectors_e->move_x + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));  	
		memcpy(vectors_s->move_y + i*metaheuristic.NEIIni_selec, vectors_e->move_y + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data)); 
		memcpy(vectors_s->move_z + i*metaheuristic.NEIIni_selec, vectors_e->move_z + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));  
		memcpy(vectors_s->quat_x + i*metaheuristic.NEIIni_selec, vectors_e->quat_x + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));
		memcpy(vectors_s->quat_y + i*metaheuristic.NEIIni_selec, vectors_e->quat_y + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));
		memcpy(vectors_s->quat_z + i*metaheuristic.NEIIni_selec, vectors_e->quat_z + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));
		memcpy(vectors_s->quat_w + i*metaheuristic.NEIIni_selec, vectors_e->quat_w + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));
		memcpy((vectors_s->energy.energy + i*metaheuristic.NEIIni_selec), (vectors_e->energy.energy + i*metaheuristic.NEIIni), metaheuristic.NEIIni_selec * sizeof(type_data));		
	}
	//for(int k=0;k<64;k++)
	//	printf("SIni e-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",k,vectors_e->conformations_x[k],vectors_e->conformations_y[k],vectors_e->conformations_z[k],vectors_e->move_x[k/nlig],vectors_e->move_y[k/nlig],vectors_e->move_z[k/nlig],vectors_e->energy.energy[k/nlig]);
	//printf("***\n");
	//for(int k=0;k<64;k++)
	//	printf("SIni s-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",k,vectors_s->conformations_x[k],vectors_s->conformations_y[k],vectors_s->conformations_z[k],vectors_s->move_x[k/nlig],vectors_s->move_y[k/nlig],vectors_s->move_z[k/nlig],vectors_s->energy.energy[k/nlig]);	
	//exit(0);	
}

extern void seleccionar_mejorar_flex (unsigned int nlig, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic)
{
	unsigned int v1_tmp,l,th1;
	type_data v2_tmp, v3_tmp;
	v1_tmp = vectors_e->nconformations / vectors_e->num_surface;
	metaheuristic.PEMSel_selec = (v1_tmp * metaheuristic.PEMSel)/100;
	//v3_tmp = getPadding(metaheuristic.PEMSel_selec,16);
	//metaheuristic.PEMSel_selec += v3_tmp; 
	//printf("NEISelec %d\n",metaheuristic.PEMSel_selec);	
	
	free(vectors_s->conformations_x);
	free(vectors_s->conformations_y);
	free(vectors_s->conformations_z);
	
	free(vectors_s->move_x);
	free(vectors_s->move_y);
	free(vectors_s->move_z);
	free(vectors_s->quat_x);
	free(vectors_s->quat_y);
	free(vectors_s->quat_z);
	free(vectors_s->quat_w);
	free(vectors_s->energy.energy);
	free(vectors_s->energy.n_conformation);
	free(vectors_s->weights);
		
	vectors_s->num_surface = vectors_e->num_surface;
	vectors_s->nconformations = vectors_e->num_surface * metaheuristic.PEMSel_selec;
	vectors_s->conformations_x = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);
	vectors_s->conformations_y = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);
	vectors_s->conformations_z = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);
	
	vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nconformations);
	vectors_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);	
	vectors_s->weights = (type_data *)malloc(SCORING_TERMS*sizeof(type_data));

	//memcpy(vectors_s->vdw_params, vectors_e->vdw_params,MAXTYPES * sizeof(struct vdw_param_t)); 
	//memcpy(vectors_s->sasa_params, vectors_e->sasa_params,MAXTYPES * sizeof(struct sasa_param_t)); 
	//memcpy(vectors_s->hbond_params, vectors_e->hbond_params,MAXTYPES * sizeof(struct hbond_param_t)); 
	memcpy(vectors_s->f_params, vectors_e->f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_s->weights, vectors_e->weights,SCORING_TERMS * sizeof(type_data));

	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 0:
		case 3:
		case 2:
			th1 = metaheuristic.Threads1Sel;
			break;
	}	
	omp_set_num_threads (metaheuristic.Threads1Sel);
	#pragma omp parallel for 	
	for (unsigned int i = 0; i < vectors_e->num_surface; i++)
	{
		memcpy(vectors_s->conformations_x + i*nlig*metaheuristic.PEMSel_selec, vectors_e->conformations_x + i*nlig*v1_tmp, metaheuristic.PEMSel_selec * nlig * sizeof(type_data));  	
		memcpy(vectors_s->conformations_y + i*nlig*metaheuristic.PEMSel_selec, vectors_e->conformations_y + i*nlig*v1_tmp, metaheuristic.PEMSel_selec * nlig * sizeof(type_data)); 
		memcpy(vectors_s->conformations_z + i*nlig*metaheuristic.PEMSel_selec, vectors_e->conformations_z + i*nlig*v1_tmp, metaheuristic.PEMSel_selec * nlig * sizeof(type_data));  
								
		memcpy(vectors_s->move_x + i*metaheuristic.PEMSel_selec , vectors_e->move_x + i*v1_tmp, metaheuristic.PEMSel_selec * sizeof(type_data));  	
		memcpy(vectors_s->move_y + i*metaheuristic.PEMSel_selec, vectors_e->move_y + i*v1_tmp, metaheuristic.PEMSel_selec * sizeof(type_data)); 
		memcpy(vectors_s->move_z + i*metaheuristic.PEMSel_selec, vectors_e->move_z + i*v1_tmp, metaheuristic.PEMSel_selec * sizeof(type_data));  
		memcpy(vectors_s->quat_x + i*metaheuristic.PEMSel_selec, vectors_e->quat_x + i*v1_tmp, metaheuristic.PEMSel_selec * sizeof(type_data));
		memcpy(vectors_s->quat_y + i*metaheuristic.PEMSel_selec, vectors_e->quat_y + i*v1_tmp, metaheuristic.PEMSel_selec * sizeof(type_data));
		memcpy(vectors_s->quat_z + i*metaheuristic.PEMSel_selec, vectors_e->quat_z + i*v1_tmp, metaheuristic.PEMSel_selec * sizeof(type_data));
		memcpy(vectors_s->quat_w + i*metaheuristic.PEMSel_selec, vectors_e->quat_w + i*v1_tmp, metaheuristic.PEMSel_selec * sizeof(type_data));
		memcpy((vectors_s->energy.energy + i*metaheuristic.PEMSel_selec), (vectors_e->energy.energy + i*v1_tmp), metaheuristic.PEMSel_selec * sizeof(type_data));		
	}
}

extern void seleccionar_inicializar_multigpu_flex (unsigned int nlig, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic)
{
	//printf("NEISelec %d\n",metaheuristic.NEIIni_selec);	
	vectors_s->num_surface = vectors_e->num_surface;
	vectors_s->nconformations = vectors_e->num_surface * metaheuristic.NEIIni_selec;
	vectors_s->conformations_x = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);
	vectors_s->conformations_y = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);
	vectors_s->conformations_z = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);	
	vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nconformations);
	vectors_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);	
	vectors_s->weights = (type_data *)malloc(SCORING_TERMS*sizeof(type_data));

	//memcpy(vectors_s->vdw_params, vectors_e->vdw_params,MAXTYPES * sizeof(struct vdw_param_t)); 
	//memcpy(vectors_s->sasa_params, vectors_e->sasa_params,MAXTYPES * sizeof(struct sasa_param_t)); 
	//memcpy(vectors_s->hbond_params, vectors_e->hbond_params,MAXTYPES * sizeof(struct hbond_param_t)); 
	memcpy(vectors_s->f_params, vectors_e->f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_s->weights, vectors_e->weights,SCORING_TERMS * sizeof(type_data));

	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
	
 	omp_set_num_threads (metaheuristic.Threads1Sel);
	#pragma omp parallel for 	
	for (unsigned int i = 0; i < vectors_e->num_surface; i++)
	{
		memcpy(vectors_s->conformations_x + i*nlig*metaheuristic.NEIIni_selec, vectors_e->conformations_x + i*nlig*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * nlig * sizeof(type_data));  	
		memcpy(vectors_s->conformations_y + i*nlig*metaheuristic.NEIIni_selec, vectors_e->conformations_y + i*nlig*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * nlig * sizeof(type_data)); 
		memcpy(vectors_s->conformations_z + i*nlig*metaheuristic.NEIIni_selec, vectors_e->conformations_z + i*nlig*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * nlig * sizeof(type_data));  
		
		memcpy(vectors_s->move_x + i*metaheuristic.NEIIni_selec , vectors_e->move_x + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));  	
		memcpy(vectors_s->move_y + i*metaheuristic.NEIIni_selec, vectors_e->move_y + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data)); 
		memcpy(vectors_s->move_z + i*metaheuristic.NEIIni_selec, vectors_e->move_z + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));  
		memcpy(vectors_s->quat_x + i*metaheuristic.NEIIni_selec, vectors_e->quat_x + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));
		memcpy(vectors_s->quat_y + i*metaheuristic.NEIIni_selec, vectors_e->quat_y + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));
		memcpy(vectors_s->quat_z + i*metaheuristic.NEIIni_selec, vectors_e->quat_z + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));
		memcpy(vectors_s->quat_w + i*metaheuristic.NEIIni_selec, vectors_e->quat_w + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));
		memcpy((vectors_s->energy.energy + i*metaheuristic.NEIIni_selec), (vectors_e->energy.energy + i*metaheuristic.NEIIni), metaheuristic.NEIIni_selec * sizeof(type_data));
	}			
	
}
extern void inicializar_flex (struct receptor_t proteina, struct ligand_t ligando, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices,struct flexibility_params_t *flexibility_params,struct flexibility_data_t *flexibility_conformations, type_data **dqn_data)
{
	
	//CONFIGURING MEMORY
	unsigned int NEItemp, NEI_selec_temp, th1, stride_e, stride_s;
	type_data *internal_energy, kernels=0;
	unsigned int max_conformaciones;
	double tgenerarposi, tenergycpu, tordenarcpu, tcolocarcpu, tmejorainicpu;
	double tgenerarposi_t, tenergycpu_t, tordenarcpu_t, tcolocarcpu_t, tmejorainicpu_t;	
	struct vectors_t vectors_selec;
	
	switch (param.mode) {
		case 0:
			//GPU SELECTION
			/*int *device_current;
			cudaDeviceProp *prop;
			device_current = new int;
			prop = new cudaDeviceProp();
			cudaSetDevice(devices->id[0]);
			cudaGetDevice(device_current);
			cudaGetDeviceProperties (prop, *device_current);
			printf("\n%s\n",prop->name);*/
			//NEItemp	= metaheuristic.NEIIni;
			//NEItemp = getPadding(NEItemp,16);
			//metaheuristic.NEIIni += NEItemp;
			NEI_selec_temp = (unsigned int)ceil((metaheuristic.NEIIni * metaheuristic.PEMIni) / 100);
			metaheuristic.NEIIni_selec = NEI_selec_temp;// + getPadding(NEI_selec_temp,16);
			vectors_e->nconformations = vectors_e->num_surface * metaheuristic.NEIIni;
			vectors_e->conformations_x = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_e->nconformations);
			vectors_e->conformations_y = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_e->nconformations);
			vectors_e->conformations_z = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_e->nconformations);			
			vectors_e->move_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->move_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->move_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_e->nconformations);
			vectors_e->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			fill_conformations (vectors_e->nconformations,param,metaheuristic,ligando,vectors_e);
			tgenerarposi = wtime();//tomamos tiempos
			generate_positions_flex (ligando.atoms,ligando.nlig,param,vectors_e,metaheuristic,devices,0,flexibility_params,0);
			tgenerarposi_t += wtime() - tgenerarposi;
			replace_for_dqn(dqn_data,vectors_e,metaheuristic);
			/*for(int k=0;k<256;k++)
                                    printf("GEN %d, mx %f, my %f, mz %f, q_x %f, q_y %f, q_z %f, q_w %f\n",k,vectors_e->move_x[k],vectors_e->move_y[k],vectors_e->move_z[k],vectors_e->quat_x[k],vectors_e->quat_y[k],vectors_e->quat_z[k],vectors_e->quat_w[k]);
                            //    exit(0);
			exit(0);*/
			tenergycpu = wtime();//tomamos tiempos para calculo 
			max_conformaciones = ((devices->propiedades[0].maxGridSize[0] * devices->hilos[0*metaheuristic.num_kernels]) / WARP_SIZE) * 8;
			//for(int k=0;512 * ligando.nlig;k++)
			//          printf("EN e-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",k,vectors_e->conformations_x[k],vectors_e->conformations_y[k],vectors_e->conformations_z[k],vectors_e->move_x[k/ligando.nlig],vectors_e->move_y[k/ligando.nlig],vectors_e->move_z[k/ligando.nlig],vectors_e->energy.energy[k/ligando.nlig]);
			//exit(0);						

			if (vectors_e->nconformations < max_conformaciones)
				gpuSolver_flex (ligando,proteina,param,metaheuristic,devices,vectors_e->conformations_x, vectors_e->conformations_y, vectors_e->conformations_z,vectors_e->move_x,vectors_e->move_y,vectors_e->move_z,vectors_e->quat_x,vectors_e->quat_y,vectors_e->quat_z,vectors_e->quat_w,vectors_e->energy.energy,vectors_e->energy.n_conformation,vectors_e->weights,vectors_e->f_params,flexibility_params->n_links, vectors_e->nconformations,0,0,flexibility_params->individual_bonds_VDW,flexibility_params->individual_bonds_ES,&kernels);		
			
			else
			{
				//DIVIDIR EN TROZOS
				while((vectors_s->nconformations % max_conformaciones) != 0) max_conformaciones --;		
				//printf("va a trocear\n");				
				for (unsigned int desplazamiento=0;desplazamiento<vectors_e->nconformations;desplazamiento+=max_conformaciones)
				{
					gpuSolver_flex (ligando,proteina,param,metaheuristic,devices,vectors_e->conformations_x+(desplazamiento*ligando.nlig), vectors_e->conformations_y+(desplazamiento*ligando.nlig),vectors_e->conformations_z+(desplazamiento*ligando.nlig), vectors_e->move_x+desplazamiento,vectors_e->move_y+desplazamiento,vectors_e->move_z+desplazamiento,vectors_e->quat_x+desplazamiento,vectors_e->quat_y+desplazamiento,vectors_e->quat_z+desplazamiento,vectors_e->quat_w+desplazamiento,vectors_e->energy.energy+desplazamiento,vectors_e->energy.n_conformation+desplazamiento,vectors_e->weights,vectors_e->f_params,flexibility_params->n_links,max_conformaciones,0,desplazamiento,flexibility_params->individual_bonds_VDW,flexibility_params->individual_bonds_ES,&kernels);
					//printf("Device %d con trozo %d\n",devices->id[th],i);					
				}
			}			
			tenergycpu_t += wtime() - tenergycpu;
			tordenarcpu = wtime();
			//exit(0);
			/*for (int k = 0;k<vectors_e->nconformations;k++)
			{
				if (vectors_e->energy.energy[k] < -788)
				{
					printf("k %d en %f m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f q_w %f\n",k,vectors_e->energy.energy[k],vectors_e->move_x[k],vectors_e->move_y[k],vectors_e->move_z[k],vectors_e->quat_x[k],vectors_e->quat_y[k],vectors_e->quat_z[k],vectors_e->quat_w[k]);
					for (int l=0;l<64;l++) printf("atom %f %f %f\n",vectors_e->conformations_x[k*ligando.nlig+l],vectors_e->conformations_y[k*ligando.nlig+l],vectors_e->conformations_z[k*ligando.nlig+l]);
				}
			}*/
			//exit(0);
			
			omp_set_num_threads (metaheuristic.Threads1Ini);
			#pragma omp parallel for			
			for (unsigned int i = 0; i < vectors_e->nconformations; i += metaheuristic.NEIIni)							
			    thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + metaheuristic.NEIIni), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());			
			//colocar_flex(ligando.nlig,param,vectors_e,metaheuristic);	
			#pragma omp parallel for
                        for (unsigned int i = 0; i < vectors_e->num_surface; i++)
                                colocar_flex_by_step (param,metaheuristic,ligando.nlig,vectors_e->conformations_x + (metaheuristic.NEIIni*i*ligando.nlig),vectors_e->conformations_y + (metaheuristic.NEIIni*i*ligando.nlig),vectors_e->conformations_z + (metaheuristic.NEIIni*i*ligando.nlig),vectors_e->move_x + (metaheuristic.NEIIni*i),vectors_e->move_y + (metaheuristic.NEIIni*i),vectors_e->move_z + (metaheuristic.NEIIni*i),vectors_e->quat_x + (metaheuristic.NEIIni*i),vectors_e->quat_y + (metaheuristic.NEIIni*i),vectors_e->quat_z + (metaheuristic.NEIIni*i),vectors_e->quat_w + (metaheuristic.NEIIni*i),vectors_e->energy.energy + (metaheuristic.NEIIni*i),vectors_e->energy.n_conformation + (metaheuristic.NEIIni*i),metaheuristic.NEIIni,i);
                        thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			tordenarcpu_t += wtime() - tordenarcpu;		
			//for (int k=7600;k<7630;k++) printf("k %f\n",vectors_e->energy.energy[k]);
			//for(int k=0;k<vectors_e->nconformations;k++)
			 //     printf("EN e-number %d, m_x %f, m_y %f, m_z %f, energy %f\n",k,vectors_e->move_x[k],vectors_e->move_y[k],vectors_e->move_z[k],vectors_e->energy.energy[k]);
			//exit(0);
				
			if (metaheuristic.PEMIni > 0)
			{
				tmejorainicpu = wtime(); //tomamos tiempos de mejora
				seleccionar_inicializar_flex(ligando.nlig,vectors_e,&vectors_selec,param,metaheuristic);						
				//for(int k=0;k<vectors_selec.nconformations;k++)
				//   printf("EN e-number %d, q_x %f, q_y %f, q_z %f, q_w %f\n",k,vectors_e->quat_x[k],vectors_e->quat_y[k],vectors_e->quat_z[k],vectors_e->quat_w[k]);
				//exit(0);
				if (metaheuristic.NEEImp > 0)
				{	
					if (param.montecarlo)
						montecarlo_flex_environment (proteina,ligando,param,&vectors_selec,metaheuristic,devices,flexibility_params,0,0);
					else
						gpu_mejorar_flex_environment (proteina,ligando,param,&vectors_selec,metaheuristic,devices,0,flexibility_params,0);						
				}
				else
				{
					if (param.montecarlo)
                                                montecarlo_flex(proteina,ligando,param,&vectors_selec,metaheuristic,devices,flexibility_params,0,0);
                                        else
                                                gpu_mejorar_flex (proteina,ligando,param,&vectors_selec,metaheuristic,devices,0,flexibility_params,0);
				}
				//incluir_gpu_mejorar_flex (ligando.nlig,&vectors_selec,vectors_e,param,metaheuristic);
				//for(int k=0;k<vectors_selec.nconformations;k++)
                              	//printf("EN e-number %d, m_x %f, m_y %f, m_z %f, energy %f\n",k,vectors_selec.move_x[k],vectors_selec.move_y[k],vectors_selec.move_z[k],vectors_selec.energy.energy[k]);
                        	//exit(0);
				stride_e = vectors_selec.nconformations / vectors_selec.num_surface;
                                stride_s = vectors_e->nconformations / vectors_e->num_surface;
                                #pragma omp parallel for
                                for (unsigned int i = 0; i < vectors_e->num_surface; i++)
                                        incluir_gpu_flex_by_step (ligando.nlig,vectors_selec.conformations_x + (stride_e*ligando.nlig*i), vectors_selec.conformations_y + (stride_e*ligando.nlig*i), vectors_selec.conformations_z + (stride_e*ligando.nlig*i), vectors_selec.move_x + (stride_e*i), vectors_selec.move_y + (stride_e*i), vectors_selec.move_z + (stride_e*i), vectors_selec.quat_x + (stride_e*i), vectors_selec.quat_y + (stride_e*i), vectors_selec.quat_z + (stride_e*i), vectors_selec.quat_w + (stride_e*i), vectors_selec.energy.energy + (stride_e*i), vectors_selec.energy.n_conformation + (stride_e*i), vectors_e->conformations_x + (stride_s*ligando.nlig*i), vectors_e->conformations_y + (stride_s*ligando.nlig*i), vectors_e->conformations_z + (stride_s*ligando.nlig*i), vectors_e->move_x + (stride_s*i),  vectors_e->move_y + (stride_s*i),  vectors_e->move_z + (stride_s*i),  vectors_e->quat_x + (stride_s*i),  vectors_e->quat_y + (stride_s*i),  vectors_e->quat_z + (stride_s*i),  vectors_e->quat_w + (stride_s*i),  vectors_e->energy.energy + (stride_s*i),  vectors_e->energy.n_conformation + (stride_s*i), stride_e, stride_s, i,param,metaheuristic);
                                thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
				tmejorainicpu_t += wtime() - tmejorainicpu;				
				
				//omp_set_num_threads (metaheuristic.Threads1Ini);
				//#pragma omp parallel for 
				//for (int i = 0; i < vectors_e->nconformations; i += metaheuristic.NEIIni)			
				//	thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + metaheuristic.NEIIni), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());
				//colocar_flex(ligando.nlig,param,vectors_e,metaheuristic);						
			}
			/*for (int k = 0;k<vectors_e->nconformations;k++)
                        {
                                if (vectors_e->energy.energy[k] < -788)
                                {
                                        printf("fuera k %d en %f m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f q_w %f\n",k,vectors_e->energy.energy[k],vectors_e->move_x[k],vectors_e->move_y[k],vectors_e->move_z[k],vectors_e->quat_x[k],vectors_e->quat_y[k],vectors_e->quat_z[k],vectors_e->quat_w[k]);
                                        for (int l=0;l<64;l++) printf("atom %f %f %f\n",vectors_e->conformations_x[k*ligando.nlig+l],vectors_e->conformations_y[k*ligando.nlig+l],vectors_e->conformations_z[k*ligando.nlig+l]);
                                }
                        }*/              	
                        //for(int k=0;k<vectors_e->nconformations;k++)
                        //      printf("EN e-number %d, m_x %f, m_y %f, m_z %f, energy %f\n",k,vectors_e->move_x[k],vectors_e->move_y[k],vectors_e->move_z[k],vectors_e->energy.energy[k]);
                        //exit(0);                	
			seleccionar_NEFIni_flex(ligando.nlig,vectors_e,vectors_s->conformations_x,vectors_s->conformations_y,vectors_s->conformations_z,vectors_s,metaheuristic,param);
			printf("\nFases INICILIZAR:\n");
			printf("\tTiempo Generacion posiciones: %lf seg\n",tgenerarposi_t);				
			printf("\tTiempo Funcion de scoring: %lf seg\n",tenergycpu_t);	
			printf("\tTiempo Ordenar: %lf seg\n",tordenarcpu_t);
			if (metaheuristic.PEMIni > 0) printf("\tTiempo Mejorar: %lf seg\n",tmejorainicpu_t);	
			printf("\tTiempo Colocar: %lf seg\n",tcolocarcpu_t);	
			//exit(0);
			/*for (int k = 0;k<vectors_s->nconformations;k++)
                        {
                                if (vectors_s->energy.energy[k] < -772)
                                {
                                        printf("k %d en %f m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f q_w %f\n",k,vectors_s->energy.energy[k],vectors_s->move_x[k],vectors_s->move_y[k],vectors_s->move_z[k],vectors_s->quat_x[k],vectors_s->quat_y[k],vectors_s->quat_z[k],vectors_s->quat_w[k]);
                                        for (int l=0;l<64;l++) printf("atom %f %f %f\n",vectors_s->conformations_x[k*ligando.nlig+l],vectors_s->conformations_y[k*ligando.nlig+l],vectors_s->conformations_z[k*ligando.nlig+l]);
                                }
                        }*/	
			break;			
			
		case 1:			
		case 2:
			switch (param.mode) {
				case 1:
					th1 = 1;
					break;
				case 0:
				case 3:
				case 2:
					th1 = metaheuristic.Threads1Ini;
					break;
			}	
			//OPEN-MP SELECTION
			vectors_e->nconformations = vectors_e->num_surface * metaheuristic.NEIIni;
			internal_energy = (type_data *)calloc(sizeof(type_data),vectors_e->nconformations);
			flexibility_conformations = (flexibility_data_t *)malloc(sizeof(flexibility_data_t)*vectors_e->nconformations);
			vectors_e->conformations_x = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_e->nconformations);
			vectors_e->conformations_y = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_e->nconformations);
			vectors_e->conformations_z = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_e->nconformations);						
			vectors_e->move_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->move_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->move_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_e->nconformations);
			vectors_e->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			
			fill_conformations (vectors_e->nconformations,param,metaheuristic,ligando,vectors_e);
			tgenerarposi = wtime();//tomamos tiempos	
			generate_positions_cpp_flex (ligando.nlig,param,vectors_e,metaheuristic,flexibility_params,flexibility_conformations);
			tgenerarposi_t += wtime() - tgenerarposi;
			replace_for_dqn(dqn_data,vectors_e,metaheuristic);
			
			tenergycpu = wtime();//tomamos tiempos para calculo			
			ForcesCpuSolver_flex(param,ligando,proteina,vectors_e->conformations_x,vectors_e->conformations_y,vectors_e->conformations_z,vectors_e->move_x,vectors_e->move_y,vectors_e->move_z,vectors_e->quat_x,vectors_e->quat_y,vectors_e->quat_z,vectors_e->quat_w,vectors_e->energy.energy,vectors_e->energy.n_conformation,vectors_e->weights,vectors_e->f_params,vectors_e->nconformations,metaheuristic,flexibility_params->n_links);
			//forces_CPU_flex (param,proteina.atoms,ligando.atoms,ligando.nlig,proteina.rec_x,proteina.rec_y,proteina.rec_z,vectors_e->conformations_x,vectors_e->conformations_y,vectors_e->conformations_z,vectors_e->move_x,vectors_e->move_y,vectors_e->move_z,vectors_e->quat_x,vectors_e->quat_y,vectors_e->quat_z,vectors_e->quat_w,proteina.rectype,ligando.ligtype,ligando.ql,proteina.qr,proteina.bonds,ligando.bonds,vectors_e->energy.energy,vectors_e->energy.n_conformation,vectors_e->weights,vectors_e->f_params,vectors_e->nconformations,metaheuristic,flexibility_params->n_links);	
			energy_internal_conformations_cpu (vectors_e->nconformations,param,metaheuristic,vectors_e->conformations_x,vectors_e->conformations_y,vectors_e->conformations_z,&ligando,vectors_e->weights,vectors_e->f_params,internal_energy,flexibility_params->individual_bonds_VDW,flexibility_params->individual_bonds_ES);
			energy_total (vectors_e->nconformations,param,metaheuristic,vectors_e->energy.energy,internal_energy);		
			
			tenergycpu_t += wtime() - tenergycpu;
			omp_set_num_threads(th1);
			#pragma omp parallel for
			for (unsigned int i = 0; i < vectors_e->nconformations; i += metaheuristic.NEIIni)							
			    thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + metaheuristic.NEIIni), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());			
			//colocar_flex(ligando.nlig,param,vectors_e,metaheuristic);
			#pragma omp parallel for
                        for (unsigned int i = 0; i < vectors_e->num_surface; i++)
                                colocar_flex_by_step (param,metaheuristic,ligando.nlig,vectors_e->conformations_x + (metaheuristic.NEIIni*i*ligando.nlig),vectors_e->conformations_y + (metaheuristic.NEIIni*i*ligando.nlig),vectors_e->conformations_z + (metaheuristic.NEIIni*i*ligando.nlig),vectors_e->move_x + (metaheuristic.NEIIni*i),vectors_e->move_y + (metaheuristic.NEIIni*i),vectors_e->move_z + (metaheuristic.NEIIni*i),vectors_e->quat_x + (metaheuristic.NEIIni*i),vectors_e->quat_y + (metaheuristic.NEIIni*i),vectors_e->quat_z + (metaheuristic.NEIIni*i),vectors_e->quat_w + (metaheuristic.NEIIni*i),vectors_e->energy.energy + (metaheuristic.NEIIni*i),vectors_e->energy.n_conformation + (metaheuristic.NEIIni*i),metaheuristic.NEIIni,i);
                        thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			
			//for(int k=0;k<64;k++)
			//	printf("EN e-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",k,vectors_e->conformations_x[k],vectors_e->conformations_y[k],vectors_e->conformations_z[k],vectors_e->move_x[k/ligando.nlig],vectors_e->move_y[k/ligando.nlig],vectors_e->move_z[k/ligando.nlig],vectors_e->energy.energy[k/ligando.nlig]);
			//exit(0);
			
			
			if (metaheuristic.PEMIni > 0)
			{		
				//printf("MEJORANDO %d\n");	
				tmejorainicpu = wtime(); //tomamos tiempos de mejora
				seleccionar_inicializar_flex(ligando.nlig,vectors_e,&vectors_selec,param,metaheuristic);
				if (metaheuristic.NEEImp > 0)
				{
					 if (param.montecarlo)
                                                montecarlo_cpp_flex_environment(proteina,ligando,param,vectors_selec.num_surface, vectors_selec.weights, vectors_selec.f_params, vectors_selec.conformations_x, vectors_selec.conformations_y, vectors_selec.conformations_z, vectors_selec.move_x, vectors_selec.move_y, vectors_selec.move_z, vectors_selec.quat_x, vectors_selec.quat_y, vectors_selec.quat_z, vectors_selec.quat_w, vectors_selec.energy.energy, vectors_selec.energy.n_conformation, vectors_selec.nconformations,metaheuristic,flexibility_params,flexibility_conformations,0);
                                        else
                                                mejorar_flex_environment(proteina,ligando,param,vectors_selec.weights, vectors_selec.f_params, vectors_selec.conformations_x, vectors_selec.conformations_y, vectors_selec.conformations_z, vectors_selec.move_x, vectors_selec.move_y, vectors_selec.move_z, vectors_selec.quat_x, vectors_selec.quat_y, vectors_selec.quat_z, vectors_selec.quat_w, vectors_selec.energy.energy, vectors_selec.energy.n_conformation, vectors_selec.num_surface, vectors_selec.nconformations, metaheuristic, flexibility_params, flexibility_conformations,0);
				}
				else
				{	
					if (param.montecarlo)						
						montecarlo_cpp_flex(proteina,ligando,param,vectors_selec.num_surface, vectors_selec.weights, vectors_selec.f_params, vectors_selec.conformations_x, vectors_selec.conformations_y, vectors_selec.conformations_z, vectors_selec.move_x, vectors_selec.move_y, vectors_selec.move_z, vectors_selec.quat_x, vectors_selec.quat_y, vectors_selec.quat_z, vectors_selec.quat_w, vectors_selec.energy.energy, vectors_selec.energy.n_conformation, vectors_selec.nconformations,metaheuristic,flexibility_params,flexibility_conformations,0);						
					else
						mejorar_flex(proteina,ligando,param,vectors_selec.weights, vectors_selec.f_params, vectors_selec.conformations_x, vectors_selec.conformations_y, vectors_selec.conformations_z, vectors_selec.move_x, vectors_selec.move_y, vectors_selec.move_z, vectors_selec.quat_x, vectors_selec.quat_y, vectors_selec.quat_z, vectors_selec.quat_w, vectors_selec.energy.energy, vectors_selec.energy.n_conformation, vectors_selec.num_surface, vectors_selec.nconformations, metaheuristic, flexibility_params, flexibility_conformations,0);           									
				}
				//incluir_gpu_mejorar_flex (ligando.nlig,&vectors_selec,vectors_e,param,metaheuristic);
				stride_e = vectors_selec.nconformations / vectors_selec.num_surface;
                                stride_s = vectors_e->nconformations / vectors_e->num_surface;
                                #pragma omp parallel for
                                for (unsigned int i = 0; i < vectors_e->num_surface; i++)
                                        incluir_gpu_flex_by_step (ligando.nlig,vectors_selec.conformations_x + (stride_e*ligando.nlig*i), vectors_selec.conformations_y + (stride_e*ligando.nlig*i), vectors_selec.conformations_z + (stride_e*ligando.nlig*i), vectors_selec.move_x + (stride_e*i), vectors_selec.move_y + (stride_e*i), vectors_selec.move_z + (stride_e*i), vectors_selec.quat_x + (stride_e*i), vectors_selec.quat_y + (stride_e*i), vectors_selec.quat_z + (stride_e*i), vectors_selec.quat_w + (stride_e*i), vectors_selec.energy.energy + (stride_e*i), vectors_selec.energy.n_conformation + (stride_e*i), vectors_e->conformations_x + (stride_s*ligando.nlig*i), vectors_e->conformations_y + (stride_s*ligando.nlig*i), vectors_e->conformations_z + (stride_s*ligando.nlig*i), vectors_e->move_x + (stride_s*i),  vectors_e->move_y + (stride_s*i),  vectors_e->move_z + (stride_s*i),  vectors_e->quat_x + (stride_s*i),  vectors_e->quat_y + (stride_s*i),  vectors_e->quat_z + (stride_s*i),  vectors_e->quat_w + (stride_s*i),  vectors_e->energy.energy + (stride_s*i),  vectors_e->energy.n_conformation + (stride_s*i), stride_e, stride_s, i,param,metaheuristic);
                                thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
				//omp_set_num_threads(th1);
				//#pragma omp parallel for
				//for (int i = 0; i < vectors_e->nconformations; i += metaheuristic.NEIIni)			
				//	thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + metaheuristic.NEIIni), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());										
				//colocar_flex(ligando.nlig,param,vectors_e,metaheuristic);
				tmejorainicpu_t += wtime() - tmejorainicpu;							
					
			}
						
			seleccionar_NEFIni_flex(ligando.nlig,vectors_e,vectors_s->conformations_x,vectors_s->conformations_y,vectors_s->conformations_z,vectors_s,metaheuristic,param);
			printf("\nFases INICILIZAR:\n");
			printf("\tTiempo Generacion posiciones: %lf seg\n",tgenerarposi_t);				
			printf("\tTiempo funcion de scoring: %lf seg\n",tenergycpu_t);	
			printf("\tTiempo Ordenar: %lf seg\n",tordenarcpu_t);
			if (metaheuristic.PEMIni > 0) printf("\tTiempo Mejorar: %lf seg\n",tmejorainicpu_t);	
			printf("\tTiempo Colocar: %lf seg\n",tcolocarcpu_t);				
			free(flexibility_conformations);
			free(internal_energy);
			
			break;
		case 3:
			//MULTIGPU SELECTION
			//int n_devices,k,*device_current_m;
			/*cudaDeviceProp *prop_m;
			device_current_m = new int;
			prop_m = new cudaDeviceProp();
			cudaSetDevice(param.ngpu);
			cudaGetDevice(device_current_m);
			cudaGetDeviceProperties (prop_m, *device_current_m);
			printf("\n%s\n",prop_m->name);*/
			//NEItemp	= metaheuristic.NEIIni;
			//NEItemp = getPadding(NEItemp,16);
			//metaheuristic.NEIIni += NEItemp;
			NEI_selec_temp = (unsigned int)ceil((metaheuristic.NEIIni * metaheuristic.PEMIni) / 100);
			metaheuristic.NEIIni_selec = NEI_selec_temp;// + getPadding(NEI_selec_temp,16);

			vectors_e->nconformations = vectors_e->num_surface * metaheuristic.NEIIni;
			vectors_e->conformations_x = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_e->nconformations);
			vectors_e->conformations_y = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_e->nconformations);
			vectors_e->conformations_z = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_e->nconformations);			
			
			vectors_e->move_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->move_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->move_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_e->nconformations);
			vectors_e->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
									
			fill_conformations (vectors_e->nconformations,param,metaheuristic,ligando,vectors_e);
			tgenerarposi = wtime();//tomamos tiempos
			generate_positions_flex (ligando.atoms,ligando.nlig,param,vectors_e,metaheuristic,devices,0,flexibility_params,0);
			tgenerarposi_t += wtime() - tgenerarposi;
			replace_for_dqn(dqn_data,vectors_e,metaheuristic);
						
			tenergycpu = wtime();//tomamos tiempos para calculo L-J
			multigpuSolver_flex(ligando,proteina,vectors_e->num_surface,vectors_e->conformations_x,vectors_e->conformations_y,vectors_e->conformations_z, vectors_e->move_x,vectors_e->move_y,vectors_e->move_z,vectors_e->quat_x,vectors_e->quat_y,vectors_e->quat_z,vectors_e->quat_w,vectors_e->energy,vectors_e->weights,vectors_e->f_params,vectors_e->nconformations,param,metaheuristic,devices,flexibility_params);
			//exit(0);
			tenergycpu_t += wtime() - tenergycpu;	
			//cudaSetDevice(devices->id[0]);	
			tordenarcpu = wtime();
			omp_set_num_threads (metaheuristic.Threads1Ini);
			#pragma omp parallel for			
			for (unsigned int i = 0; i < vectors_e->nconformations; i += metaheuristic.NEIIni)							
				thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + metaheuristic.NEIIni), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());				
			tordenarcpu_t += wtime() - tordenarcpu;
			//colocar_flex(ligando.nlig,param,vectors_e,metaheuristic);	
			#pragma omp parallel for
                        for (unsigned int i = 0; i < vectors_e->num_surface; i++)
                                colocar_flex_by_step (param,metaheuristic,ligando.nlig,vectors_e->conformations_x + (metaheuristic.NEIIni*i*ligando.nlig),vectors_e->conformations_y + (metaheuristic.NEIIni*i*ligando.nlig),vectors_e->conformations_z + (metaheuristic.NEIIni*i*ligando.nlig),vectors_e->move_x + (metaheuristic.NEIIni*i),vectors_e->move_y + (metaheuristic.NEIIni*i),vectors_e->move_z + (metaheuristic.NEIIni*i),vectors_e->quat_x + (metaheuristic.NEIIni*i),vectors_e->quat_y + (metaheuristic.NEIIni*i),vectors_e->quat_z + (metaheuristic.NEIIni*i),vectors_e->quat_w + (metaheuristic.NEIIni*i),vectors_e->energy.energy + (metaheuristic.NEIIni*i),vectors_e->energy.n_conformation + (metaheuristic.NEIIni*i),metaheuristic.NEIIni,i);
                        thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);

			if (metaheuristic.PEMIni > 0)
			{
				tmejorainicpu = wtime(); //tomamos tiempos de mejora
				seleccionar_inicializar_multigpu_flex(ligando.nlig,vectors_e,&vectors_selec,param,metaheuristic);
				//exit(0);
				if (metaheuristic.NEEImp > 0)
				{
					multigpuSolver_mejora_flex_environment (ligando,proteina,vectors_selec.num_surface,vectors_selec.conformations_x,vectors_selec.conformations_y,vectors_selec.conformations_z,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z,vectors_selec.quat_x,vectors_selec.quat_y,vectors_selec.quat_z,vectors_selec.quat_w,vectors_selec.energy.energy,vectors_selec.energy.n_conformation,vectors_selec.weights, vectors_selec.f_params, vectors_selec.nconformations,param,metaheuristic,devices,0,flexibility_params);
				}
				else
				{	
					multigpuSolver_mejora_flex (ligando,proteina,vectors_selec.num_surface,vectors_selec.conformations_x,vectors_selec.conformations_y,vectors_selec.conformations_z,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z,vectors_selec.quat_x,vectors_selec.quat_y,vectors_selec.quat_z,vectors_selec.quat_w,vectors_selec.energy.energy,vectors_selec.energy.n_conformation,vectors_selec.weights, vectors_selec.f_params, vectors_selec.nconformations,param,metaheuristic,devices,0,flexibility_params);				
				}
				
				//incluir_gpu_mejorar_flex (ligando.nlig,&vectors_selec,vectors_e,param,metaheuristic);			
				stride_e = vectors_selec.nconformations / vectors_selec.num_surface;
                                stride_s = vectors_e->nconformations / vectors_e->num_surface;
                                #pragma omp parallel for
                                for (unsigned int i = 0; i < vectors_e->num_surface; i++)
                                        incluir_gpu_flex_by_step (ligando.nlig,vectors_selec.conformations_x + (stride_e*ligando.nlig*i), vectors_selec.conformations_y + (stride_e*ligando.nlig*i), vectors_selec.conformations_z + (stride_e*ligando.nlig*i), vectors_selec.move_x + (stride_e*i), vectors_selec.move_y + (stride_e*i), vectors_selec.move_z + (stride_e*i), vectors_selec.quat_x + (stride_e*i), vectors_selec.quat_y + (stride_e*i), vectors_selec.quat_z + (stride_e*i), vectors_selec.quat_w + (stride_e*i), vectors_selec.energy.energy + (stride_e*i), vectors_selec.energy.n_conformation + (stride_e*i), vectors_e->conformations_x + (stride_s*ligando.nlig*i), vectors_e->conformations_y + (stride_s*ligando.nlig*i), vectors_e->conformations_z + (stride_s*ligando.nlig*i), vectors_e->move_x + (stride_s*i),  vectors_e->move_y + (stride_s*i),  vectors_e->move_z + (stride_s*i),  vectors_e->quat_x + (stride_s*i),  vectors_e->quat_y + (stride_s*i),  vectors_e->quat_z + (stride_s*i),  vectors_e->quat_w + (stride_s*i),  vectors_e->energy.energy + (stride_s*i),  vectors_e->energy.n_conformation + (stride_s*i), stride_e, stride_s, i,param,metaheuristic);
                                thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
				tmejorainicpu_t += wtime() - tmejorainicpu;				
				
				//omp_set_num_threads (metaheuristic.Threads1Ini);
				//#pragma omp parallel for 
				//for (int i = 0; i < vectors_e->nconformations; i += metaheuristic.NEIIni)			
				//	thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + metaheuristic.NEIIni), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());
				//colocar_flex(ligando.nlig,param,vectors_e,metaheuristic);						
			}
			
			seleccionar_NEFIni_flex(ligando.nlig,vectors_e,vectors_s->conformations_x,vectors_s->conformations_y,vectors_s->conformations_z,vectors_s,metaheuristic,param);
			printf("\nFases INICILIZAR:\n");
			printf("\tTiempo Generacion posiciones: %lf seg\n",tgenerarposi_t);				
			printf("\tTiempo funcion de scoring: %lf seg\n",tenergycpu_t);	
			printf("\tTiempo Ordenar: %lf seg\n",tordenarcpu_t);
			if (metaheuristic.PEMIni > 0) printf("\tTiempo Mejorar: %lf seg\n",tmejorainicpu_t);	
			printf("\tTiempo Colocar: %lf seg\n",tcolocarcpu_t);	
			break;		
	}
	
	if (metaheuristic.PEMIni > 0)
	{	
		free(vectors_selec.conformations_x);
		free(vectors_selec.conformations_y);
		free(vectors_selec.conformations_z);
		free(vectors_selec.move_x);
		free(vectors_selec.move_y);
		free(vectors_selec.move_z);
		free(vectors_selec.quat_x);
		free(vectors_selec.quat_y);
		free(vectors_selec.quat_z);
		free(vectors_selec.quat_w);
		free(vectors_selec.energy.energy);
		free(vectors_selec.energy.n_conformation);
		free(vectors_selec.weights);
	}
}


