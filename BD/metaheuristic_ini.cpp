#include "energy_common.h"
#include "energy_cuda.h"
#include "energy_positions.h"
#include "energy_GpuSolver.h"
#include "energy_CpuSolver.h"
#include "energy_montecarlo.h"
#include "metaheuristic_ini.h"
#include "vector_types.h"
#include "montecarlo.h"
#include "metaheuristic_inc.h"
#include "metaheuristic_mgpu.h"
#include "wtime.h"
#include <thrust/sequence.h>
#include <thrust/sort.h>
#include <thrust/functional.h>
#include <omp.h>

extern void mejorar_environment (struct receptor_t proteina, struct ligand_t ligando, struct param_t param,  unsigned int num_surface, type_data *weights, struct force_field_param_t *f_params, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int nconformations, struct metaheuristic_t metaheuristic, unsigned int opt)
{

	struct vectors_t vectors_mejora;
	unsigned int steps,moveType=MOVE,bucle,fase;	
	double tenergymejora;
	double tenergymejora_t;
	unsigned int tam = nconformations / num_surface;
	vectors_mejora.num_surface = num_surface;
	vectors_mejora.nconformations = nconformations * metaheuristic.NEEImp;

	vectors_mejora.move_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.move_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.move_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations);
	vectors_mejora.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);	
	vectors_mejora.weights = (type_data *)malloc(sizeof(type_data)*SCORING_TERMS);
 	//printf("Memoria para mejora creada....\n");
	
	//memcpy(vectors_mejora.move_x, move_x, vectors_mejora.nconformations * sizeof(type_data));  
	//memcpy(vectors_mejora.move_y, move_y, vectors_mejora.nconformations * sizeof(type_data));  
	//memcpy(vectors_mejora.move_z, move_z, vectors_mejora.nconformations * sizeof(type_data));  
	//memcpy(vectors_mejora.quat_x, quat_x, vectors_mejora.nconformations * sizeof(type_data));  
	//memcpy(vectors_mejora.quat_y, quat_y, vectors_mejora.nconformations * sizeof(type_data));  
	//memcpy(vectors_mejora.quat_z, quat_z, vectors_mejora.nconformations * sizeof(type_data));  
	//memcpy(vectors_mejora.quat_w, quat_w, vectors_mejora.nconformations * sizeof(type_data));  
	//emcpy(vectors_mejora.energy.n_conformation, nconfs, vectors_mejora.nconformations * sizeof(int));
	memcpy(vectors_mejora.f_params, f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_mejora.weights, weights,SCORING_TERMS * sizeof(type_data));	
	memset(vectors_mejora.energy.energy,0,vectors_mejora.nconformations * sizeof(type_data));
	thrust::sequence(vectors_mejora.energy.n_conformation,vectors_mejora.energy.n_conformation + vectors_mejora.nconformations);
				
	fill_conformations_environment (move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,nconformations,param,metaheuristic); 	

	steps = 0;
	if (opt == 0) 
	{
		bucle = metaheuristic.IMEIni;
	}
	if (opt == 1)
	{	
		bucle = metaheuristic.IMEImp;	
	}
	if (opt == 2)
	{
		bucle = metaheuristic.IMEMUCom;
	}
	moveType = MOVE;
	//printf("Voy a mejorar....\n");
while (steps < bucle){		
		if (moveType == MOVE){
			move_mejora(vectors_mejora.num_surface,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,param,tam,metaheuristic);
			moveType = ROTATE;
			//printf("He movido\n");
		}
		else {
			rotate(vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,param,vectors_mejora.nconformations,metaheuristic);
			moveType = MOVE;
		}		
		//printf("Voy a calcular la energia...\n");
		tenergymejora = wtime();
		ForcesCpuSolver(param,ligando,proteina,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,vectors_mejora.energy.energy,vectors_mejora.energy.n_conformation,vectors_mejora.weights,vectors_mejora.f_params,vectors_mejora.nconformations,metaheuristic);		
		tenergymejora_t += wtime() - tenergymejora;
		//omp_set_num_threads (metaheuristic.Threads1Ini);
		//#pragma omp parallel for			
		//for (unsigned int i = 0; i < vectors_mejora.nconformations; i += metaheuristic.NEEImp)							    	thrust::stable_sort_by_key((vectors_mejora.energy.energy + i),(vectors_mejora.energy.energy + i + metaheuristic.NEEImp), (vectors_mejora.energy.n_conformation + i), thrust::less<type_data>());
		/*#pragma omp parallel for
               	for (unsigned int i = 0; i < vectors_mejora.nconformations*metaheuristic.NEEImp; i+=metaheuristic.NEEImp)
                	colocar_by_step (param,metaheuristic,ligando.nlig,vectors_mejora.move_x + (metaheuristic.NEIIni*i),vectors_e->move_y + (metaheuristic.NEIIni*i),vectors_e->move_z + (metaheuristic.NEIIni*i),vectors_e->quat_x + (metaheuristic.NEIIni*i),vectors_e->quat_y + (metaheuristic.NEIIni*i),vectors_e->quat_z + (metaheuristic.NEIIni*i),vectors_e->quat_w + (metaheuristic.NEIIni*i),vectors_e->energy.energy + (metaheuristic.NEIIni*i),vectors_e->energy.n_conformation + (metaheuristic.NEIIni*i),metaheuristic.NEIIni,i);
                thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);*/
			
		incluir_mejorar_inicializar_environment(vectors_mejora, move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,nconformations,metaheuristic, param);
		fill_conformations_environment (move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,nconformations,param,metaheuristic);	
		steps++;	
	}
	free(vectors_mejora.move_x);
	free(vectors_mejora.move_y);
	free(vectors_mejora.move_z);
	free(vectors_mejora.quat_x);
	free(vectors_mejora.quat_y);
	free(vectors_mejora.quat_z);
	free(vectors_mejora.quat_w);
	free(vectors_mejora.energy.energy);
	free(vectors_mejora.energy.n_conformation);
	free(vectors_mejora.weights);
}
extern void incluir_gpu_by_step (unsigned int nlig, type_data *e_move_x, type_data *e_move_y, type_data *e_move_z, type_data *e_quat_x, type_data *e_quat_y, type_data *e_quat_z, type_data *e_quat_w, type_data *e_energy, unsigned int *e_nconfs, type_data *s_move_x, type_data *s_move_y, type_data *s_move_z, type_data *s_quat_x, type_data *s_quat_y, type_data *s_quat_z, type_data *s_quat_w, type_data *s_energy, unsigned int *s_nconfs, unsigned int stride_e, unsigned int stride_s, unsigned int pos,struct param_t param, struct metaheuristic_t metaheuristic)
{

        struct vectors_t *vtmp = (struct vectors_t *)malloc(sizeof(struct vectors_t));
        vtmp->nconformations = stride_e + stride_s;
        vtmp->move_x = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->move_y = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->move_z = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->quat_x = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->quat_y = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->quat_z = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->quat_w = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->energy.energy = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vtmp->nconformations);
	thrust::sequence(vtmp->energy.n_conformation,vtmp->energy.n_conformation + vtmp->nconformations);

        thrust::stable_sort_by_key(e_energy,e_energy + stride_e, e_nconfs, thrust::less<type_data>());

        colocar_by_step (param,metaheuristic,nlig,e_move_x,e_move_y,e_move_z,e_quat_x,e_quat_y,e_quat_z,e_quat_w,e_energy,e_nconfs,stride_e,pos);

        memcpy(vtmp->move_x, e_move_x, stride_e * sizeof(type_data));
        memcpy(vtmp->move_x + stride_e, s_move_x, stride_s * sizeof(type_data));

        memcpy(vtmp->move_y, e_move_y, stride_e * sizeof(type_data));
        memcpy(vtmp->move_y + stride_e, s_move_y, stride_s * sizeof(type_data));

        memcpy(vtmp->move_z, e_move_z, stride_e * sizeof(type_data));
        memcpy(vtmp->move_z + stride_e, s_move_z, stride_s * sizeof(type_data));

        memcpy(vtmp->quat_x, e_quat_x, stride_e * sizeof(type_data));
        memcpy(vtmp->quat_x + stride_e, s_quat_x, stride_s * sizeof(type_data));

        memcpy(vtmp->quat_y, e_quat_y, stride_e * sizeof(type_data));
        memcpy(vtmp->quat_y + stride_e, s_quat_y, stride_s * sizeof(type_data));

        memcpy(vtmp->quat_z, e_quat_z, stride_e * sizeof(type_data));
        memcpy(vtmp->quat_z + stride_e, s_quat_z, stride_s * sizeof(type_data));

        memcpy(vtmp->quat_w, e_quat_w, stride_e * sizeof(type_data));
        memcpy(vtmp->quat_w + stride_e, s_quat_w, stride_s * sizeof(type_data));

        memcpy(vtmp->energy.energy, e_energy, stride_e * sizeof(type_data));
        memcpy(vtmp->energy.energy + stride_e, s_energy, stride_s * sizeof(type_data));

        thrust::stable_sort_by_key(vtmp->energy.energy,vtmp->energy.energy + vtmp->nconformations, vtmp->energy.n_conformation, thrust::less<type_data>());
        colocar_by_step (param,metaheuristic,nlig,vtmp->move_x,vtmp->move_y,vtmp->move_z,vtmp->quat_x,vtmp->quat_y,vtmp->quat_z,vtmp->quat_w,vtmp->energy.energy,vtmp->energy.n_conformation,vtmp->nconformations,0);

        memcpy(s_move_x, vtmp->move_x, stride_s * sizeof(type_data));
        memcpy(s_move_y, vtmp->move_y, stride_s * sizeof(type_data));
        memcpy(s_move_z, vtmp->move_z, stride_s * sizeof(type_data));

        memcpy(s_quat_x, vtmp->quat_x, stride_s * sizeof(type_data));
        memcpy(s_quat_y, vtmp->quat_y, stride_s * sizeof(type_data));
        memcpy(s_quat_z, vtmp->quat_z, stride_s * sizeof(type_data));
        memcpy(s_quat_w, vtmp->quat_w, stride_s * sizeof(type_data));

        memcpy(s_energy, vtmp->energy.energy, stride_s * sizeof(type_data));

        free(vtmp->move_x);
        free(vtmp->move_y);
        free(vtmp->move_z);
        free(vtmp->quat_x);
        free(vtmp->quat_y);
        free(vtmp->quat_z);
        free(vtmp->quat_w);
        free(vtmp->energy.energy);
        free(vtmp->energy.n_conformation);
}


extern void seleccionar_NEFIni (struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param)
{	
	unsigned int stride_s, stride_e;
	unsigned int th1;
	stride_s = metaheuristic.NEFMIni_selec;
	stride_e = metaheuristic.NEIIni - metaheuristic.NEFPIni_selec;	
	
	vectors_s->num_surface = vectors_e->num_surface;
	vectors_s->nconformations = vectors_e->num_surface * metaheuristic.NEFIni;

	vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nconformations);
	vectors_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->surface_x = (type_data *)malloc(sizeof(type_data)*vectors_s->num_surface);
	vectors_s->surface_y = (type_data *)malloc(sizeof(type_data)*vectors_s->num_surface);
	vectors_s->surface_z = (type_data *)malloc(sizeof(type_data)*vectors_s->num_surface);
	vectors_s->weights = (type_data *)malloc(sizeof(type_data)*SCORING_TERMS);

	memcpy(vectors_s->surface_x,vectors_e->surface_x,sizeof(type_data)*vectors_s->num_surface);
	memcpy(vectors_s->surface_y,vectors_e->surface_y,sizeof(type_data)*vectors_s->num_surface);
	memcpy(vectors_s->surface_z,vectors_e->surface_z,sizeof(type_data)*vectors_s->num_surface);
	
	//memcpy(vectors_s->vdw_params, vectors_e->vdw_params,MAXTYPES * sizeof(struct vdw_param_t));
	//memcpy(vectors_s->sasa_params, vectors_e->sasa_params,MAXTYPES * sizeof(struct sasa_param_t));
	//memcpy(vectors_s->hbond_params, vectors_e->hbond_params,MAXTYPES * sizeof(struct hbond_param_t));
	memcpy(vectors_s->f_params, vectors_e->f_params,MAXTYPES * sizeof(struct force_field_param_t));
	memcpy(vectors_s->weights, vectors_e->weights,SCORING_TERMS * sizeof(type_data));

	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 0:
		case 3:
		case 2:
			th1 = metaheuristic.Threads1Sel;
			break;
	}			
	
	omp_set_num_threads (th1);
	#pragma omp parallel for 	
	for (unsigned int i = 0;i < vectors_s->num_surface;i++)
	{				
		memcpy((vectors_s->move_x + i*metaheuristic.NEFIni), (vectors_e->move_x + i*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * sizeof(type_data));  	
		memcpy((vectors_s->move_y + i*metaheuristic.NEFIni), (vectors_e->move_y + i*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * sizeof(type_data)); 
		memcpy((vectors_s->move_z + i*metaheuristic.NEFIni), (vectors_e->move_z + i*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * sizeof(type_data));
		memcpy((vectors_s->quat_x + i*metaheuristic.NEFIni), (vectors_e->quat_x + i*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * sizeof(type_data));
		memcpy((vectors_s->quat_y + i*metaheuristic.NEFIni), (vectors_e->quat_y + i*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * sizeof(type_data));
		memcpy((vectors_s->quat_z + i*metaheuristic.NEFIni), (vectors_e->quat_z + i*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * sizeof(type_data));
		memcpy((vectors_s->quat_w + i*metaheuristic.NEFIni), (vectors_e->quat_w + i*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * sizeof(type_data));
		memcpy((vectors_s->energy.energy + i*metaheuristic.NEFIni), (vectors_e->energy.energy + i*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * sizeof(type_data));
		//memcpy((vectors_s->energy.n_conformation + i*metaheuristic.NEFIni), (vectors_e->energy.n_conformation + i*metaheuristic.NEIIni),metaheuristic.NEFMIni_selec * sizeof(int));				
						
		memcpy((vectors_s->move_x + i*metaheuristic.NEFIni + stride_s), (vectors_e->move_x + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(type_data));  	
		memcpy((vectors_s->move_y + i*metaheuristic.NEFIni + stride_s), (vectors_e->move_y + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(type_data)); 
		memcpy((vectors_s->move_z + i*metaheuristic.NEFIni + stride_s), (vectors_e->move_z + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(type_data));
		memcpy((vectors_s->quat_x + i*metaheuristic.NEFIni + stride_s), (vectors_e->quat_x + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(type_data));
		memcpy((vectors_s->quat_y + i*metaheuristic.NEFIni + stride_s), (vectors_e->quat_y + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(type_data));
		memcpy((vectors_s->quat_z + i*metaheuristic.NEFIni + stride_s), (vectors_e->quat_z + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(type_data));
		memcpy((vectors_s->quat_w + i*metaheuristic.NEFIni + stride_s), (vectors_e->quat_w + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(type_data));
		memcpy((vectors_s->energy.energy + i*metaheuristic.NEFIni + stride_s), (vectors_e->energy.energy + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(type_data));
	}
}

extern void incluir_mejorar_inicializar (struct vectors_t vectors_e,  type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int nconformations, struct metaheuristic_t metaheuristic, struct param_t param)
{	
	unsigned int id_conformation,th1;
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 0:
		case 3:
		case 2:
			th1 = metaheuristic.Threads1Inc;
			break;
	}		
	omp_set_num_threads (th1);
	#pragma omp parallel for private (id_conformation) 			
	for (unsigned int i = 0;i < vectors_e.nconformations;i++)
	{
		id_conformation = vectors_e.energy.n_conformation[i];
		if (vectors_e.energy.energy[i] < energy[id_conformation])
		{
			move_x[id_conformation] = vectors_e.move_x[id_conformation];
			move_y[id_conformation] = vectors_e.move_y[id_conformation];
			move_z[id_conformation] = vectors_e.move_z[id_conformation];
			quat_x[id_conformation] = vectors_e.quat_x[id_conformation];
			quat_y[id_conformation] = vectors_e.quat_y[id_conformation];
			quat_z[id_conformation] = vectors_e.quat_z[id_conformation];
			quat_w[id_conformation] = vectors_e.quat_w[id_conformation];
			energy[i] = vectors_e.energy.energy[i];
		}
		else
		{
			vectors_e.move_x[id_conformation] = move_x[id_conformation];
			vectors_e.move_y[id_conformation] = move_y[id_conformation];
			vectors_e.move_z[id_conformation] = move_z[id_conformation];
			vectors_e.quat_x[id_conformation] = quat_x[id_conformation];
			vectors_e.quat_y[id_conformation] = quat_y[id_conformation];
			vectors_e.quat_z[id_conformation] = quat_z[id_conformation];
			vectors_e.quat_w[id_conformation] = quat_w[id_conformation];
			vectors_e.energy.energy[id_conformation] = energy[id_conformation];
						
		}
	}
}

extern void incluir_mejorar_inicializar_environment (struct vectors_t vectors_e,  type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int nconformations, struct metaheuristic_t metaheuristic, struct param_t param)
{
        unsigned int id_conformation,th1;
        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 0:
                case 3:
                case 2:
                        th1 = metaheuristic.Threads1Inc;
                        break;
        }
        omp_set_num_threads (th1);
        #pragma omp parallel for //private (id_conformation)
        for (unsigned int i = 0;i < nconformations;i++)
        {
 		
		//id_conformation = vectors_e.energy.n_conformation[i];
		for (unsigned int j = 0; j < metaheuristic.NEEImp; j++)
		{
                	if (vectors_e.energy.energy[i*metaheuristic.NEEImp + j] < energy[i])
                	{
                        	move_x[i] = vectors_e.move_x[i*metaheuristic.NEEImp + j];
                        	move_y[i] = vectors_e.move_y[i*metaheuristic.NEEImp + j];
                        	move_z[i] = vectors_e.move_z[i*metaheuristic.NEEImp + j];
                        	quat_x[i] = vectors_e.quat_x[i*metaheuristic.NEEImp + j];
                        	quat_y[i] = vectors_e.quat_y[i*metaheuristic.NEEImp + j];
                        	quat_z[i] = vectors_e.quat_z[i*metaheuristic.NEEImp + j];
                        	quat_w[i] = vectors_e.quat_w[i*metaheuristic.NEEImp + j];
                        	energy[i] = vectors_e.energy.energy[i*metaheuristic.NEEImp + j];
                	}
                	else
                	{
                        	vectors_e.move_x[i*metaheuristic.NEEImp + j] = move_x[i];
                        	vectors_e.move_y[i*metaheuristic.NEEImp + j] = move_y[i];
                        	vectors_e.move_z[i*metaheuristic.NEEImp + j] = move_z[i];
                        	vectors_e.quat_x[i*metaheuristic.NEEImp + j] = quat_x[i];
                        	vectors_e.quat_y[i*metaheuristic.NEEImp + j] = quat_y[i];
                        	vectors_e.quat_z[i*metaheuristic.NEEImp + j] = quat_z[i];
                        	vectors_e.quat_w[i*metaheuristic.NEEImp + j] = quat_w[i];
                        	vectors_e.energy.energy[i*metaheuristic.NEEImp + j] = energy[i];
			}
                }
        }
}

extern void mejorar (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, unsigned int num_surface, type_data *weights, struct force_field_param_t *f_params, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int nconformations, struct metaheuristic_t metaheuristic, unsigned int opt)
{
	struct vectors_t vectors_mejora;
	unsigned int steps,moveType=MOVE,bucle,fase;	
	double tenergymejora;
	double tenergymejora_t;
	unsigned int tam = nconformations / num_surface;
	vectors_mejora.num_surface = num_surface;
	vectors_mejora.nconformations = nconformations;

	vectors_mejora.move_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.move_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.move_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations);
	vectors_mejora.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);	
	vectors_mejora.weights = (type_data *)malloc(sizeof(type_data)*SCORING_TERMS);
 	//printf("Memoria para mejora creada....\n");
	
	memcpy(vectors_mejora.move_x, move_x, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.move_y, move_y, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.move_z, move_z, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_x, quat_x, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_y, quat_y, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_z, quat_z, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_w, quat_w, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.energy.n_conformation, nconfs, vectors_mejora.nconformations * sizeof(int));
	//memcpy(vectors_mejora.vdw_params, vdw_params,MAXTYPES * sizeof(struct vdw_param_t)); 	
	//memcpy(vectors_mejora.sasa_params, sasa_params,MAXTYPES * sizeof(struct sasa_param_t)); 	
	//memcpy(vectors_mejora.hbond_params, hbond_params,MAXTYPES * sizeof(struct hbond_param_t)); 
	memcpy(vectors_mejora.f_params, f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_mejora.weights, weights,SCORING_TERMS * sizeof(type_data));	
	memset(vectors_mejora.energy.energy,0,vectors_mejora.nconformations * sizeof(type_data));
	
	steps = 0;
	if (opt == 0) 
	{
		bucle = metaheuristic.IMEIni;
	}
	if (opt == 1)
	{	
		bucle = metaheuristic.IMEImp;	
	}
	if (opt == 2)
	{
		bucle = metaheuristic.IMEMUCom;
	}
	moveType = MOVE;
	//printf("Voy a mejorar....\n");
	while (steps < bucle){		
		if (moveType == MOVE){
			move_mejora(vectors_mejora.num_surface,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,param,tam,metaheuristic);
			moveType = ROTATE;
			//printf("He movido\n");
		}
		else {
			rotate(vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,param,vectors_mejora.nconformations,metaheuristic);
			moveType = MOVE;
		}		
		//printf("Voy a calcular la energia...\n");
		tenergymejora = wtime();
		ForcesCpuSolver(param,ligando,proteina,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,vectors_mejora.energy.energy,vectors_mejora.energy.n_conformation,vectors_mejora.weights,vectors_mejora.f_params,vectors_mejora.nconformations,metaheuristic);		
		tenergymejora_t += wtime() - tenergymejora;
		incluir_mejorar_inicializar(vectors_mejora, move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,nconformations,metaheuristic, param);	
		steps++;	
	}
	free(vectors_mejora.move_x);
	free(vectors_mejora.move_y);
	free(vectors_mejora.move_z);
	free(vectors_mejora.quat_x);
	free(vectors_mejora.quat_y);
	free(vectors_mejora.quat_z);
	free(vectors_mejora.quat_w);
	free(vectors_mejora.energy.energy);
	free(vectors_mejora.energy.n_conformation);
	free(vectors_mejora.weights);
}


extern void seleccionar_inicializar (struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic)
{
	unsigned int v1_tmp,l;
	type_data v2_tmp, v3_tmp;
	unsigned int th1;
		
	v1_tmp = vectors_e->nconformations / vectors_e->num_surface;
	metaheuristic.NEIIni_selec = (v1_tmp * metaheuristic.PEMIni)/100;
	//v3_tmp = getPadding(metaheuristic.NEIIni_selec,16);
	//metaheuristic.NEIIni_selec += v3_tmp; 
	
	vectors_s->num_surface = vectors_e->num_surface;
	vectors_s->nconformations = vectors_e->num_surface * metaheuristic.NEIIni_selec;
	vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nconformations);
	vectors_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);	
	vectors_s->weights = (type_data *)malloc(sizeof(type_data)*SCORING_TERMS);

	//memcpy(vectors_s->vdw_params, vectors_e->vdw_params,MAXTYPES * sizeof(struct vdw_param_t)); 
	//memcpy(vectors_s->sasa_params, vectors_e->sasa_params,MAXTYPES * sizeof(struct sasa_param_t)); 
	//memcpy(vectors_s->hbond_params, vectors_e->hbond_params,MAXTYPES * sizeof(struct hbond_param_t));
	memcpy(vectors_s->f_params, vectors_e->f_params,MAXTYPES * sizeof(struct force_field_param_t));
	memcpy(vectors_s->weights, vectors_e->weights,sizeof(type_data)*SCORING_TERMS); 
	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 0:
		case 3:
		case 2:
			th1 = metaheuristic.Threads1Inc;
			break;
	}	
	omp_set_num_threads (th1);
	#pragma omp parallel for 	
	for (unsigned int i = 0; i < vectors_e->num_surface; i++)
	{
		memcpy(vectors_s->move_x + i*metaheuristic.NEIIni_selec , vectors_e->move_x + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));  	
		memcpy(vectors_s->move_y + i*metaheuristic.NEIIni_selec, vectors_e->move_y + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data)); 
		memcpy(vectors_s->move_z + i*metaheuristic.NEIIni_selec, vectors_e->move_z + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));  
		memcpy(vectors_s->quat_x + i*metaheuristic.NEIIni_selec, vectors_e->quat_x + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));
		memcpy(vectors_s->quat_y + i*metaheuristic.NEIIni_selec, vectors_e->quat_y + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));
		memcpy(vectors_s->quat_z + i*metaheuristic.NEIIni_selec, vectors_e->quat_z + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));
		memcpy(vectors_s->quat_w + i*metaheuristic.NEIIni_selec, vectors_e->quat_w + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));
		memcpy((vectors_s->energy.energy + i*metaheuristic.NEIIni_selec), (vectors_e->energy.energy + i*metaheuristic.NEIIni), metaheuristic.NEIIni_selec * sizeof(type_data));		
	}
}

extern void seleccionar_mejorar (struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic)
{
	unsigned int v1_tmp,l, th1;
	type_data v2_tmp, v3_tmp;	
	v1_tmp = vectors_e->nconformations / vectors_e->num_surface;
	metaheuristic.PEMSel_selec = (v1_tmp * metaheuristic.PEMSel)/100;
	//v3_tmp = getPadding(metaheuristic.PEMSel_selec,16);
	//metaheuristic.PEMSel_selec += v3_tmp; 
	
	//printf("NEISelec %d\n",metaheuristic.PEMSel_selec);	
	free(vectors_s->move_x);
	free(vectors_s->move_y);
	free(vectors_s->move_z);
	free(vectors_s->quat_x);
	free(vectors_s->quat_y);
	free(vectors_s->quat_z);
	free(vectors_s->quat_w);
	free(vectors_s->energy.energy);
	free(vectors_s->energy.n_conformation);
	free(vectors_s->weights);
		
	vectors_s->num_surface = vectors_e->num_surface;
	vectors_s->nconformations = vectors_e->num_surface * metaheuristic.PEMSel_selec;
	vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nconformations);
	vectors_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);	
	vectors_s->weights = (type_data *)malloc(sizeof(type_data)*SCORING_TERMS);
	
	//memcpy(vectors_s->vdw_params, vectors_e->vdw_params,MAXTYPES * sizeof(struct vdw_param_t)); 
	//memcpy(vectors_s->sasa_params, vectors_e->sasa_params,MAXTYPES * sizeof(struct sasa_param_t)); 
	//memcpy(vectors_s->hbond_params, vectors_e->hbond_params,MAXTYPES * sizeof(struct hbond_param_t));
	memcpy(vectors_s->f_params, vectors_e->f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_s->weights, vectors_e->weights,sizeof(type_data)*SCORING_TERMS);

	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 0:
		case 3:
		case 2:
			th1 = metaheuristic.Threads1Inc;
			break;
	}	
	omp_set_num_threads (th1);
	#pragma omp parallel for 	
	for (unsigned int i = 0; i < vectors_e->num_surface; i++)
	{
		memcpy(vectors_s->move_x + i*metaheuristic.PEMSel_selec , vectors_e->move_x + i*v1_tmp, metaheuristic.PEMSel_selec * sizeof(type_data));  	
		memcpy(vectors_s->move_y + i*metaheuristic.PEMSel_selec, vectors_e->move_y + i*v1_tmp, metaheuristic.PEMSel_selec * sizeof(type_data)); 
		memcpy(vectors_s->move_z + i*metaheuristic.PEMSel_selec, vectors_e->move_z + i*v1_tmp, metaheuristic.PEMSel_selec * sizeof(type_data));  
		memcpy(vectors_s->quat_x + i*metaheuristic.PEMSel_selec, vectors_e->quat_x + i*v1_tmp, metaheuristic.PEMSel_selec * sizeof(type_data));
		memcpy(vectors_s->quat_y + i*metaheuristic.PEMSel_selec, vectors_e->quat_y + i*v1_tmp, metaheuristic.PEMSel_selec * sizeof(type_data));
		memcpy(vectors_s->quat_z + i*metaheuristic.PEMSel_selec, vectors_e->quat_z + i*v1_tmp, metaheuristic.PEMSel_selec * sizeof(type_data));
		memcpy(vectors_s->quat_w + i*metaheuristic.PEMSel_selec, vectors_e->quat_w + i*v1_tmp, metaheuristic.PEMSel_selec * sizeof(type_data));
		memcpy((vectors_s->energy.energy + i*metaheuristic.PEMSel_selec), (vectors_e->energy.energy + i*v1_tmp), metaheuristic.PEMSel_selec * sizeof(type_data));		
	} 	
}

extern void seleccionar_inicializar_multigpu (struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic)
{
	//printf("NEISelec %d\n",metaheuristic.NEIIni_selec);	
	vectors_s->num_surface = vectors_e->num_surface;
	vectors_s->nconformations = vectors_e->num_surface * metaheuristic.NEIIni_selec;
	vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nconformations);
	vectors_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);	
	vectors_s->weights = (type_data *)malloc(sizeof(type_data)*SCORING_TERMS);

	//memcpy(vectors_s->vdw_params, vectors_e->vdw_params,MAXTYPES * sizeof(struct vdw_param_t)); 
	//memcpy(vectors_s->sasa_params, vectors_e->sasa_params,MAXTYPES * sizeof(struct sasa_param_t)); 
	//memcpy(vectors_s->hbond_params, vectors_e->hbond_params,MAXTYPES * sizeof(struct hbond_param_t)); 
	memcpy(vectors_s->f_params, vectors_e->f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_s->weights, vectors_e->weights,sizeof(type_data)*SCORING_TERMS);

	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
	
 	omp_set_num_threads (metaheuristic.Threads1Sel);
	#pragma omp parallel for 	
	for (unsigned int i = 0; i < vectors_e->num_surface; i++)
	{
		memcpy(vectors_s->move_x + i*metaheuristic.NEIIni_selec , vectors_e->move_x + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));  	
		memcpy(vectors_s->move_y + i*metaheuristic.NEIIni_selec, vectors_e->move_y + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data)); 
		memcpy(vectors_s->move_z + i*metaheuristic.NEIIni_selec, vectors_e->move_z + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));  
		memcpy(vectors_s->quat_x + i*metaheuristic.NEIIni_selec, vectors_e->quat_x + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));
		memcpy(vectors_s->quat_y + i*metaheuristic.NEIIni_selec, vectors_e->quat_y + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));
		memcpy(vectors_s->quat_z + i*metaheuristic.NEIIni_selec, vectors_e->quat_z + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));
		memcpy(vectors_s->quat_w + i*metaheuristic.NEIIni_selec, vectors_e->quat_w + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));
		memcpy((vectors_s->energy.energy + i*metaheuristic.NEIIni_selec), (vectors_e->energy.energy + i*metaheuristic.NEIIni), metaheuristic.NEIIni_selec * sizeof(type_data));
	}			
	
}

extern void seleccionar_mutar (struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic)
{
        int v1_tmp,l;
        float v2_tmp, v3_tmp;
        int th1;
        v1_tmp = vectors_e->nconformations / vectors_e->num_surface;
        metaheuristic.PEMUCom_selec = (v1_tmp * metaheuristic.PEMUCom)/100;
        //v3_tmp = getPadding(metaheuristic.PEMSel_selec,16);
        //metaheuristic.PEMSel_selec += v3_tmp;
        //printf("MUTSelec %d surf %d\n",metaheuristic.PEMUCom_selec,vectors_e->num_surface);

        free(vectors_s->move_x);
        free(vectors_s->move_y);
        free(vectors_s->move_z);
        free(vectors_s->quat_x);
        free(vectors_s->quat_y);
        free(vectors_s->quat_z);
        free(vectors_s->quat_w);
        free(vectors_s->energy.energy);
        free(vectors_s->energy.n_conformation);
	free(vectors_s->weights);
        vectors_s->num_surface = vectors_e->num_surface;
        vectors_s->nconformations = vectors_e->num_surface * metaheuristic.PEMUCom_selec;
        vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
        vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
        vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
        vectors_s->quat_x = (float *)malloc(sizeof(float)*vectors_s->nconformations);
        vectors_s->quat_y = (float *)malloc(sizeof(float)*vectors_s->nconformations);
        vectors_s->quat_z = (float *)malloc(sizeof(float)*vectors_s->nconformations);
        vectors_s->quat_w = (float *)malloc(sizeof(float)*vectors_s->nconformations);
        vectors_s->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nconformations);
        vectors_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->weights = (type_data *)malloc(sizeof(type_data)*SCORING_TERMS);

        //memcpy(vectors_s->vdw_params, vectors_e->vdw_params,MAXTYPES * sizeof(struct vdw_param_t));
        //memcpy(vectors_s->sasa_params, vectors_e->sasa_params,MAXTYPES * sizeof(struct sasa_param_t));
        //memcpy(vectors_s->hbond_params, vectors_e->hbond_params,MAXTYPES * sizeof(struct hbond_param_t));
        memcpy(vectors_s->f_params, vectors_e->f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_s->weights, vectors_e->weights,sizeof(type_data)*SCORING_TERMS);

        thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
        //secuenciar (vectors_s->energy.n_conformation,vectors_s->nconformations);

        switch (param.mode) {
                case 1:
                     th1 = 1;
                     break;
                case 0:
                case 3:
                case 2:
                     th1 = metaheuristic.Threads1Inc;
					break;
        }
        omp_set_num_threads (th1);
        #pragma omp parallel for
        for (int i = 0; i < vectors_e->num_surface; i++)
        {
                memcpy(vectors_s->move_x + i*metaheuristic.PEMUCom_selec , vectors_e->move_x + i*v1_tmp, metaheuristic.PEMUCom_selec * sizeof(type_data));
                memcpy(vectors_s->move_y + i*metaheuristic.PEMUCom_selec, vectors_e->move_y + i*v1_tmp, metaheuristic.PEMUCom_selec * sizeof(type_data));
                memcpy(vectors_s->move_z + i*metaheuristic.PEMUCom_selec, vectors_e->move_z + i*v1_tmp, metaheuristic.PEMUCom_selec * sizeof(type_data));
                memcpy(vectors_s->quat_x + i*metaheuristic.PEMUCom_selec, vectors_e->quat_x + i*v1_tmp, metaheuristic.PEMUCom_selec * sizeof(float));
                memcpy(vectors_s->quat_y + i*metaheuristic.PEMUCom_selec, vectors_e->quat_y + i*v1_tmp, metaheuristic.PEMUCom_selec * sizeof(float));
                memcpy(vectors_s->quat_z + i*metaheuristic.PEMUCom_selec, vectors_e->quat_z + i*v1_tmp, metaheuristic.PEMUCom_selec * sizeof(float));
                memcpy(vectors_s->quat_w + i*metaheuristic.PEMUCom_selec, vectors_e->quat_w + i*v1_tmp, metaheuristic.PEMUCom_selec * sizeof(float));
                memcpy((vectors_s->energy.energy + i*metaheuristic.PEMUCom_selec), (vectors_e->energy.energy + i*v1_tmp), metaheuristic.PEMUCom_selec * sizeof(type_data));
        }
}
					 
extern void inicializar (struct receptor_t proteina, struct ligand_t ligando, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, type_data **dqn_data)
{
	
	//CONFIGURING MEMORY
	unsigned int NEItemp, NEI_selec_temp, th1, stride_e, stride_s;
	unsigned int max_conformaciones;
	double tgenerarposi, tenergycpu, tordenarcpu, tcolocarcpu, tmejorainicpu;
	double tgenerarposi_t, tenergycpu_t, tordenarcpu_t, tcolocarcpu_t, tmejorainicpu_t;
	//int NEIIni_selec = (int)ceil((metaheuristic.NEIIni * metaheuristic.PEMIni) / 100);
	struct vectors_t vectors_selec;
	type_data kernels=0;

	
	switch (param.mode) {
		case 0:
			//GPU SELECTION
			/*int *device_current;
			cudaDeviceProp *prop;
			device_current = new int;
			prop = new cudaDeviceProp();	
			cudaSetDevice(devices->id[0]);
			cudaGetDevice(device_current);
			cudaGetDeviceProperties (prop, *device_current);
			printf("\n%s\n",prop->name);*/
			//NEItemp	= metaheuristic.NEIIni;
			//NEItemp = getPadding(NEItemp,16);
			//metaheuristic.NEIIni += NEItemp;
			NEI_selec_temp = (unsigned int)ceil((metaheuristic.NEIIni * metaheuristic.PEMIni) / 100);
			metaheuristic.NEIIni_selec = NEI_selec_temp;// + getPadding(NEI_selec_temp,16);
			vectors_e->nconformations = vectors_e->num_surface * metaheuristic.NEIIni;
			vectors_e->move_x = (type_data *)calloc(vectors_e->nconformations,sizeof(type_data));
			vectors_e->move_y = (type_data *)calloc(vectors_e->nconformations,sizeof(type_data));
			vectors_e->move_z = (type_data *)calloc(vectors_e->nconformations,sizeof(type_data));
			vectors_e->quat_x = (type_data *)calloc(vectors_e->nconformations,sizeof(type_data));
			vectors_e->quat_y = (type_data *)calloc(vectors_e->nconformations,sizeof(type_data));
			vectors_e->quat_z = (type_data *)calloc(vectors_e->nconformations,sizeof(type_data));
			vectors_e->quat_w = (type_data *)calloc(vectors_e->nconformations,sizeof(type_data));
			vectors_e->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_e->nconformations);
			vectors_e->energy.energy = (type_data *)calloc(vectors_e->nconformations,sizeof(type_data));
			thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);

			//vectors_e->nconformations -= metaheuristic.DQNIni;
                        tgenerarposi = wtime();//tomamos tiempos
                        generate_positions (param,vectors_e,metaheuristic,devices,0,0);
                        tgenerarposi_t += wtime() - tgenerarposi;
                        //vectors_e->nconformations += metaheuristic.DQNIni;
			replace_for_dqn(dqn_data, vectors_e, metaheuristic);
			//exit(0);		
			
			tenergycpu = wtime();//tomamos tiempos para calculo
			max_conformaciones = ((devices->propiedades[0].maxGridSize[0] * devices->hilos[0*metaheuristic.num_kernels]) / WARP_SIZE) * 8;
			if (vectors_e->nconformations < max_conformaciones)
				gpuSolver (ligando,proteina,param,metaheuristic,devices,vectors_e->move_x,vectors_e->move_y,vectors_e->move_z,vectors_e->quat_x,vectors_e->quat_y,vectors_e->quat_z,vectors_e->quat_w,vectors_e->energy.energy,vectors_e->energy.n_conformation,vectors_e->weights,vectors_e->f_params,vectors_e->nconformations,0,0,&kernels);		
			else
			{
				//DIVIDIR EN TROZOS
				while((vectors_s->nconformations % max_conformaciones) != 0) max_conformaciones --;		
				//printf("va a trocear\n");
				for (unsigned int desplazamiento=0;desplazamiento<vectors_e->nconformations;desplazamiento+=max_conformaciones)
				{
					gpuSolver (ligando,proteina,param,metaheuristic,devices,vectors_e->move_x+desplazamiento,vectors_e->move_y+desplazamiento,vectors_e->move_z+desplazamiento,vectors_e->quat_x+desplazamiento,vectors_e->quat_y+desplazamiento,vectors_e->quat_z+desplazamiento,vectors_e->quat_w+desplazamiento,vectors_e->energy.energy+desplazamiento,vectors_e->energy.n_conformation+desplazamiento,vectors_e->weights,vectors_e->f_params,max_conformaciones,0,desplazamiento,&kernels);
					//printf("Device %d con trozo %d\n",devices->id[th],i);					
				}
			}			
			tenergycpu_t += wtime() - tenergycpu;
			//for (int i=0; i<vectors_e->nconformations;i++) printf("conf %d move_x %f, move_y %f, move_z %f, quat.x %f energy %f\n",i,vectors_e->move_x[i],vectors_e->move_y[i],vectors_e->move_z[i],vectors_e->quat_x[i],vectors_e->energy.energy[i]);
			//exit(0);
			tordenarcpu = wtime();
			//for (int i=0; i<64;i++) printf("conf %d move_x %f, move_y %f, move_z %f, quat.x %f energy %f n_conf %d\n",i,vectors_e->move_x[i],vectors_e->move_y[i],vectors_e->move_z[i],vectors_e->quat_x[i],vectors_e->energy.energy[i],vectors_e->energy.n_conformation[i]);
			//exit(0);
			omp_set_num_threads (metaheuristic.Threads1Ini);
			#pragma omp parallel for			
			for (unsigned int i = 0; i < vectors_e->nconformations; i += metaheuristic.NEIIni)							
			    thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + metaheuristic.NEIIni), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());			
			//printf("\n****\n");
			//for (int i=0; i<64;i++) printf("conf %d move_x %f, move_y %f, move_z %f, quat.x %f energy %f n_conf %d\n",i,vectors_e->move_x[i],vectors_e->move_y[i],vectors_e->move_z[i],vectors_e->quat_x[i],vectors_e->energy.energy[i],vectors_e->energy.n_conformation[i]);
			//exit(0);
			//colocar(param,vectors_e,metaheuristic);				
			#pragma omp parallel for
			for (unsigned int i = 0; i < vectors_e->num_surface; i++)
				colocar_by_step (param,metaheuristic,ligando.nlig,vectors_e->move_x + (metaheuristic.NEIIni*i),vectors_e->move_y + (metaheuristic.NEIIni*i),vectors_e->move_z + (metaheuristic.NEIIni*i),vectors_e->quat_x + (metaheuristic.NEIIni*i),vectors_e->quat_y + (metaheuristic.NEIIni*i),vectors_e->quat_z + (metaheuristic.NEIIni*i),vectors_e->quat_w + (metaheuristic.NEIIni*i),vectors_e->energy.energy + (metaheuristic.NEIIni*i),vectors_e->energy.n_conformation + (metaheuristic.NEIIni*i),metaheuristic.NEIIni,i);
			thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			//printf("\n*COLOCADOS*\n");
			//for (int i=0; i<64;i++) printf("conf %d move_x %f, move_y %f, move_z %f, quat.x %f energy %f n_conf %d\n",i,vectors_e->move_x[i],vectors_e->move_y[i],vectors_e->move_z[i],vectors_e->quat_x[i],vectors_e->energy.energy[i],vectors_e->energy.n_conformation[i]);
			//exit(0);
			tordenarcpu_t += wtime() - tordenarcpu;
			//colocar(param,vectors_e,metaheuristic);	
			
			
			//exit(0);	
			if (metaheuristic.PEMIni > 0)
			{
				tmejorainicpu = wtime(); //tomamos tiempos de mejora
				seleccionar_inicializar(vectors_e,&vectors_selec,param,metaheuristic);
		
				if (metaheuristic.NEEImp > 0)
				{
					 if (param.montecarlo)
                                                montecarlo_environment(proteina,ligando,param,&vectors_selec,metaheuristic,devices,0,0);
                                        else
                                                gpu_mejorar_environment (proteina,ligando,param,&vectors_selec,metaheuristic,devices,0,0);
				}
				else
				{										
					if (param.montecarlo)
						montecarlo(proteina,ligando,param,&vectors_selec,metaheuristic,devices,0,0);
					else
						gpu_mejorar (proteina,ligando,param,&vectors_selec,metaheuristic,devices,0,0);
				}
				//for (int i=0; i<64;i++) printf("conf %d move_x %f, move_y %f, move_z %f, quat.x %f quat.y %f energy %f n_conf %d\n",i,vectors_selec.move_x[i],vectors_selec.move_y[i],vectors_selec.move_z[i],vectors_selec.quat_x[i],vectors_selec.quat_y[i],vectors_selec.energy.energy[i],vectors_selec.energy.n_conformation[i]);
				//exit(0);				
				//incluir_gpu_mejorar (&vectors_selec,vectors_e,param,metaheuristic);
				stride_e = vectors_selec.nconformations / vectors_selec.num_surface;
                                stride_s = vectors_e->nconformations / vectors_e->num_surface;
                                #pragma omp parallel for
                                for (unsigned int i = 0; i < vectors_e->num_surface; i++)
                                        incluir_gpu_by_step (vectors_e->nlig,vectors_selec.move_x + (stride_e*i), vectors_selec.move_y + (stride_e*i), vectors_selec.move_z + (stride_e*i), vectors_selec.quat_x + (stride_e*i), vectors_selec.quat_y + (stride_e*i), vectors_selec.quat_z + (stride_e*i), vectors_selec.quat_w + (stride_e*i), vectors_selec.energy.energy + (stride_e*i), vectors_selec.energy.n_conformation + (stride_e*i), vectors_e->move_x + (stride_s*i),  vectors_e->move_y + (stride_s*i),  vectors_e->move_z + (stride_s*i),  vectors_e->quat_x + (stride_s*i),  vectors_e->quat_y + (stride_s*i),  vectors_e->quat_z + (stride_s*i),  vectors_e->quat_w + (stride_s*i),  vectors_e->energy.energy + (stride_s*i),  vectors_e->energy.n_conformation + (stride_s*i), stride_e, stride_s, i,param,metaheuristic);
                                thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
				tmejorainicpu_t += wtime() - tmejorainicpu;								
				omp_set_num_threads (metaheuristic.Threads1Ini);
				//for (int i=0; i<64;i++) printf("conf %d move_x %f, move_y %f, move_z %f, quat.x %f energy %f n_conf %d\n",i,vectors_e->move_x[i],vectors_e->move_y[i],vectors_e->move_z[i],vectors_e->quat_x[i],vectors_e->energy.energy[i],vectors_e->energy.n_conformation[i]);
				//exit(0);
				//#pragma omp parallel for 
				//for (unsigned int i = 0; i < vectors_e->nconformations; i += metaheuristic.NEIIni)			
				//	thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + metaheuristic.NEIIni), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());				
				//printf("\n****\n");
				//for (int i=0; i<64;i++) printf("conf %d move_x %f, move_y %f, move_z %f, quat.x %f energy %f n_conf %d\n",i,vectors_e->move_x[i],vectors_e->move_y[i],vectors_e->move_z[i],vectors_e->quat_x[i],vectors_e->energy.energy[i],vectors_e->energy.n_conformation[i]);
				//exit(0);
				//colocar(param,vectors_e,metaheuristic);						
				//#pragma omp parallel for
				//for (unsigned int i = 0; i < vectors_e->surface; i++)
				//	colocar_by_step (param,metaheuristic,vectors_e->nlig,vectors_e->move_x + (metaheuristic.NEIIni*i),vectors_e->move_y + (metaheuristic.NEIIni*i),vectors_e->move_z + (metaheuristic.NEIIni*i),vectors_e->quat_x + (metaheuristic.NEIIni*i),vectors_e->quat_y + (metaheuristic.NEIIni*i),vectors_e->quat_z + (metaheuristic.NEIIni*i),vectors_e->quat_w + (metaheuristic.NEIIni*i),vectors_e->energy.energy + (metaheuristic.NEIIni*i),vectors_e->energy.n_conformation + (metaheuristic.NEIIni*i),metaheuristic.NEIIni,i);
				//thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			}
			//printf("Despues de mejorar\n");
			//exit(0);
			//printf("\n****\n");
			//for (int i=0; i<64;i++) printf("conf %d move_x %f, move_y %f, move_z %f, quat.x %f energy %f\n",i,vectors_e->move_x[i],vectors_e->move_y[i],vectors_e->move_z[i],vectors_e->quat_x[i],vectors_e->energy.energy[i]);
			//exit(0);

			seleccionar_NEFIni(vectors_e,vectors_s,metaheuristic,param);
			printf("\nFases INICILIZAR:\n");
			printf("\tTiempo Generacion posiciones: %lf seg\n",tgenerarposi_t);				
			printf("\tTiempo Funcion de scoring: %lf seg\n",tenergycpu_t);	
			printf("\tTiempo Ordenar: %lf seg\n",tordenarcpu_t);
			if (metaheuristic.PEMIni > 0) printf("\tTiempo Mejorar: %lf seg\n",tmejorainicpu_t);	
			printf("\tTiempo Colocar: %lf seg\n",tcolocarcpu_t);					
			break;			
			
		case 1:		
		case 2:
			switch (param.mode) {
			case 1:
				th1 = 1;
				break;
			case 0:
			case 3:
			case 2:
				th1 = metaheuristic.Threads1Ini;
				break;
			}	
			//OPEN-MP SELECTION
			vectors_e->nconformations = vectors_e->num_surface * metaheuristic.NEIIni;
			vectors_e->move_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->move_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->move_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_e->nconformations);
			vectors_e->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			

			thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
				
			tgenerarposi = wtime();//tomamos tiempos
			generate_positions_cpp (param,vectors_e,metaheuristic);
			tgenerarposi_t += wtime() - tgenerarposi;			
			replace_for_dqn(dqn_data, vectors_e, metaheuristic);
			//exit(0);
			tenergycpu = wtime();//tomamos tiempos para calculo
			
			ForcesCpuSolver(param,ligando,proteina,vectors_e->move_x,vectors_e->move_y,vectors_e->move_z,vectors_e->quat_x,vectors_e->quat_y,vectors_e->quat_z,vectors_e->quat_w,vectors_e->energy.energy,vectors_e->energy.n_conformation,vectors_e->weights,vectors_e->f_params,vectors_e->nconformations,metaheuristic);			
			tenergycpu_t += wtime() - tenergycpu;
			//printf("force field Energy[0] %f\n",vectors_e->energy.energy[0]);
			//exit(0);	
			omp_set_num_threads (th1);
			#pragma omp parallel for
			for (unsigned int i = 0; i < vectors_e->nconformations; i += metaheuristic.NEIIni)			
				thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + metaheuristic.NEIIni), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());
			//colocar(param,vectors_e,metaheuristic);			
			#pragma omp parallel for
                        for (unsigned int i = 0; i < vectors_e->num_surface; i++)
                                colocar_by_step (param,metaheuristic,ligando.nlig,vectors_e->move_x + (metaheuristic.NEIIni*i),vectors_e->move_y + (metaheuristic.NEIIni*i),vectors_e->move_z + (metaheuristic.NEIIni*i),vectors_e->quat_x + (metaheuristic.NEIIni*i),vectors_e->quat_y + (metaheuristic.NEIIni*i),vectors_e->quat_z + (metaheuristic.NEIIni*i),vectors_e->quat_w + (metaheuristic.NEIIni*i),vectors_e->energy.energy + (metaheuristic.NEIIni*i),vectors_e->energy.n_conformation + (metaheuristic.NEIIni*i),metaheuristic.NEIIni,i);
                        thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			
			if (metaheuristic.PEMIni > 0)
			{
				tmejorainicpu = wtime(); //tomamos tiempos de mejora
				seleccionar_inicializar(vectors_e,&vectors_selec,param,metaheuristic);
				if (metaheuristic.NEEImp > 0)
				{
					if (param.montecarlo)
                                                montecarlo_cpp_environment (proteina,ligando,param,vectors_selec.num_surface,vectors_selec.weights,vectors_selec.f_params,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z,vectors_selec.quat_x,vectors_selec.quat_y,vectors_selec.quat_z,vectors_selec.quat_w,vectors_selec.energy.energy,vectors_selec.energy.n_conformation,vectors_selec.nconformations,metaheuristic,0);
                                        else
                                                mejorar_environment (proteina,ligando,param,vectors_selec.num_surface, vectors_selec.weights, vectors_selec.f_params,vectors_selec.move_x, vectors_selec.move_y, vectors_selec.move_z, vectors_selec.quat_x, vectors_selec.quat_y, vectors_selec.quat_z, vectors_selec.quat_w, vectors_selec.energy.energy, vectors_selec.energy.n_conformation, vectors_selec.nconformations,metaheuristic,0);
				}
				else
				{
					if (param.montecarlo)
			 			montecarlo_cpp (proteina,ligando,param,vectors_selec.num_surface,vectors_selec.weights,vectors_selec.f_params,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z,vectors_selec.quat_x,vectors_selec.quat_y,vectors_selec.quat_z,vectors_selec.quat_w,vectors_selec.energy.energy,vectors_selec.energy.n_conformation,vectors_selec.nconformations,metaheuristic,0);	
					else	
						mejorar(proteina,ligando,param,vectors_selec.num_surface, vectors_selec.weights, vectors_selec.f_params,vectors_selec.move_x, vectors_selec.move_y, vectors_selec.move_z, vectors_selec.quat_x, vectors_selec.quat_y, vectors_selec.quat_z, vectors_selec.quat_w, vectors_selec.energy.energy, vectors_selec.energy.n_conformation, vectors_selec.nconformations,metaheuristic,0);
				}
				//incluir_gpu_mejorar (&vectors_selec,vectors_e,param,metaheuristic);
				stride_e = vectors_selec.nconformations / vectors_selec.num_surface;
                                stride_s = vectors_e->nconformations / vectors_e->num_surface;
				omp_set_num_threads (th1);
                                #pragma omp parallel for
                                for (unsigned int i = 0; i < vectors_e->num_surface; i++)
                                       	incluir_gpu_by_step (vectors_e->nlig,vectors_selec.move_x + (stride_e*i), vectors_selec.move_y + (stride_e*i), vectors_selec.move_z + (stride_e*i), vectors_selec.quat_x + (stride_e*i), vectors_selec.quat_y + (stride_e*i), vectors_selec.quat_z + (stride_e*i), vectors_selec.quat_w + (stride_e*i), vectors_selec.energy.energy + (stride_e*i), vectors_selec.energy.n_conformation + (stride_e*i), vectors_e->move_x + (stride_s*i),  vectors_e->move_y + (stride_s*i),  vectors_e->move_z + (stride_s*i),  vectors_e->quat_x + (stride_s*i),  vectors_e->quat_y + (stride_s*i),  vectors_e->quat_z + (stride_s*i),  vectors_e->quat_w + (stride_s*i),  vectors_e->energy.energy + (stride_s*i),  vectors_e->energy.n_conformation + (stride_s*i), stride_e, stride_s, i,param,metaheuristic);
                                thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
				
				//omp_set_num_threads (th1);
				//#pragma omp parallel for
				//for (unsigned int i = 0; i < vectors_e->nconformations; i += metaheuristic.NEIIni)			
				//	thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + metaheuristic.NEIIni), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());
				//colocar(param, vectors_e, metaheuristic);
				//#pragma omp parallel for
                                //for (unsigned int i = 0; i < vectors_e->surface; i++)
                                 //       colocar_by_step (param,metaheuristic,vectors_e->nlig,vectors_e->move_x + (metaheuristic.NEIIni*i),vectors_e->move_y + (metaheuristic.NEIIni*i),vectors_e->move_z + (metaheuristic.NEIIni*i),vectors_e->quat_x + (metaheuristic.NEIIni*i),vectors_e->quat_y + (metaheuristic.NEIIni*i),vectors_e->quat_z + (metaheuristic.NEIIni*i),vectors_e->quat_w + (metaheuristic.NEIIni*i),vectors_e->energy.energy + (metaheuristic.NEIIni*i),vectors_e->energy.n_conformation + (metaheuristic.NEIIni*i),metaheuristic.NEIIni,i);
                                //thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
				tmejorainicpu_t += wtime() - tmejorainicpu;				
			
			}					
			seleccionar_NEFIni(vectors_e,vectors_s,metaheuristic,param);
			printf("\nFases INICILIZAR:\n");
			printf("\tTiempo Generacion posiciones: %lf seg\n",tgenerarposi_t);				
			printf("\tTiempo Funcion de scoring: %lf seg\n",tenergycpu_t);				
			if (metaheuristic.PEMIni > 0) printf("\tTiempo Mejorar: %lf seg\n",tmejorainicpu_t);
			break;
		case 3:
			//MULTIGPU SELECTION
			unsigned int n_devices,k,*device_current_m;
			/*cudaDeviceProp *prop_m;
			device_current_m = new int;
			prop_m = new cudaDeviceProp();	
			
			cudaSetDevice(param.ngpu);
			cudaGetDevice(device_current_m);
			cudaGetDeviceProperties (prop_m, *device_current_m);
			printf("\n%s\n",prop_m->name);*/
			//NEItemp	= metaheuristic.NEIIni;
			//NEItemp = getPadding(NEItemp,16);
			///metaheuristic.NEIIni += NEItemp;
			NEI_selec_temp = (unsigned int)ceil((metaheuristic.NEIIni * metaheuristic.PEMIni) / 100);
			metaheuristic.NEIIni_selec = NEI_selec_temp;// + getPadding(NEI_selec_temp,16);
;
			vectors_e->nconformations = vectors_e->num_surface * metaheuristic.NEIIni;
			vectors_e->move_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->move_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->move_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_e->nconformations);
			vectors_e->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			
			//vectors_e->nconformations -= metaheuristic.DQNIni;
			tgenerarposi = wtime();//tomamos tiempos
			generate_positions (param,vectors_e,metaheuristic,devices,0,0);
			tgenerarposi_t += wtime() - tgenerarposi;
			replace_for_dqn(dqn_data, vectors_e, metaheuristic);
			//vectors_e->nconformations += metaheuristic.DQNIni;

			//exit(0);								
			tenergycpu = wtime();//tomamos tiempos para calculo L-J
			multigpuSolver(ligando,proteina,vectors_e->num_surface,vectors_e->move_x,vectors_e->move_y,vectors_e->move_z,vectors_e->quat_x,vectors_e->quat_y,vectors_e->quat_z,vectors_e->quat_w,vectors_e->energy,vectors_e->weights,vectors_e->f_params,vectors_e->nconformations,param,metaheuristic,devices);
			tenergycpu_t += wtime() - tenergycpu;
			//exit(0);
			//cudaSetDevice(devices->id[0]);	
			tordenarcpu = wtime();
			omp_set_num_threads (metaheuristic.Threads1Ini);
			#pragma omp parallel for			
			for (unsigned int i = 0; i < vectors_e->nconformations; i += metaheuristic.NEIIni)							
				thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + metaheuristic.NEIIni), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());				
			tordenarcpu_t += wtime() - tordenarcpu;
			//colocar(param,vectors_e,metaheuristic);	
			 #pragma omp parallel for
                        for (unsigned int i = 0; i < vectors_e->num_surface; i++)
                                colocar_by_step (param,metaheuristic,ligando.nlig,vectors_e->move_x + (metaheuristic.NEIIni*i),vectors_e->move_y + (metaheuristic.NEIIni*i),vectors_e->move_z + (metaheuristic.NEIIni*i),vectors_e->quat_x + (metaheuristic.NEIIni*i),vectors_e->quat_y + (metaheuristic.NEIIni*i),vectors_e->quat_z + (metaheuristic.NEIIni*i),vectors_e->quat_w + (metaheuristic.NEIIni*i),vectors_e->energy.energy + (metaheuristic.NEIIni*i),vectors_e->energy.n_conformation + (metaheuristic.NEIIni*i),metaheuristic.NEIIni,i);
                        thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			//for (int l=0;l < vectors_e->nconformations;l++)
			//			printf("Conformation: %d , move_x %f, move_y %f, move_z %f, quat.x %f, energy %f\n",vectors_e->energy.n_conformation[l],vectors_e->move_x[l], vectors_e->move_y[l], vectors_e->move_z[l],vectors_e->quat[l].x,vectors_e->energy.energy[l]);			
			//	exit(0);
			if (metaheuristic.PEMIni > 0)
			{
				tmejorainicpu = wtime(); //tomamos tiempos de mejora
				seleccionar_inicializar_multigpu(vectors_e,&vectors_selec,param,metaheuristic);		
				
                        	if (metaheuristic.PEMIni > 0)
                        	{	
					multigpuSolver_mejora_environment (ligando,proteina,vectors_selec.num_surface,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z,vectors_selec.quat_x,vectors_selec.quat_y,vectors_selec.quat_z,vectors_selec.quat_w,vectors_selec.energy.energy,vectors_selec.energy.n_conformation,vectors_selec.weights,vectors_selec.f_params,vectors_selec.nconformations,param,metaheuristic,devices,0);				
				}
				else
				{
					multigpuSolver_mejora (ligando,proteina,vectors_selec.num_surface,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z,vectors_selec.quat_x,vectors_selec.quat_y,vectors_selec.quat_z,vectors_selec.quat_w,vectors_selec.energy.energy,vectors_selec.energy.n_conformation,vectors_selec.weights,vectors_selec.f_params,vectors_selec.nconformations,param,metaheuristic,devices,0);
				}
				//incluir_gpu_mejorar (&vectors_selec,vectors_e,param,metaheuristic);	
				stride_e = vectors_selec.nconformations / vectors_selec.num_surface;
                                stride_s = vectors_e->nconformations / vectors_e->num_surface;
                                omp_set_num_threads (th1);
                                #pragma omp parallel for
                                for (unsigned int i = 0; i < vectors_e->num_surface; i++)
                                        incluir_gpu_by_step (vectors_e->nlig,vectors_selec.move_x + (stride_e*i), vectors_selec.move_y + (stride_e*i), vectors_selec.move_z + (stride_e*i), vectors_selec.quat_x + (stride_e*i), vectors_selec.quat_y + (stride_e*i), vectors_selec.quat_z + (stride_e*i), vectors_selec.quat_w + (stride_e*i), vectors_selec.energy.energy + (stride_e*i), vectors_selec.energy.n_conformation + (stride_e*i), vectors_e->move_x + (stride_s*i),  vectors_e->move_y + (stride_s*i),  vectors_e->move_z + (stride_s*i),  vectors_e->quat_x + (stride_s*i),  vectors_e->quat_y + (stride_s*i),  vectors_e->quat_z + (stride_s*i),  vectors_e->quat_w + (stride_s*i),  vectors_e->energy.energy + (stride_s*i),  vectors_e->energy.n_conformation + (stride_s*i), stride_e, stride_s, i,param,metaheuristic);
                                thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);				
				tmejorainicpu_t += wtime() - tmejorainicpu;				
				//omp_set_num_threads (metaheuristic.Threads1Ini);
				//#pragma omp parallel for 
				//for (unsigned int i = 0; i < vectors_e->nconformations; i += metaheuristic.NEIIni)			
				//	thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + metaheuristic.NEIIni), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());
				//colocar(param,vectors_e,metaheuristic);						
				//#pragma omp parallel for
                               // for (unsigned int i = 0; i < vectors_e->surface; i++)
                                 //       colocar_by_step (param,metaheuristic,vectors_e->nlig,vectors_e->move_x + (metaheuristic.NEIIni*i),vectors_e->move_y + (metaheuristic.NEIIni*i),vectors_e->move_z + (metaheuristic.NEIIni*i),vectors_e->quat_x + (metaheuristic.NEIIni*i),vectors_e->quat_y + (metaheuristic.NEIIni*i),vectors_e->quat_z + (metaheuristic.NEIIni*i),vectors_e->quat_w + (metaheuristic.NEIIni*i),vectors_e->energy.energy + (metaheuristic.NEIIni*i),vectors_e->energy.n_conformation + (metaheuristic.NEIIni*i),metaheuristic.NEIIni,i);
                                //thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			}
			seleccionar_NEFIni(vectors_e,vectors_s,metaheuristic,param);
			printf("\nFases INICILIZAR:\n");
			printf("\tTiempo Generacion posiciones: %lf seg\n",tgenerarposi_t);				
			printf("\tTiempo Funcion de scoring: %lf seg\n",tenergycpu_t);	
			printf("\tTiempo Ordenar: %lf seg\n",tordenarcpu_t);
			if (metaheuristic.PEMIni > 0) printf("\tTiempo Mejorar: %lf seg\n",tmejorainicpu_t);	
			printf("\tTiempo Colocar: %lf seg\n",tcolocarcpu_t);	
			//exit(0);
			break;			
	}

	if (metaheuristic.PEMIni > 0)
	{			
		free(vectors_selec.move_x);	
		free(vectors_selec.move_y);
		free(vectors_selec.move_z);
		free(vectors_selec.quat_x);
		free(vectors_selec.quat_y);
		free(vectors_selec.quat_z);
		free(vectors_selec.quat_w);
		free(vectors_selec.energy.energy);
		free(vectors_selec.energy.n_conformation);
		free(vectors_selec.weights);
	}
}


