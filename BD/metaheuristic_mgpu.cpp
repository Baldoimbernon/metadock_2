#include "definitions.h"
#include "wtime.h"
#include "energy_common.h"
#include "energy_struct.h"
#include "energy_GpuSolver.h"
#include "energy_positions.h"
#include "energy_montecarlo.h"
#include "metaheuristic_mgpu.h"
#include <thrust/sequence.h>
#include <thrust/sort.h>
#include <omp.h>

using namespace std;

extern void multigpuSolver (struct ligand_t ligando, struct receptor_t proteina, unsigned int surface, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, struct energy_t energy, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices)
{
	double tiempo_i, tiempo_f;
	type_data kernels=0;
	unsigned int  pc_temp, dev, stride_temp,ttotal = 0;
	
	pc_temp = nconformations / surface;
	devices->stride[0] = 0;	
	/*stride_temp = floor((devices->trabajo[0] * 0.01)*surface);	
	devices->conformaciones[0] = stride_temp * pc_temp;
	ttotal = stride_temp;
	//exit(0);
	//printf("nconformations %d, surface %d pc_temp %d\n",nconformations,surface,pc_temp);	
	
	for (unsigned int i=(devices->n_devices - 1);i > 0;i--)
	{
		stride_temp = floor((devices->trabajo[i] * 0.01)*surface);
		devices->conformaciones[i] = stride_temp * pc_temp;
		ttotal += stride_temp;
	}
	unsigned int diferencia = surface - ttotal;
	for (unsigned int i=0;i<diferencia;i++)
	{
		dev = i % devices->n_devices;
		devices->conformaciones[dev] += pc_temp;
	}
	for (unsigned int i=(devices->n_devices-1);i > 0;i--)
	{
		devices->stride[i] = nconformations - devices->conformaciones[i];
		nconformations = nconformations - devices->conformaciones[i];		
	}*/
	 for (int i=0;i < devices->n_devices;i++)
        {
                stride_temp = floor((devices->trabajo[i] * 0.01)*nconformations);
                devices->conformaciones[i] = stride_temp;
                ttotal += stride_temp;
        }
        int diferencia = nconformations - ttotal;
        for (int i=0;i<diferencia;i++)
        {
                dev = i % devices->n_devices;
                devices->conformaciones[dev] ++;
        }
        for (int i=(devices->n_devices-1);i > 0;i--)
        {
                devices->stride[i] = nconformations - devices->conformaciones[i];
                nconformations = nconformations - devices->conformaciones[i];
        }	
	tiempo_i = wtime();
	omp_set_num_threads(devices->n_devices);
	//for (int i =0;i<423168;i++) 
	//	printf("conf %d, move_x %f, quat_w %f, energy %f\n",energy.n_conformation[i],move_x[i],quat_w[i],energy.energy[i]);
	//exit(0);
	#pragma omp parallel for 	
	for (unsigned int j=0;j<devices->n_devices;j++)
	{
		unsigned int th = omp_get_thread_num();
		unsigned int stride_d = devices->stride[j];
		unsigned long long int max_conformaciones = (devices->propiedades[j].maxGridSize[0] * (devices->hilos[th*metaheuristic.num_kernels] / WARP_SIZE)) * 8;
		
		//printf("Th %d j %d stride %d conformaciones %d max_conformaciones %lu\n",th,j,stride_d,devices->conformaciones[j],max_conformaciones);
		if (devices->conformaciones[j] < max_conformaciones)
			gpuSolver (ligando,proteina,param,metaheuristic,devices,move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy.energy+stride_d,energy.n_conformation+stride_d,weights,f_params,devices->conformaciones[j],j,stride_d,&kernels);		
		else
		{
			//DIVIDIR EN TROZOS
			while(devices->conformaciones[j] % max_conformaciones != 0) max_conformaciones --;
			printf("va a trocear\n");			
			for (unsigned int desplazamiento=0;desplazamiento<devices->conformaciones[j];desplazamiento+=max_conformaciones)
			{
				gpuSolver (ligando,proteina,param,metaheuristic,devices,move_x+stride_d+desplazamiento,move_y+stride_d+desplazamiento,move_z+stride_d+desplazamiento,quat_x+stride_d+desplazamiento,quat_y+stride_d+desplazamiento,quat_z+stride_d+desplazamiento,quat_w+stride_d+desplazamiento,energy.energy+stride_d+desplazamiento,energy.n_conformation+stride_d+desplazamiento,weights,f_params,max_conformaciones,j,stride_d+desplazamiento,&kernels);
				//printf("Device %d con trozo %d\n",devices->id[th],desplazamiento);				
			}
		}
	}
	tiempo_f = wtime() - tiempo_i;
	
}

extern void multigpuSolver_mejora (struct ligand_t ligando, struct receptor_t proteina, unsigned int surface, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int fase)
{
	double tiempo_i, tiempo_f;
	unsigned int  pc_temp, dev, stride_temp,ttotal = 0;
	
	pc_temp = nconformations / surface;
	devices->stride[0] = 0;	
	/*stride_temp = (unsigned int)((devices->trabajo[0] * 0.01)*surface);	
	devices->conformaciones[0] = stride_temp * pc_temp;
	ttotal = stride_temp;
	for (unsigned int i=(devices->n_devices - 1);i > 0;i--)
	{
		stride_temp = (unsigned int)((devices->trabajo[i] * 0.01)*surface);
		devices->conformaciones[i] = stride_temp * pc_temp;	
		ttotal += stride_temp;		
	}
	devices->conformaciones[0] += (surface - ttotal)*pc_temp;
	
	for (unsigned int i=(devices->n_devices-1);i > 0;i--)
	{
		devices->stride[i] = nconformations - devices->conformaciones[i];
		nconformations = nconformations - devices->conformaciones[i];		
	}	
	for (unsigned int i = 0;i<devices->n_devices;i++)*/
	 for (int i=0;i < devices->n_devices;i++)
        {
                stride_temp = floor((devices->trabajo[i] * 0.01)*nconformations);
                devices->conformaciones[i] = stride_temp;
                ttotal += stride_temp;
        }
        int diferencia = nconformations - ttotal;
        for (int i=0;i<diferencia;i++)
        {
                dev = i % devices->n_devices;
                devices->conformaciones[dev] ++;
        }
        for (int i=(devices->n_devices-1);i > 0;i--)
        {
                devices->stride[i] = nconformations - devices->conformaciones[i];
                nconformations = nconformations - devices->conformaciones[i];
        }
	tiempo_i = wtime();
	omp_set_num_threads(devices->n_devices);
	
	#pragma omp parallel for 	
	for (unsigned int j=0;j<devices->n_devices;j++)
	{
		unsigned int th = omp_get_thread_num();
		unsigned int stride_d = devices->stride[j];
		//int stride_d = th * stride;		
		//printf("Th %d j %d stride %d\n",th,j,stride_d);	
		if (param.montecarlo)
		{
			unsigned long long int max_conformaciones_1 = (devices->propiedades[j].maxGridSize[0] * (devices->hilos[th*metaheuristic.num_kernels] / WARP_SIZE)) * 8;
		
			//printf("Th %d j %d stride %d conformaciones %d max_conformaciones %lu\n",th,j,stride_d,devices->conformaciones[j],max_conformaciones_1);
			if (devices->conformaciones[j] < max_conformaciones_1)
				multigpu_mejorar_montecarlo (proteina,ligando,param,weights,f_params,devices,move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,nconfs+stride_d,metaheuristic,devices->conformaciones[j],j,stride_d,fase);	
			else
			{
				//DIVIDIR EN TROZOS
				while(devices->conformaciones[j] % max_conformaciones_1 != 0) max_conformaciones_1 --;
				printf("va a trocear\n");			
				for (unsigned int desplazamiento=0;desplazamiento<devices->conformaciones[j];desplazamiento+=max_conformaciones_1)
				{
					multigpu_mejorar_montecarlo (proteina,ligando,param,weights,f_params,devices,move_x+stride_d+desplazamiento,move_y+stride_d+desplazamiento,move_z+stride_d+desplazamiento,quat_x+stride_d+desplazamiento,quat_y+stride_d+desplazamiento,quat_z+stride_d+desplazamiento,quat_w+stride_d+desplazamiento,energy+stride_d+desplazamiento,nconfs+stride_d+desplazamiento,metaheuristic,max_conformaciones_1,j,stride_d+desplazamiento,fase);
					//printf("Device %d con trozo %d\n",devices->id[th],desplazamiento);	
				}
			}
		}
		else
		{	
			unsigned long long int max_conformaciones_2 = (devices->propiedades[j].maxGridSize[0] * (devices->hilos[j*metaheuristic.num_kernels] / WARP_SIZE)) * 8;
			//max_conformaciones_2 = 240000;	
			//printf("Th %d j %d stride %d conformaciones %d max_conformaciones %lu\n",th,j,stride_d,devices->conformaciones[j],max_conformaciones_2);
			if (devices->conformaciones[j] < max_conformaciones_2)
				multigpu_mejorar_gpusolver (proteina,ligando,param,weights,f_params,devices,move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,nconfs+stride_d,metaheuristic,devices->conformaciones[j],j,stride_d,fase);	
			else
			{
				//DIVIDIR EN TROZOS
				while(devices->conformaciones[j] % max_conformaciones_2 != 0) max_conformaciones_2 --;
				printf("va a trocear\n");			
				for (unsigned int desplazamiento=0;desplazamiento<devices->conformaciones[j];desplazamiento+=max_conformaciones_2)
				{
					multigpu_mejorar_gpusolver (proteina,ligando,param,weights,f_params,devices,move_x+stride_d+desplazamiento,move_y+stride_d+desplazamiento,move_z+stride_d+desplazamiento,quat_x+stride_d+desplazamiento,quat_y+stride_d+desplazamiento,quat_z+stride_d+desplazamiento,quat_w+stride_d+desplazamiento,energy+stride_d+desplazamiento,nconfs+stride_d+desplazamiento,metaheuristic,max_conformaciones_2,j,stride_d+desplazamiento,fase);	
					//printf("Device %d con trozo %d\n",devices->id[th],desplazamiento);	
				}
			}
		}
	}
	tiempo_f = wtime() - tiempo_i;
	
}


extern void multigpuSolver_mejora_environment (struct ligand_t ligando, struct receptor_t proteina, unsigned int surface, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int fase)
{
	struct vectors_t vectors_mejora;
	double tiempo_i, tiempo_f;
        unsigned int  pc_temp, dev, stride_temp,ttotal = 0;

	unsigned int n_conf_temp = nconformations;
        pc_temp = nconformations / surface;
        devices->stride[0] = 0;
        /*stride_temp = (unsigned int)((devices->trabajo[0] * 0.01)*surface);
        devices->conformaciones[0] = stride_temp * pc_temp;
        ttotal = stride_temp;
        for (unsigned int i=(devices->n_devices - 1);i > 0;i--)
        {
                stride_temp = (unsigned int)((devices->trabajo[i] * 0.01)*surface);
                devices->conformaciones[i] = stride_temp * pc_temp;
                ttotal += stride_temp;
        }
        devices->conformaciones[0] += (surface - ttotal)*pc_temp;

        for (unsigned int i=(devices->n_devices-1);i > 0;i--)
        {
                devices->stride[i] = nconformations - devices->conformaciones[i];
                nconformations = nconformations - devices->conformaciones[i];
        }
        for (unsigned int i = 0;i<devices->n_devices;i++)*/
	 for (int i=0;i < devices->n_devices;i++)
        {
                stride_temp = floor((devices->trabajo[i] * 0.01)*nconformations);
                devices->conformaciones[i] = stride_temp;
                ttotal += stride_temp;
        }
        int diferencia = nconformations - ttotal;
        for (int i=0;i<diferencia;i++)
        {
                dev = i % devices->n_devices;
                devices->conformaciones[dev] ++;
        }
        for (int i=(devices->n_devices-1);i > 0;i--)
        {
                devices->stride[i] = nconformations - devices->conformaciones[i];
                nconformations = nconformations - devices->conformaciones[i];
        }
        tiempo_i = wtime();
	nconformations = n_conf_temp;

	vectors_mejora.num_surface = surface;
    	vectors_mejora.nconformations = nconformations * metaheuristic.NEEImp;

    	vectors_mejora.move_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    	vectors_mejora.move_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    	vectors_mejora.move_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    	vectors_mejora.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    	vectors_mejora.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    	vectors_mejora.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    	vectors_mejora.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    	vectors_mejora.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations);
    	vectors_mejora.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    	vectors_mejora.weights = (type_data *)malloc(sizeof(type_data)*SCORING_TERMS);

    	memcpy(vectors_mejora.f_params, f_params,MAXTYPES * sizeof(struct force_field_param_t));
    	memcpy(vectors_mejora.weights, weights,SCORING_TERMS * sizeof(type_data));
    
    	thrust::sequence(vectors_mejora.energy.n_conformation,vectors_mejora.energy.n_conformation + vectors_mejora.nconformations);

    	fill_conformations_environment (move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,nconformations,param,metaheuristic);
	
        omp_set_num_threads(devices->n_devices);

        #pragma omp parallel for
		for (unsigned int j=0;j<devices->n_devices;j++)
	        {
        	        unsigned int th = omp_get_thread_num();
                	unsigned int stride_d = devices->stride[j];
                	//int stride_d = th * stride;
                	//printf("Th %d j %d stride %d\n",th,j,stride_d);
                	if (param.montecarlo)
                	{
                        	unsigned long long int max_conformaciones_1 = (devices->propiedades[j].maxGridSize[0] * (devices->hilos[th*metaheuristic.num_kernels] / WARP_SIZE)) * 8;

                        	//printf("Th %d j %d stride %d conformaciones %d max_conformaciones %lu\n",th,j,stride_d,devices->conformaciones[j],max_conformaciones_1);
                        	if (devices->conformaciones[j] < max_conformaciones_1)
                                	multigpu_mejorar_montecarlo_environment (proteina,ligando,param,weights,f_params,devices,move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,nconfs+stride_d,vectors_mejora.move_x+(stride_d*metaheuristic.NEEImp),vectors_mejora.move_y+(stride_d*metaheuristic.NEEImp),vectors_mejora.move_z+(stride_d*metaheuristic.NEEImp),vectors_mejora.quat_x+(stride_d*metaheuristic.NEEImp),vectors_mejora.quat_y+(stride_d*metaheuristic.NEEImp),vectors_mejora.quat_z+(stride_d*metaheuristic.NEEImp),vectors_mejora.quat_w+(stride_d*metaheuristic.NEEImp),vectors_mejora.energy.energy+(stride_d*metaheuristic.NEEImp),vectors_mejora.energy.n_conformation+(stride_d*metaheuristic.NEEImp),metaheuristic,devices->conformaciones[j],devices->conformaciones[j]*metaheuristic.NEEImp,j,stride_d,fase);
                        	else
                        	{
                                	//DIVIDIR EN TROZOS
                                	while(devices->conformaciones[j] % max_conformaciones_1 != 0) max_conformaciones_1 --;
                                	printf("va a trocear\n");
                                	for (unsigned int desplazamiento=0;desplazamiento<devices->conformaciones[j];desplazamiento+=max_conformaciones_1)
					{
                                        	multigpu_mejorar_montecarlo_environment (proteina,ligando,param,weights,f_params,devices,move_x+stride_d+desplazamiento,move_y+stride_d+desplazamiento,move_z+stride_d+desplazamiento,quat_x+stride_d+desplazamiento,quat_y+stride_d+desplazamiento,quat_z+stride_d+desplazamiento,quat_w+stride_d+desplazamiento,energy+stride_d+desplazamiento,nconfs+stride_d+desplazamiento,vectors_mejora.move_x+((stride_d+desplazamiento)*metaheuristic.NEEImp),vectors_mejora.move_y+((stride_d+desplazamiento)*metaheuristic.NEEImp),vectors_mejora.move_z+((stride_d+desplazamiento)*metaheuristic.NEEImp),vectors_mejora.quat_x+((stride_d+desplazamiento)*metaheuristic.NEEImp),vectors_mejora.quat_y+((stride_d+desplazamiento)*metaheuristic.NEEImp),vectors_mejora.quat_z+((stride_d+desplazamiento)*metaheuristic.NEEImp),vectors_mejora.quat_w+((stride_d+desplazamiento)*metaheuristic.NEEImp),vectors_mejora.energy.energy+((stride_d+desplazamiento)*metaheuristic.NEEImp),vectors_mejora.energy.n_conformation+((stride_d+desplazamiento)*metaheuristic.NEEImp),metaheuristic,max_conformaciones_1,max_conformaciones_1*metaheuristic.NEEImp,j,stride_d+desplazamiento,fase);
                                        	//printf("Device %d con trozo %d\n",devices->id[th],desplazamiento);
                                	}
                        	}
                	}
                	else
                	{
				unsigned long long int max_conformaciones_2 = (devices->propiedades[j].maxGridSize[0] * (devices->hilos[j*metaheuristic.num_kernels] / WARP_SIZE)) * 8;
                        //max_conformaciones_2 = 240000;
                        //printf("Th %d j %d stride %d conformaciones %d max_conformaciones %lu\n",th,j,stride_d,devices->conformaciones[j],max_conformaciones_2);
                        	if (devices->conformaciones[j] < max_conformaciones_2)
                                	multigpu_mejorar_gpusolver_environment (proteina,ligando,param,weights,f_params,devices,move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,nconfs+stride_d,vectors_mejora.move_x+(stride_d*metaheuristic.NEEImp),vectors_mejora.move_y+(stride_d*metaheuristic.NEEImp),vectors_mejora.move_z+(stride_d*metaheuristic.NEEImp),vectors_mejora.quat_x+(stride_d*metaheuristic.NEEImp),vectors_mejora.quat_y+(stride_d*metaheuristic.NEEImp),vectors_mejora.quat_z+(stride_d*metaheuristic.NEEImp),vectors_mejora.quat_w+(stride_d*metaheuristic.NEEImp),vectors_mejora.energy.energy+(stride_d*metaheuristic.NEEImp),vectors_mejora.energy.n_conformation+(stride_d*metaheuristic.NEEImp),metaheuristic,devices->conformaciones[j],devices->conformaciones[j]*metaheuristic.NEEImp,j,stride_d,fase);
                       		else
                        	{
                                	//DIVIDIR EN TROZOS
                                	while(devices->conformaciones[j] % max_conformaciones_2 != 0) max_conformaciones_2 --;
                                	printf("va a trocear\n");
                                	for (unsigned int desplazamiento=0;desplazamiento<devices->conformaciones[j];desplazamiento+=max_conformaciones_2)
                                	{
                                        	multigpu_mejorar_gpusolver_environment (proteina,ligando,param,weights,f_params,devices,move_x+stride_d+desplazamiento,move_y+stride_d+desplazamiento,move_z+stride_d+desplazamiento,quat_x+stride_d+desplazamiento,quat_y+stride_d+desplazamiento,quat_z+stride_d+desplazamiento,quat_w+stride_d+desplazamiento,energy+stride_d+desplazamiento,nconfs+stride_d+desplazamiento,vectors_mejora.move_x+((stride_d+desplazamiento)*metaheuristic.NEEImp),vectors_mejora.move_y+((stride_d+desplazamiento)*metaheuristic.NEEImp),vectors_mejora.move_z+((stride_d+desplazamiento)*metaheuristic.NEEImp),vectors_mejora.quat_x+((stride_d+desplazamiento)*metaheuristic.NEEImp),vectors_mejora.quat_y+((stride_d+desplazamiento)*metaheuristic.NEEImp),vectors_mejora.quat_z+((stride_d+desplazamiento)*metaheuristic.NEEImp),vectors_mejora.quat_w+((stride_d+desplazamiento)*metaheuristic.NEEImp),vectors_mejora.energy.energy+((stride_d+desplazamiento)*metaheuristic.NEEImp),vectors_mejora.energy.n_conformation+((stride_d+desplazamiento)*metaheuristic.NEEImp),metaheuristic,max_conformaciones_2,max_conformaciones_2*metaheuristic.NEEImp,j,stride_d+desplazamiento,fase);
                                        	//printf("Device %d con trozo %d\n",devices->id[th],desplazamiento);
                                	}
                        	}
                	}
        }
        tiempo_f = wtime() - tiempo_i;
	free(vectors_mejora.move_x);
	free(vectors_mejora.move_y);
	free(vectors_mejora.move_z);
	free(vectors_mejora.quat_x);
	free(vectors_mejora.quat_y);
	free(vectors_mejora.quat_z);
	free(vectors_mejora.quat_w);
	free(vectors_mejora.energy.energy);
	free(vectors_mejora.energy.n_conformation);
	free(vectors_mejora.weights);
}

extern void mejorar_multigpu (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, struct vectors_t *vectors_selec, struct metaheuristic_t metaheuristic, struct device_t *devices,unsigned int fase)
{
		
	multigpuSolver_mejora (ligando,proteina,vectors_selec->num_surface,vectors_selec->move_x,vectors_selec->move_y,vectors_selec->move_z,vectors_selec->quat_x,vectors_selec->quat_y,vectors_selec->quat_z,vectors_selec->quat_w,vectors_selec->energy.energy,vectors_selec->energy.n_conformation,vectors_selec->weights,vectors_selec->f_params,vectors_selec->nconformations,param,metaheuristic,devices,fase);				
					
}

extern void mejorar_multigpu_environment (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, struct vectors_t *vectors_selec, struct metaheuristic_t metaheuristic, struct device_t *devices,unsigned int fase)
{

        multigpuSolver_mejora_environment (ligando,proteina,vectors_selec->num_surface,vectors_selec->move_x,vectors_selec->move_y,vectors_selec->move_z,vectors_selec->quat_x,vectors_selec->quat_y,vectors_selec->quat_z,vectors_selec->quat_w,vectors_selec->energy.energy,vectors_selec->energy.n_conformation,vectors_selec->weights,vectors_selec->f_params,vectors_selec->nconformations,param,metaheuristic,devices,fase);

}
