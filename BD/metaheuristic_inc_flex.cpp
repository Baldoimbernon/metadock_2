#include <thrust/sequence.h>
#include <thrust/sort.h>
#include <thrust/functional.h>
#include <omp.h>
#include "energy_common.h"
#include "energy_common_vs.h"
#include "energy_cuda.h"
#include "metaheuristic_comb.h"
#include "metaheuristic_ini.h"
#include "energy_positions.h"
#include "vector_types.h"
extern void colocar_flex_by_step (struct param_t param, struct metaheuristic_t metaheuristic, unsigned int nlig,  type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int stride, unsigned int pos)
{
	struct vectors_t *vtmp = (struct vectors_t *)malloc(sizeof(struct vectors_t));
        unsigned int nc,nc_conf_o,nc_conf_d;

        vtmp->nconformations = stride;
        vtmp->conformations_x = (type_data *)malloc(sizeof(type_data)*nlig*stride);
        vtmp->conformations_y = (type_data *)malloc(sizeof(type_data)*nlig*stride);
        vtmp->conformations_z = (type_data *)malloc(sizeof(type_data)*nlig*stride);
        vtmp->move_x = (type_data *)malloc(sizeof(type_data)*stride);
        vtmp->move_y = (type_data *)malloc(sizeof(type_data)*stride);
        vtmp->move_z = (type_data *)malloc(sizeof(type_data)*stride);
        vtmp->quat_x = (type_data *)malloc(sizeof(type_data)*stride);
        vtmp->quat_y = (type_data *)malloc(sizeof(type_data)*stride);
        vtmp->quat_z = (type_data *)malloc(sizeof(type_data)*stride);
        vtmp->quat_w = (type_data *)malloc(sizeof(type_data)*stride);
        vtmp->energy.energy = (type_data *)malloc(sizeof(type_data)*stride);

        for (unsigned int i=0; i < stride;i++)
        {
                nc = nconfs[i] - (pos * stride);
                nc_conf_o = nc * nlig;
                nc_conf_d = i * nlig;

                memcpy(vtmp->conformations_x + nc_conf_d,conformations_x + nc_conf_o, nlig * sizeof(type_data));
                memcpy(vtmp->conformations_y + nc_conf_d,conformations_y + nc_conf_o, nlig * sizeof(type_data));
                memcpy(vtmp->conformations_z + nc_conf_d,conformations_z + nc_conf_o, nlig * sizeof(type_data));

                vtmp->move_x[i] = move_x[nc];
                vtmp->move_y[i] = move_y[nc];
                vtmp->move_z[i] = move_z[nc];
                vtmp->quat_x[i] = quat_x[nc];
                vtmp->quat_y[i] = quat_y[nc];
                vtmp->quat_z[i] = quat_z[nc];
                vtmp->quat_w[i] = quat_w[nc];
                vtmp->energy.energy[i] = energy[i];
        }
        memcpy(conformations_x, vtmp->conformations_x, stride * nlig * sizeof(type_data));
        memcpy(conformations_y, vtmp->conformations_y, stride * nlig * sizeof(type_data));
        memcpy(conformations_z, vtmp->conformations_z, stride * nlig * sizeof(type_data));
        memcpy(move_x, vtmp->move_x, stride * sizeof(type_data));
        memcpy(move_y, vtmp->move_y, stride * sizeof(type_data));
        memcpy(move_z, vtmp->move_z, stride * sizeof(type_data));
        memcpy(quat_x, vtmp->quat_x, stride * sizeof(type_data));
        memcpy(quat_y, vtmp->quat_y, stride * sizeof(type_data));
        memcpy(quat_z, vtmp->quat_z, stride * sizeof(type_data));
        memcpy(quat_w, vtmp->quat_w, stride * sizeof(type_data));
        memcpy(energy, vtmp->energy.energy, stride * sizeof(type_data));
	
	free(vtmp->conformations_x);
        free(vtmp->conformations_y);
        free(vtmp->conformations_z);
        free(vtmp->move_x);
        free(vtmp->move_y);
        free(vtmp->move_z);
        free(vtmp->quat_x);
        free(vtmp->quat_y);
        free(vtmp->quat_z);
        free(vtmp->quat_w);
        free(vtmp->energy.energy);
}

extern void colocar_flex (unsigned int nlig, struct param_t param, struct vectors_t *vectors_e_s, struct metaheuristic_t metaheuristic)
{
	struct vectors_t *vtmp = (struct vectors_t *)malloc(sizeof(struct vectors_t));
	unsigned int nc,nc_conf_o,nc_conf_d;
	unsigned int th1;
	
	vtmp->num_surface = vectors_e_s->num_surface;
	vtmp->nconformations = vectors_e_s->nconformations;
	vtmp->conformations_x = (type_data *)malloc(sizeof(type_data)*nlig*vectors_e_s->nconformations);
	vtmp->conformations_y = (type_data *)malloc(sizeof(type_data)*nlig*vectors_e_s->nconformations);
	vtmp->conformations_z = (type_data *)malloc(sizeof(type_data)*nlig*vectors_e_s->nconformations);
	vtmp->move_x = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);
	vtmp->move_y = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);
	vtmp->move_z = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);
	vtmp->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);	
	vtmp->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);	
	vtmp->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);	
	vtmp->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);		
	vtmp->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 0:
		case 3:
		case 2:
			th1 = metaheuristic.Threads1Inc;
			break;
	}		
	omp_set_num_threads (th1);
	//#pragma omp parallel for		
	for (unsigned int i=0; i<vectors_e_s->nconformations;i++)
	{
		nc = vectors_e_s->energy.n_conformation[i];
		//if (nc == 7653)  
		//{
		//	printf("n_conf %d vectors_e_s->energy %f\n", nc,vectors_e_s->energy.energy[i]);
		//	printf("1º atomo nc %d %f %f %f\n",nc,vectors_e_s->conformations_x[nc*nlig],vectors_e_s->conformations_y[nc*nlig],vectors_e_s->conformations_z[nc*nlig]);
		//}
		nc_conf_o = nc * nlig;				
		nc_conf_d = i * nlig;				
			
		memcpy(vtmp->conformations_x + nc_conf_d,vectors_e_s->conformations_x + nc_conf_o, nlig * sizeof(type_data));
		memcpy(vtmp->conformations_y + nc_conf_d,vectors_e_s->conformations_y + nc_conf_o, nlig * sizeof(type_data));
		memcpy(vtmp->conformations_z + nc_conf_d,vectors_e_s->conformations_z + nc_conf_o, nlig * sizeof(type_data));
		//if (nc == 7653)
                //{
		//	 printf("1º atomo i %d %f %f %f\n",i,vtmp->conformations_x[i*nlig],vtmp->conformations_y[i*nlig],vtmp->conformations_z[i*nlig]);
		//}
		vtmp->move_x[i] = vectors_e_s->move_x[nc];
		vtmp->move_y[i] = vectors_e_s->move_y[nc];
		vtmp->move_z[i] = vectors_e_s->move_z[nc];
		vtmp->quat_x[i] = vectors_e_s->quat_x[nc];
		vtmp->quat_y[i] = vectors_e_s->quat_y[nc];
		vtmp->quat_z[i] = vectors_e_s->quat_z[nc];
		vtmp->quat_w[i] = vectors_e_s->quat_w[nc];
		vtmp->energy.energy[i] = vectors_e_s->energy.energy[i];
	}

	//printf("%f %f %f\n",vtmp->move_x[0],vtmp->move_y[0],vtmp->move_z[0]);
	//memset(vectors_e_s->conformations_x,0, vectors_e_s->nconformations * nlig);
	//memset(vectors_e_s->conformations_y,0, vectors_e_s->nconformations * nlig);
	//memset(vectors_e_s->conformations_z,0, vectors_e_s->nconformations * nlig);
	memcpy(vectors_e_s->conformations_x, vtmp->conformations_x, vectors_e_s->nconformations * nlig * sizeof(type_data));  	
	memcpy(vectors_e_s->conformations_y, vtmp->conformations_y, vectors_e_s->nconformations * nlig * sizeof(type_data));  	
	memcpy(vectors_e_s->conformations_z, vtmp->conformations_z, vectors_e_s->nconformations * nlig * sizeof(type_data));  	
	memcpy(vectors_e_s->move_x, vtmp->move_x, vectors_e_s->nconformations * sizeof(type_data));  	
	memcpy(vectors_e_s->move_y, vtmp->move_y, vectors_e_s->nconformations * sizeof(type_data)); 
	memcpy(vectors_e_s->move_z, vtmp->move_z, vectors_e_s->nconformations * sizeof(type_data));
	memcpy(vectors_e_s->quat_x, vtmp->quat_x, vectors_e_s->nconformations * sizeof(type_data));
	memcpy(vectors_e_s->quat_y, vtmp->quat_y, vectors_e_s->nconformations * sizeof(type_data));
	memcpy(vectors_e_s->quat_z, vtmp->quat_z, vectors_e_s->nconformations * sizeof(type_data));
	memcpy(vectors_e_s->quat_w, vtmp->quat_w, vectors_e_s->nconformations * sizeof(type_data));
	memcpy(vectors_e_s->energy.energy, vtmp->energy.energy, vectors_e_s->nconformations * sizeof(type_data));
	thrust::sequence(vectors_e_s->energy.n_conformation,vectors_e_s->energy.n_conformation + vectors_e_s->nconformations);
        
	//if (nc == 7653)
          //      {
        //                 printf("1º atomo i %f %f %f\n",vectors_e_s->conformations_x[7616*nlig],vectors_e_s->conformations_y[7616*nlig],vectors_e_s->conformations_z[7616*nlig]);
        //        }
	free(vtmp->conformations_x);
	free(vtmp->conformations_y);
	free(vtmp->conformations_z);
	free(vtmp->move_x);
	free(vtmp->move_y);
	free(vtmp->move_z);
	free(vtmp->quat_x);
	free(vtmp->quat_y);
	free(vtmp->quat_z);
	free(vtmp->quat_w);
	free(vtmp->energy.energy);
}

extern void incluir_flex (unsigned int nlig, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param, type_data *max, unsigned int *improve, unsigned int *conformacion)
{
	unsigned int conf, th1;	
	type_data tmp_max;
	struct vectors_t vtmp;
	unsigned int total_included = metaheuristic.NEMInc_selec + (metaheuristic.NEFIni - metaheuristic.NEMInc_selec);	
	metaheuristic.NEPInc_selec = metaheuristic.NEFIni - metaheuristic.NEMInc_selec;
	
	unsigned int stride_e = vectors_e->nconformations / vectors_e->num_surface;
	unsigned int stride_s = vectors_s->nconformations / vectors_s->num_surface;
	
	unsigned int stride_t = stride_e +  stride_s; 	
	vtmp.num_surface = vectors_e->num_surface;
	
	vtmp.nconformations = vectors_e->num_surface * stride_t;
	vtmp.conformations_x = (type_data *)malloc(sizeof(type_data)*nlig*vtmp.nconformations);
	vtmp.conformations_y = (type_data *)malloc(sizeof(type_data)*nlig*vtmp.nconformations);
	vtmp.conformations_z = (type_data *)malloc(sizeof(type_data)*nlig*vtmp.nconformations);
	vtmp.move_x = (type_data *)malloc(sizeof(type_data)*vtmp.nconformations);
	vtmp.move_y = (type_data *)malloc(sizeof(type_data)*vtmp.nconformations);
	vtmp.move_z = (type_data *)malloc(sizeof(type_data)*vtmp.nconformations);
	vtmp.quat_x = (type_data *)malloc(sizeof(type_data)*vtmp.nconformations);	
	vtmp.quat_y = (type_data *)malloc(sizeof(type_data)*vtmp.nconformations);	
	vtmp.quat_z = (type_data *)malloc(sizeof(type_data)*vtmp.nconformations);	
	vtmp.quat_w = (type_data *)malloc(sizeof(type_data)*vtmp.nconformations);	
	vtmp.energy.energy = (type_data *)malloc(sizeof(type_data)*vtmp.nconformations);
	vtmp.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vtmp.nconformations);
	thrust::sequence(vtmp.energy.n_conformation,vtmp.energy.n_conformation + vtmp.nconformations);
	
	switch (param.mode) {		
		//case 0:	
		case 1:
			th1 = 1;
			break;
	  	case 0:		
		case 2:
		case 3:	
			th1 = metaheuristic.Threads1Inc;
			break;
	}
	omp_set_num_threads(th1);
	#pragma omp parallel for
	for (unsigned int i = 0;i < vectors_s->num_surface;i++)
	{
		memcpy(vtmp.conformations_x+ i*nlig*(stride_e +  stride_s), vectors_s->conformations_x + i*nlig*stride_s, stride_s * nlig * sizeof(type_data)); 
		memcpy(vtmp.conformations_y+ i*nlig*(stride_e +  stride_s), vectors_s->conformations_y + i*nlig*stride_s, stride_s * nlig * sizeof(type_data)); 
		memcpy(vtmp.conformations_z+ i*nlig*(stride_e +  stride_s), vectors_s->conformations_z + i*nlig*stride_s, stride_s * nlig * sizeof(type_data)); 
		memcpy(vtmp.move_x + i*stride_t, vectors_s->move_x + i*stride_s, stride_s * sizeof(type_data)); 
		memcpy(vtmp.move_y + i*stride_t, vectors_s->move_y + i*stride_s, stride_s * sizeof(type_data)); 
		memcpy(vtmp.move_z + i*stride_t, vectors_s->move_z + i*stride_s, stride_s * sizeof(type_data)); 
		memcpy(vtmp.quat_x + i*stride_t, vectors_s->quat_x + i*stride_s, stride_s * sizeof(type_data)); 
		memcpy(vtmp.quat_y + i*stride_t, vectors_s->quat_y + i*stride_s, stride_s * sizeof(type_data)); 
		memcpy(vtmp.quat_z + i*stride_t, vectors_s->quat_z + i*stride_s, stride_s * sizeof(type_data)); 
		memcpy(vtmp.quat_w + i*stride_t, vectors_s->quat_w + i*stride_s, stride_s * sizeof(type_data)); 
		memcpy(vtmp.energy.energy + i*stride_t, vectors_s->energy.energy + i*stride_s, stride_s * sizeof(type_data)); 
		
		memcpy(vtmp.conformations_x+ i*nlig*(stride_e +  stride_s) + nlig*stride_s, vectors_e->conformations_x + i*nlig*stride_e, stride_e * nlig * sizeof(type_data));  			
		memcpy(vtmp.conformations_y+ i*nlig*(stride_e +  stride_s) + nlig*stride_s, vectors_e->conformations_y + i*nlig*stride_e, stride_e * nlig * sizeof(type_data));  			
		memcpy(vtmp.conformations_z+ i*nlig*(stride_e +  stride_s) + nlig*stride_s, vectors_e->conformations_z + i*nlig*stride_e, stride_e * nlig * sizeof(type_data));	
		memcpy(vtmp.move_x + i*stride_t + stride_s, vectors_e->move_x + i*stride_e, stride_e * sizeof(type_data));  			
		memcpy(vtmp.move_y + i*stride_t + stride_s, vectors_e->move_y + i*stride_e, stride_e * sizeof(type_data));  			
		memcpy(vtmp.move_z + i*stride_t + stride_s, vectors_e->move_z + i*stride_e, stride_e * sizeof(type_data));  			
		memcpy(vtmp.quat_x + i*stride_t + stride_s, vectors_e->quat_x + i*stride_e, stride_e * sizeof(type_data));  
		memcpy(vtmp.quat_y + i*stride_t + stride_s, vectors_e->quat_y + i*stride_e, stride_e * sizeof(type_data));  	
		memcpy(vtmp.quat_z + i*stride_t + stride_s, vectors_e->quat_z + i*stride_e, stride_e * sizeof(type_data));  	
		memcpy(vtmp.quat_w + i*stride_t + stride_s, vectors_e->quat_w + i*stride_e, stride_e * sizeof(type_data));  			
		memcpy(vtmp.energy.energy + i*stride_t + stride_s, vectors_e->energy.energy + i*stride_e, stride_e * sizeof(type_data));  					
	}
	
	#pragma omp parallel for
	for (unsigned int i = 0; i < vtmp.nconformations; i += stride_t)			
		thrust::stable_sort_by_key((vtmp.energy.energy + i),(vtmp.energy.energy + i + stride_t), (vtmp.energy.n_conformation + i), thrust::less<type_data>());			
	//colocar_flex(nlig,param,&vtmp,metaheuristic);
	#pragma omp parallel for
        for (unsigned int i = 0; i < vtmp.num_surface; i++)
                colocar_flex_by_step (param,metaheuristic,nlig,vtmp.conformations_x + (stride_t*nlig*i), vtmp.conformations_y + (stride_t*nlig*i), vtmp.conformations_z + (stride_t*nlig*i), vtmp.move_x + (stride_t*i),vtmp.move_y + (stride_t*i),vtmp.move_z + (stride_t*i),vtmp.quat_x + (stride_t*i),vtmp.quat_y + (stride_t*i),vtmp.quat_z + (stride_t*i),vtmp.quat_w + (stride_t*i),vtmp.energy.energy + (stride_t*i),vtmp.energy.n_conformation + (stride_t*i),stride_t,i);
        thrust::sequence(vtmp.energy.n_conformation,vtmp.energy.n_conformation + vtmp.nconformations);
	
	free(vectors_s->conformations_x); free(vectors_s->conformations_y); free(vectors_s->conformations_z);	
	free(vectors_s->move_x); free(vectors_s->move_y); free(vectors_s->move_z); free(vectors_s->quat_x); free(vectors_s->quat_y); free(vectors_s->quat_z); free(vectors_s->quat_w);
	free(vectors_s->energy.n_conformation); free(vectors_s->energy.energy);
	
	vectors_s->num_surface = vectors_e->num_surface;
	vectors_s->nconformations = vectors_s->num_surface * total_included;

	//printf("Número de conformaciones: %d\n",vectors_s->nconformations);
	vectors_s->conformations_x = (type_data *)malloc(sizeof(type_data)* nlig * vectors_s->nconformations);
	vectors_s->conformations_y = (type_data *)malloc(sizeof(type_data)* nlig * vectors_s->nconformations);
	vectors_s->conformations_z = (type_data *)malloc(sizeof(type_data)* nlig * vectors_s->nconformations);

	vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nconformations);
	vectors_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	
	#pragma omp parallel for 
	for (int i = 0;i < vectors_s->num_surface;i++)
	{		

		memcpy(vectors_s->conformations_x + i*nlig*total_included, vtmp.conformations_x + i*nlig*stride_t, metaheuristic.NEMInc_selec * nlig * sizeof(type_data));  	
		memcpy(vectors_s->conformations_y + i*nlig*total_included, vtmp.conformations_y + i*nlig*stride_t, metaheuristic.NEMInc_selec * nlig * sizeof(type_data));  	
		memcpy(vectors_s->conformations_z + i*nlig*total_included, vtmp.conformations_z + i*nlig*stride_t, metaheuristic.NEMInc_selec * nlig * sizeof(type_data));  	

		memcpy(vectors_s->move_x + i*total_included, vtmp.move_x + i*stride_t, metaheuristic.NEMInc_selec * sizeof(type_data));  	
		memcpy(vectors_s->move_y + i*total_included, vtmp.move_y + i*stride_t, metaheuristic.NEMInc_selec * sizeof(type_data)); 
		memcpy(vectors_s->move_z + i*total_included, vtmp.move_z + i*stride_t, metaheuristic.NEMInc_selec * sizeof(type_data));
		memcpy(vectors_s->quat_x + i*total_included, vtmp.quat_x + i*stride_t, metaheuristic.NEMInc_selec * sizeof(type_data));
		memcpy(vectors_s->quat_y + i*total_included, vtmp.quat_y + i*stride_t, metaheuristic.NEMInc_selec * sizeof(type_data));
		memcpy(vectors_s->quat_z + i*total_included, vtmp.quat_z + i*stride_t, metaheuristic.NEMInc_selec * sizeof(type_data));
		memcpy(vectors_s->quat_w + i*total_included, vtmp.quat_w + i*stride_t, metaheuristic.NEMInc_selec * sizeof(type_data));
		memcpy(vectors_s->energy.energy + i*total_included, vtmp.energy.energy + i*stride_t, metaheuristic.NEMInc_selec * sizeof(type_data));										
	
		memcpy(vectors_s->conformations_x + i*nlig*total_included + metaheuristic.NEMInc_selec*nlig, vtmp.conformations_x + i*nlig*stride_t + (stride_t - metaheuristic.NEPInc_selec)*nlig, metaheuristic.NEPInc_selec * nlig * sizeof(type_data));  	
		memcpy(vectors_s->conformations_y + i*nlig*total_included + metaheuristic.NEMInc_selec*nlig, vtmp.conformations_y + i*nlig*stride_t + (stride_t - metaheuristic.NEPInc_selec)*nlig, metaheuristic.NEPInc_selec * nlig * sizeof(type_data));  	
		memcpy(vectors_s->conformations_z + i*nlig*total_included + metaheuristic.NEMInc_selec*nlig, vtmp.conformations_z + i*nlig*stride_t + (stride_t - metaheuristic.NEPInc_selec)*nlig, metaheuristic.NEPInc_selec * nlig * sizeof(type_data));  	
				
		memcpy(vectors_s->move_x + i*total_included + metaheuristic.NEMInc_selec, vtmp.move_x + i*stride_t + (stride_t - metaheuristic.NEPInc_selec), metaheuristic.NEPInc_selec * sizeof(type_data));  	
		memcpy(vectors_s->move_y + i*total_included + metaheuristic.NEMInc_selec, vtmp.move_y + i*stride_t + (stride_t - metaheuristic.NEPInc_selec), metaheuristic.NEPInc_selec * sizeof(type_data)); 
		memcpy(vectors_s->move_z + i*total_included + metaheuristic.NEMInc_selec, vtmp.move_z + i*stride_t + (stride_t - metaheuristic.NEPInc_selec), metaheuristic.NEPInc_selec * sizeof(type_data));
		memcpy(vectors_s->quat_x + i*total_included + metaheuristic.NEMInc_selec, vtmp.quat_x + i*stride_t + (stride_t - metaheuristic.NEPInc_selec), metaheuristic.NEPInc_selec * sizeof(type_data));
		memcpy(vectors_s->quat_y + i*total_included + metaheuristic.NEMInc_selec, vtmp.quat_y + i*stride_t + (stride_t - metaheuristic.NEPInc_selec), metaheuristic.NEPInc_selec * sizeof(type_data));
		memcpy(vectors_s->quat_z + i*total_included + metaheuristic.NEMInc_selec, vtmp.quat_z + i*stride_t + (stride_t - metaheuristic.NEPInc_selec), metaheuristic.NEPInc_selec * sizeof(type_data));
		memcpy(vectors_s->quat_w + i*total_included + metaheuristic.NEMInc_selec, vtmp.quat_w + i*stride_t + (stride_t - metaheuristic.NEPInc_selec), metaheuristic.NEPInc_selec * sizeof(type_data));
		memcpy(vectors_s->energy.energy + i*total_included + metaheuristic.NEMInc_selec, vtmp.energy.energy + i*stride_t + (stride_t - metaheuristic.NEPInc_selec), metaheuristic.NEPInc_selec * sizeof(type_data));	
	}
	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);	

	tmp_max = *max;
	
	for (unsigned int j = 0; j < vectors_s->num_surface; j++)			
		if ((vectors_s->energy.energy[j*total_included] < tmp_max) && (vectors_s->energy.energy[j*total_included] > MAX_ENERGY))
		{					
			tmp_max = vectors_s->energy.energy[j*total_included];
			conf = j*total_included;
		}
	if (tmp_max == *max)  
		*improve=*improve+1; 
	else	
	{ 
		*improve=0; 
		*max = tmp_max; 			
		*conformacion = conf;	
	}
//	for(int k=0;k<vectors_e->nconformations;k++)
//                printf("e-number INC %d, m_x %f, m_y %f, m_z %f, energy %f\n",k,vectors_e->move_x[k],vectors_e->move_y[k],vectors_e->move_z[k],vectors_e->energy.energy[k]);
//for(int k=0;k<vectors_s->nconformations;k++)
  //              printf("s-number INC %d, m_x %f, m_y %f, m_z %f, energy %f\n",k,vectors_s->move_x[k],vectors_s->move_y[k],vectors_s->move_z[k],vectors_s->energy.energy[k]);
	free(vtmp.conformations_x);
	free(vtmp.conformations_y);
	free(vtmp.conformations_z);
	free(vtmp.move_x);
	free(vtmp.move_y);
	free(vtmp.move_z);
	free(vtmp.quat_x);
	free(vtmp.quat_y);
	free(vtmp.quat_z);
	free(vtmp.quat_w);
	free(vtmp.energy.energy);
	free(vtmp.energy.n_conformation);
}

extern void incluir_warm_up_flex (int nlig, struct vectors_t *vectors_e, struct param_t param, unsigned int hilos)
{
	struct vectors_t vectors_s;
	type_data tmp_1 = param.por_conf_warm_up_inc * param.conf_warm_up_cpu * 0.01;
	unsigned int tmp = (unsigned int) tmp_1;

	vectors_s.num_surface = vectors_e->num_surface;
	vectors_s.nconformations = vectors_s.num_surface * tmp;
	vectors_s.conformations_x = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s.nconformations);
	vectors_s.conformations_y = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s.nconformations);
	vectors_s.conformations_z = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s.nconformations);
			
	vectors_s.move_x = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.move_y = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.move_z = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s.nconformations);
	vectors_s.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	
	omp_set_num_threads (hilos);
	#pragma omp parallel for 	
	for (int i = 0;i < vectors_s.num_surface;i++)
	{	
		memcpy(vectors_s.conformations_x + i*nlig*tmp, vectors_e->conformations_x + i*nlig*param.conf_warm_up_cpu, tmp * nlig* sizeof(type_data));  	
		memcpy(vectors_s.conformations_y + i*nlig*tmp, vectors_e->conformations_y + i*nlig*param.conf_warm_up_cpu, tmp * nlig* sizeof(type_data));  	
		memcpy(vectors_s.conformations_z + i*nlig*tmp, vectors_e->conformations_z + i*nlig*param.conf_warm_up_cpu, tmp * nlig* sizeof(type_data));  			
		
		memcpy(vectors_s.move_x + i*tmp, vectors_e->move_x + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));  	
		memcpy(vectors_s.move_y + i*tmp, vectors_e->move_y + i*param.conf_warm_up_cpu, tmp * sizeof(type_data)); 
		memcpy(vectors_s.move_z + i*tmp, vectors_e->move_z + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.quat_x + i*tmp, vectors_e->quat_x + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.quat_y + i*tmp, vectors_e->quat_y + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.quat_z + i*tmp, vectors_e->quat_z + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.quat_w + i*tmp, vectors_e->quat_w + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.energy.energy + i*tmp, vectors_e->energy.energy + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));

		memcpy(vectors_s.conformations_x + i*nlig*tmp, vectors_e->conformations_x + i*nlig*param.conf_warm_up_cpu, tmp * nlig* sizeof(type_data));  	
		memcpy(vectors_s.conformations_y + i*nlig*tmp, vectors_e->conformations_y + i*nlig*param.conf_warm_up_cpu, tmp * nlig* sizeof(type_data));  	
		memcpy(vectors_s.conformations_z + i*nlig*tmp, vectors_e->conformations_z + i*nlig*param.conf_warm_up_cpu, tmp * nlig* sizeof(type_data));  			
		
		memcpy(vectors_s.move_x + i*tmp, vectors_e->move_x + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));  	
		memcpy(vectors_s.move_y + i*tmp, vectors_e->move_y + i*param.conf_warm_up_cpu, tmp * sizeof(type_data)); 
		memcpy(vectors_s.move_z + i*tmp, vectors_e->move_z + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.quat_x + i*tmp, vectors_e->quat_x + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.quat_y + i*tmp, vectors_e->quat_y + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.quat_z + i*tmp, vectors_e->quat_z + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.quat_w + i*tmp, vectors_e->quat_w + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.energy.energy + i*tmp, vectors_e->energy.energy + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));						
	}
	thrust::sequence(vectors_s.energy.n_conformation,vectors_s.energy.n_conformation + vectors_s.nconformations);
	
	free(vectors_s.conformations_x);
	free(vectors_s.conformations_y);
	free(vectors_s.conformations_z);
	free(vectors_s.move_x);
	free(vectors_s.move_y);
	free(vectors_s.move_z);
	free(vectors_s.quat_x);
	free(vectors_s.quat_y);
	free(vectors_s.quat_z);
	free(vectors_s.quat_w);
	free(vectors_s.energy.energy);
	free(vectors_s.energy.n_conformation);
}
