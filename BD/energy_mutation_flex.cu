#include <omp.h>
#include "wtime.h"
#include "energy_GpuSolver.h"
#include "energy_kernel.h"
#include "energy_moving.h"
#include "energy_common.h"
#include "energy_common-gpu.h"
#include "energy_mutation.h"
#include "energy_montecarlo.h"
#include "energy_rotation.h"
#include "energy_positions_flex.h"
#include "energy_flexibility.h"

__global__ void mutation_random_flex (curandState_t * states, unsigned int *n_update, unsigned long int max)
{	
	//OBTENEMOS LA CONFORMACION
	unsigned long int tid = threadIdx.x + threadIdx.y * blockDim.x;
	unsigned int blockID = blockIdx.x + blockIdx.y * gridDim.x;
	unsigned long int id_conformation = blockID * (blockDim.x * blockDim.y) + (threadIdx.y*blockDim.x) + threadIdx.x;
	//int id_conformation;
	if (id_conformation < max)
	{
		//ENLACE Y FRAGMENTO A GIRAR ALEATORIO
		n_update[id_conformation] =  getRandomNumber(&states[id_conformation]) % 5;
	}

}

__global__ void mutation_kernel_flex (curandState_t * states, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, unsigned int *n_update, type_data max_desp, type_data rotation, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, unsigned int nlig, unsigned int n_links, unsigned int n_fragments, int *links_d, int *links_fragments_d, int *fragments_d, int *fragments_tam_d, unsigned long int max)
{
	curandState_t random_state;
	type_data a[3],b[3],eje[3],p1[3];
    type_data ang;
    unsigned int num_point, frag_act, position_link;
    unsigned int rot_point, link;
    type_data qeje_x, qeje_y, qeje_z, qeje_w;
	unsigned int tid = threadIdx.x + threadIdx.y * blockDim.x;
	unsigned int blockID = blockIdx.x + blockIdx.y * gridDim.x;
	unsigned long int id_conformation = blockID * (blockDim.x * blockDim.y) + (threadIdx.y*blockDim.x) + threadIdx.x;
	type_data local_x, local_y, local_z, local_w, quaternion_x, quaternion_y, quaternion_z, quaternion_w;
	type_data angle;
	
	//int id_conformation;
	if (id_conformation < max)
	{
		switch (n_update[id_conformation]) 
		{
				case 0:
					//MOVE X
					random_state = states[id_conformation];		
					move_x[id_conformation] += getRandomNumber_double(&random_state, max_desp); //Desplazamiento entre -max_desp y max_desp
					states[id_conformation] = random_state;
					break;
				case 1:
					//MOVE Y
					random_state = states[id_conformation];							
					move_y[id_conformation] += getRandomNumber_double(&random_state, max_desp);
					states[id_conformation] = random_state;
					break;
				case 2:
					//MOVE Z
					random_state = states[id_conformation];		
					move_z[id_conformation] += getRandomNumber_double(&random_state, max_desp);
					states[id_conformation] = random_state;
					break;
				case 3:
					//ROTATION
					random_state = states[id_conformation];
	
					quaternion_x = quat_x[id_conformation];
					quaternion_y = quat_y[id_conformation];
					quaternion_z = quat_z[id_conformation];
					quaternion_w = quat_w[id_conformation];

					angle = getRandomNumber_double(&random_state, rotation);
					eje[0] = getRandomNumber_double(&random_state, 1);
					eje[1] = getRandomNumber_double(&random_state, 1);
					eje[2] = getRandomNumber_double(&random_state, 1);

					setRotation (&local_x, &local_y, &local_z, &local_w, angle, eje);
					composeRotation (&local_x, &local_y, &local_z, &local_w, &quaternion_x, &quaternion_y, &quaternion_z, &quaternion_w, &quaternion_x, &quaternion_y, &quaternion_z, &quaternion_w);

					normalize(&quaternion_x,&quaternion_y,&quaternion_z,&quaternion_w);
	
					quat_x[id_conformation] = quaternion_x;
					quat_y[id_conformation] = quaternion_y;
					quat_z[id_conformation] = quaternion_z;
					quat_w[id_conformation] = quaternion_w;
		
					states[id_conformation] = random_state;
					break;
				case 4:
					//FLEX
					type_data t_p = getRandomNumber_double (&states[id_conformation], rotation);
					//ENLACE Y FRAGMENTO A GIRAR ALEATORIO
					unsigned int link =  getRandomNumber(&states[id_conformation]) % n_links;
					//OBTENEMOS EL PUNTO DEL ENLACE DESDE DONDE ROTAR
					unsigned int rot_point = getRandomNumber(&states[id_conformation]) % 2;
					unsigned long int position_conformation = id_conformation * nlig;
					position_link = link;

					//EJE DE GIRO
					a[0] = conformations_x[position_conformation + (links_d[2 * position_link] - 1)];
					a[1] = conformations_y[position_conformation + (links_d[2 * position_link] - 1)];
					a[2] = conformations_z[position_conformation + (links_d[2 * position_link] - 1)];
					b[0] = conformations_x[position_conformation + (links_d[(2 * position_link)+1] - 1)];
					b[1] = conformations_y[position_conformation + (links_d[(2 * position_link)+1] - 1)];
					b[2] = conformations_z[position_conformation + (links_d[(2 * position_link)+1] - 1)];

					if (rot_point == 0){
							eje[0] = b[0] - a[0];
							eje[1] = b[1] - a[1];
							eje[2] = b[2] - a[2];
							p1[0] = conformations_x[position_conformation + (links_d[2 * position_link] - 1)];
							p1[1] = conformations_y[position_conformation + (links_d[2 * position_link] - 1)];
							p1[2] = conformations_z[position_conformation + (links_d[2 * position_link] - 1)];
							num_point = (2 * position_link) + 1;
					}else {
							eje[0] = a[0] - b[0];
							eje[1] = a[1] - b[1];
							eje[2] = a[2] - b[2];
							p1[0] = conformations_x[position_conformation + (links_d[(2 * position_link)+1] - 1)];
							p1[1] = conformations_y[position_conformation + (links_d[(2 * position_link)+1] - 1)];
							p1[2] = conformations_z[position_conformation + (links_d[(2 * position_link)+1] - 1)];
							num_point = 2 * position_link;
					}

					//CONFIGURACION EJE DE ROTACION COMO QUATERNION
					setRotation_flexibility(&qeje_x,&qeje_y,&qeje_z,&qeje_w,ang,eje);					
					unsigned int shift_fragment = 0;
					//ROTAMOS TODOS LOS FRAGMENTOS QUE TENGAN COMO ORIGEN EL PUNTO DE ROTACION					
					frag_act = links_fragments_d[num_point];
					for (unsigned int i2 = 0; i2 < (frag_act - 1); i2++)
                        			shift_fragment = shift_fragment + fragments_tam_d[i2];
					rotateLigandFragment((fragments_d + shift_fragment),fragments_tam_d[frag_act-1],(conformations_x + position_conformation), (conformations_y + position_conformation), (conformations_z + position_conformation), qeje_x, qeje_y, qeje_z, qeje_w, p1);
					break;
		}
	}
}

extern void mutation_gpu_calculation_flex (struct ligand_t ligando, struct receptor_t proteina, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, struct flexibility_params_t *flexibility_params, unsigned int nconformations,unsigned int orden_device, unsigned int stride)
{
	curandState_t *states_d;
	cudaError_t cudaStatus;
	type_data *quat_d_x, *quat_d_y, *quat_d_z, *quat_d_w;
	type_data *move_x_d, *move_y_d, *move_z_d;
	unsigned int *n_update,ngpu;
	int *links_d, *links_fragments_d, *fragments_d, *fragments_tam_d;
    type_data *conformations_x_d, *conformations_y_d, *conformations_z_d;
	
	unsigned int randSize = nconformations * sizeof(curandState_t);
	unsigned int moveSize = nconformations * sizeof(int);
	unsigned int moveSize_f = nconformations * sizeof(type_data);
	

	//cudaEvent_t start_m, stop_m, start_k, stop_k;
	type_data time_transfer = 0;
        type_data memory_in, memory_out, kernels = 0, tmp;	

	ngpu = devices->id[orden_device];
	cudaSetDevice(ngpu);
	cudaDeviceReset();
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) printf("Error al principio de mutacion\n");
	
	unsigned int blk_t = ceil(nconformations / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
	dim3 grid_t  (ceil(blk_t/8)+1, 8);	
	dim3 hilos_t (devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);	
	unsigned int max_t = nconformations;	
	
	dataToGPUEnergy(nconformations, move_x, move_y, move_z, quat_x, quat_y, quat_z, quat_w, move_x_d, move_y_d, move_z_d, quat_d_x, quat_d_y, quat_d_z, quat_d_w);
	dataToGPUFlexibilityParameters_mutation(nconformations,ligando.atoms,links_d,flexibility_params->links,fragments_d,flexibility_params->fragments,links_fragments_d,flexibility_params->links_fragments,fragments_tam_d,flexibility_params->fragments_tam,flexibility_params->n_links,flexibility_params->n_fragments);

	dataToGPUConformations(nconformations,ligando.nlig,conformations_x,conformations_x_d,conformations_y,conformations_y_d,conformations_z,conformations_z_d);
	

	cudaStatus = cudaMalloc((void**) &states_d, randSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc states_d values failed!");
	cudaStatus = cudaMalloc((void**) &n_update, moveSize);
	
	
	setupCurandState <<< (nconformations/devices->hilos[orden_device*metaheuristic.num_kernels+4])+1, devices->hilos[orden_device*metaheuristic.num_kernels+4] >>>  (states_d, param.automatic_seed, param.seed, nconformations);
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel setupCurandState failed \n");
	
	mutation_random <<<grid_t,hilos_t>>> (states_d,n_update,max_t);
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel mutation_random failed \n");
	//exit(0);	
	mutation_kernel_flex <<<grid_t,hilos_t>>> (states_d,conformations_x_d,conformations_y_d,conformations_z_d,n_update,param.max_desp,param.rotation_mu,move_x_d,move_y_d,move_z_d,quat_d_x,quat_d_y,quat_d_z,quat_d_w,ligando.nlig,flexibility_params->n_links,flexibility_params->n_fragments,links_d,links_fragments_d,fragments_d,fragments_tam_d,max_t);																																			
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel mutation failed \n");

    cudaStatus = cudaMemcpy(move_x, move_x_d, moveSize_f, cudaMemcpyDeviceToHost);
    if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
    cudaStatus = cudaMemcpy(move_y, move_y_d, moveSize_f, cudaMemcpyDeviceToHost);
    if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
    cudaStatus = cudaMemcpy(move_z, move_z_d, moveSize_f, cudaMemcpyDeviceToHost);
    if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
    //exit(0);
    cudaStatus = cudaMemcpy(quat_x, quat_d_x, moveSize_f, cudaMemcpyDeviceToHost);
    if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
    cudaStatus = cudaMemcpy(quat_y, quat_d_y, moveSize_f, cudaMemcpyDeviceToHost);
    if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
    cudaStatus = cudaMemcpy(quat_z, quat_d_z, moveSize_f, cudaMemcpyDeviceToHost);
    if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
    cudaStatus = cudaMemcpy(quat_w, quat_d_w, moveSize_f, cudaMemcpyDeviceToHost);
    if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");


	cudaFree(conformations_x_d);
    cudaFree(conformations_y_d);
    cudaFree(conformations_z_d);


    cudaFree(links_d);
    cudaFree(links_fragments_d);
    cudaFree(fragments_d);
    cudaFree(fragments_tam_d);
		
	cudaFree(move_x_d);
    cudaFree(move_y_d);
    cudaFree(move_z_d);
    cudaFree(quat_d_x);
    cudaFree(quat_d_y);
    cudaFree(quat_d_z);
    cudaFree(quat_d_w);
    cudaFree(states_d);
	cudaFree(n_update);
    cudaDeviceReset();
		
	gpuSolver_flex(ligando,proteina,param,metaheuristic,devices,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,weights,f_params,flexibility_params->n_links,nconformations,orden_device,stride,flexibility_params->individual_bonds_VDW,flexibility_params->individual_bonds_ES,&kernels);
	//exit(0);
	if (metaheuristic.IMEMUCom > 0)
	{
		if (param.montecarlo)
			multigpu_mejorar_montecarlo_flex (proteina,ligando,param,weights,f_params,devices,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,metaheuristic,flexibility_params,nconformations,orden_device,stride,2);
		else
			multigpu_mejorar_gpusolver_flex (proteina,ligando,param,weights,f_params,devices,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,metaheuristic,nconformations,orden_device,stride,flexibility_params,2);
			
	}
	//printf("Fin mutacion\n");
	//exit(0);
	
}


extern void mutation_gpu_flex (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, type_data *weights, struct force_field_param_t *f_params, struct flexibility_params_t *flexibility_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *n_conformation, unsigned int nconformations, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int orden_device, unsigned int stride) 
{
	size_t available,total_m;
    cudaError_t cudaStatus;
    unsigned long int max_memory;
    unsigned long int max_positions;
    unsigned long int max_common;
    
    unsigned long int typeSize = proteina.nrec * sizeof(char);
    unsigned long int recSize = proteina.nrec * sizeof(type_data);
    unsigned long int bondSize = proteina.nrec * sizeof(int) * MAXBOND;	    
    unsigned long int randSize = nconformations * sizeof(curandState_t);	
    unsigned long int moveSize_tam = nconformations * sizeof(type_data);
	
	unsigned long int confSize_tam = nconformations * ligando.nlig * sizeof(type_data);
	unsigned long int linksSize = flexibility_params->n_links * 2 * sizeof(int);
    unsigned long int linksfragmentsSize = flexibility_params->n_links * 2 * sizeof(int);
    unsigned long int fragmentstamSize = flexibility_params->n_fragments * sizeof(int);
    unsigned long int fragmentsSize = flexibility_params->n_links * (ligando.atoms - 2) * sizeof(int);
	unsigned long int boolSize = ligando.atoms * 2 * sizeof(bool);
    
    unsigned long int tam_max, tam_tmp, nconformations_count_total, nconformations_count_partial, tam_conformation, stride_d;
    type_data memory_in, memory_out, kernels = 0, tmp;

    max_positions = moveSize_tam * 7 + randSize + confSize_tam * 3;
    max_common = (recSize * 4) + typeSize + bondSize + linksSize + linksfragmentsSize + fragmentstamSize + fragmentsSize + boolSize; 
    max_memory = max_positions;
    tam_conformation =  (sizeof(type_data) * 7) + sizeof(curandState_t) + 3 * sizeof(type_data);

	cudaSetDevice(devices->id[orden_device]);
    //cudaDeviceReset();
    cudaMemGetInfo(&tam_max, &total_m);
    cudaDeviceSynchronize();
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
    //std::cout << tam_max << " " << total_m << "\n";
    tam_tmp = tam_max - (tam_max * 0.2);

	if (tam_tmp < max_memory)
        {
                //printf("Trocear en 1 GPU\n");
                max_memory = max_common;
                nconformations_count_total = 0;
                nconformations_count_partial = 0;
                stride_d = 0;
                while (nconformations_count_total < nconformations)
                {
                        max_memory += tam_conformation;
                        if (max_memory > tam_tmp)
                        {
                                //printf("Trozo %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
                                //nconformations_count_total--;
                                //nconformations_count_partial--;
                                //Lanzar con nconformations_count_partial;							
								mutation_gpu_calculation_flex (ligando,proteina,param,metaheuristic,devices,weights,f_params,conformations_x + (stride_d*ligando.nlig), conformations_y + (stride_d*ligando.nlig), conformations_z + (stride_d*ligando.nlig), move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,n_conformation+stride_d,flexibility_params,nconformations_count_partial,orden_device,stride+stride_d);
                                stride_d += nconformations_count_partial;
                                max_memory = max_common;
                                nconformations_count_partial = 0;
                        }
                        else
                        {
                                nconformations_count_total++;
                                nconformations_count_partial++;
                        }
                }
                //printf("Trozo final %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);				
				nconformations_count_partial--;
				mutation_gpu_calculation_flex (ligando,proteina,param,metaheuristic,devices,weights,f_params,conformations_x + (stride_d*ligando.nlig), conformations_y + (stride_d*ligando.nlig), conformations_z + (stride_d*ligando.nlig), move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,n_conformation+stride_d,flexibility_params,nconformations_count_partial,orden_device,stride+stride_d);
        }

        else
        {
                //printf("No Trocea\n");
                //Lanzar con todo
				mutation_gpu_calculation_flex (ligando,proteina,param,metaheuristic,devices,weights,f_params,conformations_x , conformations_y , conformations_z , move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,n_conformation,flexibility_params,nconformations,orden_device,stride);				
        }	


}
