#ifndef ENERGY_FLEXIBILITY_H
#define ENERGY_FLEXIBILITY_H

#include "type.h"
#include "energy_cuda.h"

extern __device__ void rotate3D_flexibility (type_data  q_x, type_data  q_y, type_data  q_z, type_data  q_w, type_data* src, type_data * dst);
extern __device__ void setRotation_flexibility (type_data *q_x, type_data *q_y, type_data *q_z, type_data *q_w, type_data angle, type_data * vector);
extern __device__ void rotateLigandFragment(int *f, unsigned int size_f, type_data* lig_x, type_data* lig_y, type_data* lig_z, type_data qeje_x, type_data  qeje_y, type_data qeje_z, type_data qeje_w, type_data p1[3]);

__global__ void angulations_conformations (curandState_t * states, struct flexibility_data_t * flexibility_conformations, int n_links, type_data max_angle_flex, unsigned int max);
__global__ void update_angulations_conformations (curandState_t * states, struct flexibility_data_t * flexibility_conformations, type_data * energy_sig, type_data* energy_ant, unsigned int max);
__global__ void flexibilityKernel (type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, curandState_t * states, struct flexibility_data_t * flexibility_conformations, unsigned int nlig, int n_links, int n_fragments, int *links_d, int *links_fragments_d, int *fragments_d, int *fragments_tam_d, unsigned int max_angle_flex, unsigned int max);
extern void flexibility_function(struct ligand_t ligando, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, struct flexibility_params_t *flexibility_params, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, unsigned int orden_device);

#endif
