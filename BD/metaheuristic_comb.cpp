#include <omp.h>
#include "energy_common.h"
#include "energy_cuda.h"
#include "energy_positions.h"
#include "energy_GpuSolver.h"
#include "energy_CpuSolver.h"
#include "energy_CpuSolver_common.h"
#include "wtime.h"
#include "metaheuristic_mgpu.h"
#include "metaheuristic_inc.h"
#include "rotation.h"
#include <thrust/sequence.h>
#include <thrust/sort.h>
#include <thrust/functional.h>

////Devuelve un número aleatorio entre 1 y max
extern unsigned int getRandomNumber_modified (unsigned int n, unsigned int act)
{
	type_data tmp = act;
	while ((tmp == act ) || (tmp >= n))
	 tmp = (unsigned int)((n*2) * frand());
	return tmp;
}

void combinar_individual (unsigned int nlig, type_data *e_move_x, type_data *e_move_y, type_data *e_move_z, type_data *e_quat_x, type_data *e_quat_y, type_data *e_quat_z, type_data *e_quat_w, type_data *s_move_x, type_data *s_move_y, type_data *s_move_z, type_data *s_quat_x, type_data *s_quat_y, type_data *s_quat_z, type_data *s_quat_w , struct param_t param, unsigned int j, unsigned int k, unsigned int l)
{

	type_data local_x, local_y, local_z, local_w, quat_tmp_x, quat_tmp_y, quat_tmp_z, quat_tmp_w;
	type_data angle,eje[3];
	
	unsigned int pos_o = j;	
	unsigned int pos_d = k;
	unsigned int pos_s = l;

	s_move_x[pos_s] = (e_move_x[pos_o] + e_move_x[pos_d])/2;
	s_move_y[pos_s] = (e_move_y[pos_o] + e_move_y[pos_d])/2;
	s_move_z[pos_s] = (e_move_z[pos_o] + e_move_z[pos_d])/2;
	
	quat_tmp_x = e_quat_x[pos_o];
	quat_tmp_y = e_quat_y[pos_o];
	quat_tmp_z = e_quat_z[pos_o];
	quat_tmp_w = e_quat_w[pos_o];
	angle = getRealRandomNumber(param.rotation);
	eje[0] = getRealRandomNumber (1);
	eje[1] = getRealRandomNumber (1);
	eje[2] = getRealRandomNumber (1);
	setRotation_cpp (&local_x, &local_y, &local_z, &local_w, angle, eje);				
	composeRotation_cpp (&local_x, &local_y, &local_z, &local_w, &quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w, &quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w);
	normalize_cpp(&quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w);
	s_quat_x[pos_s] = quat_tmp_x;
	s_quat_y[pos_s] = quat_tmp_y;
	s_quat_z[pos_s] = quat_tmp_z;
	s_quat_w[pos_s] = quat_tmp_w;
}

extern void combinar_group (unsigned int nlig, type_data *e_move_x, type_data *e_move_y, type_data *e_move_z, type_data *e_quat_x, type_data *e_quat_y, type_data *e_quat_z, type_data *e_quat_w, type_data *s_move_x, type_data *s_move_y, type_data *s_move_z, type_data *s_quat_x, type_data *s_quat_y, type_data *s_quat_z, type_data *s_quat_w , struct metaheuristic_t metaheuristic, unsigned int mejores, unsigned int peores, unsigned int mejorespeores, unsigned int stride_e, unsigned int stride_s, struct param_t param)
{	
//int l = 0;
	unsigned int base = 0;
	unsigned int ind_m,ind_p,k,j;
	unsigned int div_m = metaheuristic.NMMCom_selec - 1;
	unsigned int div_p = metaheuristic.NPPCom_selec - 1;
	unsigned int div_mp = metaheuristic.NMPCom_selec - 1;
	unsigned int h2;	
	switch (param.mode) {
		
		case 1:
			h2 = 1;
			break;
		case 0:
		case 2:
		case 3:	
			h2 = 1;//metaheuristic.Threads2Com;
			break;
	}
	//omp_set_num_threads(h2);
	//#pragma omp parallel for private(j,k)
	if ((metaheuristic.NEMSel_selec > 0) && (mejores > 2))
	{
		for (unsigned int m = 0; m < mejores; m++)
		{
			j = m / div_m;
			k = (div_m * (j+1)) - m;
			if (k <= j) k--;
			//printf("l = %d, j = %d, k = %d\n",m,j,k);
			combinar_individual (nlig,e_move_x,e_move_y,e_move_z,e_quat_x,e_quat_y,e_quat_z,e_quat_w,s_move_x,s_move_y,s_move_z,s_quat_x,s_quat_y,s_quat_z,s_quat_w,param,j,k,m);
		}
	}
				
	//peores con peores
	if ((metaheuristic.NEPSel_selec > 0) && (peores > 2))
	{
		base =  metaheuristic.NMMCom_selec * ( metaheuristic.NMMCom_selec - 1);
		for (unsigned int j = 0; j < metaheuristic.NPPCom_selec; j++)
			 for (unsigned int k = 0; k < metaheuristic.NPPCom_selec; k++)
				if (k != j)
				{
					combinar_individual (nlig,e_move_x,e_move_y,e_move_z,e_quat_x,e_quat_y,e_quat_z,e_quat_w,s_move_x,s_move_y,s_move_z,s_quat_x,s_quat_y,s_quat_z,s_quat_w,param,j + metaheuristic.NEMSel_selec,k + metaheuristic.NEMSel_selec,base);	
					base++;
				}
	}
			
	//mejores con peores
	
	if ((metaheuristic.NEMSel_selec > 0) && (metaheuristic.NEPSel_selec > 0) && (mejorespeores > 2))
	{
		base = (metaheuristic.NMMCom_selec * ( metaheuristic.NMMCom_selec - 1)) + (metaheuristic.NPPCom_selec * ( metaheuristic.NPPCom_selec - 1));
		ind_m = 0;//getRandomNumber_modified(metaheuristic.NEMSel_selec-1,0);
		ind_p = 0;//getRandomNumber_modified(metaheuristic.NEPSel_selec-1,0);

		//#pragma omp parallel for private(ind_m,ind_p)
		for (unsigned int m = base; m < (mejorespeores + base); m++)
		{					
			//combinar_individual_vs_flex (nlig,vectors_e,vectors_s,metaheuristic,param,stride_e,stride_s,i,ind_m,ind_p+metaheuristic.NEMSel_selec,m);
			//printf("ind_m %d, ind_p %d, m %d\n",ind_m,ind_p,m);
			combinar_individual (nlig,e_move_x,e_move_y,e_move_z,e_quat_x,e_quat_y,e_quat_z,e_quat_w,s_move_x,s_move_y,s_move_z,s_quat_x,s_quat_y,s_quat_z,s_quat_w,param,ind_m,ind_p + metaheuristic.NEMSel_selec,m);	
			ind_m = (ind_m + 1) % metaheuristic.NMMCom_selec;
			ind_p = (ind_p + 1) % metaheuristic.NPPCom_selec;
			if ((ind_p % metaheuristic.NEPSel_selec) == 0) ind_p++;
			//printf("ind_m %d, ind_p %d m %d\n",ind_m,ind_p,m);
			//l++;						
		}
	}		
}

extern void combinar (struct receptor_t proteina, struct ligand_t ligando, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param)
{
	unsigned int mejores = 0, peores = 0, mejorespeores = 0;
	unsigned int stride_s, stride_e, th1;	
	type_data tcombinargroupcpu, tenergycombinarcpu;
	type_data tcombinargroupcpu_t, tenergycombinarcpu_t;
	
	//omp_set_nested(1);
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 0:
		case 3:
		case 2:
			th1 = metaheuristic.Threads1Com;
			break;
	}
						
	vectors_s->num_surface = vectors_e->num_surface;
			
	mejores = metaheuristic.NMMCom_selec * (metaheuristic.NMMCom_selec - 1);	
	mejorespeores = metaheuristic.NMPCom_selec * (metaheuristic.NMPCom_selec - 1);	
	peores = metaheuristic.NPPCom_selec * (metaheuristic.NPPCom_selec - 1);
	stride_s = mejores + mejorespeores + peores;
	//stride_e = metaheuristic.NEMSel_selec + metaheuristic.NEPSel_selec;
	stride_e = vectors_e->nconformations / vectors_e->num_surface;
	
	//printf("Mejores: %d %d, peores: %d %d, mejorespeores: %d %d, stride_s: %d \n",mejores,metaheuristic.NMMCom_selec,peores,metaheuristic.NPPCom_selec,mejorespeores,metaheuristic.NMPCom_selec,stride_s);
	vectors_s->nconformations = vectors_e->num_surface * stride_s;
	vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nconformations);
	vectors_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->weights = (type_data *)malloc(sizeof(type_data)*SCORING_TERMS);
	
	//memcpy(vectors_s->vdw_params, vectors_e->vdw_params,MAXTYPES * sizeof(struct vdw_param_t));			
	//memcpy(vectors_s->sasa_params, vectors_e->sasa_params,MAXTYPES * sizeof(struct sasa_param_t));	
	//memcpy(vectors_s->hbond_params, vectors_e->hbond_params,MAXTYPES * sizeof(struct hbond_param_t));
	memcpy(vectors_s->f_params, vectors_e->f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_s->weights, vectors_e->weights,SCORING_TERMS * sizeof(type_data));
	
	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
			
	tcombinargroupcpu = wtime();
	//omp_set_nested(1);
	//omp_set_num_threads (th1);
	//#pragma omp parallel for 		 			
	for (unsigned int i = 0; i < vectors_s->num_surface;i++)	
	{				
		combinar_group (ligando.nlig, (vectors_e->move_x + i*stride_e), (vectors_e->move_y + i*stride_e), (vectors_e->move_z + i*stride_e), (vectors_e->quat_x + i*stride_e), (vectors_e->quat_y + i*stride_e), (vectors_e->quat_z + i*stride_e), (vectors_e->quat_w + i*stride_e), (vectors_s->move_x + i*stride_s), (vectors_s->move_y + i*stride_s), (vectors_s->move_z + i*stride_s), (vectors_s->quat_x + i*stride_s), (vectors_s->quat_y + i*stride_s), (vectors_s->quat_z + i*stride_s), (vectors_s->quat_w + i*stride_s), metaheuristic, mejores, peores, mejorespeores, stride_e, stride_s, param);						
	}
	tcombinargroupcpu_t += wtime() - tcombinargroupcpu;			
	tenergycombinarcpu = wtime();		
				
	ForcesCpuSolver(param,ligando,proteina,vectors_s->move_x,vectors_s->move_y,vectors_s->move_z,vectors_s->quat_x, vectors_s->quat_y, vectors_s->quat_z, vectors_s->quat_w,vectors_s->energy.energy,vectors_s->energy.n_conformation,vectors_s->weights,vectors_s->f_params,vectors_s->nconformations,metaheuristic);	
	tenergycombinarcpu_t += wtime() - tenergycombinarcpu;
	#pragma omp parallel for			
	for (unsigned int i = 0; i < vectors_s->nconformations; i += stride_s)							
		thrust::stable_sort_by_key((vectors_s->energy.energy + i),(vectors_s->energy.energy + i + stride_s), (vectors_s->energy.n_conformation + i), thrust::less<type_data>());						
	//colocar(param,vectors_s,metaheuristic);	
	#pragma omp parallel for
	for (unsigned int i = 0; i < vectors_s->num_surface; i++)
		colocar_by_step (param,metaheuristic,ligando.nlig,vectors_s->move_x + (stride_s*i),vectors_s->move_y + (stride_s*i),vectors_s->move_z + (stride_s*i),vectors_s->quat_x + (stride_s*i),vectors_s->quat_y + (stride_s*i),vectors_s->quat_z + (stride_s*i),vectors_s->quat_w + (stride_s*i),vectors_s->energy.energy + (stride_s*i),vectors_s->energy.n_conformation + (stride_s*i),stride_s,i);
	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
}

extern void combinar_gpu (struct receptor_t proteina, struct ligand_t ligando, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param, struct device_t *devices, unsigned int orden_device)
{
	unsigned int mejores, peores, mejorespeores, mejores_tmp, peores_tmp, mejorespeores_tmp,stride_e,stride_s;
	type_data kernels=0;	
	//cudaError_t cudaStatus;	
	
	mejores_tmp = metaheuristic.NMMCom_selec;
	peores_tmp = metaheuristic.NPPCom_selec;
	mejorespeores_tmp = metaheuristic.NMPCom_selec;

	mejores = mejores_tmp * (mejores_tmp - 1);	
	mejorespeores = mejorespeores_tmp * (mejorespeores_tmp - 1);	
	peores = peores_tmp * (peores_tmp - 1);
	//printf("mejores %d, peores %d, mejorespeores %d\n",mejores,peores,mejorespeores);
			
	stride_s = mejores + mejorespeores + peores;
	//stride_e = metaheuristic.NEMSel_selec + metaheuristic.NEPSel_selec;
	stride_e = vectors_e->nconformations / vectors_e->num_surface;
	
	vectors_s->num_surface = vectors_e->num_surface;			
	vectors_s->nconformations = vectors_e->num_surface * stride_s;
	vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nconformations);
	vectors_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);	
	//memcpy(vectors_s->vdw_params, vectors_e->vdw_params,MAXTYPES * sizeof(struct vdw_param_t));				
	//memcpy(vectors_s->sasa_params, vectors_e->sasa_params,MAXTYPES * sizeof(struct sasa_param_t));				
	//memcpy(vectors_s->hbond_params, vectors_e->hbond_params,MAXTYPES * sizeof(struct hbond_param_t));
	vectors_s->weights = (type_data *)malloc(SCORING_TERMS*sizeof(type_data));
	memcpy(vectors_s->f_params, vectors_e->f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_s->weights, vectors_e->weights,SCORING_TERMS * sizeof(type_data));	
	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
	//printf("conformaciones %d\n",vectors_s->nconformations);
	omp_set_num_threads(metaheuristic.Threads1Com);
	#pragma omp parallel for	
	for (unsigned int i = 0; i < vectors_s->num_surface;i++)	
	{				
		combinar_group (ligando.nlig, (vectors_e->move_x + i*stride_e), (vectors_e->move_y + i*stride_e), (vectors_e->move_z + i*stride_e), (vectors_e->quat_x + i*stride_e), (vectors_e->quat_y + i*stride_e), (vectors_e->quat_z + i*stride_e), (vectors_e->quat_w + i*stride_e), (vectors_s->move_x + i*stride_s), (vectors_s->move_y + i*stride_s), (vectors_s->move_z + i*stride_s), (vectors_s->quat_x + i*stride_s), (vectors_s->quat_y + i*stride_s), (vectors_s->quat_z + i*stride_s), (vectors_s->quat_w + i*stride_s), metaheuristic, mejores, peores, mejorespeores, stride_e, stride_s, param);	
	}
	
	unsigned int max_conformaciones = ((devices->propiedades[orden_device].maxGridSize[0] * devices->hilos[orden_device*metaheuristic.num_kernels]) / WARP_SIZE);
	//max_conformaciones =500000;
	//printf("max_surface1d %d max %lu int %d\n",devices->propiedades[orden_device].maxSurface1D,max_conformaciones,sizeof(int));
	if (vectors_s->nconformations < max_conformaciones)
	{
			//printf("Combinado...voy a por el scoring\n");						
			gpuSolver (ligando,proteina,param,metaheuristic,devices,vectors_s->move_x,vectors_s->move_y,vectors_s->move_z,vectors_s->quat_x, vectors_s->quat_y, vectors_s->quat_z,vectors_s->quat_w, vectors_s->energy.energy,vectors_s->energy.n_conformation,vectors_s->weights,vectors_s->f_params,vectors_s->nconformations,0,0,&kernels);		
	}
	else
	{
		//DIVIDIR EN TROZOS
		while((vectors_s->nconformations % max_conformaciones) != 0) max_conformaciones --;		
		printf("va a trocear\n");		
		for (unsigned int desplazamiento=0;desplazamiento<vectors_s->nconformations;desplazamiento+=max_conformaciones)
		{
			gpuSolver (ligando,proteina,param,metaheuristic,devices,vectors_s->move_x+desplazamiento,vectors_s->move_y+desplazamiento,vectors_s->move_z+desplazamiento,vectors_s->quat_x+desplazamiento,vectors_s->quat_y+desplazamiento,vectors_s->quat_z+desplazamiento,vectors_s->quat_w+desplazamiento,vectors_s->energy.energy+desplazamiento,vectors_s->energy.n_conformation +desplazamiento,vectors_s->weights,vectors_s->f_params,max_conformaciones,0,desplazamiento,&kernels);
			//printf("Device %d con trozo %d\n",devices->id[0],desplazamiento);			
		}
	}

	
	omp_set_num_threads (metaheuristic.Threads1Com);
	#pragma omp parallel for			
	for (unsigned int i = 0; i < vectors_s->nconformations; i += stride_s)					
		thrust::stable_sort_by_key((vectors_s->energy.energy + i),(vectors_s->energy.energy + i + stride_s), (vectors_s->energy.n_conformation + i), thrust::less<type_data>());			
	//colocar(param,vectors_s,metaheuristic);	
	#pragma omp parallel for
        for (unsigned int i = 0; i < vectors_s->num_surface; i++)
                colocar_by_step (param,metaheuristic,ligando.nlig,vectors_s->move_x + (stride_s*i),vectors_s->move_y + (stride_s*i),vectors_s->move_z + (stride_s*i),vectors_s->quat_x + (stride_s*i),vectors_s->quat_y + (stride_s*i),vectors_s->quat_z + (stride_s*i),vectors_s->quat_w + (stride_s*i),vectors_s->energy.energy + (stride_s*i),vectors_s->energy.n_conformation + (stride_s*i),stride_s,i);
        thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
	

}	


extern void combinar_multigpu (struct receptor_t proteina, struct ligand_t ligando, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param, struct device_t *devices)
{
	unsigned int mejores, peores, mejorespeores, mejores_tmp, peores_tmp, mejorespeores_tmp,stride_e,stride_s;	
	//cudaError_t cudaStatus;	
	//printf("Entra..\n");
	//exit(0);	
	mejores_tmp = metaheuristic.NMMCom_selec;
	peores_tmp = metaheuristic.NPPCom_selec;
	mejorespeores_tmp = metaheuristic.NMPCom_selec;

	mejores = mejores_tmp * (mejores_tmp - 1);	
	mejorespeores = mejorespeores_tmp * (mejorespeores_tmp - 1);	
	peores = peores_tmp * (peores_tmp - 1);
	
			
	stride_s = mejores + mejorespeores + peores;
	stride_e = vectors_e->nconformations / vectors_e->num_surface;
	
	vectors_s->num_surface = vectors_e->num_surface;			
	vectors_s->nconformations = vectors_e->num_surface * stride_s;
	vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_x = (type_data *)calloc(sizeof(type_data),vectors_s->nconformations);
	vectors_s->quat_y = (type_data *)calloc(sizeof(type_data),vectors_s->nconformations);
	vectors_s->quat_z = (type_data *)calloc(sizeof(type_data),vectors_s->nconformations);
	vectors_s->quat_w = (type_data *)calloc(sizeof(type_data),vectors_s->nconformations);
	vectors_s->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nconformations);
	vectors_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);	
	//memcpy(vectors_s->vdw_params, vectors_e->vdw_params,MAXTYPES * sizeof(struct vdw_param_t));	
	//memcpy(vectors_s->sasa_params, vectors_e->sasa_params,MAXTYPES * sizeof(struct sasa_param_t));					
	//memcpy(vectors_s->hbond_params, vectors_e->hbond_params,MAXTYPES * sizeof(struct hbond_param_t));
	//exit(0);
	vectors_s->weights = (type_data *)malloc(SCORING_TERMS*sizeof(type_data));
	memcpy(vectors_s->f_params, vectors_e->f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_s->weights, vectors_e->weights,SCORING_TERMS * sizeof(type_data));
													
	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
    	//exit(0);		
	omp_set_num_threads(metaheuristic.Threads1Com);
	#pragma omp parallel for	
	for (unsigned int i = 0; i < vectors_s->num_surface;i++)	
	{				
		combinar_group (ligando.nlig, (vectors_e->move_x + i*stride_e), (vectors_e->move_y + i*stride_e), (vectors_e->move_z + i*stride_e), (vectors_e->quat_x + i*stride_e), (vectors_e->quat_y + i*stride_e), (vectors_e->quat_z + i*stride_e), (vectors_e->quat_w + i*stride_e), (vectors_s->move_x + i*stride_s), (vectors_s->move_y + i*stride_s), (vectors_s->move_z + i*stride_s), (vectors_s->quat_x + i*stride_s), (vectors_s->quat_y + i*stride_s), (vectors_s->quat_z + i*stride_s), (vectors_s->quat_w + i*stride_s), metaheuristic, mejores, peores, mejorespeores, stride_e, stride_s, param);	
	}
	//exit(0);		
	multigpuSolver (ligando,proteina,vectors_s->num_surface, vectors_s->move_x,vectors_s->move_y,vectors_s->move_z,vectors_s->quat_x, vectors_s->quat_y, vectors_s->quat_z, vectors_s->quat_w,vectors_s->energy,vectors_s->weights, vectors_s->f_params, vectors_s->nconformations, param, metaheuristic, devices);
	
	omp_set_num_threads (metaheuristic.Threads1Com);
	#pragma omp parallel for			
	for (unsigned int i = 0; i < vectors_s->nconformations; i += stride_s)			
		thrust::stable_sort_by_key((vectors_s->energy.energy + i),(vectors_s->energy.energy + i + stride_s), (vectors_s->energy.n_conformation + i), thrust::less<type_data>());						
	//colocar(param,vectors_s,metaheuristic);	
	#pragma omp parallel for
        for (unsigned int i = 0; i < vectors_s->num_surface; i++)
                colocar_by_step (param,metaheuristic,ligando.nlig,vectors_s->move_x + (stride_s*i),vectors_s->move_y + (stride_s*i),vectors_s->move_z + (stride_s*i),vectors_s->quat_x + (stride_s*i),vectors_s->quat_y + (stride_s*i),vectors_s->quat_z + (stride_s*i),vectors_s->quat_w + (stride_s*i),vectors_s->energy.energy + (stride_s*i),vectors_s->energy.n_conformation + (stride_s*i),stride_s,i);
        thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
	//for (int i=0;i<vectors_s->nconformations;i++) printf("id %d, %f %f %f %f %f %f %f\n",i,vectors_s->move_x[i],vectors_s->move_y[i],vectors_s->move_z[i],vectors_s->quat_x[i],vectors_s->quat_y[i],vectors_s->quat_z[i],vectors_s->energy.energy[i]);
}	



