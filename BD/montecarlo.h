#ifndef MONTECARLO_H_
#define MONTECARLO_H_
#include "energy_struct.h"

extern void montecarlo_cpp (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, unsigned int num_surface, type_data *weights, struct force_field_param_t *f_params, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int nconformations, struct metaheuristic_t metaheuristic, unsigned int opt);
extern void montecarlo_cpp_environment (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, unsigned int num_surface, type_data *weights, struct force_field_param_t *f_params, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int nconformations, struct metaheuristic_t metaheuristic, unsigned int opt);

extern void montecarlo_cpp_flex (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, unsigned int num_surface, type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int nconformations, struct metaheuristic_t metaheuristic, struct flexibility_params_t *flexibility_params, struct flexibility_data_t *flexibility_conformations, unsigned int opt);
extern void montecarlo_cpp_flex_environment (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, unsigned int num_surface, type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int nconformations, struct metaheuristic_t metaheuristic, struct flexibility_params_t *flexibility_params, struct flexibility_data_t *flexibility_conformations, unsigned int opt);


#endif
