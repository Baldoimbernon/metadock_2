#ifndef __ENERGY_MUTATION_CPP_H
#define __ENERGY_MUTATION_CPP_H
#include "energy_struct.h"

extern void mutation_cpp (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, struct metaheuristic_t metaheuristic, type_data *weights, struct force_field_param_t *f_params, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int num_surface, unsigned int nconformations);
extern void mutation_calculation_cpp (struct param_t param, struct metaheuristic_t metaheuristic, unsigned int *n_update, int seed, int automatic_seed, type_data max_desp, type_data rotation, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, unsigned int nconformations);
extern void mutation_random_cpp (struct metaheuristic_t metaheuristic, unsigned int nconformations, struct param_t param, unsigned int *n_update);

#endif
