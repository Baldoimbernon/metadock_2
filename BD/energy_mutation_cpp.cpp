#include <omp.h>
#include "wtime.h"
#include "type.h"
#include "energy_struct.h"
#include "energy_mutation.h"
#include "energy_CpuSolver.h"
#include "metaheuristic_ini.h"
#include "energy_mutation_cpp.h"
#include "energy_CpuSolver_common.h"
#include "montecarlo.h"
#include "rotation.h"


extern void mutation_cpp (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, struct metaheuristic_t metaheuristic, type_data *weights, struct force_field_param_t *f_params, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int num_surface, unsigned int nconformations)
{
	unsigned int *n_update;
	n_update = (unsigned int *)malloc(sizeof(unsigned int)*nconformations);
	mutation_random_cpp (metaheuristic,nconformations,param,n_update);
	mutation_calculation_cpp (param,metaheuristic,n_update,param.seed,param.automatic_seed,param.max_desp,param.rotation,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,nconformations);

	ForcesCpuSolver(param,ligando,proteina,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,weights,f_params,nconformations,metaheuristic);

	if (metaheuristic.IMEMUCom > 0)
        {
                if (param.montecarlo)
			montecarlo_cpp (proteina,ligando,param,num_surface,weights,f_params,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,nconformations,metaheuristic,2);
                else
			mejorar(proteina,ligando,param,num_surface,weights,f_params,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,nconformations,metaheuristic,2);            
        }

	free(n_update);

}
extern void mutation_calculation_cpp (struct param_t param, struct metaheuristic_t metaheuristic, unsigned int *n_update, int seed, int automatic_seed, type_data max_desp, type_data rotation, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, unsigned int nconformations)
{
 	unsigned int th1;
        type_data angle, eje[3];
        type_data local_x, quat_tmp_x, local_y, quat_tmp_y, local_z, quat_tmp_z, local_w, quat_tmp_w ;

        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 0:
                case 3:
                case 2:
                        th1 = metaheuristic.Threads1Ini;
                        break;
        }
        omp_set_num_threads (th1);
	#pragma omp parallel for private(quat_tmp_x,quat_tmp_y,quat_tmp_z,quat_tmp_w,eje,angle,local_x,local_y,local_z,local_w) 
        for (unsigned int id_conformation=0; id_conformation < nconformations; id_conformation++)
        {
                switch (n_update[id_conformation])
                {
                	case 0:
                        	//MOVE X
                        	move_x[id_conformation] += getRealRandomNumber_r(max_desp,automatic_seed,seed,id_conformation+seed); //Desplazamiento entre -max_desp y max_desp
                        	break;
                 	case 1:
                        	//MOVE Y
                                move_y[id_conformation] += getRealRandomNumber_r(max_desp,automatic_seed,seed,id_conformation+seed); //Desplazamiento entre -max_desp y max_desp
                                break;
			case 2:
                                //MOVE Z
                                move_z[id_conformation] += getRealRandomNumber_r(max_desp,automatic_seed,seed,id_conformation+seed); //Desplazamiento entre -max_desp y max_desp
                                break;
                        case 3:
                                //ROTATION
				quat_tmp_x = quat_x[id_conformation];
                		quat_tmp_y = quat_y[id_conformation];
                		quat_tmp_z = quat_z[id_conformation];
                		quat_tmp_w = quat_w[id_conformation];
                		angle = getRealRandomNumber(param.rotation);
                		eje[0] = getRealRandomNumber (1);
                		eje[1] = getRealRandomNumber (1);
                		eje[2] = getRealRandomNumber (1);
                		setRotation_cpp (&local_x, &local_y, &local_z, &local_w, angle, eje);
                		composeRotation_cpp (&local_x, &local_y, &local_z, &local_w, &quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w, &quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w);
                		normalize_cpp(&quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w );
                		quat_x[id_conformation] = quat_tmp_x;
                		quat_y[id_conformation] = quat_tmp_y;
                		quat_z[id_conformation] = quat_tmp_z;
                		quat_w[id_conformation] = quat_tmp_w;
				break;
		}	
	}

}

extern void mutation_random_cpp (struct metaheuristic_t metaheuristic, unsigned int nconformations, struct param_t param, unsigned int *n_update)
{
        unsigned int th1;

        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 0:
                case 3:
                case 2:
                        th1 = metaheuristic.Threads1Ini;
                        break;
        }
        omp_set_num_threads (th1);
        #pragma omp parallel 
        for (unsigned int id_conformation=0; id_conformation < nconformations; id_conformation++)
        {
                n_update[id_conformation] =  getIntNumber(10000) % 4;
        }
}

extern void mutation_multigpu (struct ligand_t ligando, struct receptor_t proteina, unsigned int surface, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int fase)
{

	unsigned int  pc_temp, stride_temp,ttotal = 0, dev;
        //pc_temp = nconformations / surface;
        devices->stride[0] = 0;
        //stride_temp = (int)((devices->trabajo[0] * 0.01)*surface);
        //devices->conformaciones[0] = stride_temp * pc_temp;
        //ttotal = stride_temp;

        for (unsigned int i=0;i < devices->n_devices;i++)
        {
                stride_temp = floor((devices->trabajo[i] * 0.01)*nconformations);
                devices->conformaciones[i] = stride_temp;
                ttotal += stride_temp;
        }
        unsigned int diferencia = nconformations - ttotal;
        for (unsigned int i=0;i<diferencia;i++)
        {
                dev = i % devices->n_devices;
                devices->conformaciones[dev] ++;
        }
        for (unsigned int i=(devices->n_devices-1);i > 0;i--)
        {
                devices->stride[i] = nconformations - devices->conformaciones[i];
                nconformations = nconformations - devices->conformaciones[i];
        }

	
        omp_set_num_threads(devices->n_devices);

        #pragma omp parallel for
        for (unsigned int j=0;j<devices->n_devices;j++)
        {
                unsigned int th = omp_get_thread_num();
                unsigned int stride_d = devices->stride[th];
		mutation_gpu (proteina,ligando,param,weights,f_params,move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,nconfs+stride_d,devices->conformaciones[th],metaheuristic,devices,th,stride_d);
	}

}
