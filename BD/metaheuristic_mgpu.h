#ifndef __METAHEURISTIC_MGPU_H
#define __METAHEURISTIC_MGPU_H

extern void multigpuSolver (struct ligand_t ligando, struct receptor_t proteina, unsigned int surface, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, struct energy_t energy, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices);
extern void multigpuSolver_mejora (struct ligand_t ligando, struct receptor_t proteina, unsigned int surface, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int fase);
extern void multigpuSolver_mejora_environment (struct ligand_t ligando, struct receptor_t proteina, unsigned int surface, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int fase);

extern void mejorar_multigpu (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, struct vectors_t *vectors_selec, struct metaheuristic_t metaheuristic, struct device_t *devices,unsigned int fase);
extern void mejorar_multigpu_environment (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, struct vectors_t *vectors_selec, struct metaheuristic_t metaheuristic, struct device_t *devices,unsigned int fase);

#endif
