#ifndef __METAHEU_INI_FLEX_H__
#define __METAHEU_INI_FLEX_H__

extern void inicializar_flex (struct receptor_t proteina, struct ligand_t ligando, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic,struct device_t *devices,struct flexibility_params_t *flexibility_params, struct flexibility_data_t *flexibility_conformations, type_data **dqn_data);
extern void fill_conformations (unsigned int N, struct param_t param, struct metaheuristic_t metaheuristic, struct ligand_t ligando, type_data *&conformations_x, type_data *&conformations_y, type_data *&conformations_z);
extern void seleccionar_mejorar_flex (unsigned int nlig, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic);
extern void seleccionar_inicializar_flex (unsigned int nlig, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic);
extern void seleccionar_inicializar_multigpu_flex (unsigned int nlig, struct vectors_t *vectors_e, struct vectors_t * vectors_s, struct param_t param, struct metaheuristic_t metaheuristic);

extern void incluir_mejorar_inicializar_flex (unsigned int nlig, struct vectors_t vectors_e, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int num_surface, unsigned int nconformations ,struct metaheuristic_t metaheuristic, struct param_t param);
extern void incluir_mejorar_inicializar_environment_flex (unsigned int nlig, struct vectors_t vectors_e, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int nconformations, struct metaheuristic_t metaheuristic, struct param_t param);

//extern void incluir_gpu_flex (unsigned int nlig, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic);
//extern void incluir_gpu_mejorar_flex (unsigned int nlig, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic);
extern void seleccionar_NEFIni_flex (unsigned int nlig, struct vectors_t *vectors_e, type_data *&conformations_x, type_data *&conformations_y, type_data *&conformations_z, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param);

//Mejorar
extern void mejorar_flex (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int num_surface, unsigned int nconformations, struct metaheuristic_t metaheuristic, struct flexibility_params_t *flexibility_params, struct flexibility_data_t *flexibility_conformations, unsigned int opt);

extern void mejorar_flex_environment (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int num_surface, unsigned int nconformations, struct metaheuristic_t metaheuristic, struct flexibility_params_t *flexibility_params, struct flexibility_data_t *flexibility_conformations, unsigned int opt);

extern void incluir_gpu_flex_by_step (unsigned int nlig, type_data *e_conformations_x, type_data *e_conformations_y, type_data *e_conformations_z, type_data *e_move_x, type_data *e_move_y, type_data *e_move_z, type_data *e_quat_x, type_data *e_quat_y, type_data *e_quat_z, type_data *e_quat_w, type_data *e_energy, unsigned int *e_nconfs, type_data *s_conformations_x, type_data *s_conformations_y, type_data *s_conformations_z, type_data *s_move_x, type_data *s_move_y, type_data *s_move_z, type_data *s_quat_x, type_data *s_quat_y, type_data *s_quat_z, type_data *s_quat_w, type_data *s_energy, unsigned int *s_nconfs, unsigned int stride_e, unsigned int stride_s, unsigned int pos,struct param_t param, struct metaheuristic_t metaheuristic);

#endif
