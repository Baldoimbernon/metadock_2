#include "energy_kernel.h"
#include "energy_common.h"
#include "energy_common-gpu.h"
#include "energy_GpuSolver.h"
#include "wtime.h"


extern void gpuSolver (struct ligand_t ligando, struct receptor_t proteina, struct param_t params, struct metaheuristic_t metaheuristic, struct device_t *devices, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconf, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations,unsigned int orden_device, unsigned int stride_conformations, type_data *time_kernels)
{
    size_t available,total_m;
    cudaError_t cudaStatus;
    unsigned long int max_memory;
    unsigned long int max_positions;
    unsigned long int max_common;
    unsigned long int max_conf;
    unsigned long int typeSize = proteina.nrec * sizeof(char);
    unsigned long int recSize = proteina.nrec * sizeof(type_data);
    unsigned long int bondSize = proteina.nrec * sizeof(unsigned int) * MAXBOND;
    
    unsigned long int ligTypeSize = ligando.nlig * sizeof(char);
    unsigned long int ligSize = ligando.nlig * sizeof(type_data);
    unsigned long int ligBondSize = ligando.nlig * sizeof(unsigned int) * MAXBOND;
    	
    unsigned long int moveSize_tam = nconformations * sizeof(type_data);
    unsigned long int energyConf_tam = nconformations * sizeof(unsigned int);
    unsigned long int tam_max, tam_tmp, nconformations_count_total, nconformations_count_partial, tam_conformation, stride_d;

    max_positions = (moveSize_tam * 8) + energyConf_tam;
    max_common = (recSize * 4) + typeSize + bondSize + (ligSize * 4) + ligTypeSize + ligBondSize;

    max_memory = max_positions + max_common;

    tam_conformation =  (sizeof(type_data) * 8) + sizeof(unsigned int);

    cudaSetDevice(devices->id[orden_device]);
    //cudaDeviceReset();
    cudaMemGetInfo(&tam_max, &total_m);
    cudaDeviceSynchronize();
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
    //std::cout << tam_max << " " << total_m << "\n";
    tam_tmp = tam_max - (tam_max * 0.2);

	if (tam_tmp < max_memory)
        {
                //printf("Trocear en 1 GPU\n");
                max_memory = max_common;
                nconformations_count_total = 0;
                nconformations_count_partial = 0;
                stride_d = 0;
                while (nconformations_count_total < nconformations)
                {
                        max_memory += tam_conformation;
                        if (max_memory > tam_tmp)
                        {
                                //printf("Trozo %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
                                //nconformations_count_total--;
                                //nconformations_count_partial--;
                                //Lanzar con nconformations_count_partial;                       
				gpuSolver_calculation (ligando,proteina,params,metaheuristic,devices,move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,nconf+stride_d,weights,f_params,nconformations_count_partial,orden_device,stride_conformations+stride_d,time_kernels);
                                stride_d += nconformations_count_partial;
                                max_memory = max_common;
                                nconformations_count_partial = 0;
                        }
                        else
                        {
                                nconformations_count_total++;
                                nconformations_count_partial++;
                        }
                }
                //printf("Trozo final %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
		nconformations_count_partial--;
		gpuSolver_calculation (ligando,proteina,params,metaheuristic,devices,move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,nconf+stride_d,weights,f_params,nconformations_count_partial,orden_device,stride_conformations+stride_d,time_kernels);
        }
        else
        {
		//printf("No Trocea\n");
		//exit(0);
                //Lanzar con todo
		gpuSolver_calculation (ligando,proteina,params,metaheuristic,devices,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconf,weights,f_params,nconformations,orden_device,stride_conformations,time_kernels);
	}
	
}

extern void gpuSolver_calculation (struct ligand_t ligando, struct receptor_t proteina, struct param_t params, struct metaheuristic_t metaheuristic, struct device_t *devices, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconf, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations,unsigned int orden_device, unsigned int stride_conformations, type_data *time_kernels)
{
	type_data *energy_d, *move_x_d, *move_y_d, *move_z_d;
	type_data *quat_d_x, *quat_d_y, *quat_d_z, *quat_d_w;
	//struct receptor_t *proteina_d;
	//struct ligand_t *ligando_d;
	type_data kernels=0;
	type_data *rec_x_d, *rec_y_d, *rec_z_d, *qr_d;
	char *rectype_d, *ligtype_d;
	unsigned int *bondsr_d, *bonds_d;
	type_data *lig_x_d, *lig_y_d, *lig_z_d, *ql_d;
	type_data kernel_ini = 0;
	type_data kernel_fin = 0;
	
	cudaError_t cudaStatus;	
	//cudaEvent_t start_k, stop_k;

	unsigned int *n_conformations_d, ngpu;
	unsigned int energySize = nconformations * sizeof(type_data);
	ngpu = devices->id[orden_device];
	cudaSetDevice(ngpu);
	cudaDeviceReset();
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) printf("Error al principio de gpusolver\n");
	//exit(0);
	//printf("Nc %d\n",nconformations);
	dataToGPUEnergy(nconformations, move_x, move_y, move_z, quat_x, quat_y, quat_z, quat_w, move_x_d, move_y_d, move_z_d, quat_d_x, quat_d_y, quat_d_z, quat_d_w);
	//commonDataToGPU (proteina, proteina_d,ligando, ligando_d);
	commonDataToGPU_WS (proteina,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,ligando,lig_x_d,lig_y_d,lig_z_d,ql_d,ligtype_d,bonds_d);
	//	PROTEINA EN MEMORIA GLOBAL
	cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) printf("COPY FAILED!\n");
	//exit(0);	
	cudaStatus = cudaMalloc((void**) &energy_d, energySize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc energy_d");
	cudaStatus = cudaMalloc((void**) &n_conformations_d, sizeof(unsigned int)*nconformations);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc n_conformations_d");
	
	cudaStatus = cudaMemset (energy_d,0,energySize);
	cudaStatus = cudaMemcpy (n_conformations_d,nconf, sizeof(unsigned int)*nconformations, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy n_conformations_d failed!");
		
	//guardar tipos de ligando en memoria constante
	save_params (weights,f_params);
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "COPY FAILED!");


	cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeFourByte);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_by_warp, cudaFuncCachePreferEqual);
	cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_by_warp_type1, cudaFuncCachePreferEqual);
	cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_by_warp_type2, cudaFuncCachePreferEqual);
	
	unsigned int size = (5 * devices->hilos[orden_device*metaheuristic.num_kernels] * sizeof(type_data)) + (sizeof(char)*devices->hilos[orden_device*metaheuristic.num_kernels]);	
	unsigned int blk = ceil(nconformations*WARP_SIZE / devices->hilos[orden_device*metaheuristic.num_kernels]);
	unsigned long int max_e = nconformations* WARP_SIZE;	
	dim3 hilos (devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);
	dim3 grid  (ceil(blk/8)+1, 8);	
	
	switch (params.scoring_function_type)
	{
		case 0:
			kernel_ini = wtime();		
			Gpu_full_Kernel_Conformations_by_warp <<<grid,hilos,size>>> (proteina.nrec,ligando.atoms,ligando.nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,move_x_d,move_y_d,move_z_d,quat_d_x, quat_d_y, quat_d_z, quat_d_w,lig_x_d,lig_y_d,lig_z_d,ql_d,ligtype_d,bonds_d,energy_d,n_conformations_d,stride_conformations,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
			kernel_fin = wtime() - kernel_ini;
			//printf("id %d kernel_ini %f kernel_fin %f\n",ngpu,kernel_ini,kernel_fin);
			break;
		case 1:
			kernel_ini = wtime();
			Gpu_full_Kernel_Conformations_by_warp_type1 <<<grid,hilos,size>>> (proteina.nrec,ligando.atoms,ligando.nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,move_x_d,move_y_d,move_z_d,quat_d_x, quat_d_y, quat_d_z, quat_d_w,lig_x_d,lig_y_d,lig_z_d,ql_d,ligtype_d,bonds_d,energy_d,n_conformations_d,stride_conformations,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
			kernel_fin = wtime() - kernel_ini;
                        //printf("id %d kernels %f\n",ngpu,kernels);
                        break;
		case 2:
			kernel_ini = wtime();
			Gpu_full_Kernel_Conformations_by_warp_type2 <<<grid,hilos,size>>> (proteina.nrec,ligando.atoms,ligando.nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,move_x_d,move_y_d,move_z_d,quat_d_x, quat_d_y, quat_d_z, quat_d_w,lig_x_d,lig_y_d,lig_z_d,ql_d,ligtype_d,bonds_d,energy_d,n_conformations_d,stride_conformations,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
			kernel_fin = wtime() - kernel_ini;
                        //printf("id %d kernels %f\n",ngpu,kernels);
                        break;
	}

	//*time_kernels += kernels;		
	*time_kernels += kernel_fin;
	cudaDeviceSynchronize();				
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "KERNEL FAILED! %d %d %d",devices->id[orden_device],nconformations,devices->hilos[orden_device*metaheuristic.num_kernels]);
			
	cudaStatus = cudaMemcpy (energy, energy_d, energySize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy energy_d failed!");
	//cudaStatus = cudaMemcpy (nconf, n_conformations_d, sizeof(int)*nconformations, cudaMemcpyDeviceToHost);
	//if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy n_conformations_d failed!");
	//printf("energy.energy[1000] = %f conformacion %d\n",energy.energy[1000],energy.n_conformation[1000]);	
	
	
	cudaFree(move_x_d);
	cudaFree(move_y_d);
	cudaFree(move_z_d);
	cudaFree(quat_d_x);
	cudaFree(quat_d_y);
	cudaFree(quat_d_z);
	cudaFree(quat_d_w);
	cudaFree(rec_x_d);
	cudaFree(rec_y_d);
	cudaFree(rec_z_d);
	cudaFree(qr_d);
	cudaFree(bondsr_d);
	cudaFree(rectype_d);
	cudaFree(lig_x_d);
	cudaFree(lig_y_d);
	cudaFree(lig_z_d);
	cudaFree(ql_d);
	cudaFree(ligtype_d);
	cudaFree(bonds_d);
	cudaFree(energy_d);
	cudaFree(n_conformations_d);
	cudaDeviceReset();	
	
}

extern void gpuSolver_flex (struct ligand_t ligando, struct receptor_t proteina, struct param_t params, struct metaheuristic_t metaheuristic, struct device_t *devices, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconf, type_data *weights, struct force_field_param_t *f_params, unsigned int tor, unsigned int nconformations,unsigned int orden_device, unsigned int stride_conformations,bool *individual_bonds_VDW, bool *individual_bonds_ES, type_data *time_kernels)
{
	
	size_t available,total_m;
    cudaError_t cudaStatus;
    unsigned long int max_memory;
    unsigned long int max_positions;
    unsigned long int max_common;
    unsigned long int max_conf;
    unsigned long int typeSize = proteina.nrec * sizeof(char);
    unsigned long int recSize = proteina.nrec * sizeof(type_data);
    unsigned long int bondSize = proteina.nrec * sizeof(unsigned int) * MAXBOND;
    
	unsigned long int ligTypeSize = ligando.nlig * sizeof(char);
    unsigned long int ligSize = ligando.nlig * sizeof(type_data);
    unsigned long int ligBondSize = ligando.nlig * sizeof(unsigned int) * MAXBOND;
    unsigned long int boolSize = ligando.atoms * 2 * sizeof(bool);	
	unsigned long int confSize_tam = nconformations * ligando.nlig * sizeof(type_data);
	
	
    unsigned long int moveSize_tam = nconformations * sizeof(type_data);
    unsigned long int energyConf_tam = nconformations * sizeof(unsigned int);
    unsigned long int tam_max, tam_tmp, nconformations_count_total, nconformations_count_partial, tam_conformation, stride_d;

    max_positions = (moveSize_tam * 8) + moveSize_tam + energyConf_tam;
	max_conf = (3 * confSize_tam) + (boolSize * 2);
    max_common = (recSize * 4) + typeSize + bondSize + (ligSize * 4) + ligTypeSize + ligBondSize + boolSize;

    max_memory = max_positions + max_common;

    tam_conformation =  (sizeof(type_data) * 8) + sizeof(type_data) + sizeof(unsigned int) + (3 * ligando.nlig * sizeof(type_data));
    //exit(0);
    cudaSetDevice(devices->id[orden_device]);
    //cudaDeviceReset();
    cudaMemGetInfo(&tam_max, &total_m);
    cudaDeviceSynchronize();
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
    //std::cout << tam_max << " " << total_m << "\n";
    //exit(0);
    tam_tmp = tam_max - (tam_max * 0.2);
	
	if (tam_tmp < max_memory)
        {
                //printf("Trocear en 1 GPU\n");
                max_memory = max_common;
                nconformations_count_total = 0;
                nconformations_count_partial = 0;
                stride_d = 0;
                while (nconformations_count_total < nconformations)
                {
                        max_memory += tam_conformation;
                        if (max_memory > tam_tmp)
                        {
                                //printf("Trozo %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
                                //nconformations_count_total--;
                                //nconformations_count_partial--;
                                //Lanzar con nconformations_count_partial;                       
				gpuSolver_calculation_flex (ligando,proteina,params,metaheuristic,devices,conformations_x+(stride_d*ligando.nlig),conformations_y+(stride_d*ligando.nlig),conformations_z+(stride_d*ligando.nlig),move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,nconf+stride_d,weights,f_params,tor,nconformations_count_partial,orden_device,stride_d+stride_conformations,individual_bonds_VDW,individual_bonds_ES,time_kernels);	
                                stride_d += nconformations_count_partial;
                                max_memory = max_common;
                                nconformations_count_partial = 0;
                        }
                        else
                        {
                                nconformations_count_total++;
                                nconformations_count_partial++;
                        }
                }
                //printf("Trozo final %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
		nconformations_count_partial--;
		gpuSolver_calculation_flex (ligando,proteina,params,metaheuristic,devices,conformations_x+(stride_d*ligando.nlig),conformations_y+(stride_d*ligando.nlig),conformations_z+(stride_d*ligando.nlig),move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,nconf+stride_d,weights,f_params,tor,nconformations_count_partial,orden_device,stride_d+stride_conformations,individual_bonds_VDW,individual_bonds_ES,time_kernels);	
        }
        else
        {
		//printf("No Trocea\n");
		//exit(0);
                //Lanzar con todo
		gpuSolver_calculation_flex (ligando,proteina,params,metaheuristic,devices,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconf,weights,f_params,tor,nconformations,orden_device,stride_conformations,individual_bonds_VDW,individual_bonds_ES,time_kernels);	
	}
}

extern void gpuSolver_calculation_flex (struct ligand_t ligando, struct receptor_t proteina, struct param_t params, struct metaheuristic_t metaheuristic, struct device_t *devices, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconf, type_data *weights, struct force_field_param_t *f_params, unsigned int tor, unsigned int nconformations,unsigned int orden_device, unsigned int stride_conformations,bool *individual_bonds_VDW, bool *individual_bonds_ES, type_data *time_kernels)
{
	type_data *internal_energy, *internal_energy_d, *energy_d, *move_x_d, *move_y_d, *move_z_d;
	type_data *quat_d_x, *quat_d_y, *quat_d_z, *quat_d_w;
	type_data *conformations_x_d, *conformations_y_d, *conformations_z_d;
	bool *individual_bonds_VDW_d, *individual_bonds_ES_d;
	//struct receptor_t *proteina_d;
	//struct ligand_t *ligando_d;

	type_data *rec_x_d, *rec_y_d, *rec_z_d, *qr_d;
	char *rectype_d, *ligtype_d;
	unsigned int *bondsr_d, *bonds_d;
	type_data *lig_x_d, *lig_y_d, *lig_z_d, *ql_d;
	cudaError_t cudaStatus;
	unsigned int *n_conformations_d,ngpu;
	type_data kernel_ini = 0;
        type_data kernel_fin = 0;

	//printf("nconformations: %d\n",nconformations);
	unsigned int energySize = nconformations * sizeof(type_data);
	ngpu = devices->id[orden_device];

	cudaSetDevice(ngpu);
	//cudaDeviceReset();
	
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) printf("Error seleccion device %d\n",devices->id[orden_device]);	
	//internal_energy = (type_data *)malloc(sizeof(type_data)*nconformations);
	//guardar tipos de ligando en memoria constante
	
	dataToGPUEnergy(nconformations, move_x, move_y, move_z, quat_x, quat_y, quat_z, quat_w, move_x_d, move_y_d, move_z_d, quat_d_x, quat_d_y, quat_d_z, quat_d_w);
	dataToGPUInternalEnergy(nconformations,ligando.nlig,ligando.atoms,conformations_x,conformations_y,conformations_z,conformations_x_d,conformations_y_d,conformations_z_d,individual_bonds_VDW,individual_bonds_ES,individual_bonds_VDW_d,individual_bonds_ES_d,internal_energy_d);
	//commonDataToGPU (proteina, proteina_d,ligando, ligando_d);
	commonDataToGPU_WS (proteina,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,ligando,lig_x_d,lig_y_d,lig_z_d,ql_d,ligtype_d,bonds_d);
//	PROTEINA EN MEMORIA GLOBAL
	cudaDeviceSynchronize();
	save_params (weights,f_params);	
	cudaStatus = cudaMalloc((void**) &energy_d, energySize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc energy_d");
	cudaStatus = cudaMalloc((void**) &n_conformations_d, sizeof(int)*nconformations);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc n_conformations_d");
	
	//cudaStatus = cudaMemset (energy_d,0,energySize);
	cudaStatus = cudaMemcpy (n_conformations_d,nconf, sizeof(int)*nconformations, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy n_conformations_d failed!");
	
	
	cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeFourByte);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_flex_by_warp, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_flex_by_warp_type1, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_flex_by_warp_type2, cudaFuncCachePreferEqual);

	cudaFuncSetCacheConfig(energy_internal_conformations, cudaFuncCachePreferEqual);
	cudaFuncSetCacheConfig(energy_internal_conformations_type2, cudaFuncCachePreferEqual);

	unsigned int blk_t = ceil(nconformations / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
	dim3 grid_t  (ceil(blk_t/8)+1, 8);	
	dim3 hilos_t (devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);	
	unsigned long int max_t = nconformations;	
			
	
	unsigned int size = 5 * devices->hilos[orden_device*metaheuristic.num_kernels] * sizeof(type_data) + sizeof(char)*devices->hilos[orden_device*metaheuristic.num_kernels];	
	unsigned int blk_e = ceil(nconformations*WARP_SIZE / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
	unsigned long int max_e = nconformations* WARP_SIZE;	
	dim3 hilos_e (devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);
	dim3 grid_e  (ceil(blk_e/8)+1, 8);
	
	
	switch (params.scoring_function_type)
	{
		case 0:
			kernel_ini = wtime();
			Gpu_full_Kernel_Conformations_flex_by_warp <<<grid_e,hilos_e,size>>> (proteina.nrec,ligando.atoms,ligando.nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d,conformations_y_d,conformations_z_d,move_x_d,move_y_d,move_z_d,quat_d_x, quat_d_y, quat_d_z, quat_d_w,ql_d,ligtype_d,bonds_d,energy_d,n_conformations_d,stride_conformations,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
			cudaDeviceSynchronize();
			kernel_fin = wtime() - kernel_ini;
			//printf("ngpu %d kernel_fin %f\n",ngpu,kernel_fin);
			break;
		case 1:
			kernel_ini = wtime();
			Gpu_full_Kernel_Conformations_flex_by_warp_type1 <<<grid_e,hilos_e,size>>> (tor,proteina.nrec,ligando.atoms,ligando.nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d,conformations_y_d,conformations_z_d,move_x_d,move_y_d,move_z_d,quat_d_x, quat_d_y, quat_d_z, quat_d_w,ql_d,ligtype_d,bonds_d,energy_d,n_conformations_d,stride_conformations,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
			cudaDeviceSynchronize();
			kernel_fin = wtime() - kernel_ini;
			break;
		case 2:
			kernel_ini = wtime();
			Gpu_full_Kernel_Conformations_flex_by_warp_type2 <<<grid_e,hilos_e,size>>> (tor,proteina.nrec,ligando.atoms,ligando.nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d,conformations_y_d,conformations_z_d,move_x_d,move_y_d,move_z_d,quat_d_x, quat_d_y, quat_d_z, quat_d_w,ql_d,ligtype_d,bonds_d,energy_d,n_conformations_d,stride_conformations,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
			cudaDeviceSynchronize();
			kernel_fin = wtime() - kernel_ini;
			break;
	}
		
	cudaDeviceSynchronize();				
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "KERNEL FAILED ENERGY!");
	
	switch (params.scoring_function_type)
	{
		case 0:
		case 1:
			energy_internal_conformations<<<grid_e,hilos_e,size>>>(ql_d,ligtype_d,conformations_x_d,conformations_y_d,conformations_z_d,ligando.atoms,ligando.nlig,internal_energy_d,n_conformations_d,stride_conformations,individual_bonds_VDW_d,individual_bonds_ES_d,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
			break;
		case 2:
			energy_internal_conformations_type2<<<grid_e,hilos_e,size>>>(ql_d,ligtype_d,conformations_x_d,conformations_y_d,conformations_z_d,ligando.atoms,ligando.nlig,internal_energy_d,n_conformations_d,stride_conformations,individual_bonds_VDW_d,individual_bonds_ES_d,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
                        break;
	}
	*time_kernels = kernel_fin;
	cudaDeviceSynchronize();				
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "KERNEL FAILED INTERNAL ENERGY!");

	/*float valor;
	cudaStatus = cudaMemcpy (&valor,internal_energy_d+7653, sizeof(float), cudaMemcpyDeviceToHost);	
	printf("conf 0 valor internal_energy %f\n",valor);	*/
	energy_total <<<grid_t,hilos_t>>>(energy_d,internal_energy_d,max_t);
	cudaDeviceSynchronize();				
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "KERNEL FAILED TOTAL ENERGY en GPUSOLVER!");
	
	cudaStatus = cudaMemcpy (energy, energy_d, energySize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy energy_d failed!");
	/*cudaStatus = cudaMemcpy (internal_energy, internal_energy_d, energySize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy internal_energy failed!");*/
	
	cudaFree(move_x_d);
	cudaFree(move_y_d);
	cudaFree(move_z_d);
	cudaFree(quat_d_x);
	cudaFree(quat_d_y);
	cudaFree(quat_d_z);
	cudaFree(quat_d_w);
	cudaFree(rec_x_d);
	cudaFree(rec_y_d);
	cudaFree(rec_z_d);
	cudaFree(qr_d);
	cudaFree(bondsr_d);
	cudaFree(rectype_d);
	cudaFree(lig_x_d);
	cudaFree(lig_y_d);
	cudaFree(lig_z_d);
	cudaFree(ql_d);
	cudaFree(ligtype_d);
	cudaFree(bonds_d);
	cudaFree(energy_d);
	cudaFree(n_conformations_d);
	
	cudaFree(internal_energy_d);
	cudaFree(conformations_x_d);
	cudaFree(conformations_y_d);
	cudaFree(conformations_z_d);
	cudaFree(individual_bonds_VDW_d);
	cudaFree(individual_bonds_ES_d);
	cudaDeviceReset();	
	//exit(0);
	
}
