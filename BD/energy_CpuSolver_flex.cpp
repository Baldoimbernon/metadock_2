#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include <omp.h>
#include "definitions.h"
#include "energy_common.h"
#include "rotation.h"
#include "energy_CpuSolver.h"
#include "energy_CpuSolver_common.h"
#include "flexibility.h"
#include "vector_types.h"

//using namespace std;

extern void generate_positions_cpp_flex (unsigned int nlig, struct param_t param, struct vectors_t *vectors, struct metaheuristic_t metaheuristic, struct flexibility_params_t *flexibility_params,struct flexibility_data_t * flexibility_conformations)
{
	
	unsigned int steps,moveType=MOVE;
	steps = 0;
	param.steps = 20;
	
	init_quat(vectors->quat_x,vectors->quat_y,vectors->quat_z,vectors->quat_w,vectors->nconformations);
	while (steps < param.steps){		
		if (moveType == MOVE){			
			move(vectors->num_surface,vectors->move_x,vectors->move_y,vectors->move_z,vectors->surface_x,vectors->surface_y,vectors->surface_z,param,metaheuristic,metaheuristic.NEIIni);
			moveType = ROTATE;
		}
		if (moveType = ROTATE) {
			rotate(vectors->quat_x,vectors->quat_y,vectors->quat_z,vectors->quat_w,param,vectors->nconformations,metaheuristic);
			moveType = FLEX;
		}
		if (moveType = FLEX) {
			angulations_conformations_cpp (vectors->nconformations,metaheuristic,param,flexibility_conformations,flexibility_params->n_links,param.flex_angle);
			flexibility_cpp (vectors->nconformations,metaheuristic,param,vectors->conformations_x,vectors->conformations_y,vectors->conformations_z,flexibility_conformations,nlig,flexibility_params->n_links,flexibility_params->n_fragments,flexibility_params->links,flexibility_params->links_fragments,flexibility_params->fragments,flexibility_params->fragments_tam,param.flex_angle);									
			moveType = MOVE;
		}
		steps++;	
	}
}

extern void generate_positions_cpp_warm_up_flex (unsigned int nlig, struct param_t param, struct vectors_t *vectors, struct metaheuristic_t metaheuristic, struct flexibility_params_t *flexibility_params,struct flexibility_data_t *flexibility_conformations)
{
	
	unsigned int steps,moveType=MOVE;
	steps = 0;
	param.steps = 2;
	init_quat(vectors->quat_x,vectors->quat_y,vectors->quat_z,vectors->quat_w,vectors->nconformations);
	
	while (steps < param.steps){		
		if (moveType == MOVE){			
			move(vectors->num_surface,vectors->move_x,vectors->move_y,vectors->move_z,vectors->surface_x,vectors->surface_y,vectors->surface_z,param,metaheuristic,param.conf_warm_up_cpu);
			moveType = ROTATE;
		}
		if (moveType = ROTATE) {			
			rotate(vectors->quat_x,vectors->quat_y,vectors->quat_z,vectors->quat_w,param,vectors->nconformations,metaheuristic);
			moveType = FLEX;
		}
		if (moveType = FLEX) {
			angulations_conformations_cpp (vectors->nconformations,metaheuristic,param,flexibility_conformations,flexibility_params->n_links,param.flex_angle);
			flexibility_cpp (vectors->nconformations,metaheuristic,param,vectors->conformations_x,vectors->conformations_y,vectors->conformations_z,flexibility_conformations,nlig,flexibility_params->n_links,flexibility_params->n_fragments,flexibility_params->links,flexibility_params->links_fragments,flexibility_params->fragments,flexibility_params->fragments_tam,param.flex_angle);												
			moveType = MOVE;
		}
		steps++;	
	}
}

extern void forces_CPU_flex_type2 (struct param_t param, unsigned int atoms_r, unsigned int atoms_l,unsigned  int nlig, type_data *rec_x, type_data *rec_y, type_data *rec_z, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *moves_x, type_data *moves_y, type_data *moves_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, char* rectype, char* ligtype, type_data *ql ,type_data *qr, unsigned int *bonds_r, unsigned int *bonds_l, type_data *energy, unsigned int *nconfs, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct metaheuristic_t metaheuristic, unsigned int tor)
{

     	type_data term6, dist,temp_sqrt=0,temp_vdw=0, temp_es = 0, temp_hbond = 0, temp_solv = 0, eps, sig, miatomo[3], v[3], w[3], e_es, e_vdw, e_hbond, e_solv, pre_calc, lig_map,tmp_e_es=0,tmp_e_vdw=0,tmp_e_hbond=0,tmp_e_solv=0, term12;
        unsigned int j,i,k,id_conformation;
        unsigned int th1;
        unsigned int total;
        char ind1, ind2;
        type_data qaux_x,qaux_y,qaux_z,qaux_w;
        double tmpconst,tmpconst_h,cA,cB,cC,cD,eps_hb,sig_hb,rA,rB,dxA,dxB,rC,rD,dxC,dxD;
        type_data solv_asp_1, solv_asp_2, solv_vol_1, solv_vol_2,solv_qasp_1,solv_qasp_2;
        double minus_inv_two_sigma_sqd = -0.5L / (sigma_autodock * sigma_autodock);
        type_data exponencial;

        type_data difx,dify,difz;
        type_data  mod2x, mod2y, mod2z;
        //omp_set_nested(1);
        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 2:
                        th1 = metaheuristic.Threads1Fit;
                        break;
        }
        //exit(0);
        //printf("weights[0] %f \n",weights[0]);
        //exit(0);
        omp_set_num_threads (th1);
        #pragma omp parallel for private (id_conformation,dist,temp_es,temp_vdw,temp_hbond,e_es,e_vdw,e_hbond,miatomo,ind1,ind2,qaux_x,qaux_y,qaux_z,qaux_w,difx,dify,difz,mod2x,mod2y,mod2z,eps,sig,eps_hb,tmpconst,sig_hb,term6,v,w,solv_asp_1,solv_vol_1,solv_asp_2,solv_vol_2,pre_calc,lig_map,e_solv,temp_solv,exponencial,cA,cB,cC,cD,dxA,dxB,dxC,dxD,rA,rB,rC,rD,term12,tmp_e_es,tmp_e_vdw,tmp_e_solv,tmp_e_hbond)
	for (unsigned int k = 0; k < nconformations; k++)
        {
                id_conformation = nconfs[k];
                for(unsigned int i=0;i<atoms_l;i++){                                    // "j" is related with nparticles of receptor

                        e_es = 0;
                        e_vdw = 0;
                        e_hbond = 0;
                        e_solv = 0;
                        ind1 = ligtype[i];
                        qaux_x = quat_x[id_conformation];
                        qaux_y = quat_y[id_conformation];
                        qaux_z = quat_z[id_conformation];
                        qaux_w = quat_w[id_conformation];
                        rotate3DPoint_cpp (&qaux_x,&qaux_y,&qaux_z,&qaux_w,&conformations_x[(id_conformation*nlig)+i],&conformations_y[(id_conformation*nlig)+i],&conformations_z[(id_conformation*nlig)+i],miatomo);
                        miatomo[0] += moves_x[id_conformation];
                        miatomo[1] += moves_y[id_conformation];
                        miatomo[2] += moves_z[id_conformation];
                        solv_asp_1 = f_params[ind1].asp;
                        solv_vol_1 = f_params[ind1].vol;

			for(unsigned int j=0;j<atoms_r;j++){                            // "i" is related with nparticles of ligand
                                e_es = 0;
                                e_vdw = 0;
                                e_hbond = 0;
                                e_solv = 0;
                                ind2 = rectype[j];
                                solv_asp_2 = f_params[ind2].asp;
                                solv_vol_2 = f_params[ind2].vol;
                                difx= rec_x[j] - miatomo[0];
                                dify= rec_y[j] - miatomo[1];
                                difz= rec_z[j] - miatomo[2];
                                mod2x=difx*difx;
                                mod2y=dify*dify;
                                mod2z=difz*difz;

                                difx=mod2x+mod2y+mod2z;
                                dist = sqrtf(difx);
                                exponencial = exp(minus_inv_two_sigma_sqd * difx) * weights[3];

                                double dato_eij = calc_ddd_Mehler_Solmajer(dist,APPROX_ZERO);
                                //dato_eij = -0.01465;
                                //printf("e(rij) = %f\n",dato_eij);
                                eps = sqrt(f_params[ind1].epsilon * f_params[ind2].epsilon);
                                sig = ((f_params[ind1].sigma + f_params[ind2].sigma))*0.5L;
                                eps_hb =  (f_params[ind1].epsilon_h +  f_params[ind2].epsilon_h);//  * a_scoring.p_hbond;
                                sig_hb = ((f_params[ind1].sigma_h + f_params[ind2].sigma_h));//*0.5L;
                                //dist = clamp(dist, (RMIN_ELEC*RMIN_ELEC));

                                tmpconst = eps / (double)(xA - xB);
                                tmpconst_h = eps_hb / (double)(xC - xD);

                                cA =  tmpconst * pow( (double)sig, (double)xA ) * xB;
                                cB =  tmpconst * pow( (double)sig, (double)xB ) * xA;

                                cC =  tmpconst_h * pow( (double)sig_hb, (double)xC ) * xD;
                                cD =  tmpconst_h * pow( (double)sig_hb, (double)xD ) * xC;

                                dxA = (double) xA;
                                dxB = (double) xB;
                                dxC = (double) xC;
                                dxD = (double) xD;

                                rA = pow( dist, dxA);
                                rB = pow( dist, dxB);
                                rC = pow( dist, dxC);
                                rD = pow( dist, dxD);

                                term6 = cB / rB;//pow((double)dist, (double)xB);
                                term12 = cA / rA;//pow((double)dist, (double)xA);
				e_es = (1.0 / (dato_eij * dist)) * qr[j] * ELECSCALE * weights[2];
                                //if (dist < 7.0)
                                e_vdw = term12 - term6;

                                if (dist < 8)
                                {
                                        pre_calc = ((solv_asp_1 * solv_vol_2)  + ((solv_asp_2 + qsolpar * fabs(qr[j])) * solv_vol_1)) * exponencial;// * a_scoring.p_desolv;
                                        lig_map = qsolpar * solv_vol_2 * exponencial;
                                         e_solv = pre_calc + lig_map;
                                }

                                if ((dist > 0) && (dist < 8) && (ind1 == OXI || ind1 == NIT2 || ind1 == AZU)){
                                        if (ind2 == HBOND){
                                                //printf("Hola\n");
                                                sub_h(miatomo, miatomo + 1, miatomo + 2,  rec_x + j, rec_y + j, rec_z + j, v);
                                                sub_h(&(rec_x[bonds_r[MAXBOND * j]]), &(rec_y[bonds_r[MAXBOND * j]]),&(rec_z[bonds_r[MAXBOND * j]]),rec_x + j, rec_y + j, rec_z + j, w);
                                                double cosT = cosTheta_h (v, w);

                                                if ( cosT < 0.0 ){
                                                        double sinT = sqrt(1.0-cosT*cosT);
                                                        e_hbond = sinT * ((cC / rC) - (cD / rD) + cosT);
                                                        //printf("e_b %f\n",e_hbond);
                                                }
                                                // printf("e_b %f\n",e_hbond);
                                        }
                                }
                                if ((dist > 0) && (dist < 8) &&  ind1 == HBOND){
                                         //printf("Hola\n");
                                        if (ind2 == OXI || ind2 == NIT2 || ind2 == AZU){
                                                //printf("Hola\n");
						sub_h(&(conformations_x[(id_conformation*nlig)+bonds_l[MAXBOND*i]]), &(conformations_y[(id_conformation*nlig)+bonds_l[MAXBOND*i]]), &(conformations_z[(id_conformation*nlig)+bonds_l[MAXBOND*i]]), miatomo, miatomo + 1, miatomo + 2, v);
                                                //sub_h(&lig_x([bonds_l[MAXBOND*i]]), &(lig_y[bonds_l[MAXBOND*i]]), &(lig_z[bonds_l[MAXBOND*i]]), miatomo, miatomo + 1, miatomo + 2, v);
                                                sub_h(rec_x + j, rec_y + j, rec_z + j,miatomo, miatomo + 1, miatomo + 2, w);
                                                double cosT = cosTheta_h (v, w);

                                                if ( cosT < 0.0 ){
                                                        double sinT = sqrtf(1.0-cosT*cosT);
                                                        e_hbond += sinT * ((cC / rC) - (cD / rD) + cosT);
                                                        //printf("e_b %f\n",e_hbond);
                                                }
                                        }
                                }
                                tmp_e_es += e_es;
                                tmp_e_vdw += e_vdw;
                                tmp_e_hbond += e_hbond;
                                tmp_e_solv += e_solv;

                          }


                        temp_es += (tmp_e_es * ql[i]);
                        temp_vdw += tmp_e_vdw;
                        temp_hbond += tmp_e_hbond;
                        temp_solv += (tmp_e_solv * fabs(ql[i]) );
                        tmp_e_es=0;
                        tmp_e_vdw=0;
                        tmp_e_hbond=0;
                        tmp_e_solv=0;
                }


        //energy[id_conformation]= (temp_es * a_scoring.p_estat) + (temp_vdw*a_scoring.p_vdw) + (temp_hbond*a_scoring.p_hbond) + (temp_solv*a_scoring.p_desolv + (tor * a_scoring.p_tor));
        //printf("TOTAL conf %d e_es %f e_vdw %f e_bond %f e_solv %f e_tor %f\n",k,temp_es,temp_vdw*a_scoring.p_vdw,temp_hbond*a_scoring.p_hbond,temp_solv,tor * a_scoring.p_tor);
        energy[k] = (temp_es) + (temp_vdw*weights[0]) + (temp_hbond*weights[1]) + (temp_solv) + (tor*weights[3]);
        temp_es = 0;
        temp_vdw = 0;
        temp_hbond = 0;
        temp_solv = 0;
        }
}

extern void forces_CPU_flex_type1 (struct param_t param, unsigned int atoms_r, unsigned int atoms_l,unsigned  int nlig, type_data *rec_x, type_data *rec_y, type_data *rec_z, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *moves_x, type_data *moves_y, type_data *moves_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, char* rectype, char* ligtype, type_data *ql ,type_data *qr, unsigned int *bonds_r, unsigned int *bonds_l, type_data *energy, unsigned int *nconfs, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct metaheuristic_t metaheuristic,unsigned int tor){

	type_data term6, dist,temp_sqrt=0,temp_vdw=0, temp_es = 0, temp_hbond = 0, temp_solv = 0, eps, sig, miatomo[3], v[3], w[3], e_es, e_vdw, e_hbond, e_solv, pre_calc, lig_map;
        unsigned int j,i,k,id_conformation;
        unsigned int total;
        char ind1, ind2;
        unsigned int th1;

	type_data solv_asp_1, solv_asp_2, solv_vol_1, solv_vol_2,solv_qasp_1,solv_qasp_2;
        double minus_inv_two_sigma_sqd = -0.5L / (sigma_autodock * sigma_autodock);
        type_data exponencial;

        type_data qaux_x, qaux_y, qaux_z, qaux_w;


        type_data difx,dify,difz;
        type_data  mod2x, mod2y, mod2z;
        //omp_set_nested(1);
        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 2:
                        th1 = metaheuristic.Threads1Fit;
                        break;
        }
        //omp_set_nested(1);
        omp_set_num_threads (th1);
        #pragma omp parallel for private (id_conformation,dist,temp_es,temp_vdw,temp_hbond,e_es,e_vdw,e_hbond,miatomo,ind1,ind2,qaux_x,qaux_y,qaux_z,qaux_w,difx,dify,difz,mod2x,mod2y,mod2z,eps,sig,term6,v,w,solv_asp_1,solv_vol_1,solv_asp_2,solv_vol_2,pre_calc,lig_map,e_solv,temp_solv,exponencial)
        for (unsigned int k = 0; k < nconformations; k++)
        {
                id_conformation = nconfs[k];
                for(unsigned int i=0;i<atoms_l;i++){                                    // "j" is related with nparticles of receptor
                        ind1 = ligtype[i];
			solv_asp_1 = f_params[ind1].asp;
                        solv_vol_1 = f_params[ind1].vol;
                        qaux_x = quat_x[id_conformation];
                        qaux_y = quat_y[id_conformation];
                        qaux_z = quat_z[id_conformation];
                        qaux_w = quat_w[id_conformation];
                        rotate3DPoint_cpp (&qaux_x,&qaux_y,&qaux_z,&qaux_w,&conformations_x[(id_conformation*nlig)+i],&conformations_y[(id_conformation*nlig)+i],&conformations_z[(id_conformation*nlig)+i],miatomo);
                        miatomo[0] += moves_x[id_conformation];
                        miatomo[1] += moves_y[id_conformation];
                        miatomo[2] += moves_z[id_conformation];

			for(int j=0;j<atoms_r;j++){                             // "i" is related with nparticles of ligand
                                e_es = 0;
                                e_vdw = 0;
                                e_hbond = 0;
				e_solv = 0;
                                ind2 = rectype[j];
				solv_asp_2 = f_params[ind2].asp;
                                solv_vol_2 = f_params[ind2].vol;
                                difx= rec_x[j] - miatomo[0];
                                dify= rec_y[j] - miatomo[1];
                                difz= rec_z[j] - miatomo[2];
                                mod2x=difx*difx;
                                mod2y=dify*dify;
                                mod2z=difz*difz;

                                difx=mod2x+mod2y+mod2z;
                                dist = sqrtf(difx);
				exponencial = exp(minus_inv_two_sigma_sqd * difx) * weights[3];
                                //eps = sqrtf(vdw_params[ind1].epsilon * vdw_params[ind2].epsilon);
                                //sig = vdw_params[ind1].sigma * vdw_params[ind2].sigma;
                                eps = sqrtf(f_params[ind1].epsilon * f_params[ind2].epsilon);
                                sig = f_params[ind1].sigma * f_params[ind2].sigma;

                                term6 = sig / difx;

                                term6 = term6 * term6 * term6;
                                e_es = ql[i] * (1/dist) * qr[j];
                                //e_vdw = 4.0 * eps * term6 * (term6 - 1.0);
				if (dist > 0.05) e_vdw = 4.0 * eps * term6 * (term6 - 1.0); else e_vdw=10000000;
				pre_calc = ((solv_asp_1 * solv_vol_2)  + ((solv_asp_2 + qsolpar * fabs(qr[j])) * solv_vol_1)) * exponencial;
                                lig_map = qsolpar * solv_vol_2 * exponencial;
                                e_solv = pre_calc + lig_map;
			
				if (dist > 0.18 && dist < 0.39 && (ind1 == OXI || ind1 == NIT2 || ind1 == AZU)){
                                        if (ind2 == HBOND){
                                                sub_h(miatomo, miatomo + 1, miatomo + 2,  rec_x + j, rec_y + j, rec_z + j, v);
                                                sub_h(&(rec_x[bonds_r[MAXBOND * j]]), &(rec_y[bonds_r[MAXBOND * j]]),&(rec_z[bonds_r[MAXBOND * j]]),rec_x + j, rec_y + j, rec_z + j, w);
                                                type_data cosT = cosTheta_h (v, w);

                                                if ( cosT < 0.0 ){
                                                        type_data sinT = sqrt(1.0-cosT*cosT);
                                                        e_vdw *= sinT;
                                                }
                                        }
                                }

                                if (dist > 0.18 && dist < 0.39 &&  ind1 == HBOND){
                                        if (ind2 == OXI || ind2 == NIT2 || ind2 == AZU){
                                                sub_h(&(conformations_x[(id_conformation*nlig)+bonds_l[MAXBOND*i]]), &(conformations_y[(id_conformation*nlig)+bonds_l[MAXBOND*i]]), &(conformations_z[(id_conformation*nlig)+bonds_l[MAXBOND*i]]), miatomo, miatomo + 1, miatomo + 2, v);
                                                sub_h(rec_x + j, rec_y + j, rec_z + j,miatomo, miatomo + 1, miatomo + 2, w);
                                                type_data cosT = cosTheta_h (v, w);

                                                if ( cosT < 0.0 ){
                                                        type_data sinT = sqrt(1.0-cosT*cosT);
                                                        e_vdw *= sinT;
                                                        dify = difx *difx *difx *difx *difx;
                                                        //e_hbond += -cosT * (hbond_params[ind2].c12 / (dify*difx) - hbond_params[ind2].c10 / dify );
                                                        e_hbond += -cosT * (f_params[ind2].epsilon_h / (dify*difx) - f_params[ind2].sigma_h / dify );
                                                }
                                        }
                                }
                                temp_es += e_es;
                                temp_vdw += e_vdw;
                                temp_hbond += e_hbond;
				temp_solv += e_solv; 
                        }
			temp_solv *= fabs(ql[i]);
                }
                energy[k] = (temp_es * ES_CONSTANT * weights[2]) + (temp_vdw * weights[0]) + (temp_hbond * weights[1]) + (temp_solv) + (tor * weights[4]);
                temp_es = 0;
                temp_vdw = 0;
                temp_hbond = 0;
		temp_solv = 0;
		
	}

}
extern void forces_CPU_flex (struct param_t param, unsigned int atoms_r, unsigned int atoms_l,unsigned  int nlig, type_data *rec_x, type_data *rec_y, type_data *rec_z, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *moves_x, type_data *moves_y, type_data *moves_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, char* rectype, char* ligtype, type_data *ql ,type_data *qr, unsigned int *bonds_r, unsigned int *bonds_l, type_data *energy, unsigned int *nconfs, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct metaheuristic_t metaheuristic){
	type_data term6, dist,temp_sqrt=0,temp_vdw=0, temp_es = 0, temp_hbond = 0, eps, sig, miatomo[3], v[3], w[3], e_es, e_vdw, e_hbond;
	unsigned int j,i,k,id_conformation;
	unsigned int total;
	char ind1, ind2;
	unsigned int th1;
	type_data qaux_x, qaux_y, qaux_z, qaux_w;
	

	type_data difx,dify,difz;
	type_data  mod2x, mod2y, mod2z;	
	//omp_set_nested(1);
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 2:
			th1 = metaheuristic.Threads1Fit;
			break;
	}
	//omp_set_nested(1);
	omp_set_num_threads (th1);
	#pragma omp parallel for private (id_conformation,dist,temp_es,temp_vdw,temp_hbond,e_es,e_vdw,e_hbond,miatomo,ind1,ind2,qaux_x,qaux_y,qaux_z,qaux_w,difx,dify,difz,mod2x,mod2y,mod2z,eps,sig,term6,v,w)	
	for (unsigned int k = 0; k < nconformations; k++)
	{						
		id_conformation = nconfs[k];			
		for(unsigned int i=0;i<atoms_l;i++){					// "j" is related with nparticles of receptor			
			ind1 = ligtype[i];
			qaux_x = quat_x[id_conformation];
			qaux_y = quat_y[id_conformation];
			qaux_z = quat_z[id_conformation];
			qaux_w = quat_w[id_conformation];
			rotate3DPoint_cpp (&qaux_x,&qaux_y,&qaux_z,&qaux_w,&conformations_x[(id_conformation*nlig)+i],&conformations_y[(id_conformation*nlig)+i],&conformations_z[(id_conformation*nlig)+i],miatomo);
			miatomo[0] += moves_x[id_conformation];
			miatomo[1] += moves_y[id_conformation];
			miatomo[2] += moves_z[id_conformation];
					
			for(int j=0;j<atoms_r;j++){				// "i" is related with nparticles of ligand
				e_es = 0;
				e_vdw = 0;
				e_hbond = 0;
				ind2 = rectype[j];
				difx= rec_x[j] - miatomo[0];
				dify= rec_y[j] - miatomo[1];
				difz= rec_z[j] - miatomo[2];
				mod2x=difx*difx;
				mod2y=dify*dify;
				mod2z=difz*difz;
			
				difx=mod2x+mod2y+mod2z;
				dist = sqrtf(difx);
				//eps = sqrtf(vdw_params[ind1].epsilon * vdw_params[ind2].epsilon);
				//sig = vdw_params[ind1].sigma * vdw_params[ind2].sigma;
				eps = sqrtf(f_params[ind1].epsilon * f_params[ind2].epsilon);
				sig = f_params[ind1].sigma * f_params[ind2].sigma;
			
				term6 = sig / difx;
						
				term6 = term6 * term6 * term6; 						
				e_es = ql[i] * (1/dist) * qr[j];
				//e_vdw = 4.0 * eps * term6 * (term6 - 1.0);	
				if (dist > 0.05) e_vdw = 4.0 * eps * term6 * (term6 - 1.0); else e_vdw=10000000;
						
				if (dist > 0.18 && dist < 0.39 && (ind1 == OXI || ind1 == NIT2 || ind1 == AZU)){
					if (ind2 == HBOND){
						sub_h(miatomo, miatomo + 1, miatomo + 2,  rec_x + j, rec_y + j, rec_z + j, v);
						sub_h(&(rec_x[bonds_r[MAXBOND * j]]), &(rec_y[bonds_r[MAXBOND * j]]),&(rec_z[bonds_r[MAXBOND * j]]),rec_x + j, rec_y + j, rec_z + j, w);
						type_data cosT = cosTheta_h (v, w);

						if ( cosT < 0.0 ){
							type_data sinT = sqrt(1.0-cosT*cosT);
							e_vdw *= sinT;
							dify = difx *difx *difx *difx *difx ;
							//e_hbond = -cosT * ( hbond_params[ind1].c12 / (dify*difx)  -  hbond_params[ind1].c10 / dify) ;
							e_hbond = -cosT * ( f_params[ind1].epsilon_h / (dify*difx)  -  f_params[ind1].sigma_h / dify) ;
						}	
					}
				}

				if (dist > 0.18 && dist < 0.39 &&  ind1 == HBOND){
					if (ind2 == OXI || ind2 == NIT2 || ind2 == AZU){				
						sub_h(&(conformations_x[(id_conformation*nlig)+bonds_l[MAXBOND*i]]), &(conformations_y[(id_conformation*nlig)+bonds_l[MAXBOND*i]]), &(conformations_z[(id_conformation*nlig)+bonds_l[MAXBOND*i]]), miatomo, miatomo + 1, miatomo + 2, v);		
						sub_h(rec_x + j, rec_y + j, rec_z + j,miatomo, miatomo + 1, miatomo + 2, w);
						type_data cosT = cosTheta_h (v, w);
						
						if ( cosT < 0.0 ){
							type_data sinT = sqrt(1.0-cosT*cosT);
							e_vdw *= sinT;
							dify = difx *difx *difx *difx *difx;
							//e_hbond += -cosT * (hbond_params[ind2].c12 / (dify*difx) - hbond_params[ind2].c10 / dify );
							e_hbond += -cosT * (f_params[ind2].epsilon_h / (dify*difx) - f_params[ind2].sigma_h / dify );
						}
					}
				}
				temp_es += e_es;
				temp_vdw += e_vdw;
				temp_hbond += e_hbond; 
			}					
		}
		energy[id_conformation] = (temp_es * ES_CONSTANT * weights[2]) + (temp_vdw * weights[0]) + (temp_hbond * weights[1]);	
		temp_es = 0;
		temp_vdw = 0;
		temp_hbond = 0;
	}
}


type_data distance_cpu (type_data *lig_x, type_data *lig_y, type_data *lig_z, int i, int j)
{
	type_data difx,dify,difz,mod2x,mod2y,mod2z,dist;

	difx = lig_x[j] - lig_x[i];
	dify = lig_y[j] - lig_y[i];
	difz = lig_z[j] - lig_z[i];

	mod2x = difx * difx;
	mod2y = dify * dify;
	mod2z = difz * difz;
	
	dist = mod2x + mod2y + mod2z;
	return (dist);

}

//energy_internal_conformations_cpu (n,conformations_x, conformations_y, conformations_z, ligando,vdw_params,energy,individual_bonds_VDW,individual_bonds_ES);

extern void energy_internal_conformations_cpu_type2 (unsigned int n, struct param_t param, struct metaheuristic_t metaheuristic, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, struct ligand_t *ligando, type_data *weights, struct force_field_param_t *f_params, type_data *energy, bool *individual_bonds_VDW, bool *individual_bonds_ES)
{
	char ind1, ind2;
	type_data temp_dist, temp_es, temp_vdw, temp, rij;
	type_data term6, term12, eps, sig;
	double tmpconst,cA,cB,rA,rB,dxA,dxB;
	unsigned int atoms, nlig, id_conformation,h1,h2;
	unsigned int total;
	unsigned int th1;
	
	atoms = ligando->atoms;
	nlig = ligando->nlig;
	total = n * ligando->nlig;
	//recorremos todas las conformaciones
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 2:
			th1 = metaheuristic.Threads1Fit;
			break;
	}

	omp_set_num_threads (th1);
	#pragma omp parallel for private(temp_es,temp_vdw,id_conformation,ind1,ind2,temp_dist,rij,term6,term12,eps,sig,temp,tmpconst,cA,cB,rA,rB,dxA,dxB)				
	for (int k=0; k < total; k+=nlig)
	{	
		temp_es = 0;
		temp_vdw = 0;
		id_conformation = k / nlig;
				
		for (int i=0;i<(atoms-1);i++)
		{
			ind1 = ligando->ligtype[i];
			for (int j=i+1;j<atoms;j++)
			{				
				temp_dist = distance_cpu((conformations_x + k),(conformations_y + k), (conformations_z + k),i,j);
				rij = sqrtf(temp_dist);

                		double dato_eij = calc_ddd_Mehler_Solmajer(rij,APPROX_ZERO);
                		ind2 = ligando->ligtype[j];
                		eps = sqrt(f_params[ind1].epsilon * f_params[ind2].epsilon);
                		sig = ((f_params[ind1].sigma + f_params[ind2].sigma))*0.5L;

                		tmpconst = eps / (double)(xA - xB);
                		cA =  tmpconst * pow( (double)sig, (double)xA ) * xB;
                		cB =  tmpconst * pow( (double)sig, (double)xB ) * xA;
                		dxA = (double) xA;
                		dxB = (double) xB;
                		rA = pow( rij, dxA);
                		rB = pow( rij, dxB);
                		term6 = cB / rB;//pow((double)dist, (double)xB);
                		term12 = cA / rA;//pow((double)dist, (double)xA);
                		temp_es += ((1.0 / (dato_eij * rij)) * ligando->ql[j] * ELECSCALE * weights[2] * !individual_bonds_ES[(i * atoms) + j]);
                		//if (dist < 7.0)
                		temp_vdw += ((term12 - term6) * !individual_bonds_VDW[(i*atoms) + j]);
            		}
			temp_es *= ligando->ql[i];
        	}
        	temp = temp_es + (temp_vdw + weights[0]);
        	energy[id_conformation] = temp;
    	}
}

extern void energy_internal_conformations_cpu (unsigned int n, struct param_t param, struct metaheuristic_t metaheuristic, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, struct ligand_t *ligando, type_data *weights, struct force_field_param_t *f_params, type_data *energy, bool *individual_bonds_VDW, bool *individual_bonds_ES) {

	char ind1, ind2;
	type_data temp_dist, temp_es, temp_vdw, temp, rij;
	type_data term6, term12, eps, sig;
	unsigned int atoms, nlig, id_conformation,h1,h2;
	unsigned int total;
	unsigned int th1;
	
	atoms = ligando->atoms;
	nlig = ligando->nlig;
	total = n * ligando->nlig;
	//recorremos todas las conformaciones
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 2:
			th1 = metaheuristic.Threads1Fit;
			break;
	}

	omp_set_num_threads (th1);
	#pragma omp parallel for private(temp_es,temp_vdw,id_conformation,ind1,ind2,temp_dist,rij,term6,eps,sig,temp)				
	for (int k=0; k < total; k+=nlig)
	{	
		temp_es = 0;
		temp_vdw = 0;
		id_conformation = k / nlig;
				
		for (int i=0;i<(atoms-1);i++)
		{
			ind1 = ligando->ligtype[i];
			for (int j=i+1;j<atoms;j++)
			{				
				temp_dist = distance_cpu((conformations_x + k),(conformations_y + k), (conformations_z + k),i,j);
				rij = sqrtf(temp_dist);
				temp_es += ligando->ql[i] * (1/rij) * ligando->ql[j] * !individual_bonds_ES[(i * atoms) + j];		
			
				ind2 = ligando->ligtype[j];
				//eps = sqrtf(vdw_params[ind1].epsilon * vdw_params[ind2].epsilon);
				//sig = vdw_params[ind1].sigma * vdw_params[ind2].sigma;
				eps = sqrtf(f_params[ind1].epsilon * f_params[ind2].epsilon);
				sig = f_params[ind1].sigma * f_params[ind2].sigma;
				term6 = sig / temp_dist;
				term6 = term6 * term6 * term6; // ^6
				temp_vdw += 4.0*eps*term6*(term6 - 1.0) * !individual_bonds_VDW[(i*atoms) + j];
					
			}
		}
		temp_es *= (ES_CONSTANT * weights[2]);	
		temp = temp_es + (temp_vdw + weights[0]);
		energy[id_conformation] = temp;	
	}
}

extern void energy_total (unsigned int n, struct param_t param, struct metaheuristic_t metaheuristic, type_data *energy_sig, type_data *internal_energy)
{
	switch (param.mode) {
		case 1:	
			for (unsigned int i=0; i < n; i++)
				energy_sig[i] += internal_energy[i];
			break;
		case 2:
			omp_set_num_threads (metaheuristic.Threads1Fit);
			#pragma omp parallel for 
			for (unsigned int i=0; i < n; i++)
			{
				//if((i==0)||(i==1))printf("internal_energy %d %f %f\n",i,internal_energy[i],energy_sig[i]);
				energy_sig[i] += internal_energy[i];
			}
			break;			
	}
}
extern void ForcesCpuSolver_flex(struct param_t param, struct ligand_t ligando, struct receptor_t proteina, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct metaheuristic_t metaheuristic, unsigned int tor)
{
	struct timeval start;
	struct timeval stop;
	unsigned long elapsed;

	gettimeofday(&start,NULL); 

	switch (param.scoring_function_type)
	{
		case 0:		
			forces_CPU_flex(param,proteina.atoms,ligando.atoms,ligando.nlig,proteina.rec_x,proteina.rec_y,proteina.rec_z,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,proteina.rectype,ligando.ligtype,ligando.ql,proteina.qr,proteina.bonds,ligando.bonds,energy,nconfs,weights,f_params,nconformations,metaheuristic);
			break;
		case 1:
			forces_CPU_flex_type1 (param,proteina.atoms,ligando.atoms,ligando.nlig,proteina.rec_x,proteina.rec_y,proteina.rec_z,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,proteina.rectype,ligando.ligtype,ligando.ql,proteina.qr,proteina.bonds,ligando.bonds,energy,nconfs,weights,f_params,nconformations,metaheuristic,tor);
                        break;
		case 2:
			forces_CPU_flex_type2 (param,proteina.atoms,ligando.atoms,ligando.nlig,proteina.rec_x,proteina.rec_y,proteina.rec_z,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,proteina.rectype,ligando.ligtype,ligando.ql,proteina.qr,proteina.bonds,ligando.bonds,energy,nconfs,weights,f_params,nconformations,metaheuristic,tor);
                        break;
	}
	
	gettimeofday(&stop,NULL);
	elapsed = 1000000 * (stop.tv_sec - start.tv_sec);
	elapsed += stop.tv_usec - start.tv_usec;
	
	//for (int i=0;i<n;i++) total += energy[i];
	//printf ("Total Energy: %f\n",total);	
	//printf("Energy[0] = %f\n",energy.energy[0]);
	//printf ("Processing time: %f (ms)\n", elapsed/1000.);
}
