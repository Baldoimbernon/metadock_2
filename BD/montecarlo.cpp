#include "energy_common.h"
#include "rotation.h"
#include "energy_CpuSolver.h"
#include "energy_CpuSolver_flex.h"
#include "energy_CpuSolver_common.h"
#include "metaheuristic_ini_flex.h"
#include "flexibility.h"
#include "wtime.h"
#include <thrust/sequence.h>
#include <omp.h>

void move_mejora_montecarlo_environment (unsigned int nconformations, type_data *move_x_m, type_data *move_y_m, type_data *move_z_m, struct param_t param, struct metaheuristic_t metaheuristic){

        unsigned int th1;
        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 0:
                case 3:
                case 2:
                        th1 = metaheuristic.Threads1Inc;
                        break;
        }

        omp_set_num_threads (th1);
        #pragma omp parallel for
        for (unsigned int i=0; i < nconformations; i++)
        {
                //move_x[i] = move_x_m[i];
                //move_y[i] = move_y_m[i];
                //move_z[i] = move_z_m[i];
                //printf("num_surface %d, NEIIni_selec %d\n",i,j);
                //Bmove_x_m[i] += getRealRandomNumber(param.max_desp); //Desplazamiento entre -max_desp y max_desp
                //Bmove_y_m[i] += getRealRandomNumber(param.max_desp);
                //Bmove_z_m[i] += getRealRandomNumber(param.max_desp);
		move_x_m[i] += getRealRandomNumber_r(param.max_desp,param.automatic_seed,param.seed,i+param.seed);
		move_y_m[i] += getRealRandomNumber_r(param.max_desp,param.automatic_seed,param.seed,i+param.seed);
		move_z_m[i] += getRealRandomNumber_r(param.max_desp,param.automatic_seed,param.seed,i+param.seed);
                //printf("Estoy en Move, coformacion %d\n",i);
                //printf("coformation %d, move_x[%d] = %f, move_y[%d] = %f, move_z[%d] = %f, aleatorio = %f\n",i,i*BLOCK_128+j,moves_x_h[i*BLOCK_128+j],i*BLOCK_128+j,moves_y_h[i*BLOCK_128+j],i*BLOCK_128+j,moves_z_h[i*BLOCK_128+j],2 * frand());
        }

}

void move_mejora_montecarlo (unsigned int nconformations, type_data *move_x_m, type_data *move_y_m, type_data *move_z_m, type_data *move_x, type_data *move_y, type_data *move_z, struct param_t param, struct metaheuristic_t metaheuristic){
	
	unsigned int th1;
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 0:
		case 3:
		case 2:
			th1 = metaheuristic.Threads1Inc;
			break;
	}					
			
	omp_set_num_threads (th1);
	#pragma omp parallel for 	
	for (unsigned int i=0; i < nconformations; i++)
	{	
		move_x[i] = move_x_m[i];
		move_y[i] = move_y_m[i];
		move_z[i] = move_z_m[i];
		//printf("num_surface %d, NEIIni_selec %d\n",i,j);
		move_x_m[i] += getRealRandomNumber(param.max_desp); //Desplazamiento entre -max_desp y max_desp
		move_y_m[i] += getRealRandomNumber(param.max_desp);
		move_z_m[i] += getRealRandomNumber(param.max_desp);
		//printf("Estoy en Move, coformacion %d\n",i);
		//printf("coformation %d, move_x[%d] = %f, move_y[%d] = %f, move_z[%d] = %f, aleatorio = %f\n",i,i*BLOCK_128+j,moves_x_h[i*BLOCK_128+j],i*BLOCK_128+j,moves_y_h[i*BLOCK_128+j],i*BLOCK_128+j,moves_z_h[i*BLOCK_128+j],2 * frand());
	}

}


void rotate_montecarlo_environment (type_data *quat_x_m, type_data *quat_y_m, type_data *quat_z_m, type_data *quat_w_m, struct param_t param, unsigned int nconformations, struct metaheuristic_t metaheuristic){

        type_data angle, eje[3];
        unsigned int dato,th1;
        type_data local_x, quat_tmp_x, local_y, quat_tmp_y, local_z, quat_tmp_z, local_w, quat_tmp_w ;

        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 0:
                case 3:
                case 2:
                        th1 = metaheuristic.Threads1Inc;
                        break;
        }
        omp_set_num_threads (th1);
        #pragma omp parallel for private (local_x, quat_tmp_x, local_y, quat_tmp_y, local_z, quat_tmp_z, local_w, quat_tmp_w,eje,angle)
        for (unsigned int i=0; i < nconformations;i++)
        {
                //quat_x[i] = quat_x_m[i];
                //quat_y[i] = quat_y_m[i];
                //quat_z[i] = quat_z_m[i];
                //quat_w[i] = quat_w_m[i];

                quat_tmp_x = quat_x_m[i];
                quat_tmp_y = quat_y_m[i];
                quat_tmp_z = quat_z_m[i];
                quat_tmp_w = quat_w_m[i];
                angle = getRealRandomNumber(param.rotation);
                eje[0] = getRealRandomNumber (1);
                eje[1] = getRealRandomNumber (1);
                eje[2] = getRealRandomNumber (1);

                setRotation_cpp (&local_x, &local_y, &local_z, &local_w, angle, eje);
                composeRotation_cpp (&local_x, &local_y, &local_z, &local_w, &quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w, &quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w);
                normalize_cpp(&quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w );

                quat_x_m[i] = quat_tmp_x;
                quat_y_m[i] = quat_tmp_y;
                quat_z_m[i] = quat_tmp_z;
                quat_w_m[i] = quat_tmp_w;
                //printf("Estoy en Rotation, conformacion %d, eje[0] = %f, eje[1] = %f, eje[2] = %f, x = %f, y = %f, z= %f, w = %f\n",i,eje[0],eje[1],eje[2],quat_tmp.x,quat_tmp.y,quat_tmp.z,quat_tmp.w);
        }

}

void rotate_montecarlo (type_data *quat_x_m, type_data *quat_y_m, type_data *quat_z_m, type_data *quat_w_m, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, struct param_t param, unsigned int nconformations, struct metaheuristic_t metaheuristic){

	type_data angle, eje[3];
	unsigned int dato,th1;
	type_data local_x, quat_tmp_x, local_y, quat_tmp_y, local_z, quat_tmp_z, local_w, quat_tmp_w ;
	
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 0:
		case 3:
		case 2:
			th1 = metaheuristic.Threads1Inc;
			break;
	}			
	omp_set_num_threads (th1);
	#pragma omp parallel for private (local_x, quat_tmp_x, local_y, quat_tmp_y, local_z, quat_tmp_z, local_w, quat_tmp_w,eje,angle)	
	for (unsigned int i=0; i < nconformations;i++)
	{
		quat_x[i] = quat_x_m[i];
		quat_y[i] = quat_y_m[i];
		quat_z[i] = quat_z_m[i];
		quat_w[i] = quat_w_m[i];
			
		quat_tmp_x = quat_x_m[i];	
		quat_tmp_y = quat_y_m[i];							
		quat_tmp_z = quat_z_m[i];							
		quat_tmp_w = quat_w_m[i];							
		angle = getRealRandomNumber(param.rotation);
		eje[0] = getRealRandomNumber (1);
		eje[1] = getRealRandomNumber (1);
		eje[2] = getRealRandomNumber (1);

		setRotation_cpp (&local_x, &local_y, &local_z, &local_w, angle, eje);				
		composeRotation_cpp (&local_x, &local_y, &local_z, &local_w, &quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w, &quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w);
		normalize_cpp(&quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w );

		quat_x_m[i] = quat_tmp_x;
		quat_y_m[i] = quat_tmp_y;
		quat_z_m[i] = quat_tmp_z;
		quat_w_m[i] = quat_tmp_w;
		//printf("Estoy en Rotation, conformacion %d, eje[0] = %f, eje[1] = %f, eje[2] = %f, x = %f, y = %f, z= %f, w = %f\n",i,eje[0],eje[1],eje[2],quat_tmp.x,quat_tmp.y,quat_tmp.z,quat_tmp.w);
	}
			
}

void updateMove (unsigned int nconformations, type_data *move_x_m, type_data *move_y_m, type_data *move_z_m, type_data *move_x, type_data *move_y, type_data *move_z, type_data *energy_sig, type_data *energy_ant, struct param_t param, unsigned int tam, struct metaheuristic_t metaheuristic)
{
	type_data cond;
	unsigned int th1;
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 0:
		case 3:
		case 2:
			th1 = metaheuristic.Threads1Inc;
			break;
	}			
	omp_set_num_threads (th1);
	#pragma omp parallel for private (cond) 
	for (unsigned int id=0; id < nconformations; id++){				
		cond = (energy_sig[id] < energy_ant[id] || getRealRandomNumber(param.max_desp) <= exp(-(energy_sig[id] - energy_ant[id])) );
		if (cond)
		{
			move_x[id] = move_x_m[id] * cond + move_x[id] * !cond;
			move_y[id] = move_y_m[id] * cond + move_y[id] * !cond;
			move_z[id] = move_z_m[id] * cond + move_z[id] * !cond;
		}
		else
		{
			move_x_m[id] = move_x[id] * !cond;
			move_y_m[id] = move_y[id] * !cond;
			move_z_m[id] = move_z[id] * !cond;
		}
		energy_ant[id] = energy_sig[id] * cond + energy_ant[id] * !cond;
	}
}

void updateMove_environment (unsigned int nconformations, type_data *move_x_m, type_data *move_y_m, type_data *move_z_m, type_data *move_x, type_data *move_y, type_data *move_z, type_data *energy_sig, type_data *energy_ant, struct param_t param, struct metaheuristic_t metaheuristic)
{
        type_data cond;
        unsigned int th1;
        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 0:
                case 3:
                case 2:
                        th1 = metaheuristic.Threads1Inc;
                        break;
        }
	//printf("update\n");
	//exit(0);
        omp_set_num_threads (th1);
        #pragma omp parallel for private (cond)
        for (unsigned int id=0; id < nconformations; id++){

		for (unsigned int j=0; j < metaheuristic.NEEImp; j++)
		{
                	cond = (energy_sig[id*metaheuristic.NEEImp + j] < energy_ant[id] || getRealRandomNumber(param.max_desp) <= exp(-(energy_sig[id*metaheuristic.NEEImp + j] - energy_ant[id])) );
			//printf("cond %d\n",cond);
                	if (cond)
               		{
                        	move_x[id] = move_x_m[id*metaheuristic.NEEImp + j] * cond + move_x[id] * !cond;
                        	move_y[id] = move_y_m[id*metaheuristic.NEEImp + j] * cond + move_y[id] * !cond;
                        	move_z[id] = move_z_m[id*metaheuristic.NEEImp + j] * cond + move_z[id] * !cond;
                	}
                	else
                	{
                        	move_x_m[id*metaheuristic.NEEImp + j] = move_x[id] * !cond;
                        	move_y_m[id*metaheuristic.NEEImp + j] = move_y[id] * !cond;
                        	move_z_m[id*metaheuristic.NEEImp + j] = move_z[id] * !cond;
                	}
                	energy_ant[id] = energy_sig[id*metaheuristic.NEEImp + j] * cond + energy_ant[id] * !cond;
		}
        }

}

void updateRotation (type_data *quat_x_m, type_data *quat_y_m, type_data *quat_z_m, type_data *quat_w_m, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy_sig, type_data *energy_ant, struct param_t param, unsigned int nconformations, struct metaheuristic_t metaheuristic)
{
	type_data cond;
	unsigned int th1;
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 0:
		case 3:
		case 2:
			th1 = metaheuristic.Threads1Inc;
			break;
	}		
	omp_set_num_threads (th1);
	#pragma omp parallel for private (cond)
	for (unsigned int id=0; id < nconformations; id++)
	{				
		cond = (energy_sig[id] < energy_ant[id] || getRealRandomNumber(param.max_desp) <= exp(-(energy_sig[id] - energy_ant[id])) );
		if (cond)
		{
			quat_x[id] = quat_x_m[id] * cond + quat_x[id] * !cond;		
			quat_y[id] = quat_y_m[id] * cond + quat_y[id] * !cond;
			quat_z[id] = quat_z_m[id] * cond + quat_z[id] * !cond;
			quat_w[id] = quat_w_m[id] * cond + quat_w[id] * !cond;
		}
		else
		{
			quat_x_m[id] = quat_x[id] * !cond;
			quat_y_m[id] = quat_y[id] * !cond;
			quat_z_m[id] = quat_z[id] * !cond;
			quat_w_m[id] = quat_w[id] * !cond;
		}
		energy_ant[id] = energy_sig[id] * cond + energy_ant[id] * !cond;
	}
}

void updateRotation_environment (type_data *quat_x_m, type_data *quat_y_m, type_data *quat_z_m, type_data *quat_w_m, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy_sig, type_data *energy_ant, struct param_t param, unsigned int nconformations, struct metaheuristic_t metaheuristic)
{
        type_data cond;
        unsigned int th1;
        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 0:
                case 3:
                case 2:
                        th1 = metaheuristic.Threads1Inc;
                        break;
        }
        omp_set_num_threads (th1);
        #pragma omp parallel for private (cond)
        for (unsigned int id=0; id < nconformations; id++)
        {
		for (unsigned int j=0; j < metaheuristic.NEEImp; j++)
		{
                	cond = (energy_sig[id*metaheuristic.NEEImp + j] < energy_ant[id] || getRealRandomNumber(param.max_desp) <= exp(-(energy_sig[id*metaheuristic.NEEImp + j] - energy_ant[id])) );
                	if (cond)
                	{
                        	quat_x[id] = quat_x_m[id*metaheuristic.NEEImp + j] * cond + quat_x[id] * !cond;
                        	quat_y[id] = quat_y_m[id*metaheuristic.NEEImp + j] * cond + quat_y[id] * !cond;
                        	quat_z[id] = quat_z_m[id*metaheuristic.NEEImp + j] * cond + quat_z[id] * !cond;
                        	quat_w[id] = quat_w_m[id*metaheuristic.NEEImp + j] * cond + quat_w[id] * !cond;
                	}
                	else
                	{
                        	quat_x_m[id*metaheuristic.NEEImp + j] = quat_x[id] * !cond;
                        	quat_y_m[id*metaheuristic.NEEImp + j] = quat_y[id] * !cond;
                        	quat_z_m[id*metaheuristic.NEEImp + j] = quat_z[id] * !cond;
                        	quat_w_m[id*metaheuristic.NEEImp + j] = quat_w[id] * !cond;
                	}
                	energy_ant[id] = energy_sig[id*metaheuristic.NEEImp + j] * cond + energy_ant[id] * !cond;
		}
        }
}

extern void montecarlo_cpp_environment (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, unsigned int num_surface, type_data *weights, struct force_field_param_t *f_params, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int nconformations, struct metaheuristic_t metaheuristic, unsigned int opt)
{
        struct vectors_t vectors_mejora;
        unsigned int steps,moveType=MOVE,bucle,fase;
        double tenergymejora;
        double tenergymejora_t;
        
        vectors_mejora.num_surface = num_surface;
        vectors_mejora.nconformations = nconformations * metaheuristic.NEEImp;

        vectors_mejora.move_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.move_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.move_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations);
        vectors_mejora.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.weights = (type_data *)malloc(sizeof(type_data)*SCORING_TERMS);
        //printf("Memoria para mejora creada....\n");
	memcpy(vectors_mejora.weights, weights,sizeof(type_data)*SCORING_TERMS);
        memcpy(vectors_mejora.f_params, f_params,MAXTYPES * sizeof(struct force_field_param_t));

	thrust::sequence(vectors_mejora.energy.n_conformation,vectors_mejora.energy.n_conformation + vectors_mejora.nconformations);
	
        fill_conformations_environment (move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,nconformations,param,metaheuristic);
	
	steps = 0;
        if (opt == 0)
        {
                bucle = metaheuristic.IMEIni;
        }
        if (opt == 1)
        {
                bucle = metaheuristic.IMEImp;
        }
        if (opt == 2)
        {
                bucle = metaheuristic.IMEMUCom;
        }
        moveType = MOVE;
        //printf("Voy a mejorar....\n");
        while (steps < bucle){
                if (moveType == MOVE){
                        move_mejora_montecarlo_environment(vectors_mejora.nconformations,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,param,metaheuristic);
                        //printf("He movido\n");
                }
                else {
                        rotate_montecarlo_environment(vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,param,vectors_mejora.nconformations,metaheuristic);
                }
                //printf("Voy a calcular la energia...\n");

                tenergymejora = wtime();
                ForcesCpuSolver(param,ligando,proteina,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,vectors_mejora.energy.energy,vectors_mejora.energy.n_conformation,vectors_mejora.weights,vectors_mejora.f_params,vectors_mejora.nconformations,metaheuristic);
                tenergymejora_t += wtime() - tenergymejora;

		if (moveType == MOVE)
                {
                        updateMove_environment (nconformations,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,move_x,move_y,move_z,vectors_mejora.energy.energy,energy,param,metaheuristic);
                        moveType = ROTATE;
                }
                else
                {
                        updateRotation_environment(vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,quat_x,quat_y,quat_z,quat_w,vectors_mejora.energy.energy,energy,param,nconformations,metaheuristic);
                        moveType = MOVE;
                }
		//fill_conformations_environment (move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,nconformations,param,metaheuristic);
		//for (int i=0;i<vectors_mejora.nconformations;i++) printf("id %d mx %f my %f mz %f qx %f qy %f qz %f qw %f en %f num %d\n",i,vectors_mejora.move_x[i],vectors_mejora.move_y[i],vectors_mejora.move_z[i],vectors_mejora.quat_x[i],vectors_mejora.quat_y[i],vectors_mejora.quat_z[i],vectors_mejora.quat_w[i],vectors_mejora.energy.energy[i],vectors_mejora.energy.n_conformation[i]);
                steps++;
        }
        free(vectors_mejora.move_x);
        free(vectors_mejora.move_y);
        free(vectors_mejora.move_z);
        free(vectors_mejora.quat_x);
        free(vectors_mejora.quat_y);
        free(vectors_mejora.quat_z);
        free(vectors_mejora.quat_w);
        free(vectors_mejora.energy.energy);
        free(vectors_mejora.energy.n_conformation);
        free(vectors_mejora.weights);


}

extern void montecarlo_cpp (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, unsigned int num_surface, type_data *weights, struct force_field_param_t *f_params, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int nconformations, struct metaheuristic_t metaheuristic, unsigned int opt)
{
	struct vectors_t vectors_mejora;
	unsigned int steps,moveType=MOVE,bucle,fase;	
	double tenergymejora;
	double tenergymejora_t;
	unsigned int tam = nconformations / num_surface;
	vectors_mejora.num_surface = num_surface;
	vectors_mejora.nconformations = nconformations;

	vectors_mejora.move_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.move_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.move_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations);
	vectors_mejora.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);	
	vectors_mejora.weights = (type_data *)malloc(sizeof(type_data)*SCORING_TERMS);
 	//printf("Memoria para mejora creada....\n");
	
	memcpy(vectors_mejora.move_x, move_x, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.move_y, move_y, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.move_z, move_z, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_x, quat_x, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_y, quat_y, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_z, quat_z, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_w, quat_w, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.energy.energy, energy, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.energy.n_conformation, nconfs, vectors_mejora.nconformations * sizeof(int));
	//memcpy(vectors_mejora.vdw_params, vdw_params,MAXTYPES * sizeof(struct vdw_param_t)); 	
	//memcpy(vectors_mejora.sasa_params, sasa_params,MAXTYPES * sizeof(struct sasa_param_t)); 	
	//memcpy(vectors_mejora.hbond_params, hbond_params,MAXTYPES * sizeof(struct hbond_param_t)); 		
        memcpy(vectors_mejora.weights, weights,sizeof(type_data)*SCORING_TERMS);
	memcpy(vectors_mejora.f_params, f_params,MAXTYPES * sizeof(struct force_field_param_t));

	
	steps = 0;
	if (opt == 0) 
	{
		bucle = metaheuristic.IMEIni;
	}
	if (opt == 1) 
	{	
		bucle = metaheuristic.IMEImp;	
	}
	if (opt == 2)
	{
		bucle = metaheuristic.IMEMUCom;
	}
	moveType = MOVE;
	//printf("Voy a mejorar....\n");
	while (steps < bucle){		
		if (moveType == MOVE){
			move_mejora_montecarlo(vectors_mejora.nconformations,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,move_x,move_y,move_z,param,metaheuristic);			
			//printf("He movido\n");
		}
		else {
			rotate_montecarlo(vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,quat_x,quat_y,quat_z,quat_w,param,vectors_mejora.nconformations,metaheuristic);
		}		
		//printf("Voy a calcular la energia...\n");
		tenergymejora = wtime();
		ForcesCpuSolver(param,ligando,proteina,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,vectors_mejora.energy.energy,vectors_mejora.energy.n_conformation,vectors_mejora.weights,vectors_mejora.f_params,vectors_mejora.nconformations,metaheuristic);		
		tenergymejora_t += wtime() - tenergymejora;
		if (moveType == MOVE)
		{
			updateMove (vectors_mejora.nconformations,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,move_x,move_y,move_z,vectors_mejora.energy.energy,energy,param,tam,metaheuristic);
			moveType = ROTATE;
		}
		else
		{
			updateRotation(vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,quat_x,quat_y,quat_z,quat_w,vectors_mejora.energy.energy,energy,param,vectors_mejora.nconformations,metaheuristic);
			moveType = MOVE;
		}
		steps++;	
	}
	free(vectors_mejora.move_x);
	free(vectors_mejora.move_y);
	free(vectors_mejora.move_z);
	free(vectors_mejora.quat_x);
	free(vectors_mejora.quat_y);
	free(vectors_mejora.quat_z);
	free(vectors_mejora.quat_w);
	free(vectors_mejora.energy.energy);
	free(vectors_mejora.energy.n_conformation);
	free(vectors_mejora.weights);
}

extern void montecarlo_cpp_flex (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, unsigned int num_surface, type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int nconformations, struct metaheuristic_t metaheuristic, struct flexibility_params_t *flexibility_params, struct flexibility_data_t *flexibility_conformations, unsigned int opt)
{
	struct vectors_t vectors_mejora;
	unsigned int steps,moveType=MOVE,bucle,total,simulations,option;	
	type_data max_angle_flex;
	unsigned int tam = nconformations / num_surface;
	vectors_mejora.num_surface = num_surface;
	vectors_mejora.nconformations = nconformations;

	vectors_mejora.conformations_x = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_mejora.nconformations);
	vectors_mejora.conformations_y = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_mejora.nconformations);
	vectors_mejora.conformations_z = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_mejora.nconformations);
	vectors_mejora.move_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.move_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.move_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations);
	vectors_mejora.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);	
	vectors_mejora.weights = (type_data *)malloc(SCORING_TERMS*sizeof(type_data));
 	//printf("Memoria para mejora creada....\n");
	type_data *internal_energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	flexibility_conformations = (flexibility_data_t *)malloc(sizeof(flexibility_data_t)*vectors_mejora.nconformations);
	
	memcpy(vectors_mejora.conformations_x, conformations_x,vectors_mejora.nconformations * ligando.nlig * sizeof(type_data)); 
	memcpy(vectors_mejora.conformations_y, conformations_y,vectors_mejora.nconformations * ligando.nlig * sizeof(type_data)); 
	memcpy(vectors_mejora.conformations_z, conformations_z,vectors_mejora.nconformations * ligando.nlig * sizeof(type_data)); 
	memcpy(vectors_mejora.move_x, move_x, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.move_y, move_y, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.move_z, move_z, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_x, quat_x, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_y, quat_y, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_z, quat_z, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_w, quat_w, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.energy.energy, energy, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.energy.n_conformation, nconfs, vectors_mejora.nconformations * sizeof(int));

	//memcpy(vectors_mejora.vdw_params, vdw_params,MAXTYPES * sizeof(struct vdw_param_t)); 	
	//memcpy(vectors_mejora.sasa_params, sasa_params,MAXTYPES * sizeof(struct sasa_param_t)); 	
	//memcpy(vectors_mejora.hbond_params, hbond_params,MAXTYPES * sizeof(struct hbond_param_t)); 	
	memcpy(vectors_mejora.f_params, f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_mejora.weights, weights,SCORING_TERMS * sizeof(type_data));
	
	if (opt == 0)
	{
		total = metaheuristic.IMEIni + metaheuristic.IMEFlex;
		simulations = metaheuristic.IMEIni;
	}
	if (opt == 1)
	{
		total = metaheuristic.IMEImp + metaheuristic.IMEFlex;
		simulations = metaheuristic.IMEImp;
	}
	if (opt == 2)
	{
	 	total = metaheuristic.IMEMUCom + metaheuristic.IMEFlex;
                simulations = metaheuristic.IMEMUCom;
	}
	
	steps = 0;	
	option = 0;
	max_angle_flex = param.flex_angle;
	
	while (steps < total){

		//printf("step %d\n", steps);
		//REFINAMIENTO DEL ANGULO FLEXIBLE, MOVIMIENTO Y ORIENTACIÓN DEL LIGANDO
		if (steps == simulations) max_angle_flex = param.flex_angle;
		if (((steps % 50) == 0) && (steps > 0) && (steps <simulations)) 
			max_angle_flex /= 1.2;
		
			
		if (steps >=simulations)
		{
			//FLEXIBILIDAD
			angulations_conformations_cpp (vectors_mejora.nconformations,metaheuristic,param,flexibility_conformations,flexibility_params->n_links,param.flex_angle);
			flexibility_cpp (vectors_mejora.nconformations,metaheuristic,param,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,flexibility_conformations,ligando.nlig,flexibility_params->n_links,flexibility_params->n_fragments,flexibility_params->links,flexibility_params->links_fragments,flexibility_params->fragments,flexibility_params->fragments_tam,param.flex_angle);									
			max_angle_flex /= 1.1;
		}
		else
		{
			option = steps % 3;
			switch (option)
			{
			case 0:
				//MUEVE LAS CONFORMACIONES.
				move_mejora_montecarlo(vectors_mejora.nconformations,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,move_x,move_y,move_z,param,metaheuristic);
				break;
			case 1:
				//ROTA LAS CONFORMACIONES.
				rotate_montecarlo(vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,quat_x,quat_y,quat_z,quat_w,param,vectors_mejora.nconformations,metaheuristic);
				break;
			case 2:
				//FLEXIBILIDAD
				angulations_conformations_cpp (vectors_mejora.nconformations,metaheuristic,param,flexibility_conformations,flexibility_params->n_links,param.flex_angle);
				flexibility_cpp (vectors_mejora.nconformations,metaheuristic,param,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,flexibility_conformations,ligando.nlig,flexibility_params->n_links,flexibility_params->n_fragments,flexibility_params->links,flexibility_params->links_fragments,flexibility_params->fragments,flexibility_params->fragments_tam,param.flex_angle);									
				break;
			}
		}
		
		//ENERGIA
		forces_CPU_flex (param,proteina.atoms,ligando.atoms,ligando.nlig,proteina.rec_x,proteina.rec_y,proteina.rec_z,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,proteina.rectype,ligando.ligtype,ligando.ql,proteina.qr,proteina.bonds,ligando.bonds,vectors_mejora.energy.energy,vectors_mejora.energy.n_conformation,vectors_mejora.weights,vectors_mejora.f_params,vectors_mejora.nconformations,metaheuristic);
		energy_internal_conformations_cpu (vectors_mejora.nconformations,param,metaheuristic,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,&ligando,vectors_mejora.weights,vectors_mejora.f_params,internal_energy,flexibility_params->individual_bonds_VDW,flexibility_params->individual_bonds_ES);
		energy_total (vectors_mejora.nconformations,param,metaheuristic,vectors_mejora.energy.energy,internal_energy);
		if (steps >=simulations)
		{
			//FLEXIBILIDAD			
			update_angulations_conformations_cpp (vectors_mejora.nconformations,metaheuristic,param,flexibility_conformations,vectors_mejora.energy.energy,energy);
			flexibility_cpp (vectors_mejora.nconformations,metaheuristic,param,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,flexibility_conformations,ligando.nlig,flexibility_params->n_links,flexibility_params->n_fragments,flexibility_params->links,flexibility_params->links_fragments,flexibility_params->fragments,flexibility_params->fragments_tam,param.flex_angle);
			incluir_mejorar_inicializar_flex(ligando.nlig,vectors_mejora,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,num_surface,nconformations,metaheuristic,param);
		}
		else
		{
			switch (option)
			{
			case 0:
				updateMove (vectors_mejora.nconformations,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,move_x,move_y,move_z,vectors_mejora.energy.energy,energy,param,tam,metaheuristic);
				
				break;
			case 1:
				updateRotation(vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,quat_x,quat_y,quat_z,quat_w,vectors_mejora.energy.energy,energy,param,vectors_mejora.nconformations,metaheuristic);
				break;
			case 2:
				update_angulations_conformations_cpp (vectors_mejora.nconformations,metaheuristic,param,flexibility_conformations,vectors_mejora.energy.energy,energy);
				flexibility_cpp (vectors_mejora.nconformations,metaheuristic,param,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,flexibility_conformations,ligando.nlig,flexibility_params->n_links,flexibility_params->n_fragments,flexibility_params->links,flexibility_params->links_fragments,flexibility_params->fragments,flexibility_params->fragments_tam,param.flex_angle);
				incluir_mejorar_inicializar_flex(ligando.nlig,vectors_mejora,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,num_surface,nconformations,metaheuristic,param);					
				break;
			}
		}
		//incluir_mejorar_inicializar_flex(ligando.nlig,vectors_mejora,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,num_surface,nconformations,metaheuristic,param);	
		steps++;
	}
//	for (unsigned int i = 0; i < vectors_mejora.nconformations; i++)
//		for (unsigned int j = 0; j < ligando.nlig;j++)
//			printf("id %d * x %f y %f z %f * mx %f my %f mz %f * qx %f qy %f qz %f qw %f *  energy[%d] = %f\n",i,conformations_x[i*ligando.nlig + j],conformations_y[i*ligando.nlig + j],conformations_z[i*ligando.nlig + j],move_x[i],move_y[i],move_z[i],quat_x[i],quat_y[i],quat_z[i],quat_w[i],i,energy[i]);
	//exit(0);
	free(internal_energy);
	free(flexibility_conformations);
	free(vectors_mejora.conformations_x);
	free(vectors_mejora.conformations_y);
	free(vectors_mejora.conformations_z);
	free(vectors_mejora.move_x);
	free(vectors_mejora.move_y);
	free(vectors_mejora.move_z);
	free(vectors_mejora.quat_x);
	free(vectors_mejora.quat_y);
	free(vectors_mejora.quat_z);
	free(vectors_mejora.quat_w);
	free(vectors_mejora.energy.energy);
	free(vectors_mejora.energy.n_conformation);
	free(vectors_mejora.weights);
	//printf("End Mont\n");
	//exit(0);
}

extern void montecarlo_cpp_flex_environment (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, unsigned int num_surface, type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int nconformations, struct metaheuristic_t metaheuristic, struct flexibility_params_t *flexibility_params, struct flexibility_data_t *flexibility_conformations, unsigned int opt)
{
        struct vectors_t vectors_mejora;
        unsigned int steps,moveType=MOVE,bucle,total,simulations,option;
        type_data max_angle_flex;
        unsigned int tam = nconformations / num_surface;
        vectors_mejora.num_surface = num_surface;
        vectors_mejora.nconformations = nconformations * metaheuristic.NEEImp;

        vectors_mejora.conformations_x = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_mejora.nconformations);
        vectors_mejora.conformations_y = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_mejora.nconformations);
        vectors_mejora.conformations_z = (type_data *)malloc(sizeof(type_data)*ligando.nlig*vectors_mejora.nconformations);
        vectors_mejora.move_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.move_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.move_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations);
        vectors_mejora.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.weights = (type_data *)malloc(SCORING_TERMS*sizeof(type_data));
        //printf("Memoria para mejora creada....\n");
        type_data *internal_energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        flexibility_conformations = (flexibility_data_t *)malloc(sizeof(flexibility_data_t)*vectors_mejora.nconformations);

	memcpy(vectors_mejora.f_params, f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_mejora.weights, weights,SCORING_TERMS * sizeof(type_data));

	thrust::sequence(vectors_mejora.energy.n_conformation,vectors_mejora.energy.n_conformation + vectors_mejora.nconformations);

	fill_conformations_environment_flex (ligando.nlig,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w, vectors_mejora.conformations_x, vectors_mejora.conformations_y, vectors_mejora.conformations_z, vectors_mejora.move_x, vectors_mejora.move_y, vectors_mejora.move_z, vectors_mejora.quat_x, vectors_mejora.quat_y, vectors_mejora.quat_z, vectors_mejora.quat_w, nconformations, param, metaheuristic);

        if (opt == 0)
        {
                total = metaheuristic.IMEIni + metaheuristic.IMEFlex;
                simulations = metaheuristic.IMEIni;
        }
        if (opt == 1)
        {
                total = metaheuristic.IMEImp + metaheuristic.IMEFlex;
                simulations = metaheuristic.IMEImp;
        }
        if (opt == 2)
        {
                total = metaheuristic.IMEMUCom + metaheuristic.IMEFlex;
                simulations = metaheuristic.IMEMUCom;
        }

        steps = 0;
        option = 0;
        max_angle_flex = param.flex_angle;

	 while (steps < total){

                //printf("step %d\n", steps);
                //REFINAMIENTO DEL ANGULO FLEXIBLE, MOVIMIENTO Y ORIENTACIÓN DEL LIGANDO
                if (steps == simulations) max_angle_flex = param.flex_angle;
                if (((steps % 50) == 0) && (steps > 0) && (steps <simulations))
                        max_angle_flex /= 1.2;

		//for (int l=0;l<metaheuristic.NEEImp;l++) printf("ANTES conf %d, mx %f my %f mz %f qx %f qy %f qz %f qw %f energy %f\n",l,vectors_mejora.move_x[l],vectors_mejora.move_y[l],vectors_mejora.move_z[l],vectors_mejora.quat_x[l],vectors_mejora.quat_y[l],vectors_mejora.quat_z[l],vectors_mejora.quat_w[l],vectors_mejora.energy.energy[l]);
                //exit(0);
                if (steps >=simulations)
                {
                        //FLEXIBILIDAD
                        angulations_conformations_cpp (vectors_mejora.nconformations,metaheuristic,param,flexibility_conformations,flexibility_params->n_links,param.flex_angle);
                        flexibility_cpp (vectors_mejora.nconformations,metaheuristic,param,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,flexibility_conformations,ligando.nlig,flexibility_params->n_links,flexibility_params->n_fragments,flexibility_params->links,flexibility_params->links_fragments,flexibility_params->fragments,flexibility_params->fragments_tam,param.flex_angle);
                        max_angle_flex /= 1.1;
                }
                else
                {
                        option = steps % 3;
                        switch (option)
                        {
                        case 0:
                                //MUEVE LAS CONFORMACIONES.
                                move_mejora_montecarlo_environment(vectors_mejora.nconformations,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,param,metaheuristic);
                                break;
                        case 1:
                                //ROTA LAS CONFORMACIONES.
                                rotate_montecarlo_environment(vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,param,vectors_mejora.nconformations,metaheuristic);
                                break;
                        case 2:
                                //FLEXIBILIDAD
                                angulations_conformations_cpp (vectors_mejora.nconformations,metaheuristic,param,flexibility_conformations,flexibility_params->n_links,param.flex_angle);
				//for (int l=0;l<vectors_mejora.nconformations;l++) printf("conf %d ang %f\n",l,flexibility_conformations[l].ang);
                                flexibility_cpp (vectors_mejora.nconformations,metaheuristic,param,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,flexibility_conformations,ligando.nlig,flexibility_params->n_links,flexibility_params->n_fragments,flexibility_params->links,flexibility_params->links_fragments,flexibility_params->fragments,flexibility_params->fragments_tam,param.flex_angle);
                                break;
                        }
                }

		//ENERGIA
                forces_CPU_flex (param,proteina.atoms,ligando.atoms,ligando.nlig,proteina.rec_x,proteina.rec_y,proteina.rec_z,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,proteina.rectype,ligando.ligtype,ligando.ql,proteina.qr,proteina.bonds,ligando.bonds,vectors_mejora.energy.energy,vectors_mejora.energy.n_conformation,vectors_mejora.weights,vectors_mejora.f_params,vectors_mejora.nconformations,metaheuristic);
                energy_internal_conformations_cpu (vectors_mejora.nconformations,param,metaheuristic,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,&ligando,vectors_mejora.weights,vectors_mejora.f_params,internal_energy,flexibility_params->individual_bonds_VDW,flexibility_params->individual_bonds_ES);
                energy_total (vectors_mejora.nconformations,param,metaheuristic,vectors_mejora.energy.energy,internal_energy);
		//for (int l=0;l<metaheuristic.NEEImp;l++) printf("ENERGIA conf %d, mx %f my %f mz %f qx %f qy %f qz %f qw %f energy %f\n",l,vectors_mejora.move_x[l],vectors_mejora.move_y[l],vectors_mejora.move_z[l],vectors_mejora.quat_x[l],vectors_mejora.quat_y[l],vectors_mejora.quat_z[l],vectors_mejora.quat_w[l],vectors_mejora.energy.energy[l]);
                if (steps >=simulations)
                {
                        //FLEXIBILIDAD
                        update_angulations_conformations_cpp_environment (nconformations,metaheuristic,param,flexibility_conformations,vectors_mejora.energy.energy,energy);
                        flexibility_cpp (vectors_mejora.nconformations,metaheuristic,param,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,flexibility_conformations,ligando.nlig,flexibility_params->n_links,flexibility_params->n_fragments,flexibility_params->links,flexibility_params->links_fragments,flexibility_params->fragments,flexibility_params->fragments_tam,param.flex_angle);
			//incluir_mejorar_inicializar_environment_flex(ligando.nlig,vectors_mejora,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,nconformations,metaheuristic,param);
                }
                else
                {
                        switch (option)
                        {
                        case 0:
                                //updateMove (vectors_mejora.nconformations,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,move_x,move_y,move_z,vectors_mejora.energy.energy,energy,param,tam,metaheuristic);
				updateMove_environment (nconformations,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,move_x,move_y,move_z,vectors_mejora.energy.energy,energy,param,metaheuristic);
                                break;
                        case 1:
                                //updateRotation(vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,quat_x,quat_y,quat_z,quat_w,vectors_mejora.energy.energy,energy,param,vectors_mejora.nconformations,metaheuristic);
				updateRotation_environment (vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,quat_x,quat_y,quat_z,quat_w,vectors_mejora.energy.energy,energy,param,nconformations,metaheuristic);
                                break;
                        case 2:
				 //printf("Hola\n");
                                update_angulations_conformations_cpp_environment (nconformations,metaheuristic,param,flexibility_conformations,vectors_mejora.energy.energy,energy);
				//printf("Hola 1\n");
                                flexibility_cpp (vectors_mejora.nconformations,metaheuristic,param,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,flexibility_conformations,ligando.nlig,flexibility_params->n_links,flexibility_params->n_fragments,flexibility_params->links,flexibility_params->links_fragments,flexibility_params->fragments,flexibility_params->fragments_tam,param.flex_angle);
				//incluir_mejorar_inicializar_environment_flex(ligando.nlig,vectors_mejora,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,nconformations,metaheuristic,param);
                                break;
                        }
                }
		incluir_mejorar_inicializar_environment_flex(ligando.nlig,vectors_mejora,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,nconformations,metaheuristic,param);
		//fill_conformations_environment_flex (ligando.nlig,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w, vectors_mejora.conformations_x, vectors_mejora.conformations_y, vectors_mejora.conformations_z, vectors_mejora.move_x, vectors_mejora.move_y, vectors_mejora.move_z, vectors_mejora.quat_x, vectors_mejora.quat_y, vectors_mejora.quat_z, vectors_mejora.quat_w, nconformations, param, metaheuristic);
		//for (int l=0;l<metaheuristic.NEEImp;l++) printf("FINAL VUELTA conf %d, mx %f my %f mz %f qx %f qy %f qz %f qw %f energy %f\n",l,vectors_mejora.move_x[l],vectors_mejora.move_y[l],vectors_mejora.move_z[l],vectors_mejora.quat_x[l],vectors_mejora.quat_y[l],vectors_mejora.quat_z[l],vectors_mejora.quat_w[l],vectors_mejora.energy.energy[l]);
		//printf("DEF mx %f my %f mz %f qx %f qy %f qz %f qw %f energy %f\n",move_x[0],move_y[0],move_z[0],quat_x[0],quat_y[0],quat_z[0],quat_w[0],energy[0]);
                //exit(0);
                steps++;
        }
	free(internal_energy);
        free(flexibility_conformations);
        free(vectors_mejora.conformations_x);
        free(vectors_mejora.conformations_y);
        free(vectors_mejora.conformations_z);
        free(vectors_mejora.move_x);
        free(vectors_mejora.move_y);
        free(vectors_mejora.move_z);
        free(vectors_mejora.quat_x);
        free(vectors_mejora.quat_y);
        free(vectors_mejora.quat_z);
        free(vectors_mejora.quat_w);
        free(vectors_mejora.energy.energy);
        free(vectors_mejora.energy.n_conformation);
        free(vectors_mejora.weights);

} 
