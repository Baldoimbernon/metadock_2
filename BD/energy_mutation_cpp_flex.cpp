#include <omp.h>
#include "wtime.h"
#include "type.h"
#include "definitions.h"
#include "flexibility.h"
#include "energy_struct.h"
#include "energy_CpuSolver_common.h"
#include "energy_CpuSolver_flex.h"
#include "montecarlo.h"
#include "rotation.h"
#include "metaheuristic_ini_flex.h"
#include "energy_mutation_flex.h"
#include "energy_mutation_cpp_flex.h"

extern void mutation_cpp_flex (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, struct metaheuristic_t metaheuristic,  struct flexibility_params_t *flexibility_params, type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int num_surface, unsigned int nconformations)
{
        unsigned int *n_update;
	type_data *internal_energy;
	struct flexibility_data_t *flexibility_conformations;

        n_update = (unsigned int *)malloc(sizeof(unsigned int)*nconformations);
	internal_energy = (type_data *)malloc(sizeof(type_data)*nconformations);

        mutation_random_cpp_flex (metaheuristic,nconformations,param,n_update);
        mutation_calculation_cpp_flex (param,metaheuristic,n_update,param.seed,param.automatic_seed,param.max_desp,param.rotation,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,ligando.nlig,flexibility_params->n_links,flexibility_params->n_fragments,flexibility_params->links,flexibility_params->links_fragments,flexibility_params->fragments,flexibility_params->fragments_tam,param.flex_angle,nconformations);

	forces_CPU_flex(param,proteina.atoms,ligando.atoms,ligando.nlig,proteina.rec_x,proteina.rec_y,proteina.rec_z,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,proteina.rectype,ligando.ligtype,ligando.ql,proteina.qr,proteina.bonds,ligando.bonds,energy,nconfs,weights,f_params,nconformations,metaheuristic);
	//ForcesCpuSolver_flex(param,ligando,proteina,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,weights,f_params,nconformations,metaheuristic,flexibility_params->n_links);
	energy_internal_conformations_cpu (nconformations,param,metaheuristic,conformations_x,conformations_y,conformations_z,&ligando,weights,f_params,internal_energy,flexibility_params->individual_bonds_VDW,flexibility_params->individual_bonds_ES);
        energy_total (nconformations,param,metaheuristic,energy,internal_energy);

        if (metaheuristic.IMEMUCom > 0)
        {
                if (param.montecarlo)
			montecarlo_cpp_flex(proteina,ligando,param,num_surface,weights,f_params,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,nconformations,metaheuristic,flexibility_params,flexibility_conformations,2);
                else
			mejorar_flex(proteina,ligando,param,weights,f_params,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,num_surface,nconformations,metaheuristic,flexibility_params,flexibility_conformations,2);
        }

        free(n_update);
	free(internal_energy);
}


extern void mutation_calculation_cpp_flex (struct param_t param, struct metaheuristic_t metaheuristic, unsigned int *n_update, int seed, int automatic_seed, type_data max_desp, type_data rotation, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, unsigned int nlig, unsigned int n_links, unsigned int n_fragments, int *links, int *links_fragments, int *fragments, int *fragments_tam, unsigned int max_angle_flex, unsigned int nconformations)
{
	unsigned int th1;
	type_data a[3],b[3],eje[3],p1[3];
        type_data ang;
        unsigned int num_point, frag_act, position_link;
        unsigned int rot_point, link;
        unsigned int shift_fragment, position_conformation;
        type_data qeje_x, qeje_y, qeje_z, qeje_w;
        type_data angle,t_p;
        type_data local_x, quat_tmp_x, local_y, quat_tmp_y, local_z, quat_tmp_z, local_w, quat_tmp_w ;

        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 0:
                case 3:
                case 2:
                        th1 = metaheuristic.Threads1Ini;
                        break;
        }
        omp_set_num_threads (th1);
	#pragma omp parallel for private (position_conformation,position_link,ang,link,rot_point,a,b,p1,num_point,shift_fragment,frag_act,qeje_x,qeje_y,qeje_z,qeje_w,quat_tmp_x,quat_tmp_y,quat_tmp_z,quat_tmp_w,eje,angle,local_x,local_y,local_z,local_w)
        for (unsigned int id_conformation=0; id_conformation < nconformations; id_conformation++)
        {
                switch (n_update[id_conformation])
                {
                        case 0:
                                //MOVE X
                                move_x[id_conformation] += getRealRandomNumber_r(max_desp,automatic_seed,seed,id_conformation+seed); //Desplazamiento entre -max_desp y max_desp
                                break;
                        case 1:
                                //MOVE Y
                                move_y[id_conformation] += getRealRandomNumber_r(max_desp,automatic_seed,seed,id_conformation+seed); //Desplazamiento entre -max_desp y max_desp
                                break;
                        case 2:
                                //MOVE Z
                                move_z[id_conformation] += getRealRandomNumber_r(max_desp,automatic_seed,seed,id_conformation+seed); //Desplazamiento entre -max_desp y max_desp
                                break;
			 case 3:
                                //ROTATION
                                quat_tmp_x = quat_x[id_conformation];
                                quat_tmp_y = quat_y[id_conformation];
                                quat_tmp_z = quat_z[id_conformation];
                                quat_tmp_w = quat_w[id_conformation];
                                angle = getRealRandomNumber(param.rotation);
                                eje[0] = getRealRandomNumber (1);
                                eje[1] = getRealRandomNumber (1);
                                eje[2] = getRealRandomNumber (1);
                                setRotation_cpp (&local_x, &local_y, &local_z, &local_w, angle, eje);
                                composeRotation_cpp (&local_x, &local_y, &local_z, &local_w, &quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w, &quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w);
                                normalize_cpp(&quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w );
                                quat_x[id_conformation] = quat_tmp_x;
                                quat_y[id_conformation] = quat_tmp_y;
                                quat_z[id_conformation] = quat_tmp_z;
                                quat_w[id_conformation] = quat_tmp_w;
                                break;
			case 4:
				//FLEXIBILIDAD
				 //OBTENEMOS EL ANGULO EN RADIANES ALEATORIO ENTRE UN MAXIMO.
                		t_p =  (getRealRandomNumber(max_angle_flex) * PI) / 180 ;
		                //ENLACE Y FRAGMENTO A GIRAR ALEATORIO
		                link =  getIntNumber(10000) % n_links;
		                //OBTENEMOS EL PUNTO DEL ENLACE DESDE DONDE ROTAR
		                rot_point = getIntNumber(10000) % 2;

		                //NOS SITUAMOS EN EL VECTOR DE CONFORMACIONES Y DE ENLACES
		                position_conformation = id_conformation * nlig;

                		position_link = link;
		                //printf("position_conformation %d, position_link %d\n",position_conformation,position_link);
		                //EJE DE GIRO
                		a[0] = conformations_x[position_conformation + (links[2 * position_link] - 1)];
		                a[1] = conformations_y[position_conformation + (links[2 * position_link] - 1)];
		                a[2] = conformations_z[position_conformation + (links[2 * position_link] - 1)];
		                b[0] = conformations_x[position_conformation + (links[(2 * position_link)+1] - 1)];
		                b[1] = conformations_y[position_conformation + (links[(2 * position_link)+1] - 1)];
		                b[2] = conformations_z[position_conformation + (links[(2 * position_link)+1] - 1)];
		
		                if (rot_point == 0){
		                        eje[0] = b[0] - a[0];
		                        eje[1] = b[1] - a[1];
		                        eje[2] = b[2] - a[2];
		                        p1[0] = conformations_x[position_conformation + (links[2 * position_link] - 1)];
		                        p1[1] = conformations_y[position_conformation + (links[2 * position_link] - 1)];
		                        p1[2] = conformations_z[position_conformation + (links[2 * position_link] - 1)];
		                        num_point = (2 * position_link) + 1;
		                }else {
                		        eje[0] = a[0] - b[0];
		                        eje[1] = a[1] - b[1];
		                        eje[2] = a[2] - b[2];
                		        p1[0] = conformations_x[position_conformation + (links[(2 * position_link)+1] - 1)];
		                        p1[1] = conformations_y[position_conformation + (links[(2 * position_link)+1] - 1)];
		                        p1[2] = conformations_z[position_conformation + (links[(2 * position_link)+1] - 1)];
                		        num_point = 2 * position_link;
		                }

                		//CONFIGURACION EJE DE ROTACION COMO QUATERNION
		                setRotation_flexibility_cpp (&qeje_x,&qeje_y,&qeje_z,&qeje_w,ang,eje);
		
		                //int j2 = 0;
		                shift_fragment = 0;
		                //ROTAMOS TODOS LOS FRAGMENTOS QUE TENGAN COMO ORIGEN EL PUNTO DE ROTACION
		                //while ((frag_act = links_fragments_d[(num_point * n_fragments) + j2]) != 0)
		                frag_act = links_fragments[num_point];	
				break;
                }
        }

}

extern void mutation_random_cpp_flex (struct metaheuristic_t metaheuristic, unsigned int nconformations, struct param_t param, unsigned int *n_update)
{
        unsigned int th1;

        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 0:
                case 3:
                case 2:
                        th1 = metaheuristic.Threads1Ini;
                        break;
        }
        omp_set_num_threads (th1);
        #pragma omp parallel
        for (unsigned int id_conformation=0; id_conformation < nconformations; id_conformation++)
        {
                n_update[id_conformation] =  getIntNumber(10000) % 5;
        }
}
extern void mutation_multigpu_flex (struct ligand_t ligando, struct receptor_t proteina, unsigned int surface, struct flexibility_params_t *flexibility_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int fase)
{

	unsigned int  pc_temp, stride_temp,ttotal = 0, dev;
      
        //pc_temp = nconformations / surface;
        devices->stride[0] = 0;
        //stride_temp = (int)((devices->trabajo[0] * 0.01)*surface);
        //devices->conformaciones[0] = stride_temp * pc_temp;
        //ttotal = stride_temp;

        for (unsigned int i=0;i < devices->n_devices;i++)
        {
                stride_temp = floor((devices->trabajo[i] * 0.01)*nconformations);
                devices->conformaciones[i] = stride_temp;
                ttotal += stride_temp;
        }
        unsigned int diferencia = nconformations - ttotal;
        for (unsigned int i=0;i<diferencia;i++)
        {
                dev = i % devices->n_devices;
                devices->conformaciones[dev] ++;
        }
        for (unsigned int i=(devices->n_devices-1);i > 0;i--)
        {
                devices->stride[i] = nconformations - devices->conformaciones[i];
                nconformations = nconformations - devices->conformaciones[i];
        }

	
        omp_set_num_threads(devices->n_devices);

        #pragma omp parallel for
        for (unsigned int j=0;j<devices->n_devices;j++)
        {
                unsigned int th = omp_get_thread_num();
                unsigned int stride_d = devices->stride[j];						
				mutation_gpu_flex (proteina,ligando,param,weights,f_params,flexibility_params,conformations_x+(stride_d*ligando.nlig),conformations_y+(stride_d*ligando.nlig),conformations_z+(stride_d*ligando.nlig),move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,nconfs+stride_d,devices->conformaciones[j],metaheuristic,devices,j,stride_d); 
	}

}
