#ifndef __ENERGY_MUTATION_H
#define __ENERGY_MUTATION_H

__global__ void mutation_random (curandState_t * states, unsigned int *n_update, unsigned long int max);
__global__ void mutation_kernel (curandState_t * states, unsigned int *n_update, type_data max_desp, type_data rotation, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, unsigned long int max);
extern void mutation_gpu_calculation (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, type_data *weights, struct force_field_param_t *f_params, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconf, unsigned int nconformations,unsigned int orden_device,unsigned int stride);
extern void mutation_gpu (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, type_data *weights, struct force_field_param_t *f_params, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconf, unsigned int nconformations, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int orden_device, unsigned int stride);
extern void mutation_multigpu (struct ligand_t ligando, struct receptor_t proteina, unsigned int surface, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int fase);



#endif
