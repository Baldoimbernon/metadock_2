//#include <thrust/device_ptr.h>
//#include <thrust/sequence.h>
#include "energy_common.h"
#include "energy_cuda.h"
#include "energy_common-gpu.h"
#include <thrust/device_ptr.h>
#include <thrust/sequence.h>

extern void dataToGPUConformations(unsigned int N, unsigned int nlig, type_data *conformations_x, type_data *&conformations_x_d, type_data *conformations_y, type_data *&conformations_y_d, type_data *conformations_z, type_data *&conformations_z_d) 
{
	cudaError_t cudaStatus;
	unsigned long int confSize = N * nlig * sizeof(type_data);
	
	cudaStatus = cudaMalloc((void**) &conformations_x_d, confSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc conformations_x_d values failed!");
	cudaStatus = cudaMalloc((void**) &conformations_y_d, confSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc conformations_y_d values failed!");
	cudaStatus = cudaMalloc((void**) &conformations_z_d, confSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc conformations_z_d values failed!");
	//cudaDeviceSynchronize();
	//cudaStatus = cudaGetLastError();
	//if (cudaStatus != cudaSuccess) printf("Error Creacion memoria conformaciones en GPU %d %d %d\n",confSize,nlig,cudaStatus);
	
	cudaStatus = cudaMemcpy(conformations_x_d, conformations_x, confSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy conformations_x_d values failed!");
	cudaStatus = cudaMemcpy(conformations_y_d, conformations_y, confSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy conformations_y_d values failed!");
	cudaStatus = cudaMemcpy(conformations_z_d, conformations_z, confSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy conformations_z_d values failed!");
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) printf("Error copia de conformaciones a GPU\n");
		
}

extern void dataToGPUConformations_environment(unsigned int N, int NEEImp, unsigned int nlig, type_data *conformations_x, type_data *&conformations_x_d, type_data *conformations_y, type_data *&conformations_y_d, type_data *conformations_z, type_data *&conformations_z_d, type_data *conformations_x_mejora, type_data *&conformations_x_d_mejora, type_data *conformations_y_mejora, type_data *&conformations_y_d_mejora, type_data *conformations_z_mejora, type_data *&conformations_z_d_mejora)
{
        cudaError_t cudaStatus;
        unsigned long int confSize = N * nlig * sizeof(type_data);
	unsigned long int confSize_environment = N * NEEImp * nlig * sizeof(type_data);

        cudaStatus = cudaMalloc((void**) &conformations_x_d, confSize);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc conformations_x_d values failed!");
        cudaStatus = cudaMalloc((void**) &conformations_y_d, confSize);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc conformations_y_d values failed!");
        cudaStatus = cudaMalloc((void**) &conformations_z_d, confSize);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc conformations_z_d values failed!");

	cudaStatus = cudaMalloc((void**) &conformations_x_d_mejora, confSize_environment);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc conformations_x_d values failed!");
        cudaStatus = cudaMalloc((void**) &conformations_y_d_mejora, confSize_environment);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc conformations_y_d values failed!");
        cudaStatus = cudaMalloc((void**) &conformations_z_d_mejora, confSize_environment);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc conformations_z_d values failed!");
        //cudaDeviceSynchronize();
        //cudaStatus = cudaGetLastError();
        //if (cudaStatus != cudaSuccess) printf("Error Creacion memoria conformaciones en GPU %d %d %d\n",confSize,nlig,cudaStatus);

        cudaStatus = cudaMemcpy(conformations_x_d, conformations_x, confSize, cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy conformations_x_d values failed!");
        cudaStatus = cudaMemcpy(conformations_y_d, conformations_y, confSize, cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy conformations_y_d values failed!");
        cudaStatus = cudaMemcpy(conformations_z_d, conformations_z, confSize, cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy conformations_z_d values failed!");
	cudaStatus = cudaMemcpy(conformations_x_d_mejora, conformations_x_mejora, confSize_environment, cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy conformations_x_d values failed!");
        cudaStatus = cudaMemcpy(conformations_y_d_mejora, conformations_y_mejora, confSize_environment, cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy conformations_y_d values failed!");
        cudaStatus = cudaMemcpy(conformations_z_d_mejora, conformations_z_mejora, confSize_environment, cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy conformations_z_d values failed!");
        cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) printf("Error copia de conformaciones a GPU\n");

}

extern void dataToGPUFlexibilityParameters(unsigned int N, unsigned int atoms, int* &links_d, int *links, int* &links_fragments_d, int *links_fragments, int* &fragments_d, int *fragments, int* &fragments_tam_d, int *fragments_tam, struct flexibility_data_t* &flexibility_conformations_d, unsigned int n_links, unsigned int n_fragments)
{
	cudaError_t cudaStatus;
	unsigned long int linksSize = n_links * 2 * sizeof(int);
	unsigned long int linksfragmentsSize = n_links * 2 * sizeof(int);
	unsigned long int fragmentstamSize = n_fragments * sizeof(int);
	unsigned long int fragmentsSize = n_links * (atoms - 2) * sizeof(int);
	unsigned long int flexibilitydadaSize = N * sizeof(struct flexibility_data_t);
	
	//ENLACES
	cudaStatus = cudaMalloc((void**) &links_d,linksSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc links failed!");

	//CADA PUNTO DE LOS ENLACES CON SUS FRAGMENTOS ASOCIADOS
	cudaStatus = cudaMalloc((void**) &links_fragments_d, linksfragmentsSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc links_fragments failed!");

	//FRAGMENTOS
	cudaStatus = cudaMalloc((void**) &fragments_d, fragmentsSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc fragments_tam failed!");


	//TAMAÑO FRAGMENTOS
	cudaStatus = cudaMalloc((void**) &fragments_tam_d, fragmentstamSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc fragments failed!");

	//PARAMETROS DE FLEXIBILIDAD
	cudaStatus = cudaMalloc((void**) &flexibility_conformations_d, flexibilitydadaSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc flexibility_conformations failed!");

	//COPIA DE DATOS INICIALES
	cudaStatus = cudaMemcpy(links_d,links, linksSize,cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy conformations failed!");

	cudaStatus = cudaMemcpy(links_fragments_d, links_fragments, linksfragmentsSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy links_fragments failed!");

	cudaStatus = cudaMemcpy(fragments_tam_d, fragments_tam, fragmentstamSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy lfragments_tam failed!");

	cudaStatus = cudaMemcpy(fragments_d, fragments, fragmentsSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy fragments failed!");	
}

extern void dataToGPUFlexibilityParameters_mutation(unsigned int N, unsigned int atoms, int* &links_d, int *links, int* &fragments_d, int *fragments, int* &links_fragments_d,int *links_fragments, int *fragments_tam_d, int *fragments_tam, unsigned int n_links, unsigned int n_fragments)
{
	 cudaError_t cudaStatus;
        unsigned long int linksSize = n_links * 2 * sizeof(int);
        unsigned long int linksfragmentsSize = n_links * 2 * sizeof(int);
        unsigned long int fragmentstamSize = n_fragments * sizeof(int);
        unsigned long int fragmentsSize = n_links * (atoms - 2) * sizeof(int);
        unsigned long int flexibilitydadaSize = N * sizeof(struct flexibility_data_t);

        //ENLACES
        cudaStatus = cudaMalloc((void**) &links_d,linksSize);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc links failed!");

        //CADA PUNTO DE LOS ENLACES CON SUS FRAGMENTOS ASOCIADOS
        cudaStatus = cudaMalloc((void**) &links_fragments_d, linksfragmentsSize);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc links_fragments failed!");

        //FRAGMENTOS
        cudaStatus = cudaMalloc((void**) &fragments_d, fragmentsSize);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc fragments_tam failed!");

        //TAMAÑO FRAGMENTOS
        cudaStatus = cudaMalloc((void**) &fragments_tam_d, fragmentstamSize);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc fragments failed!");

	//COPIA DE DATOS INICIALES
        cudaStatus = cudaMemcpy(links_d,links, linksSize,cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy conformations failed!");

        cudaStatus = cudaMemcpy(links_fragments_d, links_fragments, linksfragmentsSize, cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy links_fragments failed!");

        cudaStatus = cudaMemcpy(fragments_tam_d, fragments_tam, fragmentstamSize, cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy lfragments_tam failed!");

        cudaStatus = cudaMemcpy(fragments_d, fragments, fragmentsSize, cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy fragments failed!");
}

extern void dataToGPUInternalEnergy(unsigned int N, unsigned int nlig, unsigned int atoms, type_data *conformations_x,type_data *conformations_y,type_data *conformations_z,type_data *&conformations_x_d,type_data *&conformations_y_d,type_data *&conformations_z_d,bool *individual_bonds_VDW,bool *individual_bonds_ES,bool *&individual_bonds_VDW_d,bool *&individual_bonds_ES_d,type_data *&internal_energy_d) 
{
	cudaError_t cudaStatus;
	unsigned long int confSize = N * nlig * sizeof(type_data);
	unsigned long int eSize = N * sizeof(type_data);
	unsigned long int boolSize = atoms * atoms * sizeof(bool);
	
	cudaStatus = cudaMalloc((void**) &conformations_x_d, confSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc conformations_x_d values failed!");
	cudaStatus = cudaMalloc((void**) &conformations_y_d, confSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc conformations_y_d values failed!");
	cudaStatus = cudaMalloc((void**) &conformations_z_d, confSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc conformations_z_d values failed!");
	
	cudaStatus = cudaMalloc((void**) &internal_energy_d, eSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc internal_energy_d values failed!");
	cudaStatus = cudaMalloc((void**) &individual_bonds_VDW_d, boolSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc individual_bonds_VDW_d values failed!");	
	cudaStatus = cudaMalloc((void**) &individual_bonds_ES_d, boolSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc individual_bonds_ES_d values failed!");
	
	cudaStatus = cudaMemcpy(conformations_x_d, conformations_x, confSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy conformations_x_d values failed!");
	cudaStatus = cudaMemcpy(conformations_y_d, conformations_y, confSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy conformations_y_d values failed!");
	cudaStatus = cudaMemcpy(conformations_z_d, conformations_z, confSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy conformations_z_d values failed!");
	
	cudaStatus = cudaMemcpy(individual_bonds_VDW_d, individual_bonds_VDW, boolSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy individual_bonds_VDW_d values failed!");
	cudaStatus = cudaMemcpy(individual_bonds_ES_d, individual_bonds_ES, boolSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy individual_bonds_ES_d values failed!");	
		
}

extern void dataToGPUInternalEnergy_environment(unsigned int N, unsigned int N_mejora, unsigned int nlig, unsigned int atoms, struct flexibility_data_t* &flexibility_conformations_d_mejora, type_data *conformations_x,type_data *conformations_y,type_data *conformations_z,type_data *&conformations_x_d,type_data *&conformations_y_d,type_data *&conformations_z_d, type_data *conformations_x_mejora, type_data *conformations_y_mejora, type_data *conformations_z_mejora,type_data *&conformations_x_d_mejora,type_data *&conformations_y_d_mejora,type_data *&conformations_z_d_mejora, bool *individual_bonds_VDW,bool *individual_bonds_ES,bool *&individual_bonds_VDW_d,bool *&individual_bonds_ES_d,type_data *&internal_energy_d)
{
        cudaError_t cudaStatus;
        unsigned long int confSize = N * nlig * sizeof(type_data);
        unsigned long int eSize = N * sizeof(type_data);

	unsigned long int flexibilitydadaSize_environment = N_mejora * sizeof(struct flexibility_data_t);

	unsigned long int confSize_environment = N_mejora * nlig * sizeof(type_data);
        unsigned long int eSize_environment = N_mejora * sizeof(type_data);
        unsigned long int boolSize = atoms * atoms * sizeof(bool);

	cudaStatus = cudaMalloc((void**) &flexibility_conformations_d_mejora, flexibilitydadaSize_environment);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc flexibility_conformations failed!");

        cudaStatus = cudaMalloc((void**) &conformations_x_d, confSize);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc conformations_x_d values failed!");
        cudaStatus = cudaMalloc((void**) &conformations_y_d, confSize);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc conformations_y_d values failed!");
        cudaStatus = cudaMalloc((void**) &conformations_z_d, confSize);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc conformations_z_d values failed!");

	cudaStatus = cudaMalloc((void**) &conformations_x_d_mejora, confSize_environment);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc conformations_x_d values failed!");
        cudaStatus = cudaMalloc((void**) &conformations_y_d_mejora, confSize_environment);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc conformations_y_d values failed!");
        cudaStatus = cudaMalloc((void**) &conformations_z_d_mejora, confSize_environment);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc conformations_z_d values failed!");

        cudaStatus = cudaMalloc((void**) &internal_energy_d, eSize_environment);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc internal_energy_d values failed!");
        cudaStatus = cudaMalloc((void**) &individual_bonds_VDW_d, boolSize);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc individual_bonds_VDW_d values failed!");
        cudaStatus = cudaMalloc((void**) &individual_bonds_ES_d, boolSize);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc individual_bonds_ES_d values failed!");

        cudaStatus = cudaMemcpy(conformations_x_d, conformations_x, confSize, cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy conformations_x_d values failed!");
        cudaStatus = cudaMemcpy(conformations_y_d, conformations_y, confSize, cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy conformations_y_d values failed!");
        cudaStatus = cudaMemcpy(conformations_z_d, conformations_z, confSize, cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy conformations_z_d values failed!");

	cudaStatus = cudaMemcpy(conformations_x_d_mejora, conformations_x_mejora, confSize_environment, cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy conformations_x_d values failed!");
        cudaStatus = cudaMemcpy(conformations_y_d_mejora, conformations_y_mejora, confSize_environment, cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy conformations_y_d values failed!");
        cudaStatus = cudaMemcpy(conformations_z_d_mejora, conformations_z_mejora, confSize_environment, cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy conformations_z_d values failed!");

        cudaStatus = cudaMemcpy(individual_bonds_VDW_d, individual_bonds_VDW, boolSize, cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy individual_bonds_VDW_d values failed!");
        cudaStatus = cudaMemcpy(individual_bonds_ES_d, individual_bonds_ES, boolSize, cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy individual_bonds_ES_d values failed!");

}

extern void HostToGPU_improve (unsigned int N, type_data* &move_x_m, type_data* &move_y_m, type_data* &move_z_m, type_data* &quat_x_m, type_data* &quat_y_m, type_data* &quat_z_m, type_data* &quat_w_m, type_data* move_x, type_data* move_y, type_data* move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w){

	cudaError_t cudaStatus;
	unsigned long int moveSize = N * sizeof(type_data);
	
	cudaStatus = cudaMalloc((void**) &move_x_m, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &move_y_m, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &move_z_m, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	
	cudaStatus = cudaMalloc((void**) &quat_x_m, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_y_m, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_z_m, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_w_m, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	
	cudaStatus = cudaMemcpy(move_x_m, move_x, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(move_y_m, move_y, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(move_z_m, move_z, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	cudaStatus = cudaMemcpy(quat_x_m, quat_x, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_y_m, quat_y, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_z_m, quat_z, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_w_m, quat_w, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");

}

extern void GPUtoHost (unsigned int N, struct vectors_t *vectors_selec, type_data* move_x, type_data* move_y, type_data* move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data* energy ){

	cudaError_t cudaStatus;
	unsigned long int moveSize = N * sizeof(type_data);
	unsigned long int quatSize = N * sizeof(type_data);
	
	cudaStatus = cudaMemcpy(vectors_selec->move_x, move_x, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values move_x GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_selec->move_y, move_y, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values move_y GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_selec->move_z, move_z, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values move_z GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_selec->energy.energy, energy, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values energy GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_selec->quat_x, quat_x, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_selec->quat_y, quat_y, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_selec->quat_z, quat_z, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_selec->quat_w, quat_w, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat GPUtoHost failed!");
}
extern void commonDataToGPU (struct receptor_t proteina, struct receptor_t *& proteina_d, struct ligand_t ligando, struct ligand_t *&ligando_d){

	struct receptor_t rec_temp;
	struct ligand_t lig_temp;
	cudaError_t cudaStatus;
		
	unsigned long int recSize = proteina.nrec * sizeof(type_data);
	unsigned long int ligSize = ligando.nlig * sizeof(type_data);
	unsigned long int typeSize = proteina.nrec * sizeof(char);
	unsigned long int ligtypeSize = ligando.nlig * sizeof(char);
	unsigned long int ligbondsSize = MAXBOND * ligando.nlig * sizeof(int);

	//guarda el ligando en device memory
	lig_temp = ligando;
	cudaStatus = cudaMalloc((void**) &ligando_d, sizeof(struct ligand_t));
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	cudaStatus = cudaMalloc((void**) &lig_temp.lig_x, ligSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	cudaStatus = cudaMalloc((void**) &lig_temp.lig_y, ligSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");	
	cudaStatus = cudaMalloc((void**) &lig_temp.lig_z, ligSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	cudaStatus = cudaMalloc((void**) &lig_temp.ligtype, ligtypeSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	cudaStatus = cudaMalloc((void**) &lig_temp.ql, ligSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	cudaStatus = cudaMalloc((void**) &lig_temp.bonds, ligbondsSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	
	//printf("Subir ligando.\n");
	//Copia el ligando a la GPU
	cudaStatus = cudaMemcpy(ligando_d, &lig_temp, sizeof(struct ligand_t), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
    	cudaStatus = cudaMemcpy(lig_temp.lig_x, ligando.lig_x, ligSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(lig_temp.lig_y, ligando.lig_y, ligSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(lig_temp.lig_z, ligando.lig_z, ligSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(lig_temp.ligtype, ligando.ligtype, ligtypeSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(lig_temp.ql, ligando.ql, ligSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(lig_temp.bonds, ligando.bonds, ligbondsSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	
	//printf("Crear memoria proteina\n");	
	//guarda la proteina y la energia en global memory
	rec_temp = proteina;
	cudaStatus = cudaMalloc((void**) &proteina_d, sizeof(struct receptor_t));
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	cudaStatus = cudaMalloc((void**) &rec_temp.rec_x, recSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	cudaStatus = cudaMalloc((void**) &rec_temp.rec_y, recSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc falled!");
	cudaStatus = cudaMalloc((void**) &rec_temp.rec_z, recSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	cudaStatus = cudaMalloc((void**) &rec_temp.rectype, typeSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	cudaStatus = cudaMalloc((void**) &rec_temp.qr, recSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");	
	cudaStatus = cudaMalloc((void**) &rec_temp.bonds, proteina.nrec * MAXBOND * sizeof(int));
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");

	//Copia la proteina en la GPU
	cudaStatus = cudaMemcpy(proteina_d, &rec_temp, sizeof(struct receptor_t), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(rec_temp.rec_x, proteina.rec_x, recSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(rec_temp.rec_y, proteina.rec_y, recSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(rec_temp.rec_z, proteina.rec_z, recSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(rec_temp.rectype, proteina.rectype, typeSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(rec_temp.qr, proteina.qr, recSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");	
	cudaStatus = cudaMemcpy(rec_temp.bonds, proteina.bonds, proteina.nrec * MAXBOND * sizeof(int), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	

}

extern void commonDataToGPU_WS_vs_clb (unsigned int nconformations_mejora, unsigned int nlig, char *ligtype_mejora, unsigned int *bonds_mejora, type_data *ql_mejora, char *&ligtype_d_mejora, unsigned int *&bonds_d_mejora, type_data *&ql_d_mejora)
{
	cudaError_t cudaStatus;

	unsigned long int ligtypeSize_environment =  nconformations_mejora * nlig * sizeof(char);
        unsigned long int ligbondsSize_environment = nconformations_mejora * nlig * MAXBOND * sizeof(int);
	unsigned long int ligSize_environment = nconformations_mejora * nlig * sizeof(type_data);

	cudaStatus = cudaMalloc((void**) &ligtype_d_mejora, ligtypeSize_environment);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
        cudaStatus = cudaMalloc((void**) &ql_d_mejora, ligSize_environment);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
        cudaStatus = cudaMalloc((void**) &bonds_d_mejora, ligbondsSize_environment);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");

	cudaStatus = cudaMemcpy(ligtype_d_mejora, ligtype_mejora, ligtypeSize_environment, cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
        cudaStatus = cudaMemcpy(ql_d_mejora, ql_mejora, ligSize_environment, cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
        cudaStatus = cudaMemcpy(bonds_d_mejora, bonds_mejora, ligbondsSize_environment, cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");

	
}
extern void commonDataToGPU_WS (struct receptor_t proteina, type_data *&rec_x_d, type_data *&rec_y_d, type_data *&rec_z_d, type_data *&qr_d, char *&rectype_d, unsigned int *&bondsr_d, struct ligand_t ligando, type_data *&lig_x_d, type_data *&lig_y_d, type_data *&lig_z_d, type_data *&ql_d, char *&ligtype_d, unsigned int *&bonds_d){

	cudaError_t cudaStatus;
		
	unsigned long int recSize = proteina.nrec * sizeof(type_data);
	unsigned long int ligSize = ligando.nlig * sizeof(type_data);
	unsigned long int typeSize = proteina.nrec * sizeof(char);
	unsigned long int ligtypeSize = ligando.nlig * sizeof(char);
	unsigned long int ligbondsSize = MAXBOND * ligando.nlig * sizeof(int);
	unsigned long int recbondsSize = proteina.nrec * MAXBOND * sizeof(int);

	//guarda el ligando en device memory
	cudaStatus = cudaMalloc((void**) &lig_x_d, ligSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	cudaStatus = cudaMalloc((void**) &lig_y_d, ligSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");	
	cudaStatus = cudaMalloc((void**) &lig_z_d, ligSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	cudaStatus = cudaMalloc((void**) &ligtype_d, ligtypeSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	cudaStatus = cudaMalloc((void**) &ql_d, ligSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	cudaStatus = cudaMalloc((void**) &bonds_d, ligbondsSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	
	//printf("Subir ligando.\n");
	//Copia el ligando a la GPU
	cudaStatus = cudaMemcpy(lig_x_d, ligando.lig_x, ligSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(lig_y_d, ligando.lig_y, ligSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(lig_z_d, ligando.lig_z, ligSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(ligtype_d, ligando.ligtype, ligtypeSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(ql_d, ligando.ql, ligSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(bonds_d, ligando.bonds, ligbondsSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	
	
	//printf("Crear memoria proteina\n");	
	//guarda la proteina y la energia en global memory
	cudaStatus = cudaMalloc((void**) &rec_x_d, recSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	cudaStatus = cudaMalloc((void**) &rec_y_d, recSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc falled!");
	cudaStatus = cudaMalloc((void**) &rec_z_d, recSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	cudaStatus = cudaMalloc((void**) &rectype_d, typeSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	cudaStatus = cudaMalloc((void**) &qr_d, recSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	cudaStatus = cudaMalloc((void**) &bondsr_d, recbondsSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");	

	//Copia la proteina en la GPU
	cudaStatus = cudaMemcpy(rec_x_d, proteina.rec_x, recSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(rec_y_d, proteina.rec_y, recSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(rec_z_d, proteina.rec_z, recSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");

	cudaStatus = cudaMemcpy(rectype_d, proteina.rectype, typeSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(qr_d, proteina.qr, recSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(bondsr_d, proteina.bonds, recbondsSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");	
	
}

extern void dataToGPU_mejorar_inicializar(unsigned int N, unsigned int num_surface, type_data * &energy_d, unsigned int * &energy_nconformation_d, type_data * &energy_d_mejora, unsigned int * &energy_nconformation_d_mejora, type_data * &moves_x_d, type_data * &moves_y_d, type_data * &moves_z_d, type_data * &quat_d_x, type_data * &quat_d_y, type_data * &quat_d_z, type_data * &quat_d_w, type_data * &moves_x_d_mejora, type_data * &moves_y_d_mejora, type_data * &moves_z_d_mejora, type_data * &quat_d_mejora_x, type_data * &quat_d_mejora_y, type_data * &quat_d_mejora_z, type_data * &quat_d_mejora_w, curandState_t * &states_d, type_data* move_x, type_data* move_y, type_data* move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data* energy, unsigned int* energy_nconformation, struct metaheuristic_t metaheuristic)
{	
	cudaError_t cudaStatus;

	unsigned long int randSize = N * sizeof(curandState_t);	
	unsigned long int moveSize = N * sizeof(type_data);
	
		
	unsigned long int nconformationsize = N * sizeof(int);
 	unsigned long int energySize = N * sizeof(type_data);
	unsigned long int quatSize = N * sizeof(type_data);		
	//reserva memoria para los movimientos
	//for (int i=0;i<25;i++)printf("energy[%d] = %f\n",i,energy[i]);

	cudaStatus = cudaMalloc((void**) &moves_x_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_y_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_z_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &energy_d, energySize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &energy_nconformation_d, nconformationsize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
		
	cudaStatus = cudaMalloc((void**) &moves_x_d_mejora, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_y_d_mejora, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_z_d_mejora, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &energy_d_mejora, energySize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &energy_nconformation_d_mejora, nconformationsize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
		
	cudaStatus = cudaMalloc((void**) &quat_d_x, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_y, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_z, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_w, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");

	cudaStatus = cudaMalloc((void**) &quat_d_mejora_x, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_mejora_y, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_mejora_z, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_mejora_w, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
		
	cudaStatus = cudaMalloc((void**) &states_d, randSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc states_d values failed!");
	
	
	cudaStatus = cudaMemcpy(moves_x_d, move_x, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_y_d, move_y, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_z_d, move_z, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(energy_d, energy, energySize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(energy_nconformation_d, energy_nconformation, nconformationsize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");


	cudaStatus = cudaMemcpy(moves_x_d_mejora, move_x, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_y_d_mejora, move_y, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_z_d_mejora, move_z, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(energy_nconformation_d_mejora, energy_nconformation, nconformationsize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	cudaStatus = cudaMemcpy(quat_d_x, quat_x, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_y, quat_y, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_z, quat_z, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_w, quat_w, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	
	cudaStatus = cudaMemcpy(quat_d_mejora_x, quat_x, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_mejora_y, quat_y, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_mejora_z, quat_z, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_mejora_w, quat_w, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	//cudaStatus = cudaMemset(energy_d_mejora, 10000.0, energySize);
	
}

extern void dataToGPU_mejorar (unsigned int N, unsigned int num_surface, type_data * &energy_d, int * &energy_nconformation_d, type_data * &energy_d_mejora, unsigned int * &energy_nconformation_d_mejora, type_data * &moves_x_d, type_data * &moves_y_d, type_data * &moves_z_d, type_data * &quat_d_x, type_data * &quat_d_y, type_data * &quat_d_z, type_data * &quat_d_w, type_data * &moves_x_d_mejora, type_data * &moves_y_d_mejora, type_data * &moves_z_d_mejora, type_data * &quat_d_mejora_x, type_data * &quat_d_mejora_y, type_data * &quat_d_mejora_z, type_data * &quat_d_mejora_w, curandState_t * &states_d, type_data* move_x, type_data* move_y, type_data* move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data* energy, unsigned int* energy_nconformation, struct metaheuristic_t metaheuristic)
{	
	cudaError_t cudaStatus;

	unsigned long int randSize = N * sizeof(curandState_t);	
	unsigned long int moveSize = N * sizeof(type_data);
	unsigned long int quatSize = N * sizeof(type_data);
	unsigned long int conf = N / num_surface;
	unsigned long int nconformationsize = N * sizeof(int);
 	unsigned long int energySize = num_surface * conf * sizeof(type_data);		
	//reserva memoria para los movimientos
	//for (int i=0;i<25;i++)printf("energy[%d] = %f\n",i,energy[i]);

	cudaStatus = cudaMalloc((void**) &moves_x_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_y_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_z_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &energy_d, energySize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &energy_nconformation_d, nconformationsize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
		
	cudaStatus = cudaMalloc((void**) &moves_x_d_mejora, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_y_d_mejora, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_z_d_mejora, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &energy_d_mejora, energySize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &energy_nconformation_d_mejora, nconformationsize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	
	cudaStatus = cudaMalloc((void**) &quat_d_x, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_y, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_z, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_w, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	
	
	cudaStatus = cudaMalloc((void**) &quat_d_mejora_x, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_mejora_y, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_mejora_z, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_mejora_w, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
		
	cudaStatus = cudaMalloc((void**) &states_d, randSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc states_d values failed!");
	
	
	cudaStatus = cudaMemcpy(moves_x_d, move_x, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_y_d, move_y, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_z_d, move_z, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(energy_d, energy, energySize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(energy_nconformation_d, energy_nconformation, nconformationsize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");


	cudaStatus = cudaMemcpy(moves_x_d_mejora, move_x, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_y_d_mejora, move_y, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_z_d_mejora, move_z, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(energy_nconformation_d_mejora, energy_nconformation, nconformationsize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	cudaStatus = cudaMemcpy(quat_d_x, quat_x, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_y, quat_y, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_z, quat_z, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_w, quat_w, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	
	cudaStatus = cudaMemcpy(quat_d_mejora_x, quat_x, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_mejora_y, quat_y, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_mejora_z, quat_z, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_mejora_w, quat_w, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	cudaStatus = cudaMemset(energy_d_mejora, 10000.0, energySize);
	
}

extern void dataToGPU_multigpu_mejorar_inicializar(unsigned int N, type_data * &energy_d, unsigned int * &energy_nconformation_d, type_data * &energy_d_mejora, unsigned int * &energy_nconformation_d_mejora, type_data * &moves_x_d, type_data * &moves_y_d, type_data * &moves_z_d, type_data * &quat_d_x, type_data * &quat_d_y, type_data * &quat_d_z, type_data * &quat_d_w, type_data * &moves_x_d_mejora, type_data * &moves_y_d_mejora, type_data * &moves_z_d_mejora, type_data * &quat_d_mejora_x, type_data * &quat_d_mejora_y, type_data * &quat_d_mejora_z, type_data * &quat_d_mejora_w, curandState_t * &states_d, type_data* move_x, type_data* move_y, type_data* move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data* energy, unsigned int* energy_nconformation, struct metaheuristic_t metaheuristic)
{	
	cudaError_t cudaStatus;

	unsigned long int randSize = N * sizeof(curandState_t);	
	unsigned long int moveSize = N * sizeof(type_data);
	unsigned long int quatSize = N * sizeof(type_data);	
	unsigned long int nconformationsize = N * sizeof(int);
 	unsigned long int energySize = N * sizeof(type_data);		
	
	//reserva memoria para los movimientos
	cudaStatus = cudaMalloc((void**) &moves_x_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");	
	cudaStatus = cudaMalloc((void**) &moves_y_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_z_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &energy_d, energySize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &energy_nconformation_d, nconformationsize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");		
	cudaStatus = cudaMalloc((void**) &moves_x_d_mejora, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_y_d_mejora, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_z_d_mejora, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &energy_d_mejora, energySize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &energy_nconformation_d_mejora, nconformationsize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
 		
	cudaStatus = cudaMalloc((void**) &quat_d_x, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_y, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_z, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_w, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	
	cudaStatus = cudaMalloc((void**) &quat_d_mejora_x, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_mejora_y, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_mejora_z, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_mejora_w, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
		
	cudaStatus = cudaMalloc((void**) &states_d, randSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc states_d values failed!");
	
	
	cudaStatus = cudaMemcpy(moves_x_d, move_x, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_y_d, move_y, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_z_d, move_z, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(energy_d, energy, energySize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(energy_nconformation_d, energy_nconformation, nconformationsize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	cudaStatus = cudaMemcpy(moves_x_d_mejora, move_x, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_y_d_mejora, move_y, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_z_d_mejora, move_z, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(energy_nconformation_d_mejora, energy_nconformation, nconformationsize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	cudaStatus = cudaMemcpy(quat_d_x, quat_x, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_y, quat_y, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_z, quat_z, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_w, quat_w, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	cudaStatus = cudaMemcpy(quat_d_mejora_x, quat_x, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_mejora_y, quat_y, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_mejora_z, quat_z, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_mejora_w, quat_w, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	//cudaStatus = cudaMemset(energy_d_mejora, 0, energySize);
	
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr,"cuda memory error!\n");
}

extern void dataToGPU_multigpu_mejorar_inicializar_environment (unsigned int N, unsigned int N_mejora, type_data * &energy_d, unsigned int * &energy_nconformation_d, type_data * &energy_d_mejora, unsigned int * &energy_nconformation_d_mejora, type_data * &moves_x_d, type_data * &moves_y_d, type_data * &moves_z_d, type_data * &quat_d_x, type_data * &quat_d_y, type_data * &quat_d_z, type_data * &quat_d_w, type_data * &moves_x_d_mejora, type_data * &moves_y_d_mejora, type_data * &moves_z_d_mejora, type_data * &quat_d_x_mejora, type_data * &quat_d_y_mejora, type_data * &quat_d_z_mejora, type_data * &quat_d_w_mejora, curandState_t * &states_d_mejora, type_data* move_x, type_data* move_y, type_data* move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data* energy, unsigned int* energy_nconformation, type_data* move_x_mejora, type_data* move_y_mejora, type_data* move_z_mejora, type_data *quat_x_mejora, type_data *quat_y_mejora, type_data *quat_z_mejora, type_data *quat_w_mejora, type_data* energy_mejora, unsigned int* energy_nconformation_mejora, struct metaheuristic_t metaheuristic)
{	
	cudaError_t cudaStatus;

	//unsigned long int randSize = N * sizeof(curandState_t);	
	unsigned long int moveSize = N * sizeof(type_data);
	unsigned long int quatSize = N * sizeof(type_data);	
	unsigned long int nconformationsize = N * sizeof(int);
 	unsigned long int energySize = N * sizeof(type_data);

	unsigned long int randSize_environment = N_mejora * sizeof(curandState_t);	
	unsigned long int moveSize_environment = N_mejora * sizeof(type_data);
	unsigned long int quatSize_environment = N_mejora * sizeof(type_data);	
	unsigned long int nconformationsize_environment = N_mejora * sizeof(int);
 	unsigned long int energySize_environment = N_mejora * sizeof(type_data);	
	
	//reserva memoria para los movimientos
	cudaStatus = cudaMalloc((void**) &moves_x_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");	
	cudaStatus = cudaMalloc((void**) &moves_y_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_z_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &energy_d, energySize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &energy_nconformation_d, nconformationsize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");		
	cudaStatus = cudaMalloc((void**) &moves_x_d_mejora, moveSize_environment);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_y_d_mejora, moveSize_environment);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_z_d_mejora, moveSize_environment);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &energy_d_mejora, energySize_environment);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &energy_nconformation_d_mejora, nconformationsize_environment);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
 		
	cudaStatus = cudaMalloc((void**) &quat_d_x, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_y, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_z, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_w, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	
	cudaStatus = cudaMalloc((void**) &quat_d_x_mejora, quatSize_environment);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_y_mejora, quatSize_environment);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_z_mejora, quatSize_environment);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_w_mejora, quatSize_environment);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
		
	//cudaStatus = cudaMalloc((void**) &states_d, randSize);
	//if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc states_d values failed!");
	cudaStatus = cudaMalloc((void**) &states_d_mejora, randSize_environment);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc states_d values failed!");
	
	cudaStatus = cudaMemcpy(moves_x_d, move_x, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_y_d, move_y, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_z_d, move_z, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(energy_d, energy, energySize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(energy_nconformation_d, energy_nconformation, nconformationsize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	cudaStatus = cudaMemcpy(quat_d_x, quat_x, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_y, quat_y, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_z, quat_z, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_w, quat_w, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	cudaStatus = cudaMemcpy(moves_x_d_mejora, move_x_mejora, moveSize_environment, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_y_d_mejora, move_y_mejora, moveSize_environment, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_z_d_mejora, move_z_mejora, moveSize_environment, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(energy_d_mejora, energy_mejora, energySize_environment, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(energy_nconformation_d_mejora, energy_nconformation_mejora, nconformationsize_environment, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	cudaStatus = cudaMemcpy(quat_d_x_mejora, quat_x_mejora, quatSize_environment, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_y_mejora, quat_y_mejora, quatSize_environment, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_z_mejora, quat_z_mejora, quatSize_environment, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_w_mejora, quat_w_mejora, quatSize_environment, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	
	
}

extern void dataToGPU_mpi (unsigned int before, unsigned int N, unsigned int surface, type_data * &moves_x_d, type_data * &moves_y_d, type_data * &moves_z_d, type_data * &quat_d_x, type_data * &quat_d_y, type_data * &quat_d_z, type_data * &quat_d_w, curandState_t * &states_d, type_data* surface_x_h, type_data* surface_y_h, type_data* surface_z_h, type_data* &surface_x_d, type_data* &surface_y_d, type_data* &surface_z_d)
{	
	cudaError_t cudaStatus;

	unsigned long int randSize = N * sizeof(curandState_t);	
	unsigned long int moveSize = N * sizeof(type_data);
	unsigned long int quatSize = N * sizeof(type_data);
	unsigned long int surfaceSize = before * sizeof(type_data);
	
	
	//reserva memoria para los movimientos
	cudaStatus = cudaMalloc((void**) &moves_x_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_y_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_z_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");

	cudaStatus = cudaMalloc((void**) &quat_d_x, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_y, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_z, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_w, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	
	
	cudaStatus = cudaMalloc((void**) &states_d, randSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc states_d values failed!");
	cudaStatus = cudaMalloc((void**) &surface_x_d, surfaceSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc surface_x_d values failed!");
	cudaStatus = cudaMalloc((void**) &surface_y_d, surfaceSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc surface_y_d values failed!");	
	cudaStatus = cudaMalloc((void**) &surface_z_d, surfaceSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc surface_z_d values failed!");

	
	//PUESTO POR MI. INICIALIZA LOS VECTORES.
	cudaStatus = cudaMemset(moves_x_d, 0,moveSize); 
	cudaStatus = cudaMemset(moves_y_d, 0,moveSize);
	cudaStatus = cudaMemset(moves_z_d, 0,moveSize);
	cudaStatus = cudaMemset(quat_d_x, 0,moveSize);
	cudaStatus = cudaMemset(quat_d_y, 0,moveSize); 	
	cudaStatus = cudaMemset(quat_d_z, 0,moveSize); 
	cudaStatus = cudaMemset(quat_d_w, 0,moveSize); 
	
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemset moves_d failed!");
	cudaStatus = cudaMemset(surface_x_d, 0, surfaceSize); 
	cudaStatus = cudaMemset(surface_y_d, 0, surfaceSize);
	cudaStatus = cudaMemset(surface_z_d, 0, surfaceSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemset surface_d failed!");
	
	cudaStatus = cudaMemcpy(surface_x_d, surface_x_h, surfaceSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(surface_y_d, surface_y_h, surfaceSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(surface_z_d, surface_z_h, surfaceSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
}


extern void dataToGPU (unsigned int N, unsigned int surface, type_data * &moves_x_d, type_data * &moves_y_d, type_data * &moves_z_d, type_data * &quat_d_x, type_data * &quat_d_y, type_data * &quat_d_z, type_data * &quat_d_w, curandState_t * &states_d, type_data* surface_x_h, type_data* surface_y_h, type_data* surface_z_h, type_data* &surface_x_d, type_data* &surface_y_d, type_data* &surface_z_d){

	struct receptor_t rec_temp;
	cudaError_t cudaStatus;

	unsigned long int randSize = N * sizeof(curandState_t);	
	unsigned long int moveSize = N * sizeof(type_data);
	unsigned long int quatSize = N * sizeof(type_data);
	unsigned long int surfaceSize = surface * sizeof(type_data);
	
	
	//reserva memoria para los movimientos
	cudaStatus = cudaMalloc((void**) &moves_x_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_y_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_z_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");

	cudaStatus = cudaMalloc((void**) &quat_d_x, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_y, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_z, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_w, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	
	cudaStatus = cudaMalloc((void**) &states_d, randSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc states_d values failed!");
	cudaStatus = cudaMalloc((void**) &surface_x_d, surfaceSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc surface_x_d values failed!");
	cudaStatus = cudaMalloc((void**) &surface_y_d, surfaceSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc surface_y_d values failed!");	
	cudaStatus = cudaMalloc((void**) &surface_z_d, surfaceSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc surface_z_d values failed!");

	
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Error en memoria.\n");
//PUESTO POR MI. INICIALIZA LOS VECTORES.
	cudaStatus = cudaMemset(moves_x_d, 0,moveSize); 
	cudaStatus = cudaMemset(moves_y_d, 0,moveSize);
	cudaStatus = cudaMemset(moves_z_d, 0,moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemset moves_d failed!");
	/*daStatus = cudaMemset(surface_x_d, 0, surfaceSize); 
	cudaStatus = cudaMemset(surface_y_d, 0, surfaceSize);
	cudaStatus = cudaMemset(surface_z_d, 0, surfaceSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemset surface_d failed!");*/
	cudaStatus = cudaMemset(quat_d_x, 0,moveSize);
	cudaStatus = cudaMemset(quat_d_y, 0,moveSize); 	
	cudaStatus = cudaMemset(quat_d_z, 0,moveSize); 
	cudaStatus = cudaMemset(quat_d_w, 0,moveSize); 
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemset quat_d failed!");
	
	cudaStatus = cudaMemcpy(surface_x_d, surface_x_h, surfaceSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(surface_y_d, surface_y_h, surfaceSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(surface_z_d, surface_z_h, surfaceSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
}

extern void dataToGPUEnergy (unsigned int N, type_data * moves_x, type_data * moves_y, type_data * moves_z, type_data * quat_x, type_data * quat_y, type_data * quat_z, type_data * quat_w, type_data * &moves_x_d, type_data * &moves_y_d, type_data * &moves_z_d, type_data * &quat_d_x, type_data * &quat_d_y, type_data * &quat_d_z, type_data * &quat_d_w)
{

	unsigned long int moveSize = N * sizeof(type_data);
	unsigned long int quatSize = N * sizeof(type_data);
	cudaError_t cudaStatus;
	
//reserva memoria para los movimientos
	cudaStatus = cudaMalloc((void**) &moves_x_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_y_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_z_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");

	cudaStatus = cudaMalloc((void**) &quat_d_x, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_y, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_z, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_w, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	
	cudaStatus = cudaMemcpy(moves_x_d, moves_x, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_y_d, moves_y, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_z_d, moves_z, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");

	cudaStatus = cudaMemcpy(quat_d_x, quat_x, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_y, quat_y, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_z, quat_z, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_w, quat_w, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
}

extern void dataToGPU_combination_move (unsigned int nconformations_s, type_data * &moves_x_d_s, type_data * &moves_y_d_s, type_data * &moves_z_d_s)
{
	unsigned long int moveSize_s = nconformations_s * sizeof(type_data);	
	cudaError_t cudaStatus;
		
	cudaStatus = cudaMalloc((void**) &moves_x_d_s, moveSize_s);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_y_d_s, moveSize_s);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_z_d_s, moveSize_s);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
		
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) printf("Error en copia\n");
	
	
}

extern void dataToGPU_combination_quat_energy (struct vectors_t *vectors_s, type_data * &quat_d_s_x, type_data * &quat_d_s_y, type_data * &quat_d_s_z, type_data * &quat_d_s_w, type_data * &energy_d_s, curandState_t * &states_d) 
{	
	unsigned long int quatSize_s = vectors_s->nconformations * sizeof(type_data);	
	unsigned long int energySize_s = vectors_s->nconformations * sizeof(type_data);
	unsigned long int randSize_s = vectors_s->nconformations * sizeof(curandState_t);

	cudaError_t cudaStatus;
	cudaStatus = cudaMalloc((void**) &quat_d_s_x, quatSize_s);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d_e values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_s_y, quatSize_s);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d_e values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_s_z, quatSize_s);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d_e values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_s_w, quatSize_s);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d_e values failed!");
	
	
	cudaStatus = cudaMalloc((void**) &energy_d_s, energySize_s);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc energy_d_e values failed!");
	
		
	cudaStatus = cudaMalloc((void**) &states_d, randSize_s);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc states_d values failed!");
		
}

extern void dataToGPU_nconformations (struct vectors_t *vectors_s, unsigned int * &n_conformations_d_s) 
{
	unsigned long int positionSize = vectors_s->nconformations * sizeof(int);	
	cudaError_t cudaStatus;
	cudaStatus = cudaMalloc((void**) &n_conformations_d_s, positionSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc n_conformations_d_s values failed!");

	cudaMemcpy(n_conformations_d_s,vectors_s->energy.n_conformation,positionSize,cudaMemcpyHostToDevice);
}

extern void dataToGPU_mejorar_bucle(struct vectors_t *vectors_e_s, type_data * &energy_d, unsigned int * &energy_nconformation_d, type_data * &energy_d_mejora, unsigned int * &energy_nconformation_d_mejora, type_data * &moves_x_d, type_data * &moves_y_d, type_data * &moves_z_d, type_data * &quat_d_x, type_data * &quat_d_y, type_data * &quat_d_z, type_data * &quat_d_w, type_data * &moves_x_d_mejora, type_data * &moves_y_d_mejora, type_data * &moves_z_d_mejora, type_data * &quat_d_mejora_x, type_data * &quat_d_mejora_y, type_data * &quat_d_mejora_z, type_data * &quat_d_mejora_w, curandState_t *&states_d ,struct metaheuristic_t metaheuristic)
{
	cudaError_t cudaStatus;
	unsigned long int randSize = vectors_e_s->nconformations * sizeof(curandState_t);	
	unsigned long int moveSize = vectors_e_s->nconformations * sizeof(type_data);	
	unsigned long int nconformationsize = vectors_e_s->nconformations * sizeof(int);
 	unsigned long int energySize = vectors_e_s->nconformations * sizeof(type_data);		
	unsigned long int quatSize = vectors_e_s->nconformations * sizeof(type_data);		
	
	cudaStatus = cudaMalloc((void**) &moves_x_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_y_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_z_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &energy_d, energySize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &energy_nconformation_d, nconformationsize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
		
	cudaStatus = cudaMalloc((void**) &moves_x_d_mejora, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_y_d_mejora, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_z_d_mejora, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &energy_d_mejora, energySize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &energy_nconformation_d_mejora, nconformationsize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	
	cudaStatus = cudaMalloc((void**) &quat_d_x, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_y, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_z, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_w, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	
	cudaStatus = cudaMalloc((void**) &quat_d_mejora_x, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_mejora_y, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_mejora_z, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_mejora_w, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
		
	cudaStatus = cudaMalloc((void**) &states_d, randSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc states_d values failed!");

		
	cudaStatus = cudaMemcpy(moves_x_d, vectors_e_s->move_x, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_y_d, vectors_e_s->move_y, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_z_d, vectors_e_s->move_z, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(energy_d, vectors_e_s->energy.energy, energySize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(energy_nconformation_d, vectors_e_s->energy.n_conformation, nconformationsize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");

	cudaStatus = cudaMemcpy(moves_x_d_mejora, vectors_e_s->move_x, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_y_d_mejora, vectors_e_s->move_y, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_z_d_mejora, vectors_e_s->move_z, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(energy_nconformation_d_mejora, vectors_e_s->energy.n_conformation, nconformationsize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
		
	cudaStatus = cudaMemcpy(quat_d_x, vectors_e_s->quat_x, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_y, vectors_e_s->quat_y, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_z, vectors_e_s->quat_z, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_w, vectors_e_s->quat_w, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	cudaStatus = cudaMemcpy(quat_d_mejora_x, vectors_e_s->quat_x, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_mejora_y, vectors_e_s->quat_y, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_mejora_z, vectors_e_s->quat_z, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_mejora_w, vectors_e_s->quat_w, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemset(energy_d_mejora, 10000.0, energySize);
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "FIN copia \n");
}

extern void dataToGPU_mejorar_montecarlo(unsigned int N, type_data * &energy_d, unsigned int * &energy_nconformation_d, type_data * &energy_d_mejora, type_data * &moves_x_d, type_data * &moves_y_d, type_data * &moves_z_d, type_data * &quat_d_x, type_data * &quat_d_y, type_data * &quat_d_z, type_data * &quat_d_w, curandState_t * &states_d, type_data* move_x, type_data* move_y, type_data* move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data* energy, unsigned int* energy_nconformation, struct metaheuristic_t metaheuristic) 
{
	cudaError_t cudaStatus;

	unsigned long int randSize = N * sizeof(curandState_t);	
	unsigned long int moveSize = N * sizeof(type_data);
	unsigned long int quatSize = N * sizeof(type_data);	
	unsigned long int nconformationsize = N * sizeof(int);
 	unsigned long int energySize = N * sizeof(type_data);		
	
	cudaStatus = cudaMalloc((void**) &moves_x_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");	
	cudaStatus = cudaMalloc((void**) &moves_y_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_z_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &energy_d, energySize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &energy_nconformation_d, nconformationsize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");		
	cudaStatus = cudaMalloc((void**) &energy_d_mejora, energySize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
		
	cudaStatus = cudaMalloc((void**) &quat_d_x, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_y, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_z, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_w, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	
	cudaStatus = cudaMalloc((void**) &states_d, randSize);
	cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc states_d values failed!");
	//printf("Sin error\n");
	
	
	cudaStatus = cudaMemcpy(moves_x_d, move_x, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_y_d, move_y, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_z_d, move_z, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(energy_d, energy, energySize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(energy_nconformation_d, energy_nconformation, nconformationsize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");

	cudaStatus = cudaMemcpy(quat_d_x, quat_x, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_y, quat_y, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_z, quat_z, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_w, quat_w, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	cudaStatus = cudaMemset(energy_d_mejora, 0, energySize);
	
}

extern void dataToGPU_mejorar_montecarlo_environment(unsigned int nconformations, type_data * &energy_d, unsigned int * &energy_nconformation_d, type_data * &energy_d_mejora, unsigned int  *&energy_nconformation_d_mejora, type_data * &moves_x_d, type_data * &moves_y_d, type_data * &moves_z_d, type_data * &quat_d_x, type_data * &quat_d_y, type_data * &quat_d_z, type_data * &quat_d_w, type_data * &move_x_d_mejora, type_data * &move_y_d_mejora, type_data * &move_z_d_mejora, type_data * &quat_x_d_mejora, type_data * &quat_y_d_mejora, type_data * &quat_z_d_mejora, type_data * &quat_w_d_mejora, curandState_t * &states_d_mejora, type_data * move_x, type_data * move_y, type_data * move_z, type_data * quat_x, type_data * quat_y, type_data * quat_z, type_data * quat_w, type_data * energy,unsigned int *n_conformation,type_data * move_x_mejora, type_data * move_y_mejora, type_data * move_z_mejora, type_data * quat_x_mejora, type_data * quat_y_mejora, type_data * quat_z_mejora, type_data * quat_w_mejora, type_data * energy_mejora,unsigned int *n_conformation_mejora, struct metaheuristic_t metaheuristic)
{
	cudaError_t cudaStatus;

	
	unsigned long int moveSize = nconformations * sizeof(type_data);
	unsigned long int quatSize = nconformations * sizeof(type_data);	
	unsigned long int nconformationsize = nconformations * sizeof(unsigned int);
 	unsigned long int energySize = nconformations * sizeof(type_data);

	unsigned long int randSize_environment = nconformations * metaheuristic.NEEImp * sizeof(curandState_t);	
	unsigned long int moveSize_environment = nconformations * metaheuristic.NEEImp * sizeof(type_data);
	unsigned long int quatSize_environment = nconformations * metaheuristic.NEEImp * sizeof(type_data);	
	unsigned long int nconformationsize_environment = nconformations * metaheuristic.NEEImp * sizeof(unsigned int);
 	unsigned long int energySize_environment = nconformations * metaheuristic.NEEImp * sizeof(type_data);	
	
	cudaStatus = cudaMalloc((void**) &moves_x_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");	
	cudaStatus = cudaMalloc((void**) &moves_y_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_z_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &energy_d, energySize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &energy_nconformation_d, nconformationsize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");		
		
	cudaStatus = cudaMalloc((void**) &quat_d_x, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_y, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_z, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_w, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	
	cudaStatus = cudaMalloc((void**) &move_x_d_mejora, moveSize_environment);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");	
	cudaStatus = cudaMalloc((void**) &move_y_d_mejora, moveSize_environment);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &move_z_d_mejora, moveSize_environment);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
		
	cudaStatus = cudaMalloc((void**) &quat_x_d_mejora, quatSize_environment);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_y_d_mejora, quatSize_environment);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_z_d_mejora, quatSize_environment);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_w_d_mejora, quatSize_environment);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	
	cudaStatus = cudaMalloc((void**) &energy_d_mejora, energySize_environment);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &energy_nconformation_d_mejora, nconformationsize_environment);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");		
	
	cudaStatus = cudaMalloc((void**) &states_d_mejora, randSize_environment);
	cudaDeviceSynchronize();
    cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc states_d values failed!");
	
	cudaStatus = cudaMemcpy(moves_x_d, move_x, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_y_d, move_y, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(moves_z_d, move_z, moveSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(energy_d, energy, energySize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(energy_nconformation_d, n_conformation, nconformationsize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");

	cudaStatus = cudaMemcpy(quat_d_x, quat_x, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_y, quat_y, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_z, quat_z, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_d_w, quat_w, quatSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	cudaStatus = cudaMemcpy(move_x_d_mejora, move_x_mejora, moveSize_environment, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(move_y_d_mejora, move_y_mejora, moveSize_environment, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(move_z_d_mejora, move_z_mejora, moveSize_environment, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(energy_d_mejora, energy_mejora, energySize_environment, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(energy_nconformation_d_mejora, n_conformation_mejora, nconformationsize_environment, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");

	cudaStatus = cudaMemcpy(quat_x_d_mejora, quat_x_mejora, quatSize_environment, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_y_d_mejora, quat_y_mejora, quatSize_environment, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_z_d_mejora, quat_z_mejora, quatSize_environment, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_w_d_mejora, quat_w_mejora, quatSize_environment, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
}
