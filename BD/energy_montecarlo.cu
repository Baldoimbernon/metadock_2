#include "energy_struct.h"
#include "energy_common.h"
#include "energy_kernel.h"
#include "energy_flexibility.h"
#include "energy_moving.h"
#include "energy_common-gpu.h"
#include "energy_montecarlo.h"
#include "energy_positions_common.h"
#include <thrust/sequence.h>

extern void montecarlo_environment  (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, struct vectors_t *vectors_e_s, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int orden_device, unsigned int fase){

	struct vectors_t vectors_mejora;
	size_t available,total_m;
    cudaError_t cudaStatus;
    unsigned long int max_memory;
    unsigned long int max_positions;
    unsigned long int max_common;
 
    unsigned long int typeSize = proteina.nrec * sizeof(char);
    unsigned long int recSize = proteina.nrec * sizeof(type_data);
    unsigned long int bondSize = proteina.nrec * sizeof(int) * MAXBOND;
	
	unsigned long int ligTypeSize = ligando.nlig * sizeof(char);
    unsigned long int ligSize = ligando.nlig * sizeof(type_data);
    unsigned long int ligBondSize = ligando.nlig * sizeof(int) * MAXBOND;
    
    unsigned long int randSize = vectors_e_s->nconformations * sizeof(curandState_t);
    unsigned long int moveSize_tam = vectors_e_s->nconformations * sizeof(type_data);
    unsigned long int energyConf_tam = vectors_e_s->nconformations * sizeof(int);
    unsigned long int confSize_tam = vectors_e_s->nconformations * vectors_e_s->nlig * sizeof(type_data);
	
	unsigned long int randSize_environment = vectors_e_s->nconformations * metaheuristic.NEEImp * sizeof(curandState_t);
    unsigned long int moveSize_tam_environment = vectors_e_s->nconformations * metaheuristic.NEEImp * sizeof(type_data);
    unsigned long int energyConf_tam_environment = vectors_e_s->nconformations * metaheuristic.NEEImp * sizeof(int);
    unsigned long int confSize_tam_environment = vectors_e_s->nconformations * metaheuristic.NEEImp * ligando.nlig * sizeof(type_data);
	
    unsigned long int tam_max, tam_tmp, nconformations_count_total, nconformations_count_partial, tam_conformation, stride_d;

    max_positions = (moveSize_tam * 8) * 2 + energyConf_tam + (moveSize_tam_environment * 8) * 2 + energyConf_tam_environment;
    max_common = (recSize * 4) + typeSize + bondSize + (ligSize * 4) + ligTypeSize + ligBondSize;

    max_memory = max_positions + max_common;
    tam_conformation =  (sizeof(type_data) * 8 * 2) + sizeof(int) + sizeof(curandState_t) + ((sizeof(type_data) * 8 * 2) + sizeof(int) + sizeof(curandState_t)) * metaheuristic.NEEImp;

	vectors_mejora.num_surface = vectors_e_s->num_surface;
    vectors_mejora.nconformations = vectors_e_s->nconformations * metaheuristic.NEEImp;

    vectors_mejora.move_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    vectors_mejora.move_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    vectors_mejora.move_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    vectors_mejora.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    vectors_mejora.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    vectors_mejora.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    vectors_mejora.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    vectors_mejora.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations);
    vectors_mejora.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    vectors_mejora.weights = (type_data *)malloc(sizeof(type_data)*SCORING_TERMS);

    memcpy(vectors_mejora.f_params, vectors_e_s->f_params,MAXTYPES * sizeof(struct force_field_param_t));
    memcpy(vectors_mejora.weights, vectors_e_s->weights,SCORING_TERMS * sizeof(type_data));

    thrust::sequence(vectors_mejora.energy.n_conformation,vectors_mejora.energy.n_conformation + vectors_mejora.nconformations);
	fill_conformations_environment (vectors_e_s->move_x,vectors_e_s->move_y,vectors_e_s->move_z,vectors_e_s->quat_x,vectors_e_s->quat_y,vectors_e_s->quat_z,vectors_e_s->quat_w,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,vectors_e_s->nconformations,param,metaheuristic);
	
	cudaSetDevice(devices->id[orden_device]);
    //cudaDeviceReset();
    cudaMemGetInfo(&tam_max, &total_m);
    cudaDeviceSynchronize();
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
    //std::cout << tam_max << " " << total_m << "\n";
    tam_tmp = tam_max - (tam_max * 0.2);
	
	if (tam_tmp < max_memory)
        {
                //printf("Trocear en 1 GPU\n");
                max_memory = max_common;
                nconformations_count_total = 0;
                nconformations_count_partial = 0;
                stride_d = 0;
                while (nconformations_count_total < vectors_e_s->nconformations)
                {
                        max_memory += tam_conformation;
                        if (max_memory > tam_tmp)
                        {
                                //printf("Trozo %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
                                //nconformations_count_total--;
                                //nconformations_count_partial--;
                                //Lanzar con nconformations_count_partial;
								multigpu_mejorar_montecarlo_environment (proteina,ligando,param,vectors_e_s->weights,vectors_e_s->f_params,devices,vectors_e_s->move_x+stride_d,vectors_e_s->move_y+stride_d,vectors_e_s->move_z+stride_d,vectors_e_s->quat_x+stride_d,vectors_e_s->quat_y+stride_d,vectors_e_s->quat_z+stride_d,vectors_e_s->quat_w+stride_d,vectors_e_s->energy.energy+stride_d,vectors_e_s->energy.n_conformation+stride_d,vectors_mejora.move_x+(stride_d*metaheuristic.NEEImp),vectors_mejora.move_y+(stride_d*metaheuristic.NEEImp),vectors_mejora.move_z+(stride_d*metaheuristic.NEEImp),vectors_mejora.quat_x+(stride_d*metaheuristic.NEEImp),vectors_mejora.quat_y+(stride_d*metaheuristic.NEEImp),vectors_mejora.quat_z+(stride_d*metaheuristic.NEEImp),vectors_mejora.quat_w+(stride_d*metaheuristic.NEEImp),vectors_mejora.energy.energy+(stride_d*metaheuristic.NEEImp),vectors_mejora.energy.n_conformation+(stride_d*metaheuristic.NEEImp),metaheuristic,nconformations_count_partial,nconformations_count_partial*metaheuristic.NEEImp,orden_device,stride_d,fase);;
								stride_d += nconformations_count_partial;
                                max_memory = max_common;
                                nconformations_count_partial = 0;
                        }
                        else
                        {
                                nconformations_count_total++;
                                nconformations_count_partial++;
                        }
                }
				nconformations_count_partial--;
				multigpu_mejorar_montecarlo_environment (proteina,ligando,param,vectors_e_s->weights,vectors_e_s->f_params,devices,vectors_e_s->move_x+stride_d,vectors_e_s->move_y+stride_d,vectors_e_s->move_z+stride_d,vectors_e_s->quat_x+stride_d,vectors_e_s->quat_y+stride_d,vectors_e_s->quat_z+stride_d,vectors_e_s->quat_w+stride_d,vectors_e_s->energy.energy+stride_d,vectors_e_s->energy.n_conformation+stride_d,vectors_mejora.move_x+(stride_d*metaheuristic.NEEImp),vectors_mejora.move_y+(stride_d*metaheuristic.NEEImp),vectors_mejora.move_z+(stride_d*metaheuristic.NEEImp),vectors_mejora.quat_x+(stride_d*metaheuristic.NEEImp),vectors_mejora.quat_y+(stride_d*metaheuristic.NEEImp),vectors_mejora.quat_z+(stride_d*metaheuristic.NEEImp),vectors_mejora.quat_w+(stride_d*metaheuristic.NEEImp),vectors_mejora.energy.energy+(stride_d*metaheuristic.NEEImp),vectors_mejora.energy.n_conformation+(stride_d*metaheuristic.NEEImp),metaheuristic,nconformations_count_partial,nconformations_count_partial*metaheuristic.NEEImp,orden_device,stride_d,fase);;
	}

        else
        {
                //printf("No Trocea\n");
                //Lanzar con todo
				multigpu_mejorar_montecarlo_environment (proteina,ligando,param,vectors_e_s->weights,vectors_e_s->f_params,devices,vectors_e_s->move_x,vectors_e_s->move_y,vectors_e_s->move_z,vectors_e_s->quat_x,vectors_e_s->quat_y,vectors_e_s->quat_z,vectors_e_s->quat_w,vectors_e_s->energy.energy,vectors_e_s->energy.n_conformation,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,vectors_mejora.energy.energy,vectors_mejora.energy.n_conformation,metaheuristic,vectors_e_s->nconformations,vectors_mejora.nconformations,orden_device,stride_d,fase);
		}
}

extern void multigpu_mejorar_montecarlo_environment (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, type_data *weights, struct force_field_param_t *f_params, struct device_t *devices, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *n_conformation, type_data *move_x_mejora, type_data *move_y_mejora, type_data *move_z_mejora, type_data *quat_x_mejora, type_data *quat_y_mejora, type_data *quat_z_mejora, type_data *quat_w_mejora, type_data *energy_mejora, unsigned int *n_conformation_mejora, struct metaheuristic_t metaheuristic, unsigned int nconformations, unsigned int nconformations_mejora, unsigned int orden_device, unsigned int stride_d, unsigned int fase)
{
	curandState_t *states_d_mejora;
	//struct receptor_t *proteina_d;
	//struct ligand_t *ligando_d;
	type_data *moves_x_d, *moves_y_d, *moves_z_d, *energy_d;		
	type_data *move_x_d_mejora, *move_y_d_mejora, *move_z_d_mejora;		
	type_data *quat_x_d, *quat_y_d, *quat_z_d, *quat_w_d;
	type_data *quat_x_d_mejora, *quat_y_d_mejora, *quat_z_d_mejora, *quat_w_d_mejora;
	type_data *energy_d_mejora;
	unsigned int *energy_nconformation_d, *energy_nconformation_d_mejora;		
	
	type_data *rec_x_d, *rec_y_d, *rec_z_d, *qr_d;
	char *rectype_d, *ligtype_d;
	unsigned int *bondsr_d, *bonds_d;
	type_data *lig_x_d, *lig_y_d, *lig_z_d, *ql_d;
	
	unsigned int steps, total, moveType=MOVE;
	cudaError_t cudaStatus;
	unsigned int moveSize = nconformations * sizeof(type_data);
	unsigned int quatSize = nconformations * sizeof(type_data);
	unsigned int moveSize_environment = nconformations_mejora * sizeof(type_data);
    unsigned int confSize_environment = nconformations_mejora * sizeof(type_data);
	
	cudaSetDevice(devices->id[orden_device]);
	dataToGPU_mejorar_montecarlo_environment(nconformations, energy_d, energy_nconformation_d, energy_d_mejora, energy_nconformation_d_mejora, moves_x_d, moves_y_d, moves_z_d, quat_x_d, quat_y_d, quat_z_d, quat_w_d, move_x_d_mejora, move_y_d_mejora, move_z_d_mejora, quat_x_d_mejora, quat_y_d_mejora, quat_z_d_mejora, quat_w_d_mejora, states_d_mejora, move_x, move_y, move_z, quat_x, quat_y, quat_z, quat_w, energy,n_conformation,move_x_mejora, move_y_mejora, move_z_mejora, quat_x_mejora, quat_y_mejora, quat_z_mejora, quat_w_mejora, energy_mejora,n_conformation_mejora,metaheuristic);
	//commonDataToGPU (proteina, proteina_d, ligando, ligando_d);
	commonDataToGPU_WS (proteina,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,ligando,lig_x_d,lig_y_d,lig_z_d,ql_d,ligtype_d,bonds_d);
	//HostToGPU_improve_environment (nconformations_mejora,move_x_d_mejora, move_y_d_mejora, move_z_d_mejora, quat_x_d_mejora, quat_y_d_mejora, quat_z_d_mejora, quat_w_d_mejora);
	save_params(weights,f_params);
	 //Inicializa el generador de numeros aleatorios y el cuaternion de cada simulacion
	setupCurandState <<< (nconformations_mejora/devices->hilos[orden_device*metaheuristic.num_kernels+4])+1, devices->hilos[orden_device*metaheuristic.num_kernels+4] >>>  (states_d_mejora, param.automatic_seed,param.seed,nconformations_mejora);
	cudaDeviceSynchronize();
	// check if kernel execution generated and error
	cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");
	
	if (fase == 0)
		total = metaheuristic.IMEIni;
	if (fase == 1)	
		total = metaheuristic.IMEImp;
	if (fase == 2)
		total = metaheuristic.IMEMUCom;
	//POR AKI
	steps = 0;
	//En cada paso de minimización

	cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeFourByte);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_by_warp, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_by_warp_type1, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_by_warp_type2, cudaFuncCachePreferEqual);
	//exit(0);
	while (steps < total){
		//printf("step %d\n", steps);

		if (moveType == MOVE)	
			move_mejora_montecarlo_environment <<< (nconformations_mejora / devices->hilos[orden_device*metaheuristic.num_kernels+5])+1, devices->hilos[orden_device*metaheuristic.num_kernels+5] >>> (states_d_mejora,param.max_desp,move_x_d_mejora,move_y_d_mejora,move_z_d_mejora,nconformations_mejora);				
		else 
			rotation_montecarlo_environment <<< (nconformations_mejora / devices->hilos[orden_device*metaheuristic.num_kernels+2])+1,devices->hilos[orden_device*metaheuristic.num_kernels+2] >>> (states_d_mejora,param.rotation,quat_x_d_mejora, quat_y_d_mejora, quat_z_d_mejora, quat_w_d_mejora,nconformations_mejora);				

		// check if kernel execution generated and error
		cudaDeviceSynchronize();
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel moveKernel o rotationKernel execution failed \n");
		
		//Calcula la energía ligando-receptor
		unsigned int size = 5 * devices->hilos[orden_device*metaheuristic.num_kernels] * sizeof(type_data) + sizeof(char)*devices->hilos[orden_device*metaheuristic.num_kernels];	
		unsigned int blk = ceil(nconformations_mejora*WARP_SIZE / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
		unsigned long int max = nconformations_mejora* WARP_SIZE;	
		dim3 hilos (devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);
		dim3 grid  (ceil(blk/8)+1, 8);
	
		switch (param.scoring_function_type)
		{	
			case 0:
				Gpu_full_Kernel_Conformations_by_warp <<<grid,hilos,size>>> (proteina.nrec,ligando.atoms,ligando.nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,move_x_d_mejora,move_y_d_mejora,move_z_d_mejora,quat_x_d_mejora,quat_y_d_mejora,quat_z_d_mejora,quat_w_d_mejora,lig_x_d,lig_y_d,lig_z_d,ql_d,ligtype_d,bonds_d,energy_d_mejora,energy_nconformation_d_mejora,stride_d*metaheuristic.NEEImp,devices->hilos[orden_device*metaheuristic.num_kernels],max);
				break;
			case 1:
                                Gpu_full_Kernel_Conformations_by_warp_type1 <<<grid,hilos,size>>> (proteina.nrec,ligando.atoms,ligando.nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,move_x_d_mejora,move_y_d_mejora,move_z_d_mejora,quat_x_d_mejora,quat_y_d_mejora,quat_z_d_mejora,quat_w_d_mejora,lig_x_d,lig_y_d,lig_z_d,ql_d,ligtype_d,bonds_d,energy_d_mejora,energy_nconformation_d_mejora,stride_d*metaheuristic.NEEImp,devices->hilos[orden_device*metaheuristic.num_kernels],max);
                                break;
			case 2:
				Gpu_full_Kernel_Conformations_by_warp_type2 <<<grid,hilos,size>>> (proteina.nrec,ligando.atoms,ligando.nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,move_x_d_mejora,move_y_d_mejora,move_z_d_mejora,quat_x_d_mejora,quat_y_d_mejora,quat_z_d_mejora,quat_w_d_mejora,lig_x_d,lig_y_d,lig_z_d,ql_d,ligtype_d,bonds_d,energy_d_mejora,energy_nconformation_d_mejora,stride_d*metaheuristic.NEEImp,devices->hilos[orden_device*metaheuristic.num_kernels],max);
                                break;
		}

		cudaDeviceSynchronize();
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel energy failed!");
				
		if (moveType == MOVE)
		{
			updateMove_environment <<< (nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+5])+1, devices->hilos[orden_device*metaheuristic.num_kernels+5]  >>> (states_d_mejora, param.max_desp, moves_x_d, moves_y_d, moves_z_d, move_x_d_mejora, move_y_d_mejora, move_z_d_mejora, energy_d_mejora, energy_d, metaheuristic.NEEImp, nconformations);
			moveType = ROTATE;
		}
		else
		{
			updateRotation_environment <<< (nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+2])+1,devices->hilos[orden_device*metaheuristic.num_kernels+2] >>> (states_d_mejora, param.rotation, quat_x_d, quat_y_d, quat_z_d, quat_w_d, quat_x_d_mejora, quat_y_d_mejora, quat_z_d_mejora, quat_w_d_mejora, energy_d_mejora, energy_d, metaheuristic.NEEImp, nconformations);
			moveType = MOVE;
		}

		// check if kernel execution generated and error
		cudaDeviceSynchronize();
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel updateMoveKernel o updateRotKernel execution failed \n");
		
		//TRAIGO DE LA GPU UNA CONFORMACION Y SU ENERGIA PARA RMSD
		/*if (iteration == 1)
		{
			cudaStatus = cudaMemcpy(conformation_0, (conformations_d + (4396 * ligando_h.nlig * 3)) , 3 * ligando_h.atoms *sizeof(double), cudaMemcpyDeviceToHost);
			if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy conformation_0 failed!");
			cudaStatus = cudaMemcpy(&value_energy_rmsd, (energy_ant + 4396), sizeof(double), cudaMemcpyDeviceToHost);
			//VOLCADO DEL LIGANDO A FICHERO PARA REPRESENTAR PASO.
			ligtemp.atoms = ligando_h.atoms;
			ligtemp.lig = (double*) malloc( 3*ligando_h.atoms * sizeof(double));
			struct quaternion_t q;
			float4ToQuaternion (&quat_h[4396], &q);
			//COPIAR LAS COORDENADAS DE CADA ATOMO DE LA CONFORMACION 0
		
			generarLigando_flex (conformation_0, ligtemp, &q, &moves2_h[3*4396], ligando_h.atoms);
			completarLigando(ligando_h,ligtemp);


			sprintf(cadena, "%s/pymol/path-sim-%d",params.output_dir, steps);
			printPos(ligtemp,cadena);
			temp = rmsd (ligtemp.lig, ligando_h.lig, ligando_h.nlig);
			fp << value_energy_rmsd << " " << temp << endl;
			free(ligtemp.lig);
			//FIN DEL VOLCADO*/
		
		steps++;
		fill_conformations_mejora <<<(nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+6])+1, devices->hilos[orden_device*metaheuristic.num_kernels+6]>>> (metaheuristic.NEEImp, move_x_d_mejora,move_y_d_mejora,move_z_d_mejora,quat_x_d_mejora,quat_y_d_mejora,quat_z_d_mejora,quat_w_d_mejora,energy_d_mejora,moves_x_d,moves_y_d,moves_z_d,quat_x_d,quat_y_d,quat_z_d,quat_w_d,energy_d,nconformations);
	}
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernels antes de copia fallo!\n");
	//GPUtoHost (vectors_e_s->nconformations,vectors_e_s,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_d_mejora,energy_d_mejora);
	cudaStatus = cudaMemcpy(move_x, moves_x_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_x_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(move_y, moves_y_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_y_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(move_z, moves_z_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_z_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(energy, energy_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values energy_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_x, quat_x_d, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_y, quat_y_d, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_z, quat_z_d, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_w, quat_w_d, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d GPUtoHost failed!");	
	
	cudaFree(states_d_mejora);
	cudaFree(moves_x_d);
	cudaFree(moves_y_d);
	cudaFree(moves_z_d);
	cudaFree(quat_x_d);
	cudaFree(quat_y_d);
	cudaFree(quat_z_d);
	cudaFree(quat_w_d);
	cudaFree(rec_x_d);
	cudaFree(rec_y_d);
	cudaFree(rec_z_d);
	cudaFree(qr_d);
	cudaFree(bondsr_d);
	cudaFree(rectype_d);
	cudaFree(lig_x_d);
	cudaFree(lig_y_d);
	cudaFree(lig_z_d);
	cudaFree(ql_d);
	cudaFree(ligtype_d);
	cudaFree(bonds_d);
	cudaFree(energy_d);
	cudaFree(energy_d_mejora);
	cudaFree(energy_nconformation_d);
	cudaFree(move_x_d_mejora);
	cudaFree(move_y_d_mejora);
	cudaFree(move_z_d_mejora);
	cudaFree(quat_x_d_mejora);
	cudaFree(quat_y_d_mejora);
	cudaFree(quat_z_d_mejora);
	cudaFree(quat_w_d_mejora);
	cudaDeviceReset();	
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)	printf("Error al finalizar montecarlo\n");
		
		
	
}
extern void montecarlo  (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, struct vectors_t *vectors_e_s, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int orden_device, unsigned int fase){

	size_t available,total_m;
    cudaError_t cudaStatus;
    unsigned long int max_memory;
    unsigned long int max_positions;
    unsigned long int max_common;
 
    unsigned long int typeSize = proteina.nrec * sizeof(char);
    unsigned long int recSize = proteina.nrec * sizeof(type_data);
    unsigned long int bondSize = proteina.nrec * sizeof(int) * MAXBOND;
	
	unsigned long int ligTypeSize = ligando.nlig * sizeof(char);
    unsigned long int ligSize = ligando.nlig * sizeof(type_data);
    unsigned long int ligBondSize = ligando.nlig * sizeof(int) * MAXBOND;
    
    unsigned long int randSize = vectors_e_s->nconformations * sizeof(curandState_t);
    unsigned long int moveSize_tam = vectors_e_s->nconformations * sizeof(type_data);
    unsigned long int energyConf_tam = vectors_e_s->nconformations * sizeof(int);
    unsigned long int confSize_tam = vectors_e_s->nconformations * vectors_e_s->nlig * sizeof(type_data);
    unsigned long int tam_max, tam_tmp, nconformations_count_total, nconformations_count_partial, tam_conformation, stride_d;

    max_positions = (moveSize_tam * 8) * 2 + energyConf_tam;
    max_common = (recSize * 4) + typeSize + bondSize + (ligSize * 4) + ligTypeSize + ligBondSize;

    max_memory = max_positions + max_common;
    tam_conformation =  (sizeof(type_data) * 8 * 2) + sizeof(int) + sizeof(curandState_t);

	cudaSetDevice(devices->id[orden_device]);
    //cudaDeviceReset();
    cudaMemGetInfo(&tam_max, &total_m);
    cudaDeviceSynchronize();
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
    //std::cout << tam_max << " " << total_m << "\n";
    tam_tmp = tam_max - (tam_max * 0.2);

	if (tam_tmp < max_memory)
        {
                //printf("Trocear en 1 GPU\n");
                max_memory = max_common;
                nconformations_count_total = 0;
                nconformations_count_partial = 0;
                stride_d = 0;
                while (nconformations_count_total < vectors_e_s->nconformations)
                {
                        max_memory += tam_conformation;
                        if (max_memory > tam_tmp)
                        {
                                //printf("Trozo %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
                                //nconformations_count_total--;
                                //nconformations_count_partial--;
                                //Lanzar con nconformations_count_partial;
								multigpu_mejorar_montecarlo (proteina,ligando,param,vectors_e_s->weights,vectors_e_s->f_params,devices,vectors_e_s->move_x+stride_d,vectors_e_s->move_y+stride_d,vectors_e_s->move_z+stride_d,vectors_e_s->quat_x+stride_d,vectors_e_s->quat_y+stride_d,vectors_e_s->quat_z+stride_d,vectors_e_s->quat_w+stride_d,vectors_e_s->energy.energy+stride_d,vectors_e_s->energy.n_conformation+stride_d,metaheuristic,nconformations_count_partial,orden_device,stride_d,fase);

                                stride_d += nconformations_count_partial;
                                max_memory = max_common;
                                nconformations_count_partial = 0;
                        }
                        else
                        {
                                nconformations_count_total++;
                                nconformations_count_partial++;
                        }
                }
                //printf("Trozo final %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
		nconformations_count_partial--;
		multigpu_mejorar_montecarlo (proteina,ligando,param,vectors_e_s->weights,vectors_e_s->f_params,devices,vectors_e_s->move_x+stride_d,vectors_e_s->move_y+stride_d,vectors_e_s->move_z+stride_d,vectors_e_s->quat_x+stride_d,vectors_e_s->quat_y+stride_d,vectors_e_s->quat_z+stride_d,vectors_e_s->quat_w+stride_d,vectors_e_s->energy.energy+stride_d,vectors_e_s->energy.n_conformation+stride_d,metaheuristic,nconformations_count_partial,orden_device,stride_d,fase);
        }

        else
        {
                //printf("No Trocea\n");
                //Lanzar con todo
		multigpu_mejorar_montecarlo (proteina,ligando,param,vectors_e_s->weights,vectors_e_s->f_params,devices,vectors_e_s->move_x,vectors_e_s->move_y,vectors_e_s->move_z,vectors_e_s->quat_x,vectors_e_s->quat_y,vectors_e_s->quat_z,vectors_e_s->quat_w,vectors_e_s->energy.energy,vectors_e_s->energy.n_conformation,metaheuristic,vectors_e_s->nconformations,orden_device,0,fase);
        }
}
 
extern void multigpu_mejorar_montecarlo (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, type_data *weights, struct force_field_param_t *f_params, struct device_t *devices, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *n_conformation, struct metaheuristic_t metaheuristic, unsigned int nconformations, unsigned int orden_device, unsigned int stride_d, unsigned int fase)
{
	curandState_t *states_d;
	//struct receptor_t *proteina_d;
	//struct ligand_t *ligando_d;
	type_data *moves_x_d, *moves_y_d, *moves_z_d, *energy_d;		
	type_data *move_x_m, *move_y_m, *move_z_m;		
	type_data *quat_d_x, *quat_d_y, *quat_d_z, *quat_d_w;
	type_data *quat_x_m, *quat_y_m, *quat_z_m, *quat_w_m;
	type_data *energy_d_mejora;
	unsigned int *energy_nconformation_d;		
	
	type_data *rec_x_d, *rec_y_d, *rec_z_d, *qr_d;
	char *rectype_d, *ligtype_d;
	unsigned int *bondsr_d, *bonds_d;
	type_data *lig_x_d, *lig_y_d, *lig_z_d, *ql_d;
	
	unsigned int steps, total, moveType=MOVE;
	cudaError_t cudaStatus;
	unsigned int moveSize = nconformations * sizeof(type_data);
	unsigned int quatSize = nconformations * sizeof(type_data);
	
	cudaSetDevice(devices->id[orden_device]);	
	
	dataToGPU_mejorar_montecarlo(nconformations, energy_d, energy_nconformation_d, energy_d_mejora, moves_x_d, moves_y_d, moves_z_d, quat_d_x, quat_d_y, quat_d_z, quat_d_w, states_d, move_x,move_y, move_z, quat_x, quat_y, quat_z, quat_w, energy,n_conformation,metaheuristic);
	//commonDataToGPU (proteina, proteina_d, ligando, ligando_d);
	commonDataToGPU_WS (proteina,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,ligando,lig_x_d,lig_y_d,lig_z_d,ql_d,ligtype_d,bonds_d);
	HostToGPU_improve (nconformations,move_x_m,move_y_m,move_z_m,quat_x_m,quat_y_m,quat_z_m,quat_w_m,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w);
	save_params(weights,f_params);
	 //Inicializa el generador de numeros aleatorios y el cuaternion de cada simulacion
	setupCurandState <<< (nconformations/devices->hilos[orden_device*metaheuristic.num_kernels+4])+1, devices->hilos[orden_device*metaheuristic.num_kernels+4] >>>  (states_d, param.automatic_seed,param.seed,nconformations);
	cudaDeviceSynchronize();
	// check if kernel execution generated and error
	cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");
	
	if (fase == 0)
		total = metaheuristic.IMEIni;
	if (fase == 1)	
		total = metaheuristic.IMEImp;
	if (fase == 2)
		total = metaheuristic.IMEMUCom;
	
	steps = 0;

	cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeFourByte);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_by_warp, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_by_warp_type1, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_by_warp_type2, cudaFuncCachePreferEqual);

	//En cada paso de minimización
	while (steps < total){
		//printf("step %d\n", steps);

		if (moveType == MOVE)	
			move_mejora_montecarlo <<< (nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+5])+1, devices->hilos[orden_device*metaheuristic.num_kernels+5] >>> (states_d,param.max_desp,moves_x_d,moves_y_d,moves_z_d,move_x_m,move_y_m,move_z_m,nconformations);				
		else 
			rotation_montecarlo <<< (nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+2])+1,devices->hilos[orden_device*metaheuristic.num_kernels+2] >>> (states_d,param.rotation,quat_d_x,quat_d_y,quat_d_z,quat_d_w,quat_x_m, quat_y_m, quat_z_m, quat_w_m,nconformations);				

		// check if kernel execution generated and error
		cudaDeviceSynchronize();
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel moveKernel o rotationKernel execution failed \n");
		
		//Calcula la energía ligando-receptor
		unsigned int size = 5 * devices->hilos[orden_device*metaheuristic.num_kernels] * sizeof(type_data) + sizeof(char)*devices->hilos[orden_device*metaheuristic.num_kernels];	
		unsigned int blk = ceil(nconformations*WARP_SIZE / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
		unsigned long int max = nconformations* WARP_SIZE;	
		dim3 hilos (devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);
		dim3 grid  (ceil(blk/8)+1, 8);		                                                            

		switch (param.scoring_function_type)
		{
			case 0:
				Gpu_full_Kernel_Conformations_by_warp <<<grid,hilos,size>>> (proteina.nrec,ligando.atoms,ligando.nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,move_x_m,move_y_m,move_z_m,quat_x_m,quat_y_m,quat_z_m,quat_w_m,lig_x_d,lig_y_d,lig_z_d,ql_d,ligtype_d,bonds_d,energy_d_mejora,energy_nconformation_d,stride_d,devices->hilos[orden_device*metaheuristic.num_kernels],max);
				break;
			case 1:
				Gpu_full_Kernel_Conformations_by_warp_type1 <<<grid,hilos,size>>> (proteina.nrec,ligando.atoms,ligando.nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,move_x_m,move_y_m,move_z_m,quat_x_m,quat_y_m,quat_z_m,quat_w_m,lig_x_d,lig_y_d,lig_z_d,ql_d,ligtype_d,bonds_d,energy_d_mejora,energy_nconformation_d,stride_d,devices->hilos[orden_device*metaheuristic.num_kernels],max);
                                break;
			case 2:
				Gpu_full_Kernel_Conformations_by_warp_type2 <<<grid,hilos,size>>> (proteina.nrec,ligando.atoms,ligando.nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,move_x_m,move_y_m,move_z_m,quat_x_m,quat_y_m,quat_z_m,quat_w_m,lig_x_d,lig_y_d,lig_z_d,ql_d,ligtype_d,bonds_d,energy_d_mejora,energy_nconformation_d,stride_d,devices->hilos[orden_device*metaheuristic.num_kernels],max);
                                break;
		}

		cudaDeviceSynchronize();
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel energy failed!");
				
		if (moveType == MOVE)
		{
			updateMove <<< (nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+5])+1, devices->hilos[orden_device*metaheuristic.num_kernels+5]  >>> (states_d, param.max_desp, moves_x_d, moves_y_d, moves_z_d, move_x_m, move_y_m, move_z_m, energy_d_mejora, energy_d,nconformations);
			moveType = ROTATE;
		}
		else
		{
			updateRotation <<< (nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+2])+1,devices->hilos[orden_device*metaheuristic.num_kernels+2] >>> (states_d, param.rotation, quat_d_x, quat_d_y, quat_d_z, quat_d_w, quat_x_m, quat_y_m, quat_z_m, quat_w_m, energy_d_mejora, energy_d,nconformations);
			moveType = MOVE;
		}

		// check if kernel execution generated and error
		cudaDeviceSynchronize();
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel updateMoveKernel o updateRotKernel execution failed \n");

		//TRAIGO DE LA GPU UNA CONFORMACION Y SU ENERGIA PARA RMSD
		/*if (iteration == 1)
		{
			cudaStatus = cudaMemcpy(conformation_0, (conformations_d + (4396 * ligando_h.nlig * 3)) , 3 * ligando_h.atoms *sizeof(double), cudaMemcpyDeviceToHost);
			if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy conformation_0 failed!");
			cudaStatus = cudaMemcpy(&value_energy_rmsd, (energy_ant + 4396), sizeof(double), cudaMemcpyDeviceToHost);
			//VOLCADO DEL LIGANDO A FICHERO PARA REPRESENTAR PASO.
			ligtemp.atoms = ligando_h.atoms;
			ligtemp.lig = (double*) malloc( 3*ligando_h.atoms * sizeof(double));
			struct quaternion_t q;
			float4ToQuaternion (&quat_h[4396], &q);
			//COPIAR LAS COORDENADAS DE CADA ATOMO DE LA CONFORMACION 0
		
			generarLigando_flex (conformation_0, ligtemp, &q, &moves2_h[3*4396], ligando_h.atoms);
			completarLigando(ligando_h,ligtemp);


			sprintf(cadena, "%s/pymol/path-sim-%d",params.output_dir, steps);
			printPos(ligtemp,cadena);
			temp = rmsd (ligtemp.lig, ligando_h.lig, ligando_h.nlig);
			fp << value_energy_rmsd << " " << temp << endl;
			free(ligtemp.lig);
			//FIN DEL VOLCADO*/
		
		steps++;
	}
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernels antes de copia fallo!\n");
	//GPUtoHost (vectors_e_s->nconformations,vectors_e_s,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_d_mejora,energy_d_mejora);
	cudaStatus = cudaMemcpy(move_x, moves_x_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_x_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(move_y, moves_y_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_y_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(move_z, moves_z_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_z_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(energy, energy_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values energy_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_x, quat_d_x, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_y, quat_d_y, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_z, quat_d_z, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_w, quat_d_w, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d GPUtoHost failed!");	
	
	cudaFree(states_d);
	cudaFree(moves_x_d);
	cudaFree(moves_y_d);
	cudaFree(moves_z_d);
	cudaFree(quat_d_x);
	cudaFree(quat_d_y);
	cudaFree(quat_d_z);
	cudaFree(quat_d_w);
	cudaFree(rec_x_d);
	cudaFree(rec_y_d);
	cudaFree(rec_z_d);
	cudaFree(qr_d);
	cudaFree(bondsr_d);
	cudaFree(rectype_d);
	cudaFree(lig_x_d);
	cudaFree(lig_y_d);
	cudaFree(lig_z_d);
	cudaFree(ql_d);
	cudaFree(ligtype_d);
	cudaFree(bonds_d);
	cudaFree(energy_d);
	cudaFree(energy_d_mejora);
	cudaFree(energy_nconformation_d);
	cudaFree(move_x_m);
	cudaFree(move_y_m);
	cudaFree(move_z_m);
	cudaFree(quat_x_m);
	cudaFree(quat_y_m);
	cudaFree(quat_z_m);
	cudaFree(quat_w_m);
	cudaDeviceReset();	
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)	printf("Error al finalizar montecarlo\n");
}

extern void montecarlo_flex_environment  (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, struct vectors_t *vectors_e_s, struct metaheuristic_t metaheuristic, struct device_t *devices, struct flexibility_params_t *flexibility_params,unsigned int orden_device, unsigned int fase){

	struct vectors_t vectors_mejora;
	size_t available,total_m;
    cudaError_t cudaStatus;
    unsigned long int max_memory;
    unsigned long int max_positions;
    unsigned long int max_common;
	unsigned long int max_conf;
    unsigned long int typeSize = proteina.nrec * sizeof(char);
    unsigned long int recSize = proteina.nrec * sizeof(type_data);
    unsigned long int bondSize = proteina.nrec * sizeof(int) * MAXBOND;
	
	unsigned long int ligTypeSize = ligando.nlig * sizeof(char);
    unsigned long int ligSize = ligando.nlig * sizeof(type_data);
    unsigned long int ligBondSize = ligando.nlig * sizeof(int) * MAXBOND;
	
	unsigned long int confSize_tam = vectors_e_s->nconformations * ligando.nlig * sizeof(type_data);
	unsigned long int linksSize = flexibility_params->n_links * 2 * sizeof(int);
    unsigned long int linksfragmentsSize = flexibility_params->n_links * 2 * sizeof(int);
    unsigned long int fragmentstamSize = flexibility_params->n_fragments * sizeof(int);
    unsigned long int fragmentsSize = flexibility_params->n_links * (ligando.atoms - 2) * sizeof(int);
	unsigned long int boolSize = ligando.atoms * 2 * sizeof(bool);
    
	unsigned long int flexConfSize =  vectors_e_s->nconformations * sizeof(struct flexibility_data_t);
    unsigned long int randSize = vectors_e_s->nconformations * sizeof(curandState_t);
    unsigned long int moveSize_tam = vectors_e_s->nconformations * sizeof(type_data);
    unsigned long int energyConf_tam = vectors_e_s->nconformations * sizeof(int);
	
	unsigned long int randSize_environment = vectors_e_s->nconformations * metaheuristic.NEEImp * sizeof(curandState_t);
    unsigned long int moveSize_tam_environment = vectors_e_s->nconformations * metaheuristic.NEEImp * sizeof(type_data);
    unsigned long int energyConf_tam_environment = vectors_e_s->nconformations * metaheuristic.NEEImp * sizeof(int);
    unsigned long int confSize_tam_environment = vectors_e_s->nconformations * metaheuristic.NEEImp * ligando.nlig * sizeof(type_data);
	
    unsigned long int tam_max, tam_tmp, nconformations_count_total, nconformations_count_partial, tam_conformation, stride_d;

	max_positions = (moveSize_tam * 8) * 2 + moveSize_tam + energyConf_tam + (moveSize_tam_environment * 8) * 2 + + moveSize_tam_environment + energyConf_tam_environment;
	max_conf = (3 * confSize_tam) + (3 * confSize_tam_environment);
    max_common = (recSize * 4) + typeSize + bondSize + (ligSize * 4) + ligTypeSize + ligBondSize + (boolSize * 2) + flexConfSize + linksSize + linksfragmentsSize + fragmentstamSize + fragmentsSize;;

    max_memory = max_positions + max_conf + max_common;
    tam_conformation =  (sizeof(type_data) * 8 * 2) + sizeof(type_data) + sizeof(int) + sizeof(curandState_t) + (3 * ligando.nlig * sizeof(type_data)) + (3 * ((sizeof(type_data) * 8 * 2) + sizeof(type_data) + sizeof(int) + sizeof(curandState_t) + (3 * ligando.nlig * sizeof(type_data))));

	vectors_mejora.num_surface = vectors_e_s->num_surface;
    vectors_mejora.nconformations = vectors_e_s->nconformations * metaheuristic.NEEImp;
    vectors_mejora.conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations*ligando.nlig);
    vectors_mejora.conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations*ligando.nlig);
    vectors_mejora.conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations*ligando.nlig);

    vectors_mejora.move_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    vectors_mejora.move_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    vectors_mejora.move_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    vectors_mejora.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    vectors_mejora.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    vectors_mejora.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    vectors_mejora.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    vectors_mejora.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations);
    vectors_mejora.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    vectors_mejora.weights = (type_data *)malloc(sizeof(type_data)*SCORING_TERMS);

    memcpy(vectors_mejora.f_params, vectors_e_s->f_params,MAXTYPES * sizeof(struct force_field_param_t));
    memcpy(vectors_mejora.weights, vectors_e_s->weights,SCORING_TERMS * sizeof(type_data));

    thrust::sequence(vectors_mejora.energy.n_conformation,vectors_mejora.energy.n_conformation + vectors_mejora.nconformations);
	
	fill_conformations_environment_flex (ligando.nlig,vectors_e_s->conformations_x,vectors_e_s->conformations_y,vectors_e_s->conformations_z,vectors_e_s->move_x,vectors_e_s->move_y,vectors_e_s->move_z,vectors_e_s->quat_x,vectors_e_s->quat_y,vectors_e_s->quat_z,vectors_e_s->quat_w,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,vectors_e_s->nconformations,param,metaheuristic);
	cudaSetDevice(devices->id[orden_device]);
	
	cudaDeviceReset();
    cudaMemGetInfo(&tam_max, &total_m);
    cudaDeviceSynchronize();
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
    //std::cout << tam_max << " " << total_m << "\n";
    tam_tmp = tam_max - (tam_max * 0.2);
	
	if (tam_tmp < max_memory)
        {
                //printf("Trocear en 1 GPU\n");
                max_memory = max_common;
                nconformations_count_total = 0;
                nconformations_count_partial = 0;
                stride_d = 0;
                while (nconformations_count_total < vectors_e_s->nconformations)
                {
                        max_memory += tam_conformation;
                        if (max_memory > tam_tmp)
                        {
                                //printf("Trozo %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
                                //nconformations_count_total--;
                                //nconformations_count_partial--;
                                //Lanzar con nconformations_count_partial;
								multigpu_mejorar_montecarlo_flex_environment (proteina,ligando,param,vectors_e_s->weights, vectors_e_s->f_params, devices,vectors_e_s->conformations_x + (stride_d*ligando.nlig),vectors_e_s->conformations_y+(stride_d*ligando.nlig),vectors_e_s->conformations_z+(stride_d*ligando.nlig),vectors_e_s->move_x+stride_d,vectors_e_s->move_y+stride_d,vectors_e_s->move_z+stride_d,vectors_e_s->quat_x+stride_d,vectors_e_s->quat_y+stride_d,vectors_e_s->quat_z+stride_d,vectors_e_s->quat_w+stride_d,vectors_e_s->energy.energy+stride_d,vectors_e_s->energy.n_conformation+stride_d,vectors_mejora.conformations_x + (stride_d * metaheuristic.NEEImp * ligando.nlig),vectors_mejora.conformations_y + (stride_d * metaheuristic.NEEImp * ligando.nlig) , vectors_mejora.conformations_z + (stride_d * metaheuristic.NEEImp * ligando.nlig) ,vectors_mejora.move_x + (stride_d * metaheuristic.NEEImp),vectors_mejora.move_y + (stride_d * metaheuristic.NEEImp),vectors_mejora.move_z + (stride_d * metaheuristic.NEEImp),vectors_mejora.quat_x + (stride_d * metaheuristic.NEEImp),vectors_mejora.quat_y + (stride_d * metaheuristic.NEEImp),vectors_mejora.quat_z + (stride_d * metaheuristic.NEEImp),vectors_mejora.quat_w + (stride_d * metaheuristic.NEEImp), vectors_mejora.energy.energy + (stride_d * metaheuristic.NEEImp), vectors_mejora.energy.n_conformation + (stride_d * metaheuristic.NEEImp),metaheuristic,nconformations_count_partial, nconformations_count_partial * metaheuristic.NEEImp, orden_device,stride_d,flexibility_params,fase);
								stride_d += nconformations_count_partial;
                                max_memory = max_common;
                                nconformations_count_partial = 0;
                        }
                        else
                        {
                                nconformations_count_total++;
                                nconformations_count_partial++;
                        }
                }
                //printf("Trozo final %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
				nconformations_count_partial--;
				multigpu_mejorar_montecarlo_flex_environment (proteina,ligando,param,vectors_e_s->weights, vectors_e_s->f_params, devices,vectors_e_s->conformations_x + (stride_d*ligando.nlig),vectors_e_s->conformations_y+(stride_d*ligando.nlig),vectors_e_s->conformations_z+(stride_d*ligando.nlig),vectors_e_s->move_x+stride_d,vectors_e_s->move_y+stride_d,vectors_e_s->move_z+stride_d,vectors_e_s->quat_x+stride_d,vectors_e_s->quat_y+stride_d,vectors_e_s->quat_z+stride_d,vectors_e_s->quat_w+stride_d,vectors_e_s->energy.energy+stride_d,vectors_e_s->energy.n_conformation+stride_d,vectors_mejora.conformations_x + (stride_d * metaheuristic.NEEImp * ligando.nlig),vectors_mejora.conformations_y + (stride_d * metaheuristic.NEEImp * ligando.nlig) , vectors_mejora.conformations_z + (stride_d * metaheuristic.NEEImp * ligando.nlig) ,vectors_mejora.move_x + (stride_d * metaheuristic.NEEImp),vectors_mejora.move_y + (stride_d * metaheuristic.NEEImp),vectors_mejora.move_z + (stride_d * metaheuristic.NEEImp),vectors_mejora.quat_x + (stride_d * metaheuristic.NEEImp),vectors_mejora.quat_y + (stride_d * metaheuristic.NEEImp),vectors_mejora.quat_z + (stride_d * metaheuristic.NEEImp),vectors_mejora.quat_w + (stride_d * metaheuristic.NEEImp), vectors_mejora.energy.energy + (stride_d * metaheuristic.NEEImp), vectors_mejora.energy.n_conformation + (stride_d * metaheuristic.NEEImp),metaheuristic,nconformations_count_partial, nconformations_count_partial * metaheuristic.NEEImp, orden_device,stride_d,flexibility_params,fase);
        }

        else
        {
			//printf("No Trocea M\n");
            //Lanzar con todo
			multigpu_mejorar_montecarlo_flex_environment (proteina,ligando,param,vectors_e_s->weights, vectors_e_s->f_params, devices,vectors_e_s->conformations_x,vectors_e_s->conformations_y,vectors_e_s->conformations_z,vectors_e_s->move_x,vectors_e_s->move_y,vectors_e_s->move_z,vectors_e_s->quat_x,vectors_e_s->quat_y,vectors_e_s->quat_z,vectors_e_s->quat_w,vectors_e_s->energy.energy,vectors_e_s->energy.n_conformation,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,vectors_mejora.energy.energy,vectors_mejora.energy.n_conformation,metaheuristic,vectors_e_s->nconformations, vectors_mejora.nconformations, orden_device,stride_d,flexibility_params,fase);
		}
	free(vectors_mejora.conformations_x);
	free(vectors_mejora.conformations_y);
	free(vectors_mejora.conformations_z);
	free(vectors_mejora.move_x);
        free(vectors_mejora.move_y);
        free(vectors_mejora.move_z);
        free(vectors_mejora.quat_x);
        free(vectors_mejora.quat_y);
        free(vectors_mejora.quat_z);
        free(vectors_mejora.quat_w);
        free(vectors_mejora.energy.energy);
        free(vectors_mejora.energy.n_conformation);
        free(vectors_mejora.weights);
}

extern void multigpu_mejorar_montecarlo_flex_environment (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, type_data *weights, struct force_field_param_t *f_params, struct device_t *devices, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *n_conformation, type_data *conformations_x_mejora, type_data *conformations_y_mejora, type_data *conformations_z_mejora, type_data *move_x_mejora, type_data *move_y_mejora, type_data *move_z_mejora, type_data *quat_x_mejora, type_data *quat_y_mejora, type_data *quat_z_mejora, type_data *quat_w_mejora, type_data *energy_mejora, unsigned int *n_conformation_mejora, struct metaheuristic_t metaheuristic, unsigned int nconformations, unsigned int nconformations_mejora, unsigned int orden_device, unsigned int stride_d, struct flexibility_params_t *flexibility_params, unsigned int fase)
{
	
		curandState_t *states_d_mejora;
        //struct receptor_t *proteina_d;
        //struct ligand_t *ligando_d;
        struct flexibility_data_t *flexibility_conformations_d, *flexibility_conformations_d_mejora;
        type_data *internal_energy, *internal_energy_d, *moves_x_d, *moves_y_d, *moves_z_d, *energy_d;
        type_data *quat_x_d, *quat_y_d, *quat_z_d, *quat_w_d, *quat_x_d_mejora, *quat_y_d_mejora, *quat_z_d_mejora, *quat_w_d_mejora;
        type_data *move_x_d_mejora, *move_y_d_mejora, *move_z_d_mejora, *energy_d_mejora, max_angle_flex;
        unsigned int *energy_nconformation_d, *energy_nconformation_d_mejora;
        int *links_d, *links_fragments_d, *fragments_d, *fragments_tam_d;
        type_data *conformations_x_d, *conformations_y_d, *conformations_z_d, *conformations_x_d_mejora, *conformations_y_d_mejora, *conformations_z_d_mejora;
        bool *individual_bonds_VDW_d, *individual_bonds_ES_d;

        type_data *rec_x_d, *rec_y_d, *rec_z_d, *qr_d;
        char *rectype_d, *ligtype_d;
        unsigned int *bondsr_d, *bonds_d;
        type_data *lig_x_d, *lig_y_d, *lig_z_d, *ql_d;

        unsigned int steps, moveType=MOVE, total, simulations, option;
        cudaError_t cudaStatus;

        unsigned int moveSize = nconformations * sizeof(type_data);
        unsigned int confSize = nconformations * ligando.nlig * sizeof(type_data);
        unsigned int quatSize = nconformations * sizeof(type_data);

        unsigned int moveSize_environment = nconformations * metaheuristic.NEEImp * sizeof(type_data);
        unsigned int confSize_environment = nconformations * metaheuristic.NEEImp * ligando.nlig * sizeof(type_data);
        unsigned int quatSize_environment = nconformations * metaheuristic.NEEImp * sizeof(type_data);

		cudaSetDevice(devices->id[orden_device]);
		cudaDeviceReset();
		//printf("devices->id[orden_device] %d %d\n",devices->id[orden_device],orden_device);
		dataToGPU_mejorar_montecarlo_environment(nconformations, energy_d, energy_nconformation_d, energy_d_mejora, energy_nconformation_d_mejora, moves_x_d, moves_y_d, moves_z_d, quat_x_d, quat_y_d, quat_z_d, quat_w_d, move_x_d_mejora, move_y_d_mejora, move_z_d_mejora, quat_x_d_mejora, quat_y_d_mejora, quat_z_d_mejora, quat_w_d_mejora, states_d_mejora, move_x, move_y, move_z, quat_x, quat_y, quat_z, quat_w, energy,n_conformation,move_x_mejora, move_y_mejora, move_z_mejora, quat_x_mejora, quat_y_mejora, quat_z_mejora, quat_w_mejora, energy_mejora,n_conformation_mejora,metaheuristic);
		dataToGPUInternalEnergy_environment(nconformations,nconformations_mejora,ligando.nlig,ligando.atoms, flexibility_conformations_d_mejora, conformations_x,conformations_y,conformations_z,conformations_x_d,conformations_y_d,conformations_z_d,conformations_x_mejora, conformations_y_mejora, conformations_z_mejora, conformations_x_d_mejora,conformations_y_d_mejora,conformations_z_d_mejora, flexibility_params->individual_bonds_VDW,flexibility_params->individual_bonds_ES,individual_bonds_VDW_d,individual_bonds_ES_d,internal_energy_d);
        dataToGPUFlexibilityParameters(nconformations,ligando.atoms,links_d,flexibility_params->links,links_fragments_d,flexibility_params->links_fragments,fragments_d,flexibility_params->fragments,fragments_tam_d,flexibility_params->fragments_tam,flexibility_conformations_d,flexibility_params->n_links,flexibility_params->n_fragments);
        cudaFree(flexibility_conformations_d);
        //commonDataToGPU (proteina, proteina_d, ligando, ligando_d);
        commonDataToGPU_WS (proteina,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,ligando,lig_x_d,lig_y_d,lig_z_d,ql_d,ligtype_d,bonds_d);
	cudaStatus = cudaMalloc((void**) &flexibility_conformations_d_mejora,sizeof(struct flexibility_data_t)*nconformations_mejora);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc flexibility_conformations_d");
	cudaDeviceSynchronize();
                // check if kernel execution generated and error
                cudaStatus = cudaGetLastError();
                if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");
	//exit(0);
        save_params(weights,f_params);
	//exit(0);
		
		 //Inicializa el generador de numeros aleatorios y el cuaternion de cada simulacion
		setupCurandState <<< (nconformations_mejora/devices->hilos[orden_device*metaheuristic.num_kernels+4])+1, devices->hilos[orden_device*metaheuristic.num_kernels+4] >>>  (states_d_mejora, param.automatic_seed,param.seed,nconformations_mejora);
		cudaDeviceSynchronize();
		// check if kernel execution generated and error
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");
		
		//exit(0);	
		if (fase == 0)
		{
			total = metaheuristic.IMEIni + metaheuristic.IMEFlex;
			simulations = metaheuristic.IMEIni;
		}
		if (fase == 1)	
		{
			total = metaheuristic.IMEImp + metaheuristic.IMEFlex;
			simulations = metaheuristic.IMEImp;
		}
		if (fase == 2)	
		{
			total = metaheuristic.IMEMUCom + metaheuristic.IMEFlex;
			simulations = metaheuristic.IMEImp;
		}
		steps = 0;	
		option = 0;
		max_angle_flex = param.flex_angle;
		//En cada paso de minimización
	
		cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeFourByte);
        	cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_flex_by_warp, cudaFuncCachePreferEqual);
        	cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_flex_by_warp_type1, cudaFuncCachePreferEqual);
        	cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_flex_by_warp_type2, cudaFuncCachePreferEqual);

        	cudaFuncSetCacheConfig(energy_internal_conformations, cudaFuncCachePreferEqual);
        	cudaFuncSetCacheConfig(energy_internal_conformations_type2, cudaFuncCachePreferEqual);
	
		unsigned int blk_f = ceil(nconformations_mejora / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
		unsigned int max_f = nconformations_mejora;			
		dim3 grid_f(ceil(blk_f/8)+1, 8);
		dim3 hilos_f(devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);
		
		unsigned int blk_t = ceil(nconformations_mejora / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
		dim3 grid_t  (ceil(blk_t/8)+1, 8);
		dim3 hilos_t (devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);
		unsigned int max_t = nconformations_mejora;	
				
		unsigned int size = 5 * devices->hilos[orden_device*metaheuristic.num_kernels] * sizeof(type_data) + sizeof(char)*devices->hilos[orden_device*metaheuristic.num_kernels];	
		unsigned int blk_e = ceil(nconformations_mejora*WARP_SIZE / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
		unsigned int max_e = nconformations_mejora* WARP_SIZE;	
		dim3 hilos_e (devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);
		dim3 grid_e  (ceil(blk_e/8)+1, 8);
		//printf("Aki\n");	
		
		while (steps < total){

			//printf("step %d\n", steps);
			//REFINAMIENTO DEL ANGULO FLEXIBLE, MOVIMIENTO Y ORIENTACIÓN DEL LIGANDO
			if (steps == simulations) max_angle_flex = param.flex_angle;
			if (((steps % 50) == 0) && (steps > 0) && (steps <simulations)) 
				max_angle_flex /= 1.2;
			
				
			if (steps >=simulations)
			{
				//FLEXIBILIDAD
				angulations_conformations <<< grid_f,hilos_f >>> (states_d_mejora, flexibility_conformations_d_mejora,flexibility_params->n_links,param.flex_angle,max_f);
				flexibilityKernel  <<< grid_f,hilos_f >>>(conformations_x_d_mejora,conformations_y_d_mejora,conformations_z_d_mejora,states_d_mejora,flexibility_conformations_d_mejora,ligando.nlig,flexibility_params->n_links,flexibility_params->n_fragments,links_d,links_fragments_d,fragments_d,fragments_tam_d,param.flex_angle,max_f);					
				max_angle_flex /= 1.1;
			}
			else
			{
				option = steps % 3;
				switch (option)
				{
					case 0:
						//MUEVE LAS CONFORMACIONES.
						move_mejora_montecarlo_environment <<< (nconformations_mejora / devices->hilos[orden_device*metaheuristic.num_kernels+5])+1, devices->hilos[orden_device*metaheuristic.num_kernels+5] >>> (states_d_mejora,param.max_desp,move_x_d_mejora,move_y_d_mejora,move_z_d_mejora,nconformations_mejora);								
						break;
					case 1:
						//ROTA LAS CONFORMACIONES.
						rotation_montecarlo_environment <<< (nconformations_mejora / devices->hilos[orden_device*metaheuristic.num_kernels+2])+1,devices->hilos[orden_device*metaheuristic.num_kernels+2] >>> (states_d_mejora,param.rotation,quat_x_d_mejora, quat_y_d_mejora, quat_z_d_mejora, quat_w_d_mejora,nconformations_mejora);				
						break;
					case 2:
						//FLEXIBILIDAD
						angulations_conformations <<< grid_f,hilos_f >>> (states_d_mejora, flexibility_conformations_d_mejora,flexibility_params->n_links,param.flex_angle,max_f);
						flexibilityKernel  <<< grid_f,hilos_f >>>(conformations_x_d_mejora,conformations_y_d_mejora,conformations_z_d_mejora,states_d_mejora,flexibility_conformations_d_mejora,ligando.nlig,flexibility_params->n_links,flexibility_params->n_fragments,links_d,links_fragments_d,fragments_d,fragments_tam_d,param.flex_angle,max_f);		
						break;
				}
			}
			
			// check if kernel execution generated and error
		cudaDeviceSynchronize();
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel move_mejora, rotation or flexibility Kernel execution failed \n");
		
		//Calcula la energía ligando-receptor
	        switch (param.scoring_function_type)
		{
			case 0:	
				Gpu_full_Kernel_Conformations_flex_by_warp <<<grid_e,hilos_e,size>>> (proteina.nrec,ligando.atoms,ligando.nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d_mejora,conformations_y_d_mejora,conformations_z_d_mejora,move_x_d_mejora, move_y_d_mejora, move_z_d_mejora,quat_x_d_mejora,quat_y_d_mejora,quat_z_d_mejora,quat_w_d_mejora,ql_d,ligtype_d,bonds_d,energy_d_mejora,energy_nconformation_d_mejora,devices->stride[orden_device]*metaheuristic.NEEImp,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
				break;
			case 1:
				Gpu_full_Kernel_Conformations_flex_by_warp_type1 <<<grid_e,hilos_e,size>>> (flexibility_params->n_links,proteina.nrec,ligando.atoms,ligando.nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d_mejora,conformations_y_d_mejora,conformations_z_d_mejora,move_x_d_mejora, move_y_d_mejora, move_z_d_mejora,quat_x_d_mejora,quat_y_d_mejora,quat_z_d_mejora,quat_w_d_mejora,ql_d,ligtype_d,bonds_d,energy_d_mejora,energy_nconformation_d_mejora,devices->stride[orden_device]*metaheuristic.NEEImp,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
                                break;
			case 2:
				Gpu_full_Kernel_Conformations_flex_by_warp_type2 <<<grid_e,hilos_e,size>>> (flexibility_params->n_links,proteina.nrec,ligando.atoms,ligando.nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d_mejora,conformations_y_d_mejora,conformations_z_d_mejora,move_x_d_mejora, move_y_d_mejora, move_z_d_mejora,quat_x_d_mejora,quat_y_d_mejora,quat_z_d_mejora,quat_w_d_mejora,ql_d,ligtype_d,bonds_d,energy_d_mejora,energy_nconformation_d_mejora,devices->stride[orden_device]*metaheuristic.NEEImp,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
                                break;
		}

		cudaDeviceSynchronize();
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel energy failed!");

		switch (param.scoring_function_type)
                {
                        case 0:
			case 1:
				energy_internal_conformations<<<grid_e,hilos_e,size>>>(ql_d,ligtype_d,conformations_x_d_mejora,conformations_y_d_mejora,conformations_z_d_mejora,ligando.atoms,ligando.nlig,internal_energy_d,energy_nconformation_d_mejora,stride_d*metaheuristic.NEEImp,individual_bonds_VDW_d,individual_bonds_ES_d,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
				break;
			case 2:
				energy_internal_conformations_type2<<<grid_e,hilos_e,size>>>(ql_d,ligtype_d,conformations_x_d_mejora,conformations_y_d_mejora,conformations_z_d_mejora,ligando.atoms,ligando.nlig,internal_energy_d,energy_nconformation_d_mejora,stride_d*metaheuristic.NEEImp,individual_bonds_VDW_d,individual_bonds_ES_d,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
				break;
		}

		cudaDeviceSynchronize();				
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) fprintf(stderr, "KERNEL FAILED INTERNAL ENERGY!");
	
		//for (int j=0;j<nconformations;j++) printf("conf %d valor internal_energy %f\n",j,internal_energy[j]);	
		energy_total <<<grid_t,hilos_t>>>(energy_d_mejora,internal_energy_d,max_t);
		cudaDeviceSynchronize();				
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) fprintf(stderr, "KERNEL FAILED TOTAL ENERGY!");
		
		//HASTA AKI REVISADO
		if (steps >=simulations)
		{
			//FLEXIBILIDAD
			update_angulations_conformations_environment <<< grid_f,hilos_f >>> (metaheuristic.NEEImp, states_d_mejora, flexibility_conformations_d_mejora, energy_d_mejora, energy_d,max_f);
			flexibilityKernel  <<< grid_f,hilos_f >>>(conformations_x_d_mejora,conformations_y_d_mejora,conformations_z_d_mejora,states_d_mejora,flexibility_conformations_d_mejora,ligando.nlig,flexibility_params->n_links,flexibility_params->n_fragments,links_d,links_fragments_d,fragments_d,fragments_tam_d,param.flex_angle,max_f);					
		}
		else
		{
			switch (option)
			{
			case 0:
				updateMove_environment <<< (nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+5])+1, devices->hilos[orden_device*metaheuristic.num_kernels+5]  >>> (states_d_mejora, param.max_desp, moves_x_d, moves_y_d, moves_z_d, move_x_d_mejora, move_y_d_mejora, move_z_d_mejora, energy_d_mejora, energy_d, metaheuristic.NEEImp, nconformations);
				break;
			case 1:
				updateRotation_environment <<< (nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+2])+1,devices->hilos[orden_device*metaheuristic.num_kernels+2] >>> (states_d_mejora, param.rotation, quat_x_d, quat_y_d, quat_z_d, quat_w_d, quat_x_d_mejora, quat_y_d_mejora, quat_z_d_mejora, quat_w_d_mejora, energy_d_mejora, energy_d, metaheuristic.NEEImp, nconformations);
				break;
			case 2:
				update_angulations_conformations_environment <<< grid_f,hilos_f >>> (metaheuristic.NEEImp, states_d_mejora, flexibility_conformations_d_mejora, energy_d_mejora, energy_d,max_f);
				flexibilityKernel  <<< grid_f,hilos_f >>>(conformations_x_d_mejora,conformations_y_d_mejora,conformations_z_d_mejora,states_d_mejora,flexibility_conformations_d_mejora,ligando.nlig,flexibility_params->n_links,flexibility_params->n_fragments,links_d,links_fragments_d,fragments_d,fragments_tam_d,param.flex_angle,max_f);					
				break;
			}
		}
		
		// check if kernel execution generated and error
		cudaDeviceSynchronize();
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel updateMoveKernel o updateRotKernel execution failed \n");
		
		steps++;
		incluir_mejorar_environment_flex <<<(nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+6])+1, devices->hilos[orden_device*metaheuristic.num_kernels+6]>>> (metaheuristic.NEEImp, ligando.nlig, conformations_x_d_mejora, conformations_y_d_mejora,conformations_z_d_mejora,move_x_d_mejora,move_y_d_mejora,move_z_d_mejora,quat_x_d_mejora,quat_y_d_mejora,quat_z_d_mejora,quat_w_d_mejora,energy_d_mejora,energy_nconformation_d,conformations_x_d,conformations_y_d,conformations_z_d,moves_x_d,moves_y_d,moves_z_d,quat_x_d,quat_y_d,quat_z_d,quat_w_d,energy_d,stride_d,nconformations);
                //fill_conformations_mejora_flex <<<(nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+6])+1, devices->hilos[orden_device*metaheuristic.num_kernels+6]>>> (metaheuristic.NEEImp,ligando.nlig,conformations_x_d_mejora, conformations_y_d_mejora, conformations_z_d_mejora, move_x_d_mejora,move_y_d_mejora,move_z_d_mejora,quat_x_d_mejora,quat_y_d_mejora,quat_z_d_mejora,quat_w_d_mejora,energy_d_mejora,conformations_x_d,conformations_y_d,conformations_z_d,moves_x_d,moves_y_d,moves_z_d,quat_x_d,quat_y_d,quat_z_d,quat_w_d,energy_d,nconformations);
	}
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernels antes de copia fallo!\n");
	
	cudaStatus = cudaMemcpy(conformations_x, conformations_x_d, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values conformations_x_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(conformations_y, conformations_y_d, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values conformations_y_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(conformations_z, conformations_z_d, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values conformations_z_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(move_x, moves_x_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_x_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(move_y, moves_y_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_y_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(move_z, moves_z_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_z_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(energy, energy_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values energy_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_x, quat_x_d, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_y, quat_y_d, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_z, quat_z_d, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_w, quat_w_d, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d GPUtoHost failed!");
	//for (int k=0;k<10;k++)printf("Energy %f\n",energy[k]);	

	cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) fprintf(stderr, "Despues de copia fallo!\n");	
	/*cudaFree(states_d_mejora);
	cudaFree(moves_x_d);
	cudaFree(moves_y_d);
	cudaFree(moves_z_d);
	cudaFree(quat_x_d);
	cudaFree(quat_y_d);
	cudaFree(quat_z_d);
	cudaFree(quat_w_d);
	cudaFree(rec_x_d);
	cudaFree(rec_y_d);
	cudaFree(rec_z_d);
	cudaFree(qr_d);
	cudaFree(bondsr_d);
	cudaFree(rectype_d);
	cudaFree(lig_x_d);
	cudaFree(lig_y_d);
	cudaFree(lig_z_d);
	cudaFree(ql_d);
	cudaFree(ligtype_d);
	cudaFree(bonds_d);
	cudaFree(energy_d);
	cudaFree(energy_d_mejora);
	cudaFree(energy_nconformation_d);
	cudaFree(move_x_d_mejora);
	cudaFree(move_y_d_mejora);
	cudaFree(move_z_d_mejora);
	cudaFree(quat_x_d_mejora);
	cudaFree(quat_y_d_mejora);
	cudaFree(quat_z_d_mejora);
	cudaFree(quat_w_d_mejora);
	
	cudaFree(internal_energy_d);
	cudaFree(conformations_x_d);
	cudaFree(conformations_y_d);
	cudaFree(conformations_z_d);
	
	cudaFree(conformations_x_d_mejora);
	cudaFree(conformations_y_d_mejora);
	cudaFree(conformations_z_d_mejora);
	cudaFree(individual_bonds_VDW_d);
	cudaFree(individual_bonds_ES_d);
	
	cudaFree(flexibility_conformations_d_mejora);
	cudaFree(links_d);
	cudaFree(links_fragments_d);
	cudaFree(fragments_d);
	cudaFree(fragments_tam_d);*/
	cudaDeviceReset();	
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)	printf("Error al finalizar montecarlo %d\n",cudaStatus);
	printf("Fin \n");
	//exit(0);
		
}
extern void montecarlo_flex  (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, struct vectors_t *vectors_e_s, struct metaheuristic_t metaheuristic, struct device_t *devices, struct flexibility_params_t *flexibility_params,unsigned int orden_device, unsigned int fase){

	size_t available,total_m;
    cudaError_t cudaStatus;
    unsigned long int max_memory;
    unsigned long int max_positions;
    unsigned long int max_common;
	unsigned long int max_conf;
    unsigned long int typeSize = proteina.nrec * sizeof(char);
    unsigned long int recSize = proteina.nrec * sizeof(type_data);
    unsigned long int bondSize = proteina.nrec * sizeof(int) * MAXBOND;
	
	unsigned long int ligTypeSize = ligando.nlig * sizeof(char);
    unsigned long int ligSize = ligando.nlig * sizeof(type_data);
    unsigned long int ligBondSize = ligando.nlig * sizeof(int) * MAXBOND;
	
	unsigned long int confSize_tam = vectors_e_s->nconformations * ligando.nlig * sizeof(type_data);
	unsigned long int linksSize = flexibility_params->n_links * 2 * sizeof(int);
    unsigned long int linksfragmentsSize = flexibility_params->n_links * 2 * sizeof(int);
    unsigned long int fragmentstamSize = flexibility_params->n_fragments * sizeof(int);
    unsigned long int fragmentsSize = flexibility_params->n_links * (ligando.atoms - 2) * sizeof(int);
	unsigned long int boolSize = ligando.atoms * 2 * sizeof(bool);
    
	unsigned long int flexConfSize =  vectors_e_s->nconformations * sizeof(struct flexibility_data_t);
    unsigned long int randSize = vectors_e_s->nconformations * sizeof(curandState_t);
    unsigned long int moveSize_tam = vectors_e_s->nconformations * sizeof(type_data);
    unsigned long int energyConf_tam = vectors_e_s->nconformations * sizeof(int);
    	
	
    unsigned long int tam_max, tam_tmp, nconformations_count_total, nconformations_count_partial, tam_conformation, stride_d;

    max_positions = (moveSize_tam * 8) * 2 + moveSize_tam + energyConf_tam;
	max_conf = (3 * confSize_tam);
    max_common = (recSize * 4) + typeSize + bondSize + (ligSize * 4) + ligTypeSize + ligBondSize + (boolSize * 2) + flexConfSize + linksSize + linksfragmentsSize + fragmentstamSize + fragmentsSize;;

    max_memory = max_positions + max_conf + max_common;
    tam_conformation =  (sizeof(type_data) * 8 * 2) + sizeof(type_data) + sizeof(int) + sizeof(curandState_t) + (3 * ligando.nlig * sizeof(type_data));

	cudaSetDevice(devices->id[orden_device]);
    //cudaDeviceReset();
    cudaMemGetInfo(&tam_max, &total_m);
    cudaDeviceSynchronize();
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
    //std::cout << tam_max << " " << total_m << "\n";
    tam_tmp = tam_max - (tam_max * 0.2);
	
	if (tam_tmp < max_memory)
        {
                //printf("Trocear en 1 GPU\n");
                max_memory = max_common;
                nconformations_count_total = 0;
                nconformations_count_partial = 0;
                stride_d = 0;
                while (nconformations_count_total < vectors_e_s->nconformations)
                {
                        max_memory += tam_conformation;
                        if (max_memory > tam_tmp)
                        {
                                //printf("Trozo %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
                                //nconformations_count_total--;
                                //nconformations_count_partial--;
                                //Lanzar con nconformations_count_partial;
				multigpu_mejorar_montecarlo_flex (proteina,ligando,param,vectors_e_s->weights,vectors_e_s->f_params,devices,vectors_e_s->conformations_x+(stride_d*ligando.nlig),vectors_e_s->conformations_y+(stride_d*ligando.nlig),vectors_e_s->conformations_z+(stride_d*ligando.nlig),vectors_e_s->move_x+stride_d,vectors_e_s->move_y+stride_d,vectors_e_s->move_z+stride_d,vectors_e_s->quat_x+stride_d,vectors_e_s->quat_y+stride_d,vectors_e_s->quat_z+stride_d,vectors_e_s->quat_w+stride_d,vectors_e_s->energy.energy+stride_d,vectors_e_s->energy.n_conformation+stride_d,metaheuristic,flexibility_params,nconformations_count_partial,orden_device,stride_d,fase);

                                stride_d += nconformations_count_partial;
                                max_memory = max_common;
                                nconformations_count_partial = 0;
                        }
                        else
                        {
                                nconformations_count_total++;
                                nconformations_count_partial++;
                        }
                }
                //printf("Trozo final %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
		nconformations_count_partial--;
		multigpu_mejorar_montecarlo_flex (proteina,ligando,param,vectors_e_s->weights,vectors_e_s->f_params,devices,vectors_e_s->conformations_x+(stride_d*ligando.nlig),vectors_e_s->conformations_y+(stride_d*ligando.nlig),vectors_e_s->conformations_z+(stride_d*ligando.nlig),vectors_e_s->move_x+stride_d,vectors_e_s->move_y+stride_d,vectors_e_s->move_z+stride_d,vectors_e_s->quat_x+stride_d,vectors_e_s->quat_y+stride_d,vectors_e_s->quat_z+stride_d,vectors_e_s->quat_w+stride_d,vectors_e_s->energy.energy+stride_d,vectors_e_s->energy.n_conformation+stride_d,metaheuristic,flexibility_params,nconformations_count_partial,orden_device,stride_d,fase);
        }

        else
        {
                //printf("No Trocea M\n");
                //Lanzar con todo
		multigpu_mejorar_montecarlo_flex (proteina,ligando,param,vectors_e_s->weights,vectors_e_s->f_params,devices,vectors_e_s->conformations_x,vectors_e_s->conformations_y,vectors_e_s->conformations_z,vectors_e_s->move_x,vectors_e_s->move_y,vectors_e_s->move_z,vectors_e_s->quat_x,vectors_e_s->quat_y,vectors_e_s->quat_z,vectors_e_s->quat_w,vectors_e_s->energy.energy,vectors_e_s->energy.n_conformation,metaheuristic,flexibility_params,vectors_e_s->nconformations,orden_device,0,fase);
        }
}

extern void multigpu_mejorar_montecarlo_flex (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, type_data *weights, struct force_field_param_t *f_params, struct device_t *devices, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *n_conformation, struct metaheuristic_t metaheuristic, struct flexibility_params_t *flexibility_params, unsigned int nconformations, unsigned int orden_device, unsigned int stride_d, unsigned int fase)
{
	curandState_t *states_d;
	//struct receptor_t *proteina_d;
	//struct ligand_t *ligando_d;
	struct flexibility_data_t *flexibility_conformations_d;
	type_data *internal_energy, *internal_energy_d, *moves_x_d, *moves_y_d, *moves_z_d, *energy_d;		
	type_data *quat_d_x, *quat_d_y, *quat_d_z, *quat_d_w, *quat_x_m, *quat_y_m, *quat_z_m, *quat_w_m;
	type_data *move_x_m, *move_y_m, *move_z_m, *energy_d_mejora, max_angle_flex;
	unsigned int *energy_nconformation_d, *energy_nconformation_d_mejora;
	
	int *links_d, *links_fragments_d, *fragments_d, *fragments_tam_d;
	type_data *conformations_x_d, *conformations_y_d, *conformations_z_d;
	bool *individual_bonds_VDW_d, *individual_bonds_ES_d;	
	
	type_data *rec_x_d, *rec_y_d, *rec_z_d, *qr_d;
	char *rectype_d, *ligtype_d;
	unsigned int *bondsr_d, *bonds_d;
	type_data *lig_x_d, *lig_y_d, *lig_z_d, *ql_d;
	
	unsigned int steps, total, option,simulations;
	cudaError_t cudaStatus;
	unsigned int moveSize = nconformations * sizeof(type_data);
	unsigned int confSize = nconformations * ligando.nlig * sizeof(type_data);
	unsigned int quatSize = nconformations * sizeof(type_data);
	
	cudaSetDevice(devices->id[orden_device]);	
	cudaDeviceReset();

	//for (int k=0;k<2;k++) printf("%d x %f y %f z %f w %f\n",k,quat_x[k],quat_y[k],quat_z[k],quat_w[k]);	
	dataToGPU_mejorar_montecarlo(nconformations, energy_d, energy_nconformation_d, energy_d_mejora, moves_x_d, moves_y_d, moves_z_d, quat_d_x, quat_d_y, quat_d_z, quat_d_w, states_d, move_x, move_y, move_z, quat_x, quat_y, quat_z, quat_w, energy, n_conformation,metaheuristic);
	dataToGPUInternalEnergy(nconformations,ligando.nlig,ligando.atoms,conformations_x,conformations_y,conformations_z,conformations_x_d,conformations_y_d,conformations_z_d,flexibility_params->individual_bonds_VDW,flexibility_params->individual_bonds_ES,individual_bonds_VDW_d,individual_bonds_ES_d,internal_energy_d);
	dataToGPUFlexibilityParameters(nconformations,ligando.atoms,links_d,flexibility_params->links,links_fragments_d,flexibility_params->links_fragments,fragments_d,flexibility_params->fragments,fragments_tam_d,flexibility_params->fragments_tam,flexibility_conformations_d,flexibility_params->n_links,flexibility_params->n_fragments);
	//commonDataToGPU (proteina, proteina_d, ligando, ligando_d);
	commonDataToGPU_WS (proteina,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,ligando,lig_x_d,lig_y_d,lig_z_d,ql_d,ligtype_d,bonds_d);
	HostToGPU_improve (nconformations,move_x_m,move_y_m,move_z_m,quat_x_m,quat_y_m,quat_z_m,quat_w_m,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w);
	save_params(weights,f_params);
	 //Inicializa el generador de numeros aleatorios y el cuaternion de cada simulacion
	setupCurandState <<< (nconformations/devices->hilos[orden_device*metaheuristic.num_kernels+4])+1, devices->hilos[orden_device*metaheuristic.num_kernels+4] >>>  (states_d, param.automatic_seed,param.seed,nconformations);
	cudaDeviceSynchronize();
	// check if kernel execution generated and error
	cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");
	
	///exit(0);	
	if (fase == 0)
	{
		total = metaheuristic.IMEIni + metaheuristic.IMEFlex;
		simulations = metaheuristic.IMEIni;
	}
	if (fase == 1)	
	{
		total = metaheuristic.IMEImp + metaheuristic.IMEFlex;
		simulations = metaheuristic.IMEImp;
	}
	if (fase == 2)	
	{
		total = metaheuristic.IMEMUCom + metaheuristic.IMEFlex;
		simulations = metaheuristic.IMEImp;
	}
	steps = 0;	
	option = 0;
	max_angle_flex = param.flex_angle;
	//En cada paso de minimización
	
	unsigned int blk_f = ceil(nconformations / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
	unsigned int max_f = nconformations;			
	dim3 grid_f(ceil(blk_f/8)+1, 8);
	dim3 hilos_f(devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);
	
	unsigned int blk_t = ceil(nconformations / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
	dim3 grid_t  (ceil(blk_t/8)+1, 8);
	dim3 hilos_t (devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);
	unsigned int max_t = nconformations;	
			
	unsigned int size = 5 * devices->hilos[orden_device*metaheuristic.num_kernels] * sizeof(type_data) + sizeof(char)*devices->hilos[orden_device*metaheuristic.num_kernels];	
	unsigned int blk_e = ceil(nconformations*WARP_SIZE / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
	unsigned int max_e = nconformations* WARP_SIZE;	
	dim3 hilos_e (devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);
	dim3 grid_e  (ceil(blk_e/8)+1, 8);
	//printf("Aki\n");	

	cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeFourByte);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_flex_by_warp, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_flex_by_warp_type1, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_flex_by_warp_type2, cudaFuncCachePreferEqual);

        cudaFuncSetCacheConfig(energy_internal_conformations, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(energy_internal_conformations_type2, cudaFuncCachePreferEqual);

	while (steps < total){

		//printf("step %d\n", steps);
		//REFINAMIENTO DEL ANGULO FLEXIBLE, MOVIMIENTO Y ORIENTACIÓN DEL LIGANDO
		if (steps == simulations) max_angle_flex = param.flex_angle;
		if (((steps % 50) == 0) && (steps > 0) && (steps <simulations)) 
			max_angle_flex /= 1.2;
		
			
		if (steps >=simulations)
		{
			//FLEXIBILIDAD
			angulations_conformations <<< grid_f,hilos_f >>> (states_d, flexibility_conformations_d,flexibility_params->n_links,param.flex_angle,max_f);
			flexibilityKernel  <<< grid_f,hilos_f >>>(conformations_x_d,conformations_y_d,conformations_z_d,states_d,flexibility_conformations_d,ligando.nlig,flexibility_params->n_links,flexibility_params->n_fragments,links_d,links_fragments_d,fragments_d,fragments_tam_d,param.flex_angle,max_f);					
			max_angle_flex /= 1.1;
		}
		else
		{
			option = steps % 3;
			switch (option)
			{
			case 0:
				//MUEVE LAS CONFORMACIONES.
				move_mejora_montecarlo <<< (nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+5])+1, devices->hilos[orden_device*metaheuristic.num_kernels+5] >>> (states_d,param.max_desp,moves_x_d,moves_y_d,moves_z_d,move_x_m, move_y_m, move_z_m,nconformations);				
				break;
			case 1:
				//ROTA LAS CONFORMACIONES.
				rotation_montecarlo <<< (nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+2])+1,devices->hilos[orden_device*metaheuristic.num_kernels+2] >>> (states_d,param.rotation,quat_d_x,quat_d_y,quat_d_z,quat_d_w,quat_x_m,quat_y_m,quat_z_m,quat_w_m,nconformations);				
				break;
			case 2:
				//FLEXIBILIDAD
				angulations_conformations <<< grid_f,hilos_f >>> (states_d, flexibility_conformations_d,flexibility_params->n_links,param.flex_angle,max_f);
				flexibilityKernel  <<< grid_f,hilos_f >>>(conformations_x_d,conformations_y_d,conformations_z_d,states_d,flexibility_conformations_d,ligando.nlig,flexibility_params->n_links,flexibility_params->n_fragments,links_d,links_fragments_d,fragments_d,fragments_tam_d,param.flex_angle,max_f);		
				break;
			}
		}
		
		// check if kernel execution generated and error
		cudaDeviceSynchronize();
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel move_mejora, rotation or flexibility Kernel execution failed \n");
		
		//Calcula la energía ligando-receptor
	        switch (param.scoring_function_type)
		{
			case 0:
				Gpu_full_Kernel_Conformations_flex_by_warp <<<grid_e,hilos_e,size>>> (proteina.nrec,ligando.atoms,ligando.nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d,conformations_y_d,conformations_z_d,move_x_m, move_y_m, move_z_m,quat_x_m,quat_y_m,quat_z_m,quat_w_m,ql_d,ligtype_d,bonds_d,energy_d_mejora,energy_nconformation_d,devices->stride[orden_device],devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
				break;
			case 1:
				Gpu_full_Kernel_Conformations_flex_by_warp_type1 <<<grid_e,hilos_e,size>>> (flexibility_params->n_links,proteina.nrec,ligando.atoms,ligando.nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d,conformations_y_d,conformations_z_d,move_x_m, move_y_m, move_z_m,quat_x_m,quat_y_m,quat_z_m,quat_w_m,ql_d,ligtype_d,bonds_d,energy_d_mejora,energy_nconformation_d,devices->stride[orden_device],devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
                                break;
			case 2:
				 Gpu_full_Kernel_Conformations_flex_by_warp_type2 <<<grid_e,hilos_e,size>>> (flexibility_params->n_links,proteina.nrec,ligando.atoms,ligando.nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d,conformations_y_d,conformations_z_d,move_x_m, move_y_m, move_z_m,quat_x_m,quat_y_m,quat_z_m,quat_w_m,ql_d,ligtype_d,bonds_d,energy_d_mejora,energy_nconformation_d,devices->stride[orden_device],devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
                                break;
		}

		cudaDeviceSynchronize();
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel energy failed!");
		
		switch (param.scoring_function_type)
		{
			case 0:
			case 1:
				energy_internal_conformations<<<grid_e,hilos_e,size>>>(ql_d,ligtype_d,conformations_x_d,conformations_y_d,conformations_z_d,ligando.atoms,ligando.nlig,internal_energy_d,energy_nconformation_d,stride_d,individual_bonds_VDW_d,individual_bonds_ES_d,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
				break;
			case 2:
				energy_internal_conformations_type2<<<grid_e,hilos_e,size>>>(ql_d,ligtype_d,conformations_x_d,conformations_y_d,conformations_z_d,ligando.atoms,ligando.nlig,internal_energy_d,energy_nconformation_d,stride_d,individual_bonds_VDW_d,individual_bonds_ES_d,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
                                break;
		}

		cudaDeviceSynchronize();				
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) fprintf(stderr, "KERNEL FAILED INTERNAL ENERGY!");
	
		//for (int j=0;j<nconformations;j++) printf("conf %d valor internal_energy %f\n",j,internal_energy[j]);	
		energy_total <<<grid_t,hilos_t>>>(energy_d_mejora,internal_energy_d,max_t);
		cudaDeviceSynchronize();				
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) fprintf(stderr, "KERNEL FAILED TOTAL ENERGY!");
		
		//HASTA AKI REVISADO
		if (steps >=simulations)
		{
			//FLEXIBILIDAD
			update_angulations_conformations <<< grid_f,hilos_f >>> (states_d, flexibility_conformations_d, energy_d_mejora, energy_d,max_f);
			flexibilityKernel  <<< grid_f,hilos_f >>>(conformations_x_d,conformations_y_d,conformations_z_d,states_d,flexibility_conformations_d,ligando.nlig,flexibility_params->n_links,flexibility_params->n_fragments,links_d,links_fragments_d,fragments_d,fragments_tam_d,param.flex_angle,max_f);					
		}
		else
		{
			switch (option)
			{
			case 0:
				updateMove <<< (nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+5])+1, devices->hilos[orden_device*metaheuristic.num_kernels+5]  >>> (states_d, param.max_desp, moves_x_d, moves_y_d, moves_z_d, move_x_m, move_y_m, move_z_m, energy_d_mejora, energy_d,nconformations);
				break;
			case 1:
				updateRotation <<< (nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+2])+1,devices->hilos[orden_device*metaheuristic.num_kernels+2] >>> (states_d, param.rotation, quat_d_x, quat_d_y, quat_d_z, quat_d_w, quat_x_m, quat_y_m, quat_z_m, quat_w_m, energy_d_mejora, energy_d,nconformations);
				break;
			case 2:
				update_angulations_conformations <<< grid_f,hilos_f >>> (states_d, flexibility_conformations_d, energy_d_mejora, energy_d,max_f);
				flexibilityKernel  <<< grid_f,hilos_f >>>(conformations_x_d,conformations_y_d,conformations_z_d,states_d,flexibility_conformations_d,ligando.nlig,flexibility_params->n_links,flexibility_params->n_fragments,links_d,links_fragments_d,fragments_d,fragments_tam_d,param.flex_angle,max_f);					
				break;
			}
		}
		
		// check if kernel execution generated and error
		cudaDeviceSynchronize();
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel updateMoveKernel o updateRotKernel execution failed \n");
		
		steps++;
	}
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernels antes de copia fallo!\n");
	
	cudaStatus = cudaMemcpy(conformations_x, conformations_x_d, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values conformations_x_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(conformations_y, conformations_y_d, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values conformations_y_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(conformations_z, conformations_z_d, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values conformations_z_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(move_x, moves_x_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_x_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(move_y, moves_y_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_y_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(move_z, moves_z_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_z_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(energy, energy_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values energy_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_x, quat_d_x, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_y, quat_d_y, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_z, quat_d_z, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_w, quat_d_w, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d GPUtoHost failed!");
	//for (int k=0;k<10;k++)printf("Energy %f\n",energy[k]);	
	cudaFree(states_d);
	cudaFree(moves_x_d);
	cudaFree(moves_y_d);
	cudaFree(moves_z_d);
	cudaFree(quat_d_x);
	cudaFree(quat_d_y);
	cudaFree(quat_d_z);
	cudaFree(quat_d_w);
	cudaFree(rec_x_d);
	cudaFree(rec_y_d);
	cudaFree(rec_z_d);
	cudaFree(qr_d);
	cudaFree(bondsr_d);
	cudaFree(rectype_d);
	cudaFree(lig_x_d);
	cudaFree(lig_y_d);
	cudaFree(lig_z_d);
	cudaFree(ql_d);
	cudaFree(ligtype_d);
	cudaFree(bonds_d);
	cudaFree(energy_d);
	cudaFree(energy_d_mejora);
	cudaFree(energy_nconformation_d);
	cudaFree(move_x_m);
	cudaFree(move_y_m);
	cudaFree(move_z_m);
	cudaFree(quat_x_m);
	cudaFree(quat_y_m);
	cudaFree(quat_z_m);
	cudaFree(quat_w_m);
	
	cudaFree(internal_energy_d);
	cudaFree(conformations_x_d);
	cudaFree(conformations_y_d);
	cudaFree(conformations_z_d);
	cudaFree(individual_bonds_VDW_d);
	cudaFree(individual_bonds_ES_d);
	
	cudaFree(flexibility_conformations_d);
	cudaFree(links_d);
	cudaFree(links_fragments_d);
	cudaFree(fragments_d);
	cudaFree(fragments_tam_d);
	
	cudaDeviceReset();
	
}
