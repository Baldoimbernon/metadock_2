#ifndef FLEXIBILITY_H
#define FLEXIBILITY_H

#include "energy_struct.h"

extern void rotate3D_flexibility_cpp (type_data  q_x, type_data  q_y, type_data  q_z, type_data  q_w, type_data* src, type_data * dst);
extern void setRotation_flexibility_cpp (type_data *q_x, type_data *q_y, type_data *q_z, type_data *q_w, type_data angle, type_data * vector);
extern void rotateLigandFragment_cpp (int *f, unsigned int size_f, type_data* lig_x, type_data* lig_y, type_data* lig_z, type_data qeje_x, type_data qeje_y, type_data qeje_z, type_data qeje_w, type_data p1[3]);
extern void flexibility_cpp (unsigned int nconformations, struct metaheuristic_t metaheuristic, struct param_t param, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, struct flexibility_data_t * flexibility_conformations, unsigned int nlig, unsigned int n_links, unsigned int n_fragments, int *links, int *links_fragments, int *fragments, int *fragments_tam, unsigned int max_angle_flex);
extern void angulations_conformations_cpp (unsigned int nconformations, struct metaheuristic_t metaheuristic, struct param_t param, struct flexibility_data_t * flexibility_conformations, unsigned int n_links, type_data max_angle_flex);

extern void update_angulations_conformations_cpp (unsigned int nconformations, struct metaheuristic_t metaheuristic, struct param_t param, struct flexibility_data_t * flexibility_conformations, type_data * energy_sig, type_data* energy_ant);
extern void update_angulations_conformations_cpp_environment (unsigned int nconformations, struct metaheuristic_t metaheuristic, struct param_t param, struct flexibility_data_t * flexibility_conformations, type_data * energy_sig, type_data* energy_ant);

extern void update_angulations_conformations_tradicional_cpp (unsigned int nconformations, struct metaheuristic_t metaheuristic, struct param_t param, struct flexibility_data_t * flexibility_conformations, type_data * energy_sig, type_data* energy_ant);
extern void update_angulations_conformations_tradicional_cpp (unsigned int nconformations, struct metaheuristic_t metaheuristic, struct param_t param, struct flexibility_data_t * flexibility_conformations, type_data * energy_sig, type_data* energy_ant);

extern void flexibility_function_cpp (struct ligand_t ligando, struct param_t param, struct metaheuristic_t metaheuristic, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, struct flexibility_data_t * flexibility_conformations,struct flexibility_params_t *flexibility_params, unsigned int nconformations);
#endif
