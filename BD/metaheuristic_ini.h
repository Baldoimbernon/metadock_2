#ifndef __METAHEU_INI_H__
#define __METAHEU_INI_H__

extern void inicializar (struct receptor_t proteina, struct ligand_t ligando, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic,struct device_t *devices, type_data **dqn_data);
extern void seleccionar_mejorar (struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic);
extern void seleccionar_inicializar (struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic);
extern void seleccionar_inicializar_multigpu (struct vectors_t *vectors_e, struct vectors_t * vectors_s, struct param_t param, struct metaheuristic_t metaheuristic);
extern void incluir_mejorar_inicializar (struct vectors_t vectors_e,  type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int nconformations, struct metaheuristic_t metaheuristic, struct param_t param);

extern void incluir_mejorar_inicializar_environment (struct vectors_t vectors_e,  type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int nconformations, struct metaheuristic_t metaheuristic, struct param_t param);
//extern void incluir_gpu (struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic);
//extern void incluir_gpu_mejorar (struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic);
extern void seleccionar_NEFIni (struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param);
extern void seleccionar_mutar (struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic);
//Mejorar
extern void mejorar (struct receptor_t proteina, struct ligand_t ligando, struct param_t param,  unsigned int num_surface, type_data *weights, struct force_field_param_t *f_params, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int nconformations, struct metaheuristic_t metaheuristic, unsigned int opt);

extern void mejorar_environment (struct receptor_t proteina, struct ligand_t ligando, struct param_t param,  unsigned int num_surface, type_data *weights, struct force_field_param_t *f_params, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int nconformations, struct metaheuristic_t metaheuristic, unsigned int opt);

extern void incluir_gpu_by_step (unsigned int nlig, type_data *e_move_x, type_data *e_move_y, type_data *e_move_z, type_data *e_quat_x, type_data *e_quat_y, type_data *e_quat_z, type_data *e_quat_w, type_data *e_energy, unsigned int *e_nconfs, type_data *s_move_x, type_data *s_move_y, type_data *s_move_z, type_data *s_quat_x, type_data *s_quat_y, type_data *s_quat_z, type_data *s_quat_w, type_data *s_energy, unsigned int *s_nconfs, unsigned int stride_e, unsigned int stride_s, unsigned int pos,struct param_t param, struct metaheuristic_t metaheuristic);

#endif
