#include "energy_common.h"
#include "energy_positions.h"
#include "energy_kernel.h"
#include "energy_common-gpu.h"
#include "energy_moving.h"
#include "metaheuristic_inc.h"
#include "energy_positions_common.h"
#include <thrust/sequence.h>


extern void gpu_mejorar (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, struct vectors_t *vectors_e_s, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int orden_device, unsigned int fase)
{
	
	size_t available,total_m;
    cudaError_t cudaStatus;
    unsigned long int max_memory;
    unsigned long int max_positions;
    unsigned long int max_common;
    
    unsigned long int typeSize = proteina.nrec * sizeof(char);
    unsigned long int recSize = proteina.nrec * sizeof(type_data);
    unsigned long int bondSize = proteina.nrec * sizeof(int) * MAXBOND;
	
	unsigned long int ligTypeSize = ligando.nlig * sizeof(char);
    unsigned long int ligSize = ligando.nlig * sizeof(type_data);
    unsigned long int ligBondSize = ligando.nlig * sizeof(int) * MAXBOND;
    
    unsigned long int randSize = vectors_e_s->nconformations * sizeof(curandState_t);
    unsigned long int moveSize_tam = vectors_e_s->nconformations * sizeof(type_data);
    unsigned long int energyConf_tam = vectors_e_s->nconformations * sizeof(int);
    unsigned long int confSize_tam = vectors_e_s->nconformations * ligando.nlig * sizeof(type_data);
    unsigned long int tam_max, tam_tmp, nconformations_count_total, nconformations_count_partial, tam_conformation, stride_d;

    max_positions = (moveSize_tam * 8) * 2 + energyConf_tam;
    max_common = (recSize * 4) + typeSize + bondSize + (ligSize * 4) + ligTypeSize + ligBondSize;

    max_memory = max_positions + max_common;
    tam_conformation =  (sizeof(type_data) * 8 * 2) + sizeof(int) * 2 + sizeof(curandState_t);

	cudaSetDevice(devices->id[orden_device]);
    //cudaDeviceReset();
    cudaMemGetInfo(&tam_max, &total_m);
    cudaDeviceSynchronize();
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
    //std::cout << tam_max << " " << total_m << "\n";
    tam_tmp = tam_max - (tam_max * 0.2);

	if (tam_tmp < max_memory)
        {
                //printf("Trocear en 1 GPU\n");
                max_memory = max_common;
                nconformations_count_total = 0;
                nconformations_count_partial = 0;
                stride_d = 0;
                while (nconformations_count_total < vectors_e_s->nconformations)
                {
                        max_memory += tam_conformation;
                        if (max_memory > tam_tmp)
                        {
                                //printf("Trozo %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
                                //nconformations_count_total--;
                                //nconformations_count_partial--;
                                //Lanzar con nconformations_count_partial;
								multigpu_mejorar_gpusolver (proteina,ligando,param,vectors_e_s->weights,vectors_e_s->f_params,devices,vectors_e_s->move_x+stride_d,vectors_e_s->move_y+stride_d,vectors_e_s->move_z+stride_d,vectors_e_s->quat_x+stride_d,vectors_e_s->quat_y+stride_d,vectors_e_s->quat_z+stride_d,vectors_e_s->quat_w+stride_d,vectors_e_s->energy.energy+stride_d,vectors_e_s->energy.n_conformation+stride_d,metaheuristic,nconformations_count_partial,orden_device,stride_d,fase);

                                stride_d += nconformations_count_partial;
                                max_memory = max_common;
                                nconformations_count_partial = 0;
                        }
                        else
                        {
                                nconformations_count_total++;
                                nconformations_count_partial++;
                        }
                }
                //printf("Trozo final %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
				nconformations_count_partial--;
				multigpu_mejorar_gpusolver (proteina,ligando,param,vectors_e_s->weights,vectors_e_s->f_params,devices,vectors_e_s->move_x+stride_d,vectors_e_s->move_y+stride_d,vectors_e_s->move_z+stride_d,vectors_e_s->quat_x+stride_d,vectors_e_s->quat_y+stride_d,vectors_e_s->quat_z+stride_d,vectors_e_s->quat_w+stride_d,vectors_e_s->energy.energy+stride_d,vectors_e_s->energy.n_conformation+stride_d,metaheuristic,nconformations_count_partial,orden_device,stride_d,fase);
        }

        else
        {
                //printf("No Trocea\n");
                //Lanzar con todo
				multigpu_mejorar_gpusolver (proteina,ligando,param,vectors_e_s->weights,vectors_e_s->f_params,devices,vectors_e_s->move_x,vectors_e_s->move_y,vectors_e_s->move_z,vectors_e_s->quat_x,vectors_e_s->quat_y,vectors_e_s->quat_z,vectors_e_s->quat_w,vectors_e_s->energy.energy,vectors_e_s->energy.n_conformation,metaheuristic,vectors_e_s->nconformations,orden_device,0,fase);	
        }
	
}

extern void gpu_mejorar_environment (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, struct vectors_t *vectors_e_s, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int orden_device, unsigned int fase)
{
	struct vectors_t vectors_mejora;
	
	size_t available,total_m;
    cudaError_t cudaStatus;
    unsigned long int max_memory;
    unsigned long int max_positions;
    unsigned long int max_common;
    
    unsigned long int typeSize = proteina.nrec * sizeof(char);
    unsigned long int recSize = proteina.nrec * sizeof(type_data);
    unsigned long int bondSize = proteina.nrec * sizeof(int) * MAXBOND;
	
	unsigned long int ligTypeSize = ligando.nlig * sizeof(char);
    unsigned long int ligSize = ligando.nlig * sizeof(type_data);
    unsigned long int ligBondSize = ligando.nlig * sizeof(int) * MAXBOND;
    
    unsigned long int randSize = vectors_e_s->nconformations * sizeof(curandState_t);
    unsigned long int moveSize_tam = vectors_e_s->nconformations * sizeof(type_data);
    unsigned long int energyConf_tam = vectors_e_s->nconformations * sizeof(int);
    unsigned long int confSize_tam = vectors_e_s->nconformations * ligando.nlig * sizeof(type_data);
	
	unsigned long int randSize_environment = vectors_e_s->nconformations * metaheuristic.NEEImp * sizeof(curandState_t);
    unsigned long int moveSize_tam_environment = vectors_e_s->nconformations * metaheuristic.NEEImp * sizeof(type_data);
    unsigned long int energyConf_tam_environment = vectors_e_s->nconformations * metaheuristic.NEEImp * sizeof(int);
    unsigned long int confSize_tam_environment = vectors_e_s->nconformations * metaheuristic.NEEImp * ligando.nlig * sizeof(type_data);
	
    unsigned long int tam_max, tam_tmp, nconformations_count_total, nconformations_count_partial, tam_conformation, stride_d;

    max_positions = (moveSize_tam * 8) * 2 + energyConf_tam + (moveSize_tam_environment * 8) * 2 + energyConf_tam_environment;
    max_common = (recSize * 4) + typeSize + bondSize + (ligSize * 4) + ligTypeSize + ligBondSize;

    max_memory = max_positions + max_common;
    tam_conformation =  (sizeof(type_data) * 8 * 2) + sizeof(int) * 2 + sizeof(curandState_t) + ((sizeof(type_data) * 8 * 2) + sizeof(int) * 2 + sizeof(curandState_t)) *  metaheuristic.NEEImp;

    vectors_mejora.num_surface = vectors_e_s->num_surface;
    vectors_mejora.nconformations = vectors_e_s->nconformations * metaheuristic.NEEImp;

    vectors_mejora.move_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    vectors_mejora.move_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    vectors_mejora.move_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    vectors_mejora.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    vectors_mejora.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    vectors_mejora.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    vectors_mejora.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    vectors_mejora.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations);
    vectors_mejora.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    vectors_mejora.weights = (type_data *)malloc(sizeof(type_data)*SCORING_TERMS);

    memcpy(vectors_mejora.f_params, vectors_e_s->f_params,MAXTYPES * sizeof(struct force_field_param_t));
    memcpy(vectors_mejora.weights, vectors_e_s->weights,SCORING_TERMS * sizeof(type_data));
    
    thrust::sequence(vectors_mejora.energy.n_conformation,vectors_mejora.energy.n_conformation + vectors_mejora.nconformations);
    fill_conformations_environment (vectors_e_s->move_x,vectors_e_s->move_y,vectors_e_s->move_z,vectors_e_s->quat_x,vectors_e_s->quat_y,vectors_e_s->quat_z,vectors_e_s->quat_w,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,vectors_e_s->nconformations,param,metaheuristic);
	
	cudaSetDevice(devices->id[orden_device]);
    //cudaDeviceReset();
    cudaMemGetInfo(&tam_max, &total_m);
    cudaDeviceSynchronize();
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
    //std::cout << tam_max << " " << total_m << "\n";
    tam_tmp = tam_max - (tam_max * 0.2);

	if (tam_tmp < max_memory)
        {
                //printf("Trocear en 1 GPU\n");
                max_memory = max_common;
                nconformations_count_total = 0;
                nconformations_count_partial = 0;
                stride_d = 0;
                while (nconformations_count_total < vectors_e_s->nconformations)
                {
                        max_memory += tam_conformation;
                        if (max_memory > tam_tmp)
                        {
                                //printf("Trozo %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
                                //nconformations_count_total--;
                                //nconformations_count_partial--;
                                //Lanzar con nconformations_count_partial;
								multigpu_mejorar_gpusolver_environment (proteina,ligando,param,vectors_e_s->weights,vectors_e_s->f_params,devices,vectors_e_s->move_x+stride_d,vectors_e_s->move_y+stride_d,vectors_e_s->move_z+stride_d,vectors_e_s->quat_x+stride_d,vectors_e_s->quat_y+stride_d,vectors_e_s->quat_z+stride_d,vectors_e_s->quat_w+stride_d,vectors_e_s->energy.energy+stride_d,vectors_e_s->energy.n_conformation+stride_d,vectors_mejora.move_x+(stride_d*metaheuristic.NEEImp),vectors_mejora.move_y+(stride_d*metaheuristic.NEEImp),vectors_mejora.move_z+(stride_d*metaheuristic.NEEImp),vectors_mejora.quat_x+(stride_d*metaheuristic.NEEImp),vectors_mejora.quat_y+(stride_d*metaheuristic.NEEImp),vectors_mejora.quat_z+(stride_d*metaheuristic.NEEImp),vectors_mejora.quat_w+(stride_d*metaheuristic.NEEImp),vectors_mejora.energy.energy+(stride_d*metaheuristic.NEEImp),vectors_mejora.energy.n_conformation+(stride_d*metaheuristic.NEEImp),metaheuristic,nconformations_count_partial,nconformations_count_partial*metaheuristic.NEEImp,orden_device,stride_d,fase);
								
                                stride_d += nconformations_count_partial;
                                max_memory = max_common;
                                nconformations_count_partial = 0;
                        }
                        else
                        {
                                nconformations_count_total++;
                                nconformations_count_partial++;
                        }
                }
                //printf("Trozo final %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
				nconformations_count_partial--;
				multigpu_mejorar_gpusolver_environment (proteina,ligando,param,vectors_e_s->weights,vectors_e_s->f_params,devices,vectors_e_s->move_x+stride_d,vectors_e_s->move_y+stride_d,vectors_e_s->move_z+stride_d,vectors_e_s->quat_x+stride_d,vectors_e_s->quat_y+stride_d,vectors_e_s->quat_z+stride_d,vectors_e_s->quat_w+stride_d,vectors_e_s->energy.energy+stride_d,vectors_e_s->energy.n_conformation+stride_d,vectors_mejora.move_x+(stride_d*metaheuristic.NEEImp),vectors_mejora.move_y+(stride_d*metaheuristic.NEEImp),vectors_mejora.move_z+(stride_d*metaheuristic.NEEImp),vectors_mejora.quat_x+(stride_d*metaheuristic.NEEImp),vectors_mejora.quat_y+(stride_d*metaheuristic.NEEImp),vectors_mejora.quat_z+(stride_d*metaheuristic.NEEImp),vectors_mejora.quat_w+(stride_d*metaheuristic.NEEImp),vectors_mejora.energy.energy+(stride_d*metaheuristic.NEEImp),vectors_mejora.energy.n_conformation+(stride_d*metaheuristic.NEEImp),metaheuristic,nconformations_count_partial,nconformations_count_partial*metaheuristic.NEEImp,orden_device,stride_d,fase);
        }

        else
        {
                //printf("No Trocea\n");
                //Lanzar con todo
				multigpu_mejorar_gpusolver_environment (proteina,ligando,param,vectors_e_s->weights,vectors_e_s->f_params,devices,vectors_e_s->move_x,vectors_e_s->move_y,vectors_e_s->move_z,vectors_e_s->quat_x,vectors_e_s->quat_y,vectors_e_s->quat_z,vectors_e_s->quat_w,vectors_e_s->energy.energy,vectors_e_s->energy.n_conformation,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,vectors_mejora.energy.energy,vectors_mejora.energy.n_conformation,metaheuristic,vectors_e_s->nconformations,vectors_mejora.nconformations,orden_device,stride_d,fase);
        }

	free(vectors_mejora.move_x);
        free(vectors_mejora.move_y);
        free(vectors_mejora.move_z);
        free(vectors_mejora.quat_x);
        free(vectors_mejora.quat_y);
        free(vectors_mejora.quat_z);
        free(vectors_mejora.quat_w);
        free(vectors_mejora.energy.energy);
        free(vectors_mejora.energy.n_conformation);
        free(vectors_mejora.weights);
	
}

extern void generate_positions (struct param_t param, struct vectors_t *vectors_e_s, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int orden_device, unsigned int n) 
{

	size_t available,total_m;
    cudaError_t cudaStatus;
    unsigned long int max_memory;
    unsigned long int max_positions;
    unsigned long int max_common;
    
    unsigned long int randSize = vectors_e_s->nconformations * sizeof(curandState_t);
	unsigned long int surfaceSize = vectors_e_s->num_surface * sizeof(type_data);
	
    unsigned long int moveSize_tam = vectors_e_s->nconformations * sizeof(type_data);
    //unsigned long int confSize_tam = vectors_e_s->nconformations * ligando.nlig * sizeof(type_data);
    unsigned long int tam_max, tam_tmp, nconformations_count_total, nconformations_count_partial, tam_conformation, stride_d;

    max_positions = moveSize_tam * 7 + randSize;
    max_common = surfaceSize * 3;

    max_memory = max_positions + max_common;
    tam_conformation =  (sizeof(type_data) * 7) + sizeof(curandState_t);

	cudaSetDevice(devices->id[orden_device]);
    //cudaDeviceReset();
    cudaMemGetInfo(&tam_max, &total_m);
    cudaDeviceSynchronize();
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
    //std::cout << tam_max << " " << total_m << "\n";
    tam_tmp = tam_max - (tam_max * 0.2);
	
	if (tam_tmp < max_memory)
        {
                //printf("Trocear en 1 GPU\n");
                max_memory = max_common;
                nconformations_count_total = 0;
                nconformations_count_partial = 0;
                stride_d = 0;
                while (nconformations_count_total < vectors_e_s->nconformations)
                {
                        max_memory += tam_conformation;
                        if (max_memory > tam_tmp)
                        {
                                //printf("Trozo %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
                                //nconformations_count_total--;
                                //nconformations_count_partial--;
                                //Lanzar con nconformations_count_partial;
								generate_positions_calulation (param,nconformations_count_partial,vectors_e_s->num_surface,vectors_e_s->move_x+stride_d,vectors_e_s->move_y+stride_d,vectors_e_s->move_z+stride_d,vectors_e_s->quat_x+stride_d,vectors_e_s->quat_y+stride_d,vectors_e_s->quat_z+stride_d,vectors_e_s->quat_w+stride_d,vectors_e_s->surface_x,vectors_e_s->surface_y,vectors_e_s->surface_z,metaheuristic,devices,orden_device,0,stride_d/metaheuristic.NEIIni);

                                stride_d += nconformations_count_partial;
                                max_memory = max_common;
                                nconformations_count_partial = 0;
                        }
                        else
                        {
                                nconformations_count_total++;
                                nconformations_count_partial++;
                        }
                }
                //printf("Trozo final %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
				nconformations_count_partial--;
				generate_positions_calulation (param,nconformations_count_partial,vectors_e_s->num_surface,vectors_e_s->move_x+stride_d,vectors_e_s->move_y+stride_d,vectors_e_s->move_z+stride_d,vectors_e_s->quat_x+stride_d,vectors_e_s->quat_y+stride_d,vectors_e_s->quat_z+stride_d,vectors_e_s->quat_w+stride_d,vectors_e_s->surface_x,vectors_e_s->surface_y,vectors_e_s->surface_z,metaheuristic,devices,orden_device,0,stride_d/metaheuristic.NEIIni);
        }

        else
        {
                //printf("No Trocea\n");
                //Lanzar con todo
				generate_positions_calulation (param,vectors_e_s->nconformations,vectors_e_s->num_surface,vectors_e_s->move_x,vectors_e_s->move_y,vectors_e_s->move_z,vectors_e_s->quat_x,vectors_e_s->quat_y,vectors_e_s->quat_z,vectors_e_s->quat_w,vectors_e_s->surface_x,vectors_e_s->surface_y,vectors_e_s->surface_z,metaheuristic,devices,orden_device,0,0);
        }


}


extern void generate_positions_calulation (struct param_t param, unsigned int nconformations, unsigned int num_surface, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *surface_x, type_data *surface_y, type_data *surface_z, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int orden_device, unsigned int n, unsigned int start_point) 
{

	curandState_t *states_d;
	type_data *moves_x_d, *moves_y_d, *moves_z_d, * surface_x_d, * surface_y_d, * surface_z_d;		
	type_data *quat_d_x, *quat_d_y, *quat_d_z, *quat_d_w;
	unsigned int steps, moveType=MOVE,total,inicial;
	unsigned int confSize = nconformations * sizeof(type_data);
	cudaError_t cudaStatus;
	cudaSetDevice(devices->id[orden_device]);
	
	dataToGPU(nconformations, num_surface, moves_x_d, moves_y_d, moves_z_d, quat_d_x, quat_d_y, quat_d_z, quat_d_w, states_d, surface_x, surface_y, surface_z, surface_x_d, surface_y_d, surface_z_d);
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Error en memoria.\n");

     //Inicializa el generador de numeros aleatorios y el cuaternion de cada simulacion
	setupCurandState <<< (nconformations/devices->hilos[orden_device*metaheuristic.num_kernels+4])+1, devices->hilos[orden_device*metaheuristic.num_kernels+4] >>>  (states_d, param.automatic_seed,param.seed,nconformations);

	// check if kernel execution generated and error
	cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");

	setup <<< (nconformations /devices->hilos[orden_device*metaheuristic.num_kernels+3])+1, devices->hilos[orden_device*metaheuristic.num_kernels+3] >>> (quat_d_x, quat_d_y, quat_d_z, quat_d_w,nconformations);

	// check if kernel execution generated and error
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");
	steps = 0;	
	if (n == 0)
	{
		total = 20; 
		inicial = metaheuristic.NEIIni;
		//printf("inicial %d\n",metaheuristic.NEIIni);
	}
	else 
	{	
		total = param.steps_warm_up_gpu;
		inicial = 1000;
	}
	while (steps < total)
	{
		if (moveType == MOVE)
		{
			move <<< (nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+1])+1,devices->hilos[orden_device*metaheuristic.num_kernels+1] >>> (states_d, surface_x_d, surface_y_d, surface_z_d, param.max_desp, moves_x_d, moves_y_d, moves_z_d,start_point, inicial,nconformations);
			moveType = ROTATE;
		}
		else
		{
			rotation <<< (nconformations /devices->hilos[orden_device*metaheuristic.num_kernels+2])+1, devices->hilos[orden_device*metaheuristic.num_kernels+2] >>> (states_d, param.rotation, quat_d_x, quat_d_y, quat_d_z, quat_d_w,nconformations);
			moveType = MOVE;
		}
	
	// check if kernel execution generated and erro
		cudaDeviceSynchronize();
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel move o rotation execution failed \n");
		steps++;
	}

	cudaStatus = cudaMemcpy(move_x, moves_x_d, confSize, cudaMemcpyDeviceToHost);	
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed1!");
	cudaStatus = cudaMemcpy(move_y, moves_y_d, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(move_z, moves_z_d, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	cudaStatus = cudaMemcpy(quat_x, quat_d_x, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_y, quat_d_y, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_z, quat_d_z, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_w, quat_d_w, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");

	cudaFree(states_d);
	cudaFree(moves_x_d);
	cudaFree(moves_y_d);
	cudaFree(moves_z_d);
	cudaFree(quat_d_x);
	cudaFree(quat_d_y);
	cudaFree(quat_d_z);
	cudaFree(quat_d_w);
	cudaFree(surface_x_d);
	cudaFree(surface_y_d);
	cudaFree(surface_z_d);
	cudaDeviceReset();


}

extern void multigpu_mejorar_gpusolver (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, type_data *weights, struct force_field_param_t *f_params, struct device_t *devices, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *n_conformation, struct metaheuristic_t metaheuristic, unsigned int nconformations, unsigned int orden_device, unsigned int stride_d, unsigned int fase)
{

	curandState_t *states_d;
	//struct receptor_t *proteina_d;
	//struct ligand_t *ligando_d;
	type_data *moves_x_d, *moves_y_d, *moves_z_d, *energy_d;		
	type_data *quat_d_x, *quat_d_y, *quat_d_z, *quat_d_w, *quat_d_mejora_x, *quat_d_mejora_y, *quat_d_mejora_z, *quat_d_mejora_w;
	type_data *moves_x_d_mejora, *moves_y_d_mejora, *moves_z_d_mejora, *energy_d_mejora;
	unsigned int *energy_nconformation_d, *energy_nconformation_d_mejora;	

	type_data *rec_x_d, *rec_y_d, *rec_z_d, *qr_d;
	char *rectype_d, *ligtype_d;
	unsigned int *bondsr_d, *bonds_d, total;
	type_data *lig_x_d, *lig_y_d, *lig_z_d, *ql_d;	
	
	int steps, moveType=MOVE;
	cudaError_t cudaStatus;
	unsigned int moveSize = nconformations * sizeof(type_data);
	unsigned int confSize = nconformations * sizeof(type_data);
	
	cudaSetDevice(devices->id[orden_device]);
	cudaDeviceReset();	
 	
	dataToGPU_multigpu_mejorar_inicializar(nconformations, energy_d, energy_nconformation_d, energy_d_mejora, energy_nconformation_d_mejora, moves_x_d, moves_y_d, moves_z_d, quat_d_x, quat_d_y, quat_d_z, quat_d_w, moves_x_d_mejora, moves_y_d_mejora, moves_z_d_mejora, quat_d_mejora_x,quat_d_mejora_y,quat_d_mejora_z,quat_d_mejora_w, states_d, move_x, move_y, move_z, quat_x, quat_y, quat_z, quat_w, energy, n_conformation, metaheuristic);
	//commonDataToGPU (proteina, proteina_d, ligando, ligando_d);
	commonDataToGPU_WS (proteina,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,ligando,lig_x_d,lig_y_d,lig_z_d,ql_d,ligtype_d,bonds_d);
	save_params(weights,f_params);
	///Inicializa el generador de numeros aleatorios y el cuaternion de cada simulacion
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Error en memoria\n");
	
	setupCurandState <<< (nconformations/devices->hilos[orden_device*metaheuristic.num_kernels+4])+1, devices->hilos[orden_device*metaheuristic.num_kernels+4] >>>  (states_d, param.automatic_seed,param.seed,nconformations);

	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");
	if (fase == 0)
		total = metaheuristic.IMEIni;
	if (fase == 1)
		total = metaheuristic.IMEImp;
	if (fase == 2)
		total = metaheuristic.IMEMUCom;
	steps = 0;
	moveType = MOVE;
	//printf("Voy a mejorar....\n");
	cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeFourByte);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_by_warp, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_by_warp_type1, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_by_warp_type2, cudaFuncCachePreferEqual);

	while (steps < total){
		//printf("Device %d pasada %d\n",orden_device,steps);

		if (moveType == MOVE)
		{
			//printf("He movido\n");
			move_mejora <<< (nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+5])+1, devices->hilos[orden_device*metaheuristic.num_kernels+5] >>> (states_d,param.max_desp,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,nconformations);
			moveType = ROTATE;
		}
		else {
			rotation <<< (nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+2])+1, devices->hilos[orden_device*metaheuristic.num_kernels+2] >>> (states_d,param.rotation,quat_d_mejora_x,quat_d_mejora_y,quat_d_mejora_z,quat_d_mejora_w,nconformations);
				moveType = MOVE;
		}
		//printf("Voy a calcular la energia...\n");
		
		int size = 5 * devices->hilos[orden_device*metaheuristic.num_kernels] * sizeof(type_data) + sizeof(char)*devices->hilos[orden_device*metaheuristic.num_kernels];	
		int blk = ceil(nconformations*WARP_SIZE / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
		unsigned int max = nconformations* WARP_SIZE;	
		dim3 hilos (devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);
		dim3 grid  (ceil(blk/8)+1, 8);		                                                             
		
		switch (param.scoring_function_type)
		{
			case 0:
				Gpu_full_Kernel_Conformations_by_warp <<<grid,hilos,size>>> (proteina.nrec,ligando.atoms,ligando.nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_d_mejora_x,quat_d_mejora_y,quat_d_mejora_z,quat_d_mejora_w,lig_x_d,lig_y_d,lig_z_d,ql_d,ligtype_d,bonds_d,energy_d_mejora,energy_nconformation_d,stride_d,devices->hilos[orden_device*metaheuristic.num_kernels],max);
				break;
			case 1:
				Gpu_full_Kernel_Conformations_by_warp_type1 <<<grid,hilos,size>>> (proteina.nrec,ligando.atoms,ligando.nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_d_mejora_x,quat_d_mejora_y,quat_d_mejora_z,quat_d_mejora_w,lig_x_d,lig_y_d,lig_z_d,ql_d,ligtype_d,bonds_d,energy_d_mejora,energy_nconformation_d,stride_d,devices->hilos[orden_device*metaheuristic.num_kernels],max);
                                break;
			case 2:
				Gpu_full_Kernel_Conformations_by_warp_type2 <<<grid,hilos,size>>> (proteina.nrec,ligando.atoms,ligando.nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_d_mejora_x,quat_d_mejora_y,quat_d_mejora_z,quat_d_mejora_w,lig_x_d,lig_y_d,lig_z_d,ql_d,ligtype_d,bonds_d,energy_d_mejora,energy_nconformation_d,stride_d,devices->hilos[orden_device*metaheuristic.num_kernels],max);
                                break;
		}
		cudaDeviceSynchronize();
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel energy failed! ERROR %d",cudaStatus);
		incluir_mejorar <<<(nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+6])+1, devices->hilos[orden_device*metaheuristic.num_kernels+6]>>> (moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_d_mejora_x,quat_d_mejora_y,quat_d_mejora_z,quat_d_mejora_w,energy_d_mejora,energy_nconformation_d_mejora,moves_x_d,moves_y_d,moves_z_d,quat_d_x,quat_d_y,quat_d_z,quat_d_w,energy_d,stride_d,nconformations);	
		steps++;	
	}
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernels antes de copia fallo!\n");
	cudaStatus = cudaMemcpy(move_x, moves_x_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_x_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(move_y, moves_y_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_y_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(move_z, moves_z_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_z_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(energy, energy_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values energy_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_x, quat_d_x, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_y, quat_d_y, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_z, quat_d_z, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_w, quat_d_w, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	
	cudaFree(states_d);
	cudaFree(moves_x_d);
	cudaFree(moves_y_d);
	cudaFree(moves_z_d);
	cudaFree(quat_d_x);
	cudaFree(quat_d_y);
	cudaFree(quat_d_z);
	cudaFree(quat_d_w);
	cudaFree(rec_x_d);
	cudaFree(rec_y_d);
	cudaFree(rec_z_d);
	cudaFree(qr_d);
	cudaFree(bondsr_d);
	cudaFree(rectype_d);
	cudaFree(lig_x_d);
	cudaFree(lig_y_d);
	cudaFree(lig_z_d);
	cudaFree(ql_d);
	cudaFree(ligtype_d);
	cudaFree(bonds_d);
	cudaFree(energy_d);
	cudaFree(energy_d_mejora);
	cudaFree(energy_nconformation_d);
	cudaFree(energy_nconformation_d_mejora);
	cudaFree(moves_x_d_mejora);
	cudaFree(moves_y_d_mejora);
	cudaFree(moves_z_d_mejora);
	cudaFree(quat_d_mejora_x);
	cudaFree(quat_d_mejora_y);
	cudaFree(quat_d_mejora_z);
	cudaFree(quat_d_mejora_w);
	cudaDeviceReset();	
}


extern void multigpu_mejorar_gpusolver_environment (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, type_data *weights, struct force_field_param_t *f_params, struct device_t *devices, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *n_conformation, type_data *move_x_mejora, type_data *move_y_mejora, type_data *move_z_mejora, type_data *quat_x_mejora, type_data *quat_y_mejora, type_data *quat_z_mejora, type_data *quat_w_mejora, type_data *energy_mejora, unsigned int *n_conformation_mejora, struct metaheuristic_t metaheuristic, unsigned int nconformations, unsigned int nconformations_mejora, unsigned int orden_device, unsigned int stride_d, unsigned int fase)
{

	curandState_t *states_d_mejora;
	//struct receptor_t *proteina_d;
	//struct ligand_t *ligando_d;
	type_data *moves_x_d, *moves_y_d, *moves_z_d, *energy_d;		
	type_data *quat_d_x, *quat_d_y, *quat_d_z, *quat_d_w, *quat_d_x_mejora, *quat_d_y_mejora, *quat_d_z_mejora, *quat_d_w_mejora;
	type_data *moves_x_d_mejora, *moves_y_d_mejora, *moves_z_d_mejora, *energy_d_mejora;
	unsigned int *energy_nconformation_d, *energy_nconformation_d_mejora;	

	type_data *rec_x_d, *rec_y_d, *rec_z_d, *qr_d;
	char *rectype_d, *ligtype_d;
	unsigned int *bondsr_d, *bonds_d, total;
	type_data *lig_x_d, *lig_y_d, *lig_z_d, *ql_d;	
	
	int steps, moveType=MOVE;
	cudaError_t cudaStatus;
	unsigned int moveSize = nconformations * sizeof(type_data);
	unsigned int confSize = nconformations * sizeof(type_data);
	unsigned int moveSize_environment = nconformations_mejora * sizeof(type_data);
	unsigned int confSize_environment = nconformations_mejora * sizeof(type_data);
	
	cudaSetDevice(devices->id[orden_device]);
	cudaDeviceReset();	
 	
	dataToGPU_multigpu_mejorar_inicializar_environment(nconformations, nconformations_mejora, energy_d, energy_nconformation_d, energy_d_mejora, energy_nconformation_d_mejora, moves_x_d, moves_y_d, moves_z_d, quat_d_x, quat_d_y, quat_d_z, quat_d_w, moves_x_d_mejora, moves_y_d_mejora, moves_z_d_mejora, quat_d_x_mejora,quat_d_y_mejora,quat_d_z_mejora,quat_d_w_mejora, states_d_mejora, move_x, move_y, move_z, quat_x, quat_y, quat_z, quat_w, energy, n_conformation, move_x_mejora, move_y_mejora, move_z_mejora, quat_x_mejora, quat_y_mejora, quat_z_mejora, quat_w_mejora, energy_mejora, n_conformation_mejora, metaheuristic);
	//commonDataToGPU (proteina, proteina_d, ligando, ligando_d);
	commonDataToGPU_WS (proteina,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,ligando,lig_x_d,lig_y_d,lig_z_d,ql_d,ligtype_d,bonds_d);
	//printf("Hola\n");
	//for (int l=0;l<nconformations*metaheuristic.NEEImp;l++) printf("ANTES conf %d, mx %f my %f mz %f qx %f qy %f qz %f qw %f energy %f\n",l,move_x_mejora[l],move_y_mejora[l],move_z_mejora[l],quat_x_mejora[l],quat_y_mejora[l],quat_z_mejora[l],quat_w_mejora[l],energy[l/metaheuristic.NEEImp]);
	//exit(0);
	save_params(weights,f_params);
	///Inicializa el generador de numeros aleatorios y el cuaternion de cada simulacion
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Error en memoria\n");
	
	setupCurandState <<< (nconformations_mejora/devices->hilos[orden_device*metaheuristic.num_kernels+4])+1, devices->hilos[orden_device*metaheuristic.num_kernels+4] >>>  (states_d_mejora, param.automatic_seed,param.seed,nconformations_mejora);

	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");
	if (fase == 0)
		total = metaheuristic.IMEIni;
	if (fase == 1)
		total = metaheuristic.IMEImp;
	if (fase == 2)
		total = metaheuristic.IMEMUCom;
	steps = 0;
	moveType = MOVE;
	
	cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeFourByte);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_by_warp, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_by_warp_type1, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_by_warp_type2, cudaFuncCachePreferEqual);

	while (steps < total){
		//printf("Device %d pasada %d\n",orden_device,steps);

		if (moveType == MOVE)
		{
			//printf("He movido\n");
			move_mejora <<< (nconformations_mejora / devices->hilos[orden_device*metaheuristic.num_kernels+5])+1, devices->hilos[orden_device*metaheuristic.num_kernels+5] >>> (states_d_mejora,param.max_desp,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,nconformations_mejora);
			moveType = ROTATE;
		}
		else {
			rotation <<< (nconformations_mejora / devices->hilos[orden_device*metaheuristic.num_kernels+2])+1, devices->hilos[orden_device*metaheuristic.num_kernels+2] >>> (states_d_mejora,param.rotation,quat_d_x_mejora,quat_d_y_mejora,quat_d_z_mejora,quat_d_w_mejora,nconformations_mejora);
				moveType = MOVE;
		}
		//printf("Voy a calcular la energia...\n");
		
		unsigned int size = 5 * devices->hilos[orden_device*metaheuristic.num_kernels] * sizeof(type_data) + sizeof(char)*devices->hilos[orden_device*metaheuristic.num_kernels];	
		unsigned int blk = ceil(nconformations_mejora*WARP_SIZE / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
		unsigned int max = nconformations_mejora* WARP_SIZE;	
		dim3 hilos (devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);
		dim3 grid  (ceil(blk/8)+1, 8);		                                                             
	
		switch (param.scoring_function_type)
		{
			case 0:
				Gpu_full_Kernel_Conformations_by_warp <<<grid,hilos,size>>> (proteina.nrec,ligando.atoms,ligando.nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_d_x_mejora,quat_d_y_mejora,quat_d_z_mejora,quat_d_w_mejora,lig_x_d,lig_y_d,lig_z_d,ql_d,ligtype_d,bonds_d,energy_d_mejora,energy_nconformation_d_mejora,stride_d*metaheuristic.NEEImp,devices->hilos[orden_device*metaheuristic.num_kernels],max);
				break;
			case 1:
                                Gpu_full_Kernel_Conformations_by_warp_type1 <<<grid,hilos,size>>> (proteina.nrec,ligando.atoms,ligando.nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_d_x_mejora,quat_d_y_mejora,quat_d_z_mejora,quat_d_w_mejora,lig_x_d,lig_y_d,lig_z_d,ql_d,ligtype_d,bonds_d,energy_d_mejora,energy_nconformation_d_mejora,stride_d*metaheuristic.NEEImp,devices->hilos[orden_device*metaheuristic.num_kernels],max);
                                break;
			case 2:	
                                Gpu_full_Kernel_Conformations_by_warp_type2 <<<grid,hilos,size>>> (proteina.nrec,ligando.atoms,ligando.nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_d_x_mejora,quat_d_y_mejora,quat_d_z_mejora,quat_d_w_mejora,lig_x_d,lig_y_d,lig_z_d,ql_d,ligtype_d,bonds_d,energy_d_mejora,energy_nconformation_d_mejora,stride_d*metaheuristic.NEEImp,devices->hilos[orden_device*metaheuristic.num_kernels],max);
                                break;
		}
		
		cudaDeviceSynchronize();
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel energy failed! ERROR %d",cudaStatus);
		incluir_mejorar_environment <<<(nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+6])+1, devices->hilos[orden_device*metaheuristic.num_kernels+6]>>> (metaheuristic.NEEImp, moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_d_x_mejora,quat_d_y_mejora,quat_d_z_mejora,quat_d_w_mejora,energy_d_mejora,energy_nconformation_d,moves_x_d,moves_y_d,moves_z_d,quat_d_x,quat_d_y,quat_d_z,quat_d_w,energy_d,stride_d,nconformations);	
		//fill_conformations_mejora <<<(nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+6])+1, devices->hilos[orden_device*metaheuristic.num_kernels+6]>>> (metaheuristic.NEEImp, moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_d_x_mejora,quat_d_y_mejora,quat_d_z_mejora,quat_d_w_mejora,energy_d_mejora,moves_x_d,moves_y_d,moves_z_d,quat_d_x,quat_d_y,quat_d_z,quat_d_w,energy_d,nconformations);
		steps++;	
	}
	
	
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernels antes de copia fallo!\n");
	cudaStatus = cudaMemcpy(move_x, moves_x_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_x_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(move_y, moves_y_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_y_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(move_z, moves_z_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_z_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(energy, energy_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values energy_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_x, quat_d_x, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_y, quat_d_y, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_z, quat_d_z, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_w, quat_d_w, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	
	cudaFree(states_d_mejora);
	cudaFree(moves_x_d);
	cudaFree(moves_y_d);
	cudaFree(moves_z_d);
	cudaFree(quat_d_x);
	cudaFree(quat_d_y);
	cudaFree(quat_d_z);
	cudaFree(quat_d_w);
	cudaFree(moves_x_d_mejora);
	cudaFree(moves_y_d_mejora);
	cudaFree(moves_z_d_mejora);
	cudaFree(quat_d_x_mejora);
	cudaFree(quat_d_y_mejora);
	cudaFree(quat_d_z_mejora);
	cudaFree(quat_d_w_mejora);
	cudaFree(rec_x_d);
	cudaFree(rec_y_d);
	cudaFree(rec_z_d);
	cudaFree(qr_d);
	cudaFree(bondsr_d);
	cudaFree(rectype_d);
	cudaFree(lig_x_d);
	cudaFree(lig_y_d);
	cudaFree(lig_z_d);
	cudaFree(ql_d);
	cudaFree(ligtype_d);
	cudaFree(bonds_d);
	cudaFree(energy_d);
	cudaFree(energy_d_mejora);
	cudaFree(energy_nconformation_d);
	cudaFree(energy_nconformation_d_mejora);
	cudaDeviceReset();	
}
