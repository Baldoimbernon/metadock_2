#include "definitions.h"
#include "wtime.h"
#include "energy_common.h"
#include "energy_struct.h"
#include "energy_GpuSolver.h"
#include "energy_positions.h"
#include "energy_positions_flex.h"
#include "energy_positions_common.h"
#include "energy_montecarlo.h"
#include "metaheuristic_mgpu.h"
#include "metaheuristic_mgpu_flex.h"
#include "metaheuristic_mgpu_common.h"
#include <thrust/sequence.h>
#include <thrust/sort.h>
#include <omp.h>

using namespace std;

extern void multigpuSolver_flex (struct ligand_t ligando, struct receptor_t proteina, unsigned int surface, type_data *conf_x, type_data *conf_y, type_data *conf_z, type_data *move_x, type_data *move_y, type_data *move_z,type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, struct energy_t energy, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices,struct flexibility_params_t *flexibility_params)
{
	double tiempo_i, tiempo_f;
	unsigned int  pc_temp, dev, stride_temp,ttotal = 0;
	type_data kernels = 0;
		
	pc_temp = nconformations / surface;
	devices->stride[0] = 0;	
	/*stride_temp = floor((devices->trabajo[0] * 0.01)*surface);	
	devices->conformaciones[0] = stride_temp * pc_temp;
	ttotal = stride_temp;
	//printf("stride_temp %d %d\n",stride_temp,pc_temp);
	//exit(0);	
	for (unsigned int i=(devices->n_devices - 1);i > 0;i--)
	{
		stride_temp = floor((devices->trabajo[i] * 0.01)*surface);
		devices->conformaciones[i] = stride_temp * pc_temp;
		ttotal += stride_temp;
	}
	unsigned int diferencia = surface - ttotal;
	for (unsigned int i=0;i<diferencia;i++)
	{
		dev = i % devices->n_devices;
		devices->conformaciones[dev] += pc_temp;
	}
	for (unsigned int i=(devices->n_devices-1);i > 0;i--)
	{
		devices->stride[i] = nconformations - devices->conformaciones[i];
		nconformations = nconformations - devices->conformaciones[i];		
	}*/
	 for (int i=0;i < devices->n_devices;i++)
        {
                stride_temp = floor((devices->trabajo[i] * 0.01)*nconformations);
                devices->conformaciones[i] = stride_temp;
                ttotal += stride_temp;
        }
        int diferencia = nconformations - ttotal;
        for (int i=0;i<diferencia;i++)
        {
                dev = i % devices->n_devices;
                devices->conformaciones[dev] ++;
        }
        for (int i=(devices->n_devices-1);i > 0;i--)
        {
                devices->stride[i] = nconformations - devices->conformaciones[i];
                nconformations = nconformations - devices->conformaciones[i];
        }	
	tiempo_i = wtime();
	omp_set_num_threads(devices->n_devices);
	#pragma omp parallel for 	
	for (unsigned int j=0;j<devices->n_devices;j++)
	{
		unsigned int th = omp_get_thread_num();
		unsigned int stride_d = devices->stride[j];
		unsigned long int max_conformaciones = (devices->propiedades[j].maxGridSize[0] * (devices->hilos[th*metaheuristic.num_kernels] / WARP_SIZE)) * 8;
		//printf("Th %d j %d GPU %d stride %d conformaciones %d max_conformaciones %lu\n",th,j,devices->id[j],stride_d,devices->conformaciones[j],max_conformaciones);
		
		if (devices->conformaciones[j] < max_conformaciones)
			gpuSolver_flex (ligando,proteina,param,metaheuristic,devices,conf_x+(stride_d*ligando.nlig),conf_y+(stride_d*ligando.nlig),conf_z+(stride_d*ligando.nlig),move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy.energy+stride_d,energy.n_conformation+stride_d,weights,f_params,flexibility_params->n_links,devices->conformaciones[j],j,stride_d,flexibility_params->individual_bonds_VDW,flexibility_params->individual_bonds_ES,&kernels);					
		else
		{
			//DIVIDIR EN TROZOS
			while(devices->conformaciones[j] % max_conformaciones != 0) max_conformaciones --;
			printf("va a trocear\n");
			for (unsigned int desplazamiento=0;desplazamiento<devices->conformaciones[j];desplazamiento+=max_conformaciones)
			{
				gpuSolver_flex (ligando,proteina,param,metaheuristic,devices,conf_x+((stride_d+desplazamiento)*ligando.nlig),conf_y+((stride_d+desplazamiento)*ligando.nlig),conf_z+((stride_d+desplazamiento)*ligando.nlig),move_x+stride_d+desplazamiento,move_y+stride_d+desplazamiento,move_z+stride_d+desplazamiento,quat_x+stride_d+desplazamiento,quat_y+stride_d+desplazamiento,quat_z+stride_d+desplazamiento,quat_w+stride_d+desplazamiento,energy.energy+stride_d+desplazamiento,energy.n_conformation+stride_d+desplazamiento,weights,f_params,flexibility_params->n_links,max_conformaciones,j,stride_d+desplazamiento,flexibility_params->individual_bonds_VDW,flexibility_params->individual_bonds_ES,&kernels);
				//printf("Trozo Device %d\n",devices->id[th]);
			}
		}
	}
	tiempo_f = wtime() - tiempo_i;
	//printf("\tTiempo total GPU's: %f seg\n",tiempo_f);
	//exit(0);
	
}

extern void multigpuSolver_mejora_flex_environment (struct ligand_t ligando, struct receptor_t proteina, unsigned int surface, type_data *conf_x, type_data *conf_y, type_data *conf_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int fase, struct flexibility_params_t *flexibility_params)
{
	struct vectors_t vectors_mejora;
        double tiempo_i, tiempo_f;
        unsigned int  pc_temp, stride_temp,ttotal = 0;
	unsigned int n_c_temp = nconformations;
	unsigned int dev;
	//printf("nconformations %d, surface %d\n",nconformations,surface);
        pc_temp = nconformations / surface;
        devices->stride[0] = 0;
        /*stride_temp = (unsigned int)((devices->trabajo[0] * 0.01)*surface);
        devices->conformaciones[0] = stride_temp * pc_temp;
        ttotal = stride_temp;
	//printf("stride_temp %d %d\n",stride_temp,pc_temp);
        for (unsigned int i=(devices->n_devices - 1);i > 0;i--)
        {
                stride_temp = (unsigned int)((devices->trabajo[i] * 0.01)*surface);
                devices->conformaciones[i] = stride_temp * pc_temp;
                ttotal += stride_temp;
        }
        devices->conformaciones[0] += (surface - ttotal)*pc_temp;

        for (unsigned int i=(devices->n_devices-1);i > 0;i--)
        {
                devices->stride[i] = nconformations - devices->conformaciones[i];
                nconformations = nconformations - devices->conformaciones[i];
        }
        for (unsigned int i = 0;i<devices->n_devices;i++)*/
	 for (int i=0;i < devices->n_devices;i++)
        {
                stride_temp = floor((devices->trabajo[i] * 0.01)*nconformations);
                devices->conformaciones[i] = stride_temp;
                ttotal += stride_temp;
        }
        int diferencia = nconformations - ttotal;
        for (int i=0;i<diferencia;i++)
        {
                dev = i % devices->n_devices;
                devices->conformaciones[dev] ++;
        }
        for (int i=(devices->n_devices-1);i > 0;i--)
        {
                devices->stride[i] = nconformations - devices->conformaciones[i];
                nconformations = nconformations - devices->conformaciones[i];
        }
        tiempo_i = wtime();
        //omp_set_num_threads(devices->n_devices);
	nconformations = n_c_temp;
	vectors_mejora.num_surface = surface;
        vectors_mejora.nconformations = nconformations * metaheuristic.NEEImp;
	//printf(" vectors_mejora.nconformations %d, nconformations %d , metaheuristic.NEEImp %d\n",vectors_mejora.nconformations,nconformations,metaheuristic.NEEImp);

	vectors_mejora.conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations*ligando.nlig);
	vectors_mejora.conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations*ligando.nlig);
	vectors_mejora.conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations*ligando.nlig);
        vectors_mejora.move_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.move_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.move_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations);
        vectors_mejora.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.weights = (type_data *)malloc(sizeof(type_data)*SCORING_TERMS);

        memcpy(vectors_mejora.f_params, f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_mejora.weights, weights,SCORING_TERMS * sizeof(type_data));

        thrust::sequence(vectors_mejora.energy.n_conformation,vectors_mejora.energy.n_conformation + vectors_mejora.nconformations);
	
	fill_conformations_environment_flex (ligando.nlig,conf_x,conf_y,conf_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,nconformations,param,metaheuristic);

	omp_set_num_threads(devices->n_devices);

        //#pragma omp parallel for
                for (unsigned int j=0;j<devices->n_devices;j++)
                {
                        unsigned int th = omp_get_thread_num();
                        unsigned int stride_d = devices->stride[j];
                        //int stride_d = th * stride;
                        //printf("Th %d j %d stride %d\n",th,j,stride_d);
                        if (param.montecarlo)
                        {
                                unsigned long long int max_conformaciones_1 = (devices->propiedades[j].maxGridSize[0] * (devices->hilos[th*metaheuristic.num_kernels] / WARP_SIZE)) * 8;

                                //printf("Th %d j %d stride %d conformaciones %d max_conformaciones %lu mejora.conf %d\n",th,j,stride_d,devices->conformaciones[j],max_conformaciones_1,vectors_mejora.nconformations);
                                if (devices->conformaciones[j] < max_conformaciones_1)
				{
					//printf("Dentro Th %d j %d stride %d conformaciones %d max_conformaciones %lu\n",th,j,stride_d,devices->conformaciones[j],max_conformaciones_1);
					 //for(int k=0;k<vectors_mejora.nconformations*ligando.nlig;k++)
                                	//	printf("EN MEJO number %d x %f y %f z %f , m_x %f, m_y %f, m_z %f, energy %f\n",k,vectors_mejora.conformations_x[k],vectors_mejora.conformations_y[k],vectors_mejora.conformations_z[k],vectors_mejora.move_x[k/ligando.nlig],vectors_mejora.move_y[k/ligando.nlig],vectors_mejora.move_z[k/ligando.nlig],energy[(k/ligando.nlig)/metaheuristic.NEEImp]);
					 //exit(0);
					 multigpu_mejorar_montecarlo_flex_environment (proteina,ligando,param,weights, f_params, devices,conf_x + (stride_d*ligando.nlig),conf_y+(stride_d*ligando.nlig),conf_z+(stride_d*ligando.nlig),move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,nconfs+stride_d,vectors_mejora.conformations_x + (stride_d * metaheuristic.NEEImp * ligando.nlig),vectors_mejora.conformations_y + (stride_d * metaheuristic.NEEImp * ligando.nlig) , vectors_mejora.conformations_z + (stride_d * metaheuristic.NEEImp * ligando.nlig) ,vectors_mejora.move_x + (stride_d * metaheuristic.NEEImp),vectors_mejora.move_y + (stride_d * metaheuristic.NEEImp),vectors_mejora.move_z + (stride_d * metaheuristic.NEEImp),vectors_mejora.quat_x + (stride_d * metaheuristic.NEEImp),vectors_mejora.quat_y + (stride_d * metaheuristic.NEEImp),vectors_mejora.quat_z + (stride_d * metaheuristic.NEEImp),vectors_mejora.quat_w + (stride_d * metaheuristic.NEEImp), vectors_mejora.energy.energy + (stride_d * metaheuristic.NEEImp), vectors_mejora.energy.n_conformation + (stride_d * metaheuristic.NEEImp),metaheuristic,devices->conformaciones[j],devices->conformaciones[j]*metaheuristic.NEEImp,j,stride_d,flexibility_params,fase);
				}
			
				else
                        	{
                        		//DIVIDIR EN TROZOS
                        		while(devices->conformaciones[j] % max_conformaciones_1 != 0) max_conformaciones_1 --;
                              		printf("va a trocear\n");
                                	for (unsigned int desplazamiento=0;desplazamiento<devices->conformaciones[j];desplazamiento+=max_conformaciones_1)
                                	{
						multigpu_mejorar_montecarlo_flex_environment (proteina,ligando,param,weights,f_params, devices,conf_x + ((stride_d+desplazamiento)*ligando.nlig),conf_y+((stride_d+desplazamiento)*ligando.nlig),conf_z+((stride_d+desplazamiento)*ligando.nlig),move_x+(stride_d+desplazamiento),move_y+(stride_d+desplazamiento),move_z+(stride_d+desplazamiento),quat_x+(stride_d+desplazamiento),quat_y+(stride_d+desplazamiento),quat_z+(stride_d+desplazamiento),quat_w+(stride_d+desplazamiento),energy+(stride_d+desplazamiento),nconfs+(stride_d+desplazamiento),vectors_mejora.conformations_x + ((stride_d+desplazamiento) * metaheuristic.NEEImp * ligando.nlig),vectors_mejora.conformations_y + ((stride_d+desplazamiento) * metaheuristic.NEEImp * ligando.nlig) , vectors_mejora.conformations_z + ((stride_d+desplazamiento) * metaheuristic.NEEImp * ligando.nlig) ,vectors_mejora.move_x + ((stride_d+desplazamiento) * metaheuristic.NEEImp),vectors_mejora.move_y + ((stride_d+desplazamiento) * metaheuristic.NEEImp),vectors_mejora.move_z + ((stride_d+desplazamiento) * metaheuristic.NEEImp),vectors_mejora.quat_x + ((stride_d+desplazamiento) * metaheuristic.NEEImp),vectors_mejora.quat_y + ((stride_d+desplazamiento) * metaheuristic.NEEImp),vectors_mejora.quat_z + ((stride_d+desplazamiento) * metaheuristic.NEEImp),vectors_mejora.quat_w + ((stride_d+desplazamiento) * metaheuristic.NEEImp), vectors_mejora.energy.energy + ((stride_d+desplazamiento) * metaheuristic.NEEImp), vectors_mejora.energy.n_conformation + ((stride_d+desplazamiento) * metaheuristic.NEEImp),metaheuristic,max_conformaciones_1,max_conformaciones_1*metaheuristic.NEEImp,j,stride_d+desplazamiento,flexibility_params,fase);
                                		//printf("Device %d con trozo %d\n",devices->id[th],desplazamiento);
                                	}
                      		}
                   	}
                        else
                        {
                                unsigned long long int max_conformaciones_2 = (devices->propiedades[j].maxGridSize[0] * (devices->hilos[j*metaheuristic.num_kernels] / WARP_SIZE)) * 8;
                        //max_conformaciones_2 = 240000;
                        //printf("Th %d j %d stride %d conformaciones %d max_conformaciones %lu\n",th,j,stride_d,devices->conformaciones[j],max_conformaciones_2);
			//printf("Dentro Th %d j %d stride %d conformaciones %d max_conformaciones %lu\n",th,j,stride_d,devices->conformaciones[j],max_conformaciones_2);
                        //for(int k=0;k<vectors_mejora.nconformations*ligando.nlig;k++)
                        //      printf("EN MEJO number %d x %f y %f z %f , m_x %f, m_y %f, m_z %f, energy %f\n",k,vectors_mejora.conformations_x[k],vectors_mejora.conformations_y[k],vectors_mejora.conformations_z[k],vectors_mejora.move_x[k/ligando.nlig],vectors_mejora.move_y[k/ligando.nlig],vectors_mejora.move_z[k/ligando.nlig],energy[(k/ligando.nlig)/metaheuristic.NEEImp]);
                        //exit(0);
                                if (devices->conformaciones[j] < max_conformaciones_2)
					multigpu_mejorar_gpusolver_flex_environment (proteina,ligando,param,weights, f_params, devices,conf_x + (stride_d*ligando.nlig),conf_y+(stride_d*ligando.nlig),conf_z+(stride_d*ligando.nlig),move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,nconfs+stride_d,vectors_mejora.conformations_x + (stride_d * metaheuristic.NEEImp * ligando.nlig),vectors_mejora.conformations_y + (stride_d * metaheuristic.NEEImp * ligando.nlig) , vectors_mejora.conformations_z + (stride_d * metaheuristic.NEEImp * ligando.nlig) ,vectors_mejora.move_x + (stride_d * metaheuristic.NEEImp),vectors_mejora.move_y + (stride_d * metaheuristic.NEEImp),vectors_mejora.move_z + (stride_d * metaheuristic.NEEImp),vectors_mejora.quat_x + (stride_d * metaheuristic.NEEImp),vectors_mejora.quat_y + (stride_d * metaheuristic.NEEImp),vectors_mejora.quat_z + (stride_d * metaheuristic.NEEImp),vectors_mejora.quat_w + (stride_d * metaheuristic.NEEImp), vectors_mejora.energy.energy + (stride_d * metaheuristic.NEEImp), vectors_mejora.energy.n_conformation + (stride_d * metaheuristic.NEEImp),metaheuristic,devices->conformaciones[j],devices->conformaciones[j]*metaheuristic.NEEImp,j,stride_d,flexibility_params,fase);
				else
				{
					 //DIVIDIR EN TROZOS
                                        while(devices->conformaciones[j] % max_conformaciones_2 != 0) max_conformaciones_2 --;
                                        printf("va a trocear\n");
                                        for (unsigned int desplazamiento=0;desplazamiento<devices->conformaciones[j];desplazamiento+=max_conformaciones_2)
                                        {
						multigpu_mejorar_gpusolver_flex_environment (proteina,ligando,param,weights,f_params, devices,conf_x + ((stride_d+desplazamiento)*ligando.nlig),conf_y+((stride_d+desplazamiento)*ligando.nlig),conf_z+((stride_d+desplazamiento)*ligando.nlig),move_x+(stride_d+desplazamiento),move_y+(stride_d+desplazamiento),move_z+(stride_d+desplazamiento),quat_x+(stride_d+desplazamiento),quat_y+(stride_d+desplazamiento),quat_z+(stride_d+desplazamiento),quat_w+(stride_d+desplazamiento),energy+(stride_d+desplazamiento),nconfs+(stride_d+desplazamiento),vectors_mejora.conformations_x + ((stride_d+desplazamiento) * metaheuristic.NEEImp * ligando.nlig),vectors_mejora.conformations_y + ((stride_d+desplazamiento) * metaheuristic.NEEImp * ligando.nlig) , vectors_mejora.conformations_z + ((stride_d+desplazamiento) * metaheuristic.NEEImp * ligando.nlig) ,vectors_mejora.move_x + ((stride_d+desplazamiento) * metaheuristic.NEEImp),vectors_mejora.move_y + ((stride_d+desplazamiento) * metaheuristic.NEEImp),vectors_mejora.move_z + ((stride_d+desplazamiento) * metaheuristic.NEEImp),vectors_mejora.quat_x + ((stride_d+desplazamiento) * metaheuristic.NEEImp),vectors_mejora.quat_y + ((stride_d+desplazamiento) * metaheuristic.NEEImp),vectors_mejora.quat_z + ((stride_d+desplazamiento) * metaheuristic.NEEImp),vectors_mejora.quat_w + ((stride_d+desplazamiento) * metaheuristic.NEEImp), vectors_mejora.energy.energy + ((stride_d+desplazamiento) * metaheuristic.NEEImp), vectors_mejora.energy.n_conformation + ((stride_d+desplazamiento) * metaheuristic.NEEImp),metaheuristic,max_conformaciones_2,max_conformaciones_2*metaheuristic.NEEImp,j,stride_d+desplazamiento,flexibility_params,fase);
						 //printf("Device %d con trozo %d\n",devices->id[th],desplazamiento);
					}
                                }
                        }
        }
        
        tiempo_f = wtime() - tiempo_i;
	free(vectors_mejora.conformations_x);
	free(vectors_mejora.conformations_y);
	free(vectors_mejora.conformations_z);
        free(vectors_mejora.move_x);
        free(vectors_mejora.move_y);
        free(vectors_mejora.move_z);
        free(vectors_mejora.quat_x);
        free(vectors_mejora.quat_y);
        free(vectors_mejora.quat_z);
        free(vectors_mejora.quat_w);
        free(vectors_mejora.energy.energy);
        free(vectors_mejora.energy.n_conformation);
        free(vectors_mejora.weights);			

}

extern void multigpuSolver_mejora_flex (struct ligand_t ligando, struct receptor_t proteina, unsigned int surface, type_data *conf_x, type_data *conf_y, type_data *conf_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int fase, struct flexibility_params_t *flexibility_params)
{
	double tiempo_i, tiempo_f;
	unsigned int  pc_temp, dev, stride_temp,ttotal = 0;
	
	pc_temp = nconformations / surface;
	devices->stride[0] = 0;	
	/*stride_temp = (unsigned int)((devices->trabajo[0] * 0.01)*surface);	
	devices->conformaciones[0] = stride_temp * pc_temp;
	ttotal = stride_temp;
	for (unsigned int i=(devices->n_devices - 1);i > 0;i--)
	{
		stride_temp = (unsigned int)((devices->trabajo[i] * 0.01)*surface);
		devices->conformaciones[i] = stride_temp * pc_temp;	
		ttotal += stride_temp;		
	}
	devices->conformaciones[0] += (surface - ttotal)*pc_temp;
	
	for (unsigned int i=(devices->n_devices-1);i > 0;i--)
	{
		devices->stride[i] = nconformations - devices->conformaciones[i];
		nconformations = nconformations - devices->conformaciones[i];		
	}	
	for (unsigned int i = 0;i<devices->n_devices;i++)*/
	 for (int i=0;i < devices->n_devices;i++)
        {
                stride_temp = floor((devices->trabajo[i] * 0.01)*nconformations);
                devices->conformaciones[i] = stride_temp;
                ttotal += stride_temp;
        }
        int diferencia = nconformations - ttotal;
        for (int i=0;i<diferencia;i++)
        {
                dev = i % devices->n_devices;
                devices->conformaciones[dev] ++;
        }
        for (int i=(devices->n_devices-1);i > 0;i--)
        {
                devices->stride[i] = nconformations - devices->conformaciones[i];
                nconformations = nconformations - devices->conformaciones[i];
        }
	tiempo_i = wtime();
	omp_set_num_threads(devices->n_devices);
	
	#pragma omp parallel for 	
	for (unsigned int j=0;j<devices->n_devices;j++)
	{
		unsigned int th = omp_get_thread_num();
		unsigned int stride_d = devices->stride[j];
		//int stride_d = th * stride;		
		//printf("Th %d j %d stride %d conformations %d \n",th,j,devices->conformaciones[j],stride_d);	
		if (param.montecarlo)
		{
			unsigned long int max_conformaciones_1 = (devices->propiedades[j].maxGridSize[0] * (devices->hilos[j*metaheuristic.num_kernels] / WARP_SIZE)) * 8;
			//printf("Th %d j %d GPU %d stride %d conformaciones %d max_conformaciones %lu\n",th,j,devices->id[j],stride_d,devices->conformaciones[j],max_conformaciones_1);
		
			if (devices->conformaciones[j] < max_conformaciones_1)
				multigpu_mejorar_montecarlo_flex (proteina,ligando,param,weights,f_params,devices,conf_x+(stride_d*ligando.nlig),conf_y+(stride_d*ligando.nlig),conf_z+(stride_d*ligando.nlig),move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,nconfs+stride_d,metaheuristic,flexibility_params,devices->conformaciones[j],j,stride_d,fase);	
			else
			{
				//DIVIDIR EN TROZOS
				while(devices->conformaciones[j] % max_conformaciones_1 != 0) max_conformaciones_1 --;
				printf("va a trocear\n");
				for (unsigned int desplazamiento=0;desplazamiento<devices->conformaciones[j];desplazamiento+=max_conformaciones_1)
				{	
					multigpu_mejorar_montecarlo_flex (proteina,ligando,param,weights,f_params,devices,conf_x+((stride_d+desplazamiento)*ligando.nlig),conf_y+((stride_d+desplazamiento)*ligando.nlig),conf_z+((stride_d+desplazamiento)*ligando.nlig),move_x+(stride_d+desplazamiento),move_y+(stride_d+desplazamiento),move_z+(stride_d+desplazamiento),quat_x+(stride_d+desplazamiento),quat_y+(stride_d+desplazamiento),quat_z+(stride_d+desplazamiento),quat_w+(stride_d+desplazamiento),energy+(stride_d+desplazamiento),nconfs+(stride_d+desplazamiento),metaheuristic,flexibility_params,max_conformaciones_1,j,(stride_d+desplazamiento),fase);
					//printf("Trozo Device %d\n",devices->id[th]);
				}
			}	
		}
		else
		{
			unsigned long int max_conformaciones_2 = (devices->propiedades[j].maxGridSize[0] * (devices->hilos[j*metaheuristic.num_kernels] / WARP_SIZE)) * 8;
			//printf("Th %d j %d GPU %d stride %d conformaciones %d max_conformaciones %lu\n",th,j,devices->id[th],stride_d,devices->conformaciones[j],max_conformaciones_2);
		
			if (devices->conformaciones[th] < max_conformaciones_2)
				multigpu_mejorar_gpusolver_flex (proteina,ligando,param,weights,f_params,devices,conf_x+(stride_d*ligando.nlig),conf_y+(stride_d*ligando.nlig),conf_z+(stride_d*ligando.nlig),move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,nconfs+stride_d,metaheuristic,devices->conformaciones[j],j,stride_d,flexibility_params,fase);	
			else
			{
				//DIVIDIR EN TROZOS
				while(devices->conformaciones[j] % max_conformaciones_2 != 0) max_conformaciones_2 --;
				printf("va a trocear\n");
				for (unsigned int desplazamiento=0;desplazamiento<devices->conformaciones[j];desplazamiento+=max_conformaciones_2)
				{	
					multigpu_mejorar_gpusolver_flex (proteina,ligando,param,weights,f_params,devices,conf_x+((stride_d+desplazamiento)*ligando.nlig),conf_y+((stride_d+desplazamiento)*ligando.nlig),conf_z+((stride_d+desplazamiento)*ligando.nlig),move_x+(stride_d+desplazamiento),move_y+(stride_d+desplazamiento),move_z+(stride_d+desplazamiento),quat_x+(stride_d+desplazamiento),quat_y+(stride_d+desplazamiento),quat_z+(stride_d+desplazamiento),quat_w+(stride_d+desplazamiento),energy+(stride_d+desplazamiento),nconfs+(stride_d+desplazamiento),metaheuristic,max_conformaciones_2,j,(stride_d+desplazamiento),flexibility_params,fase);
					//printf("Trozo Device %d\n",devices->id[th]);
				}	
			}
		}
	}
	tiempo_f = wtime() - tiempo_i;
	//printf("\tTiempo MultiGPU mejora: %f seg\n",tiempo_f);
	
}


extern void mejorar_multigpu_flex (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, struct vectors_t *vectors_selec, struct metaheuristic_t metaheuristic, struct device_t *devices,unsigned int fase, struct flexibility_params_t *flexibility_params)
{
		
	multigpuSolver_mejora_flex (ligando,proteina,vectors_selec->num_surface,vectors_selec->conformations_x,vectors_selec->conformations_y,vectors_selec->conformations_z,vectors_selec->move_x,vectors_selec->move_y,vectors_selec->move_z,vectors_selec->quat_x,vectors_selec->quat_y,vectors_selec->quat_z,vectors_selec->quat_w,vectors_selec->energy.energy,vectors_selec->energy.n_conformation,vectors_selec->weights,vectors_selec->f_params,vectors_selec->nconformations,param,metaheuristic,devices,fase,flexibility_params);				
					
}

extern void mejorar_multigpu_flex_environment (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, struct vectors_t *vectors_selec, struct metaheuristic_t metaheuristic, struct device_t *devices,unsigned int fase, struct flexibility_params_t *flexibility_params)
{

        multigpuSolver_mejora_flex_environment (ligando,proteina,vectors_selec->num_surface,vectors_selec->conformations_x,vectors_selec->conformations_y,vectors_selec->conformations_z,vectors_selec->move_x,vectors_selec->move_y,vectors_selec->move_z,vectors_selec->quat_x,vectors_selec->quat_y,vectors_selec->quat_z,vectors_selec->quat_w,vectors_selec->energy.energy,vectors_selec->energy.n_conformation,vectors_selec->weights,vectors_selec->f_params,vectors_selec->nconformations,param,metaheuristic,devices,fase,flexibility_params);

}


