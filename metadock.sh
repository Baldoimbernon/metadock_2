#!/bin/bash
# Comprobación de argumentos
if [ $# -gt 11 ]; then
	echo "Usage BD: ./metadock.sh -BD config_file config_file_DQN force_field_file pdbqt_dir output_dir receptor ligand"
	echo "Usage VS: ./metadock.sh -VS config_file config_file_DQN force_field_file pdbqt_dir input_ligs output_dir receptor x_coord y_coord z_coord"

	exit -1
fi
pdbqt_dir=$5
config_file=$2
config_file_vs=$2
config_dqn_file=$3
output_dir=$6
receptor=$7
ligand=$8
input_dir_vs=$6
output_dir_vs=$7
receptor_vs=$8
x=$9
y=$10
z=${11}
VS_c='-VS'
BD_c='-BD'
dir_ejecutable='/home/baldomero/metadock-1.1/'
param_field=$4
#optimization=$4/optimization
pymol_file="pymol.pml"
echo " "

case "$1" in

-BD)
	echo "Config File: $config_file *  Config DQN-File $config_dqn_file * Output directory: $output_dir * Receptor: $receptor * Ligand: $ligand"

	# Crear directorios para guardar las conformaciones generadas
	mkdir -p $output_dir/molecules
	mkdir -p $output_dir/energies
	#mkdir -p $output_dir/txt
	#mkdir -p $output_dir/optimization/best_ligs
	#mkdir -p $output_dir/optimization/pymol

	rm -f $output_dir/molecules/*
	rm -f $output_dir/energies/*
	#rm -f $output_dir/txt/*
	#rm -f $output_dir/optimization/best_ligs/*
	#rm -f $output_dir/optimization/pymol/*

	echo "Executing Metadock BD mode Now...OK"

	./energy -c $config_file -d $config_dqn_file -o $output_dir -p $receptor -l $ligand -D $param_field -q $pdbqt_dir 2> log

	# Generar resultados 

	#echo "Generating energy files"

	#./process_results/energy_report/energy_report -i $output_dir -l $ligand -D ./process_results/ -m 0	
	;;
-VS)
	echo "Config File: $config_file_vs * input_directory: $input_dir_vs * output directory: $output_dir_vs * Receptor: $receptor_vs * X $x * Y $y * Z $z "
	mkdir -p $output_dir_vs/molecules
        mkdir -p $output_dir_vs/energies
        #mkdir -p $output_dir_vs/txt
        #mkdir -p $output_dir/optimization/best_ligs
        #mkdir -p $output_dir/optimization/pymol

        rm -f $output_dir_vs/molecules/*
        rm -f $output_dir_vs/energies/*
        #rm -f $output_dir_vs/txt/*
	mkdir -p $input_dir_vs/rigid
	mkdir -p $input_dir_vs/flex
	
	#./energy -a $input_dir_vs $output_dir_vs 2> log
	
	n_rigids=`ls $input_dir_vs/rigid | wc -l`
	n_flex=`ls $input_dir_vs/flex | wc -l`

	if [ $n_rigids -gt 0 ]; then 
		input_rig=$input_dir_vs/rigid
		#./energy -c $2 -o $4 -i $3 -r $5 -d $input_rig -v 1 -f 0 2> log
		./energy -c $config_file_vs -o $output_dir_vs -p $receptor_vs -i $input_rig -q $pdbqt_dir -x $x -y $y -z $z -D $param_field -v 1 -f 0 2> log_r
		 echo "Generating energy files"
		./process_results/energy_report/energy_report -i $output_dir_vs -D ./process_results/ -m 1 2>log_er
	#	#rm -f $input_flex
	fi
	if [ $n_flex -gt 0 ]; then
		input_flex=$input_dir_vs/flex
		# ./energy -c $2 -o $4 -i $3 -r $5 -d $input_flex -v 1 -f 1 2> log
		./energy -c $config_file_vs -o $output_dir_vs -p $receptor_vs -i $input_flex -q $pdbqt_dir -x $x -y $y -z $z -D $param_field -v 1 -f 1 2>log_f

		 echo "Generating energy files"

		./process_results/energy_report/energy_report -i $output_dir_vs -D ./process_results/ -m 1 2>log_er
		#rm -f $input_flex/*
	fi
	;;
*) 
	echo "Invalid OPTION"
	echo "Usage BD: ./metadock.sh -BD config_file force_field_file pdbqt_dir output_dir receptor ligand"
        echo "Usage VS: ./metadock.sh -VS config_file force_field_file pdbqt_dir input_ligs output_dir receptor x_coord y_coord z_coord"
	;;
esac
