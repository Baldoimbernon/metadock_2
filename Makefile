#OS Name (Linux or Darwin)
OSUPPER = $(shell uname -s 2>/dev/null | tr [:lower:] [:upper:])
OSLOWER = $(shell uname -s 2>/dev/null | tr [:upper:] [:lower:])

# Flags to detect 32-bit or 64-bit OS platform
OS_SIZE = $(shell uname -m | sed -e "s/i.86/32/" -e "s/x86_64/64/")
OS_ARCH = $(shell uname -m | sed -e "s/i386/i686/")


# These flags will override any settings
ifeq ($(i386),1)
	OS_SIZE = 32
	OS_ARCH = i686
endif

ifeq ($(x86_64),1)
	OS_SIZE = 64
	OS_ARCH = x86_64
endif

# Flags to detect either a Linux system (linux) or Mac OSX (darwin)
DARWIN = $(strip $(findstring DARWIN, $(OSUPPER)))

# Location of the CUDA Toolkit binaries and libraries
CUDA_PATH       ?= /usr/local/cuda-8.0
CUDA_INC_PATH   ?= $(CUDA_PATH)/include
CUDA_BIN_PATH   ?= $(CUDA_PATH)/bin
ifneq ($(DARWIN),)
  CUDA_LIB_PATH  ?= $(CUDA_PATH)/lib
else
  ifeq ($(OS_SIZE),32)
    CUDA_LIB_PATH  ?= $(CUDA_PATH)/lib
  else
    CUDA_LIB_PATH  ?= $(CUDA_PATH)/lib64
  endif
endif

# Common binaries
NVCC            ?= $(CUDA_BIN_PATH)/nvcc
GCC             ?= g++

# Extra user f
EXTRA_NVCCFLAGS ?= -w -use_fast_math
EXTRA_LDFLAGS   ?= -w -use_fast_math

# CUDA code generation flags 37 50 52 60 61
GENCODE_SM10    := -gencode arch=compute_10,code=sm_10
GENCODE_SM20    := -gencode arch=compute_20,code=sm_20 
GENCODE_SM35    := -gencode arch=compute_35,code=sm_35
GENCODE_SM37	:= -gencode arch=compute_37,code=sm_37
GENCODE_SM50	:= -gencode arch=compute_50,code=sm_50
GENCODE_SM52	:= -gencode arch=compute_52,code=sm_52
GENCODE_SM60	:= -gencode arch=compute_60,code=sm_60
GENCODE_SM61	:= -gencode arch=compute_61,code=sm_61
GENCODE_FLAGS   := $(GENCODE_SM20) $(GENCODE_SM35) $(GENCODE_SM37) $(GENCODE_SM50) $(GENCODE_SM52) $(GENCODE_SM60) $(GENCODE_SM61) --cudart=shared

# OS-specific build flags
ifneq ($(DARWIN),) 
      LDFLAGS   := -Xlinker -rpath $(CUDA_LIB_PATH) -L$(CUDA_LIB_PATH) -lstdc++ -lcudart -fopenmp
      CCFLAGS   := -arch $(OS_ARCH) 
else
  ifeq ($(OS_SIZE),32)
      LDFLAGS   := -L$(CUDA_LIB_PATH) -L/usr/lib/openbabel/2.3.2 -lopenbabel -lstdc++ -lcudart -fopenmp 
      CCFLAGS   := -m32
  else
      LDFLAGS   := -L$(CUDA_LIB_PATH) -L/usr/lib/openbabel/2.3.2 -lopenbabel -lstdc++ -lcudart -fopenmp
      CCFLAGS   := -m64
  endif
endif

# OS-architecture specific flags
ifeq ($(OS_SIZE),32)
      NVCCFLAGS := -m32
else
      NVCCFLAGS := -m64
endif

# Debug build flags
ifeq ($(dbg),1)
      CCFLAGS   += -g
      NVCCFLAGS += -g -G
      TARGET    := debug
else
      CCFLAGS   += -O3
      NVCCFLAGS += -O3  -Wno-deprecated-gpu-targets
      TARGET    := release
endif


# Common includes and paths for CUDA
INCLUDES      := -I$(CUDA_INC_PATH) -I/usr/include/openbabel-2.0 -I./VS -I./BD -I./common 

# Target rules
all: build

build: energy

# Compile "common"

quicksort_m.o: ./common/quicksort_m.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp
	
energy_common.o: ./common/energy_common.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

energy_CpuSolver_common.o: ./common/energy_CpuSolver_common.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp
	
energy_kernel.o: ./common/energy_kernel.cu
	$(NVCC) $(NVCCFLAGS) $(EXTRA_NVCCFLAGS) $(GENCODE_FLAGS) $(INCLUDES) -c -w -Xptxas -dlcm=ca -Xcompiler '-fPIC' -dc -use_fast_math $<	

energy_moving.o: ./common/energy_moving.cu
	$(NVCC) $(NVCCFLAGS) $(EXTRA_NVCCFLAGS) $(GENCODE_FLAGS) $(INCLUDES) -c -w -Xptxas -dlcm=ca -Xcompiler '-fPIC' -dc -use_fast_math $<
	
energy_positions_common.o: ./common/energy_positions_common.cu
	$(NVCC) $(NVCCFLAGS) $(EXTRA_NVCCFLAGS) $(GENCODE_FLAGS) $(INCLUDES) -c -w -Xptxas -dlcm=ca -Xcompiler '-fPIC' -dc -use_fast_math $<

wtime.o: ./common/wtime.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

ligand_work.o: ./common/ligand_work.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

ligand_babel.o: ./common/ligand_babel.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp	

metaheuristic_comb_common.o: ./common/metaheuristic_comb_common.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

metaheuristic_comb_vs_common.o: ./common/metaheuristic_comb_vs_common.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

metaheuristic_sel_common.o: ./common/metaheuristic_sel_common.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

metaheuristic_sel_vs_common.o: ./common/metaheuristic_sel_vs_common.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp
		
metaheuristic_inc_common.o: ./common/metaheuristic_inc_common.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

metaheuristic_inc_vs_common.o: ./common/metaheuristic_inc_vs_common.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

metaheuristic_mgpu_common.o: ./common/metaheuristic_mgpu_common.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

metaheuristic_mgpu_vs_common.o: ./common/metaheuristic_mgpu_vs_common.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

metaheuristic_sel_flex_common.o: ./common/metaheuristic_sel_flex_common.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

metaheuristic_mgpu_flex_common.o: ./common/metaheuristic_mgpu_flex_common.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

rotation.o: ./common/rotation.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp	
	
metaheuristic_cpu.o: ./common/metaheuristic_cpu.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

energy.o: ./common/energy.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -o $@ -c -w $< -fopenmp

# BD compile	
energy_mutation.o: ./BD/energy_mutation.cu
	$(NVCC) $(NVCCFLAGS) $(EXTRA_NVCCFLAGS) $(GENCODE_FLAGS) $(INCLUDES) -c -w -Xptxas -dlcm=ca -Xcompiler '-fPIC' -dc -use_fast_math $<

energy_mutation_flex.o: ./BD/energy_mutation_flex.cu
	$(NVCC) $(NVCCFLAGS) $(EXTRA_NVCCFLAGS) $(GENCODE_FLAGS) $(INCLUDES) -c -w -Xptxas -dlcm=ca -Xcompiler '-fPIC' -dc -use_fast_math $<

energy_common-gpu.o: ./BD/energy_common-gpu.cu
	$(NVCC) $(NVCCFLAGS) $(EXTRA_NVCCFLAGS) $(GENCODE_FLAGS) $(INCLUDES) -c -w -Xptxas -dlcm=ca -Xcompiler '-fPIC' -dc -use_fast_math $<

energy_CpuSolver.o: ./BD/energy_CpuSolver.cpp 
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

energy_flexibility.o: ./BD/energy_flexibility.cu
	$(NVCC) $(NVCCFLAGS) $(EXTRA_NVCCFLAGS) $(GENCODE_FLAGS) $(INCLUDES) -c -w -Xptxas -dlcm=ca -Xcompiler '-fPIC' -dc -use_fast_math $<

energy_CpuSolver_flex.o: ./BD/energy_CpuSolver_flex.cpp 
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp
		
energy_montecarlo.o: ./BD/energy_montecarlo.cu
	$(NVCC) $(NVCCFLAGS) $(EXTRA_NVCCFLAGS) $(GENCODE_FLAGS) $(INCLUDES) -c -w -Xptxas -dlcm=ca -Xcompiler '-fPIC' -dc -use_fast_math $<
	
energy_GpuSolver.o: ./BD/energy_GpuSolver.cu
	$(NVCC) $(NVCCFLAGS) $(EXTRA_NVCCFLAGS) $(GENCODE_FLAGS) $(INCLUDES) -c -w -Xptxas -dlcm=ca -Xcompiler '-fPIC' -dc -use_fast_math $<

energy_positions.o: ./BD/energy_positions.cu
	$(NVCC) $(NVCCFLAGS) $(EXTRA_NVCCFLAGS) $(GENCODE_FLAGS) $(INCLUDES) -c -w -Xptxas -dlcm=ca -Xcompiler '-fPIC' -dc -use_fast_math $<

energy_positions_flex.o: ./BD/energy_positions_flex.cu
	$(NVCC) $(NVCCFLAGS) $(EXTRA_NVCCFLAGS) $(GENCODE_FLAGS) $(INCLUDES) -c -w -Xptxas -dlcm=ca -Xcompiler '-fPIC' -dc -use_fast_math $<

energy_mutation_cpp.o: ./BD/energy_mutation_cpp.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

energy_mutation_cpp_flex.o: ./BD/energy_mutation_cpp_flex.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

energy_surface.o: ./BD/energy_surface.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

flexibility.o: ./BD/flexibility.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp
	
metaheuristic_comb.o: ./BD/metaheuristic_comb.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp
	
metaheuristic_comb_flex.o: ./BD/metaheuristic_comb_flex.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

metaheuristic_inc.o: ./BD/metaheuristic_inc.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

metaheuristic_inc_flex.o: ./BD/metaheuristic_inc_flex.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp	

metaheuristic_mgpu.o: ./BD/metaheuristic_mgpu.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

metaheuristic_mgpu_flex.o: ./BD/metaheuristic_mgpu_flex.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp
	
metaheuristic_ini_flex.o: ./BD/metaheuristic_ini_flex.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp
	
metaheuristic_ini.o: ./BD/metaheuristic_ini.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp	

metaheuristic_sel.o: ./BD/metaheuristic_sel.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp
	
metaheuristic_sel_flex.o: ./BD/metaheuristic_sel_flex.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp
	
montecarlo.o: ./BD/montecarlo.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp
	
# VS compile	

energy_positions_vs_flex.o: ./VS/energy_positions_vs_flex.cu
	$(NVCC) $(NVCCFLAGS) $(EXTRA_NVCCFLAGS) $(GENCODE_FLAGS) $(INCLUDES) -c -w -Xptxas -dlcm=ca -Xcompiler '-fPIC' -dc -use_fast_math $<
	
energy_flexibility_vs.o: ./VS/energy_flexibility_vs.cu
	$(NVCC) $(NVCCFLAGS) $(EXTRA_NVCCFLAGS) $(GENCODE_FLAGS) $(INCLUDES) -c -w -Xptxas -dlcm=ca -Xcompiler '-fPIC' -dc -use_fast_math $<
	
energy_common-gpu_vs.o: ./VS/energy_common-gpu_vs.cu
	$(NVCC) $(NVCCFLAGS) $(EXTRA_NVCCFLAGS) $(GENCODE_FLAGS) $(INCLUDES) -c -w -Xptxas -dlcm=ca -Xcompiler '-fPIC' -dc -use_fast_math $<
	
energy_positions_vs.o: ./VS/energy_positions_vs.cu
	$(NVCC) $(NVCCFLAGS) $(EXTRA_NVCCFLAGS) $(GENCODE_FLAGS) $(INCLUDES) -c -w -Xptxas -dlcm=ca -Xcompiler '-fPIC' -dc -use_fast_math $<
	
energy_GpuSolver_vs.o: ./VS/energy_GpuSolver_vs.cu
	$(NVCC) $(NVCCFLAGS) $(EXTRA_NVCCFLAGS) $(GENCODE_FLAGS) $(INCLUDES) -c -w -Xptxas -dlcm=ca -Xcompiler '-fPIC' -dc -use_fast_math $<
	
energy_montecarlo_vs.o: ./VS/energy_montecarlo_vs.cu
	$(NVCC) $(NVCCFLAGS) $(EXTRA_NVCCFLAGS) $(GENCODE_FLAGS) $(INCLUDES) -c -w -Xptxas -dlcm=ca -Xcompiler '-fPIC' -dc -use_fast_math $<

energy_mutation_vs.o: ./VS/energy_mutation_vs.cu 
	$(NVCC) $(NVCCFLAGS) $(EXTRA_NVCCFLAGS) $(GENCODE_FLAGS) $(INCLUDES) -c -w -Xptxas -dlcm=ca -Xcompiler '-fPIC' -dc -use_fast_math $<

energy_mutation_vs_flex.o: ./VS/energy_mutation_vs_flex.cu
	$(NVCC) $(NVCCFLAGS) $(EXTRA_NVCCFLAGS) $(GENCODE_FLAGS) $(INCLUDES) -c -w -Xptxas -dlcm=ca -Xcompiler '-fPIC' -dc -use_fast_math $<

		
flexibility_vs.o: ./VS/flexibility_vs.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

metaheuristic_ini_vs_flex.o: ./VS/metaheuristic_ini_vs_flex.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp
		
metaheuristic_inc_vs_flex.o: ./VS/metaheuristic_inc_vs_flex.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp
		
energy_CpuSolver_vs_flex.o: ./VS/energy_CpuSolver_vs_flex.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp
	
energy_vs.o: ./VS/energy_vs.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

energy_common_vs.o: ./VS/energy_common_vs.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

energy_CpuSolver_vs.o: ./VS/energy_CpuSolver_vs.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp
	
metaheuristic_ini_vs.o: ./VS/metaheuristic_ini_vs.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp
		
metaheuristic_sel_vs.o: ./VS/metaheuristic_sel_vs.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp
	
metaheuristic_inc_vs.o: ./VS/metaheuristic_inc_vs.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp
	
metaheuristic_comb_vs.o: ./VS/metaheuristic_comb_vs.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp
	
metaheuristic_comb_vs_flex.o: ./VS/metaheuristic_comb_vs_flex.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp
		
montecarlo_vs.o: ./VS/montecarlo_vs.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

metaheuristic_mgpu_vs.o: ./VS/metaheuristic_mgpu_vs.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp	

metaheuristic_mgpu_vs_flex.o: ./VS/metaheuristic_mgpu_vs_flex.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp 

energy_mutation_cpp_vs.o: ./VS/energy_mutation_cpp_vs.cpp 
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

energy_mutation_cpp_vs_flex.o: ./VS/energy_mutation_cpp_vs_flex.cpp
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -c -w $< -o $@ -fopenmp

dlink.o: energy_mutation_vs_flex.o energy_mutation_vs.o energy_mutation_flex.o energy_kernel.o energy_moving.o energy_positions_common.o energy_mutation.o energy_common-gpu.o energy_flexibility.o energy_montecarlo.o energy_GpuSolver.o energy_positions.o energy_positions_flex.o energy_positions_vs_flex.o energy_flexibility_vs.o energy_common-gpu_vs.o energy_positions_vs.o energy_GpuSolver_vs.o energy_montecarlo_vs.o
	$(NVCC) $(NVCCFLAGS) $(EXTRA_NVCCFLAGS) $(GENCODE_FLAGS) $(INCLUDES) -w  -Xptxas -dlcm=ca -Xcompiler '-fPIC' -dlink -use_fast_math $+ -o $@

cuda_kernels.a: energy_mutation_vs_flex.o energy_mutation_vs.o energy_mutation_flex.o energy_kernel.o energy_moving.o energy_positions_common.o energy_mutation.o energy_common-gpu.o energy_flexibility.o energy_montecarlo.o energy_GpuSolver.o energy_positions.o energy_positions_flex.o energy_positions_vs_flex.o energy_flexibility_vs.o energy_common-gpu_vs.o energy_positions_vs.o energy_GpuSolver_vs.o energy_montecarlo_vs.o dlink.o
	$(GCC) $(CCFLAGS) $(EXTRA_CCFLAGS) $(INCLUDES) -shared -w -o $@ $+ $(LDFLAGS) $(EXTRA_LDFLAGS)
	
energy: energy.o energy_mutation_cpp_vs_flex.o energy_mutation_cpp_vs.o energy_mutation_cpp.o energy_mutation_cpp_flex.o quicksort_m.o energy_common.o energy_CpuSolver_common.o wtime.o ligand_work.o ligand_babel.o metaheuristic_comb_common.o metaheuristic_comb_vs_common.o metaheuristic_inc_vs_common.o metaheuristic_sel_common.o metaheuristic_sel_vs_common.o metaheuristic_inc_common.o metaheuristic_mgpu_common.o metaheuristic_sel_flex_common.o metaheuristic_mgpu_flex_common.o rotation.o metaheuristic_cpu.o energy_CpuSolver.o energy_CpuSolver_flex.o energy_surface.o flexibility.o metaheuristic_comb.o metaheuristic_comb_flex.o metaheuristic_inc.o metaheuristic_inc_flex.o metaheuristic_mgpu.o metaheuristic_mgpu_flex.o metaheuristic_ini_flex.o metaheuristic_ini.o metaheuristic_sel.o metaheuristic_sel_flex.o montecarlo.o	montecarlo_vs.o metaheuristic_comb_vs.o metaheuristic_comb_vs_flex.o metaheuristic_inc_vs.o metaheuristic_sel_vs.o metaheuristic_ini_vs.o energy_CpuSolver_vs.o energy_common_vs.o energy_vs.o energy_CpuSolver_vs_flex.o metaheuristic_inc_vs_flex.o metaheuristic_ini_vs_flex.o flexibility_vs.o metaheuristic_mgpu_vs_common.o metaheuristic_mgpu_vs.o metaheuristic_mgpu_vs_flex.o cuda_kernels.a
	$(GCC) $(INCLUDES) $(CCFLAGS) -o $@ $+ $(LDFLAGS) $(EXTRA_LDFLAGS)

run: build
	./energy

clean:
	rm -f energy *.o log log_* *.a

data:
	rm -f log log_* prueba.pbs.e* prueba.pbs.o* slurm-* 
