#include <thrust/sequence.h>
#include "energy_common.h"
#include "energy_common_vs.h"
#include "energy_struct.h"
#include "metaheuristic_cpu.h"
#include "metaheuristic_mgpu.h"
#include "metaheuristic_mgpu_vs.h"
#include "metaheuristic_mgpu_common.h"
#include "metaheuristic_mgpu_vs_common.h"
#include "metaheuristic_mgpu_vs_flex.h"
#include "metaheuristic_ini.h"
#include "metaheuristic_ini_vs.h"
#include "metaheuristic_ini_vs_flex.h"
#include "metaheuristic_comb_vs.h"
#include "metaheuristic_comb_vs_flex.h"
#include "metaheuristic_sel_vs.h"
#include "metaheuristic_inc_vs.h"
#include "montecarlo_vs.h"
#include "energy_CpuSolver_vs.h"
#include "energy_positions_vs.h"
#include "energy_positions.h"
#include "energy_positions_common.h"
#include "energy_positions_vs_flex.h"
#include "energy_montecarlo_vs.h"
#include "energy_mutation.h"
#include "energy_mutation_cpp.h"
#include "energy_mutation_flex.h"
#include "energy_mutation_cpp_flex.h"
#include "energy_mutation_vs.h"
#include "energy_mutation_cpp_vs.h"
#include "energy_mutation_vs_flex.h"
#include "energy_mutation_cpp_vs_flex.h"
#include "ligand.h"
#include "wtime.h"
#include <dirent.h>
#include <unistd.h>
#include <signal.h>

using namespace std;

extern void max_atoms_ligand (char *filename, unsigned int *atoms)
{
	unsigned int nAtom,nBond;
	char cadena[20];

	ifstream file (filename, ifstream::in );

	if (!file.good()) {
		cout << "can't open file " << filename << endl;
		cout << "Will not read ligand file" << endl;
		exit(1);
	}

	file >> cadena;
	file >> cadena;
	//Read number of atoms
	file >> *atoms >> nBond;
	//printf("atoms %d nBonds %d\n",*atoms,nBond);
}

extern void extract_ligands_params (struct ligand_params_t *&ligand_params, struct vectors_t *vectors, struct param_t param)
{
	DIR *dir;
	char cadena[FILENAME];
	struct dirent *ent;
	unsigned int files,atoms_max,atoms;
	
	dir = opendir(param.input_ligs);
	if (dir == NULL)
	{
		printf("ERROR open directory %s\n",param.input_dir_l);
		exit(1);
	}
	files = 0;
	atoms_max = 0;
	
	while ((ent = readdir(dir)) != NULL)
	{
		if ((strcmp(ent->d_name, ".") != 0) && (strcmp(ent->d_name, "..") != 0))
		{		
			sprintf(cadena,"%s/%s",param.input_ligs,ent->d_name);
			//printf("Cadena: %s\n",cadena);	
			max_atoms_ligand(cadena,&atoms);
			if (atoms > atoms_max) atoms_max = atoms;
			files++;
		}
	}
	closedir(dir);
	//printf("Max atoms: %d, files: %d\n",atoms_max,files);
	vectors->nlig = atoms_max;
	vectors->files = files;
	ligand_params = (ligand_params_t *)malloc(sizeof(ligand_params_t)*files);
	files = 0;
	dir = opendir(param.input_ligs);
	if (dir == NULL)
	{	
		printf("ERROR open directory %s\n",param.input_dir_l);
		exit(1);
	}
	while ((ent = readdir(dir)) != NULL)
	{
		if ((strcmp(ent->d_name, ".") != 0) && (strcmp(ent->d_name, "..") != 0))
		{
			sprintf(cadena,"%s/%s",param.input_ligs,ent->d_name);
			max_atoms_ligand(cadena,&atoms_max);
			//printf("Fichero: %s\n",cadena);
			strcpy(ligand_params[files].file,cadena);
			ligand_params[files].atoms = atoms_max;
			files++;
		}
			
	}
	closedir(dir);
}

extern void salir_alarma_vs (int i)
{
	printf("¡¡¡ FIN POR TIEMPO !!!\n");
	exit(0);
}

extern void vs_type (struct vectors_t *vectors, struct param_t param, struct metaheuristic_t metaheuristic, char *configfile) 
{
		
	struct ligand_params_t *ligand_params;
	struct flexibility_params_t * flexibility_params;
	struct flexibility_data_t *flexibility_conformations;
	double twarm_up,tinicializar, tseleccion, tcombinar, tmut, tmejora, tincluir, ttotal;
	double twarm_up_t,tinicializar_t, tseleccion_t, tcombinar_t, tmut_t, tmejora_t, tincluir_t,ttotal_t;
	struct vectors_t vectors_ini, vectors_selec, vectors_comb, vectors_o;
	struct device_t devices;
	struct receptor_t protein;
	struct receptor_t proteina;
	type_data *c_x, *c_y, *c_z, *ql_tmp;
	char *ligtype_tmp, *subtype_tmp, *nbonds_tmp;
	char cadena[FILENAME];
	char str[2];
	unsigned int *bonds_tmp, number_devices;
	unsigned int alarma;
	unsigned int stride_e, stride_s;
	FILE *fp;
	//struct ligand_t ligando, ligtemp;
	Protein_entry * Protein;
	Bond_tp * Bond;
	
	unsigned int error, device, err, nAtom, nBond, improve = 0, count = 0;
	unsigned int qrSize, recSize;
	//char *dir_b = {"./ligs"};
	readConfigFile_vs(configfile,param,metaheuristic,vectors);
	comprobation_params (param,metaheuristic);
	ruta (param.protein,param.input_dir_p);
	if ( param.scoring_function_type == S_AD4)
        {
                vectors->ac_x *= 10;
                vectors->ac_y *= 10;
                vectors->ac_z *= 10;
        }
	//sprintf(cadena,"%s/%s",param.input_params,param.forcefield);
    	//strcpy(param.forcefield,cadena);
	force_field_read (param.forcefield,vectors->weights,vectors->f_params,param.scoring_function_type);
	
	adjust_param_GPU(&param.mode);	
	
	//read_params(param.vdw_p,param.sasa_p,param.hbond_p,vectors->vdw_params,vectors->sasa_params,vectors->hbond_params);
	initProtein(param.protein, Protein, Bond, nBond, proteina.nrec,param.scoring_function_type);
	readProtein (Protein,proteina,qrSize,recSize);	
	
	extract_ligands_params(ligand_params,vectors,param);
	
	vectors->nlig += getPadding(vectors->nlig, 32);
	if (param.alarma)
	{
		alarma = param.duracion_alarma * 60;
		alarm(alarma);
		signal(SIGALRM,salir_alarma_vs);
	}	
	//printf("nlig %d\n",vectors->nlig);
	//exit(0);
	c_x = (type_data *)malloc(sizeof(type_data)*vectors->nlig*vectors->files);
	c_y = (type_data *)malloc(sizeof(type_data)*vectors->nlig*vectors->files);
	c_z = (type_data *)malloc(sizeof(type_data)*vectors->nlig*vectors->files);
	ql_tmp = (type_data *)malloc(sizeof(type_data)*vectors->nlig*vectors->files);
	ligtype_tmp = (char *) malloc(sizeof(char)*vectors->nlig*vectors->files);
	subtype_tmp = (char *) malloc(sizeof(char)*vectors->nlig*SUBTYPEMAXLEN*vectors->files);
	bonds_tmp = (unsigned int *)malloc(sizeof(unsigned int)*vectors->nlig*MAXBOND*vectors->files);
	nbonds_tmp = (char *)calloc(vectors->nlig*vectors->files,sizeof(char));
	//exit(0);	
	for (unsigned int i=0;i < vectors->files;i++)
	{
		//printf("%s\n",ligand_params[i].file);	
		readLigand_vs (ligand_params[i].file,ligand_params[i].type_json,vectors->nlig,c_x + i*vectors->nlig,c_y + i*vectors->nlig,c_z + i*vectors->nlig,ligtype_tmp + i*vectors->nlig,subtype_tmp + i*vectors->nlig*SUBTYPEMAXLEN,nbonds_tmp + i*vectors->nlig,bonds_tmp + i*vectors->nlig*MAXBOND,ql_tmp + i*vectors->nlig,param.scoring_function_type);	
		
	}
	//exit(0);	
	if (param.auto_flex == 1)
	{
		flexibility_params = (flexibility_params_t *)malloc(sizeof(flexibility_params_t)*vectors->files);
		for (unsigned int i=0;i < vectors->files;i++)
		{
			//printf("%s\n",ligand_params[i].file);
			ligand_babel (param,ligand_params[i].file,&flexibility_params[i],param.level_bonds_VDW,param.level_bonds_ES);
		}
	}	
	//printf("PARAM MODE %d\n",param.mode);
	/*for (int h=0;h<vectors->files;h++)
	{
		int r=0;
		printf("FILE %d nlinks %d \n",h,flexibility_params[h].n_links);
		for (int k=0;k<flexibility_params[h].n_links;k++)
		{
			printf("\tLINK %d ** %d - %d\n",k,flexibility_params[h].links[r],flexibility_params[h].links[r+1]);
			r=r+2;
		}
	}*/
	//exit(0);
	switch (param.mode) {
		
		case 0:
			printf("\nGPU SOLVER MODE...\n");
			printf("******************\n");					
			genera_gpu(&devices,param.ngpu,metaheuristic);
			//exit(0);
			error = 0;
                        //printf("devices->id[0] %d\n",devices.id[0]);
                        sprintf(str,"%d",param.ngpu);
                        strcat(param.gpu,"_");
                        strcat(param.gpu,str);
                        strcat(param.gpu,"\0");
                        if ((param.optimizacion) && ((fp = fopen(param.gpu,"r")) == NULL))
                        {
                                twarm_up = wtime();
				warm_up_bloques_gpu_vs (c_x,c_y,c_z,ligand_params,ql_tmp,ligtype_tmp,subtype_tmp,bonds_tmp,nbonds_tmp,proteina,vectors,&devices,vectors->weights,vectors->f_params,param,metaheuristic,param.conf_warm_up_gpu,0);
                                twarm_up_t = wtime() - twarm_up;
                                escribir_gpu (param,&metaheuristic,&devices);
                                //printf("Threadsperblock1Ini: %d\n",metaheuristic.Threadsperblock1Ini);
                        }
                        else
                        {
                                if (param.optimizacion)
                                {
                                        readGpuOptimizationFile(param.gpu,param.ngpu,metaheuristic,&devices,&error);
					comprobation_params (param,metaheuristic);
                                        if (error)
                                        {
                                                twarm_up = wtime();
                                                warm_up_bloques_gpu_vs (c_x,c_y,c_z,ligand_params,ql_tmp,ligtype_tmp,subtype_tmp,bonds_tmp,nbonds_tmp,proteina,vectors,&devices,vectors->weights,vectors->f_params,param,metaheuristic,param.conf_warm_up_gpu,0);
                                                twarm_up_t = wtime() - twarm_up;
                                                escribir_gpu (param,&metaheuristic,&devices);
                                        }
                                }
                        }
			break;
		case 1:
                        printf("\nSEQUENTIAL MODE...\n");
                        printf("******************\n");
                        break;
                case 2:
                        printf("\nOPEN-MP MODE...\n");
                        printf("***************\n");
                        error = 0;
                        if ((param.optimizacion) && ((fp = fopen(param.openmp,"r")) == NULL))
                        {
                                twarm_up = wtime();
                                warm_up_openmp_vs (c_x,c_y,c_z,ligand_params,ql_tmp,ligtype_tmp,subtype_tmp,bonds_tmp,nbonds_tmp,proteina,vectors,&vectors_o,vectors->weights,vectors->f_params,param.conf_warm_up_cpu,param,&metaheuristic);
                                twarm_up_t = wtime() - twarm_up;
                        }
                        else
                        {
                                if (param.optimizacion)
                                {
                                        readOpenmpOptimizationFile(param.openmp,metaheuristic,&error);
					comprobation_params (param,metaheuristic);
                                        if (error)
                                        {
                                                twarm_up = wtime();
                                                warm_up_openmp_vs (c_x,c_y,c_z,ligand_params,ql_tmp,ligtype_tmp,subtype_tmp,bonds_tmp,nbonds_tmp,proteina,vectors,&vectors_o,vectors->weights,vectors->f_params,param.conf_warm_up_cpu,param,&metaheuristic);
                                                twarm_up_t = wtime() - twarm_up;
                                        }
                                }
                        }
                        //exit(0);
                        break;
		case 3:
			printf("\nMULTI-GPU MODE...\n");
			printf("*****************\n");
			twarm_up = wtime();			
			warm_up_devices_vs (c_x,c_y,c_z,ligand_params,ql_tmp,ligtype_tmp,subtype_tmp,bonds_tmp,nbonds_tmp,proteina,vectors,&devices,vectors->weights,vectors->f_params,param,metaheuristic,param.conf_warm_up_gpu,&number_devices);	
			twarm_up_t = wtime() - twarm_up;
			//exit(0);
			break;	
	}
	
	switch (param.auto_flex) {
		
		case 0:  //Rigid
			switch (param.mode) {
				case 0:
				case 1:
				case 2:
				case 3:							
					ttotal = wtime();	
					tinicializar = wtime();
					inicializar_vs(proteina,ligand_params,vectors,&vectors_ini,param,metaheuristic,&devices,c_x,c_y,c_z,ql_tmp,ligtype_tmp,subtype_tmp,nbonds_tmp,bonds_tmp);printf("Inicializado..\n");
					tinicializar_t = wtime() - tinicializar;
					//exit(0);
					//EXTRAER CONFORMACION
					/*int max = 10000;
					int n_c,shift;
					shift = vectors_ini.nconformations / vectors_ini.num_surface;
					for (int i=0;i<vectors_ini.num_surface;i++)
					if (vectors_ini.energy.energy[i*shift] < max)
					{
						max = vectors_ini.energy.energy[i*shift];
						n_c = i*shift;
					}
					printf("MAX %f , conf %d\n",max,n_c);
					configurar_ligandos_finales(proteina,ligando,&ligtemp,&vectors_ini,param,metaheuristic,n_c);
					//maximo(&vectors_ini,&max);	
					exit(0);*/
					
					while ((count < metaheuristic.NMIFin) && (improve < metaheuristic.NIRFin))
					{
						printf("%d pass RIGID in progress....\n",count+1);				
						tseleccion = wtime();
						seleccionar_vs(vectors_ini.nlig,&vectors_ini,&vectors_selec,metaheuristic,param);printf("Seleccionado..\n");		
						tseleccion_t += wtime() - tseleccion;
						//exit(0);
						//printf("\tTime Seleccionar: %lf seg\n",tseleccion_t);
						tcombinar = wtime ();
						switch (param.mode) {
							case 0:
								combinar_gpu_vs (proteina,ligand_params,&vectors_selec,&vectors_comb,metaheuristic,param,&devices,0);printf("Combinado..%f %f %f\n",vectors_comb.energy.energy[0],vectors_comb.energy.energy[1],vectors_comb.energy.energy[2]);
								//exit(0);
								break;
							case 1:
							case 2:
								combinar_vs(proteina,ligand_params,&vectors_selec,&vectors_comb,metaheuristic,param);printf("Combinado..\n");
								break;
							case 3:
								combinar_multigpu_vs (proteina,ligand_params,&vectors_selec,&vectors_comb,metaheuristic,param,&devices);printf("Combinado..\n");								
								break;
						}
						tcombinar_t += wtime() - tcombinar;	
						tmut = wtime();
                                                if (metaheuristic.PEMUCom > 0)
                                                {
                                                        seleccionar_mutar_vs (vectors_comb.nlig,&vectors_comb,&vectors_selec,param,metaheuristic);
                                                        switch (param.mode) {
                                                                case 0:
                                                                     	mutation_gpu_vs (proteina,param,ligand_params,metaheuristic,&devices,vectors_selec.weights,vectors_selec.f_params,vectors_selec.conformations_x,vectors_selec.conformations_y,vectors_selec.conformations_z,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z,vectors_selec.quat_x,vectors_selec.quat_y,vectors_selec.quat_z,vectors_selec.quat_w,vectors_selec.energy.energy,vectors_selec.energy.n_conformation,vectors_selec.ql,vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.nlig,vectors_selec.files,vectors_selec.nconformations,param.ngpu,0);printf("Mutado...\n");  
                                                                        break;
                                                                case 1:
                                                                case 2:
                                                                         mutation_cpp_vs (proteina,param,metaheuristic,ligand_params,vectors_selec.weights,vectors_selec.f_params,vectors_selec.conformations_x, vectors_selec.conformations_y, vectors_selec.conformations_z,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z,vectors_selec.quat_x,vectors_selec.quat_y,vectors_selec.quat_z,vectors_selec.quat_w,vectors_selec.energy.energy,vectors_selec.energy.n_conformation,vectors_selec.bonds,vectors_selec.ligtype,vectors_selec.ql,vectors_selec.nlig,vectors_selec.files,vectors_selec.nconformations);printf("Mutado...\n");
                                                                        break;
                                                                case 3:
                                                                        mutation_multigpu_vs (proteina,ligand_params,vectors_selec.files,vectors_selec.nlig,vectors_selec.conformations_x,vectors_selec.conformations_y,vectors_selec.conformations_z,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z,vectors_selec.quat_x,vectors_selec.quat_y,vectors_selec.quat_z,vectors_selec.quat_w,vectors_selec.energy.energy,vectors_selec.energy.n_conformation,vectors_selec.weights,vectors_selec.f_params,vectors_selec.ql,vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.nconformations,param,metaheuristic,&devices,2);printf("Mutado...\n");
                                                                        break;
                                                        }
                                                        //incluir_gpu_mejorar_vs(vectors_selec.nlig,&vectors_selec,&vectors_comb,param,metaheuristic);
							stride_e = vectors_selec.nconformations / vectors_selec.files;
							stride_s = vectors_comb.nconformations / vectors_comb.files;
							#pragma omp parallel for
							for (unsigned int i = 0; i < vectors_selec.files; i++)
								incluir_gpu_vs_by_step (vectors_comb.nlig,vectors_selec.move_x + (stride_e*i), vectors_selec.move_y + (stride_e*i), vectors_selec.move_z + (stride_e*i), vectors_selec.quat_x + (stride_e*i), vectors_selec.quat_y + (stride_e*i), vectors_selec.quat_z + (stride_e*i), vectors_selec.quat_w + (stride_e*i), vectors_selec.energy.energy + (stride_e*i), vectors_selec.energy.n_conformation + (stride_e*i), vectors_comb.move_x + (stride_s*i),  vectors_comb.move_y + (stride_s*i),  vectors_comb.move_z + (stride_s*i),  vectors_comb.quat_x + (stride_s*i),  vectors_comb.quat_y + (stride_s*i),  vectors_comb.quat_z + (stride_s*i),  vectors_comb.quat_w + (stride_s*i),  vectors_comb.energy.energy + (stride_s*i),  vectors_comb.energy.n_conformation + (stride_s*i), stride_e, stride_s, i,param,metaheuristic);
							thrust::sequence(vectors_comb.energy.n_conformation,vectors_comb.energy.n_conformation + vectors_comb.nconformations);
                                                }
                                                tmut_t += wtime() - tmut;
						if (metaheuristic.PEMSel > 0)
						{
							seleccionar_mejorar_vs (vectors_comb.nlig,&vectors_comb,&vectors_selec,param,metaheuristic);
							tmejora = wtime();
							//printf("Conf para mejorar %d\n",vectors_selec.nconformations);
							if (metaheuristic.NEEImp > 0)
							{
								switch (param.mode) {
                                                                	case 0:
										if (param.montecarlo)
                                                                        {
                                                                                montecarlo_vs_environment(proteina,ligand_params,param,&vectors_selec,metaheuristic,&devices,0,1);printf("Mejorado..\n");
                                                                        }
                                                                        else
                                                                        {
                                                                                gpu_mejorar_vs_environment (proteina,ligand_params,param,&vectors_selec,metaheuristic,&devices,0,vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.ql,1);printf("Mejorado..\n");
                                                                        }
                                                                        break;
									case 1:
                                                                	case 2:
                                                                        	if (param.montecarlo)
                                                                        	{
                                                                                	montecarlo_cpp_vs_environment (proteina,ligand_params,param,vectors_selec.weights, vectors_selec.f_params, vectors_selec.conformations_x, vectors_selec.conformations_y, vectors_selec.conformations_z, vectors_selec.move_x, vectors_selec.move_y, vectors_selec.move_z, vectors_selec.quat_x, vectors_selec.quat_y, vectors_selec.quat_z, vectors_selec.quat_w, vectors_selec.energy.energy, vectors_selec.energy.n_conformation,vectors_selec.ql,vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.nlig,vectors_selec.files,vectors_selec.nconformations,metaheuristic,1);printf("Mejorado..\n");
                                                                        	}
                                                                        	else
                                                                        	{
                                                                                	mejorar_vs_environment (proteina,ligand_params,param,vectors_selec.weights, vectors_selec.f_params, vectors_selec.conformations_x,vectors_selec.conformations_y,vectors_selec.conformations_z,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z,vectors_selec.quat_x,vectors_selec.quat_y,vectors_selec.quat_z,vectors_selec.quat_w,vectors_selec.energy.energy,vectors_selec.energy.n_conformation,vectors_selec.ql,vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.nlig,vectors_selec.files,vectors_selec.nconformations,metaheuristic,1);printf("Mejorado..\n");
                                                                        	}
                                                                        	break;
									case 3:
										mejorar_multigpu_vs_environment (proteina,ligand_params,param,&vectors_selec,metaheuristic,&devices,1);printf("Mejorado..\n");
										break;
								}
							
							}
							else
							{
								switch (param.mode) {
								case 0:
									if (param.montecarlo)
									{
										montecarlo_vs(proteina,ligand_params,param,&vectors_selec,metaheuristic,&devices,0,1);printf("Mejorado..\n");
									}
									else
									{	
										gpu_mejorar_vs (proteina,ligand_params,param,&vectors_selec,metaheuristic,&devices,0,vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.ql,1);printf("Mejorado..\n");
									}
									break;
								case 1:
								case 2:
									if (param.montecarlo)
									{
										montecarlo_cpp_vs(proteina,ligand_params,param,vectors_selec.weights, vectors_selec.f_params, vectors_selec.conformations_x, vectors_selec.conformations_y, vectors_selec.conformations_z, vectors_selec.move_x, vectors_selec.move_y, vectors_selec.move_z, vectors_selec.quat_x, vectors_selec.quat_y, vectors_selec.quat_z, vectors_selec.quat_w, vectors_selec.energy.energy, vectors_selec.energy.n_conformation,vectors_selec.ql,vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.nlig,vectors_selec.files,vectors_selec.nconformations,metaheuristic,1);printf("Mejorado..\n");
									}
									else
									{
										mejorar_vs(proteina,ligand_params,param,vectors_selec.weights, vectors_selec.f_params, vectors_selec.conformations_x,vectors_selec.conformations_y,vectors_selec.conformations_z,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z,vectors_selec.quat_x,vectors_selec.quat_y,vectors_selec.quat_z,vectors_selec.quat_w,vectors_selec.energy.energy,vectors_selec.energy.n_conformation,vectors_selec.ql,vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.nlig,vectors_selec.files,vectors_selec.nconformations,metaheuristic,1);printf("Mejorado..\n");
									}
									break;
								case 3:	
									mejorar_multigpu_vs (proteina,ligand_params,param,&vectors_selec,metaheuristic,&devices,1);printf("Mejorado..\n");
								
									break;
								}
							}
							//incluir_gpu_mejorar_vs (vectors_comb.nlig,&vectors_selec,&vectors_comb,param,metaheuristic);
							stride_e = vectors_selec.nconformations / vectors_selec.files;
                                                        stride_s = vectors_comb.nconformations / vectors_comb.files;
                                                        #pragma omp parallel for
                                                        for (unsigned int i = 0; i < vectors_selec.files; i++)
                                                                incluir_gpu_vs_by_step (vectors_comb.nlig,vectors_selec.move_x + (stride_e*i), vectors_selec.move_y + (stride_e*i), vectors_selec.move_z + (stride_e*i), vectors_selec.quat_x + (stride_e*i), vectors_selec.quat_y + (stride_e*i), vectors_selec.quat_z + (stride_e*i), vectors_selec.quat_w + (stride_e*i), vectors_selec.energy.energy + (stride_e*i), vectors_selec.energy.n_conformation + (stride_e*i), vectors_comb.move_x + (stride_s*i),  vectors_comb.move_y + (stride_s*i),  vectors_comb.move_z + (stride_s*i),  vectors_comb.quat_x + (stride_s*i),  vectors_comb.quat_y + (stride_s*i),  vectors_comb.quat_z + (stride_s*i),  vectors_comb.quat_w + (stride_s*i),  vectors_comb.energy.energy + (stride_s*i),  vectors_comb.energy.n_conformation + (stride_s*i), stride_e, stride_s, i,param,metaheuristic);
                                                        thrust::sequence(vectors_comb.energy.n_conformation,vectors_comb.energy.n_conformation + vectors_comb.nconformations);
							tmejora_t += wtime() - tmejora;	
							//printf("Conformaciones combinadas %d\n",vectors_comb.nconformations);	
							//for (int l=0;l < vectors_comb.nconformations;l++)
							//	printf("Conformation: %d , move_x %f, move_y %f, move_z %f, quat.x %f, energy %f\n",vectors_comb.energy.n_conformation[l],vectors_comb.move_x[l], vectors_comb.move_y[l],vectors_comb.move_z[l],vectors_comb.quat_x[l],vectors_comb.energy.energy[l]);
							//exit(0);	
						}					
						tincluir = wtime();
						incluir_vs(vectors_ini.nlig,&vectors_comb,&vectors_ini,metaheuristic,param);printf("Incluido..\n");
						tincluir_t += wtime() - tincluir;
						/*limpiar(&vectors_comb,&vectors_selec);
						printf("Max: %f, Improve: %d\n",max,improve);*/				
						count++;
					}
					ttotal_t = wtime() - ttotal;
					printf("\n**PHASES VS RIGID**:\n");
					if (!param.optimizacion)
					{
						printf("\t*****************\n");
						printf("\tWARM_UP: %f seg\n",twarm_up_t);
						printf("\t*****************\n");
					}
					printf("\tTime Inicializar VS: %f seg\n",tinicializar_t);
					printf("\tTime Seleccionar VS: %f seg\n",tseleccion_t);
					printf("\tTime Combinar VS: %f seg\n",tcombinar_t);
					if (metaheuristic.PEMSel > 0) printf("\tTime Mejorar VS: %lf seg\n",tmejora_t);
					if (metaheuristic.PEMUCom > 0) printf("\tTime Mutation: %lf seg\n",tmut_t);
					printf("\tTime Incluir VS: %f seg\n",tincluir_t);			
					printf("\tTime Total VS: %f seg\n",ttotal_t);
				break;
					
			}
			break;
		case 1:  //Flex
			switch (param.mode) {
				case 0:
				case 1:
				case 2:
				case 3:							
					ttotal = wtime();	
					tinicializar = wtime();
					inicializar_vs_flex(proteina,ligand_params,vectors,&vectors_ini,param,metaheuristic,&devices,c_x,c_y,c_z,ql_tmp,ligtype_tmp,subtype_tmp,nbonds_tmp,bonds_tmp,flexibility_params,flexibility_conformations);printf("Inicializar..\n");// %f %f %f\n",vectors_ini.energy.energy[0],vectors_ini.energy.energy[1],vectors_ini.energy.energy[2]);
					tinicializar_t = wtime() - tinicializar;
					
					//break;
					while ((count < metaheuristic.NMIFin) && (improve < metaheuristic.NIRFin))
					{
						printf("%d pass FLEX in progress....\n",count+1);				
						tseleccion = wtime();
						seleccionar_vs(vectors_ini.nlig,&vectors_ini,&vectors_selec,metaheuristic,param);printf("Seleccionado..\n");//%f %f %f\n",vectors_selec.energy.energy[0],vectors_selec.energy.energy[1],vectors_selec.energy.energy[2]);		
						tseleccion_t += wtime() - tseleccion;
						//exit(0);
						//printf("\tTime Seleccionar: %lf seg\n",tseleccion_t);
						tcombinar = wtime ();
						switch (param.mode) {
							case 0:
								combinar_gpu_vs_flex (proteina,ligand_params,&vectors_selec,&vectors_comb,metaheuristic,param,&devices,flexibility_params,0);printf("Combinado..%f %f %f\n",vectors_comb.energy.energy[0],vectors_comb.energy.energy[1],vectors_comb.energy.energy[2]);
								//exit(0);
								break;
							case 1:
							case 2:
								combinar_vs_flex(proteina,ligand_params,&vectors_selec,&vectors_comb,metaheuristic,param,flexibility_params,flexibility_conformations);printf("Combinado..\n");
								break;
							case 3:
								combinar_multigpu_vs_flex (proteina,ligand_params,&vectors_selec,&vectors_comb,metaheuristic,param,&devices,flexibility_params);printf("Combinado..\n");								
								//break;
						}
						tcombinar_t += wtime() - tcombinar;
						//printf("Conformaciones combinadas %d\n",vectors_comb.nconformations);
                                                //for (int l=0;l < vectors_comb.nconformations;l++)
                                                 //     printf("Conformation: %d , move_x %f, move_y %f, move_z %f, quat.x %f, energy %f\n",vectors_comb.energy.n_conformation[l],vectors_comb.move_x[l], vectors_comb.move_y[l],vectors_comb.move_z[l],vectors_comb.quat_x[l],vectors_comb.energy.energy[l]);
                                                //exit(0);
						 tmut = wtime();
                                                if (metaheuristic.PEMUCom > 0)
                                                {
                                                        seleccionar_mutar_vs (vectors_comb.nlig,&vectors_comb,&vectors_selec,param,metaheuristic);
                                                        switch (param.mode) {
                                                                case 0:
                                                                        mutation_gpu_vs_flex (proteina,param,ligand_params,flexibility_params,metaheuristic,&devices,vectors_selec.weights,vectors_selec.f_params,vectors_selec.conformations_x,vectors_selec.conformations_y,vectors_selec.conformations_z,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z,vectors_selec.quat_x,vectors_selec.quat_y,vectors_selec.quat_z,vectors_selec.quat_w,vectors_selec.energy.energy,vectors_selec.energy.n_conformation, vectors_selec.ql,vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.nlig,vectors_selec.files,vectors_selec.nconformations,0,0);printf("Mutado...\n");
                                                                        break;
                                                                case 1:
                                                                case 2:
                                                                        mutation_cpp_vs_flex (proteina,param,metaheuristic,ligand_params,flexibility_params,flexibility_conformations,vectors_selec.weights,vectors_selec.f_params,vectors_selec.conformations_x,vectors_selec.conformations_y,vectors_selec.conformations_z,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z,vectors_selec.quat_x,vectors_selec.quat_y,vectors_selec.quat_z,vectors_selec.quat_w,vectors_selec.energy.energy,vectors_selec.energy.n_conformation,vectors_selec.bonds,vectors_selec.ligtype,vectors_selec.ql,vectors_selec.nlig,vectors_selec.files,vectors_selec.nconformations);printf("Mutado...\n");
                                                                        break;
                                                                case 3:
                                                                        mutation_multigpu_vs_flex (proteina,flexibility_params,ligand_params,vectors_selec.files,vectors_selec.nlig,vectors_selec.conformations_x,vectors_selec.conformations_y,vectors_selec.conformations_z,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z,vectors_selec.quat_x,vectors_selec.quat_y,vectors_selec.quat_z,vectors_selec.quat_w,vectors_selec.energy.energy,vectors_selec.energy.n_conformation,vectors_selec.weights,vectors_selec.f_params,vectors_selec.ql,vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.nconformations,param,metaheuristic,&devices,2);printf("Mutado...\n");
									break;
                                                        }
                                                        //incluir_gpu_vs_flex(vectors_selec.nlig,&vectors_selec,&vectors_comb,param,metaheuristic);
							stride_e = vectors_selec.nconformations / vectors_selec.files;
							stride_s = vectors_comb.nconformations / vectors_comb.files;
                                			#pragma omp parallel for
                                			for (unsigned int i = 0; i < vectors_selec.files; i++)
							{
								struct vectors_t v;
                                        			incluir_gpu_vs_flex_by_step (vectors_selec.nlig,vectors_selec.conformations_x + (stride_e*vectors_selec.nlig*i), vectors_selec.conformations_y + (stride_e*vectors_selec.nlig*i), vectors_selec.conformations_z + (stride_e*vectors_selec.nlig*i), vectors_selec.move_x + (stride_e*i), vectors_selec.move_y + (stride_e*i), vectors_selec.move_z + (stride_e*i), vectors_selec.quat_x + (stride_e*i), vectors_selec.quat_y + (stride_e*i), vectors_selec.quat_z + (stride_e*i), vectors_selec.quat_w + (stride_e*i), vectors_selec.energy.energy + (stride_e*i), vectors_selec.energy.n_conformation + (stride_e*i), vectors_comb.conformations_x + (stride_s*vectors_comb.nlig*i),  vectors_comb.conformations_y + (stride_s*vectors_comb.nlig*i),  vectors_comb.conformations_z + (stride_s*vectors_comb.nlig*i), vectors_comb.move_x + (stride_s*i),  vectors_comb.move_y + (stride_s*i),  vectors_comb.move_z + (stride_s*i),  vectors_comb.quat_x + (stride_s*i),  vectors_comb.quat_y + (stride_s*i),  vectors_comb.quat_z + (stride_s*i),  vectors_comb.quat_w + (stride_s*i),  vectors_comb.energy.energy + (stride_s*i),  vectors_comb.energy.n_conformation + (stride_s*i), stride_e, stride_s, i,param,metaheuristic);
							}
                                			thrust::sequence(vectors_comb.energy.n_conformation,vectors_comb.energy.n_conformation + vectors_comb.nconformations);
							//printf("Conformaciones combinadas %d\n",vectors_comb.nconformations);
                                                        //for (int l=0;l < vectors_comb.nconformations;l++)
                                                        //      printf("Conformation: %d , move_x %f, move_y %f, move_z %f, quat.x %f, energy %f\n",vectors_comb.energy.n_conformation[l],vectors_comb.move_x[l], vectors_comb.move_y[l],vectors_comb.move_z[l],vectors_comb.quat_x[l],vectors_comb.energy.energy[l]);
                                                        //exit(0);
                                                }
                                                tmut_t += wtime() - tmut; 

						if (metaheuristic.PEMSel > 0)
						{
							//exit(0);
							seleccionar_mejorar_vs (vectors_comb.nlig,&vectors_comb,&vectors_selec,param,metaheuristic);
							tmejora = wtime();
							//printf("Conf para mejorar %d\n",vectors_selec.nconformations);
							//exit(0);
							if (metaheuristic.NEEImp > 0)
							{
								switch (param.mode) {
                                                                        case 0:
										if (param.montecarlo)
                                                                                {
                                                                                        montecarlo_vs_flex_environment(proteina,ligand_params,param,&vectors_selec,metaheuristic,&devices,flexibility_params,0,1);printf("Mejorado..\n");
                                                                                }
                                                                                else
                                                                                {
                                                                                        gpu_mejorar_vs_flex_environment (proteina,ligand_params,flexibility_params, param,&vectors_selec,metaheuristic,&devices,0,vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.ql,1);printf("Mejorado..\n");
                                                                                }
                                                                                break;
									case 1:
                                                                        case 2:
                                                                                if (param.montecarlo)
                                                                                {
                                                                                        montecarlo_cpp_vs_flex_environment (proteina,ligand_params,param,vectors_selec.weights, vectors_selec.f_params,vectors_selec.conformations_x, vectors_selec.conformations_y, vectors_selec.conformations_z, vectors_selec.move_x, vectors_selec.move_y, vectors_selec.move_z, vectors_selec.quat_x, vectors_selec.quat_y, vectors_selec.quat_z, vectors_selec.quat_w, vectors_selec.energy.energy, vectors_selec.energy.n_conformation,vectors_selec.ql,vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.nlig,vectors_selec.files,vectors_selec.nconformations,metaheuristic,flexibility_params,flexibility_conformations,1);printf("Mejorado..\n");
                                                                                }
                                                                                else
                                                                                {
                                                                                        mejorar_vs_flex_environment (proteina,ligand_params,param,vectors_selec.weights, vectors_selec.f_params, vectors_selec.conformations_x, vectors_selec.conformations_y, vectors_selec.conformations_z, vectors_selec.move_x, vectors_selec.move_y, vectors_selec.move_z, vectors_selec.quat_x, vectors_selec.quat_y, vectors_selec.quat_z, vectors_selec.quat_w, vectors_selec.energy.energy, vectors_selec.energy.n_conformation,vectors_selec.ql,vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.nlig,vectors_selec.files,vectors_selec.nconformations,metaheuristic,flexibility_params,flexibility_conformations,1);printf("Mejorado..\n");
                                                                                }
                                                                                break;
									case 3:
										mejorar_multigpu_vs_flex_environment (proteina,ligand_params,flexibility_params,param,&vectors_selec,metaheuristic,&devices,1);printf("Mejorado..\n");
										break;
								}
							}
							else
							{
								switch (param.mode) {
									case 0:
										if (param.montecarlo)
										{								
											montecarlo_vs_flex(proteina,ligand_params,param,&vectors_selec,metaheuristic,&devices,flexibility_params,0,1);printf("Mejorado..\n");
										}
										else
										{		
											gpu_mejorar_vs_flex (proteina,ligand_params,flexibility_params, param,&vectors_selec,metaheuristic,&devices,0,vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.ql,1);printf("Mejorado..\n");
										}				
										break;
									case 1:
									case 2:
										if (param.montecarlo)
										{
											montecarlo_cpp_vs_flex(proteina,ligand_params,param,vectors_selec.weights, vectors_selec.f_params,vectors_selec.conformations_x, vectors_selec.conformations_y, vectors_selec.conformations_z, vectors_selec.move_x, vectors_selec.move_y, vectors_selec.move_z, vectors_selec.quat_x, vectors_selec.quat_y, vectors_selec.quat_z, vectors_selec.quat_w, vectors_selec.energy.energy, vectors_selec.energy.n_conformation,vectors_selec.ql,vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.nlig,vectors_selec.files,vectors_selec.nconformations,metaheuristic,flexibility_params,flexibility_conformations,1);printf("Mejorado..\n");
										}
										else
										{
											mejorar_vs_flex(proteina,ligand_params,param,vectors_selec.weights, vectors_selec.f_params, vectors_selec.conformations_x, vectors_selec.conformations_y, vectors_selec.conformations_z, vectors_selec.move_x, vectors_selec.move_y, vectors_selec.move_z, vectors_selec.quat_x, vectors_selec.quat_y, vectors_selec.quat_z, vectors_selec.quat_w, vectors_selec.energy.energy, vectors_selec.energy.n_conformation,vectors_selec.ql,vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.nlig,vectors_selec.files,vectors_selec.nconformations,metaheuristic,flexibility_params,flexibility_conformations,1);printf("Mejorado..\n");
										}
										break;
									case 3:
										mejorar_multigpu_vs_flex (proteina,ligand_params,flexibility_params,param,&vectors_selec,metaheuristic,&devices,1);printf("Mejorado..\n");
										break;
								}
							}
							//incluir_gpu_vs_flex (vectors_comb.nlig,&vectors_selec,&vectors_comb,param,metaheuristic);
							stride_e = vectors_selec.nconformations / vectors_selec.files;
                                                        stride_s = vectors_comb.nconformations / vectors_comb.files;
                                                        #pragma omp parallel for
                                                        for (unsigned int i = 0; i < vectors_selec.files; i++)
							{
								struct vectors_t v;
                                                                incluir_gpu_vs_flex_by_step (vectors_selec.nlig,vectors_selec.conformations_x + (stride_e*vectors_selec.nlig*i), vectors_selec.conformations_y + (stride_e*vectors_selec.nlig*i), vectors_selec.conformations_z + (stride_e*vectors_selec.nlig*i), vectors_selec.move_x + (stride_e*i), vectors_selec.move_y + (stride_e*i), vectors_selec.move_z + (stride_e*i), vectors_selec.quat_x + (stride_e*i), vectors_selec.quat_y + (stride_e*i), vectors_selec.quat_z + (stride_e*i), vectors_selec.quat_w + (stride_e*i), vectors_selec.energy.energy + (stride_e*i), vectors_selec.energy.n_conformation + (stride_e*i), vectors_comb.conformations_x + (stride_s*vectors_comb.nlig*i),  vectors_comb.conformations_y + (stride_s*vectors_comb.nlig*i),  vectors_comb.conformations_z + (stride_s*vectors_comb.nlig*i), vectors_comb.move_x + (stride_s*i),  vectors_comb.move_y + (stride_s*i),  vectors_comb.move_z + (stride_s*i),  vectors_comb.quat_x + (stride_s*i),  vectors_comb.quat_y + (stride_s*i),  vectors_comb.quat_z + (stride_s*i),  vectors_comb.quat_w + (stride_s*i),  vectors_comb.energy.energy + (stride_s*i),  vectors_comb.energy.n_conformation + (stride_s*i), stride_e, stride_s, i,param,metaheuristic);
							}
                                                        thrust::sequence(vectors_comb.energy.n_conformation,vectors_comb.energy.n_conformation + vectors_comb.nconformations);
							tmejora_t += wtime() - tmejora;	
							//printf("Conformaciones combinadas %d\n",vectors_comb.nconformations);	
							//for (int l=0;l < vectors_comb.nconformations;l++)
							//	printf("Conformation: %d , move_x %f, move_y %f, move_z %f, quat.x %f, energy %f\n",vectors_comb.energy.n_conformation[l],vectors_comb.move_x[l], vectors_comb.move_y[l],vectors_comb.move_z[l],vectors_comb.quat_x[l],vectors_comb.energy.energy[l]);
							//exit(0);	
						}	
						tincluir = wtime();
						
						incluir_vs(vectors_ini.nlig,&vectors_comb,&vectors_ini,metaheuristic,param);printf("Incluido..\n");
						tincluir_t += wtime() - tincluir;
						count++;
					}
					ttotal_t = wtime() - ttotal;
					printf("\n**PHASES VS FLEX**:\n");
					if (!param.optimizacion)
					{
						printf("\t*****************\n");
						printf("\tWARM_UP: %f seg\n",twarm_up_t);
						printf("\t*****************\n");
					}
					printf("\tTime Inicializar VS: %f seg\n",tinicializar_t);
					printf("\tTime Seleccionar VS: %f seg\n",tseleccion_t);
					printf("\tTime Combinar VS: %f seg\n",tcombinar_t);
					if (metaheuristic.PEMSel > 0) printf("\tTime Mejorar VS: %lf seg\n",tmejora_t);
					if (metaheuristic.PEMUCom > 0) printf("\tTime Mutation: %lf seg\n",tmut_t);
					printf("\tTime Incluir VS: %f seg\n",tincluir_t);			
					printf("\tTime Total VS: %f seg\n",ttotal_t);
					break;
			}
			break;
	}
	configurar_ligandos_finales_vs (proteina,ligand_params,&vectors_ini,param,metaheuristic,flexibility_params);
	
}
