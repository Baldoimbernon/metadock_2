#ifndef __ENERGY_MUTATION_CPP_VS_FLEX_H
#define __ENERGY_MUTATION_CPP_VS_FLEX_H

#include "energy_struct.h"
extern void mutation_cpp_vs_flex (struct receptor_t proteina, struct param_t param, struct metaheuristic_t metaheuristic, struct ligand_params_t *ligand_params, struct flexibility_params_t *flexibility_params, struct flexibility_data_t *flexibility_conformations, type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int *bonds, char *ligtype, type_data *ql, unsigned int nlig, unsigned int files, unsigned int nconformations);

extern void mutation_calculation_cpp_vs_flex (struct param_t param, struct metaheuristic_t metaheuristic, struct flexibility_params_t *flexibility_params, unsigned int *n_update, int seed, int automatic_seed, type_data max_desp, type_data rotation, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, unsigned int nlig, unsigned int files, unsigned int nconformations);

extern void mutation_random_cpp_vs_flex (struct metaheuristic_t metaheuristic, unsigned int nconformations, struct param_t param, unsigned int *n_update);

#endif
