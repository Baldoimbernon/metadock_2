#ifndef FLEXIBILITY_VS_H
#define FLEXIBILITY_VS_H


extern void rotate3D_flexibility_cpp_vs (type_data  q_x, type_data  q_y, type_data  q_z, type_data  q_w, type_data* src, type_data * dst);
extern void setRotation_flexibility_cpp_vs (type_data *q_x, type_data *q_y, type_data *q_z, type_data *q_w, type_data angle, type_data * vector);
extern void rotateLigandFragment_cpp_vs (int *f, unsigned int size_f, type_data* lig_x, type_data* lig_y, type_data* lig_z, type_data qeje_x, type_data qeje_y, type_data qeje_z, type_data qeje_w, type_data p1[3]);

extern void flexibility_cpp_vs (unsigned int nconformations, unsigned int files, struct metaheuristic_t metaheuristic, struct param_t param, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, struct flexibility_data_t * flexibility_conformations, struct flexibility_params_t *flexibility_params, unsigned int nlig, unsigned int max_angle_flex);//, int n_links, int n_fragments, int *links, int *links_fragments, int *fragments, int *fragments_tam, int max_angle_flex);
extern void angulations_conformations_cpp_vs (unsigned int nconformations, unsigned int files, struct metaheuristic_t metaheuristic, struct param_t param, struct flexibility_data_t * flexibility_conformations, struct flexibility_params_t *flexibility_params, type_data max_angle_flex);
#endif
