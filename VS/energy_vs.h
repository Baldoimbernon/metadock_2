#ifndef __VS_ENERGY_H__
#define __VS_ENERGY_H__

extern void max_atoms_ligand (char *filename, unsigned int *atoms);
extern void vs_type (struct vectors_t *vectors, struct param_t param, struct metaheuristic_t metaheuristic, char *configfile);

#endif
