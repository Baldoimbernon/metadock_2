#include "energy_flexibility_vs.h"
#include "definitions.h"
#include "energy_positions.h"
#include "energy_kernel.h"
#include "energy_common-gpu.h"
#include "energy_common-gpu_vs.h"
#include "energy_moving.h"
#include "metaheuristic_inc.h"
#include "wtime.h"
#include "energy_positions_vs_flex.h"
#include "energy_common.h"
#include "energy_positions_common.h"
#include <thrust/sort.h>

extern void gpu_mejorar_vs_flex_environment (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct flexibility_params_t *flexibility_params, struct param_t param, struct vectors_t *vectors_e_s, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int orden_device, char *ligtype, unsigned int *bonds, type_data *ql, unsigned int fase)
{
	struct vectors_t vectors_mejora;

        size_t available,total_m;
        cudaError_t cudaStatus;
        unsigned long int max_memory;
        unsigned long int max_positions;
        unsigned long int max_common;
        unsigned long int max_conf;
        unsigned long int max_flex_conf;
        unsigned long int typeSize = proteina.nrec * sizeof(char);
        unsigned long int recSize = proteina.nrec * sizeof(type_data);
        unsigned long int bondSize = proteina.nrec * sizeof(unsigned int) * MAXBOND;

        unsigned long int randSize = vectors_e_s->nconformations * sizeof(curandState_t);
        unsigned long int moveSize_tam = vectors_e_s->nconformations * sizeof(type_data);
        unsigned long int energyConf_tam = vectors_e_s->nconformations * sizeof(unsigned int);
        unsigned long int confSize_tam = vectors_e_s->nconformations * vectors_e_s->nlig * sizeof(type_data);
        unsigned long int confSize_tam_char = vectors_e_s->nconformations * vectors_e_s->nlig * sizeof(char);
        unsigned long int confSize_bonds = vectors_e_s->nconformations * vectors_e_s->nlig * MAXBOND *  sizeof(unsigned int);
        unsigned long int flexConfSize =  vectors_e_s->nconformations * sizeof(struct flexibility_data_t);

        unsigned long int randSize_environment = vectors_e_s->nconformations * metaheuristic.NEEImp * sizeof(curandState_t);
        unsigned long int moveSize_tam_environment = vectors_e_s->nconformations * metaheuristic.NEEImp * sizeof(type_data);
        unsigned long int energyConf_tam_environment = vectors_e_s->nconformations * metaheuristic.NEEImp * sizeof(unsigned int);
        unsigned long int confSize_tam_environment = vectors_e_s->nconformations * vectors_e_s->nlig * metaheuristic.NEEImp * sizeof(type_data);
        unsigned long int confSize_tam_char_environment = vectors_e_s->nconformations * vectors_e_s->nlig * metaheuristic.NEEImp * sizeof(char);
        unsigned long int confSize_bonds_environment = vectors_e_s->nconformations * vectors_e_s->nlig * MAXBOND *  metaheuristic.NEEImp * sizeof(unsigned int);
        unsigned long int flexConfSize_environment =  vectors_e_s->nconformations * metaheuristic.NEEImp * sizeof(struct flexibility_data_t);

        unsigned long int flexSize = vectors_e_s->files *  sizeof(struct flexibility_params_t) + vectors_e_s->files * sizeof(type_data);
        unsigned long int linksSize = 0, linksfragmentsSize = 0, fragmentstamSize = 0, fragmentsSize = 0, boolSize = 0;

        unsigned long int tam_max, tam_tmp, nconformations_count_total, nconformations_count_partial, tam_conformation, stride_d;

        for (unsigned int i = 0; i < vectors_e_s->files; i++)
        {
                linksSize += flexibility_params[i].n_links * 2 * sizeof(int);
                linksfragmentsSize += flexibility_params[i].n_links * 2 * sizeof(int);
                fragmentstamSize += flexibility_params[i].n_fragments * sizeof(int);
                fragmentsSize += flexibility_params[i].n_links * (ligand_params[i].atoms - 2) * sizeof(int);
                boolSize += ligand_params[i].atoms * ligand_params[i].atoms * sizeof(bool);
        }

        max_positions = (moveSize_tam * 8) * 2 + moveSize_tam + energyConf_tam * 2 + (moveSize_tam_environment * 8) * 2 + moveSize_tam_environment + energyConf_tam_environment * 2;
        max_conf = (confSize_tam * 3) + randSize + confSize_tam + confSize_tam_char + confSize_bonds + flexConfSize + (confSize_tam_environment * 3) + randSize_environment + confSize_tam_environment + confSize_tam_char_environment + confSize_bonds_environment + flexConfSize_environment;
        max_common = (recSize * 4) + typeSize + bondSize;
        max_flex_conf =  flexSize + linksSize + linksfragmentsSize + fragmentstamSize + fragmentsSize + boolSize;
        max_memory = max_positions + max_conf + max_common + max_flex_conf;
        tam_conformation =  (sizeof(type_data) * 8 * 2) + sizeof(type_data) + sizeof(unsigned int) * 2 + (sizeof(type_data) * vectors_e_s->nlig * 3) +  sizeof(curandState_t) + vectors_e_s->nlig * sizeof(type_data) +  vectors_e_s->nlig * sizeof(char) +  vectors_e_s->nlig * MAXBOND *  sizeof(unsigned int) + (3 * ((sizeof(type_data) * 8 * 2) + sizeof(type_data) + sizeof(unsigned int) * 2 + (sizeof(type_data) * vectors_e_s->nlig * 3) +  sizeof(curandState_t) + vectors_e_s->nlig * sizeof(type_data) +  vectors_e_s->nlig * sizeof(char) +  vectors_e_s->nlig * MAXBOND *  sizeof(unsigned int)));

	vectors_mejora.files = vectors_e_s->files;
        vectors_mejora.nlig = vectors_e_s->nlig;
        vectors_mejora.nconformations = vectors_e_s->nconformations * metaheuristic.NEEImp;

        vectors_mejora.conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations*vectors_mejora.nlig);
        vectors_mejora.conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations*vectors_mejora.nlig);
        vectors_mejora.conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations*vectors_mejora.nlig);

        vectors_mejora.ql = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations*vectors_mejora.nlig);
        vectors_mejora.ligtype = (char *)malloc(sizeof(char)*vectors_mejora.nconformations*vectors_mejora.nlig);
        vectors_mejora.bonds = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations*vectors_mejora.nlig*MAXBOND);

        vectors_mejora.move_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.move_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.move_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations);
        vectors_mejora.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.weights = (type_data *)malloc(sizeof(type_data)*SCORING_TERMS);

        memcpy(vectors_mejora.f_params, vectors_e_s->f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_mejora.weights, vectors_e_s->weights,SCORING_TERMS * sizeof(type_data));

        fill_conformations_environment_vs (vectors_mejora.nlig,vectors_e_s->conformations_x,vectors_e_s->conformations_y,vectors_e_s->conformations_z,vectors_e_s->move_x,vectors_e_s->move_y,vectors_e_s->move_z,vectors_e_s->quat_x,vectors_e_s->quat_y,vectors_e_s->quat_z,vectors_e_s->quat_w,ligtype,ql,bonds,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,vectors_mejora.ligtype,vectors_mejora.ql,vectors_mejora.bonds,vectors_e_s->nconformations,param,metaheuristic);

        thrust::sequence(vectors_mejora.energy.n_conformation,vectors_mejora.energy.n_conformation + vectors_mejora.nconformations);

	cudaSetDevice(devices->id[orden_device]);
        //cudaDeviceReset();
        cudaMemGetInfo(&tam_max, &total_m);
        cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
        //std::cout << tam_max << " " << total_m << "\n";
        tam_tmp = tam_max - (tam_max * 0.2);
        if (tam_tmp < max_memory)
        {
                //printf("Trocear en 1 GPU\n");
                max_memory = max_common;
                nconformations_count_total = 0;
                nconformations_count_partial = 0;
                stride_d = 0;
                while (nconformations_count_total < vectors_e_s->nconformations)
                {
                        max_memory += tam_conformation;
                        if (max_memory > tam_tmp)
                        {
                                //printf("Trozo %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
                                //nconformations_count_total--;
                                //nconformations_count_partial--;
                                //Lanzar con nconformations_count_partial;
				multigpu_mejorar_gpusolver_vs_flex_environment (vectors_e_s->nconformations,vectors_mejora.files,vectors_mejora.nlig,vectors_e_s->ligtype + (stride_d * vectors_mejora.nlig), vectors_e_s->bonds + (stride_d * MAXBOND * vectors_mejora.nlig),vectors_e_s->ql + (stride_d * vectors_mejora.nlig),vectors_mejora.ligtype + (stride_d * metaheuristic.NEEImp * vectors_mejora.nlig),vectors_mejora.bonds + (stride_d * metaheuristic.NEEImp * MAXBOND * vectors_mejora.nlig),vectors_mejora.ql + (stride_d * metaheuristic.NEEImp * vectors_mejora.nlig),ligand_params,flexibility_params,proteina,param,vectors_mejora.weights,vectors_mejora.f_params,devices,vectors_e_s->conformations_x + (stride_d * vectors_mejora.nlig),vectors_e_s->conformations_y + (stride_d * vectors_mejora.nlig),vectors_e_s->conformations_z + (stride_d * vectors_mejora.nlig) ,vectors_e_s->move_x + stride_d, vectors_e_s->move_y + stride_d, vectors_e_s->move_z + stride_d, vectors_e_s->quat_x + stride_d, vectors_e_s->quat_y + stride_d, vectors_e_s->quat_z + stride_d, vectors_e_s->quat_w + stride_d, vectors_e_s->energy.energy + stride_d, vectors_e_s->energy.n_conformation + stride_d, vectors_mejora.conformations_x + (stride_d * metaheuristic.NEEImp * vectors_mejora.nlig), vectors_mejora.conformations_y + (stride_d * metaheuristic.NEEImp * vectors_mejora.nlig), vectors_mejora.conformations_z + (stride_d * metaheuristic.NEEImp * vectors_mejora.nlig), vectors_mejora.move_x + (stride_d * metaheuristic.NEEImp), vectors_mejora.move_y + (stride_d * metaheuristic.NEEImp), vectors_mejora.move_z + (stride_d * metaheuristic.NEEImp), vectors_mejora.quat_x + (stride_d * metaheuristic.NEEImp), vectors_mejora.quat_y + (stride_d * metaheuristic.NEEImp), vectors_mejora.quat_z + (stride_d * metaheuristic.NEEImp), vectors_mejora.quat_w + (stride_d * metaheuristic.NEEImp), vectors_mejora.energy.energy + (stride_d * metaheuristic.NEEImp), vectors_mejora.energy.n_conformation + (stride_d * metaheuristic.NEEImp), metaheuristic, nconformations_count_partial, nconformations_count_partial * metaheuristic.NEEImp, orden_device, stride_d, fase);
				stride_d += nconformations_count_partial;
                                max_memory = max_common;
                                nconformations_count_partial = 0;
                        }
                        else
                        {
                                nconformations_count_total++;
                                nconformations_count_partial++;
                        }
                }
                //printf("Trozo final %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
		 multigpu_mejorar_gpusolver_vs_flex_environment (vectors_e_s->nconformations,vectors_mejora.files,vectors_mejora.nlig,vectors_e_s->ligtype + (stride_d * vectors_mejora.nlig), vectors_e_s->bonds + (stride_d * MAXBOND * vectors_mejora.nlig),vectors_e_s->ql + (stride_d * vectors_mejora.nlig),vectors_mejora.ligtype + (stride_d * metaheuristic.NEEImp * vectors_mejora.nlig),vectors_mejora.bonds + (stride_d * metaheuristic.NEEImp * MAXBOND * vectors_mejora.nlig),vectors_mejora.ql + (stride_d * metaheuristic.NEEImp * vectors_mejora.nlig),ligand_params,flexibility_params,proteina,param,vectors_mejora.weights,vectors_mejora.f_params,devices,vectors_e_s->conformations_x + (stride_d * vectors_mejora.nlig),vectors_e_s->conformations_y + (stride_d * vectors_mejora.nlig),vectors_e_s->conformations_z + (stride_d * vectors_mejora.nlig) ,vectors_e_s->move_x + stride_d, vectors_e_s->move_y + stride_d, vectors_e_s->move_z + stride_d, vectors_e_s->quat_x + stride_d, vectors_e_s->quat_y + stride_d, vectors_e_s->quat_z + stride_d, vectors_e_s->quat_w + stride_d, vectors_e_s->energy.energy + stride_d, vectors_e_s->energy.n_conformation + stride_d, vectors_mejora.conformations_x + (stride_d * metaheuristic.NEEImp * vectors_mejora.nlig), vectors_mejora.conformations_y + (stride_d * metaheuristic.NEEImp * vectors_mejora.nlig), vectors_mejora.conformations_z + (stride_d * metaheuristic.NEEImp * vectors_mejora.nlig), vectors_mejora.move_x + (stride_d * metaheuristic.NEEImp), vectors_mejora.move_y + (stride_d * metaheuristic.NEEImp), vectors_mejora.move_z + (stride_d * metaheuristic.NEEImp), vectors_mejora.quat_x + (stride_d * metaheuristic.NEEImp), vectors_mejora.quat_y + (stride_d * metaheuristic.NEEImp), vectors_mejora.quat_z + (stride_d * metaheuristic.NEEImp), vectors_mejora.quat_w + (stride_d * metaheuristic.NEEImp), vectors_mejora.energy.energy + (stride_d * metaheuristic.NEEImp), vectors_mejora.energy.n_conformation + (stride_d * metaheuristic.NEEImp), metaheuristic, nconformations_count_partial, nconformations_count_partial * metaheuristic.NEEImp, orden_device, stride_d, fase);

	}
        else
        {
                //printf("No Trocea\n");
                //Lanzar con todo
		 multigpu_mejorar_gpusolver_vs_flex_environment (vectors_e_s->nconformations,vectors_mejora.files,vectors_mejora.nlig,vectors_e_s->ligtype,vectors_e_s->bonds,vectors_e_s->ql,vectors_mejora.ligtype,vectors_mejora.bonds,vectors_mejora.ql,ligand_params,flexibility_params,proteina,param,vectors_mejora.weights,vectors_mejora.f_params,devices,vectors_e_s->conformations_x,vectors_e_s->conformations_y,vectors_e_s->conformations_z,vectors_e_s->move_x, vectors_e_s->move_y, vectors_e_s->move_z, vectors_e_s->quat_x, vectors_e_s->quat_y, vectors_e_s->quat_z, vectors_e_s->quat_w, vectors_e_s->energy.energy, vectors_e_s->energy.n_conformation, vectors_mejora.conformations_x, vectors_mejora.conformations_y, vectors_mejora.conformations_z, vectors_mejora.move_x, vectors_mejora.move_y, vectors_mejora.move_z, vectors_mejora.quat_x , vectors_mejora.quat_y, vectors_mejora.quat_z, vectors_mejora.quat_w, vectors_mejora.energy.energy, vectors_mejora.energy.n_conformation, metaheuristic, vectors_e_s->nconformations, vectors_mejora.nconformations, orden_device, stride_d, fase);

	}
        free(vectors_mejora.conformations_x);
        free(vectors_mejora.conformations_y);
        free(vectors_mejora.conformations_z);
        free(vectors_mejora.move_x);
        free(vectors_mejora.move_y);
        free(vectors_mejora.move_z);
        free(vectors_mejora.quat_x);
        free(vectors_mejora.quat_y);
        free(vectors_mejora.quat_z);
        free(vectors_mejora.quat_w);
        free(vectors_mejora.ql);
        free(vectors_mejora.bonds);
        free(vectors_mejora.ligtype);
        free(vectors_mejora.energy.energy);
        free(vectors_mejora.energy.n_conformation);
        free(vectors_mejora.weights);
}

extern void gpu_mejorar_vs_flex (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct flexibility_params_t *flexibility_params, struct param_t param, struct vectors_t *vectors_e_s, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int orden_device, char *ligtype, unsigned int *bonds, type_data *ql, unsigned int fase)
{
	size_t available,total_m;
        cudaError_t cudaStatus;
        unsigned long int max_memory;
        unsigned long int max_positions;
        unsigned long int max_common;
        unsigned long int max_conf;
        unsigned long int max_flex_conf;
        unsigned long int typeSize = proteina.nrec * sizeof(char);
        unsigned long int recSize = proteina.nrec * sizeof(type_data);
        unsigned long int bondSize = proteina.nrec * sizeof(unsigned int) * MAXBOND;
        unsigned long int randSize = vectors_e_s->nconformations * sizeof(curandState_t);
        unsigned long int moveSize_tam = vectors_e_s->nconformations * sizeof(type_data);
        unsigned long int energyConf_tam = vectors_e_s->nconformations * sizeof(unsigned int);
        unsigned long int confSize_tam = vectors_e_s->nconformations * vectors_e_s->nlig * sizeof(type_data);
        unsigned long int confSize_tam_char = vectors_e_s->nconformations * vectors_e_s->nlig * sizeof(char);
        unsigned long int confSize_bonds = vectors_e_s->nconformations * vectors_e_s->nlig * MAXBOND *  sizeof(unsigned int);
        unsigned long int flexConfSize =  vectors_e_s->nconformations * sizeof(struct flexibility_data_t);
        unsigned long int flexSize = vectors_e_s->files *  sizeof(struct flexibility_params_t) + vectors_e_s->files * sizeof(type_data);
        unsigned long int linksSize = 0, linksfragmentsSize = 0, fragmentstamSize = 0, fragmentsSize = 0, boolSize = 0;
        unsigned long int tam_max, tam_tmp, nconformations_count_total, nconformations_count_partial, tam_conformation, stride_d;

        for (unsigned int i = 0; i < vectors_e_s->files; i++)
        {
                linksSize += flexibility_params[i].n_links * 2 * sizeof(int);
                linksfragmentsSize += flexibility_params[i].n_links * 2 * sizeof(int);
                fragmentstamSize += flexibility_params[i].n_fragments * sizeof(int);
                fragmentsSize += flexibility_params[i].n_links * (ligand_params[i].atoms - 2) * sizeof(int);
                boolSize += ligand_params[i].atoms * ligand_params[i].atoms * sizeof(bool);
        }

        max_positions = (moveSize_tam * 8) * 2 + moveSize_tam + energyConf_tam * 2;
        max_conf = (confSize_tam * 3) + randSize + confSize_tam + confSize_tam_char + confSize_bonds + flexConfSize;
        max_common = (recSize * 4) + typeSize + bondSize;
        max_flex_conf =  flexSize + linksSize + linksfragmentsSize + fragmentstamSize + fragmentsSize + boolSize;
        max_memory = max_positions + max_conf + max_common + max_flex_conf;
        tam_conformation =  (sizeof(type_data) * 8 * 2) + sizeof(type_data) + sizeof(unsigned int) * 2 + (sizeof(type_data) * vectors_e_s->nlig * 3) +  sizeof(curandState_t) + vectors_e_s->nlig * sizeof(type_data) +  vectors_e_s->nlig * sizeof(char) +  vectors_e_s->nlig * MAXBOND *  sizeof(unsigned int);

	cudaSetDevice(devices->id[orden_device]);
        cudaDeviceReset();
        cudaMemGetInfo(&tam_max, &total_m);
        cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
        std::cout << tam_max << " " << total_m << "\n";
        tam_tmp = tam_max - (tam_max * 0.2);
        if (tam_tmp < max_memory)
        {
                //printf("Trocear en 1 GPU\n");
                max_memory = max_common + max_flex_conf;
                nconformations_count_total = 0;
                nconformations_count_partial = 0;
                stride_d = 0;
                while (nconformations_count_total < vectors_e_s->nconformations)
                {
                        max_memory += tam_conformation;
                        if (max_memory > tam_tmp)
                        {
                                //printf("Trozo %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
                                //nconformations_count_total--;
                                //nconformations_count_partial--;
                                //Lanzar con nconformations_count_partial;
				multigpu_mejorar_gpusolver_vs_flex (vectors_e_s->nconformations,vectors_e_s->files,vectors_e_s->nlig,ligtype+(stride_d*vectors_e_s->nlig),bonds+(stride_d*vectors_e_s->nlig*MAXBOND),ql+(stride_d*vectors_e_s->nlig),ligand_params,flexibility_params,proteina,param,vectors_e_s->weights,vectors_e_s->f_params,devices,vectors_e_s->conformations_x+(stride_d*vectors_e_s->nlig),vectors_e_s->conformations_y+(stride_d*vectors_e_s->nlig),vectors_e_s->conformations_z+(stride_d*vectors_e_s->nlig),vectors_e_s->move_x+stride_d,vectors_e_s->move_y+stride_d,vectors_e_s->move_z+stride_d,vectors_e_s->quat_x+stride_d,vectors_e_s->quat_y+stride_d,vectors_e_s->quat_z+stride_d,vectors_e_s->quat_w+stride_d,vectors_e_s->energy.energy+stride_d,vectors_e_s->energy.n_conformation+stride_d,metaheuristic,nconformations_count_partial,orden_device,stride_d,fase);
				stride_d += nconformations_count_partial;
                                max_memory = max_common + max_flex_conf;
                                nconformations_count_partial = 0;
                        }
                        else
                        {
                                nconformations_count_total++;
                                nconformations_count_partial++;
                        }
                }
                //printf("Trozo final %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
		nconformations_count_partial--;
		multigpu_mejorar_gpusolver_vs_flex (vectors_e_s->nconformations,vectors_e_s->files,vectors_e_s->nlig,ligtype+(stride_d*vectors_e_s->nlig),bonds+(stride_d*vectors_e_s->nlig*MAXBOND),ql+(stride_d*vectors_e_s->nlig),ligand_params,flexibility_params,proteina,param,vectors_e_s->weights,vectors_e_s->f_params,devices,vectors_e_s->conformations_x+(stride_d*vectors_e_s->nlig),vectors_e_s->conformations_y+(stride_d*vectors_e_s->nlig),vectors_e_s->conformations_z+(stride_d*vectors_e_s->nlig),vectors_e_s->move_x+stride_d,vectors_e_s->move_y+stride_d,vectors_e_s->move_z+stride_d,vectors_e_s->quat_x+stride_d,vectors_e_s->quat_y+stride_d,vectors_e_s->quat_z+stride_d,vectors_e_s->quat_w+stride_d,vectors_e_s->energy.energy+stride_d,vectors_e_s->energy.n_conformation+stride_d,metaheuristic,nconformations_count_partial,orden_device,stride_d,fase);
	}
        else
        {
                //printf("No Trocea\n");
                //Lanzar montecarlo con todo
		multigpu_mejorar_gpusolver_vs_flex (vectors_e_s->nconformations,vectors_e_s->files,vectors_e_s->nlig,ligtype,bonds,ql,ligand_params,flexibility_params,proteina,param,vectors_e_s->weights,vectors_e_s->f_params,devices,vectors_e_s->conformations_x,vectors_e_s->conformations_y,vectors_e_s->conformations_z,vectors_e_s->move_x,vectors_e_s->move_y,vectors_e_s->move_z,vectors_e_s->quat_x,vectors_e_s->quat_y,vectors_e_s->quat_z,vectors_e_s->quat_w,vectors_e_s->energy.energy,vectors_e_s->energy.n_conformation,metaheuristic,vectors_e_s->nconformations,orden_device,0,fase);
	}
	//exit(0);
	
}

extern void multigpu_mejorar_gpusolver_vs_flex_environment (unsigned int total_conformations, unsigned int files, unsigned int nlig, char* ligtype, unsigned int *bonds, type_data *ql, char* ligtype_mejora, unsigned int *bonds_mejora, type_data *ql_mejora, struct ligand_params_t *ligand_params, struct flexibility_params_t *flexibility_params, struct receptor_t proteina, struct param_t param, type_data *weights, struct force_field_param_t *f_params, struct device_t *devices, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *n_conformation, type_data *conformations_x_mejora, type_data *conformations_y_mejora, type_data *conformations_z_mejora, type_data *move_x_mejora, type_data *move_y_mejora, type_data *move_z_mejora, type_data *quat_x_mejora, type_data *quat_y_mejora, type_data *quat_z_mejora, type_data *quat_w_mejora, type_data *energy_mejora, unsigned int *n_conformation_mejora, struct metaheuristic_t metaheuristic, unsigned int nconformations, unsigned int nconformations_mejora, unsigned int orden_device, unsigned int stride_d, unsigned int fase)
{

        curandState_t *states_d_mejora;
        struct receptor_t *proteina_d;
        struct flexibility_params_t *flexibility_params_d;
        struct flexibility_data_t *flexibility_conformations_d_mejora;
        type_data *internal_energy, *internal_energy_d, *moves_x_d, *moves_y_d, *moves_z_d, *energy_d, *ql_d, *ql_d_mejora;
        type_data *quat_x_d, *quat_y_d, *quat_z_d, *quat_w_d, *quat_x_d_mejora, *quat_y_d_mejora, *quat_z_d_mejora, *quat_w_d_mejora;
        type_data *moves_x_d_mejora, *moves_y_d_mejora, *moves_z_d_mejora, *energy_d_mejora;
        unsigned int *energy_nconformation_d, *energy_nconformation_d_mejora;
        char *ligtype_d, *ligtype_d_mejora;
        unsigned int *bonds_d, *bonds_d_mejora;
        unsigned int *atoms, *atoms_d, *nlinks, *nlinks_d;
        type_data *conformations_x_d, *conformations_y_d, *conformations_z_d, *conformations_x_d_mejora, *conformations_y_d_mejora, *conformations_z_d_mejora;

        type_data *rec_x_d, *rec_y_d, *rec_z_d, *qr_d;
        char *rectype_d;
        unsigned int *bondsr_d;

        unsigned int steps, moveType=MOVE, total, simulations;
        cudaError_t cudaStatus;

        unsigned long int moveSize = nconformations * sizeof(type_data);
        unsigned long int confSize = nconformations * nlig * sizeof(type_data);
	
        int tam_environment = (total_conformations * metaheuristic.NEEImp) / files;
        //printf("total_conformations %d , files %d\n",total_conformations,files);
        cudaSetDevice(devices->id[orden_device]);
        cudaDeviceReset();
        //internal_energy = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);

	nlinks = (unsigned int *)calloc(files,sizeof(unsigned int));
        atoms = (unsigned int *)malloc(sizeof(unsigned int)*files);
        for (unsigned int i=0;i<files;i++)atoms[i]=ligand_params[i].atoms;
	for (unsigned int i=0;i<files;i++)nlinks[i]=flexibility_params[i].n_links;

	dataToGPUFlexibility_Params(files,ligand_params,flexibility_params,flexibility_params_d);

	dataToGPU_multigpu_mejorar_inicializar_environment(nconformations, nconformations_mejora, energy_d, energy_nconformation_d, energy_d_mejora, energy_nconformation_d_mejora, moves_x_d, moves_y_d, moves_z_d, quat_x_d, quat_y_d, quat_z_d, quat_w_d, moves_x_d_mejora, moves_y_d_mejora, moves_z_d_mejora, quat_x_d_mejora,quat_y_d_mejora,quat_z_d_mejora,quat_w_d_mejora, states_d_mejora, move_x, move_y, move_z, quat_x, quat_y, quat_z, quat_w, energy, n_conformation, move_x_mejora, move_y_mejora, move_z_mejora, quat_x_mejora, quat_y_mejora, quat_z_mejora, quat_w_mejora, energy_mejora, n_conformation_mejora, metaheuristic);

        dataToGPUConformations_environment(nconformations,metaheuristic.NEEImp,nlig,conformations_x,conformations_x_d,conformations_y,conformations_y_d,conformations_z,conformations_z_d,conformations_x_mejora,conformations_x_d_mejora,conformations_y_mejora,conformations_y_d_mejora,conformations_z_mejora,conformations_z_d_mejora);

        commonDataToGPU_WS_vs (files,nconformations,nlig,proteina,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,atoms,atoms_d,nlinks,nlinks_d,ligtype,ligtype_d,bonds,bonds_d,ql,ql_d);
        commonDataToGPU_WS_vs_clb(nconformations_mejora,nlig,ligtype_mejora,bonds_mejora,ql_mejora,ligtype_d_mejora,bonds_d_mejora,ql_d_mejora);
        save_params(weights,f_params);

	cudaStatus = cudaMalloc((void**)&internal_energy_d,sizeof(type_data)*nconformations_mejora);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc internal_energy_d");
        cudaStatus = cudaMalloc((void**) &flexibility_conformations_d_mejora,sizeof(struct flexibility_data_t)*nconformations_mejora);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc flexibility_conformations_d");
         //Inicializa el generador de numeros aleatorios y el cuaternion de cada simulacion
        //printf("Memoria reservada\n");
        //exit(0);
        setupCurandState <<<(nconformations_mejora/devices->hilos[orden_device*metaheuristic.num_kernels+4])+1, devices->hilos[orden_device*metaheuristic.num_kernels+4] >>>  (states_d_mejora, param.automatic_seed,param.seed,nconformations_mejora);

        // check if kernel execution generated and error
        cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");

	steps = 0;
        moveType = MOVE;
        if (fase == 0)
        {
                total = metaheuristic.IMEIni + metaheuristic.IMEFlex;
                simulations = metaheuristic.IMEIni;
        }
        if (fase == 1)
        {
                total = metaheuristic.IMEImp + metaheuristic.IMEFlex;
                 simulations = metaheuristic.IMEImp;
        }
        if (fase == 2)
        {
                total = metaheuristic.IMEMUCom + metaheuristic.IMEFlex;
                 simulations = metaheuristic.IMEMUCom;
        }
        //printf("Voy a mejorar....\n");

	cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeFourByte);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_vs_by_warp, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_vs_by_warp_type1, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_vs_by_warp_type2, cudaFuncCachePreferEqual);	

	cudaFuncSetCacheConfig(energy_internal_conformations_vs, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(energy_internal_conformations_vs_type2, cudaFuncCachePreferEqual);

        unsigned int blk_f = ceil(nconformations_mejora / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
        unsigned int max_f = nconformations_mejora;
        dim3 grid_f(ceil(blk_f/8)+1, 8);
        dim3 hilos_f(devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);

        unsigned int blk_t = ceil(nconformations_mejora / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
        dim3 grid_t  (ceil(blk_t/8)+1, 8);
        dim3 hilos_t (devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);
        unsigned int max_t = nconformations_mejora;

        unsigned int size = 5 * devices->hilos[orden_device*metaheuristic.num_kernels] * sizeof(type_data) + sizeof(char)*devices->hilos[orden_device*metaheuristic.num_kernels];
        unsigned int blk_e = ceil(nconformations_mejora*WARP_SIZE / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
        unsigned long int max_e = nconformations_mejora* WARP_SIZE;
        dim3 hilos_e (devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);
	dim3 grid_e  (ceil(blk_e/8)+1, 8);

	 while (steps < total){
                if (steps >=simulations)
                {
                        //FLEXIBILIDAD
                        angulations_conformations_vs <<< grid_f,hilos_f >>> (states_d_mejora, flexibility_conformations_d_mejora,flexibility_params_d,param.flex_angle,stride_d,tam_environment,max_f);
                        flexibilityKernel_vs  <<< grid_f,hilos_f >>>(conformations_x_d_mejora,conformations_y_d_mejora,conformations_z_d_mejora,states_d_mejora,flexibility_conformations_d_mejora,flexibility_params_d,nlig,param.flex_angle,stride_d,tam_environment, max_f);
                        cudaDeviceSynchronize();
                        cudaStatus = cudaGetLastError();
                        if (cudaStatus != cudaSuccess) printf("flexibility failed!\n");
                        //printf("FLEX\n");

                }
                else
                {
                        if (moveType == MOVE)
                        {
                                //printf("He movido\n");
                                move_mejora <<< (nconformations_mejora / devices->hilos[orden_device*metaheuristic.num_kernels+5])+1,devices->hilos[orden_device*metaheuristic.num_kernels+5] >>> (states_d_mejora,param.max_desp,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,nconformations_mejora);
                                cudaDeviceSynchronize();
                                cudaStatus = cudaGetLastError();
                                if (cudaStatus != cudaSuccess) printf("kernel move_mejora failed\n");
                        }
                        if (moveType == ROTATE)
                        {
                                rotation <<< (nconformations_mejora /devices->hilos[orden_device*metaheuristic.num_kernels+2])+1, devices->hilos[orden_device*metaheuristic.num_kernels+2] >>> (states_d_mejora,param.rotation,quat_x_d_mejora,quat_y_d_mejora,quat_z_d_mejora,quat_w_d_mejora,nconformations_mejora);
                                cudaDeviceSynchronize();
                                cudaStatus = cudaGetLastError();
                                if (cudaStatus != cudaSuccess) printf("kernel rotation failed\n");
                        }
                        if (moveType == FLEX)
                        {
                                angulations_conformations_vs <<< grid_f,hilos_f >>> (states_d_mejora, flexibility_conformations_d_mejora,flexibility_params_d,param.flex_angle,stride_d,tam_environment,max_f);
                                flexibilityKernel_vs  <<< grid_f,hilos_f >>>(conformations_x_d_mejora,conformations_y_d_mejora,conformations_z_d_mejora,states_d_mejora,flexibility_conformations_d_mejora,flexibility_params_d,nlig,param.flex_angle,stride_d,tam_environment, max_f);
                                cudaDeviceSynchronize();
                                cudaStatus = cudaGetLastError();
                                if (cudaStatus != cudaSuccess) printf("Error Kernel flexibilidad\n");
                        }
                }
		
		switch (param.scoring_function_type)
		{
			case 0:
				Gpu_full_Kernel_Conformations_vs_by_warp <<<grid_e,hilos_e,size>>> (proteina.nrec,tam_environment,atoms_d,nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d_mejora,conformations_y_d_mejora,conformations_z_d_mejora,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_x_d_mejora, quat_y_d_mejora, quat_z_d_mejora, quat_w_d_mejora,ql_d_mejora,ligtype_d_mejora,bonds_d_mejora,energy_d_mejora,energy_nconformation_d_mejora,stride_d*metaheuristic.NEEImp,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
				break;
			case 1:
				Gpu_full_Kernel_Conformations_vs_by_warp_type1 <<<grid_e,hilos_e,size>>> (nlinks_d,proteina.nrec,tam_environment,atoms_d,nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d_mejora,conformations_y_d_mejora,conformations_z_d_mejora,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_x_d_mejora, quat_y_d_mejora, quat_z_d_mejora, quat_w_d_mejora,ql_d_mejora,ligtype_d_mejora,bonds_d_mejora,energy_d_mejora,energy_nconformation_d_mejora,stride_d*metaheuristic.NEEImp,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
                                break;
			case 2:
				Gpu_full_Kernel_Conformations_vs_by_warp_type2 <<<grid_e,hilos_e,size>>> (nlinks_d,proteina.nrec,tam_environment,atoms_d,nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d_mejora,conformations_y_d_mejora,conformations_z_d_mejora,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_x_d_mejora, quat_y_d_mejora, quat_z_d_mejora, quat_w_d_mejora,ql_d_mejora,ligtype_d_mejora,bonds_d_mejora,energy_d_mejora,energy_nconformation_d_mejora,stride_d*metaheuristic.NEEImp,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
                                break;

		}
                cudaDeviceSynchronize();
                cudaStatus = cudaGetLastError();
                if (cudaStatus != cudaSuccess) printf("Kernel energy failed!");

		switch (param.scoring_function_type)
		{
			case 0:
			case 2:
		                energy_internal_conformations_vs<<<grid_e,hilos_e>>>(conformations_x_d_mejora,conformations_y_d_mejora,conformations_z_d_mejora,atoms_d,nlig,flexibility_params_d,internal_energy_d,energy_nconformation_d_mejora,ql_d_mejora,ligtype_d_mejora,stride_d*metaheuristic.NEEImp,devices->hilos[orden_device*metaheuristic.num_kernels],max_e,tam_environment);
				break;
			case 1:
				energy_internal_conformations_vs_type2<<<grid_e,hilos_e>>>(conformations_x_d_mejora,conformations_y_d_mejora,conformations_z_d_mejora,atoms_d,nlig,flexibility_params_d,internal_energy_d,energy_nconformation_d_mejora,ql_d_mejora,ligtype_d_mejora,stride_d*metaheuristic.NEEImp,devices->hilos[orden_device*metaheuristic.num_kernels],max_e,tam_environment);
                                break;
		}
                cudaDeviceSynchronize();
                cudaStatus = cudaGetLastError();
                if (cudaStatus != cudaSuccess) printf("KERNEL FAILED INTERNAL ENERGY!");

                energy_total <<<grid_t,hilos_t>>>(energy_d_mejora,internal_energy_d,max_t);
                cudaDeviceSynchronize();
                cudaStatus = cudaGetLastError();
                if (cudaStatus != cudaSuccess) printf("KERNEL FAILED TOTAL ENERGY IMPROVE!\n");

		if (steps >=simulations)
                {
                        update_angulations_conformations_tradicional <<< grid_f,hilos_f >>> (states_d_mejora, flexibility_conformations_d_mejora, energy_d_mejora, energy_d,max_f);
                        flexibilityKernel_vs  <<< grid_f,hilos_f >>>(conformations_x_d_mejora,conformations_y_d_mejora,conformations_z_d_mejora,states_d_mejora,flexibility_conformations_d_mejora,flexibility_params_d,nlig,param.flex_angle,stride_d,tam_environment, max_f);

                }
                else
                {
			if (moveType == FLEX)
                        {
                                update_angulations_conformations_tradicional <<< grid_f,hilos_f >>> (states_d_mejora, flexibility_conformations_d_mejora, energy_d_mejora, energy_d,max_f);
                                flexibilityKernel_vs  <<< grid_f,hilos_f >>>(conformations_x_d_mejora,conformations_y_d_mejora,conformations_z_d_mejora,states_d_mejora,flexibility_conformations_d_mejora,flexibility_params_d,nlig,param.flex_angle,stride_d,tam_environment, max_f);
                        }
        	
                	if (moveType == MOVE) moveType = ROTATE;
                        else if (moveType == ROTATE) moveType = FLEX;
                        else if (moveType == FLEX) moveType = MOVE;
		}
                incluir_mejorar_environment_flex <<<(nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+6])+1, devices->hilos[orden_device*metaheuristic.num_kernels+6]>>> (metaheuristic.NEEImp, nlig, conformations_x_d_mejora, conformations_y_d_mejora,conformations_z_d_mejora,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_x_d_mejora,quat_y_d_mejora,quat_z_d_mejora,quat_w_d_mejora,energy_d_mejora,energy_nconformation_d_mejora,conformations_x_d,conformations_y_d,conformations_z_d,moves_x_d,moves_y_d,moves_z_d,quat_x_d,quat_y_d,quat_z_d,quat_w_d,energy_d,stride_d,nconformations);
                //fill_conformations_mejora_flex <<<(nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+6])+1, devices->hilos[orden_device*metaheuristic.num_kernels+6]>>> (metaheuristic.NEEImp,nlig,conformations_x_d_mejora, conformations_y_d_mejora, conformations_z_d_mejora, moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_x_d_mejora,quat_y_d_mejora,quat_z_d_mejora,quat_w_d_mejora,energy_d_mejora,conformations_x_d,conformations_y_d,conformations_z_d,moves_x_d,moves_y_d,moves_z_d,quat_x_d,quat_y_d,quat_z_d,quat_w_d,energy_d,nconformations);
                steps++;
                //printf("steps %d\n",steps);
        }
	cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernels antes de copia fallo improve ini!\n");
        cudaStatus = cudaMemcpy(conformations_x, conformations_x_d, confSize, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values conformations_x_d GPUtoHost failed!");
        cudaStatus = cudaMemcpy(conformations_y, conformations_y_d, confSize, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values conformations_y_d GPUtoHost failed!");
        cudaStatus = cudaMemcpy(conformations_z, conformations_z_d, confSize, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values conformations_z_d GPUtoHost failed!");
        cudaStatus = cudaMemcpy(move_x, moves_x_d, moveSize, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_x_d_mejora GPUtoHost failed!");
        cudaStatus = cudaMemcpy(move_y, moves_y_d, moveSize, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_y_d_mejora GPUtoHost failed!");
        cudaStatus = cudaMemcpy(move_z, moves_z_d, moveSize, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_z_d_mejora GPUtoHost failed!");
        cudaStatus = cudaMemcpy(energy, energy_d, moveSize, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values energy_d_mejora GPUtoHost failed!");
        cudaStatus = cudaMemcpy(quat_x, quat_x_d, moveSize, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
        cudaStatus = cudaMemcpy(quat_y, quat_y_d, moveSize, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
        cudaStatus = cudaMemcpy(quat_z, quat_z_d, moveSize, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
        cudaStatus = cudaMemcpy(quat_w, quat_w_d, moveSize, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
        cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) fprintf(stderr, "Copy failed!\n");

	 //printf("***FIN MEJORA***\n");

	free(atoms);
	free(nlinks);
	cudaFree(atoms_d);
	cudaFree(nlinks_d);
        cudaFree(states_d_mejora);
        cudaFree(moves_x_d);
        cudaFree(moves_y_d);
        cudaFree(moves_z_d);
        cudaFree(quat_x_d);
        cudaFree(quat_y_d);
        cudaFree(quat_z_d);
        cudaFree(quat_w_d);
        cudaFree(rec_x_d);
        cudaFree(rec_y_d);
        cudaFree(rec_z_d);
        cudaFree(qr_d);
        cudaFree(bondsr_d);
        cudaFree(rectype_d);
        cudaFree(ql_d);
        cudaFree(ligtype_d);
        cudaFree(bonds_d);
	cudaFree(ql_d_mejora);
        cudaFree(ligtype_d_mejora);
        cudaFree(bonds_d_mejora);
        cudaFree(energy_d);
        cudaFree(energy_d_mejora);
        cudaFree(energy_nconformation_d);
        cudaFree(energy_nconformation_d_mejora);
        cudaFree(moves_x_d_mejora);
        cudaFree(moves_y_d_mejora);
        cudaFree(moves_z_d_mejora);
        cudaFree(quat_x_d_mejora);
        cudaFree(quat_y_d_mejora);
        cudaFree(quat_z_d_mejora);
        cudaFree(quat_w_d_mejora);

        cudaFree(flexibility_params_d);
        cudaFree(flexibility_conformations_d_mejora);

        cudaFree(conformations_x_d);
        cudaFree(conformations_y_d);
        cudaFree(conformations_z_d);
	cudaFree(conformations_x_d_mejora);
        cudaFree(conformations_y_d_mejora);
        cudaFree(conformations_z_d_mejora);

        cudaDeviceReset();

}
extern void multigpu_mejorar_gpusolver_vs_flex (unsigned int total_conformations, unsigned int files, unsigned int nlig, char* ligtype, unsigned int *bonds, type_data *ql, struct ligand_params_t *ligand_params, struct flexibility_params_t *flexibility_params, struct receptor_t proteina, struct param_t param, type_data *weights, struct force_field_param_t *f_params, struct device_t *devices, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *n_conformation, struct metaheuristic_t metaheuristic, unsigned int nconformations, unsigned int orden_device, unsigned int stride_d, unsigned int fase)
{

	curandState_t *states_d;
	struct receptor_t *proteina_d;
	struct flexibility_params_t *flexibility_params_d;
	struct flexibility_data_t *flexibility_conformations_d;
	type_data *internal_energy, *internal_energy_d, *moves_x_d, *moves_y_d, *moves_z_d, *energy_d, *ql_d;		
	type_data *quat_d_x, *quat_d_y, *quat_d_z, *quat_d_w, *quat_d_mejora_x, *quat_d_mejora_y, *quat_d_mejora_z, *quat_d_mejora_w;
	type_data *moves_x_d_mejora, *moves_y_d_mejora, *moves_z_d_mejora, *energy_d_mejora;
	unsigned int *energy_nconformation_d, *energy_nconformation_d_mejora;		
	char *ligtype_d;
	unsigned int *bonds_d;
	unsigned int *atoms, *atoms_d, *nlinks, *nlinks_d;
	type_data *conformations_x_d, *conformations_y_d, *conformations_z_d;
	
	type_data *rec_x_d, *rec_y_d, *rec_z_d, *qr_d;
	char *rectype_d;
	unsigned int *bondsr_d;

	unsigned int steps, moveType=MOVE, total, simulations;
	cudaError_t cudaStatus;
	
	unsigned long int moveSize = nconformations * sizeof(type_data);
	unsigned long int confSize = nconformations * nlig * sizeof(type_data);
	int tam = total_conformations / files;
	//printf("total_conformations %d , files %d\n",total_conformations,files);	
	cudaSetDevice(devices->id[orden_device]);		
	cudaDeviceReset();
	//internal_energy = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);
	
	nlinks = (unsigned int *)calloc(files,sizeof(unsigned int));
	atoms = (unsigned int *)malloc(sizeof(unsigned int)*files);
	for (unsigned int i=0;i<files;i++)atoms[i]=ligand_params[i].atoms;
	for (unsigned int i=0;i<files;i++)nlinks[i]=flexibility_params[i].n_links;
		
	dataToGPUFlexibility_Params(files,ligand_params,flexibility_params,flexibility_params_d);
	
	dataToGPU_multigpu_mejorar_inicializar(nconformations, energy_d, energy_nconformation_d, energy_d_mejora, energy_nconformation_d_mejora, moves_x_d, moves_y_d, moves_z_d, quat_d_x, quat_d_y, quat_d_z, quat_d_w, moves_x_d_mejora, moves_y_d_mejora, moves_z_d_mejora, quat_d_mejora_x, quat_d_mejora_y, quat_d_mejora_z, quat_d_mejora_w, states_d, move_x, move_y, move_z, quat_x, quat_y, quat_z, quat_w, energy, n_conformation, metaheuristic);
		
	dataToGPUConformations(nconformations,nlig,conformations_x,conformations_x_d,conformations_y,conformations_y_d,conformations_z,conformations_z_d);
	
	commonDataToGPU_WS_vs (files,nconformations,nlig,proteina, rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d, atoms, atoms_d, nlinks, nlinks_d, ligtype, ligtype_d, bonds, bonds_d, ql, ql_d);
	
	save_params (weights,f_params);

	cudaStatus = cudaMalloc((void**)&internal_energy_d,sizeof(type_data)*nconformations);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc internal_energy_d");
	cudaStatus = cudaMalloc((void**) &flexibility_conformations_d,sizeof(struct flexibility_data_t)*nconformations);	
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc flexibility_conformations_d");
	 //Inicializa el generador de numeros aleatorios y el cuaternion de cada simulacion
	//printf("Memoria reservada\n");
	//exit(0);
	setupCurandState <<<(nconformations/devices->hilos[orden_device*metaheuristic.num_kernels+4])+1, devices->hilos[orden_device*metaheuristic.num_kernels+4] >>>  (states_d, param.automatic_seed,param.seed,nconformations);

	// check if kernel execution generated and error
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");
	
	steps = 0;
	moveType = MOVE;
	if (fase == 0)
	{
		total = metaheuristic.IMEIni + metaheuristic.IMEFlex;
		simulations = metaheuristic.IMEIni;
	}
	if (fase == 1)
	{
		total = metaheuristic.IMEImp + metaheuristic.IMEFlex;
		 simulations = metaheuristic.IMEImp;
	}
	if (fase == 2)
	{
                total = metaheuristic.IMEMUCom + metaheuristic.IMEFlex;
                 simulations = metaheuristic.IMEMUCom;
        }
	//printf("Voy a mejorar....\n");
	
	cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeFourByte);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_vs_by_warp, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_vs_by_warp_type1, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_vs_by_warp_type2, cudaFuncCachePreferEqual);

        cudaFuncSetCacheConfig(energy_internal_conformations_vs, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(energy_internal_conformations_vs_type2, cudaFuncCachePreferEqual);

	unsigned int blk_f = ceil(nconformations / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
	unsigned int max_f = nconformations;			
	dim3 grid_f(ceil(blk_f/8)+1, 8);
	dim3 hilos_f(devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);
	
	unsigned int blk_t = ceil(nconformations / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
	dim3 grid_t  (ceil(blk_t/8)+1, 8);	
	dim3 hilos_t (devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);	
	unsigned int max_t = nconformations;	
			
	unsigned int size = 5 * devices->hilos[orden_device*metaheuristic.num_kernels] * sizeof(type_data) + sizeof(char)*devices->hilos[orden_device*metaheuristic.num_kernels];	
	unsigned int blk_e = ceil(nconformations*WARP_SIZE / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
	unsigned long int max_e = nconformations* WARP_SIZE;	
	dim3 hilos_e (devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);
	dim3 grid_e  (ceil(blk_e/8)+1, 8);
	
	while (steps < total){		
		if (steps >=simulations)
		{
			//FLEXIBILIDAD
			angulations_conformations_vs <<< grid_f,hilos_f >>> (states_d, flexibility_conformations_d,flexibility_params_d,param.flex_angle,stride_d,tam,max_f);
			flexibilityKernel_vs  <<< grid_f,hilos_f >>>(conformations_x_d,conformations_y_d,conformations_z_d,states_d,flexibility_conformations_d,flexibility_params_d,nlig,param.flex_angle,stride_d,tam, max_f);								
			cudaDeviceSynchronize();
			cudaStatus = cudaGetLastError();
			if (cudaStatus != cudaSuccess) printf("flexibility failed!\n");
			//printf("FLEX\n");
			
		}
		else
		{
			if (moveType == MOVE)
			{
				//printf("He movido\n");
				move_mejora <<< (nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+5])+1,devices->hilos[orden_device*metaheuristic.num_kernels+5] >>> (states_d,param.max_desp,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,nconformations);
				cudaDeviceSynchronize();
				cudaStatus = cudaGetLastError();
				if (cudaStatus != cudaSuccess) printf("kernel move_mejora failed\n");
			}
			if (moveType == ROTATE)
			{
				rotation <<< (nconformations /devices->hilos[orden_device*metaheuristic.num_kernels+2])+1, devices->hilos[orden_device*metaheuristic.num_kernels+2] >>> (states_d,param.rotation,quat_d_mejora_x,quat_d_mejora_y,quat_d_mejora_z,quat_d_mejora_w,nconformations);
				cudaDeviceSynchronize();
				cudaStatus = cudaGetLastError();
				if (cudaStatus != cudaSuccess) printf("kernel rotation failed\n");
			}
			if (moveType == FLEX)
			{
				angulations_conformations_vs <<< grid_f,hilos_f >>> (states_d, flexibility_conformations_d,flexibility_params_d,param.flex_angle,stride_d,tam,max_f);
				flexibilityKernel_vs  <<< grid_f,hilos_f >>>(conformations_x_d,conformations_y_d,conformations_z_d,states_d,flexibility_conformations_d,flexibility_params_d,nlig,param.flex_angle,stride_d,tam, max_f);
				cudaDeviceSynchronize();
				cudaStatus = cudaGetLastError();
				if (cudaStatus != cudaSuccess) printf("Error Kernel flexibilidad\n");				
			}
		}
	
		switch (param.scoring_function_type)
		{
			case 0:
				Gpu_full_Kernel_Conformations_vs_by_warp <<<grid_e,hilos_e,size>>> (proteina.nrec,tam,atoms_d,nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d,conformations_y_d,conformations_z_d,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_d_mejora_x, quat_d_mejora_y, quat_d_mejora_z, quat_d_mejora_w,ql_d,ligtype_d,bonds_d,energy_d_mejora,energy_nconformation_d,stride_d,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);                                                             
				break;
			case 1:
				Gpu_full_Kernel_Conformations_vs_by_warp_type1 <<<grid_e,hilos_e,size>>> (nlinks_d,proteina.nrec,tam,atoms_d,nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d,conformations_y_d,conformations_z_d,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_d_mejora_x, quat_d_mejora_y, quat_d_mejora_z, quat_d_mejora_w,ql_d,ligtype_d,bonds_d,energy_d_mejora,energy_nconformation_d,stride_d,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
                                break;

			case 2:
				Gpu_full_Kernel_Conformations_vs_by_warp_type2 <<<grid_e,hilos_e,size>>> (nlinks_d,proteina.nrec,tam,atoms_d,nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d,conformations_y_d,conformations_z_d,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_d_mejora_x, quat_d_mejora_y, quat_d_mejora_z, quat_d_mejora_w,ql_d,ligtype_d,bonds_d,energy_d_mejora,energy_nconformation_d,stride_d,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
                                break;
		}

		cudaDeviceSynchronize();
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) printf("Kernel energy failed!");
			
		switch (param.scoring_function_type)
		{
			case 0:
			case 1:
				energy_internal_conformations_vs<<<grid_e,hilos_e>>>(conformations_x_d,conformations_y_d,conformations_z_d,atoms_d,nlig,flexibility_params_d,internal_energy_d,energy_nconformation_d,ql_d,ligtype_d,stride_d,devices->hilos[orden_device*metaheuristic.num_kernels],max_e,tam);
				break;
			case 2:
				energy_internal_conformations_vs_type2<<<grid_e,hilos_e>>>(conformations_x_d,conformations_y_d,conformations_z_d,atoms_d,nlig,flexibility_params_d,internal_energy_d,energy_nconformation_d,ql_d,ligtype_d,stride_d,devices->hilos[orden_device*metaheuristic.num_kernels],max_e,tam);
                                break;
		}
		cudaDeviceSynchronize();				
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) printf("KERNEL FAILED INTERNAL ENERGY!");
	
		energy_total <<<grid_t,hilos_t>>>(energy_d_mejora,internal_energy_d,max_t);
		cudaDeviceSynchronize();				
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) printf("KERNEL FAILED TOTAL ENERGY IMPROVE!\n");
		if (steps >=simulations)
		{
			update_angulations_conformations_tradicional <<< grid_f,hilos_f >>> (states_d, flexibility_conformations_d, energy_d_mejora, energy_d,max_f);
			flexibilityKernel_vs  <<< grid_f,hilos_f >>>(conformations_x_d,conformations_y_d,conformations_z_d,states_d,flexibility_conformations_d,flexibility_params_d,nlig,param.flex_angle,stride_d,tam, max_f);								
			
		}
		else
		{
			if (moveType == FLEX)
			{
				update_angulations_conformations_tradicional <<< grid_f,hilos_f >>> (states_d, flexibility_conformations_d, energy_d_mejora, energy_d,max_f);
				flexibilityKernel_vs  <<< grid_f,hilos_f >>>(conformations_x_d,conformations_y_d,conformations_z_d,states_d,flexibility_conformations_d,flexibility_params_d,nlig,param.flex_angle,stride_d,tam, max_f);								
			}
			else
			{
				incluir_mejorar <<<(nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+6])+1, devices->hilos[orden_device*metaheuristic.num_kernels+6]>>> (moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_d_mejora_x,quat_d_mejora_y,quat_d_mejora_z,quat_d_mejora_w,energy_d_mejora,energy_nconformation_d,moves_x_d,moves_y_d,moves_z_d,quat_d_x, quat_d_y, quat_d_z, quat_d_w,energy_d,stride_d,nconformations);	
			}
			if (moveType == MOVE) moveType = ROTATE;
                        else if (moveType == ROTATE) moveType = FLEX;
                        else if (moveType == FLEX) moveType = MOVE;
			
		}
		steps++;		
		//printf("steps %d\n",steps);
	}
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernels antes de copia fallo improve ini!\n");	
	cudaStatus = cudaMemcpy(conformations_x, conformations_x_d, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values conformations_x_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(conformations_y, conformations_y_d, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values conformations_y_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(conformations_z, conformations_z_d, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values conformations_z_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(move_x, moves_x_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_x_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(move_y, moves_y_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_y_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(move_z, moves_z_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_z_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(energy, energy_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values energy_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_x, quat_d_x, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_y, quat_d_y, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_z, quat_d_z, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_w, quat_d_w, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Copy failed!\n");
	//for (int i=0;i<13312;i++) printf("conf %d, xyz %f %f %f m %f %f %f, q %f %f %f, energy %f %d\n",i,vectors_e_s->conformations_x[i],vectors_e_s->conformations_y[i], vectors_e_s->conformations_z[i],vectors_e_s->move_x[i/vectors_e_s->nlig],vectors_e_s->move_y[i/vectors_e_s->nlig],vectors_e_s->move_z[i/vectors_e_s->nlig],vectors_e_s->quat_x[i/vectors_e_s->nlig],vectors_e_s->quat_y[i/vectors_e_s->nlig],vectors_e_s->quat_z[i/vectors_e_s->nlig],vectors_e_s->energy.energy[i/vectors_e_s->nlig],vectors_e_s->energy.n_conformation[i/vectors_e_s->nlig]);
	
	//printf("***FIN MEJORA***\n");	
	free(atoms);
	free(nlinks);
	cudaFree(atoms_d);
	cudaFree(nlinks_d);
	cudaFree(states_d);
	cudaFree(moves_x_d);
	cudaFree(moves_y_d);
	cudaFree(moves_z_d);
	cudaFree(quat_d_x);
	cudaFree(quat_d_y);
	cudaFree(quat_d_z);
	cudaFree(quat_d_w);
	cudaFree(rec_x_d);
	cudaFree(rec_y_d);
	cudaFree(rec_z_d);
	cudaFree(qr_d);
	cudaFree(bondsr_d);
	cudaFree(rectype_d);
	cudaFree(ql_d);
	cudaFree(ligtype_d);
	cudaFree(bonds_d);
	cudaFree(energy_d);
	cudaFree(energy_d_mejora);
	cudaFree(energy_nconformation_d);
	cudaFree(energy_nconformation_d_mejora);
	cudaFree(moves_x_d_mejora);
	cudaFree(moves_y_d_mejora);
	cudaFree(moves_z_d_mejora);
	cudaFree(quat_d_mejora_x);
	cudaFree(quat_d_mejora_y);
	cudaFree(quat_d_mejora_z);
	cudaFree(quat_d_mejora_w);
	
	cudaFree(flexibility_params_d);
	cudaFree(flexibility_conformations_d);
	
	cudaFree(conformations_x_d);
	cudaFree(conformations_y_d);
	cudaFree(conformations_z_d);

	cudaDeviceReset();



}


extern void generate_positions_vs_flex (unsigned int nlig, struct ligand_params_t *ligand_params, struct param_t param, struct vectors_t *vectors_e_s, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int orden_device, struct flexibility_params_t *flexibility_params,unsigned int n) 
{
	size_t available,total_m;
    cudaError_t cudaStatus;
    unsigned long int max_memory;
    unsigned long int max_positions;
    unsigned long int max_common;
    unsigned long int max_conf;
    unsigned long int randSize = vectors_e_s->nconformations * sizeof(curandState_t);
		
    unsigned long int moveSize_tam = vectors_e_s->nconformations * sizeof(type_data);
    unsigned long int confSize_tam = vectors_e_s->nconformations * nlig * sizeof(type_data);
	unsigned long int linksSize = flexibility_params->n_links * 2 * sizeof(unsigned int);
    unsigned long int linksfragmentsSize = flexibility_params->n_links * 2 * sizeof(unsigned int);
    unsigned long int fragmentstamSize = flexibility_params->n_fragments * sizeof(unsigned int);
    unsigned long int fragmentsSize = flexibility_params->n_links * (ligand_params->atoms - 2) * sizeof(unsigned int);
	unsigned long int boolSize = ligand_params->atoms * 2 * sizeof(bool);
    
	unsigned long int flexConfSize =  vectors_e_s->nconformations * sizeof(struct flexibility_data_t);
    unsigned long int tam_max, tam_tmp, nconformations_count_total, nconformations_count_partial, tam_conformation, stride_d;
    unsigned int conf_point = vectors_e_s->nconformations / vectors_e_s->files;

    max_positions = moveSize_tam * 7 + randSize;
    max_common = flexConfSize + linksSize + linksfragmentsSize + fragmentstamSize + fragmentsSize + (boolSize * 2);
    max_conf = (3 * confSize_tam);
    max_memory = max_positions + max_common + max_conf;
    tam_conformation =  (sizeof(type_data) * 7) + sizeof(curandState_t) + (3 * nlig * sizeof(type_data));

    cudaSetDevice(devices->id[orden_device]);
    cudaDeviceReset();
    cudaMemGetInfo(&tam_max, &total_m);
    //cudaDeviceSynchronize();
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
    tam_tmp = tam_max - (tam_max * 0.2);
    std::cout << tam_max << " " << total_m << " " << max_memory << " " << tam_tmp << " max_common: " << max_common <<"\n";

	if (tam_tmp < max_memory)
        {
                //printf("Trocear en 1 GPU\n");
                max_memory = max_common;
                nconformations_count_total = 0;
                nconformations_count_partial = 0;
                stride_d = 0;
                while (nconformations_count_total < vectors_e_s->nconformations)
                {
                        max_memory += tam_conformation;
                        if (max_memory > tam_tmp)
                        {
                                //printf("Trozo %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
                                //nconformations_count_total--;
                                //nconformations_count_partial--;
                                //Lanzar con nconformations_count_partial;
				generate_positions_calculation_vs_flex (vectors_e_s->files,vectors_e_s->nlig,ligand_params,param,nconformations_count_partial,vectors_e_s->conformations_x+(stride_d*vectors_e_s->nlig),vectors_e_s->conformations_y+(stride_d*vectors_e_s->nlig),vectors_e_s->conformations_z+(stride_d*vectors_e_s->nlig),vectors_e_s->move_x+stride_d,vectors_e_s->move_y+stride_d,vectors_e_s->move_z+stride_d,vectors_e_s->quat_x+stride_d,vectors_e_s->quat_y+stride_d,vectors_e_s->quat_z+stride_d,vectors_e_s->quat_w+stride_d,vectors_e_s->ac_x,vectors_e_s->ac_y,vectors_e_s->ac_z,metaheuristic,devices,orden_device,flexibility_params,stride_d,conf_point,0);

                                stride_d += nconformations_count_partial;
                                max_memory = max_common;
                                nconformations_count_partial = 0;
                        }
                        else
                        {
                                nconformations_count_total++;
                                nconformations_count_partial++;
                        }
                }
                //printf("Trozo final %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
		nconformations_count_partial--;
		generate_positions_calculation_vs_flex (vectors_e_s->files,vectors_e_s->nlig,ligand_params,param,nconformations_count_partial,vectors_e_s->conformations_x+(stride_d*vectors_e_s->nlig),vectors_e_s->conformations_y+(stride_d*vectors_e_s->nlig),vectors_e_s->conformations_z+(stride_d*vectors_e_s->nlig),vectors_e_s->move_x+stride_d,vectors_e_s->move_y+stride_d,vectors_e_s->move_z+stride_d,vectors_e_s->quat_x+stride_d,vectors_e_s->quat_y+stride_d,vectors_e_s->quat_z+stride_d,vectors_e_s->quat_w+stride_d,vectors_e_s->ac_x,vectors_e_s->ac_y,vectors_e_s->ac_z,metaheuristic,devices,orden_device,flexibility_params,stride_d,conf_point,0);
        }

        else
        {
                //printf("No Trocea\n");
                //Lanzar con todo
		generate_positions_calculation_vs_flex (vectors_e_s->files,vectors_e_s->nlig,ligand_params,param,vectors_e_s->nconformations,vectors_e_s->conformations_x,vectors_e_s->conformations_y,vectors_e_s->conformations_z,vectors_e_s->move_x,vectors_e_s->move_y,vectors_e_s->move_z,vectors_e_s->quat_x,vectors_e_s->quat_y,vectors_e_s->quat_z,vectors_e_s->quat_w,vectors_e_s->ac_x,vectors_e_s->ac_y,vectors_e_s->ac_z,metaheuristic,devices,orden_device,flexibility_params,0,conf_point,0);
        }
	//printf("Fin de generar\n");
	//exit(0);
}

extern void generate_positions_calculation_vs_flex (unsigned int files, unsigned int nlig, struct ligand_params_t *ligand_params, struct param_t param, unsigned int nconformations, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data ac_x, type_data ac_y, type_data ac_z, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int orden_device, struct flexibility_params_t *flexibility_params,unsigned int stride_d,unsigned int conf_point, unsigned int n) 
{

	curandState_t *states_d;
	type_data *moves_x_d, *moves_y_d, *moves_z_d;	
	type_data *conformations_x_d, *conformations_y_d, *conformations_z_d;
	
	struct flexibility_data_t* flexibility_conformations_d;
	struct flexibility_params_t* flexibility_params_d;
	type_data *quat_d_x, *quat_d_y, *quat_d_z, *quat_d_w;
	unsigned int steps, moveType=MOVE,total,inicial,option;
	unsigned long int confSize = nconformations * nlig * sizeof(type_data);
	unsigned long int moveSize = nconformations * sizeof(type_data);
	//int tam = nconformations / files;
	cudaError_t cudaStatus;
	cudaSetDevice(devices->id[orden_device]);
	cudaDeviceReset();
	//cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) fprintf(stderr, "Error en Select device.\n");
	//printf("device: %d %s\n",devices->id[orden_device],devices->propiedades[orden_device].name);
	//printf("tam %d nconf %d files %d\n",tam,nconformations,files);	
	dataToGPU_vs(nconformations, moves_x_d, moves_y_d, moves_z_d, quat_d_x, quat_d_y, quat_d_z, quat_d_w, states_d);
	dataToGPUFlexibility_Params(files,ligand_params,flexibility_params,flexibility_params_d);	
	dataToGPUConformations(nconformations,nlig,conformations_x,conformations_x_d,conformations_y,conformations_y_d,conformations_z,conformations_z_d);
	cudaMalloc((void**) &flexibility_conformations_d,sizeof(struct flexibility_data_t)*nconformations);
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Error en memoria.\n");
	//exit(0);
     //Inicializa el generador de numeros aleatorios y el cuaternion de cada simulacion
	setupCurandState <<< (nconformations/devices->hilos[orden_device*metaheuristic.num_kernels+4])+1, devices->hilos[orden_device*metaheuristic.num_kernels+4] >>>  (states_d, param.automatic_seed,param.seed,nconformations);

	// check if kernel execution generated and error
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel setupCurandState execution failed \n");

	setup <<< (nconformations /devices->hilos[orden_device*metaheuristic.num_kernels+3])+1, devices->hilos[orden_device*metaheuristic.num_kernels+3] >>> (quat_d_x,quat_d_y,quat_d_z,quat_d_w,nconformations);

	// check if kernel execution generated and error
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel setup execution failed \n");
	
	
	steps = 0;	
	if (n == 0)
	{
		total = 20; 
	}
	else 
	{	
		total = param.steps_warm_up_gpu;
	}
	//printf("x %f y %f z %f\n",ac_x,ac_y,ac_z);
	unsigned int blk_f = ceil(nconformations / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
	unsigned int max_f = nconformations;	
	dim3 hilos_f (devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);
	dim3 grid_f  (ceil(blk_f/8)+1, 8);
	while (steps < total)
	{
		if (moveType == MOVE)
		{
			move_vs <<< (nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+1])+1,devices->hilos[orden_device*metaheuristic.num_kernels+1] >>> (states_d, param.max_desp, moves_x_d, moves_y_d, moves_z_d, ac_x, ac_y, ac_z,nconformations);
			moveType = ROTATE;
		}
		if (moveType == ROTATE)
		{
			rotation <<< (nconformations /devices->hilos[orden_device*metaheuristic.num_kernels+2])+1, devices->hilos[orden_device*metaheuristic.num_kernels+2] >>> (states_d, param.rotation, quat_d_x, quat_d_y, quat_d_z, quat_d_w,nconformations);
			moveType = FLEX;
		}
		if (moveType == FLEX)
		{
			angulations_conformations_vs <<< grid_f,hilos_f >>> (states_d, flexibility_conformations_d,flexibility_params_d,param.flex_angle,stride_d,conf_point,max_f);
			flexibilityKernel_vs  <<< grid_f,hilos_f >>>(conformations_x_d,conformations_y_d,conformations_z_d,states_d,flexibility_conformations_d,flexibility_params_d,nlig,param.flex_angle,stride_d,conf_point, max_f);
			moveType == MOVE;
		}
	// check if kernel execution generated and erro
		cudaDeviceSynchronize();
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel move o rotation execution failed \n");
		steps++;
		//exit(0);
	}
	//exit(0);
	cudaStatus = cudaMemcpy(conformations_x, conformations_x_d, confSize, cudaMemcpyDeviceToHost);	
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed1!");
	cudaStatus = cudaMemcpy(conformations_y, conformations_y_d, confSize, cudaMemcpyDeviceToHost);	
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(conformations_z, conformations_z_d, confSize, cudaMemcpyDeviceToHost);	
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	cudaStatus = cudaMemcpy(move_x, moves_x_d, moveSize, cudaMemcpyDeviceToHost);	
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed1!");
	cudaStatus = cudaMemcpy(move_y, moves_y_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(move_z, moves_z_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	cudaStatus = cudaMemcpy(quat_x, quat_d_x, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_y, quat_d_y, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_z, quat_d_z, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(quat_w, quat_d_w, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	//for (int i=0;i<nconformations*nlig;i++)printf("conf %d, conf_x %f, conf_y %f, conf_z %f, energy_mejora[%d] = %f\n",i,conformations_x[i],conformations_y[i],conformations_z[i]);
	//exit(0);
	cudaFree(states_d);
	cudaFree(conformations_x_d);
	cudaFree(conformations_y_d);
	cudaFree(conformations_z_d);
	cudaFree(moves_x_d);
	cudaFree(moves_y_d);
	cudaFree(moves_z_d);
	cudaFree(quat_d_x);
	cudaFree(quat_d_y);
	cudaFree(quat_d_z);
	cudaFree(quat_d_w);
	cudaFree(flexibility_conformations_d);
	cudaFree(flexibility_params_d);
	cudaDeviceReset();
	//exit(0);

}
