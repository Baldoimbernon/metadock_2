#include "definitions.h"
#include "wtime.h"
#include "energy_common.h"
#include "energy_kernel.h"
#include "energy_common-gpu.h"
#include "energy_common-gpu_vs.h"
#include "energy_GpuSolver_vs.h"



extern void gpuSolver_vs (unsigned int conf_point, unsigned int files, unsigned int nlig, char *ligtype, unsigned int *bonds, type_data *ql, struct ligand_params_t *ligand_params, struct receptor_t proteina, struct param_t params, struct metaheuristic_t metaheuristic, struct device_t *devices, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconf, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations,unsigned int orden_device, unsigned int stride_conformations)
{
	
	 size_t available,total_m;
        cudaError_t cudaStatus;
        unsigned long int max_memory;
        unsigned long int max_positions;
        unsigned long int max_common;
        unsigned long int max_conf;
        unsigned long int typeSize = proteina.nrec * sizeof(char);
        unsigned long int recSize = proteina.nrec * sizeof(type_data);
        unsigned long int bondSize = proteina.nrec * sizeof(unsigned int) * MAXBOND;
       
        unsigned long int moveSize_tam = nconformations * sizeof(type_data);
        unsigned long int energyConf_tam = nconformations * sizeof(unsigned int);
        unsigned long int confSize_tam = nconformations * nlig * sizeof(type_data);
        unsigned long int confSize_tam_char = nconformations * nlig * sizeof(char);
        unsigned long int confSize_bonds = nconformations * nlig * MAXBOND *  sizeof(unsigned int);
        unsigned long int tam_max, tam_tmp, nconformations_count_total, nconformations_count_partial, tam_conformation, stride_d;

        max_positions = (moveSize_tam * 8) + energyConf_tam;
        max_conf = (confSize_tam * 3) + confSize_tam + confSize_tam_char + confSize_bonds;
        max_common = (recSize * 4) + typeSize + bondSize;

        max_memory = max_positions + max_conf + max_common;

        tam_conformation =  (sizeof(type_data) * 8) + sizeof(unsigned int) + (sizeof(type_data) * nlig * 3) +  nlig * sizeof(type_data) +  nlig * sizeof(char) +  nlig * MAXBOND *  sizeof(unsigned int);

        cudaSetDevice(devices->id[orden_device]);
        cudaDeviceReset();
        cudaMemGetInfo(&tam_max, &total_m);
        //cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
        std::cout << tam_max << " " << total_m << "\n";
        tam_tmp = tam_max - (tam_max * 0.2);

	if (tam_tmp < max_memory)
        {
                //printf("Trocear en 1 GPU\n");
                max_memory = max_common;
                nconformations_count_total = 0;
                nconformations_count_partial = 0;
                stride_d = 0;
                while (nconformations_count_total < nconformations)
                {
                        max_memory += tam_conformation;
                        if (max_memory > tam_tmp)
                        {
                                //printf("Trozo %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
                                //nconformations_count_total--;
                                //nconformations_count_partial--;
                                //Lanzar con nconformations_count_partial;                       
				  gpuSolver_calculation_vs (conf_point,files,nlig,ligtype+(stride_d*nlig),bonds+(stride_d*nlig*MAXBOND),ql+(stride_d*nlig),ligand_params,proteina,params,metaheuristic,devices,conformations_x+(stride_d*nlig),conformations_y+(stride_d*nlig),conformations_z+(stride_d*nlig),move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,nconf+stride_d,weights,f_params,nconformations_count_partial,orden_device,stride_conformations+stride_d);
                                stride_d += nconformations_count_partial;
                                max_memory = max_common;
                                nconformations_count_partial = 0;
                        }
                        else
                        {
                                nconformations_count_total++;
                                nconformations_count_partial++;
                        }
                }
                //printf("Trozo final %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
		nconformations_count_partial--;
                 gpuSolver_calculation_vs (conf_point,files,nlig,ligtype+(stride_d*nlig),bonds+(stride_d*nlig*MAXBOND),ql+(stride_d*nlig),ligand_params,proteina,params,metaheuristic,devices,conformations_x+(stride_d*nlig),conformations_y+(stride_d*nlig),conformations_z+(stride_d*nlig),move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,nconf+stride_d,weights,f_params,nconformations_count_partial,orden_device,stride_conformations+stride_d);
        }

        else
        {
		printf("No Trocea\n");
                //Lanzar con todo
		gpuSolver_calculation_vs (conf_point,files,nlig,ligtype,bonds,ql,ligand_params,proteina,params,metaheuristic,devices,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconf,weights,f_params,nconformations,orden_device,stride_conformations);
	}
}

extern void gpuSolver_calculation_vs (unsigned int conf_point, unsigned int files, unsigned int nlig, char *ligtype, unsigned int *bonds, type_data *ql, struct ligand_params_t *ligand_params, struct receptor_t proteina, struct param_t params, struct metaheuristic_t metaheuristic, struct device_t *devices, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconf, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations,unsigned int orden_device, unsigned int stride_conformations)
{
        type_data *energy_d, *move_x_d, *move_y_d, *move_z_d;
        type_data *conformations_x_d, *conformations_y_d, *conformations_z_d;
        char *ligtype_d;
        unsigned int *bonds_d;
        unsigned int *atoms, *atoms_d, *nlinks, *nlinks_d;

        type_data *rec_x_d, *rec_y_d, *rec_z_d, *qr_d;
        char *rectype_d;
        unsigned int *bondsr_d;

        type_time total_time_kernels;
        type_data elapsedTime;
        type_data *quat_d_x, *quat_d_y, *quat_d_z, *quat_d_w, *ql_d;
        cudaError_t cudaStatus;

        unsigned int *n_conformations_d;

        //printf("nconformations: %d\n",nconformations);
        unsigned long int energySize = nconformations * sizeof(type_data);
        cudaSetDevice(devices->id[orden_device]);
	cudaDeviceReset();
        //printf("Device %d nconfor %d nlig %d\n",devices->id[orden_device],nconformations,nlig);
        atoms = (unsigned int *)malloc(sizeof(unsigned int)*files);
	nlinks = (unsigned int *)calloc(files,sizeof(unsigned int));
        for (unsigned int i=0;i<files;i++)atoms[i]=ligand_params[i].atoms;

        //cudaDeviceSynchronize();
        //cudaStatus = cudaGetLastError();
        //if (cudaStatus != cudaSuccess) printf ("Error dispositivo\n");
        dataToGPUEnergy(nconformations, move_x, move_y, move_z, quat_x, quat_y, quat_z, quat_w, move_x_d, move_y_d, move_z_d, quat_d_x, quat_d_y, quat_d_z, quat_d_w);
        dataToGPUConformations(nconformations,nlig,conformations_x,conformations_x_d,conformations_y,conformations_y_d,conformations_z,conformations_z_d);
        //commonDataToGPU_vs (files,nconformations,nlig,proteina, proteina_d, atoms, atoms_d, ligtype, ligtype_d, bonds, bonds_d, ql, ql_d);
        commonDataToGPU_WS_vs (files,nconformations,nlig,proteina, rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d, atoms, atoms_d, nlinks, nlinks_d,ligtype, ligtype_d, bonds, bonds_d, ql, ql_d);
//      PROTEINA Y PARAMETROS EN MEMORIA GLOBAL

        cudaStatus = cudaMalloc((void**) &energy_d, energySize);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc energy_d");
        cudaStatus = cudaMalloc((void**) &n_conformations_d, sizeof(unsigned int)*nconformations);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc n_conformations_d");

        //cudaStatus = cudaMemset (energy_d,0,energySize);
        cudaStatus = cudaMemcpy (n_conformations_d,nconf, sizeof(unsigned int)*nconformations, cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy n_conformations_d failed!");

	 //guardar tipos de ligando en memoria constante
        save_params (weights,f_params);
        cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) fprintf(stderr, "COPY FAILED!");

	cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeFourByte);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_vs_by_warp, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_vs_by_warp_type1, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_vs_by_warp_type2, cudaFuncCachePreferEqual);

        unsigned int size = 5 * devices->hilos[orden_device*metaheuristic.num_kernels] * sizeof(type_data) + sizeof(char)*devices->hilos[orden_device*metaheuristic.num_kernels];
        unsigned int blk_e = ceil(nconformations*WARP_SIZE / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
        unsigned long int max_e = nconformations* WARP_SIZE;
        dim3 hilos_e (devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);
        dim3 grid_e  (ceil(blk_e/8)+1, 8);

        switch (params.scoring_function_type)
	{
		case 0:	
			Gpu_full_Kernel_Conformations_vs_by_warp <<<grid_e,hilos_e,size>>> (proteina.nrec,conf_point,atoms_d,nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d,conformations_y_d,conformations_z_d,move_x_d,move_y_d,move_z_d,quat_d_x, quat_d_y, quat_d_z, quat_d_w,ql_d,ligtype_d,bonds_d,energy_d,n_conformations_d,stride_conformations,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
			break;
		case 1:
			Gpu_full_Kernel_Conformations_vs_by_warp_type1 <<<grid_e,hilos_e,size>>> (nlinks_d,proteina.nrec,conf_point,atoms_d,nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d,conformations_y_d,conformations_z_d,move_x_d,move_y_d,move_z_d,quat_d_x, quat_d_y, quat_d_z, quat_d_w,ql_d,ligtype_d,bonds_d,energy_d,n_conformations_d,stride_conformations,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
                        break;
		case 2:
                        Gpu_full_Kernel_Conformations_vs_by_warp_type2 <<<grid_e,hilos_e,size>>> (nlinks_d,proteina.nrec,conf_point,atoms_d,nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d,conformations_y_d,conformations_z_d,move_x_d,move_y_d,move_z_d,quat_d_x, quat_d_y, quat_d_z, quat_d_w,ql_d,ligtype_d,bonds_d,energy_d,n_conformations_d,stride_conformations,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
                        break;
	}

        cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) fprintf(stderr, "KERNEL FAILED ENERGY!");

        cudaStatus = cudaMemcpy (energy, energy_d, energySize, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy energy_d failed!");

	free(atoms);
	free(nlinks);
        cudaFree(move_x_d);
        cudaFree(move_y_d);
        cudaFree(move_z_d);
        cudaFree(quat_d_x);
        cudaFree(quat_d_y);
        cudaFree(quat_d_z);
        cudaFree(quat_d_w);
        cudaFree(rec_x_d);
        cudaFree(rec_y_d);
        cudaFree(rec_z_d);
        cudaFree(qr_d);
        cudaFree(bondsr_d);
        cudaFree(rectype_d);
        cudaFree(atoms_d);
	cudaFree(nlinks_d);
        cudaFree(ql_d);
        cudaFree(ligtype_d);
        cudaFree(bonds_d);
        cudaFree(energy_d);
        cudaFree(n_conformations_d);
        cudaFree(conformations_x_d);
        cudaFree(conformations_y_d);
        cudaFree(conformations_z_d);
        cudaDeviceReset();
}

extern void gpuSolver_vs_flex (unsigned int conf_point, unsigned int files, unsigned int nlig, char *ligtype, unsigned int *bonds, type_data *ql, struct ligand_params_t *ligand_params, struct flexibility_params_t *flexibility_params, struct receptor_t proteina, struct param_t params, struct metaheuristic_t metaheuristic, struct device_t *devices, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconf, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations,unsigned int orden_device, unsigned int stride_conformations)
{

	 size_t available,total_m;
        cudaError_t cudaStatus;
        unsigned long int max_memory;
        unsigned long int max_positions;
        unsigned long int max_common;
        unsigned long int max_conf;
        unsigned long int max_flex_conf;
        unsigned long int typeSize = proteina.nrec * sizeof(char);
        unsigned long int recSize = proteina.nrec * sizeof(type_data);
        unsigned long int bondSize = proteina.nrec * sizeof(unsigned int) * MAXBOND;
        unsigned long int moveSize_tam = nconformations * sizeof(type_data);
        unsigned long int energyConf_tam = nconformations * sizeof(unsigned int);
        unsigned long int confSize_tam = nconformations * nlig * sizeof(type_data);
        unsigned long int confSize_tam_char = nconformations * nlig * sizeof(char);
        unsigned long int confSize_bonds = nconformations * nlig * MAXBOND *  sizeof(unsigned int);
        unsigned long int flexConfSize =  nconformations * sizeof(struct flexibility_data_t);
        unsigned long int flexSize = files *  sizeof(struct flexibility_params_t) + files * sizeof(type_data);
        unsigned long int linksSize = 0, linksfragmentsSize = 0, fragmentstamSize = 0, fragmentsSize = 0, boolSize = 0;
        unsigned long int tam_max, tam_tmp, nconformations_count_total, nconformations_count_partial, tam_conformation, stride_d;

        for (unsigned int i = 0; i < files; i++)
        {
                linksSize += flexibility_params[i].n_links * 2 * sizeof(unsigned int);
                linksfragmentsSize += flexibility_params[i].n_links * 2 * sizeof(unsigned int);
                fragmentstamSize += flexibility_params[i].n_fragments * sizeof(unsigned int);
                fragmentsSize += flexibility_params[i].n_links * (ligand_params[i].atoms - 2) * sizeof(unsigned int);
                boolSize += ligand_params[i].atoms * ligand_params[i].atoms * sizeof(bool);
        }

        max_positions = (moveSize_tam * 8) + moveSize_tam + energyConf_tam ;
        max_conf = (confSize_tam * 3) + confSize_tam + confSize_tam_char + confSize_bonds + flexConfSize;
        max_common = (recSize * 4) + typeSize + bondSize;
        max_flex_conf =  flexSize + linksSize + linksfragmentsSize + fragmentstamSize + fragmentsSize + boolSize;
        max_memory = max_positions + max_conf + max_common + max_flex_conf;
        tam_conformation =  (sizeof(type_data) * 8)  + sizeof(type_data) + sizeof(unsigned int) + (sizeof(type_data) * nlig * 3) + nlig * sizeof(type_data) +  nlig * sizeof(char) +  nlig * MAXBOND *  sizeof(unsigned int);

	cudaSetDevice(devices->id[orden_device]);
        cudaDeviceReset();
        cudaMemGetInfo(&tam_max, &total_m);
        //cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
        //std::cout << tam_max << " " << total_m << "\n";
        tam_tmp = tam_max - (tam_max * 0.2);
        if (tam_tmp < max_memory)
        {
                //printf("Trocear en 1 GPU\n");
                max_memory = max_common + max_flex_conf;
                nconformations_count_total = 0;
                nconformations_count_partial = 0;
                stride_d = 0;
                while (nconformations_count_total < nconformations)
                {
                        max_memory += tam_conformation;
                        if (max_memory > tam_tmp)
                        {
				//nconformations_count_total--;
                                //nconformations_count_partial--;
                                //printf("Trozo de GPUSOLVER %u %u %u stride_d %u stride_conformations %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d,stride_conformations,nconformations);
                                //Lanzar con nconformations_count_partial;
   			  	gpuSolver_vs_calculation_flex (conf_point,files,nlig,ligtype+(stride_d*nlig),bonds+(stride_d*nlig*MAXBOND),ql+(stride_d*nlig),ligand_params,flexibility_params,proteina,params,metaheuristic,devices,conformations_x+(stride_d*nlig), conformations_y+(stride_d*nlig),conformations_z+(stride_d*nlig),move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,nconf+stride_d,weights,f_params,nconformations_count_partial,orden_device,stride_d+stride_conformations);	
                                stride_d += nconformations_count_partial;
                                max_memory = max_common + max_flex_conf;
                                nconformations_count_partial = 0;
                        }
                        else
                        {
                                nconformations_count_total++;
                                nconformations_count_partial++;
                        }
                }
		nconformations_count_partial--;
                //printf("Trozo final de GPUSOLVER %u %u %u stride_d %u stride_conformations %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d,stride_conformations, nconformations);
                gpuSolver_vs_calculation_flex (conf_point,files,nlig,ligtype+(stride_d*nlig),bonds+(stride_d*nlig*MAXBOND),ql+(stride_d*nlig),ligand_params,flexibility_params,proteina,params,metaheuristic,devices,conformations_x+(stride_d*nlig), conformations_y+(stride_d*nlig),conformations_z+(stride_d*nlig),move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,nconf+stride_d,weights,f_params,nconformations_count_partial,orden_device,stride_d+stride_conformations);
        }
        else
        {
		printf("No Trocea\n");
                //Lanzar con todo
		gpuSolver_vs_calculation_flex (conf_point,files,nlig,ligtype,bonds,ql,ligand_params,flexibility_params,proteina,params,metaheuristic,devices,conformations_x, conformations_y, conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconf,weights,f_params,nconformations,orden_device,stride_conformations);
	}
			
}


extern void gpuSolver_vs_calculation_flex (unsigned int conf_point, unsigned int files, unsigned int nlig, char *ligtype, unsigned int *bonds, type_data *ql, struct ligand_params_t *ligand_params, struct flexibility_params_t *flexibility_params, struct receptor_t proteina, struct param_t params, struct metaheuristic_t metaheuristic, struct device_t *devices, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconf, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations,unsigned int orden_device, unsigned int stride_conformations)
{
        cudaError_t cudaStatus;
        type_data *internal_energy, *internal_energy_d, *energy_d, *move_x_d, *move_y_d, *move_z_d;
        type_data *conformations_x_d, *conformations_y_d, *conformations_z_d;

        char *ligtype_d;
        unsigned int *bonds_d;
        unsigned int *atoms, *atoms_d, *nlinks, *nlinks_d, stride;

        type_data *rec_x_d, *rec_y_d, *rec_z_d, *qr_d;
        char *rectype_d;
        unsigned int *bondsr_d;

        struct flexibility_params_t *flexibility_params_d;
        type_data *quat_d_x, *quat_d_y, *quat_d_z, *quat_d_w, *ql_d;
        unsigned int *n_conformations_d;

        unsigned long int energySize = nconformations * sizeof(type_data);
        cudaSetDevice(devices->id[orden_device]);
	cudaDeviceReset();
        //printf("Device %d nconfor %d nlig %d\n",devices->id[orden_device],nconformations,nlig);
        internal_energy = (type_data *)malloc(sizeof(type_data)*nconformations);
	nlinks = (unsigned int *)calloc(files,sizeof(unsigned int));
	for (unsigned int i=0;i<files;i++) nlinks[i]=flexibility_params[i].n_links;
        atoms = (unsigned int *)malloc(sizeof(unsigned int)*files);
        for (unsigned int i=0;i<files;i++){
		atoms[i]=ligand_params[i].atoms; 
		//printf("i %d atom %d\n",i,atoms[i]);
		//if (i==1360) printf(" %s\n",ligand_params[i].file);
	}
        //cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) printf ("Error dispositivo\n");
        dataToGPUFlexibility_Params(files,ligand_params,flexibility_params,flexibility_params_d);
        //exit(0);
        dataToGPUEnergy(nconformations, move_x, move_y, move_z, quat_x, quat_y, quat_z, quat_w, move_x_d, move_y_d, move_z_d, quat_d_x, quat_d_y, quat_d_z, quat_d_w);
        dataToGPUConformations(nconformations,nlig,conformations_x,conformations_x_d,conformations_y,conformations_y_d,conformations_z,conformations_z_d);
        //commonDataToGPU_vs (files,nconformations,nlig,proteina, proteina_d, atoms, atoms_d, ligtype, ligtype_d, bonds, bonds_d, ql, ql_d);
        commonDataToGPU_WS_vs (files,nconformations,nlig,proteina, rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d, atoms, atoms_d, nlinks, nlinks_d, ligtype, ligtype_d, bonds, bonds_d, ql, ql_d);
        //printf("LLega aki\n");
	//printf("LLega aki\n");
        //f_example<<<1,8>>>(files,atoms_d,flexibility_params_d);
        //fprintf(stderr, "CALCULO ENERGETICO!");
        cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) fprintf(stderr, "commonDataToGpu failed!");

        //exit(0);
//      PROTEINA Y PARAMETROS EN MEMORIA GLOBAL
        cudaStatus = cudaMalloc((void**) &internal_energy_d, energySize);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc energy_d");

        cudaStatus = cudaMalloc((void**) &energy_d, energySize);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc energy_d");
        cudaStatus = cudaMalloc((void**) &n_conformations_d, sizeof(unsigned int)*nconformations);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc n_conformations_d");

        //cudaStatus = cudaMemset (energy_d,0,energySize);
        cudaStatus = cudaMemcpy (n_conformations_d,nconf, sizeof(unsigned int)*nconformations, cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy n_conformations_d failed!");

        //guardar tipos de ligando en memoria constante
        save_params (weights,f_params);
        cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) fprintf(stderr, "COPY FAILED!");

	cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeFourByte);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_vs_by_warp, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_vs_by_warp_type1, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_vs_by_warp_type2, cudaFuncCachePreferEqual);

        cudaFuncSetCacheConfig(energy_internal_conformations_vs, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(energy_internal_conformations_vs_type2, cudaFuncCachePreferEqual);

        unsigned int blk_t = ceil(nconformations / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
        dim3 grid_t  (ceil(blk_t/8)+1, 8);
        dim3 hilos_t (devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);
        unsigned long int max_t = nconformations;

        unsigned int size = 5 * devices->hilos[orden_device*metaheuristic.num_kernels] * sizeof(type_data) + sizeof(char)*devices->hilos[orden_device*metaheuristic.num_kernels];
        unsigned int blk_e = ceil(nconformations*WARP_SIZE / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
        unsigned long int max_e = nconformations* WARP_SIZE;
        dim3 hilos_e (devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);
        dim3 grid_e  (ceil(blk_e/8)+1, 8);

        switch (params.scoring_function_type)
	{
		case 0: 
			Gpu_full_Kernel_Conformations_vs_by_warp <<<grid_e,hilos_e,size>>> (proteina.nrec,conf_point,atoms_d,nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d,conformations_y_d,conformations_z_d,move_x_d,move_y_d,move_z_d,quat_d_x, quat_d_y, quat_d_z, quat_d_w,ql_d,ligtype_d,bonds_d,energy_d,n_conformations_d,stride_conformations,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
			break;
		case 1:
                        Gpu_full_Kernel_Conformations_vs_by_warp_type1 <<<grid_e,hilos_e,size>>> (nlinks_d,proteina.nrec,conf_point,atoms_d,nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d,conformations_y_d,conformations_z_d,move_x_d,move_y_d,move_z_d,quat_d_x, quat_d_y, quat_d_z, quat_d_w,ql_d,ligtype_d,bonds_d,energy_d,n_conformations_d,stride_conformations,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
                        break;
		case 2:
                        Gpu_full_Kernel_Conformations_vs_by_warp_type2 <<<grid_e,hilos_e,size>>> (nlinks_d,proteina.nrec,conf_point,atoms_d,nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d,conformations_y_d,conformations_z_d,move_x_d,move_y_d,move_z_d,quat_d_x, quat_d_y, quat_d_z, quat_d_w,ql_d,ligtype_d,bonds_d,energy_d,n_conformations_d,stride_conformations,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
                        break;
	}

        cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) fprintf(stderr, "KERNEL FAILED ENERGY! %d ",cudaStatus);
	
	 //energy_internal_conformations_vs<<<grid_e,hilos_e,size>>>(conformations_x_d,conformations_y_d,conformations_z_d,atoms_d,nlig,flexibility_params_d,internal_energy_d,n_conformations_d,ql_d,ligtype_d,stride_conformations,devices->hilos[orden_device*metaheuristic.num_kernels],max_e,conf_point);
	switch (params.scoring_function_type)
	{
		case 0:	
		case 1:
			energy_internal_conformations_vs<<<grid_e,hilos_e>>>(conformations_x_d,conformations_y_d,conformations_z_d,atoms_d,nlig,flexibility_params_d,internal_energy_d,n_conformations_d,ql_d,ligtype_d,stride_conformations,devices->hilos[orden_device*metaheuristic.num_kernels],max_e,conf_point); //Esta es la buena
			break;
		case 2:
			printf("Hola\n");
			energy_internal_conformations_vs_type2<<<grid_e,hilos_e>>>(conformations_x_d,conformations_y_d,conformations_z_d,atoms_d,nlig,flexibility_params_d,internal_energy_d,n_conformations_d,ql_d,ligtype_d,stride_conformations,devices->hilos[orden_device*metaheuristic.num_kernels],max_e,conf_point); //Esta es la buena
                        break;
	}
        cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) fprintf(stderr, "KERNEL FAILED INTERNAL ENERGY!");
        //cudaMemcpy(internal_energy,internal_energy_d,nconformations*sizeof(type_data),cudaMemcpyDeviceToHost);
        //for (int j=0;j<nconformations;j++) printf("conf %d valor internal_energy %f\n",j,internal_energy[j]);
        //exit(0);
        energy_total <<<grid_t,hilos_t>>>(energy_d,internal_energy_d,max_t);
        cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) fprintf(stderr, "KERNEL FAILED TOTAL ENERGY en GPUSOLVER!");


        cudaStatus = cudaMemcpy (energy, energy_d, energySize, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy energy_d failed!");
	cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) fprintf(stderr, "Error copy energy \n");
        //fprintf(stderr, "FIN ENERGY!\n");
	free(atoms);
	free(internal_energy);
	free(nlinks);
        cudaFree(move_x_d);
        cudaFree(move_y_d);
        cudaFree(move_z_d);
        cudaFree(quat_d_x);
        cudaFree(quat_d_y);
        cudaFree(quat_d_z);
        cudaFree(quat_d_w);
        cudaFree(rec_x_d);
        cudaFree(rec_y_d);
        cudaFree(rec_z_d);
        cudaFree(qr_d);
        cudaFree(bondsr_d);
        cudaFree(rectype_d);
        cudaFree(atoms_d);
	cudaFree(nlinks_d);
        cudaFree(ql_d);
        cudaFree(ligtype_d);
        cudaFree(bonds_d);
        cudaFree(energy_d);
        cudaFree(n_conformations_d);

        cudaFree(internal_energy_d);
        cudaFree(conformations_x_d);
        cudaFree(conformations_y_d);
        cudaFree(conformations_z_d);
        cudaDeviceReset();


}
