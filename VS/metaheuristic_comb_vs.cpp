#include "energy_common.h"
#include "energy_CpuSolver_vs.h"
#include "energy_CpuSolver.h"
#include "energy_CpuSolver_common.h"
#include "energy_GpuSolver_vs.h"
#include "flexibility.h"
#include "rotation.h"
#include "wtime.h"
#include "metaheuristic_comb.h"
#include "metaheuristic_inc_vs.h"
#include "metaheuristic_comb_vs.h"
#include "metaheuristic_mgpu_vs.h"
#include <thrust/sequence.h>
#include <thrust/sort.h>
#include <thrust/functional.h>
#include <omp.h>

void combinar_individual_vs (unsigned int nlig, type_data *e_move_x, type_data *e_move_y, type_data *e_move_z, type_data *e_quat_x, type_data *e_quat_y, type_data *e_quat_z, type_data *e_quat_w, type_data *s_move_x, type_data *s_move_y, type_data *s_move_z, type_data *s_quat_x, type_data *s_quat_y, type_data *s_quat_z, type_data *s_quat_w , struct param_t param, unsigned int j, unsigned int k, unsigned int l)
{
	type_data local_x, local_y, local_z, local_w, quat_tmp_x, quat_tmp_y, quat_tmp_z, quat_tmp_w;
	type_data angle,eje[3];
	
	unsigned int pos_o = j;	
	unsigned int pos_d = k;
	unsigned int pos_s = l;

	s_move_x[pos_s] = (e_move_x[pos_o] + e_move_x[pos_d])/2;
	s_move_y[pos_s] = (e_move_y[pos_o] + e_move_y[pos_d])/2;
	s_move_z[pos_s] = (e_move_z[pos_o] + e_move_z[pos_d])/2;
	
	quat_tmp_x = e_quat_x[pos_o];
	quat_tmp_y = e_quat_y[pos_o];
	quat_tmp_z = e_quat_z[pos_o];
	quat_tmp_w = e_quat_w[pos_o];
	angle = getRealRandomNumber(param.rotation);
	eje[0] = getRealRandomNumber (1);
	eje[1] = getRealRandomNumber (1);
	eje[2] = getRealRandomNumber (1);
	setRotation_cpp (&local_x, &local_y, &local_z, &local_w, angle, eje);				
	composeRotation_cpp (&local_x, &local_y, &local_z, &local_w, &quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w, &quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w);
	normalize_cpp(&quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w);
	s_quat_x[pos_s] = quat_tmp_x;
	s_quat_y[pos_s] = quat_tmp_y;
	s_quat_z[pos_s] = quat_tmp_z;
	s_quat_w[pos_s] = quat_tmp_w;
}

extern void combinar_group_vs (unsigned int nlig, type_data *e_move_x, type_data *e_move_y, type_data *e_move_z, type_data *e_quat_x, type_data *e_quat_y, type_data *e_quat_z, type_data *e_quat_w, type_data *s_move_x, type_data *s_move_y, type_data *s_move_z, type_data *s_quat_x, type_data *s_quat_y, type_data *s_quat_z, type_data *s_quat_w , struct metaheuristic_t metaheuristic, unsigned int mejores, unsigned int peores, unsigned int mejorespeores, unsigned int stride_e, unsigned int stride_s, struct param_t param)
{	
	//int l = 0;
	unsigned int base = 0;
	unsigned int ind_m,ind_p,k,j;
	unsigned int div_m = metaheuristic.NMMCom_selec - 1;
	unsigned int div_p = metaheuristic.NPPCom_selec - 1;
	unsigned int div_mp = metaheuristic.NMPCom_selec - 1;
	unsigned int h2;	
	switch (param.mode) {
		
		case 1:
			h2 = 1;
			break;
		case 0:
		case 2:
		case 3:	
			h2 = 1;//metaheuristic.Threads2Com;
			break;
	}
	//omp_set_num_threads(h2);
	//#pragma omp parallel for private(j,k)
	if ((metaheuristic.NEMSel_selec > 0) && (mejores > 2))
	{
	//omp_set_num_threads(h2);
	//#pragma omp parallel for private(j,k)
		for (unsigned int m = 0; m < mejores; m++)
		{
			j = m / div_m;
			k = (div_m * (j+1)) - m;
			if (k <= j) k--;
			//printf("l = %d, j = %d, k = %d\n",m,j,k);
			combinar_individual_vs (nlig,e_move_x,e_move_y,e_move_z,e_quat_x,e_quat_y,e_quat_z,e_quat_w,s_move_x,s_move_y,s_move_z,s_quat_x,s_quat_y,s_quat_z,s_quat_w,param,j,k,m);
		}
	}
				
	//peores con peores
	if ((metaheuristic.NEPSel_selec > 0) && (peores > 2))
	{
		base =  metaheuristic.NMMCom_selec * ( metaheuristic.NMMCom_selec - 1);
		//#pragma omp parallel for private (j,k)
		for (unsigned int j = 0; j < metaheuristic.NPPCom_selec; j++)
			 for (unsigned int k = 0; k < metaheuristic.NPPCom_selec; k++)
				if (k != j)
				{
					combinar_individual_vs (nlig,e_move_x,e_move_y,e_move_z,e_quat_x,e_quat_y,e_quat_z,e_quat_w,s_move_x,s_move_y,s_move_z,s_quat_x,s_quat_y,s_quat_z,s_quat_w,param,j + metaheuristic.NEMSel_selec,k + metaheuristic.NEMSel_selec,base);	
					base++;
				}
			
	}
			
	//mejores con peores
	
	if ((metaheuristic.NEMSel_selec > 0) && (metaheuristic.NEPSel_selec > 0) && (mejorespeores > 2))
	{
		base = (metaheuristic.NMMCom_selec * ( metaheuristic.NMMCom_selec - 1)) + (metaheuristic.NPPCom_selec * ( metaheuristic.NPPCom_selec - 1));
		ind_m = getRandomNumber_modified(metaheuristic.NEMSel_selec-1,0);
		ind_p = getRandomNumber_modified(metaheuristic.NEPSel_selec-1,0);

		//#pragma omp parallel for private(ind_m,ind_p)
		for (unsigned int m = base; m < (mejorespeores + base); m++)
		{					
			//combinar_individual_vs_flex (nlig,vectors_e,vectors_s,metaheuristic,param,stride_e,stride_s,i,ind_m,ind_p+metaheuristic.NEMSel_selec,m);
			//printf("ind_m %d, ind_p %d, m %d\n",ind_m,ind_p,m);
			combinar_individual_vs (nlig,e_move_x,e_move_y,e_move_z,e_quat_x,e_quat_y,e_quat_z,e_quat_w,s_move_x,s_move_y,s_move_z,s_quat_x,s_quat_y,s_quat_z,s_quat_w,param,ind_m,ind_p + metaheuristic.NEMSel_selec,m);	
			ind_m = (ind_m + 1) % metaheuristic.NEMSel_selec;
			ind_p = (ind_p + 1) % metaheuristic.NEPSel_selec;
			if ((ind_p % metaheuristic.NEPSel_selec) == 0) ind_p++;
			//printf("ind_m %d, ind_p %d m %d\n",ind_m,ind_p,m);
			//l++;						
		}
	}		
}



extern void combinar_vs (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param)
{
	unsigned int mejores = 0, peores = 0, mejorespeores = 0;
	unsigned int stride_s, stride_e;	
	type_data tcombinargroupcpu, tenergycombinarcpu;
	type_data tcombinargroupcpu_t, tenergycombinarcpu_t;
	type_data *internal_energy;
	unsigned int h1;
	unsigned int *nlinks = (unsigned int *)calloc(vectors_e->files,sizeof(unsigned int));
	//omp_set_nested(1);
	switch (param.mode) {		
		case 1:		
			h1 = 1;				
			break;
		case 2:
			h1 = metaheuristic.Threads1Com;
			break;
	}
	vectors_s->files = vectors_e->files;
	vectors_s->nlig = vectors_e->nlig;
		
	mejores = metaheuristic.NMMCom_selec * (metaheuristic.NMMCom_selec - 1);	
	mejorespeores = metaheuristic.NMPCom_selec * (metaheuristic.NMPCom_selec - 1);	
	peores = metaheuristic.NPPCom_selec * (metaheuristic.NPPCom_selec - 1);
	stride_s = mejores + mejorespeores + peores;
	stride_e = metaheuristic.NEMSel_selec + metaheuristic.NEPSel_selec;
	//printf("Mejores: %d %d, peores: %d %d, mejorespeores: %d %d, stride_s: %d \n",mejores,metaheuristic.NMMCom_selec,peores,metaheuristic.NPPCom_selec,mejorespeores,metaheuristic.NMPCom_selec,stride_s);
	//exit(0);
	vectors_s->nconformations = vectors_e->files * stride_s;
	vectors_s->conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nlig*vectors_s->nconformations);
	vectors_s->conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nlig*vectors_s->nconformations);
	vectors_s->conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nlig*vectors_s->nconformations);
	
	vectors_s->ql = (type_data *)malloc(sizeof(type_data)*vectors_s->nlig*vectors_s->nconformations);
	vectors_s->ligtype = (char *)malloc(sizeof(char)*vectors_s->nlig*vectors_s->nconformations);
	vectors_s->subtype = (char *)malloc(sizeof(char)*vectors_s->nlig*SUBTYPEMAXLEN*vectors_s->nconformations);
	vectors_s->bonds = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nlig*MAXBOND*vectors_s->nconformations);
	vectors_s->nbonds = (char *)malloc(sizeof(char)*vectors_s->nlig*vectors_s->nconformations);

	vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nconformations);
	vectors_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->weights = (type_data *)malloc(SCORING_TERMS * sizeof(type_data));

	memcpy(vectors_s->f_params, vectors_e->f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_s->weights, vectors_e->weights,SCORING_TERMS * sizeof(type_data));
	
		
	omp_set_num_threads(h1);
	#pragma omp parallel for	
	for (unsigned int j=0;j<vectors_s->files;j++)
		for (unsigned int k=0;k<stride_s;k++)
		{
			memcpy(vectors_s->conformations_x + j*stride_s*vectors_s->nlig + k*vectors_s->nlig,vectors_e->conformations_x + j*stride_e*vectors_s->nlig,vectors_s->nlig*sizeof(type_data));
			memcpy(vectors_s->conformations_y + j*stride_s*vectors_s->nlig + k*vectors_s->nlig,vectors_e->conformations_y + j*stride_e*vectors_s->nlig,vectors_s->nlig*sizeof(type_data));
			memcpy(vectors_s->conformations_z + j*stride_s*vectors_s->nlig + k*vectors_s->nlig,vectors_e->conformations_z + j*stride_e*vectors_s->nlig,vectors_s->nlig*sizeof(type_data));
				
			memcpy(vectors_s->ql + j*stride_s*vectors_s->nlig + k*vectors_s->nlig,vectors_e->ql + j*stride_e*vectors_s->nlig,vectors_s->nlig*sizeof(type_data));
			memcpy(vectors_s->ligtype + j*stride_s*vectors_s->nlig + k*vectors_s->nlig,vectors_e->ligtype + j*stride_e*vectors_s->nlig,vectors_s->nlig*sizeof(char));
			memcpy(vectors_s->subtype + j*stride_s*SUBTYPEMAXLEN*vectors_s->nlig + k*vectors_s->nlig*SUBTYPEMAXLEN, vectors_e->subtype + j*stride_e*vectors_s->nlig*SUBTYPEMAXLEN, vectors_s->nlig*SUBTYPEMAXLEN*sizeof(char));
			memcpy(vectors_s->bonds + j*stride_s*MAXBOND*vectors_s->nlig + k*vectors_s->nlig*MAXBOND, vectors_e->bonds + j*stride_e*vectors_s->nlig*MAXBOND, vectors_s->nlig*MAXBOND*sizeof(unsigned int));
			memcpy(vectors_s->nbonds + j*stride_s*vectors_s->nlig + k*vectors_s->nlig, vectors_e->nbonds + j*stride_e*vectors_s->nlig, vectors_s->nlig*sizeof(char));
		}		
								
	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);	
	tcombinargroupcpu = wtime();
	//exit(0);	
	omp_set_num_threads(h1);
	#pragma omp parallel for		
	for (unsigned int i = 0; i < vectors_s->files;i++)	
	{							
		combinar_group_vs(vectors_s->nlig, (vectors_e->move_x + i*stride_e), (vectors_e->move_y + i*stride_e), (vectors_e->move_z + i*stride_e), (vectors_e->quat_x + i*stride_e), (vectors_e->quat_y + i*stride_e), (vectors_e->quat_z + i*stride_e), (vectors_e->quat_w + i*stride_e), (vectors_s->move_x + i*stride_s), (vectors_s->move_y + i*stride_s), (vectors_s->move_z + i*stride_s), (vectors_s->quat_x + i*stride_s), (vectors_s->quat_y + i*stride_s), (vectors_s->quat_z + i*stride_s), (vectors_s->quat_w + i*stride_s), metaheuristic, mejores, peores, mejorespeores, stride_e, stride_s, param);							
	}
	tcombinargroupcpu_t += wtime() - tcombinargroupcpu;			
	tenergycombinarcpu = wtime();			
	//exit(0);	
	tenergycombinarcpu = wtime();	
	ForcesCpuSolver_vs(nlinks,vectors_s->nlig,vectors_s->files,param,proteina,ligand_params,vectors_s->conformations_x,vectors_s->conformations_y,vectors_s->conformations_z,vectors_s->move_x,vectors_s->move_y,vectors_s->move_z,vectors_s->quat_x,vectors_s->quat_y,vectors_s->quat_z,vectors_s->quat_w,vectors_s->ligtype,vectors_s->ql,vectors_s->bonds,vectors_s->energy.energy,vectors_s->energy.n_conformation,vectors_s->weights,vectors_s->f_params,vectors_s->nconformations,metaheuristic);			
	tenergycombinarcpu_t += wtime() - tenergycombinarcpu;
		
	omp_set_num_threads(h1);
	#pragma omp parallel for				
	for (unsigned int i = 0; i < vectors_s->nconformations; i += stride_s)			
		thrust::stable_sort_by_key((vectors_s->energy.energy + i),(vectors_s->energy.energy + i + stride_s), (vectors_s->energy.n_conformation + i), thrust::less<type_data>());			
	//colocar_vs(vectors_s->nlig,param,vectors_s,metaheuristic);
	#pragma omp parallel for
	for (unsigned int i = 0; i < vectors_s->files; i++)
		colocar_vs_by_step (param,metaheuristic,vectors_s->nlig,vectors_s->move_x + (stride_s*i),vectors_s->move_y + (stride_s*i),vectors_s->move_z + (stride_s*i),vectors_s->quat_x + (stride_s*i),vectors_s->quat_y + (stride_s*i),vectors_s->quat_z + (stride_s*i),vectors_s->quat_w + (stride_s*i),vectors_s->energy.energy + (stride_s*i),vectors_s->energy.n_conformation + (stride_s*i),stride_s,i);
	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
}

extern void combinar_gpu_vs (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param, struct device_t *devices, unsigned int orden_device)
{
	unsigned int mejores, peores, mejorespeores,stride_e,stride_s;
	//cudaError_t cudaStatus;
	//cudaSetDevice(devices->id[orden_device]);
	//cudaDeviceReset();
	vectors_s->files = vectors_e->files;
	vectors_s->nlig = vectors_e->nlig;
			
	mejores = metaheuristic.NMMCom_selec * (metaheuristic.NMMCom_selec - 1);	
	mejorespeores = metaheuristic.NMPCom_selec * (metaheuristic.NMPCom_selec - 1);	
	peores = metaheuristic.NPPCom_selec * (metaheuristic.NPPCom_selec - 1);
	stride_s = mejores + mejorespeores + peores;
	stride_e = vectors_e->nconformations / vectors_e->files;
	//printf("Mejores: %d %d, peores: %d %d, mejorespeores: %d %d, stride_s: %d \n",mejores,metaheuristic.NMMCom_selec,peores,metaheuristic.NPPCom_selec,mejorespeores,metaheuristic.NMPCom_selec,stride_s);
	vectors_s->nconformations = vectors_e->files * stride_s;
	vectors_s->conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nlig*vectors_s->nconformations);
	vectors_s->conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nlig*vectors_s->nconformations);
	vectors_s->conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nlig*vectors_s->nconformations);
			
	vectors_s->ql = (type_data *)malloc(sizeof(type_data)*vectors_s->nlig*vectors_s->nconformations);
	vectors_s->ligtype = (char *)malloc(sizeof(char)*vectors_s->nlig*vectors_s->nconformations);
	vectors_s->subtype = (char *)malloc(sizeof(char)*vectors_s->nlig*SUBTYPEMAXLEN*vectors_s->nconformations);
	vectors_s->bonds = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nlig*MAXBOND*vectors_s->nconformations);
	vectors_s->nbonds = (char *)malloc(sizeof(char)*vectors_s->nlig*vectors_s->nconformations);

	vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nconformations);
	vectors_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);	

	vectors_s->weights = (type_data *)malloc(SCORING_TERMS * sizeof(type_data));

        memcpy(vectors_s->f_params, vectors_e->f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_s->weights, vectors_e->weights,SCORING_TERMS * sizeof(type_data));

				
	omp_set_num_threads(metaheuristic.Threads1Com);
	#pragma omp parallel for	
	for (unsigned int j=0;j<vectors_s->files;j++)
		for (unsigned int k=0;k<stride_s;k++)
		{
			memcpy(vectors_s->conformations_x + j*stride_s*vectors_s->nlig + k*vectors_s->nlig,vectors_e->conformations_x + j*stride_e*vectors_s->nlig,vectors_s->nlig*sizeof(type_data));
			memcpy(vectors_s->conformations_y + j*stride_s*vectors_s->nlig + k*vectors_s->nlig,vectors_e->conformations_y + j*stride_e*vectors_s->nlig,vectors_s->nlig*sizeof(type_data));
			memcpy(vectors_s->conformations_z + j*stride_s*vectors_s->nlig + k*vectors_s->nlig,vectors_e->conformations_z + j*stride_e*vectors_s->nlig,vectors_s->nlig*sizeof(type_data));
			
			memcpy(vectors_s->ql + j*stride_s*vectors_s->nlig + k*vectors_s->nlig,vectors_e->ql + j*stride_e*vectors_s->nlig,vectors_s->nlig*sizeof(type_data));
			memcpy(vectors_s->ligtype + j*stride_s*vectors_s->nlig + k*vectors_s->nlig,vectors_e->ligtype + j*stride_e*vectors_s->nlig,vectors_s->nlig*sizeof(char));
			memcpy(vectors_s->subtype + j*stride_s*SUBTYPEMAXLEN*vectors_s->nlig + k*vectors_s->nlig*SUBTYPEMAXLEN, vectors_e->subtype + j*stride_e*vectors_s->nlig*SUBTYPEMAXLEN, vectors_s->nlig*SUBTYPEMAXLEN*sizeof(char));
			memcpy(vectors_s->bonds + j*stride_s*MAXBOND*vectors_s->nlig + k*vectors_s->nlig*MAXBOND, vectors_e->bonds + j*stride_e*vectors_s->nlig*MAXBOND, vectors_s->nlig*MAXBOND*sizeof(unsigned int));
			memcpy(vectors_s->nbonds + j*stride_s*vectors_s->nlig + k*vectors_s->nlig, vectors_e->nbonds + j*stride_e*vectors_s->nlig, vectors_s->nlig*sizeof(char));
		}		
						
	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);	
	
	omp_set_num_threads(metaheuristic.Threads1Com);
	#pragma omp parallel for		
	for (unsigned int i = 0; i < vectors_s->files;i++)	
	{				
		combinar_group_vs(vectors_s->nlig, (vectors_e->move_x + i*stride_e), (vectors_e->move_y + i*stride_e), (vectors_e->move_z + i*stride_e), (vectors_e->quat_x + i*stride_e), (vectors_e->quat_y + i*stride_e), (vectors_e->quat_z + i*stride_e), (vectors_e->quat_w + i*stride_e), (vectors_s->move_x + i*stride_s), (vectors_s->move_y + i*stride_s), (vectors_s->move_z + i*stride_s), (vectors_s->quat_x + i*stride_s), (vectors_s->quat_y + i*stride_s), (vectors_s->quat_z + i*stride_s), (vectors_s->quat_w + i*stride_s), metaheuristic, mejores, peores, mejorespeores, stride_e, stride_s, param);							
	}
			
	unsigned long int max_conformaciones = ((devices->propiedades[0].maxGridSize[0] * devices->hilos[0*metaheuristic.num_kernels]) / WARP_SIZE) * 8;
	
	if (vectors_s->nconformations < max_conformaciones)
		gpuSolver_vs (stride_s,vectors_s->files,vectors_s->nlig,vectors_s->ligtype,vectors_s->bonds,vectors_s->ql,ligand_params,proteina,param,metaheuristic,devices,vectors_s->conformations_x, vectors_s->conformations_y, vectors_s->conformations_z,vectors_s->move_x,vectors_s->move_y,vectors_s->move_z,vectors_s->quat_x,vectors_s->quat_y,vectors_s->quat_z,vectors_s->quat_w,vectors_s->energy.energy,vectors_s->energy.n_conformation,vectors_s->weights, vectors_s->f_params,vectors_s->nconformations,0,0);		
	else
	{
		//DIVIDIR EN TROZOS
		while((vectors_s->nconformations % max_conformaciones) != 0) max_conformaciones --;		
		//printf("va a trocear\n");				
		for (unsigned int desplazamiento=0;desplazamiento<vectors_s->nconformations;desplazamiento+=max_conformaciones)
		{
			gpuSolver_vs (stride_s,vectors_s->files,vectors_s->nlig,vectors_s->ligtype+(desplazamiento*vectors_s->nlig),vectors_s->bonds+(desplazamiento*vectors_s->nlig*MAXBOND),vectors_s->ql+(desplazamiento*vectors_s->nlig),ligand_params,proteina,param,metaheuristic,devices,vectors_s->conformations_x+(desplazamiento*vectors_s->nlig), vectors_s->conformations_y+(desplazamiento*vectors_s->nlig),vectors_s->conformations_z+(desplazamiento*vectors_s->nlig), vectors_s->move_x+desplazamiento,vectors_s->move_y+desplazamiento,vectors_s->move_z+desplazamiento,vectors_s->quat_x+desplazamiento,vectors_s->quat_y+desplazamiento,vectors_s->quat_z+desplazamiento,vectors_s->quat_w+desplazamiento,vectors_s->energy.energy+desplazamiento,vectors_s->energy.n_conformation+desplazamiento,vectors_s->weights,vectors_s->f_params,max_conformaciones,0,desplazamiento);
			//printf("Device %d con trozo %d\n",devices->id[th],i);					
		}
	}
	//for(int k=0;k<(vectors_s->nconformations*vectors_s->nlig);k++)
		//	printf("comb-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",k,vectors_s->conformations_x[k],vectors_s->conformations_y[k],vectors_s->conformations_z[k],vectors_s->move_x[k/vectors_s->nlig],vectors_s->move_y[k/vectors_s->nlig],vectors_s->move_z[k/vectors_s->nlig],vectors_s->energy.energy[k/vectors_s->nlig]);	
	//exit(0);
			
	omp_set_num_threads(metaheuristic.Threads1Com);
	#pragma omp parallel for				
	for (unsigned int i = 0; i < vectors_s->nconformations; i += stride_s)			
		thrust::stable_sort_by_key((vectors_s->energy.energy + i),(vectors_s->energy.energy + i + stride_s), (vectors_s->energy.n_conformation + i), thrust::less<type_data>());			
	//colocar_vs(vectors_s->nlig,param,vectors_s,metaheuristic);
	#pragma omp parallel for
	for (unsigned int i = 0; i < vectors_s->files; i++)
		colocar_vs_by_step (param,metaheuristic,vectors_s->nlig,vectors_s->move_x + (stride_s*i),vectors_s->move_y + (stride_s*i),vectors_s->move_z + (stride_s*i),vectors_s->quat_x + (stride_s*i),vectors_s->quat_y + (stride_s*i),vectors_s->quat_z + (stride_s*i),vectors_s->quat_w + (stride_s*i),vectors_s->energy.energy + (stride_s*i),vectors_s->energy.n_conformation + (stride_s*i),stride_s,i);
	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
			
}


extern void combinar_multigpu_vs (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param, struct device_t *devices)
{
	unsigned int mejores,peores,mejorespeores,stride_s,stride_e;
	struct receptor_t *proteina_d;
	
	vectors_s->files = vectors_e->files;
	vectors_s->nlig = vectors_e->nlig;
			
	mejores = metaheuristic.NMMCom_selec * (metaheuristic.NMMCom_selec - 1);	
	mejorespeores = metaheuristic.NMPCom_selec * (metaheuristic.NMPCom_selec - 1);	
	peores = metaheuristic.NPPCom_selec * (metaheuristic.NPPCom_selec - 1);
	stride_s = mejores + mejorespeores + peores;
	stride_e = vectors_e->nconformations / vectors_e->files;
	//printf("Mejores: %d %d, peores: %d %d, mejorespeores: %d %d, stride_s: %d \n",mejores,metaheuristic.NMMCom_selec,peores,metaheuristic.NPPCom_selec,mejorespeores,metaheuristic.NMPCom_selec,stride_s);
	vectors_s->nconformations = vectors_e->files * stride_s;
	vectors_s->conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nlig*vectors_s->nconformations);
	vectors_s->conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nlig*vectors_s->nconformations);
	vectors_s->conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nlig*vectors_s->nconformations);
			
	vectors_s->ql = (type_data *)malloc(sizeof(type_data)*vectors_s->nlig*vectors_s->nconformations);
	vectors_s->ligtype = (char *)malloc(sizeof(char)*vectors_s->nlig*vectors_s->nconformations);
	vectors_s->subtype = (char *)malloc(sizeof(char)*vectors_s->nlig*SUBTYPEMAXLEN*vectors_s->nconformations);
	vectors_s->bonds = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nlig*MAXBOND*vectors_s->nconformations);
	vectors_s->nbonds = (char *)malloc(sizeof(char)*vectors_s->nlig*vectors_s->nconformations);

	vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nconformations);
	vectors_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);	

	vectors_s->weights = (type_data *)malloc(SCORING_TERMS * sizeof(type_data));

        memcpy(vectors_s->f_params, vectors_e->f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_s->weights, vectors_e->weights,SCORING_TERMS * sizeof(type_data));

	omp_set_num_threads(metaheuristic.Threads1Com);
	#pragma omp parallel for	
	for (unsigned int j=0;j<vectors_s->files;j++)
		for (unsigned int k=0;k<stride_s;k++)
		{
			memcpy(vectors_s->conformations_x + j*stride_s*vectors_s->nlig + k*vectors_s->nlig,vectors_e->conformations_x + j*stride_e*vectors_s->nlig,vectors_s->nlig*sizeof(type_data));
			memcpy(vectors_s->conformations_y + j*stride_s*vectors_s->nlig + k*vectors_s->nlig,vectors_e->conformations_y + j*stride_e*vectors_s->nlig,vectors_s->nlig*sizeof(type_data));
			memcpy(vectors_s->conformations_z + j*stride_s*vectors_s->nlig + k*vectors_s->nlig,vectors_e->conformations_z + j*stride_e*vectors_s->nlig,vectors_s->nlig*sizeof(type_data));
			
			memcpy(vectors_s->ql + j*stride_s*vectors_s->nlig + k*vectors_s->nlig,vectors_e->ql + j*stride_e*vectors_s->nlig,vectors_s->nlig*sizeof(type_data));
			memcpy(vectors_s->ligtype + j*stride_s*vectors_s->nlig + k*vectors_s->nlig,vectors_e->ligtype + j*stride_e*vectors_s->nlig,vectors_s->nlig*sizeof(char));
			memcpy(vectors_s->subtype + j*stride_s*SUBTYPEMAXLEN*vectors_s->nlig + k*vectors_s->nlig*SUBTYPEMAXLEN, vectors_e->subtype + j*stride_e*vectors_s->nlig*SUBTYPEMAXLEN, vectors_s->nlig*SUBTYPEMAXLEN*sizeof(char));
			memcpy(vectors_s->bonds + j*stride_s*MAXBOND*vectors_s->nlig + k*vectors_s->nlig*MAXBOND, vectors_e->bonds + j*stride_e*vectors_s->nlig*MAXBOND, vectors_s->nlig*MAXBOND*sizeof(unsigned int));
			memcpy(vectors_s->nbonds + j*stride_s*vectors_s->nlig + k*vectors_s->nlig, vectors_e->nbonds + j*stride_e*vectors_s->nlig, vectors_s->nlig*sizeof(char));
		}		
						
	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);	
	
	omp_set_num_threads(metaheuristic.Threads1Com);
	#pragma omp parallel for		
	for (unsigned int i = 0; i < vectors_s->files;i++)	
	{				
		combinar_group_vs(vectors_s->nlig, (vectors_e->move_x + i*stride_e), (vectors_e->move_y + i*stride_e), (vectors_e->move_z + i*stride_e), (vectors_e->quat_x + i*stride_e), (vectors_e->quat_y + i*stride_e), (vectors_e->quat_z + i*stride_e), (vectors_e->quat_w + i*stride_e), (vectors_s->move_x + i*stride_s), (vectors_s->move_y + i*stride_s), (vectors_s->move_z + i*stride_s), (vectors_s->quat_x + i*stride_s), (vectors_s->quat_y + i*stride_s), (vectors_s->quat_z + i*stride_s), (vectors_s->quat_w + i*stride_s), metaheuristic, mejores, peores, mejorespeores, stride_e, stride_s, param);							
	}
	
	multigpuSolver_vs (vectors_s->files,vectors_s->nlig, vectors_s->ligtype, vectors_s->bonds, vectors_s->ql, ligand_params, proteina, vectors_s->conformations_x, vectors_s->conformations_y, vectors_s->conformations_z, vectors_s->move_x, vectors_s->move_y, vectors_s->move_z, vectors_s->quat_x, vectors_s->quat_y, vectors_s->quat_z, vectors_s->quat_w, vectors_s->energy, vectors_s->weights, vectors_s->f_params, vectors_s->nconformations, param, metaheuristic, devices);				
	
	omp_set_num_threads(metaheuristic.Threads1Com);
	#pragma omp parallel for				
	for (unsigned int i = 0; i < vectors_s->nconformations; i += stride_s)			
		thrust::stable_sort_by_key((vectors_s->energy.energy + i),(vectors_s->energy.energy + i + stride_s), (vectors_s->energy.n_conformation + i), thrust::less<type_data>());			
	//colocar_vs(vectors_s->nlig,param,vectors_s,metaheuristic);
	#pragma omp parallel for
	for (unsigned int i = 0; i < vectors_s->files; i++)
		colocar_vs_by_step (param,metaheuristic,vectors_s->nlig,vectors_s->move_x + (stride_s*i),vectors_s->move_y + (stride_s*i),vectors_s->move_z + (stride_s*i),vectors_s->quat_x + (stride_s*i),vectors_s->quat_y + (stride_s*i),vectors_s->quat_z + (stride_s*i),vectors_s->quat_w + (stride_s*i),vectors_s->energy.energy + (stride_s*i),vectors_s->energy.n_conformation + (stride_s*i),stride_s,i);
	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
	
}	

