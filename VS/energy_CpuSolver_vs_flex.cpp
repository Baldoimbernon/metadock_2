#include "energy_common.h"
#include "rotation.h"
#include "energy_CpuSolver.h"
#include "energy_CpuSolver_vs.h"
#include "energy_CpuSolver_common.h"
#include "flexibility_vs.h"
#include <omp.h>

using namespace std;

extern void generate_positions_cpp_vs_flex (struct param_t param, struct vectors_t *vectors, struct metaheuristic_t metaheuristic, struct flexibility_params_t *flexibility_params,struct flexibility_data_t * flexibility_conformations)
{
	
	unsigned int steps,moveType=MOVE;
	steps = 0;
	param.steps = 20;
	//printf("x %f, y %f, z %f %d\n",vectors->ac_x,vectors->ac_y,vectors->ac_z,vectors->nconformations);
	//exit(0);
	init_quat(vectors->quat_x,vectors->quat_y,vectors->quat_z,vectors->quat_w,vectors->nconformations);
	while (steps < param.steps){		
		if (moveType == MOVE){			
			move_vs(vectors->files,vectors->move_x,vectors->move_y,vectors->move_z,vectors->ac_x,vectors->ac_y,vectors->ac_z,param,metaheuristic,metaheuristic.NEIIni);
		}
		if (moveType == ROTATE) {
			rotate_vs(vectors->quat_x,vectors->quat_y,vectors->quat_z,vectors->quat_w,param,vectors->nconformations,metaheuristic);
		}
		if (moveType == FLEX) {
			angulations_conformations_cpp_vs (vectors->nconformations,vectors->files,metaheuristic,param,flexibility_conformations,flexibility_params,param.flex_angle);
			flexibility_cpp_vs (vectors->nconformations,vectors->files,metaheuristic,param,vectors->conformations_x,vectors->conformations_y, vectors->conformations_z, flexibility_conformations,flexibility_params,vectors->nlig, param.flex_angle);													
		}
		if (moveType == MOVE) moveType = ROTATE;
		else if (moveType == ROTATE) moveType = FLEX;
		else if (moveType == FLEX) moveType = MOVE;
		steps++;	
	}
	//for (int i=0;i<vectors->nconformations;i++) 
	//	printf("id %d m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f\n",i,vectors->move_x[i],vectors->move_y[i],vectors->move_z[i],vectors->quat_x[i],vectors->quat_y[i],vectors->quat_z[i]);
	//		exit(0);
	
}

type_data distance_cpu_vs (type_data *lig_x, type_data *lig_y, type_data *lig_z, unsigned int i, unsigned int j)
{
	type_data difx,dify,difz,mod2x,mod2y,mod2z,dist;

	difx = lig_x[j] - lig_x[i];
	dify = lig_y[j] - lig_y[i];
	difz = lig_z[j] - lig_z[i];

	mod2x = difx * difx;
	mod2y = dify * dify;
	mod2z = difz * difz;
	
	dist = mod2x + mod2y + mod2z;
	return (dist);

}

extern void energy_internal_conformations_cpu_vs_type2 (unsigned int n, unsigned int nlig, unsigned int files, struct param_t param, struct metaheuristic_t metaheuristic, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, struct ligand_params_t *ligand_params, struct flexibility_params_t *flexibility_params, type_data *weights, struct force_field_param_t *f_params, type_data *energy, type_data *ql, char *ligtype)
{	
	char ind1,ind2;
	unsigned int h1,h2;
	type_data temp_vdw=0, temp_es=0,rij;
    type_data temp_total;
    double tmpconst,cA,cB,rA,rB,dxA,dxB;
    type_data ligx,ligy,ligz,distx,disty,distz,mx,my,mz,temp_dist;
    type_data eps, sig, epslig, siglig, term6,term12,temp;
    type_data miatomo[3];

    type_data qaux_x,qaux_y,qaux_z,qaux_w;
	unsigned int atoms, id_file, id_conformation,tam;
	unsigned int total;
	
	total = n * nlig;
	tam = n / files;
	//recorremos todas las conformaciones
	switch (param.mode) {
		case 1:
			h1 = 1;
			break;
		case 2:
			h1 = metaheuristic.Threads1Fit; 
			break;
	}
	
	//h2 = metaheuristic.Threads2Fit;
								
	//omp_set_nested(1);
	omp_set_num_threads (h1);
	#pragma omp parallel for private(temp_es,temp_vdw,id_conformation,id_file,atoms,ind1,ind2,temp_dist,rij,term6,term12,eps,sig,temp,tmpconst,cA,cB,rA,rB,dxA,dxB)			
	for (unsigned int k=0; k < total; k+=nlig)
	{	
		temp_es = 0;
		temp_vdw = 0;
		id_conformation = k / nlig;
		id_file = id_conformation / tam;
		atoms = ligand_params[id_file].atoms;
		//printf("k %d nlig %d id_conformation %d id_file %datoms %d\n",k,nlig,id_conformation,id_file,atoms);
		
		for (unsigned int i=0;i<(atoms-1);i++)
		{
			ind1 = ligtype[id_conformation*nlig + i];
			for (unsigned int j=i+1;j<atoms;j++)
			{				
				temp_dist = distance_cpu_vs((conformations_x + k),(conformations_y + k), (conformations_z + k),i,j);
				rij = sqrtf(temp_dist);
				
				double dato_eij = calc_ddd_Mehler_Solmajer(rij,APPROX_ZERO);
				ind2 = ligtype[k + j];
				eps = sqrt(f_params[ind1].epsilon * f_params[ind2].epsilon);
                sig = ((f_params[ind1].sigma + f_params[ind2].sigma))*0.5L;
				
				tmpconst = eps / (double)(xA - xB);
				cA =  tmpconst * pow( (double)sig, (double)xA ) * xB;
                cB =  tmpconst * pow( (double)sig, (double)xB ) * xA;
				dxA = (double) xA;
                dxB = (double) xB;
				rA = pow( rij, dxA);
                rB = pow( rij, dxB);
				term6 = cB / rB;//pow((double)dist, (double)xB);
                term12 = cA / rA;//pow((double)dist, (double)xA);
                temp_es += ((1.0 / (dato_eij * rij)) * ql[j] * ELECSCALE * weights[2] * !flexibility_params[id_file].individual_bonds_ES[(i * atoms) + j]);
                //if (dist < 7.0)
                temp_vdw += ((term12 - term6) * !flexibility_params[id_file].individual_bonds_VDW[(i*atoms) + j]);
			}
			temp_es *= ql[i];
		}		
		temp = temp_es + (temp_vdw + weights[0]);
        energy[id_conformation] = temp;
	}
}


extern void energy_internal_conformations_cpu_vs (unsigned int n, unsigned int nlig, unsigned int files, struct param_t param, struct metaheuristic_t metaheuristic, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, struct ligand_params_t *ligand_params, struct flexibility_params_t *flexibility_params, type_data *weights, struct force_field_param_t *f_params, type_data *energy, type_data *ql, char *ligtype) {

	char ind1, ind2;
	type_data temp_dist, temp_es, temp_vdw, temp, rij;
	type_data term6, term12, eps, sig;
	unsigned int atoms, id_file, id_conformation,tam;
	unsigned int total;
	unsigned int h1;
	//atoms = ligand_params[id_file].atoms;
	total = n * nlig;
	tam = n / files;
	//recorremos todas las conformaciones
	switch (param.mode) {
		case 1:
			h1 = 1;
			break;
		case 2:
			h1 = metaheuristic.Threads1Fit; 
			break;
	}
	
	//h2 = metaheuristic.Threads2Fit;
								
	//omp_set_nested(1);
	omp_set_num_threads (h1);
	#pragma omp parallel for private(temp_es,temp_vdw,id_conformation,id_file,atoms,ind1,ind2,temp_dist,rij,term6,eps,sig,temp)			
	for (unsigned int k=0; k < total; k+=nlig)
	{	
		temp_es = 0;
		temp_vdw = 0;
		id_conformation = k / nlig;
		id_file = id_conformation / tam;
		atoms = ligand_params[id_file].atoms;
		//printf("k %d nlig %d id_conformation %d id_file %datoms %d\n",k,nlig,id_conformation,id_file,atoms);
		
		for (unsigned int i=0;i<(atoms-1);i++)
		{
			ind1 = ligtype[id_conformation*nlig + i];
			for (unsigned int j=i+1;j<atoms;j++)
			{				
				temp_dist = distance_cpu_vs((conformations_x + k),(conformations_y + k), (conformations_z + k),i,j);
				rij = sqrtf(temp_dist);
				temp_es += ql[k + i] * (1/rij) * ql[k + j] * !flexibility_params[id_file].individual_bonds_ES[(i * atoms) + j];		
				ind2 = ligtype[k + j];
				//eps = sqrtf(vdw_params[ind1].epsilon * vdw_params[ind2].epsilon);
				//sig = vdw_params[ind1].sigma * vdw_params[ind2].sigma;
				eps = sqrtf(f_params[ind1].epsilon * f_params[ind2].epsilon);
				sig = f_params[ind1].sigma * f_params[ind2].sigma;
				term6 = sig / temp_dist;
				term6 = term6 * term6 * term6; // ^6
				temp_vdw += 4.0*eps*term6*(term6 - 1.0) * !flexibility_params[id_file].individual_bonds_VDW[(i*atoms) + j];
			}
		}		
		temp_es *= (ES_CONSTANT * weights[2]);
                temp = temp_es + (temp_vdw + weights[0]);
                energy[id_conformation] = temp;
	}
}



