#include "definitions.h"
#include "wtime.h"
#include "energy_common.h"
#include "energy_struct.h"
#include "energy_GpuSolver_vs.h"
#include "energy_positions_vs_flex.h"
#include "energy_montecarlo_vs.h"
#include "metaheuristic_mgpu.h"
#include <omp.h>
#include <thrust/sequence.h>

using namespace std;
		
extern void multigpuSolver_vs_flex (unsigned int files, unsigned int nlig, char* ligtype, unsigned int *bonds, type_data *ql, struct ligand_params_t *ligand_params, struct flexibility_params_t *flexibility_params, struct receptor_t proteina, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, struct energy_t energy, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices)
{
	double tiempo_i, tiempo_f;
	unsigned int  pc_temp, dev, stride_temp,ttotal = 0;
	
	pc_temp = nconformations;// / files;
	devices->stride[0] = 0;	
	/*stride_temp = floor((devices->trabajo[0] * 0.01));//files);	
	devices->conformaciones[0] = stride_temp * pc_temp;
	ttotal = stride_temp;
	printf("trabajo 0 %d trabajo 1 %d\n",devices->trabajo[0],devices->trabajo[1]);
	//exit(0);
	printf("nconformations %d, files %d pc_temp %d\n",nconformations,files,pc_temp);	
	
	for (unsigned int i=(devices->n_devices - 1);i > 0;i--)
	{
		stride_temp = floor((devices->trabajo[i] * 0.01));//files);
		devices->conformaciones[i] = stride_temp * pc_temp;
		ttotal += stride_temp;
	}
	unsigned int diferencia = pc_temp-ttotal;//files - ttotal;
	for (unsigned int i=0;i<diferencia;i++)
	{
		dev = i % devices->n_devices;
		devices->conformaciones[dev] ++;//= pc_temp;
	}
	for (unsigned int i=(devices->n_devices-1);i > 0;i--)
	{
		devices->stride[i] = nconformations - devices->conformaciones[i];
		nconformations = nconformations - devices->conformaciones[i];		
	}*/
	for (int i=0;i < devices->n_devices;i++)
        {
                stride_temp = floor((devices->trabajo[i] * 0.01)*nconformations);
                devices->conformaciones[i] = stride_temp;
                ttotal += stride_temp;
        }
        int diferencia = nconformations - ttotal;
        for (int i=0;i<diferencia;i++)
        {
                dev = i % devices->n_devices;
                devices->conformaciones[dev] ++;
        }
        for (int i=(devices->n_devices-1);i > 0;i--)
        {
                devices->stride[i] = nconformations - devices->conformaciones[i];
                nconformations = nconformations - devices->conformaciones[i];
        }	
	tiempo_i = wtime();
	omp_set_num_threads(devices->n_devices);
	//for (int i =0;i<423168;i++) 
	//	printf("conf %d, move_x %f, quat_w %f, energy %f\n",energy.n_conformation[i],move_x[i],quat_w[i],energy.energy[i]);
	//exit(0);
	#pragma omp parallel for 	
	for (unsigned int j=0;j<devices->n_devices;j++)
	{
		unsigned int th = omp_get_thread_num();
		unsigned int stride_d = devices->stride[j];
		unsigned long long int max_conformaciones = (devices->propiedades[j].maxGridSize[0] * (devices->hilos[j*metaheuristic.num_kernels] / WARP_SIZE)) * 8;
		
		printf("INT Th %d j %d stride %d conformaciones %d max_conformaciones %lu\n",th,j,stride_d,devices->conformaciones[j],max_conformaciones);
		if (devices->conformaciones[j] < max_conformaciones)
			gpuSolver_vs_flex (pc_temp,files,nlig,ligtype+(stride_d*nlig),bonds+(stride_d*nlig*MAXBOND),ql+(stride_d*nlig),ligand_params,flexibility_params,proteina,param,metaheuristic,devices,conformations_x+(stride_d*nlig),conformations_y+(stride_d*nlig),conformations_z+(stride_d*nlig),move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy.energy+stride_d,energy.n_conformation+stride_d,weights,f_params,devices->conformaciones[j],j,stride_d);		
			
		else
		{
			//DIVIDIR EN TROZOS
			while(devices->conformaciones[j] % max_conformaciones != 0) max_conformaciones --;
			//printf("va a trocear\n");			
			for (unsigned int desplazamiento=0;desplazamiento<devices->conformaciones[j];desplazamiento+=max_conformaciones)
			{
				gpuSolver_vs_flex (pc_temp,files,nlig,ligtype+desplazamiento*nlig,bonds+desplazamiento*nlig*MAXBOND,ql+desplazamiento*nlig,ligand_params,flexibility_params,proteina,param,metaheuristic,devices,conformations_x+((stride_d+desplazamiento)*nlig),conformations_y+((stride_d+desplazamiento)*nlig),conformations_z+((stride_d+desplazamiento)*nlig),move_x+stride_d+desplazamiento,move_y+stride_d+desplazamiento,move_z+stride_d+desplazamiento,quat_x+stride_d+desplazamiento,quat_y+stride_d+desplazamiento,quat_z+stride_d+desplazamiento,quat_w+stride_d+desplazamiento,energy.energy+stride_d+desplazamiento,energy.n_conformation+stride_d+desplazamiento,weights,f_params,max_conformaciones,j,stride_d+desplazamiento);		
				//printf("Device %d con trozo %d\n",devices->id[th],i);				
			}
		}
	}
	tiempo_f = wtime() - tiempo_i;
	
}

extern void multigpuSolver_mejora_vs_flex_environment (unsigned int files, unsigned int nlig, char* ligtype, unsigned int *bonds, type_data *ql, struct ligand_params_t *ligand_params, struct flexibility_params_t *flexibility_params, struct receptor_t proteina, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z , type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int fase)
{
	struct vectors_t vectors_mejora;
        size_t available,total_m;
    cudaError_t cudaStatus;
    unsigned long int max_memory;
    unsigned long int max_positions;
    unsigned long int max_common;
    unsigned long int max_conf;
    unsigned long int typeSize = proteina.nrec * sizeof(char);
    unsigned long int recSize = proteina.nrec * sizeof(type_data);
    unsigned long int bondSize = proteina.nrec * sizeof(int) * MAXBOND;
    unsigned long int randSize;
    unsigned long int moveSize_tam;
    unsigned long int energyConf_tam;
    unsigned long int confSize_tam;
    unsigned long int confSize_tam_char;
    unsigned long int confSize_bonds;

     unsigned long int randSize_environment;
    unsigned long int moveSize_tam_environment;
    unsigned long int energyConf_tam_environment;
    unsigned long int confSize_tam_environment;
    unsigned long int confSize_tam_char_environment;
    unsigned long int confSize_bonds_environment;

    unsigned long int tam_max, tam_tmp, nconformations_count_total, nconformations_count_partial, tam_conformation, stride_d, stride_d_memory;

        double tiempo_i, tiempo_f;
        unsigned int  pc_temp, dev, stride_temp,ttotal = 0;
        unsigned int backup_conformations = nconformations;

        pc_temp = nconformations;// / files;
	devices->stride[0] = 0;	
	/*stride_temp = floor((devices->trabajo[0] * 0.01));//files);	
	devices->conformaciones[0] = stride_temp * pc_temp;
	ttotal = stride_temp;
	printf("trabajo 0 %d trabajo 1 %d\n",devices->trabajo[0],devices->trabajo[1]);
	//exit(0);
	printf("nconformations %d, files %d pc_temp %d\n",nconformations,files,pc_temp);	
	
	for (unsigned int i=(devices->n_devices - 1);i > 0;i--)
	{
		stride_temp = floor((devices->trabajo[i] * 0.01));//files);
		devices->conformaciones[i] = stride_temp * pc_temp;
		ttotal += stride_temp;
	}
	unsigned int diferencia = pc_temp-ttotal;//files - ttotal;
	for (unsigned int i=0;i<diferencia;i++)
	{
		dev = i % devices->n_devices;
		devices->conformaciones[dev] ++;//= pc_temp;
	}
	for (unsigned int i=(devices->n_devices-1);i > 0;i--)
	{
		devices->stride[i] = nconformations - devices->conformaciones[i];
		nconformations = nconformations - devices->conformaciones[i];		
	}*/	
	for (int i=0;i < devices->n_devices;i++)
        {
                stride_temp = floor((devices->trabajo[i] * 0.01)*nconformations);
                devices->conformaciones[i] = stride_temp;
                ttotal += stride_temp;
        }
        int diferencia = nconformations - ttotal;
        for (int i=0;i<diferencia;i++)
        {
                dev = i % devices->n_devices;
                devices->conformaciones[dev] ++;
        }
        for (int i=(devices->n_devices-1);i > 0;i--)
        {
                devices->stride[i] = nconformations - devices->conformaciones[i];
                nconformations = nconformations - devices->conformaciones[i];
        }
	nconformations = pc_temp;

        omp_set_num_threads(devices->n_devices);

	vectors_mejora.files = files;
        vectors_mejora.nlig = nlig;
        vectors_mejora.nconformations = nconformations * metaheuristic.NEEImp;

        vectors_mejora.conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations*vectors_mejora.nlig);
        vectors_mejora.conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations*vectors_mejora.nlig);
        vectors_mejora.conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations*vectors_mejora.nlig);

        vectors_mejora.ql = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations*vectors_mejora.nlig);
        vectors_mejora.ligtype = (char *)malloc(sizeof(char)*vectors_mejora.nconformations*vectors_mejora.nlig);
        vectors_mejora.bonds = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations*vectors_mejora.nlig*MAXBOND);

        vectors_mejora.move_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.move_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.move_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations);
        vectors_mejora.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.weights = (type_data *)malloc(sizeof(type_data)*SCORING_TERMS);

        memcpy(vectors_mejora.f_params, f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_mejora.weights,weights,SCORING_TERMS * sizeof(type_data));

        fill_conformations_environment_vs (vectors_mejora.nlig,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,ligtype,ql,bonds,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,vectors_mejora.ligtype,vectors_mejora.ql,vectors_mejora.bonds,nconformations,param,metaheuristic);

        thrust::sequence(vectors_mejora.energy.n_conformation,vectors_mejora.energy.n_conformation + vectors_mejora.nconformations);

        #pragma omp parallel for
        for (unsigned int j=0;j<devices->n_devices;j++)
        {
                unsigned int th = omp_get_thread_num();
                unsigned long int stride_d = devices->stride[j];
                //int stride_d = th * stride;
                //printf("Th %d stride %d\n",th,stride_d);
                randSize = devices->conformaciones[j] * sizeof(curandState_t);
                moveSize_tam = devices->conformaciones[j] * sizeof(type_data);
                energyConf_tam = devices->conformaciones[j] * sizeof(unsigned int);
                confSize_tam = devices->conformaciones[j] * nlig * sizeof(type_data);
                confSize_tam_char = devices->conformaciones[j] * nlig * sizeof(char);
                confSize_bonds = devices->conformaciones[j] * nlig * MAXBOND *  sizeof(unsigned int);
		randSize_environment = devices->conformaciones[j] * metaheuristic.NEEImp * sizeof(curandState_t);
                moveSize_tam_environment = devices->conformaciones[j] * metaheuristic.NEEImp *sizeof(type_data);
                energyConf_tam_environment = devices->conformaciones[j] * metaheuristic.NEEImp *sizeof(unsigned int);
                confSize_tam_environment = devices->conformaciones[j] * nlig * metaheuristic.NEEImp * sizeof(type_data);
                confSize_tam_char_environment = devices->conformaciones[j] * nlig * metaheuristic.NEEImp * sizeof(char);
                confSize_bonds_environment = devices->conformaciones[j] * nlig * MAXBOND *  metaheuristic.NEEImp * sizeof(unsigned int);
		if (param.montecarlo)
                {
                        max_positions = (moveSize_tam * 8) * 2 + energyConf_tam + (moveSize_tam_environment * 8) * 2 + energyConf_tam_environment;
                        max_conf = (confSize_tam * 3) + randSize + confSize_tam + confSize_tam_char + confSize_bonds + (confSize_tam_environment * 3) + randSize_environment + confSize_tam_environment + confSize_tam_char_environment + confSize_bonds_environment;
                        max_common = (recSize * 4) + typeSize + bondSize;
                        max_memory = max_positions + max_conf + max_common;
                        tam_conformation =  (sizeof(type_data) * 8 * 2) + sizeof(unsigned int) + (sizeof(type_data) * nlig * 3) +  sizeof(curandState_t) + nlig * sizeof(type_data) +  nlig * sizeof(char) +  nlig * MAXBOND *  sizeof(unsigned int) + (3 * ((sizeof(type_data) * 8 * 2) + sizeof(unsigned int) + (sizeof(type_data) * nlig * 3) +  sizeof(curandState_t) + nlig * sizeof(type_data) +  nlig * sizeof(char) +  nlig * MAXBOND *  sizeof(unsigned int)));


                        unsigned long long int max_conformaciones_1 = (devices->propiedades[j].maxGridSize[0] * (devices->hilos[j*metaheuristic.num_kernels] / WARP_SIZE)) * 8;

                        //printf("Th %d j %d stride %d conformaciones %d max_conformaciones %lu\n",th,j,stride_d,devices->conformaciones[j],max_conformaciones_1);
                        if (devices->conformaciones[j] < max_conformaciones_1)
                        {
                                //COMPROBAR SI CABE EN MEMORIA
                                cudaSetDevice(devices->id[j]);
                                //cudaDeviceReset();
                                cudaMemGetInfo(&tam_max, &total_m);
                                cudaDeviceSynchronize();
                                cudaStatus = cudaGetLastError();
                                if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
                                std::cout << tam_max << " " << total_m << "\n";
                                tam_tmp = tam_max - (tam_max * 0.2);
                                if (tam_tmp < max_memory)
                                {
                                        //printf("Trocear en 1 GPU\n");
                                        max_memory = max_common;
                                        nconformations_count_total = 0;
                                        nconformations_count_partial = 0;
                                        stride_d_memory = 0;
                                        while (nconformations_count_total < devices->conformaciones[j])
                                        {
                                                max_memory += tam_conformation;
                                                if (max_memory > tam_tmp)
                                                {
							multigpu_mejorar_montecarlo_vs_flex_environment (backup_conformations,files,nlig,ligtype + ((stride_d + stride_d_memory)* nlig),bonds + ((stride_d + stride_d_memory) * nlig),ql + ((stride_d + stride_d_memory) * nlig),vectors_mejora.ligtype + ((stride_d + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.bonds + ((stride_d + stride_d_memory) * metaheuristic.NEEImp * MAXBOND * nlig),vectors_mejora.ql + ((stride_d + stride_d_memory) * metaheuristic.NEEImp * nlig),ligand_params,flexibility_params,proteina,  param, weights, f_params, devices, conformations_x + ((stride_d + stride_d_memory) * nlig), conformations_y + ((stride_d + stride_d_memory) * nlig), conformations_z + ((stride_d + stride_d_memory) * nlig), move_x + (stride_d + stride_d_memory), move_y + (stride_d + stride_d_memory), move_z + (stride_d + stride_d_memory), quat_x + (stride_d + stride_d_memory), quat_y + (stride_d + stride_d_memory), quat_z + (stride_d + stride_d_memory), quat_w + (stride_d + stride_d_memory), energy + (stride_d + stride_d_memory), nconfs + (stride_d + stride_d_memory), vectors_mejora.conformations_x + ((stride_d + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.conformations_y + ((stride_d + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.conformations_z + ((stride_d + stride_d_memory) * metaheuristic.NEEImp * nlig),  vectors_mejora.move_x + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.move_y + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.move_z + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_x + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_y + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_z + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_w + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.energy.energy + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.energy.n_conformation + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), metaheuristic, nconformations_count_partial,nconformations_count_partial * metaheuristic.NEEImp, j, stride_d + stride_d_memory, fase);
							 stride_d_memory += nconformations_count_partial;
                                                        max_memory = max_common;
                                                        nconformations_count_partial = 0;
                                                }
                                                else
                                                {
                                                        nconformations_count_total++;
                                                        nconformations_count_partial++;
                                                }
                                        }
                                        //printf("Trozo final %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
                                        nconformations_count_partial--;
					multigpu_mejorar_montecarlo_vs_flex_environment (backup_conformations,files,nlig,ligtype + ((stride_d + stride_d_memory)* nlig),bonds + ((stride_d + stride_d_memory) * nlig),ql + ((stride_d + stride_d_memory) * nlig),vectors_mejora.ligtype + ((stride_d + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.bonds + ((stride_d + stride_d_memory) * metaheuristic.NEEImp * MAXBOND * nlig),vectors_mejora.ql + ((stride_d + stride_d_memory) * metaheuristic.NEEImp * nlig),ligand_params,flexibility_params,proteina,  param, weights, f_params, devices, conformations_x + ((stride_d + stride_d_memory) * nlig), conformations_y + ((stride_d + stride_d_memory) * nlig), conformations_z + ((stride_d + stride_d_memory) * nlig), move_x + (stride_d + stride_d_memory), move_y + (stride_d + stride_d_memory), move_z + (stride_d + stride_d_memory), quat_x + (stride_d + stride_d_memory), quat_y + (stride_d + stride_d_memory), quat_z + (stride_d + stride_d_memory), quat_w + (stride_d + stride_d_memory), energy + (stride_d + stride_d_memory), nconfs + (stride_d + stride_d_memory), vectors_mejora.conformations_x + ((stride_d + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.conformations_y + ((stride_d + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.conformations_z + ((stride_d + stride_d_memory) * metaheuristic.NEEImp * nlig),  vectors_mejora.move_x + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.move_y + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.move_z + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_x + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_y + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_z + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_w + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.energy.energy + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.energy.n_conformation + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), metaheuristic, nconformations_count_partial,nconformations_count_partial * metaheuristic.NEEImp, j, stride_d + stride_d_memory, fase);
				 }
                       		else
                                {
                                	//printf("No Trocea\n");
                                        //Lanzar montecarlo con todo
					multigpu_mejorar_montecarlo_vs_flex_environment (backup_conformations,files,nlig,ligtype + (stride_d* nlig),bonds + (stride_d * nlig),ql + (stride_d * nlig),vectors_mejora.ligtype + (stride_d * metaheuristic.NEEImp * nlig), vectors_mejora.bonds + (stride_d * metaheuristic.NEEImp * MAXBOND * nlig),vectors_mejora.ql + (stride_d * metaheuristic.NEEImp * nlig),ligand_params,flexibility_params,proteina, param, weights, f_params, devices, conformations_x + (stride_d * nlig), conformations_y + (stride_d * nlig), conformations_z + (stride_d * nlig), move_x + stride_d, move_y + stride_d, move_z + stride_d, quat_x + stride_d, quat_y + stride_d, quat_z + stride_d, quat_w + stride_d, energy + stride_d, nconfs + stride_d, vectors_mejora.conformations_x + (stride_d * metaheuristic.NEEImp * nlig), vectors_mejora.conformations_y + (stride_d * metaheuristic.NEEImp * nlig), vectors_mejora.conformations_z + (stride_d * metaheuristic.NEEImp * nlig),  vectors_mejora.move_x + (stride_d * metaheuristic.NEEImp), vectors_mejora.move_y + (stride_d * metaheuristic.NEEImp), vectors_mejora.move_z + (stride_d * metaheuristic.NEEImp), vectors_mejora.quat_x + (stride_d * metaheuristic.NEEImp), vectors_mejora.quat_y + (stride_d * metaheuristic.NEEImp), vectors_mejora.quat_z + (stride_d * metaheuristic.NEEImp), vectors_mejora.quat_w + (stride_d * metaheuristic.NEEImp), vectors_mejora.energy.energy + (stride_d * metaheuristic.NEEImp), vectors_mejora.energy.n_conformation + (stride_d * metaheuristic.NEEImp), metaheuristic, devices->conformaciones[j], devices->conformaciones[j]*metaheuristic.NEEImp, j, stride_d, fase);
				}
			}
			else
			{
			//DIVIDIR EN TROZOS
                                while(devices->conformaciones[j] % max_conformaciones_1 != 0) max_conformaciones_1 --;
                                //printf("va a trocear\n");
                                for (unsigned int desplazamiento=0;desplazamiento<devices->conformaciones[j];desplazamiento+=max_conformaciones_1)
                                {
                                        //COMPROBAR SI CABE EN MEMORIA
                                        cudaSetDevice(devices->id[j]);
                                        //cudaDeviceReset();
                                        cudaMemGetInfo(&tam_max, &total_m);
                                        cudaDeviceSynchronize();
                                        cudaStatus = cudaGetLastError();
                                        if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
                                        std::cout << tam_max << " " << total_m << "\n";
                                        tam_tmp = tam_max - (tam_max * 0.2);
                                        if (tam_tmp < max_memory)
                                        {
                                                //printf("Trocear en 1 GPU\n");
                                                max_memory = max_common;
                                                nconformations_count_total = 0;
                                                nconformations_count_partial = 0;
                                                stride_d_memory = 0;
                                                while (nconformations_count_total < max_conformaciones_1)
                                                {
                                                        max_memory += tam_conformation;
                                                        if (max_memory > tam_tmp)
                                                        {
                                                                //printf("Trozo %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d_memory, vectors_e_s->nconformations);
                                                                //nconformations_count_total--;
                                                                //nconformations_count_partial--;
                                                                //Lanzar montencarlo con nconformations_count_partial;
								 multigpu_mejorar_montecarlo_vs_flex_environment (backup_conformations,files,nlig,ligtype + ((stride_d + desplazamiento + stride_d_memory)* nlig),bonds + ((stride_d +  desplazamiento + stride_d_memory) * nlig),ql + ((stride_d + desplazamiento + stride_d_memory) * nlig),vectors_mejora.ligtype + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.bonds + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * MAXBOND * nlig),vectors_mejora.ql + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * nlig),ligand_params,flexibility_params,proteina,  param, weights, f_params, devices, conformations_x + ((stride_d + desplazamiento + stride_d_memory) * nlig), conformations_y + ((stride_d + desplazamiento + stride_d_memory) * nlig), conformations_z + ((stride_d + desplazamiento + stride_d_memory) * nlig), move_x + (stride_d + desplazamiento + stride_d_memory), move_y + (stride_d + desplazamiento + stride_d_memory), move_z + (stride_d + desplazamiento + stride_d_memory), quat_x + (stride_d + desplazamiento + stride_d_memory), quat_y + (stride_d + desplazamiento + stride_d_memory), quat_z + (stride_d + desplazamiento + stride_d_memory), quat_w + (stride_d + desplazamiento + stride_d_memory), energy + (stride_d + desplazamiento + stride_d_memory), nconfs + (stride_d + desplazamiento + stride_d_memory), vectors_mejora.conformations_x + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.conformations_y + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.conformations_z + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * nlig),  vectors_mejora.move_x + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.move_y + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.move_z + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_x + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_y + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_z + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_w + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.energy.energy + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.energy.n_conformation + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), metaheuristic, nconformations_count_partial,nconformations_count_partial * metaheuristic.NEEImp, j, stride_d + desplazamiento + stride_d_memory, fase);
                                                         	stride_d_memory += nconformations_count_partial;
                                                         	max_memory = max_common;
                                                         	nconformations_count_partial = 0;
							 }
                                                else
                                                {
                                                        nconformations_count_total++;
                                                        nconformations_count_partial++;
                                                }
                                        }
                                        //printf("Trozo final %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
                                        nconformations_count_partial--;
					 multigpu_mejorar_montecarlo_vs_flex_environment (backup_conformations,files,nlig,ligtype + ((stride_d + desplazamiento + stride_d_memory)* nlig),bonds + ((stride_d +  desplazamiento + stride_d_memory) * nlig),ql + ((stride_d + desplazamiento + stride_d_memory) * nlig),vectors_mejora.ligtype + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.bonds + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * MAXBOND * nlig),vectors_mejora.ql + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * nlig),ligand_params,flexibility_params,proteina, param, weights, f_params, devices,conformations_x + ((stride_d + desplazamiento + stride_d_memory) * nlig), conformations_y + ((stride_d + desplazamiento + stride_d_memory) * nlig), conformations_z + ((stride_d + desplazamiento + stride_d_memory) * nlig), move_x + (stride_d + desplazamiento + stride_d_memory), move_y + (stride_d + desplazamiento + stride_d_memory), move_z + (stride_d + desplazamiento + stride_d_memory), quat_x + (stride_d + desplazamiento + stride_d_memory), quat_y + (stride_d + desplazamiento + stride_d_memory), quat_z + (stride_d + desplazamiento + stride_d_memory), quat_w + (stride_d + desplazamiento + stride_d_memory), energy + (stride_d + desplazamiento + stride_d_memory), nconfs + (stride_d + desplazamiento + stride_d_memory), vectors_mejora.conformations_x + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.conformations_y + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.conformations_z + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * nlig),  vectors_mejora.move_x + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.move_y + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.move_z + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_x + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_y + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_z + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_w + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.energy.energy + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.energy.n_conformation + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), metaheuristic, nconformations_count_partial,nconformations_count_partial * metaheuristic.NEEImp,j, stride_d + desplazamiento + stride_d_memory, fase);
				}
				 else
                                        {
                                                //printf("No Trocea\n");
                                                //Lanzar montecarlo con todo
						multigpu_mejorar_montecarlo_vs_flex_environment (backup_conformations,files,nlig,ligtype + ((stride_d + desplazamiento + stride_d_memory)* nlig),bonds + ((stride_d +  desplazamiento + stride_d_memory) * nlig),ql + ((stride_d + desplazamiento + stride_d_memory) * nlig),vectors_mejora.ligtype + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.bonds + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * MAXBOND * nlig),vectors_mejora.ql + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * nlig),ligand_params,flexibility_params,proteina,  param, weights, f_params, devices, conformations_x + ((stride_d + desplazamiento + stride_d_memory) * nlig), conformations_y + ((stride_d + desplazamiento + stride_d_memory) * nlig), conformations_z + ((stride_d + desplazamiento + stride_d_memory) * nlig), move_x + (stride_d + desplazamiento + stride_d_memory), move_y + (stride_d + desplazamiento + stride_d_memory), move_z + (stride_d + desplazamiento + stride_d_memory), quat_x + (stride_d + desplazamiento + stride_d_memory), quat_y + (stride_d + desplazamiento + stride_d_memory), quat_z + (stride_d + desplazamiento + stride_d_memory), quat_w + (stride_d + desplazamiento + stride_d_memory), energy + (stride_d + desplazamiento + stride_d_memory), nconfs + (stride_d + desplazamiento + stride_d_memory), vectors_mejora.conformations_x + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.conformations_y + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.conformations_z + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * nlig),  vectors_mejora.move_x + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.move_y + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.move_z + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_x + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_y + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_z + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_w + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.energy.energy + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.energy.n_conformation + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), metaheuristic, nconformations_count_partial,nconformations_count_partial * metaheuristic.NEEImp, j, stride_d + desplazamiento + stride_d_memory, fase);
					}
                                        //printf("Device %d con trozo %d\n",devices->id[th],i);
                                }
                        }
                }
                else
                {
                        max_positions = (moveSize_tam * 8) * 2 + energyConf_tam * 2 + (moveSize_tam_environment * 8) * 2 + energyConf_tam_environment * 2;
                        max_conf = (confSize_tam * 3) + randSize + confSize_tam + confSize_tam_char + confSize_bonds +  (confSize_tam_environment * 3) + randSize_environment + confSize_tam_environment + confSize_tam_char_environment + confSize_bonds_environment;
                        max_common = (recSize * 4) + typeSize + bondSize;
                        max_memory = max_positions + max_conf + max_common;
                        tam_conformation =  (sizeof(type_data) * 8 * 2) + sizeof(unsigned int) * 2 + (sizeof(type_data) * nlig * 3) +  sizeof(curandState_t) + nlig * sizeof(type_data) +  nlig * sizeof(char) +  nlig * MAXBOND *  sizeof(unsigned int) + (3 * ((sizeof(type_data) * 8 * 2) + sizeof(unsigned int) * 2 + (sizeof(type_data) * nlig * 3) +  sizeof(curandState_t) + nlig * sizeof(type_data) +  nlig * sizeof(char) +  nlig * MAXBOND *  sizeof(unsigned int)));

                        unsigned long long int max_conformaciones_2 = (devices->propiedades[j].maxGridSize[0] * (devices->hilos[j*metaheuristic.num_kernels] / WARP_SIZE)) * 8;

                        //printf("Th %d j %d stride %d conformaciones %d max_conformaciones %lu\n",th,j,stride_d,devices->conformaciones[j],max_conformaciones_2);
                        if (devices->conformaciones[th] < max_conformaciones_2)
                        {
						
				//COMPROBAR SI CABE EN MEMORIA
                                cudaSetDevice(devices->id[j]);
                                //cudaDeviceReset();
                                cudaMemGetInfo(&tam_max, &total_m);
                                cudaDeviceSynchronize();
                                cudaStatus = cudaGetLastError();
                                if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
                                std::cout << tam_max << " " << total_m << "\n";
                                tam_tmp = tam_max - (tam_max * 0.2);
                                if (tam_tmp < max_memory)
                                {
                                        //printf("Trocear en 1 GPU\n");
                                        max_memory = max_common;
                                        nconformations_count_total = 0;
                                        nconformations_count_partial = 0;
                                        stride_d_memory = 0;
                                        while (nconformations_count_total < devices->conformaciones[j])
                                        {
                                                max_memory += tam_conformation;
                                                if (max_memory > tam_tmp)
                                                {
                                                        multigpu_mejorar_gpusolver_vs_flex_environment (backup_conformations,files,nlig,ligtype + ((stride_d + stride_d_memory)* nlig),bonds + ((stride_d + stride_d_memory) * nlig),ql + ((stride_d + stride_d_memory) * nlig),vectors_mejora.ligtype + ((stride_d + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.bonds + ((stride_d + stride_d_memory) * metaheuristic.NEEImp * MAXBOND * nlig),vectors_mejora.ql + ((stride_d + stride_d_memory) * metaheuristic.NEEImp * nlig),ligand_params,flexibility_params,proteina, param, weights, f_params, devices, conformations_x + ((stride_d + stride_d_memory) * nlig), conformations_y + ((stride_d + stride_d_memory) * nlig), conformations_z + ((stride_d + stride_d_memory) * nlig), move_x + (stride_d + stride_d_memory), move_y + (stride_d + stride_d_memory), move_z + (stride_d + stride_d_memory), quat_x + (stride_d + stride_d_memory), quat_y + (stride_d + stride_d_memory), quat_z + (stride_d + stride_d_memory), quat_w + (stride_d + stride_d_memory), energy + (stride_d + stride_d_memory), nconfs + (stride_d + stride_d_memory), vectors_mejora.conformations_x + ((stride_d + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.conformations_y + ((stride_d + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.conformations_z + ((stride_d + stride_d_memory) * metaheuristic.NEEImp * nlig),  vectors_mejora.move_x + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.move_y + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.move_z + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_x + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_y + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_z + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_w + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.energy.energy + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.energy.n_conformation + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), metaheuristic, nconformations_count_partial,nconformations_count_partial * metaheuristic.NEEImp, j, stride_d + stride_d_memory, fase);
                                                         stride_d_memory += nconformations_count_partial;
                                                        max_memory = max_common;
                                                        nconformations_count_partial = 0;
                                                }
                                                else
                                                {
                                                        nconformations_count_total++;
                                                        nconformations_count_partial++;
                                                }
                                        }
					 //printf("Trozo final %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
                                        nconformations_count_partial--;
                                        multigpu_mejorar_gpusolver_vs_flex_environment (backup_conformations,files,nlig,ligtype + ((stride_d + stride_d_memory)* nlig),bonds + ((stride_d + stride_d_memory) * nlig),ql + ((stride_d + stride_d_memory) * nlig),vectors_mejora.ligtype + ((stride_d + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.bonds + ((stride_d + stride_d_memory) * metaheuristic.NEEImp * MAXBOND * nlig),vectors_mejora.ql + ((stride_d + stride_d_memory) * metaheuristic.NEEImp * nlig),ligand_params,flexibility_params,proteina, param, weights, f_params, devices, conformations_x + ((stride_d + stride_d_memory) * nlig), conformations_y + ((stride_d + stride_d_memory) * nlig), conformations_z + ((stride_d + stride_d_memory) * nlig), move_x + (stride_d + stride_d_memory), move_y + (stride_d + stride_d_memory), move_z + (stride_d + stride_d_memory), quat_x + (stride_d + stride_d_memory), quat_y + (stride_d + stride_d_memory), quat_z + (stride_d + stride_d_memory), quat_w + (stride_d + stride_d_memory), energy + (stride_d + stride_d_memory), nconfs + (stride_d + stride_d_memory), vectors_mejora.conformations_x + ((stride_d + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.conformations_y + ((stride_d + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.conformations_z + ((stride_d + stride_d_memory) * metaheuristic.NEEImp * nlig),  vectors_mejora.move_x + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.move_y + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.move_z + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_x + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_y + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_z + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_w + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.energy.energy + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.energy.n_conformation + ((stride_d + stride_d_memory) * metaheuristic.NEEImp), metaheuristic, nconformations_count_partial,nconformations_count_partial * metaheuristic.NEEImp, j, stride_d + stride_d_memory, fase);
                                 }
                                else
                                {
                                        //printf("No Trocea\n");
                                        //Lanzar montecarlo con todo
                                        multigpu_mejorar_gpusolver_vs_flex_environment (backup_conformations,files,nlig,ligtype + (stride_d* nlig),bonds + (stride_d * nlig),ql + (stride_d * nlig),vectors_mejora.ligtype + (stride_d * metaheuristic.NEEImp * nlig), vectors_mejora.bonds + (stride_d * metaheuristic.NEEImp * MAXBOND * nlig),vectors_mejora.ql + (stride_d * metaheuristic.NEEImp * nlig),ligand_params,flexibility_params,proteina, param, weights, f_params, devices, conformations_x + (stride_d * nlig), conformations_y + (stride_d * nlig), conformations_z + (stride_d * nlig), move_x + stride_d, move_y + stride_d, move_z + stride_d, quat_x + stride_d, quat_y + stride_d, quat_z + stride_d, quat_w + stride_d, energy + stride_d, nconfs + stride_d, vectors_mejora.conformations_x + (stride_d * metaheuristic.NEEImp * nlig), vectors_mejora.conformations_y + (stride_d * metaheuristic.NEEImp * nlig), vectors_mejora.conformations_z + (stride_d * metaheuristic.NEEImp * nlig),  vectors_mejora.move_x + (stride_d * metaheuristic.NEEImp), vectors_mejora.move_y + (stride_d * metaheuristic.NEEImp), vectors_mejora.move_z + (stride_d * metaheuristic.NEEImp), vectors_mejora.quat_x + (stride_d * metaheuristic.NEEImp), vectors_mejora.quat_y + (stride_d * metaheuristic.NEEImp), vectors_mejora.quat_z + (stride_d * metaheuristic.NEEImp), vectors_mejora.quat_w + (stride_d * metaheuristic.NEEImp), vectors_mejora.energy.energy + (stride_d * metaheuristic.NEEImp), vectors_mejora.energy.n_conformation + (stride_d * metaheuristic.NEEImp), metaheuristic, devices->conformaciones[j], devices->conformaciones[j]*metaheuristic.NEEImp, j, stride_d, fase);
                                }
                        }
                        else
                        {
                        //DIVIDIR EN TROZOS
			 while(devices->conformaciones[j] % max_conformaciones_2 != 0) max_conformaciones_2 --;
                                //printf("va a trocear\n");
                                for (unsigned int desplazamiento=0;desplazamiento<devices->conformaciones[j];desplazamiento+=max_conformaciones_2)
                                {
                                        //COMPROBAR SI CABE EN MEMORIA
					 cudaSetDevice(devices->id[j]);
                                        //cudaDeviceReset();
                                        cudaMemGetInfo(&tam_max, &total_m);
                                        cudaDeviceSynchronize();
                                        cudaStatus = cudaGetLastError();
                                        if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
                                        std::cout << tam_max << " " << total_m << "\n";
                                        tam_tmp = tam_max - (tam_max * 0.2);
                                        if (tam_tmp < max_memory)
                                        {
                                                //printf("Trocear en 1 GPU\n");
                                                max_memory = max_common;
                                                nconformations_count_total = 0;
                                                nconformations_count_partial = 0;
                                                stride_d_memory = 0;
                                                while (nconformations_count_total < max_conformaciones_2)
                                                {
                                                        max_memory += tam_conformation;
                                                        if (max_memory > tam_tmp)
                                                        {
                                                                //printf("Trozo %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d_memory, vectors_e_s->nconformations);
                                                                //nconformations_count_total--;
                                                                //nconformations_count_partial--;
                                                                //Lanzar montencarlo con nconformations_count_partial;
                                                                 multigpu_mejorar_gpusolver_vs_flex_environment (backup_conformations,files,nlig,ligtype + ((stride_d + desplazamiento + stride_d_memory)* nlig),bonds + ((stride_d +  desplazamiento + stride_d_memory) * nlig),ql + ((stride_d + desplazamiento + stride_d_memory) * nlig),vectors_mejora.ligtype + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.bonds + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * MAXBOND * nlig),vectors_mejora.ql + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * nlig),ligand_params,flexibility_params,proteina, param, weights, f_params, devices, conformations_x + ((stride_d + desplazamiento + stride_d_memory) * nlig), conformations_y + ((stride_d + desplazamiento + stride_d_memory) * nlig), conformations_z + ((stride_d + desplazamiento + stride_d_memory) * nlig), move_x + (stride_d + desplazamiento + stride_d_memory), move_y + (stride_d + desplazamiento + stride_d_memory), move_z + (stride_d + desplazamiento + stride_d_memory), quat_x + (stride_d + desplazamiento + stride_d_memory), quat_y + (stride_d + desplazamiento + stride_d_memory), quat_z + (stride_d + desplazamiento + stride_d_memory), quat_w + (stride_d + desplazamiento + stride_d_memory), energy + (stride_d + desplazamiento + stride_d_memory), nconfs + (stride_d + desplazamiento + stride_d_memory), vectors_mejora.conformations_x + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.conformations_y + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.conformations_z + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * nlig),  vectors_mejora.move_x + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.move_y + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.move_z + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_x + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_y + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_z + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_w + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.energy.energy + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.energy.n_conformation + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), metaheuristic, nconformations_count_partial,nconformations_count_partial * metaheuristic.NEEImp, j, stride_d + desplazamiento + stride_d_memory, fase);
                                                                stride_d_memory += nconformations_count_partial;
                                                                max_memory = max_common;
                                                                nconformations_count_partial = 0;
							}
                                                else
                                                {
                                                        nconformations_count_total++;
                                                        nconformations_count_partial++;
                                                }
                                        }
                                        //printf("Trozo final %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
                                        nconformations_count_partial--;
                                         multigpu_mejorar_gpusolver_vs_flex_environment (backup_conformations,files,nlig,ligtype + ((stride_d + desplazamiento + stride_d_memory)* nlig),bonds + ((stride_d +  desplazamiento + stride_d_memory) * nlig),ql + ((stride_d + desplazamiento + stride_d_memory) * nlig),vectors_mejora.ligtype + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.bonds + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * MAXBOND * nlig),vectors_mejora.ql + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * nlig),ligand_params,flexibility_params,proteina, param, weights, f_params, devices, conformations_x + ((stride_d + desplazamiento + stride_d_memory) * nlig), conformations_y + ((stride_d + desplazamiento + stride_d_memory) * nlig), conformations_z + ((stride_d + desplazamiento + stride_d_memory) * nlig), move_x + (stride_d + desplazamiento + stride_d_memory), move_y + (stride_d + desplazamiento + stride_d_memory), move_z + (stride_d + desplazamiento + stride_d_memory), quat_x + (stride_d + desplazamiento + stride_d_memory), quat_y + (stride_d + desplazamiento + stride_d_memory), quat_z + (stride_d + desplazamiento + stride_d_memory), quat_w + (stride_d + desplazamiento + stride_d_memory), energy + (stride_d + desplazamiento + stride_d_memory), nconfs + (stride_d + desplazamiento + stride_d_memory), vectors_mejora.conformations_x + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.conformations_y + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * nlig), vectors_mejora.conformations_z + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp * nlig),  vectors_mejora.move_x + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.move_y + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.move_z + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_x + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_y + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_z + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.quat_w + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.energy.energy + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), vectors_mejora.energy.n_conformation + ((stride_d + desplazamiento + stride_d_memory) * metaheuristic.NEEImp), metaheuristic, nconformations_count_partial,nconformations_count_partial * metaheuristic.NEEImp,j, stride_d + desplazamiento + stride_d_memory, fase);
                                }
                                 else
                                        {
                                                //printf("No Trocea\n");
                                                //Lanzar montecarlo con todo
						 multigpu_mejorar_gpusolver_vs_flex_environment (backup_conformations,files,nlig,ligtype + ((stride_d + desplazamiento)* nlig),bonds + ((stride_d +  desplazamiento) * nlig),ql + ((stride_d + desplazamiento) * nlig),vectors_mejora.ligtype + ((stride_d + desplazamiento) * metaheuristic.NEEImp * nlig), vectors_mejora.bonds + ((stride_d + desplazamiento) * metaheuristic.NEEImp * MAXBOND * nlig),vectors_mejora.ql + ((stride_d + desplazamiento) * metaheuristic.NEEImp * nlig),ligand_params,flexibility_params,proteina, param, weights, f_params, devices, conformations_x + ((stride_d + desplazamiento) * nlig), conformations_y + ((stride_d + desplazamiento) * nlig), conformations_z + ((stride_d + desplazamiento) * nlig), move_x + (stride_d + desplazamiento), move_y + (stride_d + desplazamiento), move_z + (stride_d + desplazamiento), quat_x + (stride_d + desplazamiento), quat_y + (stride_d + desplazamiento), quat_z + (stride_d + desplazamiento), quat_w + (stride_d + desplazamiento), energy + (stride_d + desplazamiento), nconfs + (stride_d + desplazamiento), vectors_mejora.conformations_x + ((stride_d + desplazamiento) * metaheuristic.NEEImp * nlig), vectors_mejora.conformations_y + ((stride_d + desplazamiento) * metaheuristic.NEEImp * nlig), vectors_mejora.conformations_z + ((stride_d + desplazamiento) * metaheuristic.NEEImp * nlig),  vectors_mejora.move_x + ((stride_d + desplazamiento) * metaheuristic.NEEImp), vectors_mejora.move_y + ((stride_d + desplazamiento) * metaheuristic.NEEImp), vectors_mejora.move_z + ((stride_d + desplazamiento) * metaheuristic.NEEImp), vectors_mejora.quat_x + ((stride_d + desplazamiento) * metaheuristic.NEEImp), vectors_mejora.quat_y + ((stride_d + desplazamiento) * metaheuristic.NEEImp), vectors_mejora.quat_z + ((stride_d + desplazamiento) * metaheuristic.NEEImp), vectors_mejora.quat_w + ((stride_d + desplazamiento) * metaheuristic.NEEImp), vectors_mejora.energy.energy + ((stride_d + desplazamiento) * metaheuristic.NEEImp), vectors_mejora.energy.n_conformation + ((stride_d + desplazamiento) * metaheuristic.NEEImp), metaheuristic, max_conformaciones_2,max_conformaciones_2 * metaheuristic.NEEImp, j, stride_d + desplazamiento, fase);
                                        }
                                        //printf("Device %d con trozo %d\n",devices->id[th],i);
                                }
                        }
                }

	}
}

extern void multigpuSolver_mejora_vs_flex (unsigned int files, unsigned int nlig, char* ligtype, unsigned int *bonds, type_data *ql, struct ligand_params_t *ligand_params, struct flexibility_params_t *flexibility_params, struct receptor_t proteina, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z , type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int fase)
{
	double tiempo_i, tiempo_f;
	unsigned int  pc_temp, dev, stride_temp,ttotal = 0;
	unsigned int backup_conformations = nconformations;	
	
	size_t available,total_m;
	cudaError_t cudaStatus;
	unsigned long int max_memory;
	unsigned long int max_positions;
	unsigned long int max_common;
	unsigned long int max_conf;
	unsigned long int max_flex_conf;
	unsigned long int typeSize = proteina.nrec * sizeof(char);
	unsigned long int recSize = proteina.nrec * sizeof(type_data);
	unsigned long int bondSize = proteina.nrec * sizeof(unsigned int) * MAXBOND;
	unsigned long int randSize;
	unsigned long int moveSize_tam;
	unsigned long int energyConf_tam;
    unsigned long int confSize_tam;
	unsigned long int confSize_tam_char;
	unsigned long int confSize_bonds;
	unsigned long int flexConfSize;
	unsigned long int flexSize = files *  sizeof(struct flexibility_params_t) + files * sizeof(type_data);
	unsigned long int linksSize = 0, linksfragmentsSize = 0, fragmentstamSize = 0, fragmentsSize = 0, boolSize = 0;
	unsigned long int tam_max, tam_tmp, nconformations_count_total, nconformations_count_partial, tam_conformation, stride_d_memory;

	for (int i = 0; i < files; i++)
	{
		linksSize += flexibility_params[i].n_links * 2 * sizeof(unsigned int);
		linksfragmentsSize += flexibility_params[i].n_links * 2 * sizeof(unsigned int);
		fragmentstamSize += flexibility_params[i].n_fragments * sizeof(unsigned int);
		fragmentsSize += flexibility_params[i].n_links * (ligand_params[i].atoms - 2) * sizeof(unsigned int);
		boolSize += ligand_params[i].atoms * ligand_params[i].atoms * sizeof(bool);
	}
	
	max_common = (recSize * 4) + typeSize + bondSize;
	max_flex_conf =  flexSize + linksSize + linksfragmentsSize + fragmentstamSize + fragmentsSize + boolSize;
	
	pc_temp = nconformations;// / files;
	devices->stride[0] = 0;	
	/*stride_temp = floor((devices->trabajo[0] * 0.01));//files);	
	devices->conformaciones[0] = stride_temp * pc_temp;
	ttotal = stride_temp;
	printf("trabajo 0 %d trabajo 1 %d\n",devices->trabajo[0],devices->trabajo[1]);
	//exit(0);
	printf("nconformations %d, files %d pc_temp %d\n",nconformations,files,pc_temp);	
	
	for (unsigned int i=(devices->n_devices - 1);i > 0;i--)
	{
		stride_temp = floor((devices->trabajo[i] * 0.01));//files);
		devices->conformaciones[i] = stride_temp * pc_temp;
		ttotal += stride_temp;
	}
	unsigned int diferencia = pc_temp-ttotal;//files - ttotal;
	for (unsigned int i=0;i<diferencia;i++)
	{
		dev = i % devices->n_devices;
		devices->conformaciones[dev] ++;//= pc_temp;
	}
	for (unsigned int i=(devices->n_devices-1);i > 0;i--)
	{
		devices->stride[i] = nconformations - devices->conformaciones[i];
		nconformations = nconformations - devices->conformaciones[i];		
	}*/
	for (int i=0;i < devices->n_devices;i++)
        {
                stride_temp = floor((devices->trabajo[i] * 0.01)*nconformations);
                devices->conformaciones[i] = stride_temp;
                ttotal += stride_temp;
        }
        int diferencia = nconformations - ttotal;
        for (int i=0;i<diferencia;i++)
        {
                dev = i % devices->n_devices;
                devices->conformaciones[dev] ++;
        }
        for (int i=(devices->n_devices-1);i > 0;i--)
        {
                devices->stride[i] = nconformations - devices->conformaciones[i];
                nconformations = nconformations - devices->conformaciones[i];
        }	
	omp_set_num_threads(devices->n_devices);
	#pragma omp parallel for 	
	for (unsigned int j=0;j<devices->n_devices;j++)
	{
		unsigned int th = omp_get_thread_num();
		unsigned int stride_d = devices->stride[j];
		//int stride_d = th * stride;		
		printf("Th %d stride %d conformaciones %d\n",th,stride_d,devices->conformaciones[j]);	
		randSize = devices->conformaciones[j] * sizeof(curandState_t);
		moveSize_tam = devices->conformaciones[j] * sizeof(type_data);
		energyConf_tam = devices->conformaciones[j] * sizeof(unsigned int);
		confSize_tam = devices->conformaciones[j] * nlig * sizeof(type_data);
		confSize_tam_char = devices->conformaciones[j] * nlig * sizeof(char);
		confSize_bonds = devices->conformaciones[j] * nlig * MAXBOND *  sizeof(unsigned int);
		flexConfSize =  devices->conformaciones[j] * sizeof(struct flexibility_data_t);

		if (param.montecarlo)
		{
			max_positions = (moveSize_tam * 8) * 2 + moveSize_tam + energyConf_tam;
			max_conf = (confSize_tam * 3) + randSize + confSize_tam + confSize_tam_char + confSize_bonds + flexConfSize;
			max_memory = max_positions + max_conf + max_common + max_flex_conf;
			tam_conformation =  (sizeof(type_data) * 8 * 2) + sizeof(type_data) + sizeof(unsigned int) + (sizeof(type_data) * nlig * 3) +  sizeof(curandState_t) + nlig * sizeof(type_data) +  nlig * sizeof(char) +  nlig * MAXBOND *  sizeof(unsigned int);
	
			unsigned long long int max_conformaciones_1 = (devices->propiedades[j].maxGridSize[0] * (devices->hilos[j*metaheuristic.num_kernels] / WARP_SIZE)) * 8;
		
			//printf("Th %d j %d stride %d conformaciones %d max_conformaciones %lu\n",th,j,stride_d,devices->conformaciones[j],max_conformaciones_1);
			if (devices->conformaciones[j] < max_conformaciones_1)
			{
				//COMPROBAR QUE CABE EN MEMORIA
				cudaSetDevice(devices->id[j]);
				//cudaDeviceReset();
				cudaMemGetInfo(&tam_max, &total_m);
				cudaDeviceSynchronize();
				cudaStatus = cudaGetLastError();
				if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
				std::cout << tam_max << " " << total_m << "\n";
				tam_tmp = tam_max - (tam_max * 0.2);
				if (tam_tmp < max_memory)
				{
					//printf("Trocear en 1 GPU\n");
					max_memory = max_common + max_flex_conf;
					nconformations_count_total = 0;
					nconformations_count_partial = 0;
					stride_d_memory = 0;
					while (nconformations_count_total < devices->conformaciones[th])
					{
						max_memory += tam_conformation;
						if (max_memory > tam_tmp)
						{
							//printf("Trozo %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d_memory, vectors_e_s->nconformations);
							//nconformations_count_total--;
							//nconformations_count_partial--;
							//Lanzar montencarlo con nconformations_count_partial;							
							multigpu_mejorar_montecarlo_vs_flex (backup_conformations,files,nlig, ligtype+((stride_d+stride_d_memory)*nlig),bonds+((stride_d+stride_d_memory)*nlig*MAXBOND),ql+((stride_d+stride_d_memory)*nlig),ligand_params,proteina,param,weights,f_params,devices,conformations_x+((stride_d+stride_d_memory)*nlig),conformations_y+((stride_d+stride_d_memory)*nlig),conformations_z+((stride_d+stride_d_memory)*nlig), move_x+(stride_d+stride_d_memory),move_y+(stride_d+stride_d_memory),move_z+(stride_d+stride_d_memory),quat_x+(stride_d+stride_d_memory),quat_y+(stride_d+stride_d_memory),quat_z+(stride_d+stride_d_memory),quat_w+(stride_d+stride_d_memory),energy+(stride_d+stride_d_memory),nconfs+(stride_d+stride_d_memory),metaheuristic,flexibility_params,nconformations_count_partial,j,stride_d+stride_d_memory,fase);
							stride_d_memory += nconformations_count_partial;
							max_memory = max_common + max_flex_conf;
							nconformations_count_partial = 0;
						}
						else
						{
							nconformations_count_total++;
							nconformations_count_partial++;
						}
					}
					//printf("Trozo final %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
					nconformations_count_partial--;					
					multigpu_mejorar_montecarlo_vs_flex (backup_conformations,files,nlig, ligtype+((stride_d+stride_d_memory)*nlig),bonds+((stride_d+stride_d_memory)*nlig*MAXBOND),ql+((stride_d+stride_d_memory)*nlig),ligand_params,proteina,param,weights,f_params,devices,conformations_x+((stride_d+stride_d_memory)*nlig),conformations_y+((stride_d+stride_d_memory)*nlig),conformations_z+((stride_d+stride_d_memory)*nlig), move_x+(stride_d+stride_d_memory),move_y+(stride_d+stride_d_memory),move_z+(stride_d+stride_d_memory),quat_x+(stride_d+stride_d_memory),quat_y+(stride_d+stride_d_memory),quat_z+(stride_d+stride_d_memory),quat_w+(stride_d+stride_d_memory),energy+(stride_d+stride_d_memory),nconfs+(stride_d+stride_d_memory),metaheuristic,flexibility_params,nconformations_count_partial,j,stride_d+stride_d_memory,fase);
				}	
				else
				{
					//printf("No Trocea\n");
					//Lanzar montecarlo con todo
					multigpu_mejorar_montecarlo_vs_flex (backup_conformations,files,nlig, ligtype+(stride_d*nlig),bonds+(stride_d*nlig*MAXBOND),ql+(stride_d*nlig),ligand_params,proteina,param,weights,f_params,devices,conformations_x+(stride_d*nlig),conformations_y+(stride_d*nlig),conformations_z+(stride_d*nlig), move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,nconfs+stride_d,metaheuristic,flexibility_params,devices->conformaciones[j],j,stride_d,fase); 
				}
			}
			else
			{
				//DIVIDIR EN TROZOS
				while(devices->conformaciones[j] % max_conformaciones_1 != 0) max_conformaciones_1 --;
				//printf("va a trocear\n");			
				for (unsigned int desplazamiento=0;desplazamiento<devices->conformaciones[j];desplazamiento+=max_conformaciones_1)
				{
					//COMPROBAR QUE CABE EN MEMORIA
					cudaSetDevice(devices->id[j]);
					//cudaDeviceReset();
					cudaMemGetInfo(&tam_max, &total_m);
					cudaDeviceSynchronize();
					cudaStatus = cudaGetLastError();
					if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
					//std::cout << tam_max << " " << total_m << "\n";
					tam_tmp = tam_max - (tam_max * 0.2);
					if (tam_tmp < max_memory)
					{
						//printf("Trocear en 1 GPU\n");
						max_memory = max_common + max_flex_conf;
						nconformations_count_total = 0;
						nconformations_count_partial = 0;
						stride_d_memory = 0;
						while (nconformations_count_total < max_conformaciones_1)
						{
							max_memory += tam_conformation;
							if (max_memory > tam_tmp)
							{
								//printf("Trozo %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d_memory, vectors_e_s->nconformations);
								//nconformations_count_total--;
								//nconformations_count_partial--;
								//Lanzar montencarlo con nconformations_count_partial;							
								multigpu_mejorar_montecarlo_vs_flex (backup_conformations,files,nlig, ligtype+((stride_d+desplazamiento+stride_d_memory)*nlig),bonds+((stride_d+desplazamiento+stride_d_memory)*nlig*MAXBOND),ql+((stride_d+desplazamiento+stride_d_memory)*nlig),ligand_params,proteina,param,weights,f_params,devices,conformations_x+((stride_d+desplazamiento+stride_d_memory)*nlig),conformations_y+((stride_d+desplazamiento+stride_d_memory)*nlig),conformations_z+((stride_d+desplazamiento+stride_d_memory)*nlig), move_x+(stride_d+desplazamiento+stride_d_memory),move_y+(stride_d+desplazamiento+stride_d_memory),move_z+(stride_d+desplazamiento+stride_d_memory),quat_x+(stride_d+desplazamiento+stride_d_memory),quat_y+(stride_d+desplazamiento+stride_d_memory),quat_z+(stride_d+desplazamiento+stride_d_memory),quat_w+(stride_d+desplazamiento+stride_d_memory),energy+(stride_d+desplazamiento+stride_d_memory),nconfs+(stride_d+desplazamiento+stride_d_memory),metaheuristic,flexibility_params,nconformations_count_partial,j,stride_d+desplazamiento+stride_d_memory,fase);
								stride_d_memory += nconformations_count_partial;
								max_memory = max_common + max_flex_conf;
								nconformations_count_partial = 0;
							}
							else
							{
								nconformations_count_total++;
								nconformations_count_partial++;
							}
						}
						//printf("Trozo final %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
						nconformations_count_partial--;					
						multigpu_mejorar_montecarlo_vs_flex (backup_conformations,files,nlig, ligtype+((stride_d+desplazamiento+stride_d_memory)*nlig),bonds+((stride_d+desplazamiento+stride_d_memory)*nlig*MAXBOND),ql+((stride_d+desplazamiento+stride_d_memory)*nlig),ligand_params,proteina,param,weights,f_params,devices,conformations_x+((stride_d+desplazamiento+stride_d_memory)*nlig),conformations_y+((stride_d+desplazamiento+stride_d_memory)*nlig),conformations_z+((stride_d+desplazamiento+stride_d_memory)*nlig), move_x+(stride_d+desplazamiento+stride_d_memory),move_y+(stride_d+desplazamiento+stride_d_memory),move_z+(stride_d+desplazamiento+stride_d_memory),quat_x+(stride_d+desplazamiento+stride_d_memory),quat_y+(stride_d+desplazamiento+stride_d_memory),quat_z+(stride_d+desplazamiento+stride_d_memory),quat_w+(stride_d+desplazamiento+stride_d_memory),energy+(stride_d+desplazamiento+stride_d_memory),nconfs+(stride_d+desplazamiento+stride_d_memory),metaheuristic,flexibility_params,nconformations_count_partial,j,stride_d+desplazamiento+stride_d_memory,fase);
					}	
					else
					{
						//printf("No Trocea\n");
						//Lanzar montecarlo con todo
						multigpu_mejorar_montecarlo_vs_flex (backup_conformations,files,nlig, ligtype+((stride_d+desplazamiento)*nlig),bonds+((stride_d+desplazamiento)*nlig*MAXBOND),ql+((stride_d+desplazamiento)*nlig),ligand_params,proteina,param,weights,f_params,devices,conformations_x+((stride_d+desplazamiento)*nlig),conformations_y+((stride_d+desplazamiento)*nlig),conformations_z+((stride_d+desplazamiento)*nlig), move_x+(stride_d+desplazamiento),move_y+(stride_d+desplazamiento),move_z+(stride_d+desplazamiento),quat_x+(stride_d+desplazamiento),quat_y+(stride_d+desplazamiento),quat_z+(stride_d+desplazamiento),quat_w+(stride_d+desplazamiento),energy+(stride_d+desplazamiento),nconfs+(stride_d+desplazamiento),metaheuristic,flexibility_params,max_conformaciones_1,j,(stride_d+desplazamiento),fase);
					}				
					//printf("Device %d con trozo %d\n",devices->id[th],i);				
				}
			}
		}
		else
		{		
			max_positions = (moveSize_tam * 8) * 2 + moveSize_tam + energyConf_tam;
			max_conf = (confSize_tam * 3) + randSize + confSize_tam + confSize_tam_char + confSize_bonds + flexConfSize;
			max_memory = max_positions + max_conf + max_common + max_flex_conf;
			tam_conformation =  (sizeof(type_data) * 8 * 2) + sizeof(type_data) + sizeof(unsigned int) * 2 + (sizeof(type_data) * nlig * 3) +  sizeof(curandState_t) + nlig * sizeof(type_data) +  nlig * sizeof(char) +  nlig * MAXBOND *  sizeof(unsigned int);
	
			unsigned long long int max_conformaciones_2 = (devices->propiedades[j].maxGridSize[0] * (devices->hilos[j*metaheuristic.num_kernels] / WARP_SIZE)) * 8;
		
			//printf("Th %d j %d stride %d conformaciones %d max_conformaciones %lu\n",th,j,stride_d,devices->conformaciones[j],max_conformaciones_2);
			if (devices->conformaciones[j] < max_conformaciones_2)
			{
				//COMPROBAR SI CABE EN MEMORIA
				cudaSetDevice(devices->id[j]);
				//cudaDeviceReset();
				cudaMemGetInfo(&tam_max, &total_m);
				cudaDeviceSynchronize();
				cudaStatus = cudaGetLastError();
				if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
				//std::cout << tam_max << " " << total_m << "\n";
				tam_tmp = tam_max - (tam_max * 0.2);
				if (tam_tmp < max_memory)
				{
					//printf("Trocear en 1 GPU\n");
					max_memory = max_common + max_flex_conf;
					nconformations_count_total = 0;
					nconformations_count_partial = 0;
					stride_d_memory = 0;
					while (nconformations_count_total < devices->conformaciones[j])
					{
						max_memory += tam_conformation;
						if (max_memory > tam_tmp)
						{
							//printf("Trozo %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d_memory, vectors_e_s->nconformations);
							//nconformations_count_total--;
							//nconformations_count_partial--;
							//Lanzar montencarlo con nconformations_count_partial;							
							multigpu_mejorar_gpusolver_vs_flex (backup_conformations,files,nlig,ligtype+((stride_d+stride_d_memory)*nlig),bonds+((stride_d+stride_d_memory)*nlig*MAXBOND),ql+((stride_d+stride_d_memory)*nlig),ligand_params,flexibility_params,proteina,param,weights,f_params,devices,conformations_x+((stride_d+stride_d_memory)*nlig),conformations_y+((stride_d+stride_d_memory)*nlig),conformations_z+((stride_d+stride_d_memory)*nlig),move_x+(stride_d+stride_d_memory),move_y+(stride_d+stride_d_memory),move_z+(stride_d+stride_d_memory),quat_x+(stride_d+stride_d_memory),quat_y+(stride_d+stride_d_memory),quat_z+(stride_d+stride_d_memory),quat_w+(stride_d+stride_d_memory),energy+(stride_d+stride_d_memory),nconfs+(stride_d+stride_d_memory),metaheuristic,nconformations_count_partial,j,(stride_d+stride_d_memory),fase);
							stride_d_memory += nconformations_count_partial;
							max_memory = max_common + max_flex_conf;
							nconformations_count_partial = 0;
						}
						else
						{
							nconformations_count_total++;
							nconformations_count_partial++;
						}
					}
					//printf("Trozo final %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
					nconformations_count_partial--;					
					multigpu_mejorar_gpusolver_vs_flex (backup_conformations,files,nlig,ligtype+((stride_d+stride_d_memory)*nlig),bonds+((stride_d+stride_d_memory)*nlig*MAXBOND),ql+((stride_d+stride_d_memory)*nlig),ligand_params,flexibility_params,proteina,param,weights,f_params,devices,conformations_x+((stride_d+stride_d_memory)*nlig),conformations_y+((stride_d+stride_d_memory)*nlig),conformations_z+((stride_d+stride_d_memory)*nlig),move_x+(stride_d+stride_d_memory),move_y+(stride_d+stride_d_memory),move_z+(stride_d+stride_d_memory),quat_x+(stride_d+stride_d_memory),quat_y+(stride_d+stride_d_memory),quat_z+(stride_d+stride_d_memory),quat_w+(stride_d+stride_d_memory),energy+(stride_d+stride_d_memory),nconfs+(stride_d+stride_d_memory),metaheuristic,nconformations_count_partial,j,(stride_d+stride_d_memory),fase);
				}	
				else
				{
					//printf("No Trocea\n");
					//Lanzar montecarlo con todo					
					multigpu_mejorar_gpusolver_vs_flex (backup_conformations,files,nlig,ligtype+(stride_d*nlig),bonds+(stride_d*nlig*MAXBOND),ql+(stride_d*nlig),ligand_params,flexibility_params,proteina,param,weights,f_params,devices,conformations_x+(stride_d*nlig),conformations_y+(stride_d*nlig),conformations_z+(stride_d*nlig),move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,nconfs+stride_d,metaheuristic,devices->conformaciones[j],j,stride_d,fase);
				}
			}
			else
			{
				//DIVIDIR EN TROZOS
				while(devices->conformaciones[j] % max_conformaciones_2 != 0) max_conformaciones_2 --;
				//printf("va a trocear\n");			
				for (unsigned int desplazamiento=0;desplazamiento<devices->conformaciones[j];desplazamiento+=max_conformaciones_2)
				{
					//COMPROBAR SI CABE EN MEMORIA
					cudaSetDevice(devices->id[j]);
					//cudaDeviceReset();
					cudaMemGetInfo(&tam_max, &total_m);
					cudaDeviceSynchronize();
					cudaStatus = cudaGetLastError();
					if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
					//std::cout << tam_max << " " << total_m << "\n";
					tam_tmp = tam_max - (tam_max * 0.2);
					if (tam_tmp < max_memory)
					{
						//printf("Trocear en 1 GPU\n");
						max_memory = max_common + max_flex_conf;
						nconformations_count_total = 0;
						nconformations_count_partial = 0;
						stride_d_memory = 0;
						while (nconformations_count_total < devices->conformaciones[j])
						{
							max_memory += tam_conformation;
							if (max_memory > tam_tmp)
							{
								//printf("Trozo %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d_memory, vectors_e_s->nconformations);
								//nconformations_count_total--;
								//nconformations_count_partial--;
								//Lanzar montencarlo con nconformations_count_partial;															
								multigpu_mejorar_gpusolver_vs_flex (backup_conformations,files,nlig,ligtype+((stride_d+desplazamiento+stride_d_memory)*nlig),bonds+((stride_d+desplazamiento+stride_d_memory)*nlig*MAXBOND),ql+((stride_d+desplazamiento+stride_d_memory)*nlig),ligand_params,flexibility_params,proteina,param,weights,f_params,devices,conformations_x+((stride_d+desplazamiento+stride_d_memory)*nlig),conformations_y+((stride_d+desplazamiento+stride_d_memory)*nlig),conformations_z+((stride_d+desplazamiento+stride_d_memory)*nlig),move_x+(stride_d+desplazamiento+stride_d_memory),move_y+(stride_d+desplazamiento+stride_d_memory),move_z+(stride_d+desplazamiento+stride_d_memory),quat_x+(stride_d+desplazamiento+stride_d_memory),quat_y+(stride_d+desplazamiento+stride_d_memory),quat_z+(stride_d+desplazamiento+stride_d_memory),quat_w+(stride_d+desplazamiento+stride_d_memory),energy+(stride_d+desplazamiento+stride_d_memory),nconfs+(stride_d+desplazamiento+stride_d_memory),metaheuristic,max_conformaciones_2,j,(stride_d+desplazamiento+stride_d_memory),fase);
								stride_d_memory += nconformations_count_partial;
								max_memory = max_common + max_flex_conf;
								nconformations_count_partial = 0;
							}
							else
							{
								nconformations_count_total++;
								nconformations_count_partial++;
							}
						}
						//printf("Trozo final %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
						nconformations_count_partial--;					
						multigpu_mejorar_gpusolver_vs_flex (backup_conformations,files,nlig,ligtype+((stride_d+desplazamiento+stride_d_memory)*nlig),bonds+((stride_d+desplazamiento+stride_d_memory)*nlig*MAXBOND),ql+((stride_d+desplazamiento+stride_d_memory)*nlig),ligand_params,flexibility_params,proteina,param,weights,f_params,devices,conformations_x+((stride_d+desplazamiento+stride_d_memory)*nlig),conformations_y+((stride_d+desplazamiento+stride_d_memory)*nlig),conformations_z+((stride_d+desplazamiento+stride_d_memory)*nlig),move_x+(stride_d+desplazamiento+stride_d_memory),move_y+(stride_d+desplazamiento+stride_d_memory),move_z+(stride_d+desplazamiento+stride_d_memory),quat_x+(stride_d+desplazamiento+stride_d_memory),quat_y+(stride_d+desplazamiento+stride_d_memory),quat_z+(stride_d+desplazamiento+stride_d_memory),quat_w+(stride_d+desplazamiento+stride_d_memory),energy+(stride_d+desplazamiento+stride_d_memory),nconfs+(stride_d+desplazamiento+stride_d_memory),metaheuristic,max_conformaciones_2,j,(stride_d+desplazamiento+stride_d_memory),fase);
					}	
					else
					{
						//printf("No Trocea\n");
						//Lanzar montecarlo con todo
						multigpu_mejorar_gpusolver_vs_flex (backup_conformations,files,nlig,ligtype+((stride_d+desplazamiento)*nlig),bonds+((stride_d+desplazamiento)*nlig*MAXBOND),ql+((stride_d+desplazamiento)*nlig),ligand_params,flexibility_params,proteina,param,weights,f_params,devices,conformations_x+((stride_d+desplazamiento)*nlig),conformations_y+((stride_d+desplazamiento)*nlig),conformations_z+((stride_d+desplazamiento)*nlig),move_x+(stride_d+desplazamiento),move_y+(stride_d+desplazamiento),move_z+(stride_d+desplazamiento),quat_x+(stride_d+desplazamiento),quat_y+(stride_d+desplazamiento),quat_z+(stride_d+desplazamiento),quat_w+(stride_d+desplazamiento),energy+(stride_d+desplazamiento),nconfs+(stride_d+desplazamiento),metaheuristic,max_conformaciones_2,j,(stride_d+desplazamiento),fase);
					}
					
					//printf("Device %d con trozo %d\n",devices->id[th],i);				
				}
			}
		}
	}
	tiempo_f = wtime() - tiempo_i;
	
}


extern void mejorar_multigpu_vs_flex (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct flexibility_params_t *flexibility_params, struct param_t param, struct vectors_t *vectors_selec, struct metaheuristic_t metaheuristic, struct device_t *devices,unsigned int fase)
{
		
	multigpuSolver_mejora_vs_flex (vectors_selec->files,vectors_selec->nlig,vectors_selec->ligtype,vectors_selec->bonds,vectors_selec->ql,ligand_params,flexibility_params,proteina,vectors_selec->conformations_x,vectors_selec->conformations_y,vectors_selec->conformations_z,vectors_selec->move_x,vectors_selec->move_y,vectors_selec->move_z,vectors_selec->quat_x,vectors_selec->quat_y,vectors_selec->quat_z,vectors_selec->quat_w,vectors_selec->energy.energy,vectors_selec->energy.n_conformation,vectors_selec->weights,vectors_selec->f_params,vectors_selec->nconformations,param,metaheuristic,devices,fase);					
}

extern void mejorar_multigpu_vs_flex_environment (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct flexibility_params_t *flexibility_params, struct param_t param, struct vectors_t *vectors_selec, struct metaheuristic_t metaheuristic, struct device_t *devices,unsigned int fase)
{

        multigpuSolver_mejora_vs_flex_environment (vectors_selec->files,vectors_selec->nlig,vectors_selec->ligtype,vectors_selec->bonds,vectors_selec->ql,ligand_params,flexibility_params,proteina,vectors_selec->conformations_x,vectors_selec->conformations_y,vectors_selec->conformations_z,vectors_selec->move_x,vectors_selec->move_y,vectors_selec->move_z,vectors_selec->quat_x,vectors_selec->quat_y,vectors_selec->quat_z,vectors_selec->quat_w,vectors_selec->energy.energy,vectors_selec->energy.n_conformation,vectors_selec->weights,vectors_selec->f_params,vectors_selec->nconformations,param,metaheuristic,devices,fase);
}
