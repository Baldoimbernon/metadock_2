#include <omp.h>
#include "energy_common.h"
#include "rotation.h"
#include "energy_CpuSolver_vs.h"
#include "energy_CpuSolver_vs_flex.h"
#include "energy_CpuSolver.h"
#include "energy_CpuSolver_flex.h"
#include "energy_CpuSolver_common.h"
#include "metaheuristic_ini_flex.h"
#include "montecarlo.h"
#include "flexibility.h"
#include "flexibility_vs.h"
#include "wtime.h"
#include <thrust/sequence.h>

void move_mejora_montecarlo_vs (unsigned int nconformations, type_data *move_x_m, type_data *move_y_m, type_data *move_z_m, type_data *move_x, type_data *move_y, type_data *move_z, struct param_t param, struct metaheuristic_t metaheuristic){
	
	int th1;
	switch (param.mode) {
		case 1:	
			th1 = 1;
			break;
		case 2:
			th1 = metaheuristic.Threads1Ini;
			break;
	}
	omp_set_num_threads (th1);
	#pragma omp parallel for 	
	for (unsigned int i=0; i < nconformations; i++){	
		move_x[i] = move_x_m[i];
		move_y[i] = move_y_m[i];
		move_z[i] = move_z_m[i];
		//printf("num_surface %d, NEIIni_selec %d\n",i,j);
		//move_x_m[i] += getRealRandomNumber(param.max_desp); //Desplazamiento entre -max_desp y max_desp
		//move_y_m[i] += getRealRandomNumber(param.max_desp);
		//move_z_m[i] += getRealRandomNumber(param.max_desp);
		move_x_m[i] += getRealRandomNumber_r(param.max_desp,param.automatic_seed,param.seed,i+param.seed);
                move_y_m[i] += getRealRandomNumber_r(param.max_desp,param.automatic_seed,param.seed,i+param.seed);
                move_z_m[i] += getRealRandomNumber_r(param.max_desp,param.automatic_seed,param.seed,i+param.seed);
		//printf("Estoy en Move, coformacion %d\n",i);
		//printf("coformation %d, move_x[%d] = %f, move_y[%d] = %f, move_z[%d] = %f, aleatorio = %f\n",i,i*BLOCK_128+j,moves_x_h[i*BLOCK_128+j],i*BLOCK_128+j,moves_y_h[i*BLOCK_128+j],i*BLOCK_128+j,moves_z_h[i*BLOCK_128+j],2 * frand());
	}
}

void move_mejora_montecarlo_vs_environment (unsigned int nconformations, type_data *move_x_m, type_data *move_y_m, type_data *move_z_m, struct param_t param, struct metaheuristic_t metaheuristic){

        int th1;
        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 2:
                        th1 = metaheuristic.Threads1Ini;
                        break;
        }
        omp_set_num_threads (th1);
        #pragma omp parallel for
        for (unsigned int i=0; i < nconformations; i++){
                //move_x[i/metaheuristic.NEEImp] = move_x_m[i];
                //move_y[i/metaheuristic.NEEImp] = move_y_m[i];
                //move_z[i/metaheuristic.NEEIMP] = move_z_m[i];
                //printf("num_surface %d, NEIIni_selec %d\n",i,j);
                //move_x_m[i] += getRealRandomNumber(param.max_desp); //Desplazamiento entre -max_desp y max_desp
                //move_y_m[i] += getRealRandomNumber(param.max_desp);
                //move_z_m[i] += getRealRandomNumber(param.max_desp);
		move_x_m[i] += getRealRandomNumber_r(param.max_desp,param.automatic_seed,param.seed,i+param.seed);
                move_y_m[i] += getRealRandomNumber_r(param.max_desp,param.automatic_seed,param.seed,i+param.seed);
                move_z_m[i] += getRealRandomNumber_r(param.max_desp,param.automatic_seed,param.seed,i+param.seed);
                //printf("Estoy en Move, coformacion %d\n",i);
                //printf("coformation %d, move_x[%d] = %f, move_y[%d] = %f, move_z[%d] = %f, aleatorio = %f\n",i,i*BLOCK_128+j,moves_x_h[i*BLOCK_128+j],i*BLOCK_128+j,moves_y_h[i*BLOCK_128+j],i*BLOCK_128+j,moves_z_h[i*BLOCK_128+j],2 * frand());
        }
}

void rotate_montecarlo_vs_environment (type_data *quat_x_m, type_data *quat_y_m, type_data *quat_z_m, type_data *quat_w_m, struct param_t param, int nconformations, struct metaheuristic_t metaheuristic){

        type_data angle, eje[3];
        unsigned int dato;
        unsigned int th1;
        type_data local_x, quat_tmp_x, local_y, quat_tmp_y, local_z, quat_tmp_z, local_w, quat_tmp_w ;

        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 2:
                        th1 = metaheuristic.Threads1Ini;
                        break;
        }

        omp_set_num_threads (th1);
        #pragma omp parallel for private (local_x, quat_tmp_x, local_y, quat_tmp_y, local_z, quat_tmp_z, local_w, quat_tmp_w,eje,angle)
        for (unsigned int i=0; i < nconformations;i++)
        {
                //quat_x[i] = quat_x_m[i];
                //quat_y[i] = quat_y_m[i];
                //quat_z[i] = quat_z_m[i];
                //quat_w[i] = quat_w_m[i];

                quat_tmp_x = quat_x_m[i];
                quat_tmp_y = quat_y_m[i];
                quat_tmp_z = quat_z_m[i];
                quat_tmp_w = quat_w_m[i];
                angle = getRealRandomNumber(param.rotation);
                eje[0] = getRealRandomNumber (1);
                eje[1] = getRealRandomNumber (1);
                eje[2] = getRealRandomNumber (1);

                setRotation_cpp (&local_x, &local_y, &local_z, &local_w, angle, eje);
                composeRotation_cpp (&local_x, &local_y, &local_z, &local_w, &quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w, &quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w);
                normalize_cpp(&quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w );

                quat_x_m[i] = quat_tmp_x;
                quat_y_m[i] = quat_tmp_y;
                quat_z_m[i] = quat_tmp_z;
                quat_w_m[i] = quat_tmp_w;
                //printf("Estoy en Rotation, conformacion %d, eje[0] = %f, eje[1] = %f, eje[2] = %f, x = %f, y = %f, z= %f, w = %f\n",i,eje[0],eje[1],eje[2],quat_tmp.x,quat_tmp.y,quat_tmp.z,quat_tmp.w);
        }

}

void rotate_montecarlo_vs (type_data *quat_x_m, type_data *quat_y_m, type_data *quat_z_m, type_data *quat_w_m, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, struct param_t param, int nconformations, struct metaheuristic_t metaheuristic){

	type_data angle, eje[3];
	unsigned int dato;
	unsigned int th1;
	type_data local_x, quat_tmp_x, local_y, quat_tmp_y, local_z, quat_tmp_z, local_w, quat_tmp_w ;
	
	switch (param.mode) {
		case 1:	
			th1 = 1;
			break;
		case 2:
			th1 = metaheuristic.Threads1Ini;
			break;
	}
	
	omp_set_num_threads (th1);
	#pragma omp parallel for private (local_x, quat_tmp_x, local_y, quat_tmp_y, local_z, quat_tmp_z, local_w, quat_tmp_w,eje,angle)
	for (unsigned int i=0; i < nconformations;i++)
	{
		quat_x[i] = quat_x_m[i];
		quat_y[i] = quat_y_m[i];
		quat_z[i] = quat_z_m[i];
		quat_w[i] = quat_w_m[i];
				
		quat_tmp_x = quat_x_m[i];	
		quat_tmp_y = quat_y_m[i];							
		quat_tmp_z = quat_z_m[i];							
		quat_tmp_w = quat_w_m[i];							
		angle = getRealRandomNumber(param.rotation);
		eje[0] = getRealRandomNumber (1);
		eje[1] = getRealRandomNumber (1);
		eje[2] = getRealRandomNumber (1);

		setRotation_cpp (&local_x, &local_y, &local_z, &local_w, angle, eje);				
		composeRotation_cpp (&local_x, &local_y, &local_z, &local_w, &quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w, &quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w);
		normalize_cpp(&quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w );

		quat_x_m[i] = quat_tmp_x;
		quat_y_m[i] = quat_tmp_y;
		quat_z_m[i] = quat_tmp_z;
		quat_w_m[i] = quat_tmp_w;
		//printf("Estoy en Rotation, conformacion %d, eje[0] = %f, eje[1] = %f, eje[2] = %f, x = %f, y = %f, z= %f, w = %f\n",i,eje[0],eje[1],eje[2],quat_tmp.x,quat_tmp.y,quat_tmp.z,quat_tmp.w);
	}
			
}

void updateMove_vs (unsigned int nconformations, type_data *move_x_m, type_data *move_y_m, type_data *move_z_m, type_data *move_x, type_data *move_y, type_data *move_z, type_data *energy_sig, type_data *energy_ant, struct param_t param, unsigned int tam, struct metaheuristic_t metaheuristic)
{
	type_data cond;
	unsigned int id, th1;
	
	switch (param.mode) {
		case 1:	
			th1 = 1;
			break;
		case 2:
			th1 = metaheuristic.Threads1Ini;
			break;
	}
	
	omp_set_num_threads (th1);
	#pragma omp parallel for private (cond)
	for (id=0; id < nconformations; id++){				
		cond = (energy_sig[id] < energy_ant[id] || getRealRandomNumber(param.max_desp) <= exp(-(energy_sig[id] - energy_ant[id])) );
		if (cond)
		{
			move_x[id] = move_x_m[id] * cond + move_x[id] * !cond;
			move_y[id] = move_y_m[id] * cond + move_y[id] * !cond;
			move_z[id] = move_z_m[id] * cond + move_z[id] * !cond;
		}
		else
		{
			move_x_m[id] = move_x[id] * !cond;
			move_y_m[id] = move_y[id] * !cond;
			move_z_m[id] = move_z[id] * !cond;
		}
		energy_ant[id] = energy_sig[id] * cond + energy_ant[id] * !cond;
	}
}

void updateMove_vs_environment (unsigned int nconformations, type_data *move_x_m, type_data *move_y_m, type_data *move_z_m, type_data *move_x, type_data *move_y, type_data *move_z, type_data *energy_sig, type_data *energy_ant, struct param_t param, unsigned int tam, struct metaheuristic_t metaheuristic)
{
        type_data cond;
        unsigned int id, th1;

        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 2:
                        th1 = metaheuristic.Threads1Ini;
                        break;
        }

	 omp_set_num_threads (th1);
        #pragma omp parallel for private (cond)
        for (unsigned int id=0; id < nconformations; id++){

                for (unsigned int j=0; j < metaheuristic.NEEImp; j++)
                {
                	cond = (energy_sig[id*metaheuristic.NEEImp + j] < energy_ant[id] || getRealRandomNumber(param.max_desp) <= exp(-(energy_sig[id*metaheuristic.NEEImp + j] - energy_ant[id])) );
                	if (cond)
               		{	
                        	move_x[id] = move_x_m[id*metaheuristic.NEEImp + j] * cond + move_x[id] * !cond;
                        	move_y[id] = move_y_m[id*metaheuristic.NEEImp + j] * cond + move_y[id] * !cond;
                        	move_z[id] = move_z_m[id*metaheuristic.NEEImp + j] * cond + move_z[id] * !cond;
                	}
                	else
                	{
                        	move_x_m[id*metaheuristic.NEEImp + j] = move_x[id] * !cond;
                        	move_y_m[id*metaheuristic.NEEImp + j] = move_y[id] * !cond;
                        	move_z_m[id*metaheuristic.NEEImp + j] = move_z[id] * !cond;
                	}
                	energy_ant[id] = energy_sig[id*metaheuristic.NEEImp + j] * cond + energy_ant[id] * !cond;
		}
        }
}

void updateRotation_vs (type_data *quat_x_m, type_data *quat_y_m, type_data *quat_z_m, type_data *quat_w_m, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy_sig, type_data *energy_ant, struct param_t param, unsigned int nconformations, struct metaheuristic_t metaheuristic)
{
	type_data cond;
	unsigned int th1;
	
	switch (param.mode) {
		case 1:	
			th1 = 1;
			break;
		case 2:
			th1 = metaheuristic.Threads1Ini;
			break;
	}
	omp_set_num_threads (th1);
	#pragma omp parallel for private (cond) 
	for (unsigned int id=0; id < nconformations; id++)
	{				
		cond = (energy_sig[id] < energy_ant[id] || getRealRandomNumber(param.max_desp) <= exp(-(energy_sig[id] - energy_ant[id])) );
		if (cond)
		{
			quat_x[id] = quat_x_m[id] * cond + quat_x[id] * !cond;		
			quat_y[id] = quat_y_m[id] * cond + quat_y[id] * !cond;
			quat_z[id] = quat_z_m[id] * cond + quat_z[id] * !cond;
			quat_w[id] = quat_w_m[id] * cond + quat_w[id] * !cond;
		}
		else
		{
			quat_x_m[id] = quat_x[id] * !cond;
			quat_y_m[id] = quat_y[id] * !cond;
			quat_z_m[id] = quat_z[id] * !cond;
			quat_w_m[id] = quat_w[id] * !cond;
		}
		energy_ant[id] = energy_sig[id] * cond + energy_ant[id] * !cond;
	}
		
}

void updateRotation_vs_environment (type_data *quat_x_m, type_data *quat_y_m, type_data *quat_z_m, type_data *quat_w_m, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy_sig, type_data *energy_ant, struct param_t param, unsigned int nconformations, struct metaheuristic_t metaheuristic)
{
        type_data cond;
        unsigned int th1;

        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 2:
                        th1 = metaheuristic.Threads1Ini;
                        break;
        }
	 omp_set_num_threads (th1);
        #pragma omp parallel for private (cond)
        for (unsigned int id=0; id < nconformations; id++)
        {
                for (unsigned int j=0; j < metaheuristic.NEEImp; j++)
                {
			cond = (energy_sig[id*metaheuristic.NEEImp + j] < energy_ant[id] || getRealRandomNumber(param.max_desp) <= exp(-(energy_sig[id*metaheuristic.NEEImp + j] - energy_ant[id])) );
                	if (cond)
                	{
                        	quat_x[id] = quat_x_m[id*metaheuristic.NEEImp + j] * cond + quat_x[id] * !cond;
                        	quat_y[id] = quat_y_m[id*metaheuristic.NEEImp + j] * cond + quat_y[id] * !cond;
                        	quat_z[id] = quat_z_m[id*metaheuristic.NEEImp + j] * cond + quat_z[id] * !cond;
                        	quat_w[id] = quat_w_m[id*metaheuristic.NEEImp + j] * cond + quat_w[id] * !cond;
                	}
                	else
                	{
                        	quat_x_m[id*metaheuristic.NEEImp + j] = quat_x[id] * !cond;
                        	quat_y_m[id*metaheuristic.NEEImp + j] = quat_y[id] * !cond;
                        	quat_z_m[id*metaheuristic.NEEImp + j] = quat_z[id] * !cond;
                        	quat_w_m[id*metaheuristic.NEEImp + j] = quat_w[id] * !cond;
                	}
                	energy_ant[id] = energy_sig[id*metaheuristic.NEEImp + j] * cond + energy_ant[id] * !cond;
		}
	}
}

extern void montecarlo_cpp_vs_environment (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct param_t param, type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *ql, char *ligtype, unsigned int *bonds, unsigned int nlig, unsigned int files, unsigned int nconformations, struct metaheuristic_t metaheuristic, unsigned int opt)
{
        struct vectors_t vectors_mejora;
        unsigned int steps,moveType=MOVE,bucle,total,simulations,option;
        type_data max_angle_flex;
        unsigned int tam = (nconformations * metaheuristic.NEEImp) / files;
	unsigned int *nlinks = (unsigned int *)calloc(files,sizeof(unsigned int));
        vectors_mejora.files = files;
        vectors_mejora.nlig = nlig;
        vectors_mejora.nconformations = nconformations * metaheuristic.NEEImp;

        vectors_mejora.move_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.move_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.move_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations);
        vectors_mejora.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.weights = (type_data *)malloc(SCORING_TERMS * sizeof(type_data));

	memcpy(vectors_mejora.f_params, f_params,MAXTYPES * sizeof(struct force_field_param_t));
	memcpy(vectors_mejora.weights, weights,SCORING_TERMS * sizeof(type_data));

	thrust::sequence(vectors_mejora.energy.n_conformation,vectors_mejora.energy.n_conformation + vectors_mejora.nconformations);

        fill_conformations_environment (move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,nconformations,param,metaheuristic);

	if (opt == 0)
                total = metaheuristic.IMEIni;
        if (opt == 1)
                total = metaheuristic.IMEImp;
        if (opt == 2)
                total = metaheuristic.IMEMUCom;

        steps = 0;
        option = 0;
        max_angle_flex = param.flex_angle;

        while (steps < total){

                if (moveType == MOVE){
                        move_mejora_montecarlo_vs_environment(vectors_mejora.nconformations,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,param,metaheuristic);
                        //printf("He movido\n");
                }
                else {
                        rotate_montecarlo_vs_environment(vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,param,vectors_mejora.nconformations,metaheuristic);
                }
                //printf("Voy a calcular la energia...\n");
                //ENERGIA
                ForcesCpuSolver_vs(nlinks,vectors_mejora.nlig,vectors_mejora.files,param,proteina,ligand_params,conformations_x,conformations_y,conformations_z,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,ligtype,ql,bonds,vectors_mejora.energy.energy,vectors_mejora.energy.n_conformation,vectors_mejora.weights,vectors_mejora.f_params,vectors_mejora.nconformations,metaheuristic);

        	 if (moveType == MOVE)
                {
                        updateMove_vs_environment (nconformations,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,move_x,move_y,move_z,vectors_mejora.energy.energy,energy,param,tam,metaheuristic);
                        moveType = ROTATE;
                }
                else
                {
                        updateRotation_vs_environment (vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,quat_x,quat_y,quat_z,quat_w,vectors_mejora.energy.energy,energy,param,nconformations,metaheuristic);
                        moveType = MOVE;
                }
		//fill_conformations_environment (move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,nconformations,param,metaheuristic);
                steps++;
        }
        free(vectors_mejora.move_x);
        free(vectors_mejora.move_y);
        free(vectors_mejora.move_z);
        free(vectors_mejora.quat_x);
        free(vectors_mejora.quat_y);
        free(vectors_mejora.quat_z);
        free(vectors_mejora.quat_w);
        free(vectors_mejora.energy.energy);
        free(vectors_mejora.energy.n_conformation);
        free(vectors_mejora.weights);        

}
extern void montecarlo_cpp_vs (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct param_t param, type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *ql, char *ligtype, unsigned int *bonds, unsigned int nlig, unsigned int files, unsigned int nconformations, struct metaheuristic_t metaheuristic, unsigned int opt)
{
	struct vectors_t vectors_mejora;
	unsigned int steps,moveType=MOVE,bucle,total,simulations,option;	
	type_data max_angle_flex;
	unsigned int tam = nconformations / files;
	unsigned int *nlinks = (unsigned int *)calloc(files,sizeof(unsigned int));
	vectors_mejora.files = files;
	vectors_mejora.nlig = nlig;
	vectors_mejora.nconformations = nconformations;

	vectors_mejora.move_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.move_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.move_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations);
	vectors_mejora.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);	
	vectors_mejora.weights = (type_data *)malloc(SCORING_TERMS * sizeof(type_data));
 	
	memcpy(vectors_mejora.move_x, move_x, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.move_y, move_y, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.move_z, move_z, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_x, quat_x, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_y, quat_y, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_z, quat_z, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_w, quat_w, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.energy.energy, energy, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.energy.n_conformation, nconfs, vectors_mejora.nconformations * sizeof(unsigned int));

    memcpy(vectors_mejora.f_params, f_params,MAXTYPES * sizeof(struct force_field_param_t));
    memcpy(vectors_mejora.weights, weights,SCORING_TERMS * sizeof(type_data));

		
	
	if (opt == 0)
		total = metaheuristic.IMEIni;		
	if (opt == 1)	
		total = metaheuristic.IMEImp;
	if (opt == 2)
		total = metaheuristic.IMEMUCom;
	
	steps = 0;	
	option = 0;
	max_angle_flex = param.flex_angle;
	
	while (steps < total){

		if (moveType == MOVE){
			move_mejora_montecarlo_vs(vectors_mejora.nconformations,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,move_x,move_y,move_z,param,metaheuristic);			
			//printf("He movido\n");
		}
		else {
			rotate_montecarlo_vs(vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,quat_x,quat_y,quat_z,quat_w,param,vectors_mejora.nconformations,metaheuristic);
		}		
		//printf("Voy a calcular la energia...\n");
		//ENERGIA		
		ForcesCpuSolver_vs(nlinks,vectors_mejora.nlig,vectors_mejora.files,param,proteina,ligand_params,conformations_x,conformations_y,conformations_z,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,ligtype,ql,bonds,vectors_mejora.energy.energy,vectors_mejora.energy.n_conformation,vectors_mejora.weights,vectors_mejora.f_params,vectors_mejora.nconformations,metaheuristic);
		
		if (moveType == MOVE)
		{
			updateMove_vs (vectors_mejora.nconformations,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,move_x,move_y,move_z,vectors_mejora.energy.energy,energy,param,tam,metaheuristic);
			moveType = ROTATE;
		}
		else
		{
			updateRotation_vs(vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,quat_x,quat_y,quat_z,quat_w,vectors_mejora.energy.energy,energy,param,vectors_mejora.nconformations,metaheuristic);
			moveType = MOVE;
		}
		steps++;
	}
	free(vectors_mejora.move_x);
	free(vectors_mejora.move_y);
	free(vectors_mejora.move_z);
	free(vectors_mejora.quat_x);
	free(vectors_mejora.quat_y);
	free(vectors_mejora.quat_z);
	free(vectors_mejora.quat_w);
	free(vectors_mejora.energy.energy);
	free(vectors_mejora.energy.n_conformation);
	free(vectors_mejora.weights);
	//printf("End Mont\n");
	//exit(0);
}

extern void montecarlo_cpp_vs_flex_environment (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct param_t param, type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *ql, char *ligtype, unsigned int *bonds, unsigned int nlig, unsigned int files, unsigned int nconformations, struct metaheuristic_t metaheuristic,  struct flexibility_params_t *flexibility_params, struct flexibility_data_t *flexibility_conformations, unsigned int opt)
{
	struct vectors_t vectors_mejora;
        unsigned int steps,moveType=MOVE,bucle,total,simulations,option;
        type_data max_angle_flex;
        unsigned int tam = (nconformations * metaheuristic.NEEImp) / files;
	unsigned int *nlinks = (unsigned int *)calloc(files,sizeof(unsigned int));
        vectors_mejora.files = files;
        vectors_mejora.nlig = nlig;
        vectors_mejora.nconformations = nconformations * metaheuristic.NEEImp;

        vectors_mejora.conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nlig*vectors_mejora.nconformations);
        vectors_mejora.conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nlig*vectors_mejora.nconformations);
        vectors_mejora.conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nlig*vectors_mejora.nconformations);

	vectors_mejora.ql = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations*nlig);
        vectors_mejora.ligtype = (char *)malloc(sizeof(char)*vectors_mejora.nconformations*nlig);
        vectors_mejora.bonds = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations*nlig*MAXBOND);

        vectors_mejora.move_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.move_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.move_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations);
        vectors_mejora.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.weights = (type_data *)malloc(SCORING_TERMS * sizeof(type_data));

        type_data *internal_energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        flexibility_conformations = (flexibility_data_t *)malloc(sizeof(flexibility_data_t)*vectors_mejora.nconformations);
	
        memcpy(vectors_mejora.f_params, f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_mejora.weights, weights,SCORING_TERMS * sizeof(type_data));

	thrust::sequence(vectors_mejora.energy.n_conformation,vectors_mejora.energy.n_conformation + vectors_mejora.nconformations);
	
	fill_conformations_environment_vs (vectors_mejora.nlig,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,ligtype,ql,bonds,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,vectors_mejora.ligtype,vectors_mejora.ql,vectors_mejora.bonds,nconformations,param,metaheuristic);

	for (int i=0; i < files; i++) nlinks[i]=flexibility_params[i].n_links;

	if (opt == 0)
        {
                total = metaheuristic.IMEIni + metaheuristic.IMEFlex;
                simulations = metaheuristic.IMEIni;
        }
        if (opt == 1)
        {
                total = metaheuristic.IMEImp + metaheuristic.IMEFlex;
                simulations = metaheuristic.IMEImp;
        }
        if (opt == 2)
        {
                total = metaheuristic.IMEMUCom;
                simulations = metaheuristic.IMEMUCom;
        }

        steps = 0;
        option = 0;
        max_angle_flex = param.flex_angle;

        while (steps < total){

                //printf("step %d\n", steps);
                //REFINAMIENTO DEL ANGULO FLEXIBLE, MOVIMIENTO Y ORIENTACIÓN DEL LIGANDO
                if (steps == simulations) max_angle_flex = param.flex_angle;
                if (((steps % 50) == 0) && (steps > 0) && (steps <simulations))
                        max_angle_flex /= 1.2;

                if (steps >=simulations)
                {
                        //FLEXIBILIDAD
                        angulations_conformations_cpp_vs (vectors_mejora.nconformations,vectors_mejora.files,metaheuristic,param,flexibility_conformations,flexibility_params,param.flex_angle);
                        flexibility_cpp_vs (vectors_mejora.nconformations,vectors_mejora.files,metaheuristic,param,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,flexibility_conformations,flexibility_params,vectors_mejora.nlig,param.flex_angle);
                        max_angle_flex /= 1.1;
                }
                else
                {
                        option = steps % 3;
                        switch (option)
                        {
                        case 0:
                                move_mejora_montecarlo_vs_environment(vectors_mejora.nconformations,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,param,metaheuristic);
                                //printf("He movido\n");
                                break;
                        case 1:
                                rotate_montecarlo_vs_environment(vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,param,vectors_mejora.nconformations,metaheuristic);
                                break;
                        case 2:
                                angulations_conformations_cpp_vs (vectors_mejora.nconformations,vectors_mejora.files,metaheuristic,param,flexibility_conformations,flexibility_params,param.flex_angle);
                                flexibility_cpp_vs (vectors_mejora.nconformations,vectors_mejora.files,metaheuristic,param,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,flexibility_conformations,flexibility_params,vectors_mejora.nlig,param.flex_angle);
                                break;
                        }
                }
                //printf("Voy a calcular la energia...\n");
                //ENERGIA
		//for (int i=0;i<vectors_mejora.nconformations*nlig;i++) printf("id %d x %f y %f z %f ql %f lig %d \n",i,vectors_mejora.conformations_x[i],vectors_mejora.conformations_y[i],vectors_mejora.conformations_z[i],vectors_mejora.ql[i],vectors_mejora.ligtype[i]);

                ForcesCpuSolver_vs(nlinks,vectors_mejora.nlig,vectors_mejora.files,param,proteina,ligand_params,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,vectors_mejora.ligtype,vectors_mejora.ql,vectors_mejora.bonds,vectors_mejora.energy.energy,vectors_mejora.energy.n_conformation,vectors_mejora.weights,vectors_mejora.f_params,vectors_mejora.nconformations,metaheuristic);
                energy_internal_conformations_cpu_vs (vectors_mejora.nconformations,vectors_mejora.nlig,vectors_mejora.files,param,metaheuristic,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,ligand_params,flexibility_params,vectors_mejora.weights,vectors_mejora.f_params,internal_energy,vectors_mejora.ql,vectors_mejora.ligtype);
                energy_total (vectors_mejora.nconformations,param,metaheuristic,vectors_mejora.energy.energy,internal_energy);

		//for (int i=0;i<vectors_mejora.nconformations;i++) printf("STEPS id %d m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f energy %f\n",i,vectors_mejora.move_x[i],vectors_mejora.move_y[i],vectors_mejora.move_z[i],vectors_mejora.quat_x[i],vectors_mejora.quat_y[i],vectors_mejora.quat_z[i],vectors_mejora.energy.energy[i]);
                //exit(0);

		 if (steps >=simulations)
                {
                        //FLEXIBILIDAD
                        update_angulations_conformations_cpp_environment (nconformations,metaheuristic,param,flexibility_conformations,vectors_mejora.energy.energy,energy);
                        flexibility_cpp_vs (vectors_mejora.nconformations,vectors_mejora.files,metaheuristic,param,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,flexibility_conformations,flexibility_params,vectors_mejora.nlig,param.flex_angle);
			incluir_mejorar_inicializar_environment_flex (vectors_mejora.nlig,vectors_mejora,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,nconformations,metaheuristic,param);
                }
                else
                {
                        switch (option)
                        {
                        case 0:
                                updateMove_vs_environment (nconformations,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,move_x,move_y,move_z,vectors_mejora.energy.energy,energy,param,tam,metaheuristic);
                                break;
                        case 1:
                                updateRotation_vs_environment (vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,quat_x,quat_y,quat_z,quat_w,vectors_mejora.energy.energy,energy,param,nconformations,metaheuristic);
                                break;
                        case 2:
                                update_angulations_conformations_cpp_environment (nconformations,metaheuristic,param,flexibility_conformations,vectors_mejora.energy.energy,energy);
                                flexibility_cpp_vs (vectors_mejora.nconformations,vectors_mejora.files,metaheuristic,param,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,flexibility_conformations,flexibility_params,vectors_mejora.nlig,param.flex_angle);
				//incluir_mejorar_inicializar_environment_flex (vectors_mejora.nlig,vectors_mejora,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,nconformations,metaheuristic,param);
                                break;
                        }
                }
		incluir_mejorar_inicializar_environment_flex (vectors_mejora.nlig,vectors_mejora,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,nconformations,metaheuristic,param);

		//fill_conformations_environment_flex (vectors_mejora.nlig,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w, vectors_mejora.conformations_x, vectors_mejora.conformations_y, vectors_mejora.conformations_z, vectors_mejora.move_x, vectors_mejora.move_y, vectors_mejora.move_z, vectors_mejora.quat_x, vectors_mejora.quat_y, vectors_mejora.quat_z, vectors_mejora.quat_w, nconformations, param, metaheuristic);
                steps++;
        }
	free(vectors_mejora.conformations_x);
	free(vectors_mejora.conformations_y);
	free(vectors_mejora.conformations_z);
	free(internal_energy);
	free(flexibility_conformations);
        free(vectors_mejora.move_x);
        free(vectors_mejora.move_y);
        free(vectors_mejora.move_z);
        free(vectors_mejora.quat_x);
        free(vectors_mejora.quat_y);
        free(vectors_mejora.quat_z);
        free(vectors_mejora.quat_w);
        free(vectors_mejora.energy.energy);
        free(vectors_mejora.energy.n_conformation);
        free(vectors_mejora.weights);
	free(nlinks);

}
extern void montecarlo_cpp_vs_flex (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct param_t param,  type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *ql, char *ligtype, unsigned int *bonds, unsigned int nlig, unsigned int files, unsigned int nconformations, struct metaheuristic_t metaheuristic,  struct flexibility_params_t *flexibility_params, struct flexibility_data_t *flexibility_conformations, unsigned int opt)
{
	struct vectors_t vectors_mejora;
	unsigned int steps,moveType=MOVE,bucle,total,simulations,option;	
	type_data max_angle_flex;
	unsigned int tam = nconformations / files;
	unsigned int *nlinks = (unsigned int *)calloc(files,sizeof(unsigned int));
	vectors_mejora.files = files;
	vectors_mejora.nlig = nlig;
	vectors_mejora.nconformations = nconformations;

	vectors_mejora.conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nlig*vectors_mejora.nconformations);
	vectors_mejora.conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nlig*vectors_mejora.nconformations);
	vectors_mejora.conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nlig*vectors_mejora.nconformations);
	
	vectors_mejora.move_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.move_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.move_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations);
	vectors_mejora.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);	
	vectors_mejora.weights = (type_data *)malloc(SCORING_TERMS * sizeof(type_data));
 	
	type_data *internal_energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	flexibility_conformations = (flexibility_data_t *)malloc(sizeof(flexibility_data_t)*vectors_mejora.nconformations);
	
	memcpy(vectors_mejora.f_params, f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_mejora.weights, weights,SCORING_TERMS * sizeof(type_data));

	memcpy(vectors_mejora.conformations_x, conformations_x,vectors_mejora.nconformations*vectors_mejora.nlig*sizeof(type_data)); 
	memcpy(vectors_mejora.conformations_y, conformations_y,vectors_mejora.nconformations*vectors_mejora.nlig*sizeof(type_data)); 
	memcpy(vectors_mejora.conformations_z, conformations_z,vectors_mejora.nconformations*vectors_mejora.nlig*sizeof(type_data));

	memcpy(vectors_mejora.move_x, move_x, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.move_y, move_y, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.move_z, move_z, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_x, quat_x, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_y, quat_y, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_z, quat_z, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_w, quat_w, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.energy.energy, energy, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.energy.n_conformation, nconfs, vectors_mejora.nconformations * sizeof(unsigned int));
	for (int i=0; i < files; i++) nlinks[i]=flexibility_params[i].n_links;	
	
	if (opt == 0)
	{
		total = metaheuristic.IMEIni + metaheuristic.IMEFlex;
		simulations = metaheuristic.IMEIni;
	}
	if (opt == 1)
	{
		total = metaheuristic.IMEImp + metaheuristic.IMEFlex;
		simulations = metaheuristic.IMEImp;
	}
	if (opt == 2)
	{
		total = metaheuristic.IMEMUCom;
                simulations = metaheuristic.IMEMUCom;
	}
	
	steps = 0;	
	option = 0;
	max_angle_flex = param.flex_angle;
	
	while (steps < total){

		//printf("step %d\n", steps);
		//REFINAMIENTO DEL ANGULO FLEXIBLE, MOVIMIENTO Y ORIENTACIÓN DEL LIGANDO
		if (steps == simulations) max_angle_flex = param.flex_angle;
		if (((steps % 50) == 0) && (steps > 0) && (steps <simulations)) 
			max_angle_flex /= 1.2;
		
		if (steps >=simulations)
		{
			//FLEXIBILIDAD
			angulations_conformations_cpp_vs (vectors_mejora.nconformations,vectors_mejora.files,metaheuristic,param,flexibility_conformations,flexibility_params,param.flex_angle);
			flexibility_cpp_vs (vectors_mejora.nconformations,vectors_mejora.files,metaheuristic,param,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,flexibility_conformations,flexibility_params,vectors_mejora.nlig,param.flex_angle);
			max_angle_flex /= 1.1;
		}
		else
		{
			option = steps % 3;
			switch (option)
			{		
			case 0:
				move_mejora_montecarlo_vs(vectors_mejora.nconformations,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,move_x,move_y,move_z,param,metaheuristic);			
				//printf("He movido\n");
				break;
			case 1:
				rotate_montecarlo_vs(vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,quat_x,quat_y,quat_z,quat_w,param,vectors_mejora.nconformations,metaheuristic);
				break;
			case 2:
				angulations_conformations_cpp_vs (vectors_mejora.nconformations,vectors_mejora.files,metaheuristic,param,flexibility_conformations,flexibility_params,param.flex_angle);
				flexibility_cpp_vs (vectors_mejora.nconformations,vectors_mejora.files,metaheuristic,param,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,flexibility_conformations,flexibility_params,vectors_mejora.nlig,param.flex_angle);
				break;
			}
		}		
		//printf("Voy a calcular la energia...\n");
		//ENERGIA
		
		ForcesCpuSolver_vs(nlinks,vectors_mejora.nlig,vectors_mejora.files,param,proteina,ligand_params,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,ligtype,ql,bonds,vectors_mejora.energy.energy,vectors_mejora.energy.n_conformation,vectors_mejora.weights,vectors_mejora.f_params,vectors_mejora.nconformations,metaheuristic);
		energy_internal_conformations_cpu_vs (vectors_mejora.nconformations,vectors_mejora.nlig,vectors_mejora.files,param,metaheuristic,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,ligand_params,flexibility_params,vectors_mejora.weights,vectors_mejora.f_params,internal_energy,ql,ligtype);
		energy_total (vectors_mejora.nconformations,param,metaheuristic,vectors_mejora.energy.energy,internal_energy);
		
		if (steps >=simulations)
		{
			//FLEXIBILIDAD			
			update_angulations_conformations_cpp (vectors_mejora.nconformations,metaheuristic,param,flexibility_conformations,vectors_mejora.energy.energy,energy);
			flexibility_cpp_vs (vectors_mejora.nconformations,vectors_mejora.files,metaheuristic,param,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,flexibility_conformations,flexibility_params,vectors_mejora.nlig,param.flex_angle);
			incluir_mejorar_inicializar_flex (vectors_mejora.nlig,vectors_mejora,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,files,nconformations,metaheuristic,param);
		}
		else
		{
			switch (option)
			{
			case 0:		
				updateMove_vs (vectors_mejora.nconformations,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,move_x,move_y,move_z,vectors_mejora.energy.energy,energy,param,tam,metaheuristic);
				break;
			case 1:
				updateRotation_vs(vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,quat_x,quat_y,quat_z,quat_w,vectors_mejora.energy.energy,energy,param,vectors_mejora.nconformations,metaheuristic);
				break;
			case 2:
				update_angulations_conformations_cpp (vectors_mejora.nconformations,metaheuristic,param,flexibility_conformations,vectors_mejora.energy.energy,energy);
				flexibility_cpp_vs (vectors_mejora.nconformations,vectors_mejora.files,metaheuristic,param,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,flexibility_conformations,flexibility_params,vectors_mejora.nlig,param.flex_angle);
				incluir_mejorar_inicializar_flex (vectors_mejora.nlig,vectors_mejora,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,files,nconformations,metaheuristic,param);
				break;
			}
		}
		steps++;
	}
	free(vectors_mejora.conformations_x);
        free(vectors_mejora.conformations_y);
        free(vectors_mejora.conformations_z);
        free(internal_energy);
        free(flexibility_conformations);
	free(vectors_mejora.move_x);
	free(vectors_mejora.move_y);
	free(vectors_mejora.move_z);
	free(vectors_mejora.quat_x);
	free(vectors_mejora.quat_y);
	free(vectors_mejora.quat_z);
	free(vectors_mejora.quat_w);
	free(vectors_mejora.energy.energy);
	free(vectors_mejora.energy.n_conformation);
	free(vectors_mejora.weights);
	free(nlinks);
	//printf("End Mont\n");
	//exit(0);
}
