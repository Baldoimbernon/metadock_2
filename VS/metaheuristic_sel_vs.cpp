#include "energy_common_vs.h"
#include "energy_common.h"
#include <thrust/sequence.h>
#include <thrust/sort.h>
#include <thrust/functional.h>
#include <omp.h>

extern void seleccionar_vs (unsigned int nlig, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param)
{
	unsigned int stride_s, stride_e;
	unsigned int th1;
	unsigned int total_selected = metaheuristic.NEMSel_selec + metaheuristic.NEPSel_selec;
	//printf("metaheuristic.NEFIni = %d\n",metaheuristic.NEFIni);
	stride_s = metaheuristic.NEMSel_selec;
	stride_e = metaheuristic.NEFIni - metaheuristic.NEPSel_selec;
	//printf("combined_init %d, combined_end %d, elements_copia_end %d\n",combined_init,combined_end,elements_copia_end);

	vectors_s->files = vectors_e->files;
	vectors_s->nlig = vectors_e->nlig;
	vectors_s->nconformations = vectors_e->files * total_selected;	
	
	vectors_s->conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nlig*vectors_s->nconformations);
	vectors_s->conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nlig*vectors_s->nconformations);
	vectors_s->conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nlig*vectors_s->nconformations);
	
	vectors_s->ql = (type_data *)malloc(sizeof(type_data)*vectors_s->nlig*vectors_s->nconformations);
	vectors_s->ligtype = (char *)malloc(sizeof(char)*vectors_s->nlig*vectors_s->nconformations);
	vectors_s->subtype = (char *)malloc(sizeof(char)*vectors_s->nlig*SUBTYPEMAXLEN*vectors_s->nconformations);
	vectors_s->bonds = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nlig*MAXBOND*vectors_s->nconformations);
	vectors_s->nbonds = (char *)malloc(sizeof(char)*vectors_s->nlig*vectors_s->nconformations);
	
	vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nconformations);
	vectors_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->weights = (type_data *)malloc(SCORING_TERMS * sizeof(type_data));
    memcpy(vectors_s->f_params, vectors_e->f_params,MAXTYPES * sizeof(struct force_field_param_t));
    memcpy(vectors_s->weights, vectors_e->weights,SCORING_TERMS * sizeof(type_data));
	//printf("Total_selected: %d, NEMSel_selec: %d, NEPSel_selec: %d NEFIni %d \n",total_selected,metaheuristic.NEMSel_selec,metaheuristic.NEPSel_selec,metaheuristic.NEFIni);	
	//exit(0);

	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 0:
		case 2:
		case 3:
			th1 = metaheuristic.Threads1Sel;
			break;
	}		
	omp_set_num_threads (th1);
	#pragma omp parallel for 	
	for (unsigned int i = 0;i < vectors_s->files;i++)
	{	
		memcpy(vectors_s->conformations_x + i*vectors_s->nlig*total_selected, vectors_e->conformations_x + i*nlig*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * vectors_s->nlig * sizeof(type_data));  	
		memcpy(vectors_s->conformations_y + i*vectors_s->nlig*total_selected, vectors_e->conformations_y + i*nlig*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * vectors_s->nlig * sizeof(type_data)); 
		memcpy(vectors_s->conformations_z + i*vectors_s->nlig*total_selected, vectors_e->conformations_z + i*nlig*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * vectors_s->nlig * sizeof(type_data));

		memcpy(vectors_s->ql + i*vectors_s->nlig*total_selected, vectors_e->ql + i*vectors_s->nlig*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * vectors_s->nlig * sizeof(type_data));
		memcpy(vectors_s->ligtype + i*vectors_s->nlig*total_selected, vectors_e->ligtype + i*vectors_s->nlig*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * vectors_s->nlig * sizeof(char));
		memcpy(vectors_s->subtype + i*vectors_s->nlig*SUBTYPEMAXLEN*total_selected, vectors_e->subtype + i*vectors_s->nlig*SUBTYPEMAXLEN*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * vectors_s->nlig * SUBTYPEMAXLEN * sizeof(char));
		memcpy(vectors_s->bonds + i*vectors_s->nlig*MAXBOND*total_selected, vectors_e->bonds + i*vectors_s->nlig*MAXBOND*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * vectors_s->nlig * MAXBOND * sizeof(unsigned int));
		memcpy(vectors_s->nbonds + i*vectors_s->nlig*total_selected, vectors_e->nbonds + i*vectors_s->nlig*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * vectors_s->nlig * sizeof(char));  
					
		memcpy(vectors_s->conformations_x + i*vectors_s->nlig*total_selected + stride_s*vectors_s->nlig, vectors_e->conformations_x + i*vectors_s->nlig*metaheuristic.NEFIni + stride_e*vectors_s->nlig, metaheuristic.NEPSel_selec * vectors_s->nlig * sizeof(type_data));  	
		memcpy(vectors_s->conformations_y + i*vectors_s->nlig*total_selected + stride_s*vectors_s->nlig, vectors_e->conformations_y + i*vectors_s->nlig*metaheuristic.NEFIni + stride_e*vectors_s->nlig, metaheuristic.NEPSel_selec * vectors_s->nlig * sizeof(type_data)); 
		memcpy(vectors_s->conformations_z + i*vectors_s->nlig*total_selected + stride_s*vectors_s->nlig, vectors_e->conformations_z + i*vectors_s->nlig*metaheuristic.NEFIni + stride_e*vectors_s->nlig, metaheuristic.NEPSel_selec * vectors_s->nlig * sizeof(type_data));
				
		memcpy(vectors_s->ql + i*vectors_s->nlig*total_selected + stride_s*vectors_s->nlig, vectors_e->ql + i*vectors_s->nlig*metaheuristic.NEFIni + stride_e*vectors_s->nlig, metaheuristic.NEPSel_selec * vectors_s->nlig * sizeof(type_data));
		memcpy(vectors_s->ligtype + i*vectors_s->nlig*total_selected + stride_s*vectors_s->nlig, vectors_e->ligtype + i*vectors_s->nlig*metaheuristic.NEFIni + stride_e*vectors_s->nlig, metaheuristic.NEPSel_selec * vectors_s->nlig * sizeof(char));
		memcpy(vectors_s->subtype + i*vectors_s->nlig*SUBTYPEMAXLEN*total_selected + stride_s*vectors_s->nlig*SUBTYPEMAXLEN, vectors_e->subtype + i*vectors_s->nlig*SUBTYPEMAXLEN*metaheuristic.NEFIni + stride_e*vectors_s->nlig*SUBTYPEMAXLEN, metaheuristic.NEPSel_selec * vectors_s->nlig * SUBTYPEMAXLEN * sizeof(char));
		memcpy(vectors_s->bonds + i*vectors_s->nlig*MAXBOND*total_selected  + stride_s*vectors_s->nlig*MAXBOND, vectors_e->bonds + i*vectors_s->nlig*MAXBOND*metaheuristic.NEFIni + stride_e*vectors_s->nlig*MAXBOND, metaheuristic.NEPSel_selec * vectors_s->nlig * MAXBOND * sizeof(unsigned int));
		memcpy(vectors_s->nbonds + i*vectors_s->nlig*total_selected + stride_s*vectors_s->nlig, vectors_e->nbonds + i*vectors_s->nlig*metaheuristic.NEFIni + stride_e*vectors_s->nlig, metaheuristic.NEPSel_selec * vectors_s->nlig * sizeof(char));  

		memcpy(vectors_s->move_x + i*total_selected, vectors_e->move_x + i*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * sizeof(type_data));  	
		memcpy(vectors_s->move_y + i*total_selected, vectors_e->move_y + i*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * sizeof(type_data)); 
		memcpy(vectors_s->move_z + i*total_selected, vectors_e->move_z + i*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * sizeof(type_data));
		memcpy(vectors_s->quat_x + i*total_selected, vectors_e->quat_x + i*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * sizeof(type_data));
		memcpy(vectors_s->quat_y + i*total_selected, vectors_e->quat_y + i*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * sizeof(type_data));
		memcpy(vectors_s->quat_z + i*total_selected, vectors_e->quat_z + i*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * sizeof(type_data));
		memcpy(vectors_s->quat_w + i*total_selected, vectors_e->quat_w + i*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * sizeof(type_data));
		memcpy(vectors_s->energy.energy + i*total_selected, vectors_e->energy.energy + i*metaheuristic.NEFIni, metaheuristic.NEMSel_selec * sizeof(type_data));										
					
		memcpy(vectors_s->move_x + i*total_selected + stride_s, vectors_e->move_x + i*metaheuristic.NEFIni + stride_e, metaheuristic.NEPSel_selec * sizeof(type_data));  	
		memcpy(vectors_s->move_y + i*total_selected + stride_s, vectors_e->move_y + i*metaheuristic.NEFIni + stride_e, metaheuristic.NEPSel_selec * sizeof(type_data)); 
		memcpy(vectors_s->move_z + i*total_selected + stride_s, vectors_e->move_z + i*metaheuristic.NEFIni + stride_e, metaheuristic.NEPSel_selec * sizeof(type_data));
		memcpy(vectors_s->quat_x + i*total_selected + stride_s, vectors_e->quat_x + i*metaheuristic.NEFIni + stride_e, metaheuristic.NEPSel_selec * sizeof(type_data));
		memcpy(vectors_s->quat_y + i*total_selected + stride_s, vectors_e->quat_y + i*metaheuristic.NEFIni + stride_e, metaheuristic.NEPSel_selec * sizeof(type_data));
		memcpy(vectors_s->quat_z + i*total_selected + stride_s, vectors_e->quat_z + i*metaheuristic.NEFIni + stride_e, metaheuristic.NEPSel_selec * sizeof(type_data));
		memcpy(vectors_s->quat_w + i*total_selected + stride_s, vectors_e->quat_w + i*metaheuristic.NEFIni + stride_e, metaheuristic.NEPSel_selec * sizeof(type_data));
		memcpy(vectors_s->energy.energy + i*total_selected + stride_s, vectors_e->energy.energy + i*metaheuristic.NEFIni + stride_e, metaheuristic.NEPSel_selec * sizeof(type_data));	
	}
	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);	
	//for(int k=0;k<128;k++)
	//	printf("e-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",k,vectors_e->conformations_x[k],vectors_e->conformations_y[k],vectors_e->conformations_z[k],vectors_e->move_x[k/nlig],vectors_e->move_y[k/nlig],vectors_e->move_z[k/nlig],vectors_e->energy.energy[k/nlig]);
	//printf("***\n");
	//for(int k=0;k<vectors_s->nconformations*vectors_s->nlig;k++)
	//	printf("s-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",k/nlig,vectors_s->conformations_x[k],vectors_s->conformations_y[k],vectors_s->conformations_z[k],vectors_s->move_x[k/nlig],vectors_s->move_y[k/nlig],vectors_s->move_z[k/nlig],vectors_s->energy.energy[k/nlig]);	
	//exit(0);
}


