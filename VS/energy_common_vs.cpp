#include <omp.h>
#include "energy_common.h"
#include "energy_common_vs.h"
#include "energy_struct.h"
#include "definitions.h"

using namespace std;

extern void memcpy_type_data (type_data *out, type_data *in, unsigned int n)
{
	for (unsigned int i=0;i < n; i++)
		out[i] = in[i];
}
extern void readLigand_vs (char *filename, char **type_json, unsigned int nlig, type_data *c_x, type_data *c_y, type_data *c_z, char *ligtype, char *subtype, char *nbonds, unsigned int *bonds, type_data *ql, int scoring_type){

	unsigned int nAtom, nBond, aux,ind1, ind2,dum1,dum2,dum3;
     	type_data com[3];
	//Auxiliar variables to read useless information
	char cadena[20],cadena1[20],auxtipo[10];
	unsigned int * enlaces_nit;
	
	/*ifstream file (filename, ifstream::in );

	if (!file.good()) {
		cout << "can't open file " << filename << endl;
		cout << "Will not read ligand file" << endl;
		exit(1);
	}*/
	FILE *fp;

        if ((fp = fopen(filename,"r")) == NULL) {
                printf("ReadInput(): Can't open file \"%s\"\n", filename);
                exit(1);
        }

	if (skip_to_token(fp,"@<TRIPOS>MOLECULE")) {
                exit(1);
                return;
        }
        fscanf(fp,"%s", cadena);
        fscanf(fp,"%u %u", &nAtom, &nBond);

	//file >> cadena;
	//file >> cadena;
	//Read number of atoms
	//file >> nAtom >> nBond;
	//file >>  aux >> aux >> aux;
	//file >> cadena;
	//file >> cadena;
	//file >> cadena;
	type_json = (char **)malloc(nAtom*sizeof(char *));
        for (int i = 0; i < nAtom; i++)
                type_json[i] = (char *)malloc(11*sizeof(char));
	
	enlaces_nit = (unsigned int*) calloc (nAtom, sizeof(unsigned int));

	 if (skip_to_token(fp,"@<TRIPOS>ATOM")) {
                exit(1);
                return;
        }
	
	for (unsigned int i=0; i < nAtom; i++){
		//Read type of atom
		fscanf(fp, "%u %s %f %f %f %s %u %s %f",&aux,&auxtipo,&c_x[i],&c_y[i],&c_z[i],&subtype[SUBTYPEMAXLEN*i],cadena,cadena1,&ql[i]);
		strcpy(type_json[i],auxtipo);
		//printf("c_x[i] %f c_y[i] %f c_z[i] %f\n",c_x[i],c_y[i],c_z[i]);
		//file >> aux >> auxtipo;
		//Read 3D position
		//file >> c_x[i] >> c_y[i] >> c_z[i];
		//file >> &(subtype[SUBTYPEMAXLEN*i]) >> cadena >> cadena;
		//Read charge
		//file >> ql[i];
		if (scoring_type != S_AD4)
		{
			c_x[i] /= 10.0;
			c_y[i] /= 10.0;
			c_z[i] /= 10.0;
		}
		ligtype[i] = getTypeNumber(auxtipo);
		//ligando->ligtype[i] = getTypeNumber(auxtipo);	

	}	
	//file.close();
	//Bif (scoring_type != S_AD4)
        //B{
		getCOM (c_x,c_y,c_z, nAtom, com);

		for (unsigned int i=0; i < nAtom; i++){
			c_x[i] -= com[0];
			c_y[i] -= com[1];
			c_z[i] -= com[2];
		}
	//B}
	
	//file >> cadena;
	if (skip_to_token(fp,"@<TRIPOS>BOND")) {
                exit(1);
                return;
        }	
	for (unsigned int i=0; i < nBond; i++){

          //file >> aux >> ind1 >> ind2 >> cadena;
          fscanf(fp, "%u %u %u %s",&aux, &ind1, &ind2,cadena);

          if (skip_to_eol(fp)) {
               return;
          }

          ind1--;
          ind2--;

          bonds[MAXBOND*ind1 + nbonds[ind1]] = ind2;
          bonds[MAXBOND*ind2 + nbonds[ind2]] = ind1;
          nbonds[ind1]++;
          nbonds[ind2]++;

		//Calcula el número de enlaces de cada nitrógeno
		if (ligtype[ind1] == NIT && isdigit(cadena[0])){
			enlaces_nit[ind1] += atoi(cadena);
		}

		if (ligtype[ind2] == NIT && isdigit(cadena[0])){
			enlaces_nit[ind2] += atoi(cadena);
		}
	

     }
	
	fclose(fp);
	for (unsigned int i=0; i < nAtom; i++){
		if(ligtype[i] == NIT && enlaces_nit[i] < 4)
			ligtype[i] = NIT2;	
	}
	
	for (unsigned int i=0; i < nAtom; i++){
		unsigned int tipo_enlace = ligtype[bonds[MAXBOND*i]];
		if (ligtype[i] == HID && (tipo_enlace == OXI || tipo_enlace == NIT2 || tipo_enlace == AZU))
				ligtype[i] = HBOND;
	}
	for (unsigned int i=nAtom ; i < nlig ; i++){
		ql[i] = 0.0;
		ligtype[i] = 18;
		c_x[i]  = 0.0;
		c_y[i]= 0.0;
		c_z[i]= 0.0;
	}

	
}

extern void readConfigFile_vs(char *file, struct param_t &Param, struct metaheuristic_t &Metaheuristic, struct vectors_t *vectors)
{
	FILE *fp;
	char  *colp, *valp, txtline[FILENAME], stmp[FILENAME];
	unsigned int   colon = ':';
	unsigned int   itmp;
	double ftmp, ftmp1, ftmp2, ftmp3;
	int l;

	if ((fp = fopen(file,"r")) == NULL) {
		printf("ReadInput(): No se puede abrir el fichero \"%s\"\n", file);
		exit(1);
	}
	printf("\n\t\tCONFIG FILE: %s \n",file);
	printf("\n");
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	/////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.mode = atoi(stmp);
	printf("Modo:  %d\n",Param.mode);	//Modo
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.tipo_computo = atoi(stmp);
	printf("Tipo de Computo:  %d ",Param.tipo_computo);	//Tipo de computo
	if (Param.tipo_computo == 0) printf("HOMOGENEO\n"); else printf("HETEROGENEO\n"); 
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.ngpu = atoi(stmp);
	printf("Identificador de dispositivo:  %d\n",Param.ngpu);	//Modo
	///////////////////////////////////////////////////////////		
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	///////////////////////////////////////////////////////////		
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.optimizacion = atoi(stmp);
	printf("Fase de Warm_UP:  %d\n",Param.optimizacion);	//Fase de warm up
	///////////////////////////////////////////////////////////		
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.reparto_carga = atoi(stmp);
	printf("Fase de reparto de carga optimo:  %d\n",Param.reparto_carga);	//Fase de warm up
	///////////////////////////////////////////////////////////		
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.conf_warm_up_gpu = atoi(stmp);
	printf("Conformaciones GPU(warm_up):  %d \n",Param.conf_warm_up_gpu);	//Conformaciones para entrenamiento	GPU
	///////////////////////////////////////////////////////////		
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.conf_warm_up_cpu = atoi(stmp);
	printf("Conformaciones Multicore CPU(warm_up):  %d \n",Param.conf_warm_up_cpu);	//Conformaciones para entrenamiento	CPU
	///////////////////////////////////////////////////////////		
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.por_conf_warm_up_sel = atoi(stmp);
	printf("Porcentaje de Conformaciones Multicore para Seleccion(warm_up):  %d \n",Param.por_conf_warm_up_sel);	//Porcentaje Conformaciones entrenamiento fase seleccion
	///////////////////////////////////////////////////////////		
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.por_conf_warm_up_inc = atoi(stmp);
	printf("Porcentaje de Conformaciones Multicore para Incluir(warm_up):  %d \n",Param.por_conf_warm_up_inc);	//Porcentaje Conformaciones para entrenamiento fase Incluir
	///////////////////////////////////////////////////////////		
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.limit = atoi(stmp);
	printf("Limite tamaño de bloque (warm_up):  %d \n",Param.limit);	//Limite tamaño de bloque (warm_up)
	///////////////////////////////////////////////////////////			
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.steps_warm_up_gpu = atoi(stmp);
	printf("Pasadas optimizacion tamaño de bloque (warm_up):  %d \n",Param.steps_warm_up_gpu);	//Pasadas optimizacion tamaño de bloque SIN Shared (warm_up)
	/////////////////////////////////////////////////////	
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	strcpy(Param.openmp, ".");
	strcat(Param.openmp, route_bar);
	strcat(Param.openmp, stmp);	
	printf("Fichero configuracion OPEN-MP:  %s\n",Param.openmp);	// Configuracion openmp
	/////////////////////////////////////////////////////	
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	strcpy(Param.gpu, ".");
	strcat(Param.gpu, route_bar);
	strcat(Param.gpu, stmp);	
	printf("Fichero configuracion GPU:  %s\n",Param.gpu);	// Configuracion gpu
	///////////////////////////////////////////////////////////	
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	strcpy(Param.multigpu, ".");
	strcat(Param.multigpu, route_bar);
	strcat(Param.multigpu, stmp);	
	printf("Fichero configuracion multiGPU:  %s\n",Param.multigpu);	// Configuracion multigpu
	///////////////////////////////////////////////////////////			
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	///////////////////////////////////////////////////////////		
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.alarma = atoi(stmp);
	printf("Alarma activada: ",Param.alarma);	//Alarma
	if (Param.alarma) printf("¡¡¡SI!!!\n"); else printf("NO\n");
	///////////////////////////////////////////////////////////		
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.duracion_alarma = atoi(stmp);
	printf("Duración de la alarma: %d minutos)\n",Param.duracion_alarma);	//Duracion Alarma
	///////////////////////////////////////////////////////////
	/*if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	/////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	//strcpy(Param.vdw_p, Param.input_dir_p);
	//strcat(Param.vdw_p, route_bar);
	strcpy(Param.forcefield, stmp);
	printf("Force field Params:  %s\n",Param.forcefield);	// force_field*/
	/////////////////////////////////////////////////////
	 if (fgets(txtline, FILENAME, fp) == NULL) {
                printf("ReadInput: Input error in line:\n");
                printf("%s\n",txtline);
                exit(1);
        }
        /////////////////////////////////////////////////////////
        if (fgets(txtline, FILENAME, fp) == NULL) {
                printf("ReadInput: Input error in line:\n");
                printf("%s\n",txtline);
                exit(1);
        }
        extraction_txt(txtline,stmp);
        Param.rot_type = atoi(stmp);
	printf("Modo obtencion enlaces rotables: %d \n",Param.rot_type);
	/////////////////////////////////////////////////////
        if (fgets(txtline, FILENAME, fp) == NULL) {
                printf("ReadInput: Input error in line:\n");
                printf("%s\n",txtline);
                exit(1);
        }
        /////////////////////////////////////////////////////////
        if (fgets(txtline, FILENAME, fp) == NULL) {
                printf("ReadInput: Input error in line:\n");
                printf("%s\n",txtline);
                exit(1);
        }
        extraction_txt(txtline,stmp);
        Param.scoring_function_type = atoi(stmp);
        switch (Param.scoring_function_type) {
                case 0:
                        printf("Función de scoring: %d (ES+VDW+HBonds)\n",Param.scoring_function_type);   //Tipo 0
                        break;
                case 1:
                        printf("Función de scoring: %d (ES+VDW+HBonds+Desolv)\n",Param.scoring_function_type);   //Tipo 1
                        break;
                case 2:
                        printf("Función de scoring: %d (Autodock 4 scoring function)\n",Param.scoring_function_type);   //Tipo 2
                        break;
                default:
                        printf("Función de scoring: %d. ERROR\n");
                        break;
        }
	/////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.level_bonds_VDW = atoi(stmp);
	printf("Niveles para evitar el cálculo de la Energía Interna en VDW:  %d\n ",Param.level_bonds_VDW);	// Levels avoid VDW
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);	
	Param.level_bonds_ES = atoi(stmp);
	printf("Niveles para evitar el cálculo de la Energía Interna en ES:  %d\n ",Param.level_bonds_ES);	// Levels avoid ES
	/////////////////////////////////////////////////////
		if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);	
	Param.flex_angle = atoi(stmp);
	printf("Angulo máximo de flexibilidad:  %d\n ",Param.flex_angle);	// Levels avoid ES
	/////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.automatic_seed = atoi(stmp);
	printf("Semilla Automatica:  %d ",Param.automatic_seed);	// Levels avoid ES
	if (Param.automatic_seed) printf("¡SEMILLA AUTOMATICA!\n"); else printf("NO\n");
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.seed = atoi(stmp);
	printf("Semilla:  %d\n",Param.seed);	// Semilla manual
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.max_desp = atof(stmp);
	printf("Shift:  %f\n",Param.max_desp);	// max desp
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.rotation = atoi(stmp);
	printf("Rotation angle:  %d\n",Param.rotation);	// Rotation angle
	/////////////////////////////////////////////////////
	 if (fgets(txtline, FILENAME, fp) == NULL) {
                printf("ReadInput: Input error in line:\n");
                printf("%s\n",txtline);
                exit(1);
        }
        extraction_txt(txtline,stmp);
        Param.rotation_mu = atoi(stmp);
        printf("Rotation angle Mutation:  %d\n",Param.rotation_mu); // Rotation angle mutation
	/////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.montecarlo = atoi(stmp);
	printf("Tipo de Mejora:  %d ",Param.montecarlo);       // Mejora montecarlo
        if (Param.montecarlo) printf("MONTECARLO\n"); else printf("LOCAL SEARCH\n");
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.NEEImp = atoi(stmp);
	printf("NEEImp:  %d\n",Metaheuristic.NEEImp);	// Numero de puntos en el entorno en mejora
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.IMEFlex = atoi(stmp);
	printf("IMEFlex:  %d\n",Metaheuristic.IMEFlex);	// Intensificacion para flexibilidad
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.NEIIni = atoi(stmp);
	printf("NEIIni:  %d\n",Metaheuristic.NEIIni);	// numero de elementos iniciales
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.PEMIni = atoi(stmp);
	printf("PEMIni:  %d\n",Metaheuristic.PEMIni);	// porcentaje de mejora
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.IMEIni = atoi(stmp);
	printf("IMEIni:  %d\n",Metaheuristic.IMEIni);	//Intensificacion de mejora
	Metaheuristic.NEIIni_selec = (int)ceil((Metaheuristic.NEIIni * Metaheuristic.PEMIni) / 100);
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.NEFMIni = atoi(stmp);
	printf("NEFMIni:  %d\n",Metaheuristic.NEFMIni);	// PORCENTAJE de elementos MEJORES finales
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.NEFPIni = atoi(stmp);
	printf("NEFPIni:  %d\n",Metaheuristic.NEFPIni);	// PORCENTAJE de elementos PEORES finales
	Metaheuristic.NEFMIni_selec = (int)ceil((Metaheuristic.NEIIni * Metaheuristic.NEFMIni) / 100);
	Metaheuristic.NEFPIni_selec = (int)ceil((Metaheuristic.NEIIni * Metaheuristic.NEFPIni) / 100);	
	Metaheuristic.NEFIni = Metaheuristic.NEFMIni_selec + Metaheuristic.NEFPIni_selec;
	if (Metaheuristic.NEFIni > Metaheuristic.NEIIni) {l=0; while (Metaheuristic.NEFIni > Metaheuristic.NEIIni) { if (l%2==0) Metaheuristic.NEFPIni_selec--; else Metaheuristic.NEFMIni_selec--;l++;}}
	Metaheuristic.NEFIni = Metaheuristic.NEFMIni_selec + Metaheuristic.NEFPIni_selec;
	//printf("%d %d\n",Metaheuristic.NEFMIni_selec, Metaheuristic.NEFPIni_selec);	
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.NEMSel = atoi(stmp);
	printf("NEMSel:  %d\n",Metaheuristic.NEMSel);	// NEMSel
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.NEPSel = atoi(stmp);
	printf("NEPSel:  %d\n",Metaheuristic.NEPSel);	// NEMPel	
	Metaheuristic.NEMSel_selec = (unsigned int)ceil(((Metaheuristic.NEFMIni_selec + Metaheuristic.NEFPIni_selec) * Metaheuristic.NEMSel) / 100);
	Metaheuristic.NEPSel_selec = (unsigned int)ceil(((Metaheuristic.NEFMIni_selec + Metaheuristic.NEFPIni_selec) * Metaheuristic.NEPSel) / 100);
	//printf("%d %d\n",Metaheuristic.NEMSel_selec, Metaheuristic.NEPSel_selec);
	 if ((Metaheuristic.NEMSel_selec + Metaheuristic.NEPSel_selec) > Metaheuristic.NEFIni) {l=0; while ((Metaheuristic.NEMSel_selec + Metaheuristic.NEPSel_selec) > Metaheuristic.NEFIni) { if (l%2==0) Metaheuristic.NEPSel_selec--; else Metaheuristic.NEMSel_selec--;l++;}}
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.NMMCom = atoi(stmp);
	printf("NMMCom:  %d\n",Metaheuristic.NMMCom);	// PORCENTAJE de elementos a combinar MEJORES con MEJORES
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.NMPCom = atoi(stmp);
	printf("NMPCom:  %d\n",Metaheuristic.NMPCom);	// PORCENTAJE de elementos a combinar MEJORES con PEORES
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.NPPCom = atoi(stmp);
	printf("NPPCom:  %d\n",Metaheuristic.NPPCom);	// PORCENTAJE de elementos a combinar PEORES con PEORES
	Metaheuristic.NMMCom_selec = (unsigned int)floor((Metaheuristic.NEMSel_selec  * Metaheuristic.NMMCom) / 100);
	Metaheuristic.NMPCom_selec = (unsigned int)floor(((Metaheuristic.NEMSel_selec + Metaheuristic.NEPSel_selec) * Metaheuristic.NMPCom) / 100);
	Metaheuristic.NPPCom_selec = (unsigned int)floor((Metaheuristic.NEPSel_selec  * Metaheuristic.NPPCom) / 100);
	Metaheuristic.NCOMtotal = (Metaheuristic.NMMCom_selec * (Metaheuristic.NMMCom_selec - 1)) + (Metaheuristic.NMPCom_selec * (Metaheuristic.NMPCom_selec - 1)) + (Metaheuristic.NPPCom_selec * (Metaheuristic.NPPCom_selec - 1));
	/////////////////////////////////////////////////////
	 if (fgets(txtline, FILENAME, fp) == NULL) {
                printf("ReadInput: Input error in line:\n");
                printf("%s\n",txtline);
                exit(1);
        }
        extraction_txt(txtline,stmp);
        Metaheuristic.PEMUCom = atoi(stmp);
        printf("PEMUCom:  %d\n",Metaheuristic.PEMUCom);   // PORCENTAJE de elementos a mutar
	/////////////////////////////////////////////////////
	 if (fgets(txtline, FILENAME, fp) == NULL) {
                printf("ReadInput: Input error in line:\n");
                printf("%s\n",txtline);
                exit(1);
        }
        extraction_txt(txtline,stmp);
        Metaheuristic.IMEMUCom = atoi(stmp);
        printf("IMEMUCom:  %d\n",Metaheuristic.IMEMUCom);   // Intensificación de los elementos a mutar	
	/////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.PEMSel = atoi(stmp);
	printf("PEMSel:  %d\n",Metaheuristic.PEMSel);	// porcentaje de elementos mejores para mejorar
	Metaheuristic.PEMSel_selec = (unsigned int)ceil((Metaheuristic.NCOMtotal * Metaheuristic.PEMSel) / 100);	
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.IMEImp = atoi(stmp);
	printf("IMEImp:  %d\n",Metaheuristic.IMEImp);	// Intensificacion de la mejora
	/////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);	
	Metaheuristic.NEMInc = atoi(stmp);
	Metaheuristic.NEPInc = 100 - Metaheuristic.NEMInc;
	printf("NEMInc:  %d\n",Metaheuristic.NEMInc);	// Mejores a incluir
	
	/////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	Metaheuristic.NEMInc_selec = (unsigned int)floor((Metaheuristic.NEFIni  * Metaheuristic.NEMInc) / 100);
	Metaheuristic.NEPInc_selec = (unsigned int)floor((Metaheuristic.NEFIni  * Metaheuristic.NEPInc) / 100);
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.NIRFin = atoi(stmp);
	printf("NIRFin:  %d\n",Metaheuristic.NIRFin);	// Pasadas sin mejora
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.NMIFin = atoi(stmp);
	printf("NMIFin:  %d\n",Metaheuristic.NMIFin);	// Pasadas máximas
	/////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	if ((Param.mode == 0) || (Param.mode == 3))
	{
			Metaheuristic.num_kernels = 7;
  			//////////////////////////////////////////////////////
                        if (fgets(txtline, FILENAME, fp) == NULL) {
                                printf("ReadInput: Input error in line:\n");
                                printf("%s\n",txtline);
                                exit(1);
                        }
                        extraction_txt(txtline,stmp);
                        Param.auto_max_threads = atoi(stmp);
                        if (Param.auto_max_threads == 1)
                                printf("Automatic threads:  %d AUTOMATIC\n",Param.auto_max_threads);    // Configuracion automatica threads
                        else
                                printf("Automatic threads:  %d MANUAL\n",Param.auto_max_threads);       // Configuracion automatica threads

			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads1Ini = atoi(stmp);
			printf("Hilos primer nivel Inicializar:  %d\n",Metaheuristic.Threads1Ini);	// Hilos primer nivel Inicializar
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads1Sel = atoi(stmp);
			printf("Hilos primer nivel Seleccionar:  %d\n",Metaheuristic.Threads1Sel);	// Hilos primer nivel Seleccionar
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.ThreadsperblockFit = atoi(stmp);
			printf("Numero de Hilos por bloque función Cálculo de FITNESS:  %d\n",Metaheuristic.ThreadsperblockFit);	//Numero de Hilos por bloque función FITNESS			
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.ThreadsperblockMoveIni = atoi(stmp);
			printf("Numero de Hilos por bloque función MOVE Inicializar:  %d\n",Metaheuristic.ThreadsperblockMoveIni);	//Numero de Hilos por bloque funcion MOVE Ini
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.ThreadsperblockRotIni = atoi(stmp);
			printf("Numero de Hilos por bloque función ROTACION Inicializar:  %d\n",Metaheuristic.ThreadsperblockRotIni);	//Numero de Hilos por bloque funcion ROTACION Ini
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.ThreadsperblockQuatIni = atoi(stmp);
			printf("Numero de Hilos por bloque función QUAT Inicializar:  %d\n",Metaheuristic.ThreadsperblockQuatIni);	//Numero de Hilos por bloque funcion QUAT Ini
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.ThreadsperblockRandomIni = atoi(stmp);
			printf("Numero de Hilos por bloque función CURAND Inicializar:  %d\n",Metaheuristic.ThreadsperblockRandomIni);	//Numero de Hilos por bloque funcion CURAND Ini
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads1Com = atoi(stmp);
			printf("Hilos primer nivel Combinar:  %d\n",Metaheuristic.Threads1Com);	// Hilos primer nivel Combinar
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads2Com = atoi(stmp);
			printf("Hilos segundo nivel Combinar:  %d\n",Metaheuristic.Threads1Com);	// Hilos segundo nivel Combinar
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.ThreadsperblockMoveImp = atoi(stmp);
			printf("Numero de Hilos por bloque función MOVE mejorar:  %d\n",Metaheuristic.ThreadsperblockMoveImp);	//Numero de Hilos por bloque funcion MOVE mejorar
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.ThreadsperblockIncImp = atoi(stmp);
			printf("Numero de Hilos por bloque función INCLUIR mejorar:  %d\n",Metaheuristic.ThreadsperblockIncImp);	//Numero de Hilos por bloque funcion INCLUIR mejorar			
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads1Inc = atoi(stmp);
			printf("Hilos primer nivel incluir:  %d\n",Metaheuristic.Threads1Inc);	// Hilos primer nivel Incluir
			if (Param.auto_max_threads == 1)
                        {
                                Metaheuristic.Threads1Fit = omp_get_num_procs();
                                Metaheuristic.Threads2Fit = 1;
                                Metaheuristic.Threads1Ini = omp_get_num_procs();
                                Metaheuristic.Threads1Sel = omp_get_num_procs();
                                Metaheuristic.Threads1Com = omp_get_num_procs();
                                Metaheuristic.Threads2Com = 1;
                                Metaheuristic.Threads1Imp = omp_get_num_procs();
                                Metaheuristic.Threads1Inc = omp_get_num_procs();
                                printf("Automatic Threads first level: %d\n", Metaheuristic.Threads1Fit);

                        }
	}
	if (Param.mode == 2)
	{
			for (unsigned int i = 0; i < 14;i++)
			{
				/////////////////////////////////////////////////////
				if (fgets(txtline, FILENAME, fp) == NULL) {
					printf("ReadInput: Input error in line:\n");
					printf("%s\n",txtline);
					exit(1);
				}
			}
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Param.auto_max_threads = atoi(stmp);
			if (Param.auto_max_threads == 1)
				printf("Automatic threads:  %d AUTOMATIC\n",Param.auto_max_threads);	// Configuracion automatica threads
			else
				printf("Automatic threads:  %d MANUAL\n",Param.auto_max_threads);	// Configuracion automatica threads
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads1Ini = atoi(stmp);
			printf("Hilos primer nivel Inicializar:  %d\n",Metaheuristic.Threads1Ini);	// Hilos primer nivel Inicializar
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads1Fit = atoi(stmp);
			printf("Hilos primer nivel Calculo Fitness:  %d\n",Metaheuristic.Threads1Fit);	// Hilos segundo nivel Inicializar
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads2Fit = atoi(stmp);
			printf("Hilos segundo nivel Calculo Fitness:  %d\n",Metaheuristic.Threads2Fit);	// Hilos primer nivel Seleccionar
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads1Sel = atoi(stmp);
			printf("Hilos primer nivel Seleccionar:  %d\n",Metaheuristic.Threads1Sel);	// Hilos primer nivel Combinar
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads1Com = atoi(stmp);
			printf("Hilos primer nivel Combinar:  %d\n",Metaheuristic.Threads1Com);	// Hilos segundo nivel Combinar
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads2Com = atoi(stmp);
			printf("Hilos segundo nivel Combinar:  %d\n",Metaheuristic.Threads2Com);	// Hilos primer nivel Mejorar
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads1Imp = atoi(stmp);
			printf("Hilos primer nivel Mejorar:  %d\n",Metaheuristic.Threads1Imp);	// Hilos segundo nivel Mejorar
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads1Inc = atoi(stmp);
			printf("Hilos primer nivel incluir:  %d\n",Metaheuristic.Threads1Inc);	// Hilos primer nivel Incluir
			if (Param.auto_max_threads == 1)
			{
				Metaheuristic.Threads1Fit = omp_get_num_procs();
				Metaheuristic.Threads2Fit = 1;
				Metaheuristic.Threads1Ini = omp_get_num_procs();
				Metaheuristic.Threads1Sel = omp_get_num_procs();
				Metaheuristic.Threads1Com = omp_get_num_procs();
				Metaheuristic.Threads2Com = 1;  
				Metaheuristic.Threads1Imp = omp_get_num_procs();
				Metaheuristic.Threads1Inc = omp_get_num_procs();
				printf("Automatic Threads first level: %d\n", Metaheuristic.Threads1Fit);
			}
	}
}
