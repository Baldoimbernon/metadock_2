#include "energy_cuda.h"
#include "energy_common.h"
#include "definitions.h"

extern void dataToGPUFlexibility_Params(unsigned int files, struct ligand_params_t *ligand_params, struct flexibility_params_t *flexibility_params, struct flexibility_params_t *&flexibility_params_d)
{
	struct flexibility_params_t *flex_temp;
	cudaError_t cudaStatus;
	int *links_d, *links_fragments_d, *fragments_d, *fragments_tam_d;
	bool *individual_bonds_ES_d, *individual_bonds_VDW_d;
	
	
	unsigned int linksSize,linksfragmentsSize,fragmentstamSize,fragmentsSize,flexibilitydadaSize,boolSize;
	flexibilitydadaSize = files * sizeof(struct flexibility_params_t);	
	//cudaDeviceSynchronize();
        //cudaStatus = cudaGetLastError();
        //if (cudaStatus != cudaSuccess) printf("Error antes de CARGA DATOS FLEXIBLES %d\n",cudaStatus);

	cudaStatus = cudaMalloc((void**) &flexibility_params_d, files * sizeof(struct flexibility_params_t));
	flex_temp = (struct flexibility_params_t *)malloc(files * sizeof(struct flexibility_params_t));
	for (int i = 0; i < files; i++)
	{
		flex_temp[i].n_links = flexibility_params[i].n_links;
		flex_temp[i].n_fragments = flexibility_params[i].n_fragments;
		linksSize = flexibility_params[i].n_links * 2 * sizeof(int);
		linksfragmentsSize = flexibility_params[i].n_links * 2 * sizeof(int);
		fragmentstamSize = flexibility_params[i].n_fragments * sizeof(int);
		fragmentsSize = flexibility_params[i].n_links * (ligand_params[i].atoms - 2) * sizeof(int);
		boolSize = ligand_params[i].atoms * ligand_params[i].atoms * sizeof(bool);	
		
		cudaStatus = cudaMalloc((void**) &links_d,linksSize);
		if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc falled!");
		//cudaDeviceSynchronize();
                //cudaStatus = cudaGetLastError();
                //if (cudaStatus != cudaSuccess) printf("Error CARGA DATOS FLEXIBLES despues de malloc %d %d",i,cudaStatus);
		cudaStatus = cudaMemcpy(links_d,flexibility_params[i].links,linksSize,cudaMemcpyHostToDevice);
		flex_temp[i].links = links_d;        
		cudaMemcpy(&flexibility_params_d[i],&flex_temp[i],sizeof(struct flexibility_params_t),cudaMemcpyHostToDevice);
                
	
		cudaStatus = cudaMalloc((void**) &links_fragments_d,linksfragmentsSize);
		if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc falled!");		
		cudaStatus = cudaMemcpy(links_fragments_d, flexibility_params[i].links_fragments, linksfragmentsSize, cudaMemcpyHostToDevice);
		flex_temp[i].links_fragments = links_fragments_d;
		
		cudaStatus = cudaMalloc((void**) &fragments_d,fragmentsSize);
		if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc falled!");
		cudaStatus = cudaMemcpy(fragments_d, flexibility_params[i].fragments, fragmentsSize, cudaMemcpyHostToDevice);
		flex_temp[i].fragments = fragments_d;
		
		cudaStatus = cudaMalloc((void**) &fragments_tam_d,fragmentsSize);
		if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc falled!");
		cudaStatus = cudaMemcpy(fragments_tam_d, flexibility_params[i].fragments_tam, fragmentstamSize, cudaMemcpyHostToDevice);
		flex_temp[i].fragments_tam = fragments_tam_d;

		cudaStatus = cudaMalloc((void**) &individual_bonds_ES_d,boolSize);
		if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc falled!");
		cudaStatus = cudaMemcpy(individual_bonds_ES_d, flexibility_params[i].individual_bonds_ES, boolSize, cudaMemcpyHostToDevice);
		flex_temp[i].individual_bonds_ES = individual_bonds_ES_d;

		cudaStatus = cudaMalloc((void**) &individual_bonds_VDW_d,boolSize);
		if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc falled!");
		cudaStatus = cudaMemcpy(individual_bonds_VDW_d, flexibility_params[i].individual_bonds_VDW, boolSize, cudaMemcpyHostToDevice);
		flex_temp[i].individual_bonds_VDW = individual_bonds_VDW_d;

		cudaMemcpy(&flexibility_params_d[i],&flex_temp[i],sizeof(struct flexibility_params_t),cudaMemcpyHostToDevice);
		cudaDeviceSynchronize();
	        cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) printf("Error CARGA DATOS FLEXIBLES %d %d",i,cudaStatus);
	}
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) printf("Error CARGA DATOS FLEXIBLES\n");
}

extern void commonDataToGPU_WS_vs (unsigned int files, unsigned int nconformations, unsigned int nlig, struct receptor_t proteina, type_data *&rec_x_d, type_data *&rec_y_d, type_data *&rec_z_d, type_data *&qr_d, char *&rectype_d, unsigned int *&bondsr_d, unsigned int *atoms, unsigned int *&atoms_d, unsigned int *nlinks, unsigned int *&nlinks_d, char *ligtype, char *&ligtype_d, unsigned int *bonds, unsigned int *&bonds_d, type_data *ql, type_data *&ql_d){

	cudaError_t cudaStatus;
		
	unsigned long int recSize = proteina.nrec * sizeof(type_data);
	unsigned long int typeSize = proteina.nrec * sizeof(char);

	//guarda el ligando en device memory
	//printf("Crear memoria proteina\n");	
	//guarda la proteina y la energia en global memory
	cudaStatus = cudaMalloc((void**) &rec_x_d, recSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	cudaStatus = cudaMalloc((void**) &rec_y_d, recSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc falled!");
	cudaStatus = cudaMalloc((void**) &rec_z_d, recSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	cudaStatus = cudaMalloc((void**) &rectype_d, typeSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	cudaStatus = cudaMalloc((void**) &qr_d, recSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");	
	cudaStatus = cudaMalloc((void**) &bondsr_d, proteina.nrec * MAXBOND * sizeof(int));
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");	
	

	//Copia la proteina en la GPU
	cudaStatus = cudaMemcpy(rec_x_d, proteina.rec_x, recSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(rec_y_d, proteina.rec_y, recSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(rec_z_d, proteina.rec_z, recSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");

	cudaStatus = cudaMemcpy(rectype_d, proteina.rectype, typeSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(qr_d, proteina.qr, recSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");	
	cudaStatus = cudaMemcpy(bondsr_d, proteina.bonds, proteina.nrec * MAXBOND * sizeof(int), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");	
	
	
	cudaStatus = cudaMalloc((void**) &ligtype_d,nconformations*nlig*sizeof(char));
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc falled!");
	cudaStatus = cudaMemcpy(ligtype_d, ligtype, nconformations*nlig*sizeof(char), cudaMemcpyHostToDevice);	
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	
	cudaStatus = cudaMalloc((void**) &bonds_d,nconformations*nlig*MAXBOND*sizeof(int));
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc falled!");
	cudaStatus = cudaMemcpy(bonds_d, bonds, nconformations*nlig*MAXBOND*sizeof(int), cudaMemcpyHostToDevice);	
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");

	cudaStatus = cudaMalloc((void**) &ql_d,nconformations*nlig*sizeof(type_data));
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc falled!");
	cudaStatus = cudaMemcpy(ql_d, ql, nconformations*nlig*sizeof(type_data), cudaMemcpyHostToDevice);	
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");

	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) printf("Error en memoria\n");	
	
	//crea y guarda atoms en device memory
	cudaStatus = cudaMalloc((void**) &atoms_d,files*sizeof(int));
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc falled!");
	cudaStatus = cudaMemcpy(atoms_d, atoms, files*sizeof(int), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");

	//crea y guarda el número de enlaces en device memory
        cudaStatus = cudaMalloc((void**) &nlinks_d,files*sizeof(int));
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc falled!");
        cudaStatus = cudaMemcpy(nlinks_d, nlinks, files*sizeof(int), cudaMemcpyHostToDevice);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
}

extern void commonDataToGPU_vs (unsigned int files, unsigned int nconformations, unsigned int nlig, struct receptor_t proteina, struct receptor_t *& proteina_d, unsigned int *atoms, unsigned int *&atoms_d, char *ligtype, char *&ligtype_d, unsigned int *bonds, unsigned int *&bonds_d, type_data *ql, type_data *&ql_d){

	struct receptor_t rec_temp;
	cudaError_t cudaStatus;
		
	unsigned long int recSize = proteina.nrec * sizeof(type_data);
	unsigned long int typeSize = proteina.nrec * sizeof(char);
	//crea y guarda ligtype y bonds en device memory
	cudaStatus = cudaMalloc((void**) &ligtype_d,nconformations*nlig*sizeof(char));
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc falled!");
	cudaStatus = cudaMemcpy(ligtype_d, ligtype, nconformations*nlig*sizeof(char), cudaMemcpyHostToDevice);	
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	
	cudaStatus = cudaMalloc((void**) &bonds_d,nconformations*nlig*MAXBOND*sizeof(unsigned int));
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc falled!");
	cudaStatus = cudaMemcpy(bonds_d, bonds, nconformations*nlig*MAXBOND*sizeof(unsigned int), cudaMemcpyHostToDevice);	
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");

	cudaStatus = cudaMalloc((void**) &ql_d,nconformations*nlig*sizeof(type_data));
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc falled!");
	cudaStatus = cudaMemcpy(ql_d, ql, nconformations*nlig*sizeof(type_data), cudaMemcpyHostToDevice);	
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");

	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) printf("Error en memoria\n");	

	
	
	//crea y guarda atoms en device memory
	cudaStatus = cudaMalloc((void**) &atoms_d,files*sizeof(int));
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc falled!");
	cudaStatus = cudaMemcpy(atoms_d, atoms, files*sizeof(int), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	
	
	//printf("Crear memoria proteina\n");	
	//guarda la proteina y la energia en global memory
	rec_temp = proteina;
	cudaStatus = cudaMalloc((void**) &proteina_d, sizeof(struct receptor_t));
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	cudaStatus = cudaMalloc((void**) &rec_temp.rec_x, recSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	cudaStatus = cudaMalloc((void**) &rec_temp.rec_y, recSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc falled!");
	cudaStatus = cudaMalloc((void**) &rec_temp.rec_z, recSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	cudaStatus = cudaMalloc((void**) &rec_temp.rectype, typeSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");
	cudaStatus = cudaMalloc((void**) &rec_temp.qr, recSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");	
	cudaStatus = cudaMalloc((void**) &rec_temp.bonds, proteina.nrec * MAXBOND * sizeof(unsigned int));
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc failed!");

	//Copia la proteina en la GPU
	cudaStatus = cudaMemcpy(proteina_d, &rec_temp, sizeof(struct receptor_t), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(rec_temp.rec_x, proteina.rec_x, recSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(rec_temp.rec_y, proteina.rec_y, recSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(rec_temp.rec_z, proteina.rec_z, recSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(rec_temp.rectype, proteina.rectype, typeSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	cudaStatus = cudaMemcpy(rec_temp.qr, proteina.qr, recSize, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");	
	cudaStatus = cudaMemcpy(rec_temp.bonds, proteina.bonds, proteina.nrec * MAXBOND * sizeof(unsigned int), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy failed!");
	
}


extern void dataToGPU_vs (unsigned int N, type_data * &moves_x_d, type_data * &moves_y_d, type_data * &moves_z_d, type_data * &quat_d_x, type_data * &quat_d_y, type_data * &quat_d_z, type_data * &quat_d_w, curandState_t * &states_d){

	struct receptor_t rec_temp;
	cudaError_t cudaStatus;

	unsigned long int randSize = N * sizeof(curandState_t);	
	unsigned long int moveSize = N * sizeof(type_data);
	unsigned long int quatSize = N * sizeof(type_data);
	
	
	//reserva memoria para los movimientos
	cudaStatus = cudaMalloc((void**) &moves_x_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_y_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");
	cudaStatus = cudaMalloc((void**) &moves_z_d, moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc moves_d values failed!");

	cudaStatus = cudaMalloc((void**) &quat_d_x, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_y, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_z, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	cudaStatus = cudaMalloc((void**) &quat_d_w, quatSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc quat_d values failed!");
	
	cudaStatus = cudaMalloc((void**) &states_d, randSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc states_d values failed!");
	
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) printf("Error en memoria\n");
	//PUESTO POR MI. INICIALIZA LOS VECTORES.
	/*cudaStatus = cudaMemset(moves_x_d, 0,moveSize); 
	cudaStatus = cudaMemset(moves_y_d, 0,moveSize);
	cudaStatus = cudaMemset(moves_z_d, 0,moveSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemset moves_d failed!");
	cudaStatus = cudaMemset(quat_d_x, 0,moveSize);
	cudaStatus = cudaMemset(quat_d_y, 0,moveSize); 	
	cudaStatus = cudaMemset(quat_d_z, 0,moveSize); 
	cudaStatus = cudaMemset(quat_d_w, 0,moveSize); 
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemset quat_d failed!");*/
}
