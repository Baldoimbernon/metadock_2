#include <omp.h>
#include "wtime.h"
#include "energy_GpuSolver_vs.h"
#include "energy_kernel.h"
#include "energy_moving.h"
#include "energy_common.h"
#include "energy_common-gpu.h"
#include "energy_common-gpu_vs.h"
#include "energy_mutation_vs_flex.h"
#include "energy_rotation.h"
#include "energy_positions_vs_flex.h"
#include "energy_montecarlo_vs.h"
#include "energy_flexibility_vs.h"

__global__ void mutation_random_vs_flex (curandState_t * states, unsigned int *n_update, unsigned long int max)
{	
	//OBTENEMOS LA CONFORMACION
	unsigned int tid = threadIdx.x + threadIdx.y * blockDim.x;
	unsigned int blockID = blockIdx.x + blockIdx.y * gridDim.x;
	unsigned long int id_conformation = blockID * (blockDim.x * blockDim.y) + (threadIdx.y*blockDim.x) + threadIdx.x;
	//int id_conformation;
	if (id_conformation < max)
	{
		//ENLACE Y FRAGMENTO A GIRAR ALEATORIO
		n_update[id_conformation] =  getRandomNumber(&states[id_conformation]) % 5;
	}

}

__global__ void mutation_kernel_vs_flex (struct flexibility_params_t *flexibility_params, curandState_t * states, unsigned int *n_update, type_data max_desp, type_data rotation, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, unsigned int conf_point, unsigned int nlig, unsigned int files, unsigned int stride_d, unsigned long int max)
{
	type_data a[3],b[3],p1[3];
        type_data ang;
        unsigned int num_point, frag_act, position_link;
        unsigned int rot_point, link;
        type_data qeje_x, qeje_y, qeje_z, qeje_w;
	curandState_t random_state;
	unsigned int tid = threadIdx.x + threadIdx.y * blockDim.x;
	unsigned int blockID = blockIdx.x + blockIdx.y * gridDim.x;
	unsigned long int id_conformation = blockID * (blockDim.x * blockDim.y) + (threadIdx.y*blockDim.x) + threadIdx.x;
	type_data local_x, local_y, local_z, local_w, quaternion_x, quaternion_y, quaternion_z, quaternion_w;
	type_data angle, eje[3];
	
	//int id_conformation;
	if (id_conformation < max)
	{
		switch (n_update[id_conformation]) 
		{
				case 0:
					//MOVE X
					random_state = states[id_conformation];		
					move_x[id_conformation] += getRandomNumber_double(&random_state, max_desp); //Desplazamiento entre -max_desp y max_desp
					states[id_conformation] = random_state;
					break;
				case 1:
					//MOVE Y
					random_state = states[id_conformation];							
					move_y[id_conformation] += getRandomNumber_double(&random_state, max_desp);
					states[id_conformation] = random_state;
					break;
				case 2:
					//MOVE Z
					random_state = states[id_conformation];		
					move_z[id_conformation] += getRandomNumber_double(&random_state, max_desp);
					states[id_conformation] = random_state;
					break;
				case 3:
					//ROTATION
					random_state = states[id_conformation];
	
					quaternion_x = quat_x[id_conformation];
					quaternion_y = quat_y[id_conformation];
					quaternion_z = quat_z[id_conformation];
					quaternion_w = quat_w[id_conformation];

					angle = getRandomNumber_double(&random_state, rotation);
					eje[0] = getRandomNumber_double(&random_state, 1);
					eje[1] = getRandomNumber_double(&random_state, 1);
					eje[2] = getRandomNumber_double(&random_state, 1);

					setRotation (&local_x, &local_y, &local_z, &local_w, angle, eje);
					composeRotation (&local_x, &local_y, &local_z, &local_w, &quaternion_x, &quaternion_y, &quaternion_z, &quaternion_w, &quaternion_x, &quaternion_y, &quaternion_z, &quaternion_w);

					normalize(&quaternion_x,&quaternion_y,&quaternion_z,&quaternion_w);
	
					quat_x[id_conformation] = quaternion_x;
					quat_y[id_conformation] = quaternion_y;
					quat_z[id_conformation] = quaternion_z;
					quat_w[id_conformation] = quaternion_w;
		
					states[id_conformation] = random_state;
					break;
				case 4:
					//FLEXIBILITY
					unsigned int file = (id_conformation+stride_d) / conf_point;
                			//printf("File %d id %d conf %d\n",file,id_conformation,conf_point);
                			 //OBTENEMOS EL ANGULO EN RADIANES ALEATORIO ENTRE UN MAXIMO.
                			type_data t_p = getRandomNumber_double (&states[id_conformation], rotation);
                			//ENLACE Y FRAGMENTO A GIRAR ALEATORIO
                			int link =  getRandomNumber(&states[id_conformation]) % flexibility_params[file].n_links;
                			//OBTENEMOS EL PUNTO DEL ENLACE DESDE DONDE ROTAR
                			int rot_point = getRandomNumber(&states[id_conformation]) % 2;
					
                			//printf("id %d ang %f link %d rot_point %d\n",id_conformation,ang,link,rot_point);
                			//NOS SITUAMOS EN EL VECTOR DE CONFORMACIONES Y DE ENLACES
                			unsigned int position_conformation = id_conformation * nlig;

                			position_link = link;

                			//EJE DE GIRO
                			a[0] = conformations_x[position_conformation + (flexibility_params[file].links[2 * position_link] - 1)];
                			a[1] = conformations_y[position_conformation + (flexibility_params[file].links[2 * position_link] - 1)];
                			a[2] = conformations_z[position_conformation + (flexibility_params[file].links[2 * position_link] - 1)];
                			b[0] = conformations_x[position_conformation + (flexibility_params[file].links[(2 * position_link)+1] - 1)];
                			b[1] = conformations_y[position_conformation + (flexibility_params[file].links[(2 * position_link)+1] - 1)];
                			b[2] = conformations_z[position_conformation + (flexibility_params[file].links[(2 * position_link)+1] - 1)];

                			if (rot_point == 0){
                        			eje[0] = b[0] - a[0];
                        			eje[1] = b[1] - a[1];
                        			eje[2] = b[2] - a[2];
                        			p1[0] = conformations_x[position_conformation + (flexibility_params[file].links[2 * position_link] - 1)];
                        			p1[1] = conformations_y[position_conformation + (flexibility_params[file].links[2 * position_link] - 1)];
                        			p1[2] = conformations_z[position_conformation + (flexibility_params[file].links[2 * position_link] - 1)];
                        			num_point = (2 * position_link) + 1;
                			}else {
                        			eje[0] = a[0] - b[0];
                        			eje[1] = a[1] - b[1];
                        			eje[2] = a[2] - b[2];
                        			p1[0] = conformations_x[position_conformation + (flexibility_params[file].links[(2 * position_link)+1] - 1)];
                        			p1[1] = conformations_y[position_conformation + (flexibility_params[file].links[(2 * position_link)+1] - 1)];
                        			p1[2] = conformations_z[position_conformation + (flexibility_params[file].links[(2 * position_link)+1] - 1)];
                        			num_point = 2 * position_link;
                			}

                			//CONFIGURACION EJE DE ROTACION COMO QUATERNION
                			setRotation_flexibility_vs(&qeje_x,&qeje_y,&qeje_z,&qeje_w,ang,eje);
			                //int j2 = 0;
                			unsigned int shift_fragment = 0;
                			//ROTAMOS TODOS LOS FRAGMENTOS QUE TENGAN COMO ORIGEN EL PUNTO DE ROTACION
                			//while ((frag_act = links_fragments_d[(num_point * n_fragments) + j2]) != 0)
                			frag_act = flexibility_params[file].links_fragments[num_point];
                			//{

			                for (unsigned int i2 = 0; i2 < (frag_act - 1); i2++)
                        			shift_fragment = shift_fragment + flexibility_params[file].fragments_tam[i2];
                			rotateLigandFragment_vs((flexibility_params[file].fragments + shift_fragment),flexibility_params[file].fragments_tam[frag_act-1],(conformations_x + position_conformation), (conformations_y + position_conformation), (conformations_z + position_conformation), qeje_x, qeje_y, qeje_z, qeje_w, p1);

					break;
		}
	}
}

extern void mutation_gpu_calculation_vs_flex (struct flexibility_params_t *flexibility_params, struct receptor_t proteina, struct param_t param, struct ligand_params_t *ligand_params, struct metaheuristic_t metaheuristic, struct device_t *devices, type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *ql, char *ligtype, unsigned int *bonds, unsigned int nlig, unsigned int files, unsigned int nconformations,unsigned int orden_device, unsigned int stride)
{
	curandState_t *states_d;
	cudaError_t cudaStatus;
	type_data *quat_d_x, *quat_d_y, *quat_d_z, *quat_d_w;
	type_data *move_x_d, *move_y_d, *move_z_d;
	type_data *conformations_x_d, *conformations_y_d, *conformations_z_d;
	struct flexibility_params_t *flexibility_params_d;
	unsigned int *n_update,ngpu;
	unsigned int randSize = nconformations * sizeof(curandState_t);
	unsigned int moveSize = nconformations * sizeof(int);
	unsigned int moveSize_f = nconformations * sizeof(type_data);
	unsigned int conf_point = nconformations / files;

	cudaEvent_t start_m, stop_m, start_k, stop_k;
	type_data time_transfer = 0;
        type_data memory_in, memory_out, kernels = 0, tmp;	

	ngpu = devices->id[orden_device];
	cudaSetDevice(ngpu);
	cudaDeviceReset();
	//cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) printf("Error al principio de mutacion %d\n",ngpu);
	unsigned int blk_t = ceil(nconformations / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
	conf_point = nconformations / files;
	dim3 grid_t  (ceil(blk_t/8)+1, 8);	
	dim3 hilos_t (devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);	
	unsigned long int max_t = nconformations;	
	
	dataToGPUFlexibility_Params(files,ligand_params,flexibility_params,flexibility_params_d);
	dataToGPUConformations(nconformations,nlig,conformations_x,conformations_x_d,conformations_y,conformations_y_d,conformations_z,conformations_z_d);

	dataToGPUEnergy(nconformations, move_x, move_y, move_z, quat_x, quat_y, quat_z, quat_w, move_x_d, move_y_d, move_z_d, quat_d_x, quat_d_y, quat_d_z, quat_d_w);

	cudaEventCreate(&start_m);
    	cudaEventCreate(&stop_m);
    	cudaEventCreate(&start_k);
	cudaEventCreate(&stop_k);

        cudaEventRecord(start_m, 0);
	cudaStatus = cudaMalloc((void**) &states_d, randSize);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMalloc states_d values failed!");
	cudaStatus = cudaMalloc((void**) &n_update, moveSize);
	
	cudaEventRecord(stop_m, 0);
    	cudaEventSynchronize(stop_m);
    	cudaEventElapsedTime(&memory_in, start_m, stop_m);
	
	cudaEventRecord(start_k, 0);
	setupCurandState <<< (nconformations/devices->hilos[orden_device*metaheuristic.num_kernels+4])+1, devices->hilos[orden_device*metaheuristic.num_kernels+4] >>>  (states_d, param.automatic_seed, param.seed, nconformations);
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel setupCurandState failed \n");
	
	mutation_random_vs_flex <<<grid_t,hilos_t>>> (states_d,n_update,max_t);
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel mutation_random failed \n");
	//exit(0);	
	mutation_kernel_vs_flex <<<grid_t,hilos_t>>> (flexibility_params_d,states_d,n_update,param.max_desp,param.rotation_mu,conformations_x_d,conformations_y_d,conformations_z_d,move_x_d,move_y_d,move_z_d,quat_d_x,quat_d_y,quat_d_z,quat_d_w,conf_point,nlig,files,stride,max_t);
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel mutation failed \n");
	cudaEventRecord(stop_k, 0);
    	cudaEventSynchronize(stop_k);
    	cudaEventElapsedTime(&tmp, start_k, stop_k);
        kernels += tmp;

	cudaEventRecord(start_m, 0);
        cudaStatus = cudaMemcpy(move_x, move_x_d, moveSize_f, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
        cudaStatus = cudaMemcpy(move_y, move_y_d, moveSize_f, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
        cudaStatus = cudaMemcpy(move_z, move_z_d, moveSize_f, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
        //exit(0);
        cudaStatus = cudaMemcpy(quat_x, quat_d_x, moveSize_f, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
        cudaStatus = cudaMemcpy(quat_y, quat_d_y, moveSize_f, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
        cudaStatus = cudaMemcpy(quat_z, quat_d_z, moveSize_f, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
        cudaStatus = cudaMemcpy(quat_w, quat_d_w, moveSize_f, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");

        cudaEventRecord(stop_m, 0);
	cudaEventSynchronize(stop_m);
        cudaEventElapsedTime(&memory_out, start_m, stop_m);

	cudaFree(move_x_d);
        cudaFree(move_y_d);
        cudaFree(move_z_d);
        cudaFree(quat_d_x);
        cudaFree(quat_d_y);
        cudaFree(quat_d_z);
        cudaFree(quat_d_w);
        cudaFree(states_d);
	cudaFree(n_update);
        cudaDeviceReset();
	
	gpuSolver_vs_flex (conf_point,files,nlig,ligtype,bonds,ql,ligand_params,flexibility_params,proteina,param,metaheuristic,devices,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,weights,f_params,nconformations,orden_device,stride);
	//exit(0);
	if (metaheuristic.IMEMUCom > 0)
	{
		if (param.montecarlo)
			multigpu_mejorar_montecarlo_vs_flex (nconformations,files,nlig,ligtype,bonds,ql,ligand_params,proteina,param,weights,f_params,devices,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,metaheuristic,flexibility_params,nconformations,orden_device,stride,2);
		else 
			multigpu_mejorar_gpusolver_vs_flex (nconformations,files,nlig,ligtype,bonds,ql,ligand_params,flexibility_params,proteina,param,weights,f_params,devices,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,metaheuristic,nconformations,orden_device,stride,2);
			
	}
	//printf("Fin mutacion\n");
	//exit(0);*/
	
}


extern void mutation_gpu_vs_flex (struct receptor_t proteina, struct param_t param, struct ligand_params_t *ligand_params, struct flexibility_params_t *flexibility_params, struct metaheuristic_t metaheuristic, struct device_t *devices, type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *ql, char *ligtype, unsigned int *bonds, unsigned int nlig, unsigned int files, unsigned int nconformations,unsigned int orden_device, unsigned int stride) 
{
	size_t available,total_m;
    cudaError_t cudaStatus;
    unsigned long int max_memory;
    unsigned long int max_positions;
    unsigned long int max_common;
    
    unsigned long int typeSize = proteina.nrec * sizeof(char);
    unsigned long int recSize = proteina.nrec * sizeof(type_data);
    unsigned long int bondSize = proteina.nrec * sizeof(int) * MAXBOND;	    
    unsigned long int randSize = nconformations * sizeof(curandState_t);	
    unsigned long int moveSize_tam = nconformations * sizeof(type_data);
    
    unsigned long int tam_max, tam_tmp, nconformations_count_total, nconformations_count_partial, tam_conformation, stride_d;
    type_data memory_in, memory_out, kernels = 0, tmp;

    max_positions = moveSize_tam * 7 + randSize;
    max_common = (recSize * 4) + typeSize + bondSize; 
    max_memory = max_positions;
    tam_conformation =  (sizeof(type_data) * 7) + sizeof(curandState_t);
	
	cudaSetDevice(devices->id[orden_device]);
    cudaDeviceReset();
    cudaMemGetInfo(&tam_max, &total_m);
    //cudaDeviceSynchronize();
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
    //std::cout << tam_max << " " << total_m << "\n";
    tam_tmp = tam_max - (tam_max * 0.2);

	if (tam_tmp < max_memory)
        {
                //printf("Trocear en 1 GPU\n");
                max_memory = max_common;
                nconformations_count_total = 0;
                nconformations_count_partial = 0;
                stride_d = 0;
                while (nconformations_count_total < nconformations)
                {
                        max_memory += tam_conformation;
                        if (max_memory > tam_tmp)
                        {
                                //printf("Trozo %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
                                //nconformations_count_total--;
                                //nconformations_count_partial--;
                                //Lanzar con nconformations_count_partial;						
				mutation_gpu_calculation_vs_flex (flexibility_params,proteina,param,ligand_params,metaheuristic,devices,weights,f_params,conformations_x+(stride_d*nlig), conformations_y+(stride_d*nlig),conformations_z+(stride_d*nlig), move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,nconfs+stride_d,ql+(stride_d*nlig),ligtype+(stride_d*nlig),bonds+(stride_d*nlig),nlig,files,nconformations_count_partial,orden_device,stride_d);	

                                stride_d += nconformations_count_partial;
                                max_memory = max_common;
                                nconformations_count_partial = 0;
                        }
                        else
                        {
                                nconformations_count_total++;
                                nconformations_count_partial++;
                        }
                }
                //printf("Trozo final %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
				nconformations_count_partial--;
				mutation_gpu_calculation_vs_flex (flexibility_params,proteina,param,ligand_params,metaheuristic,devices,weights,f_params,conformations_x+(stride_d*nlig), conformations_y+(stride_d*nlig),conformations_z+(stride_d*nlig), move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,nconfs+stride_d,ql+(stride_d*nlig),ligtype+(stride_d*nlig),bonds+(stride_d*nlig),nlig,files,nconformations_count_partial,orden_device,stride_d);			
		}

        else
        {
                printf("No Trocea\n");
                //Lanzar con todo
		mutation_gpu_calculation_vs_flex (flexibility_params,proteina,param,ligand_params,metaheuristic,devices,weights,f_params,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,ql,ligtype,bonds,nlig,files,nconformations_count_partial,orden_device,stride_d);

        }	


}


