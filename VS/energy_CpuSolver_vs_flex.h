#ifndef __CPUSOLVER_VS_FLEX_H
#define __CPUSOLVER_VS_FLEX_H
#include "energy_common.h"
#include "vector_types.h"


extern void generate_positions_cpp_vs_flex (struct param_t param, struct vectors_t *vectors, struct metaheuristic_t metaheuristic, struct flexibility_params_t *flexibility_params,struct flexibility_data_t * flexibility_conformations);

extern void energy_internal_conformations_cpu_vs (unsigned int n, unsigned int nlig, unsigned int files, struct param_t param, struct metaheuristic_t metaheuristic, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, struct ligand_params_t *ligand_params, struct flexibility_params_t *flexibility_params,  type_data *weights, struct force_field_param_t *f_params, type_data *energy, type_data *ql, char *ligtype);

extern void energy_internal_conformations_cpu_vs_type2 (unsigned int n, unsigned int nlig, unsigned int files, struct param_t param, struct metaheuristic_t metaheuristic, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, struct ligand_params_t *ligand_params, struct flexibility_params_t *flexibility_params, type_data *weights, struct force_field_param_t *f_params, type_data *energy, type_data *ql, char *ligtype);
#endif
