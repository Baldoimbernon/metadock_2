#include "energy_common.h"
#include "energy_common_vs.h"
#include "energy_cuda.h"
#include "metaheuristic_inc_vs_flex.h"
#include <thrust/sequence.h>
#include <thrust/sort.h>
#include <thrust/functional.h>
#include <omp.h>
extern void colocar_vs_by_step (struct param_t param, struct metaheuristic_t metaheuristic, unsigned int nlig, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int stride, unsigned int pos)
{
	struct vectors_t vtmp;
        unsigned int nc;

        vtmp.nconformations = stride;
        vtmp.move_x = (type_data *)malloc(sizeof(type_data)*stride);
        vtmp.move_y = (type_data *)malloc(sizeof(type_data)*stride);
        vtmp.move_z = (type_data *)malloc(sizeof(type_data)*stride);
        vtmp.quat_x = (type_data *)malloc(sizeof(type_data)*stride);
        vtmp.quat_y = (type_data *)malloc(sizeof(type_data)*stride);
        vtmp.quat_z = (type_data *)malloc(sizeof(type_data)*stride);
        vtmp.quat_w = (type_data *)malloc(sizeof(type_data)*stride);
	vtmp.energy.energy = (type_data *)malloc(sizeof(type_data)*stride);
	
	for (unsigned int i=0; i<stride;i++)
        {
                nc = nconfs[i] - (stride * pos);
                vtmp.move_x[i] = move_x[nc];
                vtmp.move_y[i] = move_y[nc];
                vtmp.move_z[i] = move_z[nc];
                vtmp.quat_x[i] = quat_x[nc];
                vtmp.quat_y[i] = quat_y[nc];
                vtmp.quat_z[i] = quat_z[nc];
                vtmp.quat_w[i] = quat_w[nc];
                vtmp.energy.energy[i] = energy[i];
        }
	
	memcpy(move_x, vtmp.move_x, stride * sizeof(type_data));
        memcpy(move_y, vtmp.move_y, stride * sizeof(type_data));
        memcpy(move_z, vtmp.move_z, stride * sizeof(type_data));
        memcpy(quat_x, vtmp.quat_x, stride * sizeof(type_data));
        memcpy(quat_y, vtmp.quat_y, stride * sizeof(type_data));
        memcpy(quat_z, vtmp.quat_z, stride * sizeof(type_data));
        memcpy(quat_w, vtmp.quat_w, stride * sizeof(type_data));
        memcpy(energy, vtmp.energy.energy, stride * sizeof(type_data));

	free(vtmp.move_x);
        free(vtmp.move_y);
        free(vtmp.move_z);
        free(vtmp.quat_x);
        free(vtmp.quat_y);
        free(vtmp.quat_z);
        free(vtmp.quat_w);
        free(vtmp.energy.energy);
}

/*extern void colocar_vs (unsigned int nlig, struct param_t param, struct vectors_t *vectors_e_s, struct metaheuristic_t metaheuristic)
{
	struct vectors_t vtmp;
	unsigned int nc,nc_conf_o,nc_conf_d;
	unsigned int h1;
	
	vtmp.files = vectors_e_s->files;
	vtmp.nconformations = vectors_e_s->nconformations;
	vtmp.move_x = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);
	vtmp.move_y = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);
	vtmp.move_z = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);
	vtmp.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);	
	vtmp.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);	
	vtmp.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);	
	vtmp.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);		
	vtmp.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);
	switch (param.mode) {		
		case 1:
			h1 = 1;
			break;
		case 0:
		case 3:
		case 2:
			h1 = metaheuristic.Threads1Inc;
			break;
	}
	omp_set_num_threads (h1);
	#pragma omp parallel for private (nc,nc_conf_d,nc_conf_o)		
	for (unsigned int i=0; i<vectors_e_s->nconformations;i++)
	{
		nc = vectors_e_s->energy.n_conformation[i];
		nc_conf_o = nc * nlig;				
		nc_conf_d = i * nlig;				
			
		vtmp.move_x[i] = vectors_e_s->move_x[nc];
		vtmp.move_y[i] = vectors_e_s->move_y[nc];
		vtmp.move_z[i] = vectors_e_s->move_z[nc];
		vtmp.quat_x[i] = vectors_e_s->quat_x[nc];
		vtmp.quat_y[i] = vectors_e_s->quat_y[nc];
		vtmp.quat_z[i] = vectors_e_s->quat_z[nc];
		vtmp.quat_w[i] = vectors_e_s->quat_w[nc];
		vtmp.energy.energy[i] = vectors_e_s->energy.energy[i];
	}
	
	//free(vectors_e_s->move_x); free(vectors_e_s->move_y); free(vectors_e_s->move_z);
	//free(vectors_e_s->quat_x); free(vectors_e_s->quat_y); free(vectors_e_s->quat_z); free(vectors_e_s->quat_w);
	//free(vectors_e_s->energy.energy); free(vectors_e_s->energy.n_conformation);
	//vectors_e_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);
	//vectors_e_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);
	//vectors_e_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);
	//vectors_e_s->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);
	//vectors_e_s->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);
	//vectors_e_s->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);
	//vectors_e_s->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);
	//vectors_e_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);
	//vectors_e_s->energy.n_conformation = (int *)malloc(sizeof(int)*vectors_e_s->nconformations); 
	
	memcpy(vectors_e_s->move_x, vtmp.move_x, vectors_e_s->nconformations * sizeof(type_data));  	
	memcpy(vectors_e_s->move_y, vtmp.move_y, vectors_e_s->nconformations * sizeof(type_data)); 
	memcpy(vectors_e_s->move_z, vtmp.move_z, vectors_e_s->nconformations * sizeof(type_data));
	memcpy(vectors_e_s->quat_x, vtmp.quat_x, vectors_e_s->nconformations * sizeof(type_data));
	memcpy(vectors_e_s->quat_y, vtmp.quat_y, vectors_e_s->nconformations * sizeof(type_data));
	memcpy(vectors_e_s->quat_z, vtmp.quat_z, vectors_e_s->nconformations * sizeof(type_data));
	memcpy(vectors_e_s->quat_w, vtmp.quat_w, vectors_e_s->nconformations * sizeof(type_data));
	memcpy(vectors_e_s->energy.energy, vtmp.energy.energy, vectors_e_s->nconformations * sizeof(type_data));
	thrust::sequence(vectors_e_s->energy.n_conformation,vectors_e_s->energy.n_conformation + vectors_e_s->nconformations);
	//free(vtmp.conformations_x);
	//free(vtmp.conformations_y);
	//free(vtmp.conformations_z);
	free(vtmp.move_x);
	free(vtmp.move_y);
	free(vtmp.move_z);
	free(vtmp.quat_x);
	free(vtmp.quat_y);
	free(vtmp.quat_z);
	free(vtmp.quat_w);
	free(vtmp.energy.energy);
}*/

extern void incluir_vs (unsigned int nlig, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param)
{
	struct vectors_t vtmp;
	unsigned int conf;
	unsigned int h1;
	
	unsigned int total_included = metaheuristic.NEMInc_selec + (metaheuristic.NEFIni - metaheuristic.NEMInc_selec);	
	metaheuristic.NEPInc_selec = metaheuristic.NEFIni - metaheuristic.NEMInc_selec;
	
	unsigned int stride_e = vectors_e->nconformations / vectors_e->files;
	unsigned int stride_s = vectors_s->nconformations / vectors_s->files;
	//printf("total_included %d , stride_e %d , stride_s %d\n",total_included,stride_e,stride_s);	
		
	unsigned int stride_t = stride_e +  stride_s; 	
	vtmp.files = vectors_e->files;
	vtmp.nlig = vectors_e->nlig;
	vtmp.nconformations = vectors_e->files * stride_t;
	vtmp.conformations_x = (type_data *)malloc(sizeof(type_data)*nlig*vtmp.nconformations);
	vtmp.conformations_y = (type_data *)malloc(sizeof(type_data)*nlig*vtmp.nconformations);
	vtmp.conformations_z = (type_data *)malloc(sizeof(type_data)*nlig*vtmp.nconformations);
	vtmp.move_x = (type_data *)malloc(sizeof(type_data)*vtmp.nconformations);
	vtmp.move_y = (type_data *)malloc(sizeof(type_data)*vtmp.nconformations);
	vtmp.move_z = (type_data *)malloc(sizeof(type_data)*vtmp.nconformations);
	vtmp.quat_x = (type_data *)malloc(sizeof(type_data)*vtmp.nconformations);	
	vtmp.quat_y = (type_data *)malloc(sizeof(type_data)*vtmp.nconformations);	
	vtmp.quat_z = (type_data *)malloc(sizeof(type_data)*vtmp.nconformations);	
	vtmp.quat_w = (type_data *)malloc(sizeof(type_data)*vtmp.nconformations);	
	vtmp.energy.energy = (type_data *)malloc(sizeof(type_data)*vtmp.nconformations);
	vtmp.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vtmp.nconformations);
	thrust::sequence(vtmp.energy.n_conformation,vtmp.energy.n_conformation + vtmp.nconformations);
	
	switch (param.mode) {		
		//case 0:	
		case 1:
			h1 = 1;
			break;
	  	case 0:		
		case 2:
		case 3:	
			h1 = metaheuristic.Threads1Inc;
			break;
	}
	//for (int i=0; i<64;i++) printf("INC xyz %f %f %f\n",vectors_e->conformations_x[i],vectors_e->conformations_y[i],vectors_e->conformations_z[i]);
	omp_set_num_threads(h1);
	#pragma omp parallel for
	for (unsigned int i = 0;i < vectors_s->files;i++)
	{
		memcpy(vtmp.conformations_x+ i*nlig*(stride_e +  stride_s), vectors_s->conformations_x + i*nlig*stride_s, stride_s * nlig * sizeof(type_data)); 
		memcpy(vtmp.conformations_y+ i*nlig*(stride_e +  stride_s), vectors_s->conformations_y + i*nlig*stride_s, stride_s * nlig * sizeof(type_data)); 
		memcpy(vtmp.conformations_z+ i*nlig*(stride_e +  stride_s), vectors_s->conformations_z + i*nlig*stride_s, stride_s * nlig * sizeof(type_data)); 
		memcpy(vtmp.move_x + i*stride_t, vectors_s->move_x + i*stride_s, stride_s * sizeof(type_data)); 
		memcpy(vtmp.move_y + i*stride_t, vectors_s->move_y + i*stride_s, stride_s * sizeof(type_data)); 
		memcpy(vtmp.move_z + i*stride_t, vectors_s->move_z + i*stride_s, stride_s * sizeof(type_data)); 
		memcpy(vtmp.quat_x + i*stride_t, vectors_s->quat_x + i*stride_s, stride_s * sizeof(type_data)); 
		memcpy(vtmp.quat_y + i*stride_t, vectors_s->quat_y + i*stride_s, stride_s * sizeof(type_data)); 
		memcpy(vtmp.quat_z + i*stride_t, vectors_s->quat_z + i*stride_s, stride_s * sizeof(type_data)); 
		memcpy(vtmp.quat_w + i*stride_t, vectors_s->quat_w + i*stride_s, stride_s * sizeof(type_data)); 
		memcpy(vtmp.energy.energy + i*stride_t, vectors_s->energy.energy + i*stride_s, stride_s * sizeof(type_data)); 
		
		memcpy(vtmp.conformations_x+ i*nlig*(stride_e +  stride_s) + nlig*stride_s, vectors_e->conformations_x + i*nlig*stride_e, stride_e * nlig * sizeof(type_data));  			
		memcpy(vtmp.conformations_y+ i*nlig*(stride_e +  stride_s) + nlig*stride_s, vectors_e->conformations_y + i*nlig*stride_e, stride_e * nlig * sizeof(type_data));  			
		memcpy(vtmp.conformations_z+ i*nlig*(stride_e +  stride_s) + nlig*stride_s, vectors_e->conformations_z + i*nlig*stride_e, stride_e * nlig * sizeof(type_data));	
		memcpy(vtmp.move_x + i*stride_t + stride_s, vectors_e->move_x + i*stride_e, stride_e * sizeof(type_data));  			
		memcpy(vtmp.move_y + i*stride_t + stride_s, vectors_e->move_y + i*stride_e, stride_e * sizeof(type_data));  			
		memcpy(vtmp.move_z + i*stride_t + stride_s, vectors_e->move_z + i*stride_e, stride_e * sizeof(type_data));  			
		memcpy(vtmp.quat_x + i*stride_t + stride_s, vectors_e->quat_x + i*stride_e, stride_e * sizeof(type_data));  
		memcpy(vtmp.quat_y + i*stride_t + stride_s, vectors_e->quat_y + i*stride_e, stride_e * sizeof(type_data));  	
		memcpy(vtmp.quat_z + i*stride_t + stride_s, vectors_e->quat_z + i*stride_e, stride_e * sizeof(type_data));  	
		memcpy(vtmp.quat_w + i*stride_t + stride_s, vectors_e->quat_w + i*stride_e, stride_e * sizeof(type_data));  			
		memcpy(vtmp.energy.energy + i*stride_t + stride_s, vectors_e->energy.energy + i*stride_e, stride_e * sizeof(type_data));  					
	}
	
	#pragma omp parallel for
	for (unsigned int i = 0; i < vtmp.nconformations; i += stride_t)			
		thrust::stable_sort_by_key((vtmp.energy.energy + i),(vtmp.energy.energy + i + stride_t), (vtmp.energy.n_conformation + i), thrust::less<type_data>());			
	//colocar_vs_flex(param,&vtmp,metaheuristic);

	#pragma omp parallel for
	for (unsigned int i = 0; i < vtmp.files; i++)
		colocar_vs_flex_by_step (param,metaheuristic,vtmp.nlig,vtmp.conformations_x + (stride_t*vtmp.nlig*i),vtmp.conformations_y + (stride_t*vtmp.nlig*i),vtmp.conformations_z + (stride_t*vtmp.nlig*i), vtmp.move_x + (stride_t*i),vtmp.move_y + (stride_t*i),vtmp.move_z + (stride_t*i),vtmp.quat_x + (stride_t*i),vtmp.quat_y + (stride_t*i),vtmp.quat_z + (stride_t*i),vtmp.quat_w + (stride_t*i),vtmp.energy.energy + (stride_t*i),vtmp.energy.n_conformation + (stride_t*i),stride_t,i);
	thrust::sequence(vtmp.energy.n_conformation,vtmp.energy.n_conformation + vtmp.nconformations);

	//exit(0);	
	
	free(vectors_s->move_x); free(vectors_s->move_y); free(vectors_s->move_z); free(vectors_s->quat_x); free(vectors_s->quat_y); free(vectors_s->quat_z); free(vectors_s->quat_w);free(vectors_s->energy.n_conformation); free(vectors_s->energy.energy);
	free(vectors_s->conformations_x); free(vectors_s->conformations_y); free(vectors_s->conformations_z);free(vectors_s->ql); free(vectors_s->ligtype);free(vectors_s->subtype); free(vectors_s->bonds); free(vectors_s->nbonds);
	//exit(0);
			
	vectors_s->files = vectors_e->files;
	vectors_s->nconformations = vectors_s->files * total_included;

	//printf("Número de conformaciones: %d\n",vectors_s->nconformations);
	vectors_s->conformations_x = (type_data *)malloc(sizeof(type_data)* nlig * vectors_s->nconformations);
	vectors_s->conformations_y = (type_data *)malloc(sizeof(type_data)* nlig * vectors_s->nconformations);
	vectors_s->conformations_z = (type_data *)malloc(sizeof(type_data)* nlig * vectors_s->nconformations);

	vectors_s->ql = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);
	vectors_s->ligtype = (char *)malloc(sizeof(char)*nlig*vectors_s->nconformations);
	vectors_s->subtype = (char *)malloc(sizeof(char)*nlig*SUBTYPEMAXLEN*vectors_s->nconformations);
	vectors_s->bonds = (unsigned int *)malloc(sizeof(unsigned int)*nlig*MAXBOND*vectors_s->nconformations);
	vectors_s->nbonds = (char *)malloc(sizeof(char)*nlig*vectors_s->nconformations);

	vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nconformations);
	vectors_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	//exit(0);		
	omp_set_num_threads (h1);
	//#pragma omp parallel for	
	for (unsigned int i = 0;i < vectors_s->files;i++)
	{	
		memcpy(vectors_s->conformations_x + i*nlig*total_included, vtmp.conformations_x + i*nlig*stride_t, metaheuristic.NEMInc_selec * nlig * sizeof(type_data));  	
		memcpy(vectors_s->conformations_y + i*nlig*total_included, vtmp.conformations_y + i*nlig*stride_t, metaheuristic.NEMInc_selec * nlig * sizeof(type_data));  	
		memcpy(vectors_s->conformations_z + i*nlig*total_included, vtmp.conformations_z + i*nlig*stride_t, metaheuristic.NEMInc_selec * nlig * sizeof(type_data));  	
		
		memcpy(vectors_s->move_x + i*total_included, vtmp.move_x + i*stride_t, metaheuristic.NEMInc_selec * sizeof(type_data));  	
		memcpy(vectors_s->move_y + i*total_included, vtmp.move_y + i*stride_t, metaheuristic.NEMInc_selec * sizeof(type_data)); 
		memcpy(vectors_s->move_z + i*total_included, vtmp.move_z + i*stride_t, metaheuristic.NEMInc_selec * sizeof(type_data));
		memcpy(vectors_s->quat_x + i*total_included, vtmp.quat_x + i*stride_t, metaheuristic.NEMInc_selec * sizeof(type_data));
		memcpy(vectors_s->quat_y + i*total_included, vtmp.quat_y + i*stride_t, metaheuristic.NEMInc_selec * sizeof(type_data));
		memcpy(vectors_s->quat_z + i*total_included, vtmp.quat_z + i*stride_t, metaheuristic.NEMInc_selec * sizeof(type_data));
		memcpy(vectors_s->quat_w + i*total_included, vtmp.quat_w + i*stride_t, metaheuristic.NEMInc_selec * sizeof(type_data));
		memcpy(vectors_s->energy.energy + i*total_included, vtmp.energy.energy + i*stride_t, metaheuristic.NEMInc_selec * sizeof(type_data));										
				
		memcpy(vectors_s->conformations_x + i*nlig*total_included + metaheuristic.NEMInc_selec*nlig, vtmp.conformations_x + i*nlig*stride_t + (stride_t - metaheuristic.NEPInc_selec)*nlig, metaheuristic.NEPInc_selec * nlig * sizeof(type_data));  	
		memcpy(vectors_s->conformations_y + i*nlig*total_included + metaheuristic.NEMInc_selec*nlig, vtmp.conformations_y + i*nlig*stride_t + (stride_t - metaheuristic.NEPInc_selec)*nlig, metaheuristic.NEPInc_selec * nlig * sizeof(type_data));  	
		memcpy(vectors_s->conformations_z + i*nlig*total_included + metaheuristic.NEMInc_selec*nlig, vtmp.conformations_z + i*nlig*stride_t + (stride_t - metaheuristic.NEPInc_selec)*nlig, metaheuristic.NEPInc_selec * nlig * sizeof(type_data));  	
								
		memcpy(vectors_s->move_x + i*total_included + metaheuristic.NEMInc_selec, vtmp.move_x + i*stride_t + (stride_t - metaheuristic.NEPInc_selec), metaheuristic.NEPInc_selec * sizeof(type_data));  	
		memcpy(vectors_s->move_y + i*total_included + metaheuristic.NEMInc_selec, vtmp.move_y + i*stride_t + (stride_t - metaheuristic.NEPInc_selec), metaheuristic.NEPInc_selec * sizeof(type_data)); 
		memcpy(vectors_s->move_z + i*total_included + metaheuristic.NEMInc_selec, vtmp.move_z + i*stride_t + (stride_t - metaheuristic.NEPInc_selec), metaheuristic.NEPInc_selec * sizeof(type_data));
		memcpy(vectors_s->quat_x + i*total_included + metaheuristic.NEMInc_selec, vtmp.quat_x + i*stride_t + (stride_t - metaheuristic.NEPInc_selec), metaheuristic.NEPInc_selec * sizeof(type_data));
		memcpy(vectors_s->quat_y + i*total_included + metaheuristic.NEMInc_selec, vtmp.quat_y + i*stride_t + (stride_t - metaheuristic.NEPInc_selec), metaheuristic.NEPInc_selec * sizeof(type_data));
		memcpy(vectors_s->quat_z + i*total_included + metaheuristic.NEMInc_selec, vtmp.quat_z + i*stride_t + (stride_t - metaheuristic.NEPInc_selec), metaheuristic.NEPInc_selec * sizeof(type_data));
		memcpy(vectors_s->quat_w + i*total_included + metaheuristic.NEMInc_selec, vtmp.quat_w + i*stride_t + (stride_t - metaheuristic.NEPInc_selec), metaheuristic.NEPInc_selec * sizeof(type_data));
		memcpy(vectors_s->energy.energy + i*total_included + metaheuristic.NEMInc_selec, vtmp.energy.energy + i*stride_t + (stride_t - metaheuristic.NEPInc_selec), metaheuristic.NEPInc_selec * sizeof(type_data));	
	}
	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);	
	
	//#pragma omp parallel for	
	for (unsigned int i = 0;i < vectors_s->files;i++)
	{
		for (unsigned int h = 0; h < total_included;h++)
		{
		memcpy(vectors_s->ql + i*nlig*total_included + h*nlig, vectors_e->ql + i*nlig*stride_e, nlig*sizeof(type_data));
		memcpy(vectors_s->ligtype + i*nlig*total_included + h*nlig, vectors_e->ligtype + i*nlig*stride_e, nlig * sizeof(char));
		memcpy(vectors_s->subtype + i*nlig*SUBTYPEMAXLEN*total_included + h*SUBTYPEMAXLEN*nlig, vectors_e->subtype + i*nlig*SUBTYPEMAXLEN*stride_e,nlig * SUBTYPEMAXLEN * sizeof(char));
		memcpy(vectors_s->bonds + i*nlig*MAXBOND*total_included + h*MAXBOND*nlig, vectors_e->bonds + i*nlig*MAXBOND*stride_e, nlig * MAXBOND * sizeof(unsigned int));
		memcpy(vectors_s->nbonds + i*nlig*total_included + h*nlig, vectors_e->nbonds + i*nlig*stride_e, nlig * sizeof(char));
		}
	}
	//printf("vectors_s->energy.energy 0 %f 1 %f 2 %f \n",vectors_s->energy.energy[0],vectors_s->energy.energy[1],vectors_s->energy.energy[2]);
}


