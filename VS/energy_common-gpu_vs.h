#ifndef COMMON_GPU_VS_H_
#define COMMON_GPU_VS_H_

extern void commonDataToGPU_WS_vs (unsigned int files, unsigned int nconformations, unsigned int nlig, struct receptor_t proteina, type_data *&rec_x_d, type_data *&rec_y_d, type_data *&rec_z_d, type_data *&qr_d, char *&rectype_d, unsigned int *&bondsr_d, unsigned int *atoms, unsigned int *&atoms_d, unsigned int *nlinks, unsigned int *&nlinks_d, char *ligtype, char *&ligtype_d, unsigned int *bonds, unsigned int *&bonds_d, type_data *ql, type_data *&ql_d);
extern void commonDataToGPU_vs (unsigned int files, unsigned int nconformations, unsigned int nlig, struct receptor_t proteina, struct receptor_t *& proteina_d, unsigned int *atoms, unsigned int *&atoms_d, char *ligtype, char *&ligtype_d, unsigned int *bonds, unsigned int *&bonds_d, type_data *ql, type_data *&ql_d);
extern void dataToGPU_vs (unsigned int N, type_data * &moves_x_d, type_data * &moves_y_d, type_data * &moves_z_d, type_data * &quat_d_x, type_data * &quat_d_y, type_data * &quat_d_z, type_data * &quat_d_w, curandState_t * &states_d);
extern void dataToGPUFlexibility_Params(unsigned int files, struct ligand_params_t *ligand_params, struct flexibility_params_t *flexibility_params, struct flexibility_params_t *&flexibility_params_d);
#endif /* COMMON_GPU_H_ */
