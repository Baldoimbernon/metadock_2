#include "energy_cuda.h"
#include "definitions.h"
#include "energy_kernel.h"
#include "energy_positions.h"
#include "energy_positions_common.h"
#include "energy_common.h"
#include "energy_common-gpu_vs.h"
#include "energy_common-gpu.h"
#include "energy_moving.h"
#include "energy_positions_vs.h"
#include <thrust/sort.h>

extern void gpu_mejorar_vs_environment (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct param_t param, struct vectors_t *vectors_e_s, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int orden_device, char *ligtype, unsigned int *bonds, type_data *ql, unsigned int fase)
{
	struct vectors_t vectors_mejora;

        size_t available,total_m;
        cudaError_t cudaStatus;
        unsigned long int max_memory;
        unsigned long int max_positions;
        unsigned long int max_common;
        unsigned long int max_conf;
        unsigned long int typeSize = proteina.nrec * sizeof(char);
        unsigned long int recSize = proteina.nrec * sizeof(type_data);
        unsigned long int bondSize = proteina.nrec * sizeof(unsigned int) * MAXBOND;

        unsigned long int randSize = vectors_e_s->nconformations * sizeof(curandState_t);
        unsigned long int moveSize_tam = vectors_e_s->nconformations * sizeof(type_data);
        unsigned long int energyConf_tam = vectors_e_s->nconformations * sizeof(unsigned int);
        unsigned long int confSize_tam = vectors_e_s->nconformations * vectors_e_s->nlig * sizeof(type_data);
        unsigned long int confSize_tam_char = vectors_e_s->nconformations * vectors_e_s->nlig * sizeof(char);
        unsigned long int confSize_bonds = vectors_e_s->nconformations * vectors_e_s->nlig * MAXBOND *  sizeof(unsigned int);

        unsigned long int randSize_environment = vectors_e_s->nconformations * metaheuristic.NEEImp * sizeof(curandState_t);
        unsigned long int moveSize_tam_environment = vectors_e_s->nconformations * metaheuristic.NEEImp * sizeof(type_data);
        unsigned long int energyConf_tam_environment = vectors_e_s->nconformations * metaheuristic.NEEImp * sizeof(unsigned int);
        unsigned long int confSize_tam_environment = vectors_e_s->nconformations * vectors_e_s->nlig * metaheuristic.NEEImp * sizeof(type_data);
        unsigned long int confSize_tam_char_environment = vectors_e_s->nconformations * vectors_e_s->nlig * metaheuristic.NEEImp * sizeof(char);
        unsigned long int confSize_bonds_environment = vectors_e_s->nconformations * vectors_e_s->nlig * MAXBOND * metaheuristic.NEEImp * sizeof(unsigned int);

	unsigned long int tam_max, tam_tmp, nconformations_count_total, nconformations_count_partial, tam_conformation, stride_d;
	//unsigned int *nlinks = (unsigned int *)calloc(files,sizeof(unsigned int));

        max_positions = (moveSize_tam * 8) * 2 + energyConf_tam * 2 +  ((moveSize_tam_environment * 8) * 2 + energyConf_tam_environment * 2);
        max_conf = (confSize_tam * 3) + randSize + confSize_tam + confSize_tam_char + confSize_bonds + ((confSize_tam_environment * 3) + randSize_environment + confSize_tam_environment + confSize_tam_char_environment + confSize_bonds_environment);
        max_common = (recSize * 4) + typeSize + bondSize;

        max_memory = max_positions + max_conf + max_common;

        tam_conformation =  (sizeof(type_data) * 8 * 2) + sizeof(unsigned int) * 2 + (sizeof(type_data) * vectors_e_s->nlig * 3) +  sizeof(curandState_t) + vectors_e_s->nlig * sizeof(type_data) +  vectors_e_s->nlig * sizeof(char) +  vectors_e_s->nlig * MAXBOND *  sizeof(unsigned int) + (3 * ((sizeof(type_data) * 8 * 2) + sizeof(unsigned int) * 2 + (sizeof(type_data) * vectors_e_s->nlig * 3) +  sizeof(curandState_t) + vectors_e_s->nlig * sizeof(type_data) +  vectors_e_s->nlig * sizeof(char) +  vectors_e_s->nlig * MAXBOND *  sizeof(unsigned int)));

	vectors_mejora.files = vectors_e_s->files;
        vectors_mejora.nlig = vectors_e_s->nlig;
        vectors_mejora.nconformations = vectors_e_s->nconformations * metaheuristic.NEEImp;

        vectors_mejora.conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations*vectors_mejora.nlig);
        vectors_mejora.conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations*vectors_mejora.nlig);
        vectors_mejora.conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations*vectors_mejora.nlig);

        vectors_mejora.ql = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations*vectors_mejora.nlig);
        vectors_mejora.ligtype = (char *)malloc(sizeof(char)*vectors_mejora.nconformations*vectors_mejora.nlig);
        vectors_mejora.bonds = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations*vectors_mejora.nlig*MAXBOND);

    	vectors_mejora.move_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    	vectors_mejora.move_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    	vectors_mejora.move_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    	vectors_mejora.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    	vectors_mejora.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    	vectors_mejora.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    	vectors_mejora.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    	vectors_mejora.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations);
    	vectors_mejora.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
    	vectors_mejora.weights = (type_data *)malloc(sizeof(type_data)*SCORING_TERMS);

    	memcpy(vectors_mejora.f_params, vectors_e_s->f_params,MAXTYPES * sizeof(struct force_field_param_t));
    	memcpy(vectors_mejora.weights, vectors_e_s->weights,SCORING_TERMS * sizeof(type_data));

    	fill_conformations_environment_vs (vectors_mejora.nlig,vectors_e_s->conformations_x,vectors_e_s->conformations_y,vectors_e_s->conformations_z,vectors_e_s->move_x,vectors_e_s->move_y,vectors_e_s->move_z,vectors_e_s->quat_x,vectors_e_s->quat_y,vectors_e_s->quat_z,vectors_e_s->quat_w,ligtype,ql,bonds,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,vectors_mejora.ligtype,vectors_mejora.ql,vectors_mejora.bonds,vectors_e_s->nconformations,param,metaheuristic);

	thrust::sequence(vectors_mejora.energy.n_conformation,vectors_mejora.energy.n_conformation + vectors_mejora.nconformations);
	
	cudaSetDevice(devices->id[orden_device]);
        //cudaDeviceReset();
        cudaMemGetInfo(&tam_max, &total_m);
        cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
        //std::cout << tam_max << " " << total_m << "\n";
        tam_tmp = tam_max - (tam_max * 0.2);
        if (tam_tmp < max_memory)
        {
                //printf("Trocear en 1 GPU\n");
                max_memory = max_common;
                nconformations_count_total = 0;
                nconformations_count_partial = 0;
                stride_d = 0;
                while (nconformations_count_total < vectors_e_s->nconformations)
                {
                        max_memory += tam_conformation;
                        if (max_memory > tam_tmp)
                        {
                                //printf("Trozo %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
                                //nconformations_count_total--;
                                //nconformations_count_partial--;
                                //Lanzar con nconformations_count_partial;
				multigpu_mejorar_gpusolver_vs_environment (vectors_e_s->nconformations,vectors_e_s->files,vectors_e_s->nlig,vectors_e_s->ligtype + (stride_d * vectors_e_s->nlig),vectors_e_s->bonds + (stride_d * MAXBOND * vectors_e_s->nlig),vectors_e_s->ql + (stride_d * vectors_e_s->nlig),vectors_mejora.ligtype + (stride_d * metaheuristic.NEEImp * vectors_e_s->nlig) ,vectors_mejora.bonds + (stride_d * metaheuristic.NEEImp * MAXBOND * vectors_e_s->nlig),vectors_mejora.ql + (stride_d * metaheuristic.NEEImp * vectors_e_s->nlig),ligand_params,proteina,vectors_e_s->conformations_x + (stride_d * vectors_e_s->nlig),vectors_e_s->conformations_y + (stride_d * vectors_e_s->nlig), vectors_e_s->conformations_z + (stride_d * vectors_e_s->nlig),param,vectors_mejora.weights, vectors_mejora.f_params, devices, vectors_e_s->move_x + stride_d, vectors_e_s->move_y + stride_d, vectors_e_s->move_z + stride_d, vectors_e_s->quat_x + stride_d, vectors_e_s->quat_y + stride_d, vectors_e_s->quat_z + stride_d, vectors_e_s->quat_w + stride_d, vectors_e_s->energy.energy + stride_d, vectors_e_s->energy.n_conformation + stride_d, vectors_mejora.conformations_x + (stride_d * vectors_e_s->nlig * metaheuristic.NEEImp), vectors_mejora.conformations_y + (stride_d * vectors_e_s->nlig * metaheuristic.NEEImp), vectors_mejora.conformations_z + (stride_d * vectors_e_s->nlig * metaheuristic.NEEImp), vectors_mejora.move_x + (stride_d * metaheuristic.NEEImp), vectors_mejora.move_y + (stride_d * metaheuristic.NEEImp), vectors_mejora.move_z + (stride_d * metaheuristic.NEEImp), vectors_mejora.quat_x + (stride_d * metaheuristic.NEEImp), vectors_mejora.quat_y + (stride_d * metaheuristic.NEEImp), vectors_mejora.quat_z + (stride_d * metaheuristic.NEEImp), vectors_mejora.quat_w + (stride_d * metaheuristic.NEEImp), vectors_mejora.energy.energy + (stride_d * metaheuristic.NEEImp), vectors_mejora.energy.n_conformation + (stride_d * metaheuristic.NEEImp),metaheuristic, nconformations_count_partial,nconformations_count_partial*metaheuristic.NEEImp,orden_device,stride_d,fase);

			 	stride_d += nconformations_count_partial;
                                max_memory = max_common;
                                nconformations_count_partial = 0;
                        }
                        else
                        {
                                nconformations_count_total++;
                                nconformations_count_partial++;
                        }
                }
                //printf("Trozo final %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
                nconformations_count_partial--;
		multigpu_mejorar_gpusolver_vs_environment (vectors_e_s->nconformations,vectors_e_s->files,vectors_e_s->nlig,vectors_e_s->ligtype + (stride_d * vectors_e_s->nlig),vectors_e_s->bonds + (stride_d * MAXBOND * vectors_e_s->nlig),vectors_e_s->ql + (stride_d * vectors_e_s->nlig),vectors_mejora.ligtype + (stride_d * metaheuristic.NEEImp * vectors_e_s->nlig) ,vectors_mejora.bonds + (stride_d * metaheuristic.NEEImp * MAXBOND * vectors_e_s->nlig),vectors_mejora.ql + (stride_d * metaheuristic.NEEImp * vectors_e_s->nlig),ligand_params,proteina,vectors_e_s->conformations_x + (stride_d * vectors_e_s->nlig),vectors_e_s->conformations_y + (stride_d * vectors_e_s->nlig), vectors_e_s->conformations_z + (stride_d * vectors_e_s->nlig),param,vectors_mejora.weights, vectors_mejora.f_params, devices, vectors_e_s->move_x + stride_d, vectors_e_s->move_y + stride_d, vectors_e_s->move_z + stride_d, vectors_e_s->quat_x + stride_d, vectors_e_s->quat_y + stride_d, vectors_e_s->quat_z + stride_d, vectors_e_s->quat_w + stride_d, vectors_e_s->energy.energy + stride_d, vectors_e_s->energy.n_conformation + stride_d, vectors_mejora.conformations_x + (stride_d * vectors_e_s->nlig * metaheuristic.NEEImp), vectors_mejora.conformations_y + (stride_d * vectors_e_s->nlig * metaheuristic.NEEImp), vectors_mejora.conformations_z + (stride_d * vectors_e_s->nlig * metaheuristic.NEEImp), vectors_mejora.move_x + (stride_d * metaheuristic.NEEImp), vectors_mejora.move_y + (stride_d * metaheuristic.NEEImp), vectors_mejora.move_z + (stride_d * metaheuristic.NEEImp), vectors_mejora.quat_x + (stride_d * metaheuristic.NEEImp), vectors_mejora.quat_y + (stride_d * metaheuristic.NEEImp), vectors_mejora.quat_z + (stride_d * metaheuristic.NEEImp), vectors_mejora.quat_w + (stride_d * metaheuristic.NEEImp), vectors_mejora.energy.energy + (stride_d * metaheuristic.NEEImp), vectors_mejora.energy.n_conformation + (stride_d * metaheuristic.NEEImp),metaheuristic, nconformations_count_partial,nconformations_count_partial*metaheuristic.NEEImp,orden_device,stride_d,fase);
	}
        else
        {
                //printf("No Trocea\n");
                //Lanzar con todo
		 multigpu_mejorar_gpusolver_vs_environment (vectors_e_s->nconformations,vectors_e_s->files,vectors_e_s->nlig,vectors_e_s->ligtype,vectors_e_s->bonds,vectors_e_s->ql,vectors_mejora.ligtype ,vectors_mejora.bonds,vectors_mejora.ql,ligand_params,proteina,vectors_e_s->conformations_x,vectors_e_s->conformations_y, vectors_e_s->conformations_z,param,vectors_mejora.weights, vectors_mejora.f_params, devices, vectors_e_s->move_x, vectors_e_s->move_y, vectors_e_s->move_z, vectors_e_s->quat_x, vectors_e_s->quat_y, vectors_e_s->quat_z, vectors_e_s->quat_w, vectors_e_s->energy.energy, vectors_e_s->energy.n_conformation, vectors_mejora.conformations_x , vectors_mejora.conformations_y, vectors_mejora.conformations_z, vectors_mejora.move_x, vectors_mejora.move_y, vectors_mejora.move_z, vectors_mejora.quat_x, vectors_mejora.quat_y, vectors_mejora.quat_z, vectors_mejora.quat_w, vectors_mejora.energy.energy, vectors_mejora.energy.n_conformation,metaheuristic, vectors_e_s->nconformations,vectors_mejora.nconformations,orden_device,0,fase);

	}
	free(vectors_mejora.conformations_x);
        free(vectors_mejora.conformations_y);
        free(vectors_mejora.conformations_z);
        free(vectors_mejora.move_x);
        free(vectors_mejora.move_y);
        free(vectors_mejora.move_z);
        free(vectors_mejora.quat_x);
        free(vectors_mejora.quat_y);
        free(vectors_mejora.quat_z);
        free(vectors_mejora.quat_w);
	free(vectors_mejora.ql);
	free(vectors_mejora.bonds);
	free(vectors_mejora.ligtype);
        free(vectors_mejora.energy.energy);
        free(vectors_mejora.energy.n_conformation);
        free(vectors_mejora.weights);
	//free(nlinks);
}

extern void multigpu_mejorar_gpusolver_vs_environment (unsigned int total_conformations, unsigned int files, unsigned int nlig, char *ligtype, unsigned int *bonds, type_data *ql, char *ligtype_mejora, unsigned int *bonds_mejora, type_data *ql_mejora, struct ligand_params_t *ligand_params, struct receptor_t proteina, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, struct param_t param, type_data *weights, struct force_field_param_t *f_params, struct device_t *devices, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *n_conformation,  type_data *conformations_x_mejora, type_data *conformations_y_mejora, type_data *conformations_z_mejora,  type_data *move_x_mejora, type_data *move_y_mejora, type_data *move_z_mejora, type_data *quat_x_mejora, type_data *quat_y_mejora, type_data *quat_z_mejora, type_data *quat_w_mejora, type_data *energy_mejora, unsigned int *n_conformation_mejora, struct metaheuristic_t metaheuristic, unsigned int nconformations, unsigned int nconformations_mejora, unsigned int orden_device, unsigned int stride, unsigned int fase)
{
        curandState_t *states_d_mejora;

        type_data *moves_x_d, *moves_y_d, *moves_z_d, *energy_d;
        type_data *quat_x_d, *quat_y_d, *quat_z_d, *quat_w_d, *quat_x_d_mejora, *quat_y_d_mejora, *quat_z_d_mejora, *quat_w_d_mejora;
        type_data *moves_x_d_mejora, *moves_y_d_mejora, *moves_z_d_mejora, *energy_d_mejora;
        unsigned int *energy_nconformation_d, *energy_nconformation_d_mejora;
        unsigned int *bonds_d, *bonds_d_mejora, *atoms, *atoms_d, *nlinks, *nlinks_d;
        char *ligtype_d, *ligtype_d_mejora;
        type_data *conformations_x_d, *conformations_y_d, *conformations_z_d, *ql_d, *ql_d_mejora;
	type_data *conformations_x_d_mejora, *conformations_y_d_mejora, *conformations_z_d_mejora;

        type_data *rec_x_d, *rec_y_d, *rec_z_d, *qr_d;
        char *rectype_d;
        unsigned int *bondsr_d;

        unsigned int steps, moveType=MOVE, total, simulations;
        cudaError_t cudaStatus;

        unsigned int moveSize = nconformations * sizeof(type_data);
        unsigned int confSize = nconformations * nlig * sizeof(type_data);
        unsigned int conf_point = total_conformations / files;

	unsigned int moveSize_environment = nconformations * sizeof(type_data);
        unsigned int confSize_environment = nconformations * nlig * sizeof(type_data);
        unsigned int conf_point_environment = (total_conformations * metaheuristic.NEEImp) / files;
        //printf("conf_point %d\n",conf_point);
        //type_data *e_tmp = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);

	nlinks = (unsigned int *)calloc(files,sizeof(unsigned int));
        atoms = (unsigned int *)malloc(sizeof(unsigned int)*files);
        for (unsigned int i=0;i<files;i++)atoms[i]=ligand_params[i].atoms;
        //printf("nconformations %d\n",vectors_e_s->nconformations);

        cudaSetDevice(devices->id[orden_device]);

	dataToGPU_multigpu_mejorar_inicializar_environment(nconformations, nconformations_mejora, energy_d, energy_nconformation_d, energy_d_mejora, energy_nconformation_d_mejora, moves_x_d, moves_y_d, moves_z_d, quat_x_d, quat_y_d, quat_z_d, quat_w_d, moves_x_d_mejora, moves_y_d_mejora, moves_z_d_mejora, quat_x_d_mejora,quat_y_d_mejora,quat_z_d_mejora,quat_w_d_mejora, states_d_mejora, move_x, move_y, move_z, quat_x, quat_y, quat_z, quat_w, energy, n_conformation, move_x_mejora, move_y_mejora, move_z_mejora, quat_x_mejora, quat_y_mejora, quat_z_mejora, quat_w_mejora, energy_mejora, n_conformation_mejora, metaheuristic);

	dataToGPUConformations_environment(nconformations,metaheuristic.NEEImp,nlig,conformations_x,conformations_x_d,conformations_y,conformations_y_d,conformations_z,conformations_z_d,conformations_x_mejora,conformations_x_d_mejora,conformations_y_mejora,conformations_y_d_mejora,conformations_z_mejora,conformations_z_d_mejora);

        commonDataToGPU_WS_vs (files,nconformations,nlig,proteina,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,atoms,atoms_d,nlinks,nlinks_d,ligtype,ligtype_d,bonds,bonds_d,ql,ql_d);
	commonDataToGPU_WS_vs_clb(nconformations_mejora,nlig,ligtype_mejora,bonds_mejora,ql_mejora,ligtype_d_mejora,bonds_d_mejora,ql_d_mejora);
        save_params(weights,f_params);
         //Inicializa el generador de numeros aleatorios y el cuaternion de cada simulacion
        //printf("Memoria reservada\n");
        //exit(0);

	setupCurandState <<<(nconformations_mejora/devices->hilos[orden_device*metaheuristic.num_kernels+4])+1, devices->hilos[orden_device*metaheuristic.num_kernels+4] >>>  (states_d_mejora, param.automatic_seed,param.seed,nconformations_mejora);

        // check if kernel execution generated and error
        cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");
        if (fase == 0)
                total = metaheuristic.IMEIni;
        if (fase == 1)
                total = metaheuristic.IMEImp;
        if (fase == 2)
                total = metaheuristic.IMEMUCom;
        steps = 0;
        moveType = MOVE;
        //printf("Voy a mejorar....\n");
	
	cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeFourByte);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_vs_by_warp, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_vs_by_warp_type1, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_vs_by_warp_type2, cudaFuncCachePreferEqual);

	 while (steps < total){

                if (moveType == MOVE)
                {
                        //printf("He movido\n");
                        move_mejora <<< (nconformations_mejora / devices->hilos[orden_device*metaheuristic.num_kernels+5])+1,devices->hilos[orden_device*metaheuristic.num_kernels+5] >>> (states_d_mejora,param.max_desp,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,nconformations_mejora);
                        moveType = ROTATE;
                        //exit(0);
                }
                else {
                        rotation <<< (nconformations_mejora /devices->hilos[orden_device*metaheuristic.num_kernels+2])+1, devices->hilos[orden_device*metaheuristic.num_kernels+2] >>> (states_d_mejora,param.rotation,quat_x_d_mejora,quat_y_d_mejora,quat_z_d_mejora,quat_w_d_mejora,nconformations_mejora);
                        moveType = MOVE;
                }
                //printf("Voy a calcular la energia...\n");

                unsigned int size = 5 * devices->hilos[orden_device*metaheuristic.num_kernels] * sizeof(type_data) + sizeof(char)*devices->hilos[orden_device*metaheuristic.num_kernels];
                unsigned int blk = ceil(nconformations_mejora*WARP_SIZE / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
                unsigned long int max_e = nconformations_mejora* WARP_SIZE;
                dim3 hilos_e (devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);
                dim3 grid_e  (ceil(blk/8)+1, 8);
		
		switch (param.scoring_function_type)
		{
			case 0:
				Gpu_full_Kernel_Conformations_vs_by_warp <<<grid_e,hilos_e,size>>> (proteina.nrec,conf_point_environment,atoms_d,nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d_mejora,conformations_y_d_mejora,conformations_z_d_mejora,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_x_d_mejora, quat_y_d_mejora, quat_z_d_mejora, quat_w_d_mejora,ql_d_mejora,ligtype_d_mejora,bonds_d_mejora,energy_d_mejora,energy_nconformation_d_mejora,stride*metaheuristic.NEEImp,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
				break;
			case 1:
				Gpu_full_Kernel_Conformations_vs_by_warp_type1 <<<grid_e,hilos_e,size>>> (nlinks_d,proteina.nrec,conf_point_environment,atoms_d,nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d_mejora,conformations_y_d_mejora,conformations_z_d_mejora,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_x_d_mejora, quat_y_d_mejora, quat_z_d_mejora, quat_w_d_mejora,ql_d_mejora,ligtype_d_mejora,bonds_d_mejora,energy_d_mejora,energy_nconformation_d_mejora,stride*metaheuristic.NEEImp,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
                                break;
			case 2:
				Gpu_full_Kernel_Conformations_vs_by_warp_type2 <<<grid_e,hilos_e,size>>> (nlinks_d,proteina.nrec,conf_point_environment,atoms_d,nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d_mejora,conformations_y_d_mejora,conformations_z_d_mejora,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_x_d_mejora, quat_y_d_mejora, quat_z_d_mejora, quat_w_d_mejora,ql_d_mejora,ligtype_d_mejora,bonds_d_mejora,energy_d_mejora,energy_nconformation_d_mejora,stride*metaheuristic.NEEImp,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
                                break;
		}
		
                cudaDeviceSynchronize();
                cudaStatus = cudaGetLastError();
                if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel energy failed! ERROR %d",cudaStatus);
		
		incluir_mejorar_environment <<<(nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+6])+1, devices->hilos[orden_device*metaheuristic.num_kernels+6]>>> (metaheuristic.NEEImp, moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_x_d_mejora,quat_y_d_mejora,quat_z_d_mejora,quat_w_d_mejora,energy_d_mejora,energy_nconformation_d_mejora,moves_x_d,moves_y_d,moves_z_d,quat_x_d,quat_y_d,quat_z_d,quat_w_d,energy_d,stride,nconformations);
                //fill_conformations_mejora <<<(nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+6])+1, devices->hilos[orden_device*metaheuristic.num_kernels+6]>>> (metaheuristic.NEEImp, moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_x_d_mejora,quat_y_d_mejora,quat_z_d_mejora,quat_w_d_mejora,energy_d_mejora,moves_x_d,moves_y_d,moves_z_d,quat_x_d,quat_y_d,quat_z_d,quat_w_d,energy_d,nconformations);
		steps++;
	}

	cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernels antes de copia fallo improve ini!\n");
        cudaStatus = cudaMemcpy(move_x, moves_x_d, moveSize, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_x_d_mejora GPUtoHost failed!");
        cudaStatus = cudaMemcpy(move_y, moves_y_d, moveSize, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_y_d_mejora GPUtoHost failed!");
        cudaStatus = cudaMemcpy(move_z, moves_z_d, moveSize, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_z_d_mejora GPUtoHost failed!");
        cudaStatus = cudaMemcpy(energy, energy_d, moveSize, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values energy_d_mejora GPUtoHost failed!");
        cudaStatus = cudaMemcpy(quat_x, quat_x_d, moveSize, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
        cudaStatus = cudaMemcpy(quat_y, quat_y_d, moveSize, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
        cudaStatus = cudaMemcpy(quat_z, quat_z_d, moveSize, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
        cudaStatus = cudaMemcpy(quat_w, quat_w_d, moveSize, cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
        cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) fprintf(stderr, "Copy failed!\n");

	free(atoms);
	free(nlinks);
	cudaFree(atoms_d);
	cudaFree(nlinks_d);
	cudaFree(states_d_mejora);
        cudaFree(moves_x_d);
        cudaFree(moves_y_d);
        cudaFree(moves_z_d);
        cudaFree(quat_x_d);
        cudaFree(quat_y_d);
        cudaFree(quat_z_d);
        cudaFree(quat_w_d);
        cudaFree(rec_x_d);
        cudaFree(rec_y_d);
        cudaFree(rec_z_d);
        cudaFree(qr_d);
        cudaFree(bondsr_d);
        cudaFree(rectype_d);
        cudaFree(ql_d);
        cudaFree(ligtype_d);
        cudaFree(bonds_d);
	cudaFree(ql_d_mejora);
        cudaFree(ligtype_d_mejora);
        cudaFree(bonds_d_mejora);
        cudaFree(energy_d);
        cudaFree(energy_d_mejora);
        cudaFree(energy_nconformation_d);
        cudaFree(energy_nconformation_d_mejora);
        cudaFree(moves_x_d_mejora);
        cudaFree(moves_y_d_mejora);
        cudaFree(moves_z_d_mejora);
        cudaFree(quat_x_mejora);
        cudaFree(quat_y_mejora);
        cudaFree(quat_z_mejora);
        cudaFree(quat_w_mejora);

        cudaFree(conformations_x_d);
        cudaFree(conformations_y_d);
        cudaFree(conformations_z_d);
	cudaFree(conformations_x_d_mejora);
        cudaFree(conformations_y_d_mejora);
        cudaFree(conformations_z_d_mejora);

        cudaDeviceReset();

}

extern void gpu_mejorar_vs (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct param_t param, struct vectors_t *vectors_e_s, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int orden_device, char *ligtype, unsigned int *bonds, type_data *ql, unsigned int fase)
{
	size_t available,total_m;
        cudaError_t cudaStatus;
        unsigned long int max_memory;
        unsigned long int max_positions;
        unsigned long int max_common;
        unsigned long int max_conf;
        unsigned long int typeSize = proteina.nrec * sizeof(char);
        unsigned long int recSize = proteina.nrec * sizeof(type_data);
        unsigned long int bondSize = proteina.nrec * sizeof(unsigned int) * MAXBOND;
        unsigned long int randSize = vectors_e_s->nconformations * sizeof(curandState_t);
        unsigned long int moveSize_tam = vectors_e_s->nconformations * sizeof(type_data);
        unsigned long int energyConf_tam = vectors_e_s->nconformations * sizeof(unsigned int);
        unsigned long int confSize_tam = vectors_e_s->nconformations * vectors_e_s->nlig * sizeof(type_data);
        unsigned long int confSize_tam_char = vectors_e_s->nconformations * vectors_e_s->nlig * sizeof(char);
        unsigned long int confSize_bonds = vectors_e_s->nconformations * vectors_e_s->nlig * MAXBOND *  sizeof(unsigned int);
        unsigned long int tam_max, tam_tmp, nconformations_count_total, nconformations_count_partial, tam_conformation, stride_d;

        max_positions = (moveSize_tam * 8) * 2 + energyConf_tam * 2;
        max_conf = (confSize_tam * 3) + randSize + confSize_tam + confSize_tam_char + confSize_bonds;
        max_common = (recSize * 4) + typeSize + bondSize;

        max_memory = max_positions + max_conf + max_common;

        tam_conformation =  (sizeof(type_data) * 8 * 2) + sizeof(unsigned int) * 2 + (sizeof(type_data) * vectors_e_s->nlig * 3) +  sizeof(curandState_t) + vectors_e_s->nlig * sizeof(type_data) +  vectors_e_s->nlig * sizeof(char) +  vectors_e_s->nlig * MAXBOND *  sizeof(unsigned int);

	cudaSetDevice(devices->id[orden_device]);
        //cudaDeviceReset();
        cudaMemGetInfo(&tam_max, &total_m);
        cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
        //std::cout << tam_max << " " << total_m << "\n";
        tam_tmp = tam_max - (tam_max * 0.2);
        if (tam_tmp < max_memory)
        {
                //printf("Trocear en 1 GPU\n");
                max_memory = max_common;
                nconformations_count_total = 0;
                nconformations_count_partial = 0;
                stride_d = 0;
                while (nconformations_count_total < vectors_e_s->nconformations)
                {
                        max_memory += tam_conformation;
                        if (max_memory > tam_tmp)
                        {
                                //printf("Trozo %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
                                //nconformations_count_total--;
                                //nconformations_count_partial--;
                                //Lanzar con nconformations_count_partial;
                              
				multigpu_mejorar_gpusolver_vs (vectors_e_s->nconformations,vectors_e_s->files,vectors_e_s->nlig,ligtype+(stride_d*vectors_e_s->nlig),bonds+(stride_d*vectors_e_s->nlig*MAXBOND),ql+(stride_d*vectors_e_s->nlig),ligand_params,proteina,vectors_e_s->conformations_x+(stride_d*vectors_e_s->nlig),vectors_e_s->conformations_y+(stride_d*vectors_e_s->nlig),vectors_e_s->conformations_z+(stride_d*vectors_e_s->nlig),param,vectors_e_s->weights,vectors_e_s->f_params,devices,vectors_e_s->move_x+stride_d,vectors_e_s->move_y+stride_d,vectors_e_s->move_z+stride_d,vectors_e_s->quat_x+stride_d,vectors_e_s->quat_y+stride_d,vectors_e_s->quat_z+stride_d,vectors_e_s->quat_w+stride_d,vectors_e_s->energy.energy+stride_d,vectors_e_s->energy.n_conformation+stride_d,metaheuristic,nconformations_count_partial,orden_device,stride_d,fase);
 
                                stride_d += nconformations_count_partial;
                                max_memory = max_common;
                                nconformations_count_partial = 0;
                        }
                        else
                        {
                                nconformations_count_total++;
                                nconformations_count_partial++;
                        }
                }
                //printf("Trozo final %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
		nconformations_count_partial--;
                 multigpu_mejorar_gpusolver_vs (vectors_e_s->nconformations,vectors_e_s->files,vectors_e_s->nlig,ligtype+(stride_d*vectors_e_s->nlig),bonds+(stride_d*vectors_e_s->nlig*MAXBOND),ql+(stride_d*vectors_e_s->nlig),ligand_params,proteina,vectors_e_s->conformations_x+(stride_d*vectors_e_s->nlig),vectors_e_s->conformations_y+(stride_d*vectors_e_s->nlig),vectors_e_s->conformations_z+(stride_d*vectors_e_s->nlig),param,vectors_e_s->weights,vectors_e_s->f_params,devices,vectors_e_s->move_x+stride_d,vectors_e_s->move_y+stride_d,vectors_e_s->move_z+stride_d,vectors_e_s->quat_x+stride_d,vectors_e_s->quat_y+stride_d,vectors_e_s->quat_z+stride_d,vectors_e_s->quat_w+stride_d,vectors_e_s->energy.energy+stride_d,vectors_e_s->energy.n_conformation+stride_d,metaheuristic,nconformations_count_partial,orden_device,stride_d,fase);
        }

        else
        {
                //printf("No Trocea\n");
                //Lanzar con todo
                 multigpu_mejorar_gpusolver_vs (vectors_e_s->nconformations,vectors_e_s->files,vectors_e_s->nlig,ligtype,bonds,ql,ligand_params,proteina,vectors_e_s->conformations_x,vectors_e_s->conformations_y,vectors_e_s->conformations_z,param,vectors_e_s->weights,vectors_e_s->f_params,devices,vectors_e_s->move_x,vectors_e_s->move_y,vectors_e_s->move_z,vectors_e_s->quat_x,vectors_e_s->quat_y,vectors_e_s->quat_z,vectors_e_s->quat_w,vectors_e_s->energy.energy,vectors_e_s->energy.n_conformation,metaheuristic,vectors_e_s->nconformations,orden_device,0,fase); 
        }
        //exit(0);

}

extern void multigpu_mejorar_gpusolver_vs (unsigned int total_conformations, unsigned int files, unsigned int nlig, char *ligtype, unsigned int *bonds, type_data *ql, struct ligand_params_t *ligand_params, struct receptor_t proteina, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, struct param_t param, type_data *weights, struct force_field_param_t *f_params, struct device_t *devices, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *n_conformations, struct metaheuristic_t metaheuristic, unsigned int nconformations, unsigned int orden_device, unsigned int stride, unsigned int fase)
{
	curandState_t *states_d;
	
	type_data *moves_x_d, *moves_y_d, *moves_z_d, *energy_d;		
	type_data *quat_d_x, *quat_d_y, *quat_d_z, *quat_d_w, *quat_d_mejora_x, *quat_d_mejora_y, *quat_d_mejora_z, *quat_d_mejora_w;
	type_data *moves_x_d_mejora, *moves_y_d_mejora, *moves_z_d_mejora, *energy_d_mejora;
	unsigned int *energy_nconformation_d, *energy_nconformation_d_mejora;		
	unsigned int *bonds_d, *atoms, *atoms_d, *nlinks, *nlinks_d;
	char *ligtype_d;
	type_data *conformations_x_d, *conformations_y_d, *conformations_z_d, *ql_d;
	
	type_data *rec_x_d, *rec_y_d, *rec_z_d, *qr_d;
	char *rectype_d;
	unsigned int *bondsr_d;
	
	unsigned int steps, moveType=MOVE, total, simulations;
	cudaError_t cudaStatus;
	
	unsigned int moveSize = nconformations * sizeof(type_data);
	unsigned int confSize = nconformations * nlig * sizeof(type_data);
	unsigned int conf_point = total_conformations / files;
	//printf("conf_point %d\n",conf_point);
	//type_data *e_tmp = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);

	nlinks = (unsigned int *)calloc(files,sizeof(unsigned int));
	atoms = (unsigned int *)malloc(sizeof(unsigned int)*files);
	for (unsigned int i=0;i<files;i++)atoms[i]=ligand_params[i].atoms;
	//printf("nconformations %d\n",vectors_e_s->nconformations);
	
	cudaSetDevice(devices->id[orden_device]);		

	dataToGPU_multigpu_mejorar_inicializar(nconformations, energy_d, energy_nconformation_d, energy_d_mejora, energy_nconformation_d_mejora, moves_x_d, moves_y_d, moves_z_d, quat_d_x, quat_d_y, quat_d_z, quat_d_w, moves_x_d_mejora, moves_y_d_mejora, moves_z_d_mejora, quat_d_mejora_x, quat_d_mejora_y, quat_d_mejora_z, quat_d_mejora_w, states_d, move_x, move_y, move_z, quat_x, quat_y, quat_z, quat_w, energy, n_conformations, metaheuristic);
	dataToGPUConformations(nconformations,nlig,conformations_x,conformations_x_d,conformations_y,conformations_y_d,conformations_z,conformations_z_d);
	
	commonDataToGPU_WS_vs (files,nconformations,nlig,proteina,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,atoms,atoms_d,nlinks,nlinks_d,ligtype,ligtype_d,bonds,bonds_d,ql,ql_d);
	save_params(weights,f_params);
	 //Inicializa el generador de numeros aleatorios y el cuaternion de cada simulacion
	//printf("Memoria reservada\n");
	//exit(0);
	
	setupCurandState <<<(nconformations/devices->hilos[orden_device*metaheuristic.num_kernels+4])+1, devices->hilos[orden_device*metaheuristic.num_kernels+4] >>>  (states_d, param.automatic_seed,param.seed,nconformations);

	// check if kernel execution generated and error
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");
	if (fase == 0)
		total = metaheuristic.IMEIni;
	if (fase == 1)
		total = metaheuristic.IMEImp;
	if (fase == 2)
		total = metaheuristic.IMEMUCom;
	steps = 0;
	moveType = MOVE;
	//printf("Voy a mejorar....\n");

	cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeFourByte);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_vs_by_warp, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_vs_by_warp_type1, cudaFuncCachePreferEqual);
        cudaFuncSetCacheConfig(Gpu_full_Kernel_Conformations_vs_by_warp_type2, cudaFuncCachePreferEqual);


	while (steps < total){		

		if (moveType == MOVE)
		{
			//printf("He movido\n");
			move_mejora <<< (nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+5])+1,devices->hilos[orden_device*metaheuristic.num_kernels+5] >>> (states_d,param.max_desp,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,nconformations);
			moveType = ROTATE;
			//exit(0);
		}
		else {
			rotation <<< (nconformations /devices->hilos[orden_device*metaheuristic.num_kernels+2])+1, devices->hilos[orden_device*metaheuristic.num_kernels+2] >>> (states_d,param.rotation,quat_d_mejora_x,quat_d_mejora_y,quat_d_mejora_z,quat_d_mejora_w,nconformations);
			moveType = MOVE;
		}
		//printf("Voy a calcular la energia...\n");
		
		unsigned int size = 5 * devices->hilos[orden_device*metaheuristic.num_kernels] * sizeof(type_data) + sizeof(char)*devices->hilos[orden_device*metaheuristic.num_kernels];	
		unsigned int blk = ceil(nconformations*WARP_SIZE / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
		unsigned long int max_e = nconformations* WARP_SIZE;	
		dim3 hilos_e (devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);
		dim3 grid_e  (ceil(blk/8)+1, 8);		              
		
		switch (param.scoring_function_type)
                {
                        case 0:
				Gpu_full_Kernel_Conformations_vs_by_warp <<<grid_e,hilos_e,size>>> (proteina.nrec,conf_point,atoms_d,nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d,conformations_y_d,conformations_z_d,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_d_mejora_x, quat_d_mejora_y, quat_d_mejora_z, quat_d_mejora_w,ql_d,ligtype_d,bonds_d,energy_d_mejora,energy_nconformation_d,stride,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
				break;
			case 1:
				Gpu_full_Kernel_Conformations_vs_by_warp_type1 <<<grid_e,hilos_e,size>>> (nlinks_d,proteina.nrec,conf_point,atoms_d,nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d,conformations_y_d,conformations_z_d,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_d_mejora_x, quat_d_mejora_y, quat_d_mejora_z, quat_d_mejora_w,ql_d,ligtype_d,bonds_d,energy_d_mejora,energy_nconformation_d,stride,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
                                break;
			case 2:
				Gpu_full_Kernel_Conformations_vs_by_warp_type2 <<<grid_e,hilos_e,size>>> (nlinks_d,proteina.nrec,conf_point,atoms_d,nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,conformations_x_d,conformations_y_d,conformations_z_d,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_d_mejora_x, quat_d_mejora_y, quat_d_mejora_z, quat_d_mejora_w,ql_d,ligtype_d,bonds_d,energy_d_mejora,energy_nconformation_d,stride,devices->hilos[orden_device*metaheuristic.num_kernels],max_e);
                                break;
		}

		cudaDeviceSynchronize();
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel energy failed!");
		incluir_mejorar <<<(nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+6])+1, devices->hilos[orden_device*metaheuristic.num_kernels+6]>>> (moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_d_mejora_x,quat_d_mejora_y,quat_d_mejora_z,quat_d_mejora_w,energy_d_mejora,energy_nconformation_d_mejora,moves_x_d,moves_y_d,moves_z_d,quat_d_x, quat_d_y, quat_d_z, quat_d_w,energy_d,stride,nconformations);	
		steps++;	
	}	
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernels antes de copia fallo improve ini!\n");	
	cudaStatus = cudaMemcpy(move_x, moves_x_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_x_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(move_y, moves_y_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_y_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(move_z, moves_z_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_z_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(energy, energy_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values energy_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_x, quat_d_x, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_y, quat_d_y, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_z, quat_d_z, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_w, quat_d_w, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Copy failed!\n");	

	free(atoms);
	free(nlinks);
	cudaFree(states_d);
	cudaFree(moves_x_d);
	cudaFree(moves_y_d);
	cudaFree(moves_z_d);
	cudaFree(quat_d_x);
	cudaFree(quat_d_y);
	cudaFree(quat_d_z);
	cudaFree(quat_d_w);
	cudaFree(rec_x_d);
	cudaFree(rec_y_d);
	cudaFree(rec_z_d);
	cudaFree(qr_d);
	cudaFree(bondsr_d);
	cudaFree(rectype_d);
	cudaFree(ql_d);
	cudaFree(ligtype_d);
	cudaFree(bonds_d);
	cudaFree(energy_d);
	cudaFree(energy_d_mejora);
	cudaFree(energy_nconformation_d);
	cudaFree(energy_nconformation_d_mejora);
	cudaFree(moves_x_d_mejora);
	cudaFree(moves_y_d_mejora);
	cudaFree(moves_z_d_mejora);
	cudaFree(quat_d_mejora_x);
	cudaFree(quat_d_mejora_y);
	cudaFree(quat_d_mejora_z);
	cudaFree(quat_d_mejora_w);
	
	cudaFree(conformations_x_d);
	cudaFree(conformations_y_d);
	cudaFree(conformations_z_d);

	cudaDeviceReset();
}


extern void generate_positions_vs (struct param_t param, struct vectors_t *vectors_e_s, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int orden_device, unsigned int n) 
{
	size_t available,total_m;
    cudaError_t cudaStatus;
    unsigned long int max_memory;
    unsigned long int max_positions;
    unsigned long int max_common;
    
    unsigned long int randSize = vectors_e_s->nconformations * sizeof(curandState_t);
	unsigned long int surfaceSize = vectors_e_s->num_surface * sizeof(type_data);
	
    unsigned long int moveSize_tam = vectors_e_s->nconformations * sizeof(type_data);
    //unsigned long int confSize_tam = vectors_e_s->nconformations * ligando.nlig * sizeof(type_data);
    unsigned long int tam_max, tam_tmp, nconformations_count_total, nconformations_count_partial, tam_conformation, stride_d;
    

    max_positions = moveSize_tam * 7 + randSize;
    max_common = surfaceSize * 3;

    max_memory = max_positions + max_common;
    tam_conformation =  (sizeof(type_data) * 7) + sizeof(curandState_t);

	cudaSetDevice(devices->id[orden_device]);
    //cudaDeviceReset();
    cudaMemGetInfo(&tam_max, &total_m);
    cudaDeviceSynchronize();
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) printf("Error en memGetInfo\n");
    //std::cout << tam_max << " " << total_m << "\n";
    tam_tmp = tam_max - (tam_max * 0.2);

	if (tam_tmp < max_memory)
        {
                //printf("Trocear en 1 GPU\n");
                max_memory = max_common;
                nconformations_count_total = 0;
                nconformations_count_partial = 0;
                stride_d = 0;
                while (nconformations_count_total < vectors_e_s->nconformations)
                {
                        max_memory += tam_conformation;
                        if (max_memory > tam_tmp)
                        {
                                //printf("Trozo %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
                                //nconformations_count_total--;
                                //nconformations_count_partial--;
                                //Lanzar con nconformations_count_partial;
				generate_positions_calculation_vs (vectors_e_s->nlig,param,nconformations_count_partial,vectors_e_s->move_x+stride_d,vectors_e_s->move_y+stride_d,vectors_e_s->move_z+stride_d,vectors_e_s->quat_x+stride_d,vectors_e_s->quat_y+stride_d,vectors_e_s->quat_z+stride_d,vectors_e_s->quat_w+stride_d,vectors_e_s->ac_x,vectors_e_s->ac_y,vectors_e_s->ac_z,metaheuristic,devices,orden_device,0);

                                stride_d += nconformations_count_partial;
                                max_memory = max_common;
                                nconformations_count_partial = 0;
                        }
                        else
                        {
                                nconformations_count_total++;
                                nconformations_count_partial++;
                        }
                }
                //printf("Trozo final %u %u %u %u %d\n",nconformations_count_total, nconformations_count_partial, max_memory,stride_d, vectors_e_s->nconformations);
		nconformations_count_partial--;
		generate_positions_calculation_vs (vectors_e_s->nlig,param,nconformations_count_partial,vectors_e_s->move_x+stride_d,vectors_e_s->move_y+stride_d,vectors_e_s->move_z+stride_d,vectors_e_s->quat_x+stride_d,vectors_e_s->quat_y+stride_d,vectors_e_s->quat_z+stride_d,vectors_e_s->quat_w+stride_d,vectors_e_s->ac_x,vectors_e_s->ac_y,vectors_e_s->ac_z,metaheuristic,devices,orden_device,0);
        }

        else
        {
                //printf("No Trocea\n");
                //Lanzar con todo
		generate_positions_calculation_vs (vectors_e_s->nlig,param,vectors_e_s->nconformations,vectors_e_s->move_x,vectors_e_s->move_y,vectors_e_s->move_z,vectors_e_s->quat_x,vectors_e_s->quat_y,vectors_e_s->quat_z,vectors_e_s->quat_w,vectors_e_s->ac_x,vectors_e_s->ac_y,vectors_e_s->ac_z,metaheuristic,devices,orden_device,0);
        }	


}

extern void generate_positions_calculation_vs (unsigned int nlig, struct param_t param, unsigned int nconformations, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data ac_x, type_data ac_y, type_data ac_z, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int orden_device, unsigned int n) {

	curandState_t *states_d;
	type_data *moves_x_d, *moves_y_d, *moves_z_d;		
	type_data *quat_d_x, *quat_d_y, *quat_d_z, *quat_d_w;
	unsigned int steps, moveType=MOVE,total,inicial;
	cudaError_t cudaStatus;
	unsigned long int moveSize = nconformations * sizeof(type_data);
	cudaSetDevice(devices->id[orden_device]);
	
	dataToGPU_vs(nconformations, moves_x_d, moves_y_d, moves_z_d, quat_d_x, quat_d_y, quat_d_z, quat_d_w, states_d);
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Error en memoria.\n");

     //Inicializa el generador de numeros aleatorios y el cuaternion de cada simulacion
	setupCurandState <<< (nconformations/devices->hilos[orden_device*metaheuristic.num_kernels+4])+1, devices->hilos[orden_device*metaheuristic.num_kernels+4] >>>  (states_d, param.automatic_seed,param.seed,nconformations);

	// check if kernel execution generated and error
	cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");

	setup <<< (nconformations /devices->hilos[orden_device*metaheuristic.num_kernels+3])+1, devices->hilos[orden_device*metaheuristic.num_kernels+3] >>> (quat_d_x, quat_d_y, quat_d_z, quat_d_w,nconformations);

	// check if kernel execution generated and error
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");
	steps = 0;	
	if (n == 0)
	{
		total = 20; 
		inicial = metaheuristic.NEIIni;
		//printf("inicial %d\n",metaheuristic.NEIIni);
	}
	else 
	{	
		total = param.steps_warm_up_gpu;
		inicial = 16;
	}
	while (steps < total)
	{
		if (moveType == MOVE)
		{
			move_vs <<< (nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+1])+1,devices->hilos[orden_device*metaheuristic.num_kernels+1] >>> (states_d, param.max_desp, moves_x_d, moves_y_d, moves_z_d, ac_x, ac_y, ac_z, nconformations);
			moveType = ROTATE;
		}
		else
		{
			rotation <<< (nconformations /devices->hilos[orden_device*metaheuristic.num_kernels+2])+1, devices->hilos[orden_device*metaheuristic.num_kernels+2] >>> (states_d, param.rotation, quat_d_x, quat_d_y, quat_d_z, quat_d_w,nconformations);
			moveType = MOVE;
		}
	
	// check if kernel execution generated and erro
		cudaDeviceSynchronize();
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel move o rotation execution failed \n");
		steps++;
		//printf("Steps %d\n",steps);
	}

	cudaStatus = cudaMemcpy(move_x, moves_x_d, moveSize, cudaMemcpyDeviceToHost);	
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed1!");
	cudaStatus = cudaMemcpy(move_y, moves_y_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(move_z, moves_z_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	cudaStatus = cudaMemcpy(quat_x, quat_d_x, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_y, quat_d_y, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_z, quat_d_z, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(quat_w, quat_d_w, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");

	cudaFree(states_d);
	cudaFree(moves_x_d);
	cudaFree(moves_y_d);
	cudaFree(moves_z_d);
	cudaFree(quat_d_x);
	cudaFree(quat_d_y);
	cudaFree(quat_d_z);
	cudaFree(quat_d_w);


}

