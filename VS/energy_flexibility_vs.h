#ifndef ENERGY_FLEXIBILITY_VS_H
#define ENERGY_FLEXIBILITY_VS_H

#include "energy_cuda.h"
#include "type.h"
extern __device__ void rotate3D_flexibility_vs (type_data  q_x, type_data  q_y, type_data  q_z, type_data  q_w, type_data* src, type_data * dst);
extern __device__ void setRotation_flexibility_vs (type_data *q_x, type_data *q_y, type_data *q_z, type_data *q_w, type_data angle, type_data * vector);
extern __device__ void rotateLigandFragment_vs(int *f, unsigned int size_f, type_data* lig_x, type_data* lig_y, type_data* lig_z, type_data qeje_x, type_data qeje_y, type_data qeje_z, type_data qeje_w, type_data p1[3]);

__global__ void flexibilityKernel_vs (type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, curandState_t * states, struct flexibility_data_t * flexibility_conformations, struct flexibility_params_t *flexibility_params, unsigned int nlig,unsigned int max_angle_flex, unsigned int stride_d, unsigned int conf_point, unsigned int max);
extern void flexibility_function_vs (struct ligand_params_t *ligand_params, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, struct flexibility_data_t * flexibility_conformations,struct flexibility_params_t *flexibility_params, unsigned int nconformations, unsigned int orden_device);

#endif
