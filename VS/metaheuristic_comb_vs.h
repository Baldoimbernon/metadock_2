#ifndef __METAHEU_COMB_VS_H__
#define __METAHEU_COMB_VS_H__

extern void combinar_group_vs (unsigned int nlig, type_data *e_move_x, type_data *e_move_y, type_data *e_move_z, type_data *e_quat_x, type_data *e_quat_y, type_data *e_quat_z, type_data *e_quat_w, type_data *s_move_x, type_data *s_move_y, type_data *s_move_z, type_data *s_quat_x, type_data *s_quat_y, type_data *s_quat_z, type_data *s_quat_w , struct metaheuristic_t metaheuristic, unsigned int mejores, unsigned int peores, unsigned int mejorespeores, unsigned int stride_e, unsigned int stride_s, struct param_t param);
extern void combinar_vs (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param);
extern void combinar_gpu_vs (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param, struct device_t *devices, unsigned int orden_device);
extern void combinar_multigpu_vs (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param, struct device_t *devices);


#endif
