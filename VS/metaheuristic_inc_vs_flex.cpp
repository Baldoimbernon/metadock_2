#include "energy_common.h"
#include "energy_common_vs.h"
#include <thrust/sequence.h>
#include <thrust/sort.h>
#include <thrust/functional.h>
#include <omp.h>

extern void colocar_vs_flex_by_step (struct param_t param, struct metaheuristic_t metaheuristic, unsigned int nlig,  type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int stride, unsigned int pos)
{
	struct vectors_t *vtmp = (struct vectors_t *)malloc(sizeof(struct vectors_t));
	unsigned int nc,nc_conf_o,nc_conf_d;

	vtmp->nconformations = stride;
	vtmp->conformations_x = (type_data *)malloc(sizeof(type_data)*nlig*stride);
	vtmp->conformations_y = (type_data *)malloc(sizeof(type_data)*nlig*stride);
	vtmp->conformations_z = (type_data *)malloc(sizeof(type_data)*nlig*stride);
	vtmp->move_x = (type_data *)malloc(sizeof(type_data)*stride);
	vtmp->move_y = (type_data *)malloc(sizeof(type_data)*stride);
        vtmp->move_z = (type_data *)malloc(sizeof(type_data)*stride);
        vtmp->quat_x = (type_data *)malloc(sizeof(type_data)*stride);
        vtmp->quat_y = (type_data *)malloc(sizeof(type_data)*stride);
        vtmp->quat_z = (type_data *)malloc(sizeof(type_data)*stride);
        vtmp->quat_w = (type_data *)malloc(sizeof(type_data)*stride);
        vtmp->energy.energy = (type_data *)malloc(sizeof(type_data)*stride);

	for (unsigned int i=0; i < stride;i++)
        {
                nc = nconfs[i] - (pos * stride);
                nc_conf_o = nc * nlig;
                nc_conf_d = i * nlig;

                memcpy(vtmp->conformations_x + nc_conf_d,conformations_x + nc_conf_o, nlig * sizeof(type_data));
                memcpy(vtmp->conformations_y + nc_conf_d,conformations_y + nc_conf_o, nlig * sizeof(type_data));
                memcpy(vtmp->conformations_z + nc_conf_d,conformations_z + nc_conf_o, nlig * sizeof(type_data));

                vtmp->move_x[i] = move_x[nc];
                vtmp->move_y[i] = move_y[nc];
                vtmp->move_z[i] = move_z[nc];
                vtmp->quat_x[i] = quat_x[nc];
                vtmp->quat_y[i] = quat_y[nc];
                vtmp->quat_z[i] = quat_z[nc];
                vtmp->quat_w[i] = quat_w[nc];
                vtmp->energy.energy[i] = energy[i];
        }
	memcpy(conformations_x, vtmp->conformations_x, stride * nlig * sizeof(type_data));
        memcpy(conformations_y, vtmp->conformations_y, stride * nlig * sizeof(type_data));
        memcpy(conformations_z, vtmp->conformations_z, stride * nlig * sizeof(type_data));
        memcpy(move_x, vtmp->move_x, stride * sizeof(type_data));
        memcpy(move_y, vtmp->move_y, stride * sizeof(type_data));
        memcpy(move_z, vtmp->move_z, stride * sizeof(type_data));
        memcpy(quat_x, vtmp->quat_x, stride * sizeof(type_data));
        memcpy(quat_y, vtmp->quat_y, stride * sizeof(type_data));
        memcpy(quat_z, vtmp->quat_z, stride * sizeof(type_data));
        memcpy(quat_w, vtmp->quat_w, stride * sizeof(type_data));
        memcpy(energy, vtmp->energy.energy, stride * sizeof(type_data));
        //thrust::sequence(nconfs,nconfs + vectors_e_s->stridenconformations);
        //printf("VTMP DESPUES DE COPIAR A DESTINO\n");
        //for (int i=0;i<64;i++) printf("conf %d, xyz %f %f %f \n",i,vectors_e_s->conformations_x[i],vectors_e_s->conformations_y[i], vectors_e_s->conformations_z[i]);

        free(vtmp->conformations_x);
        free(vtmp->conformations_y);
        free(vtmp->conformations_z);
        free(vtmp->move_x);
        free(vtmp->move_y);
        free(vtmp->move_z);
        free(vtmp->quat_x);
        free(vtmp->quat_y);
        free(vtmp->quat_z);
        free(vtmp->quat_w);
        free(vtmp->energy.energy);
        //exit(0);

 }

/*extern void colocar_vs_flex (struct param_t param, struct vectors_t *vectors_e_s, struct metaheuristic_t metaheuristic)
{
	struct vectors_t *vtmp = (struct vectors_t *)malloc(sizeof(struct vectors_t));
	unsigned int nc,nc_conf_o,nc_conf_d;
	unsigned int h1;
	
	vtmp->files = vectors_e_s->files;
	vtmp->nlig = vectors_e_s->nlig;
	vtmp->nconformations = vectors_e_s->nconformations;
	vtmp->conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nlig*vectors_e_s->nconformations);
	vtmp->conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nlig*vectors_e_s->nconformations);
	vtmp->conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nlig*vectors_e_s->nconformations);
	vtmp->move_x = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);
	vtmp->move_y = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);
	vtmp->move_z = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);
	vtmp->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);	
	vtmp->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);	
	vtmp->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);	
	vtmp->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);		
	vtmp->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_e_s->nconformations);
	switch (param.mode) {		
		case 1:
			h1 = 1;
			break;
		case 0:
		case 3:
		case 2:
			h1 = metaheuristic.Threads1Inc;
			break;
	}	
	omp_set_num_threads (h1);
	#pragma omp parallel for private (nc,nc_conf_d,nc_conf_o)		
	for (unsigned int i=0; i<vectors_e_s->nconformations;i++)
	{
		nc = vectors_e_s->energy.n_conformation[i];
		nc_conf_o = nc * vectors_e_s->nlig;				
		nc_conf_d = i * vectors_e_s->nlig;				
		
		memcpy(vtmp->conformations_x + nc_conf_d,vectors_e_s->conformations_x + nc_conf_o, vectors_e_s->nlig * sizeof(type_data));
		memcpy(vtmp->conformations_y + nc_conf_d,vectors_e_s->conformations_y + nc_conf_o, vectors_e_s->nlig * sizeof(type_data));
		memcpy(vtmp->conformations_z + nc_conf_d,vectors_e_s->conformations_z + nc_conf_o, vectors_e_s->nlig * sizeof(type_data));
	
		vtmp->move_x[i] = vectors_e_s->move_x[nc];
		vtmp->move_y[i] = vectors_e_s->move_y[nc];
		vtmp->move_z[i] = vectors_e_s->move_z[nc];
		vtmp->quat_x[i] = vectors_e_s->quat_x[nc];
		vtmp->quat_y[i] = vectors_e_s->quat_y[nc];
		vtmp->quat_z[i] = vectors_e_s->quat_z[nc];
		vtmp->quat_w[i] = vectors_e_s->quat_w[nc];
		vtmp->energy.energy[i] = vectors_e_s->energy.energy[i];
	}

	//printf("%f %f %f\n",vtmp->move_x[0],vtmp->move_y[0],vtmp->move_z[0]);
	//printf("VTMP ANTES DE COPIAR A DESTINO\n");
	//for (int i=0;i<64;i++) printf("conf %d, xyz %f %f %f \n",i,vtmp->conformations_x[i],vtmp->conformations_y[i], vtmp->conformations_z[i]);
//	for (int i=0;i<64;i++) printf("conf %d, xyz %f %f %f m %f %f %f, q %f %f %f, energy %f %d\n",i,vtmp->conformations_x[i],vtmp->conformations_y[i], vtmp->conformations_z[i],vtmp->move_x[i/vtmp->nlig],vtmp->move_y[i/vtmp->nlig],vtmp->move_z[i/vtmp->nlig],vtmp->quat_x[i/vtmp->nlig],vtmp->quat_y[i/vtmp->nlig],vtmp->quat_z[i/vtmp->nlig],vtmp->energy.energy[i/vtmp->nlig],vtmp->energy.n_conformation[i/vtmp->nlig]);
		
	memcpy_type_data(vectors_e_s->conformations_x, vtmp->conformations_x, vectors_e_s->nconformations * vectors_e_s->nlig);// * sizeof(type_data));  	
	memcpy_type_data(vectors_e_s->conformations_y, vtmp->conformations_y, vectors_e_s->nconformations * vectors_e_s->nlig);// * sizeof(type_data));  	
	memcpy_type_data(vectors_e_s->conformations_z, vtmp->conformations_z, vectors_e_s->nconformations * vectors_e_s->nlig);// * sizeof(type_data)); 	
	memcpy_type_data(vectors_e_s->move_x, vtmp->move_x, vectors_e_s->nconformations);// * sizeof(type_data));  	
	memcpy_type_data(vectors_e_s->move_y, vtmp->move_y, vectors_e_s->nconformations);// * sizeof(type_data)); 
	memcpy_type_data(vectors_e_s->move_z, vtmp->move_z, vectors_e_s->nconformations);// * sizeof(type_data));
	memcpy_type_data(vectors_e_s->quat_x, vtmp->quat_x, vectors_e_s->nconformations);// * sizeof(type_data));
	memcpy_type_data(vectors_e_s->quat_y, vtmp->quat_y, vectors_e_s->nconformations);// * sizeof(type_data));
	memcpy_type_data(vectors_e_s->quat_z, vtmp->quat_z, vectors_e_s->nconformations);// * sizeof(type_data));
	memcpy_type_data(vectors_e_s->quat_w, vtmp->quat_w, vectors_e_s->nconformations);// * sizeof(type_data));
	memcpy_type_data(vectors_e_s->energy.energy, vtmp->energy.energy, vectors_e_s->nconformations); 
	thrust::sequence(vectors_e_s->energy.n_conformation,vectors_e_s->energy.n_conformation + vectors_e_s->nconformations);	
	//printf("VTMP DESPUES DE COPIAR A DESTINO\n");
	//for (int i=0;i<64;i++) printf("conf %d, xyz %f %f %f \n",i,vectors_e_s->conformations_x[i],vectors_e_s->conformations_y[i], vectors_e_s->conformations_z[i]);
	
	free(vtmp->conformations_x);
	free(vtmp->conformations_y);
	free(vtmp->conformations_z);
	free(vtmp->move_x);
	free(vtmp->move_y);
	free(vtmp->move_z);
	free(vtmp->quat_x);
	free(vtmp->quat_y);
	free(vtmp->quat_z);
	free(vtmp->quat_w);
	free(vtmp->energy.energy);
	//exit(0);	
}*/
