#include <omp.h>
#include "energy_CpuSolver.h"
#include "energy_CpuSolver_common.h"
#include "energy_common.h"
#include "rotation.h"

#define PI 3.141592653589793

extern void rotate3D_flexibility_cpp_vs (type_data  q_x, type_data  q_y, type_data  q_z, type_data  q_w, type_data* src, type_data * dst){

	type_data x,y,z;

	x = ((q_w*q_w + q_x*q_x - q_y*q_y - q_z*q_z)*src[0]) + (2*(q_x*q_y - q_w*q_z)*src[1]) + (2*(q_x*q_z + q_w*q_y)*src[2]);
	y = (2*(q_y*q_x + q_w*q_z))*src[0] + ((q_w * q_w - q_x*q_x + q_y*q_y - q_z*q_z)*src[1]) + (2*(q_y*q_z - q_w*q_x)*src[2]);
	z = (2*(q_z*q_x - q_w*q_y))*src[0] + (2*(q_z*q_y + q_w*q_x)*src[1]) + ((q_w*q_w - q_x*q_x - q_y*q_y + q_z*q_z)*src[2]);
	dst[0] = x;
 	dst[1] = y;
	dst[2] = z;

}

extern void setRotation_flexibility_cpp_vs (type_data *q_x, type_data *q_y, type_data *q_z, type_data *q_w, type_data angle, type_data * vector){
	angle = angle / 2;
	type_data module_v = sqrtf((vector[0]*vector[0]) + (vector[1]*vector[1]) + (vector[2]*vector[2]));
	*q_w = cos (angle);
	//printf("module_v %f %f\n",module_v,sin(angle)/module_v);
	*q_x = (vector[0] * sin (angle))/module_v;
	*q_y = (vector[1] * sin (angle))/module_v;
	*q_z = (vector[2] * sin (angle))/module_v;
}

extern void rotateLigandFragment_cpp_vs (int *f, unsigned int size_f, type_data* lig_x, type_data* lig_y, type_data* lig_z, type_data qeje_x, type_data qeje_y, type_data qeje_z, type_data qeje_w, type_data p1[3]) {

	type_data punto1[3], punto[3];

	for (unsigned int i1 = 0; i1 < size_f;i1++)
	{
		punto[0] = lig_x[(f[i1]-1)]   - p1[0];
		punto[1] = lig_y[(f[i1]-1)] - p1[1];
		punto[2] = lig_z[(f[i1]-1)] - p1[2];

		//ROTAMOS UN PUNTO (PUNTO) ALREDEDOR DE UN EJE MARCADO POR LOS PUNTOS B - A
		rotate3D_flexibility_cpp_vs(qeje_x,qeje_y,qeje_z,qeje_w,punto,punto1);
		//PUNTO GIRADO EN (PUNTO1)


		lig_x[(f[i1]-1)]   = punto1[0] + p1[0];
		lig_y[(f[i1]-1)] = punto1[1] + p1[1];
		lig_z[(f[i1]-1)] = punto1[2] + p1[2];
	}
}

extern void flexibility_cpp_vs (unsigned int nconformations, unsigned int files, struct metaheuristic_t metaheuristic, struct param_t param, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, struct flexibility_data_t * flexibility_conformations, struct flexibility_params_t *flexibility_params, unsigned int nlig, unsigned int max_angle_flex)//, int n_links, int n_fragments, int *links, int *links_fragments, int *fragments, int *fragments_tam, int max_angle_flex)
{
	type_data a[3],b[3],eje[3],p1[3];
	type_data ang;
	unsigned int num_point, frag_act, position_link;
	unsigned int rot_point, link, k;
	int shift_fragment, position_conformation;
	type_data qeje_x, qeje_y, qeje_z, qeje_w;
	unsigned int tam = nconformations / files;
	unsigned int h1;

	switch (param.mode) {
		case 1:
			h1 = 1;
			break;
		case 2:
			h1 = metaheuristic.Threads1Ini;
			break;
	}
	omp_set_num_threads (h1);
	#pragma omp parallel for private (position_conformation,position_link,ang,link,rot_point,a,b,eje,p1,num_point,shift_fragment,frag_act,qeje_x,qeje_y,qeje_z,qeje_w,k)
	for (unsigned int id_conformation = 0; id_conformation < nconformations; id_conformation++)
	{
		ang = flexibility_conformations[id_conformation].ang;
		link = flexibility_conformations[id_conformation].link;
		rot_point = flexibility_conformations[id_conformation].p_rot;

		//NOS SITUAMOS EN EL VECTOR DE CONFORMACIONES Y DE ENLACES
		position_conformation = id_conformation * nlig;
		k = id_conformation / tam;
		position_link = link;
		//printf("principio id_conf %d, pos %d ang %f link %d rot %d pos %d\n",id_conformation,position_conformation,ang,link,rot_point,position_conformation);
		//printf("id_conf %d link %d flexibility[%d].links[2*%d]=%d **** flexibility[%d].links[(2*%d)+1]=%d\n",id_conformation,position_link,k,position_link,flexibility_params[k].links[2*position_link],k,position_link,flexibility_params[k].links[(2*position_link)+1]); 

		//EJE DE GIRO
		a[0] = conformations_x[position_conformation + (flexibility_params[k].links[2 * position_link] - 1)];
		a[1] = conformations_y[position_conformation + (flexibility_params[k].links[2 * position_link] - 1)];
		a[2] = conformations_z[position_conformation + (flexibility_params[k].links[2 * position_link] - 1)];
		b[0] = conformations_x[position_conformation + (flexibility_params[k].links[(2 * position_link)+1] - 1)];
		b[1] = conformations_y[position_conformation + (flexibility_params[k].links[(2 * position_link)+1] - 1)];
		b[2] = conformations_z[position_conformation + (flexibility_params[k].links[(2 * position_link)+1] - 1)];

		if (rot_point == 0){
			eje[0] = b[0] - a[0];
			eje[1] = b[1] - a[1];
			eje[2] = b[2] - a[2];
			p1[0] = conformations_x[position_conformation + (flexibility_params[k].links[2 * position_link] - 1)];
			p1[1] = conformations_y[position_conformation + (flexibility_params[k].links[2 * position_link] - 1)];
			p1[2] = conformations_z[position_conformation + (flexibility_params[k].links[2 * position_link] - 1)];
			num_point = (2 * position_link) + 1;
		}else {
			eje[0] = a[0] - b[0];
			eje[1] = a[1] - b[1];
			eje[2] = a[2] - b[2];
			p1[0] = conformations_x[position_conformation + (flexibility_params[k].links[(2 * position_link)+1] - 1)];
			p1[1] = conformations_y[position_conformation + (flexibility_params[k].links[(2 * position_link)+1] - 1)];
			p1[2] = conformations_z[position_conformation + (flexibility_params[k].links[(2 * position_link)+1] - 1)];
			num_point = 2 * position_link;
		}
		//printf("id_conf %d, pos %d ang %f link %d rot %d k %d qxyzw %f %f %f %f ejexyz %f %f %f \n",id_conformation,position_conformation,ang,link,rot_point,k,qeje_x,qeje_y,qeje_z,qeje_w,eje[0],eje[1],eje[2]);
		//CONFIGURACION EJE DE ROTACION COMO QUATERNION
		setRotation_flexibility_cpp_vs (&qeje_x,&qeje_y,&qeje_z,&qeje_w,ang,eje);
		//printf("FIN id_conf %d\n",id_conformation);
		
		//int j2 = 0;
		shift_fragment = 0;
		//ROTAMOS TODOS LOS FRAGMENTOS QUE TENGAN COMO ORIGEN EL PUNTO DE ROTACION
		//while ((frag_act = links_fragments_d[(num_point * n_fragments) + j2]) != 0)
		frag_act = flexibility_params[k].links_fragments[num_point];
		//{
		for (unsigned int i2 = 0; i2 < (frag_act - 1); i2++) 
			shift_fragment = shift_fragment + flexibility_params[k].fragments_tam[i2];
		rotateLigandFragment_cpp_vs((flexibility_params[k].fragments + shift_fragment),flexibility_params[k].fragments_tam[frag_act-1],(conformations_x + position_conformation), (conformations_y + position_conformation), (conformations_z + position_conformation), qeje_x, qeje_y, qeje_z, qeje_w, p1);
	}
}


extern void angulations_conformations_cpp_vs (unsigned int nconformations, unsigned int files, struct metaheuristic_t metaheuristic, struct param_t param, struct flexibility_data_t * flexibility_conformations, struct flexibility_params_t *flexibility_params, type_data max_angle_flex)
{	
	type_data t_p;
	unsigned int link, rot_point, k;
	unsigned int tam = nconformations / files;
	unsigned int h1;	
	switch (param.mode) {
		case 1:
			h1 = 1;
			break;
		case 2:
			h1 = metaheuristic.Threads1Ini;
			break;
	}
	omp_set_num_threads (h1);
	#pragma omp parallel for private(t_p,link,rot_point,k)
	for (unsigned int id_conformation=0; id_conformation < nconformations; id_conformation++)
	{
		k = id_conformation / tam;
		//OBTENEMOS EL ANGULO EN RADIANES ALEATORIO ENTRE UN MAXIMO.
		t_p =  getRealRandomNumber(max_angle_flex);
		//ENLACE Y FRAGMENTO A GIRAR ALEATORIO
		link =  getIntNumber(10000) % flexibility_params[k].n_links;
		//OBTENEMOS EL PUNTO DEL ENLACE DESDE DONDE ROTAR
		rot_point = getIntNumber(10000) % 2;
		//GUARDAR LOS VALORES
		flexibility_conformations[id_conformation].ang = (t_p * PI) / 180;
		flexibility_conformations[id_conformation].link = link;
		flexibility_conformations[id_conformation].p_rot = rot_point;
	}
}

/*extern void flexibility_function_cpp (struct ligand_t ligando, struct param_t param, struct metaheuristic_t metaheuristic, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, struct flexibility_data_t * flexibility_conformations,struct flexibility_params_t *flexibility_params, struct vdw_param_t *vdw_params, struct sasa_param_t *sasa_params, struct hbond_param_t *hbond_params, int nconformations)
{
	
	angulations_conformations_cpp (nconformations,metaheuristic,param,flexibility_conformations,flexibility_params->n_links,param.flex_angle);
	flexibility_cpp (nconformations,metaheuristic,param,conformations_x,conformations_y,conformations_z,flexibility_conformations,ligando.nlig,flexibility_params->n_links,flexibility_params->n_fragments,flexibility_params->links,flexibility_params->links_fragments,flexibility_params->fragments,flexibility_params->fragments_tam,param.flex_angle);									
		
}*/

