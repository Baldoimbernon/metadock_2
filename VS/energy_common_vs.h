#ifndef COMMON_H_VS_
#define COMMON_H_VS_
#include "definitions.h"

extern void memcpy_type_data (type_data *out, type_data *in, unsigned int n);
extern void readLigand_vs (char *filename, char **type_json, unsigned int nlig, type_data *c_x, type_data *c_y, type_data *c_z, char *ligtype, char *subtype, char *nbonds, unsigned int *bonds, type_data *ql, int scoring_type);
extern void readConfigFile_vs(char *file, struct param_t &Param, struct metaheuristic_t &Metaheuristic, struct vectors_t *vectors);

#endif /* COMMON_H_VS */
