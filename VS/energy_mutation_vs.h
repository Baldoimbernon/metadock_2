#ifndef __ENERGY_MUTATION_VS_H
#define __ENERGY_MUTATION_VS_H

#include "type.h"

__global__ void mutation_random_vs (curandState_t * states, unsigned int *n_update, unsigned long int max);
__global__ void mutation_kernel_vs (curandState_t * states, unsigned int *n_update, type_data max_desp, type_data rotation, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, unsigned long int max);
extern void mutation_gpu_calculation_vs (struct receptor_t proteina, struct param_t param, struct ligand_params_t *ligand_params, struct metaheuristic_t metaheuristic, struct device_t *devices, type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *ql, char *ligtype, unsigned int *bonds, unsigned int nlig, unsigned int files, unsigned int nconformations,unsigned int orden_device, unsigned int stride);
extern void mutation_gpu_vs (struct receptor_t proteina, struct param_t param, struct ligand_params_t *ligand_params, struct metaheuristic_t metaheuristic, struct device_t *devices, type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *ql, char *ligtype, unsigned int *bonds, unsigned int nlig, unsigned int files, unsigned int nconformations,unsigned int orden_device, unsigned int stride);
extern void mutation_multigpu_vs (struct receptor_t proteina, struct ligand_params_t *ligand_params,unsigned int files, unsigned int nlig, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *weights, struct force_field_param_t *f_params, type_data *ql, char *ligtype, unsigned int *bonds, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int fase);



#endif
