#include "energy_cuda.h"
#include "energy_common.h"
#include "energy_CpuSolver_vs.h"
#include "energy_CpuSolver_flex.h"
#include "energy_CpuSolver_vs_flex.h"
#include "energy_GpuSolver_vs.h"
#include "metaheuristic_inc_vs.h"
#include "metaheuristic_inc_vs_flex.h"
#include "metaheuristic_ini_vs.h"
#include "metaheuristic_mgpu_vs_flex.h"
#include "energy_positions_vs.h"
#include "energy_positions_vs_flex.h"
#include "montecarlo_vs.h"
#include "energy_montecarlo_vs.h"
#include "metaheuristic_ini_flex.h"
#include "flexibility_vs.h"
#include "wtime.h"
#include <thrust/sequence.h>
#include <thrust/sort.h>
#include <thrust/functional.h>
#include <omp.h>


using namespace std;

extern void incluir_gpu_vs_flex_by_step (unsigned int nlig, type_data *e_conformations_x, type_data *e_conformations_y, type_data *e_conformations_z, type_data *e_move_x, type_data *e_move_y, type_data *e_move_z, type_data *e_quat_x, type_data *e_quat_y, type_data *e_quat_z, type_data *e_quat_w, type_data *e_energy, unsigned int *e_nconfs, type_data *s_conformations_x, type_data *s_conformations_y, type_data *s_conformations_z, type_data *s_move_x, type_data *s_move_y, type_data *s_move_z, type_data *s_quat_x, type_data *s_quat_y, type_data *s_quat_z, type_data *s_quat_w, type_data *s_energy, unsigned int *s_nconfs, unsigned int stride_e, unsigned int stride_s, unsigned int pos,struct param_t param, struct metaheuristic_t metaheuristic)
{

        struct vectors_t *vtmp = (struct vectors_t *)malloc(sizeof(struct vectors_t));
        vtmp->nconformations = stride_e + stride_s;
	vtmp->conformations_x = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations*nlig);
	vtmp->conformations_y = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations*nlig);
	vtmp->conformations_z = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations*nlig);
        vtmp->move_x = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->move_y = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->move_z = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->quat_x = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->quat_y = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->quat_z = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->quat_w = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->energy.energy = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vtmp->nconformations);
	thrust::sequence(vtmp->energy.n_conformation,vtmp->energy.n_conformation + vtmp->nconformations);

        thrust::stable_sort_by_key(e_energy,e_energy + stride_e, e_nconfs, thrust::less<type_data>());

        colocar_vs_flex_by_step (param,metaheuristic,nlig,e_conformations_x,e_conformations_y,e_conformations_z,e_move_x,e_move_y,e_move_z,e_quat_x,e_quat_y,e_quat_z,e_quat_w,e_energy,e_nconfs,stride_e,pos);
		
	memmove(vtmp->conformations_x, e_conformations_x, stride_e * nlig * sizeof(type_data));
        memmove(vtmp->conformations_x + (stride_e * nlig), s_conformations_x, stride_s * nlig * sizeof(type_data));
		
	memmove(vtmp->conformations_y, e_conformations_y, stride_e * nlig * sizeof(type_data));
        memmove(vtmp->conformations_y + (stride_e * nlig), s_conformations_y, stride_s * nlig * sizeof(type_data));
		
	memmove(vtmp->conformations_z, e_conformations_z, stride_e * nlig * sizeof(type_data));
        memmove(vtmp->conformations_z + (stride_e * nlig), s_conformations_z, stride_s * nlig * sizeof(type_data));

        memmove(vtmp->move_x, e_move_x, stride_e * sizeof(type_data));
        memmove(vtmp->move_x + stride_e, s_move_x, stride_s * sizeof(type_data));

        memmove(vtmp->move_y, e_move_y, stride_e * sizeof(type_data));
        memmove(vtmp->move_y + stride_e, s_move_y, stride_s * sizeof(type_data));

        memmove(vtmp->move_z, e_move_z, stride_e * sizeof(type_data));
        memmove(vtmp->move_z + stride_e, s_move_z, stride_s * sizeof(type_data));

        memmove(vtmp->quat_x, e_quat_x, stride_e * sizeof(type_data));
        memmove(vtmp->quat_x + stride_e, s_quat_x, stride_s * sizeof(type_data));

        memmove(vtmp->quat_y, e_quat_y, stride_e * sizeof(type_data));
        memmove(vtmp->quat_y + stride_e, s_quat_y, stride_s * sizeof(type_data));

        memmove(vtmp->quat_z, e_quat_z, stride_e * sizeof(type_data));
        memmove(vtmp->quat_z + stride_e, s_quat_z, stride_s * sizeof(type_data));

        memmove(vtmp->quat_w, e_quat_w, stride_e * sizeof(type_data));
        memmove(vtmp->quat_w + stride_e, s_quat_w, stride_s * sizeof(type_data));

        memmove(vtmp->energy.energy, e_energy, stride_e * sizeof(type_data));
        memmove(vtmp->energy.energy + stride_e, s_energy, stride_s * sizeof(type_data));

        thrust::stable_sort_by_key(vtmp->energy.energy,vtmp->energy.energy + vtmp->nconformations, vtmp->energy.n_conformation, thrust::less<type_data>());
	
        colocar_vs_flex_by_step (param,metaheuristic,nlig,vtmp->conformations_x,vtmp->conformations_y,vtmp->conformations_z,vtmp->move_x,vtmp->move_y,vtmp->move_z,vtmp->quat_x,vtmp->quat_y,vtmp->quat_z,vtmp->quat_w,vtmp->energy.energy,vtmp->energy.n_conformation,stride_e + stride_s,0);
			
	memmove(s_conformations_x, vtmp->conformations_x, stride_s * nlig * sizeof(type_data));
	memmove(s_conformations_y, vtmp->conformations_y, stride_s * nlig * sizeof(type_data));
	memmove(s_conformations_z, vtmp->conformations_z, stride_s * nlig * sizeof(type_data));
		
	memmove(s_move_x, vtmp->move_x, stride_s * sizeof(type_data));
        memmove(s_move_y, vtmp->move_y, stride_s * sizeof(type_data));
        memmove(s_move_z, vtmp->move_z, stride_s * sizeof(type_data));

        memmove(s_quat_x, vtmp->quat_x, stride_s * sizeof(type_data));
        memmove(s_quat_y, vtmp->quat_y, stride_s * sizeof(type_data));
        memmove(s_quat_z, vtmp->quat_z, stride_s * sizeof(type_data));
        memmove(s_quat_w, vtmp->quat_w, stride_s * sizeof(type_data));

        memmove(s_energy, vtmp->energy.energy, stride_s * sizeof(type_data));

	free(vtmp->conformations_x);
	free(vtmp->conformations_y);
	free(vtmp->conformations_z);
        free(vtmp->move_x);
        free(vtmp->move_y);
        free(vtmp->move_z);
        free(vtmp->quat_x);
        free(vtmp->quat_y);
        free(vtmp->quat_z);
        free(vtmp->quat_w);
        free(vtmp->energy.energy);
        free(vtmp->energy.n_conformation);
	//free(vtmp);
}
/*extern void incluir_gpu_vs_flex (unsigned int nlig, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic)
{
	struct vectors_t vtmp;// = (struct vectors_t *)malloc(sizeof(struct vectors_t));
	unsigned int th1;
	unsigned int stride_e = vectors_e->nconformations / vectors_e->files;
	unsigned int stride_s = vectors_s->nconformations / vectors_s->files;
	unsigned int stride_t = stride_e +  stride_s; 	
	vtmp.files = vectors_e->files;
	vtmp.nlig = vectors_e->nlig;
	vtmp.nconformations = vectors_e->files * stride_t;
	vtmp.conformations_x = (type_data *)malloc(sizeof(type_data)*nlig*vtmp.nconformations);
	vtmp.conformations_y = (type_data *)malloc(sizeof(type_data)*nlig*vtmp.nconformations);
	vtmp.conformations_z = (type_data *)malloc(sizeof(type_data)*nlig*vtmp.nconformations);
	vtmp.move_x = (type_data *)malloc(sizeof(type_data)*vtmp.nconformations);
	vtmp.move_y = (type_data *)malloc(sizeof(type_data)*vtmp.nconformations);
	vtmp.move_z = (type_data *)malloc(sizeof(type_data)*vtmp.nconformations);
	vtmp.quat_x = (type_data *)malloc(sizeof(type_data)*vtmp.nconformations);	
	vtmp.quat_y = (type_data *)malloc(sizeof(type_data)*vtmp.nconformations);	
	vtmp.quat_z = (type_data *)malloc(sizeof(type_data)*vtmp.nconformations);	
	vtmp.quat_w = (type_data *)malloc(sizeof(type_data)*vtmp.nconformations);	
	vtmp.energy.energy = (type_data *)malloc(sizeof(type_data)*vtmp.nconformations);
	vtmp.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vtmp.nconformations);
	thrust::sequence(vtmp.energy.n_conformation,vtmp.energy.n_conformation + vtmp.nconformations);
	
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 0:
		case 2:
		case 3:
			th1 = metaheuristic.Threads1Inc;
			break;
	}
	//printf("conformationes totales %d, stride_p %d\n",vtmp.nconformations,stride_p);
	//xit(0);
	//printf("***ANTES\n");
	//for (int i=0;i<64;i++) printf("conf %d, xyz %f %f %f m %f %f %f, q %f %f %f, energy %f %d\n",i,vectors_e->conformations_x[i],vectors_e->conformations_y[i], vectors_e->conformations_z[i],vectors_e->move_x[i/vectors_e->nlig],vectors_e->move_y[i/vectors_e->nlig],vectors_e->move_z[i/vectors_e->nlig],vectors_e->quat_x[i/vectors_e->nlig],vectors_e->quat_y[i/vectors_e->nlig],vectors_e->quat_z[i/vectors_e->nlig],vectors_e->energy.energy[i/vectors_e->nlig],vectors_e->energy.n_conformation[i/vectors_e->nlig]);
	omp_set_num_threads (th1);
	#pragma omp parallel for
	for (unsigned int i = 0; i < vectors_e->nconformations; i += stride_e)			
		thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + stride_e), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());			
	colocar_vs_flex(param,vectors_e,metaheuristic);		
	
	#pragma omp parallel for
	for (unsigned int i = 0;i < vectors_e->files;i++)
	{
		memcpy(vtmp.conformations_x+ i*nlig*(stride_e +  stride_s), vectors_s->conformations_x + i*nlig*stride_s, stride_s * nlig * sizeof(type_data)); 
		memcpy(vtmp.conformations_y+ i*nlig*(stride_e +  stride_s), vectors_s->conformations_y + i*nlig*stride_s, stride_s * nlig * sizeof(type_data)); 
		memcpy(vtmp.conformations_z+ i*nlig*(stride_e +  stride_s), vectors_s->conformations_z + i*nlig*stride_s, stride_s * nlig * sizeof(type_data)); 
		memcpy(vtmp.move_x + i*stride_t, vectors_s->move_x + i*stride_s, stride_s * sizeof(type_data)); 
		memcpy(vtmp.move_y + i*stride_t, vectors_s->move_y + i*stride_s, stride_s * sizeof(type_data)); 
		memcpy(vtmp.move_z + i*stride_t, vectors_s->move_z + i*stride_s, stride_s * sizeof(type_data)); 
		memcpy(vtmp.quat_x + i*stride_t, vectors_s->quat_x + i*stride_s, stride_s * sizeof(type_data)); 
		memcpy(vtmp.quat_y + i*stride_t, vectors_s->quat_y + i*stride_s, stride_s * sizeof(type_data)); 
		memcpy(vtmp.quat_z + i*stride_t, vectors_s->quat_z + i*stride_s, stride_s * sizeof(type_data)); 
		memcpy(vtmp.quat_w + i*stride_t, vectors_s->quat_w + i*stride_s, stride_s * sizeof(type_data)); 
		memcpy(vtmp.energy.energy + i*stride_t, vectors_s->energy.energy + i*stride_s, stride_s * sizeof(type_data)); 
		
		memcpy(vtmp.conformations_x+ i*nlig*(stride_e +  stride_s) + nlig*stride_s, vectors_e->conformations_x + i*nlig*stride_e, stride_e * nlig * sizeof(type_data));  			
		memcpy(vtmp.conformations_y+ i*nlig*(stride_e +  stride_s) + nlig*stride_s, vectors_e->conformations_y + i*nlig*stride_e, stride_e * nlig * sizeof(type_data));  			
		memcpy(vtmp.conformations_z+ i*nlig*(stride_e +  stride_s) + nlig*stride_s, vectors_e->conformations_z + i*nlig*stride_e, stride_e * nlig * sizeof(type_data));	
		memcpy(vtmp.move_x + i*stride_t + stride_s, vectors_e->move_x + i*stride_e, stride_e * sizeof(type_data));  			
		memcpy(vtmp.move_y + i*stride_t + stride_s, vectors_e->move_y + i*stride_e, stride_e * sizeof(type_data));  			
		memcpy(vtmp.move_z + i*stride_t + stride_s, vectors_e->move_z + i*stride_e, stride_e * sizeof(type_data));  			
		memcpy(vtmp.quat_x + i*stride_t + stride_s, vectors_e->quat_x + i*stride_e, stride_e * sizeof(type_data));  
		memcpy(vtmp.quat_y + i*stride_t + stride_s, vectors_e->quat_y + i*stride_e, stride_e * sizeof(type_data));  	
		memcpy(vtmp.quat_z + i*stride_t + stride_s, vectors_e->quat_z + i*stride_e, stride_e * sizeof(type_data));  	
		memcpy(vtmp.quat_w + i*stride_t + stride_s, vectors_e->quat_w + i*stride_e, stride_e * sizeof(type_data));  			
		memcpy(vtmp.energy.energy + i*stride_t + stride_s, vectors_e->energy.energy + i*stride_e, stride_e * sizeof(type_data));  					
	}
	//memset(vectors_s->conformations_x,0.0,vectors_s->nconformations*nlig);	
	//memset(vectors_s->conformations_y,0.0,vectors_s->nconformations*nlig);
	//memset(vectors_s->conformations_z,0.0,vectors_s->nconformations*nlig);

	//printf("files %d %d %d\n",vtmp.files,vectors_e->nconformations,vectors_s->nconformations);	
	//exit(0);
	//printf("VTMP ANTES INCLUIR\n");
	//printf("conf %d\n",vtmp.nconformations);
	//for (int i=0;i<78848;i++) printf("conf %d, xyz %f %f %f m %f %f %f, q %f %f %f, energy %f %d\n",i,vtmp.conformations_x[i],vtmp.conformations_y[i], vtmp.conformations_z[i],vtmp.move_x[i/vectors_e->nlig],vtmp.move_y[i/vtmp.nlig],vtmp.move_z[i/vtmp.nlig],vtmp.quat_x[i/vtmp.nlig],vtmp.quat_y[i/vtmp.nlig],vtmp.quat_z[i/vtmp.nlig],vtmp.energy.energy[i/vtmp.nlig],vtmp.energy.n_conformation[i/vtmp.nlig]);
	//exit(0);
	#pragma omp parallel for
	for (unsigned int i = 0; i < vtmp.nconformations; i += stride_t)			
		thrust::stable_sort_by_key((vtmp.energy.energy + i),(vtmp.energy.energy + i + stride_t), (vtmp.energy.n_conformation + i), thrust::less<type_data>());			
	colocar_vs_flex(param,&vtmp,metaheuristic);
	//printf("VTMP DESPUES INCLUIR\n");
	//for (int i=0;i<78848;i++) printf("conf %d, xyz %f %f %f m %f %f %f, q %f %f %f, energy %f %d\n",i,vtmp.conformations_x[i],vtmp.conformations_y[i], vtmp.conformations_z[i],vtmp.move_x[i/vectors_e->nlig],vtmp.move_y[i/vtmp.nlig],vtmp.move_z[i/vtmp.nlig],vtmp.quat_x[i/vtmp.nlig],vtmp.quat_y[i/vtmp.nlig],vtmp.quat_z[i/vtmp.nlig],vtmp.energy.energy[i/vtmp.nlig],vtmp.energy.n_conformation[i/vtmp.nlig]);
	
	//exit(0);
	#pragma omp parallel for
	for (unsigned int i = 0;i < vectors_s->files;i++)
	{
		memcpy(vectors_s->conformations_x + i*nlig*stride_s, vtmp.conformations_x + i*nlig*stride_t,stride_s * nlig * sizeof(type_data));
		memcpy(vectors_s->conformations_y + i*nlig*stride_s, vtmp.conformations_y + i*nlig*stride_t,stride_s * nlig * sizeof(type_data));
		memcpy(vectors_s->conformations_z + i*nlig*stride_s, vtmp.conformations_z + i*nlig*stride_t,stride_s * nlig * sizeof(type_data));
		memcpy(vectors_s->move_x + i*stride_s, vtmp.move_x + i*stride_t,stride_s * sizeof(type_data));
		memcpy(vectors_s->move_y + i*stride_s, vtmp.move_y + i*stride_t,stride_s * sizeof(type_data)); 
		memcpy(vectors_s->move_z + i*stride_s, vtmp.move_z + i*stride_t,stride_s * sizeof(type_data)); 		
		memcpy(vectors_s->quat_x + i*stride_s, vtmp.quat_x + i*stride_t,stride_s * sizeof(type_data)); 		
		memcpy(vectors_s->quat_y + i*stride_s, vtmp.quat_y + i*stride_t,stride_s * sizeof(type_data)); 		
		memcpy(vectors_s->quat_z + i*stride_s, vtmp.quat_z + i*stride_t,stride_s * sizeof(type_data)); 		
		memcpy(vectors_s->quat_w + i*stride_s, vtmp.quat_w + i*stride_t,stride_s * sizeof(type_data)); 		
		memcpy(vectors_s->energy.energy + i*stride_s, vtmp.energy.energy + i*stride_t,stride_s * sizeof(type_data)); 		
	}
	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
	//printf("VECTOR SALIDA\n");
	//for (int i=0;i<192;i++) printf("conf %d, xyz %f %f %f m %f %f %f, q %f %f %f, energy %f %d\n",i,vectors_s->conformations_x[i],vectors_s->conformations_y[i], vectors_s->conformations_z[i],vectors_s->move_x[i/vectors_s->nlig],vectors_s->move_y[i/vectors_s->nlig],vectors_s->move_z[i/vectors_s->nlig],vectors_s->quat_x[i/vectors_s->nlig],vectors_s->quat_y[i/vectors_s->nlig],vectors_s->quat_z[i/vectors_s->nlig],vectors_s->energy.energy[i/vectors_s->nlig],vectors_s->energy.n_conformation[i/vectors_s->nlig]);
	
	//printf("***INCLUIR MEJORA***\n");
	//exit(0);
	//free(vtmp.conformations_x);
	//free(vtmp.conformations_y);
	//free(vtmp.conformations_z);
	//free(vtmp.move_x);
	//free(vtmp.move_y);
	//free(vtmp.move_z);
	//free(vtmp.quat_x);
	//free(vtmp.quat_y);
	//free(vtmp.quat_z);
	//free(vtmp.quat_w);
	//free(vtmp.energy.energy);
}*/

extern void mejorar_vs_flex_environment (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct param_t param, type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *ql, char *ligtype, unsigned int *bonds, unsigned int nlig, unsigned int files, unsigned int nconformations, struct metaheuristic_t metaheuristic, struct flexibility_params_t *flexibility_params, struct flexibility_data_t *flexibility_conformations,  unsigned int opt)
{
	struct vectors_t vectors_mejora;
        unsigned int steps,moveType=MOVE,bucle,fase,simulations,total;
        double tenergymejora;
        double tenergymejora_t;
        unsigned int tam = (nconformations * metaheuristic.NEEImp) / files;
	unsigned int *nlinks = (unsigned int *)calloc(files,sizeof(unsigned int));
        type_data *internal_energy = (type_data *)malloc(sizeof(type_data)*nconformations*metaheuristic.NEEImp);
	type_data max_angle_flex;
        flexibility_conformations = (flexibility_data_t *)malloc(sizeof(flexibility_data_t)*nconformations*metaheuristic.NEEImp);

        vectors_mejora.files = files;
        vectors_mejora.nlig = nlig;
        vectors_mejora.nconformations = nconformations * metaheuristic.NEEImp;

        vectors_mejora.conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nlig*vectors_mejora.nconformations);
        vectors_mejora.conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nlig*vectors_mejora.nconformations);
        vectors_mejora.conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nlig*vectors_mejora.nconformations);

        vectors_mejora.ql = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations*nlig);
        vectors_mejora.ligtype = (char *)malloc(sizeof(char)*vectors_mejora.nconformations*nlig);
        vectors_mejora.bonds = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations*nlig*MAXBOND);

	vectors_mejora.move_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.move_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.move_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations);
        vectors_mejora.energy.energy = (type_data *)calloc(sizeof(type_data),vectors_mejora.nconformations);
        vectors_mejora.weights = (type_data *)malloc(SCORING_TERMS * sizeof(type_data));

	thrust::sequence(vectors_mejora.energy.n_conformation,vectors_mejora.energy.n_conformation + vectors_mejora.nconformations);

	memcpy(vectors_mejora.f_params, f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_mejora.weights, weights,SCORING_TERMS * sizeof(type_data));
	
	fill_conformations_environment_vs (vectors_mejora.nlig,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,ligtype,ql,bonds,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,vectors_mejora.ligtype,vectors_mejora.ql,vectors_mejora.bonds,nconformations,param,metaheuristic);

	//for (int i=0;i<vectors_mejora.nconformations;i++) printf("INI id %d m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f energy %f\n",i,vectors_mejora.move_x[i],vectors_mejora.move_y[i],vectors_mejora.move_z[i],vectors_mejora.quat_x[i],vectors_mejora.quat_y[i],vectors_mejora.quat_z[i],vectors_mejora.energy.energy[i]);
        //exit(0);
	for (int i=0; i < files; i++) nlinks[i]=flexibility_params[i].n_links;

	steps = 0;
        if (opt == 0)
        {
                total = metaheuristic.IMEIni + metaheuristic.IMEFlex;
                simulations = metaheuristic.IMEIni;
        }
        if (opt == 1)
        {
                total = metaheuristic.IMEImp + metaheuristic.IMEFlex;
                simulations = metaheuristic.IMEImp;
        }
        if (opt == 2)
        {
                total = metaheuristic.IMEMUCom + metaheuristic.IMEFlex;
                simulations = metaheuristic.IMEMUCom;
        }

        moveType = MOVE;
	 
        //printf("Voy a mejorar....\n");
        while (steps < total){

		if (steps >=simulations)
                {
                        //FLEXIBILIDAD
                        angulations_conformations_cpp_vs (vectors_mejora.nconformations,vectors_mejora.files,metaheuristic,param,flexibility_conformations,flexibility_params,param.flex_angle);
                        flexibility_cpp_vs (vectors_mejora.nconformations,vectors_mejora.files,metaheuristic,param,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,flexibility_conformations,flexibility_params,vectors_mejora.nlig,param.flex_angle);
                        max_angle_flex /= 1.1;
                }
                else
                {
                	if (moveType == MOVE){
                        	move_mejora_vs(vectors_mejora.files,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,param,tam,metaheuristic);
                        	//moveType = ROTATE;
                        	//printf("He movido\n");
                	}
                	if (moveType == ROTATE) {
                        	rotate_vs(vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,param,vectors_mejora.nconformations,metaheuristic);
                        	//moveType = FLEX;
                	}
                	if (moveType == FLEX) {
                        	angulations_conformations_cpp_vs (vectors_mejora.nconformations,vectors_mejora.files,metaheuristic,param,flexibility_conformations,flexibility_params,param.flex_angle);
                        	flexibility_cpp_vs (vectors_mejora.nconformations,vectors_mejora.files,metaheuristic,param,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,flexibility_conformations,flexibility_params,vectors_mejora.nlig,param.flex_angle);
                        	//moveType = MOVE;
                	}
		}
                //printf("Voy a calcular la energia...\n");
                tenergymejora = wtime();
		 //for (int i=0;i<vectors_mejora.nconformations*nlig;i++) printf("id %d x %f y %f z %f ql %f lig %d \n",i,vectors_mejora.conformations_x[i],vectors_mejora.conformations_y[i],vectors_mejora.conformations_z[i],vectors_mejora.ql[i],vectors_mejora.ligtype[i]);

                ForcesCpuSolver_vs(nlinks,vectors_mejora.nlig,vectors_mejora.files,param,proteina,ligand_params,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,vectors_mejora.ligtype,vectors_mejora.ql,vectors_mejora.bonds,vectors_mejora.energy.energy,vectors_mejora.energy.n_conformation,vectors_mejora.weights,vectors_mejora.f_params,vectors_mejora.nconformations,metaheuristic);
                energy_internal_conformations_cpu_vs (vectors_mejora.nconformations,vectors_mejora.nlig,vectors_mejora.files,param,metaheuristic,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,ligand_params,flexibility_params,vectors_mejora.weights,vectors_mejora.f_params,internal_energy,vectors_mejora.ql,vectors_mejora.ligtype);
                energy_total (vectors_mejora.nconformations,param,metaheuristic,vectors_mejora.energy.energy,internal_energy);

		//for (int i=0;i<vectors_mejora.nconformations;i++) printf("STEPS id %d m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f energy %f\n",i,vectors_mejora.move_x[i],vectors_mejora.move_y[i],vectors_mejora.move_z[i],vectors_mejora.quat_x[i],vectors_mejora.quat_y[i],vectors_mejora.quat_z[i],vectors_mejora.energy.energy[i]);
       		//exit(0);
		incluir_mejorar_inicializar_environment_flex (nlig,vectors_mejora,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,nconformations,metaheuristic,param);
		//fill_conformations_environment_vs (vectors_mejora.nlig,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,ligtype,ql,bonds,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,vectors_mejora.ligtype,vectors_mejora.ql,vectors_mejora.bonds,nconformations,param,metaheuristic);

                steps++;
                if (moveType == MOVE) moveType = ROTATE;
                else if (moveType == ROTATE) moveType = FLEX;
                else if (moveType == FLEX) moveType = MOVE;
                //for(int k=0;k<64;k++)
                //      printf("STP %d Mejorar VS-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",steps,k,vectors_s->conformations_x[k],vectors_s->conformations_y[k],vectors_s->conformations_z[k],vectors_s->move_x[k/ligando.nlig],vectors_s->move_y[k/ligando.nlig],vectors_s->move_z[k/ligando.nlig],vectors_s->energy.energy[k/ligando.nlig]);

        }
        //if (opt==0)
        //      printf("\tCALCULO ENERGIA L-J EN FASE MEJORA INICIALIZAR: %lf seg\n",tenergymejora_t);
        //else
        //      printf("\tCALCULO ENERGIA L-J EN FASE MEJORAR: %lf seg\n",tenergymejora_t);
        //for(int k=0;k<160;k++)
                //      printf("STP %d Mejorar VS-number %d, m_x %f, m_y %f, m_z %f, energy %f\n",steps,k,vectors_s->move_x[k],vectors_s->move_y[k],vectors_s->move_z[k],vectors_s->energy.energy[k]);
        free(vectors_mejora.conformations_x);
        free(vectors_mejora.conformations_y);
        free(vectors_mejora.conformations_z);
        free(vectors_mejora.move_x);
        free(vectors_mejora.move_y);
        free(vectors_mejora.move_z);
        free(vectors_mejora.quat_x);
        free(vectors_mejora.quat_y);
        free(vectors_mejora.quat_z);
        free(vectors_mejora.quat_w);
        free(vectors_mejora.energy.energy);
        free(vectors_mejora.energy.n_conformation);
        free(vectors_mejora.weights);
        free(internal_energy);
	free(flexibility_conformations);
	free(nlinks);
        //for(int k=0;k<64;k++)
        //      printf("STP %d Mejorar VS-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",steps,k,vectors_s->conformations_x[k],vectors_s->conformations_y[k],vectors_s->conformations_z[k],vectors_s->move_x[k/ligando.nlig],vectors_s->move_y[k/ligando.nlig],vectors_s->move_z[k/ligando.nlig],vectors_s->energy.energy[k/ligando.nlig]);
        //exit(0);

}

extern void mejorar_vs_flex (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct param_t param, type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *ql, char *ligtype, unsigned int *bonds, unsigned int nlig, unsigned int files, unsigned int nconformations, struct metaheuristic_t metaheuristic, struct flexibility_params_t *flexibility_params, struct flexibility_data_t *flexibility_conformations,  unsigned int opt)
{
	struct vectors_t vectors_mejora;
	unsigned int steps,moveType=MOVE,bucle,fase,simulations,total;	
	double tenergymejora;
	double tenergymejora_t;
	unsigned int tam = nconformations / files;
	unsigned int *nlinks = (unsigned int *)calloc(files,sizeof(unsigned int));
	type_data *internal_energy = (type_data *)malloc(sizeof(type_data)*nconformations);
	type_data max_angle_flex;
	flexibility_conformations = (flexibility_data_t *)malloc(sizeof(flexibility_data_t)*nconformations);
	
	vectors_mejora.files = files;
	vectors_mejora.nlig = nlig;
	vectors_mejora.nconformations = nconformations;
	
	vectors_mejora.conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nlig*vectors_mejora.nconformations);
	vectors_mejora.conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nlig*vectors_mejora.nconformations);
	vectors_mejora.conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nlig*vectors_mejora.nconformations);

	vectors_mejora.move_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.move_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.move_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations);
	vectors_mejora.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);	
	vectors_mejora.weights = (type_data *)malloc(SCORING_TERMS * sizeof(type_data));

 	memcpy(vectors_mejora.move_x, move_x, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.move_y, move_y, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.move_z, move_z, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_x, quat_x, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_y, quat_y, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_z, quat_z, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_w, quat_w, vectors_mejora.nconformations * sizeof(type_data));  
	
	memcpy(vectors_mejora.conformations_x, conformations_x, vectors_mejora.nconformations * nlig * sizeof(type_data));  
	memcpy(vectors_mejora.conformations_y, conformations_y, vectors_mejora.nconformations * nlig * sizeof(type_data));  
	memcpy(vectors_mejora.conformations_z, conformations_z, vectors_mejora.nconformations * nlig * sizeof(type_data));

	//memcpy(vectors_mejora.energy.energy, vectors_s->energy.energy, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.energy.n_conformation, nconfs,vectors_mejora.nconformations * sizeof(unsigned int));

	memcpy(vectors_mejora.f_params, f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_mejora.weights, weights,SCORING_TERMS * sizeof(type_data));
	for (int i=0; i < files; i++) nlinks[i]=flexibility_params[i].n_links;
	//memset(vectors_mejora.energy.energy,0,vectors_s->nconformations * sizeof(type_data));
	//exit(0);
	
	//for(int k=0;k<160;k++)
	//		printf("STP %d Mejorar VS-number %d, m_x %f, m_y %f, m_z %f, energy %f\n",steps,k,vectors_s->move_x[k],vectors_s->move_y[k],vectors_s->move_z[k],vectors_s->energy.energy[k]);
	//printf("*******\n");
	//exit(0);
	steps = 0;
	if (opt == 0)
        {
                total = metaheuristic.IMEIni + metaheuristic.IMEFlex;
                simulations = metaheuristic.IMEIni;
        }
        if (opt == 1)
        {
                total = metaheuristic.IMEImp + metaheuristic.IMEFlex;
                simulations = metaheuristic.IMEImp;
        }
        if (opt == 2)
        {
                total = metaheuristic.IMEMUCom + metaheuristic.IMEFlex;
                simulations = metaheuristic.IMEMUCom;
        }
	
	moveType = MOVE;
	//printf("Voy a mejorar....\n");
	while (steps < total){		
		if (steps >=simulations)
                {
                        //FLEXIBILIDAD
                        angulations_conformations_cpp_vs (vectors_mejora.nconformations,vectors_mejora.files,metaheuristic,param,flexibility_conformations,flexibility_params,param.flex_angle);
                        flexibility_cpp_vs (vectors_mejora.nconformations,vectors_mejora.files,metaheuristic,param,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,flexibility_conformations,flexibility_params,vectors_mejora.nlig,param.flex_angle);
                        max_angle_flex /= 1.1;
                }
                else
                {
			if (moveType == MOVE){
				move_mejora_vs(vectors_mejora.files,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,param,tam,metaheuristic);
				//moveType = ROTATE;
				//printf("He movido\n");
			}
			if (moveType == ROTATE) {
				rotate_vs(vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,param,vectors_mejora.nconformations,metaheuristic);
				//moveType = FLEX;
			}
			if (moveType == FLEX) {
				angulations_conformations_cpp_vs (vectors_mejora.nconformations,vectors_mejora.files,metaheuristic,param,flexibility_conformations,flexibility_params,param.flex_angle);
				flexibility_cpp_vs (vectors_mejora.nconformations,vectors_mejora.files,metaheuristic,param,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,flexibility_conformations,flexibility_params,vectors_mejora.nlig,param.flex_angle);				
				//moveType = MOVE;
			}
		}				
		//printf("Voy a calcular la energia...\n");
		tenergymejora = wtime();		
		ForcesCpuSolver_vs(nlinks,vectors_mejora.nlig,vectors_mejora.files,param,proteina,ligand_params,conformations_x,conformations_y,conformations_z,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,ligtype,ql,bonds,vectors_mejora.energy.energy,vectors_mejora.energy.n_conformation,vectors_mejora.weights,vectors_mejora.f_params,vectors_mejora.nconformations,metaheuristic);
		energy_internal_conformations_cpu_vs (vectors_mejora.nconformations,vectors_mejora.nlig,vectors_mejora.files,param,metaheuristic,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,ligand_params,flexibility_params,vectors_mejora.weights,vectors_mejora.f_params,internal_energy,ql,ligtype);
		energy_total (vectors_mejora.nconformations,param,metaheuristic,vectors_mejora.energy.energy,internal_energy);
					
		//incluir_mejorar_inicializar_vs_flex(vectors_mejora.nlig,vectors_mejora, vectors_s, metaheuristic, param);	
		incluir_mejorar_inicializar_flex (nlig,vectors_mejora,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,files,nconformations,metaheuristic,param);
		steps++;
		if (moveType == MOVE) moveType = ROTATE;
		else if (moveType == ROTATE) moveType = FLEX;
		else if (moveType == FLEX) moveType = MOVE;
		//for(int k=0;k<64;k++)
		//	printf("STP %d Mejorar VS-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",steps,k,vectors_s->conformations_x[k],vectors_s->conformations_y[k],vectors_s->conformations_z[k],vectors_s->move_x[k/ligando.nlig],vectors_s->move_y[k/ligando.nlig],vectors_s->move_z[k/ligando.nlig],vectors_s->energy.energy[k/ligando.nlig]);
	
	}
	//if (opt==0)
	//	printf("\tCALCULO ENERGIA L-J EN FASE MEJORA INICIALIZAR: %lf seg\n",tenergymejora_t);
	//else
	//	printf("\tCALCULO ENERGIA L-J EN FASE MEJORAR: %lf seg\n",tenergymejora_t);
	//for(int k=0;k<160;k++)
		//	printf("STP %d Mejorar VS-number %d, m_x %f, m_y %f, m_z %f, energy %f\n",steps,k,vectors_s->move_x[k],vectors_s->move_y[k],vectors_s->move_z[k],vectors_s->energy.energy[k]);
	free(vectors_mejora.conformations_x);	
	free(vectors_mejora.conformations_y);	
	free(vectors_mejora.conformations_z);	
	free(vectors_mejora.move_x);
	free(vectors_mejora.move_y);
	free(vectors_mejora.move_z);
	free(vectors_mejora.quat_x);
	free(vectors_mejora.quat_y);
	free(vectors_mejora.quat_z);
	free(vectors_mejora.quat_w);
	free(vectors_mejora.energy.energy);
	free(vectors_mejora.energy.n_conformation);
	free(vectors_mejora.weights);
	free(internal_energy);
	//for(int k=0;k<64;k++)
	//	printf("STP %d Mejorar VS-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",steps,k,vectors_s->conformations_x[k],vectors_s->conformations_y[k],vectors_s->conformations_z[k],vectors_s->move_x[k/ligando.nlig],vectors_s->move_y[k/ligando.nlig],vectors_s->move_z[k/ligando.nlig],vectors_s->energy.energy[k/ligando.nlig]);
	//exit(0);		
	
}



extern void inicializar_vs_flex (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, type_data *c_x, type_data *c_y, type_data *c_z, type_data *ql, char *ligtype, char *subtype, char *nbonds, unsigned int *bonds,struct flexibility_params_t *flexibility_params,struct flexibility_data_t *flexibility_conformations)
{
	
	//CONFIGURING MEMORY
	unsigned int NEItemp, NEI_selec_temp, stride_e, stride_s;
	unsigned int th1;
	type_data *internal_energy;
	unsigned int max_conformaciones;
	double tgenerarposi, tenergycpu, tordenarcpu, tcolocarcpu, tmejorainicpu;
	double tgenerarposi_t, tenergycpu_t, tordenarcpu_t, tcolocarcpu_t, tmejorainicpu_t;	
	struct vectors_t vectors_selec;
	unsigned int *nlinks = (unsigned int *)calloc(vectors_e->files,sizeof(unsigned int));
	for (int i=0; i < vectors_e->files; i++) nlinks[i]=flexibility_params[i].n_links;
	switch (param.mode) {
		case 0:
			//GPU SELECTION
			//NEItemp	= metaheuristic.NEIIni;
			//NEItemp = getPadding(NEItemp,16);
			//metaheuristic.NEIIni += NEItemp;
			
			NEI_selec_temp = (int)ceil((metaheuristic.NEIIni * metaheuristic.PEMIni) / 100);
			metaheuristic.NEIIni_selec = NEI_selec_temp;// + getPadding(NEI_selec_temp,16);
			vectors_e->nconformations = vectors_e->files * metaheuristic.NEIIni;
			vectors_e->conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->ql = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->ligtype = (char *)malloc(sizeof(char)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->subtype = (char *)malloc(sizeof(char)*vectors_e->nlig*SUBTYPEMAXLEN*vectors_e->nconformations);
			vectors_e->bonds = (unsigned int *)calloc(sizeof(unsigned int),vectors_e->nlig*MAXBOND*vectors_e->nconformations);
			//if (vectors_e->bonds == NULL) printf("Error reserva\n");
			vectors_e->nbonds = (char *)malloc(sizeof(char)*vectors_e->nlig*vectors_e->nconformations);
			
			vectors_e->move_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->move_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->move_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_e->nconformations);
			vectors_e->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			//for (int i=0;i<20;i++) printf("i %d, eps %f, sig %f\n",i,vectors_e->vdw_params[i].epsilon,vectors_e->vdw_params[i].sigma);
			internal_energy = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			//flexibility_conformations = (flexibility_data_t *)malloc(sizeof(flexibility_data_t)*vectors_e->nconformations);
			//exit(0);		
			//printf("Files %d, conf %d\n",vectors_e->files,vectors_e->nconformations);
			fill_conformations_vs(param,metaheuristic,c_x,c_y,c_z,ql,ligtype,subtype,nbonds,bonds,vectors_e);
			
			//printf("Fin relleno\n");
			//exit(0);
			tgenerarposi = wtime();//tomamos tiempos
			//for (int i=0;i<vectors_e->nconformations*vectors_e->nlig;i++) printf("id %d c_x %f c_y %f c_z %f \n",i/vectors_e->nlig,vectors_e->conformations_x[i],vectors_e->conformations_y[i],vectors_e->conformations_z[i]);
			//exit(0);
			generate_positions_vs_flex (vectors_e->nlig,ligand_params,param,vectors_e,metaheuristic,devices,0,flexibility_params,0);
			tgenerarposi_t += wtime() - tgenerarposi;
			//for (int i=(1000000*vectors_e->nlig);i<(1000000+25000)*vectors_e->nlig;i++) printf("id %d c_x %f c_y %f c_z %f \n",i/vectors_e->nlig,vectors_e->conformations_x[i],vectors_e->conformations_y[i],vectors_e->conformations_z[i]);
			//for (int i=0;i<vectors_e->nconformations;i++) printf("id %d m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f\n",i,vectors_e->move_x[i],vectors_e->move_y[i],vectors_e->move_z[i],vectors_e->quat_x[i],vectors_e->quat_y[i],vectors_e->quat_z[i]);
			//exit(0);	
			tenergycpu = wtime();//tomamos tiempos para calculo
			max_conformaciones = ((devices->propiedades[0].maxGridSize[0] * devices->hilos[0*metaheuristic.num_kernels]) / WARP_SIZE) * 8;
			if (vectors_e->nconformations < max_conformaciones)
			{
				gpuSolver_vs_flex (metaheuristic.NEIIni,vectors_e->files,vectors_e->nlig,vectors_e->ligtype,vectors_e->bonds,vectors_e->ql,ligand_params,flexibility_params,proteina,param,metaheuristic,devices,vectors_e->conformations_x, vectors_e->conformations_y, vectors_e->conformations_z,vectors_e->move_x,vectors_e->move_y,vectors_e->move_z,vectors_e->quat_x,vectors_e->quat_y,vectors_e->quat_z,vectors_e->quat_w,vectors_e->energy.energy,vectors_e->energy.n_conformation,vectors_e->weights,vectors_e->f_params,vectors_e->nconformations,0,0);		
			}
			else
			{
				//DIVIDIR EN TROZOS
				while((vectors_e->nconformations % max_conformaciones) != 0) max_conformaciones --;		
				//printf("va a trocear\n");				
				//exit(0);
				//int hh=0;
				for (unsigned int desplazamiento=0;desplazamiento<vectors_e->nconformations;desplazamiento+=max_conformaciones)
				{
					printf("Total nconf %d Device %d con trozo %d %d\n",vectors_e->nconformations,devices->id[0],desplazamiento,max_conformaciones);
					gpuSolver_vs_flex (metaheuristic.NEIIni,vectors_e->files,vectors_e->nlig,vectors_e->ligtype+(desplazamiento*vectors_e->nlig),vectors_e->bonds+(desplazamiento*vectors_e->nlig*MAXBOND),vectors_e->ql+(desplazamiento*vectors_e->nlig),ligand_params,flexibility_params,proteina,param,metaheuristic,devices,vectors_e->conformations_x+(desplazamiento*vectors_e->nlig), vectors_e->conformations_y+(desplazamiento*vectors_e->nlig), vectors_e->conformations_z+(desplazamiento*vectors_e->nlig),vectors_e->move_x+desplazamiento,vectors_e->move_y+desplazamiento,vectors_e->move_z+desplazamiento,vectors_e->quat_x+desplazamiento,vectors_e->quat_y+desplazamiento,vectors_e->quat_z+desplazamiento,vectors_e->quat_w+desplazamiento,vectors_e->energy.energy+desplazamiento,vectors_e->energy.n_conformation+desplazamiento,vectors_e->weights,vectors_e->f_params,max_conformaciones,0,desplazamiento);
					
					//hh++;
					//if (hh == 2) break;
				}
				//exit(0);
			}			
			tenergycpu_t += wtime() - tenergycpu;
			//for (int i=0;i<vectors_e->nconformations;i++) printf("id %d m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f energy %f\n",i,vectors_e->move_x[i],vectors_e->move_y[i],vectors_e->move_z[i],vectors_e->quat_x[i],vectors_e->quat_y[i],vectors_e->quat_z[i], vectors_e->energy.energy[i]);
			//exit(0);
			
			tordenarcpu = wtime();
			omp_set_num_threads (metaheuristic.Threads1Ini);
			#pragma omp parallel for			
			for (unsigned int i = 0; i < vectors_e->nconformations; i += metaheuristic.NEIIni)							
			    thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + metaheuristic.NEIIni), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());		
			//exit(0);	
			//colocar_vs_flex(param,vectors_e,metaheuristic);

			#pragma omp parallel for
                        for (unsigned int i = 0; i < vectors_e->files; i++)
				colocar_vs_flex_by_step (param,metaheuristic,vectors_e->nlig,vectors_e->conformations_x + (metaheuristic.NEIIni*i*vectors_e->nlig),vectors_e->conformations_y + (metaheuristic.NEIIni*i*vectors_e->nlig),vectors_e->conformations_z + (metaheuristic.NEIIni*i*vectors_e->nlig),vectors_e->move_x + (metaheuristic.NEIIni*i),vectors_e->move_y + (metaheuristic.NEIIni*i),vectors_e->move_z + (metaheuristic.NEIIni*i),vectors_e->quat_x + (metaheuristic.NEIIni*i),vectors_e->quat_y + (metaheuristic.NEIIni*i),vectors_e->quat_z + (metaheuristic.NEIIni*i),vectors_e->quat_w + (metaheuristic.NEIIni*i),vectors_e->energy.energy + (metaheuristic.NEIIni*i),vectors_e->energy.n_conformation + (metaheuristic.NEIIni*i),metaheuristic.NEIIni,i);
			thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			tordenarcpu_t += wtime() - tordenarcpu;		
			//exit(0);
		//for (int i=0;i<vectors_e->nconformations;i++) printf("id %d m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f energy %f\n",i,vectors_e->move_x[i],vectors_e->move_y[i],vectors_e->move_z[i],vectors_e->quat_x[i],vectors_e->quat_y[i],vectors_e->quat_z[i], vectors_e->energy.energy[i]);	
			if (metaheuristic.PEMIni > 0)
			{
				tmejorainicpu = wtime(); //tomamos tiempos de mejora
				seleccionar_inicializar_vs(vectors_e->nlig,vectors_e,&vectors_selec,param,metaheuristic);
				//exit(0);
				if (metaheuristic.NEEImp > 0)
				{
					printf("entra\n");	
					if (param.montecarlo)
						montecarlo_vs_flex_environment(proteina,ligand_params,param,&vectors_selec,metaheuristic,devices,flexibility_params,0,0);
					else
						gpu_mejorar_vs_flex_environment (proteina,ligand_params,flexibility_params, param,&vectors_selec,metaheuristic,devices,0,vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.ql,0);						
				}
				else
				{
					if (param.montecarlo)
                                                montecarlo_vs_flex(proteina,ligand_params,param,&vectors_selec,metaheuristic,devices,flexibility_params,0,0);
                                        else
                                                gpu_mejorar_vs_flex (proteina,ligand_params,flexibility_params, param,&vectors_selec,metaheuristic,devices,0,vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.ql,0);
				}
				//exit(0);
				//incluir_gpu_vs_flex (vectors_e->nlig,&vectors_selec,vectors_e,param,metaheuristic);
				tmejorainicpu_t += wtime() - tmejorainicpu;				
				//for (int i=0;i<vectors_selec.nconformations;i++) printf("energy_selec [%d] = %f\n",i,vectors_selec.energy.energy[i]);
				//for (int i=0;i<vectors_e->nconformations;i++) printf("energy [%d] = %f\n",i,vectors_e->energy.energy[i]);
				//exit(0);	
				//for (int i=0;i<vectors_selec.nconformations*vectors_selec.nlig;i++) if (vectors_selec.energy.energy[i/vectors_selec.nlig] < -488.0) printf("id %d x %f y %f z%f m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f energy %f\n",i/vectors_selec.nlig,vectors_selec.conformations_x[i],vectors_selec.conformations_y[i],vectors_selec.conformations_z[i],vectors_selec.move_x[i/vectors_selec.nlig],vectors_selec.move_y[i/vectors_selec.nlig],vectors_selec.move_z[i/vectors_selec.nlig],vectors_selec.quat_x[i/vectors_selec.nlig],vectors_selec.quat_y[i/vectors_selec.nlig],vectors_selec.quat_z[i/vectors_selec.nlig], vectors_selec.energy.energy[i/vectors_selec.nlig]);

				stride_e = vectors_selec.nconformations / vectors_selec.files;
                                stride_s = vectors_e->nconformations / vectors_e->files;
				//printf("stride_e %d stride_s %d %d %d %d %d\n",stride_e,stride_s,vectors_selec.files,vectors_selec.nconformations,vectors_e->nconformations,vectors_selec.nlig);
                                #pragma omp parallel for
                                for (unsigned int i = 0; i < vectors_e->files; i++)
				{
					//struct vectors_t v;
                                        incluir_gpu_vs_flex_by_step (vectors_selec.nlig,vectors_selec.conformations_x + (stride_e*vectors_selec.nlig*i), vectors_selec.conformations_y + (stride_e*vectors_selec.nlig*i), vectors_selec.conformations_z + (stride_e*vectors_selec.nlig*i), vectors_selec.move_x + (stride_e*i), vectors_selec.move_y + (stride_e*i), vectors_selec.move_z + (stride_e*i), vectors_selec.quat_x + (stride_e*i), vectors_selec.quat_y + (stride_e*i), vectors_selec.quat_z + (stride_e*i), vectors_selec.quat_w + (stride_e*i), vectors_selec.energy.energy + (stride_e*i), vectors_selec.energy.n_conformation + (stride_e*i), (vectors_e->conformations_x + stride_s*vectors_e->nlig*i), vectors_e->conformations_y + (stride_s*vectors_e->nlig*i), vectors_e->conformations_z + (stride_s*vectors_e->nlig*i), vectors_e->move_x + (stride_s*i),  vectors_e->move_y + (stride_s*i),  vectors_e->move_z + (stride_s*i),  vectors_e->quat_x + (stride_s*i),  vectors_e->quat_y + (stride_s*i),  vectors_e->quat_z + (stride_s*i),  vectors_e->quat_w + (stride_s*i),  vectors_e->energy.energy + (stride_s*i),  vectors_e->energy.n_conformation + (stride_s*i), stride_e, stride_s, i,param,metaheuristic);
				}
                                thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
				//for (int i=0;i<vectors_e->nconformations*vectors_e->nlig;i++) if (vectors_e->energy.energy[i/vectors_selec.nlig] < -488.0) printf("id %d x %f y %f z%f m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f energy %f\n",i/vectors_e->nlig,vectors_e->conformations_x[i],vectors_e->conformations_y[i],vectors_e->conformations_z[i],vectors_e->move_x[i/vectors_selec.nlig],vectors_e->move_y[i/vectors_selec.nlig],vectors_e->move_z[i/vectors_selec.nlig],vectors_e->quat_x[i/vectors_selec.nlig],vectors_e->quat_y[i/vectors_selec.nlig],vectors_e->quat_z[i/vectors_e->nlig], vectors_e->energy.energy[i/vectors_selec.nlig]);
				//for (int i=0;i<vectors_e->nconformations;i++) printf("id %d m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f energy %f\n",i,vectors_e->move_x[i],vectors_e->move_y[i],vectors_e->move_z[i],vectors_e->quat_x[i],vectors_e->quat_y[i],vectors_e->quat_z[i], vectors_e->energy.energy[i]);				
				//#pragma omp parallel for
				//for (unsigned int i = 0; i < vectors_e->nconformations; i += metaheuristic.NEIIni)			
				//	thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + metaheuristic.NEIIni), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());										
				//colocar_vs_flex(param,vectors_e,metaheuristic);					
				//#pragma omp parallel for	
				//for (unsigned int i = 0; i < vectors_e->files;i++)
				//	colocar_vs_flex_by_step (param,metaheuristic,vectors_e->nlig,vectors_e->conformations_x + (metaheuristic.NEIIni*i*vectors_e->nlig),vectors_e->conformations_y + (metaheuristic.NEIIni*i*vectors_e->nlig),vectors_e->conformations_z + (metaheuristic.NEIIni*i*vectors_e->nlig),vectors_e->move_x + (metaheuristic.NEIIni*i),vectors_e->move_y + (metaheuristic.NEIIni*i),vectors_e->move_z + (metaheuristic.NEIIni*i),vectors_e->quat_x + (metaheuristic.NEIIni*i),vectors_e->quat_y + (metaheuristic.NEIIni*i),vectors_e->quat_z + (metaheuristic.NEIIni*i),vectors_e->quat_w + (metaheuristic.NEIIni*i),vectors_e->energy.energy + (metaheuristic.NEIIni*i),vectors_e->energy.n_conformation + (metaheuristic.NEIIni*i),metaheuristic.NEIIni,i);
				//thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			}
			//for (int i=0;i<vectors_e->nconformations;i++) printf("id %d m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f energy %f\n",i,vectors_e->move_x[i],vectors_e->move_y[i],vectors_e->move_z[i],vectors_e->quat_x[i],vectors_e->quat_y[i],vectors_e->quat_z[i], vectors_e->energy.energy[i]);
			//exit(0);
			
			seleccionar_NEFIni_vs(vectors_e->nlig,vectors_e,vectors_s,metaheuristic,param);
			printf("\nFases INICILIZAR:\n");
			printf("\tTiempo Generacion posiciones: %lf seg\n",tgenerarposi_t);				
			printf("\tTiempo ES+VDW+HB potencial: %lf seg\n",tenergycpu_t);	
			printf("\tTiempo Ordenar: %lf seg\n",tordenarcpu_t);
			if (metaheuristic.PEMIni > 0) printf("\tTiempo Mejorar: %lf seg\n",tmejorainicpu_t);	
			printf("\tTiempo Colocar: %lf seg\n",tcolocarcpu_t);		
			//for (int i=0;i<vectors_s->nconformations;i++) printf("id %d m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f energy %f\n",i,vectors_s->move_x[i],vectors_s->move_y[i],vectors_s->move_z[i],vectors_s->quat_x[i],vectors_s->quat_y[i],vectors_s->quat_z[i], vectors_s->energy.energy[i]);
			//exit(0);
			break;			
			
		case 1:			
		case 2:			
			//OPEN-MP SELECTION
			//CPU SELECTION
			//printf("files %d %d\n",vectors_e->files, vectors_e->nlig);
			switch (param.mode) {
				case 1:
					th1 = 1;
					break;
				case 0:
				case 2:
				case 3:
					th1 = metaheuristic.Threads1Ini;
					break;
			}
			
			vectors_e->nconformations = vectors_e->files * metaheuristic.NEIIni;
			vectors_e->conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->ql = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->ligtype = (char *)malloc(sizeof(char)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->subtype = (char *)malloc(sizeof(char)*vectors_e->nlig*SUBTYPEMAXLEN*vectors_e->nconformations);
			vectors_e->bonds = (unsigned int *)malloc(sizeof(unsigned int)*vectors_e->nlig*MAXBOND*vectors_e->nconformations);
			vectors_e->nbonds = (char *)malloc(sizeof(char)*vectors_e->nlig*vectors_e->nconformations);
			
			vectors_e->move_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->move_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->move_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_e->nconformations);
			vectors_e->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			internal_energy = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			flexibility_conformations = (flexibility_data_t *)malloc(sizeof(flexibility_data_t)*vectors_e->nconformations);
			fill_conformations_vs(param,metaheuristic,c_x,c_y,c_z,ql,ligtype,subtype,nbonds,bonds,vectors_e);
			tgenerarposi = wtime();//tomamos tiempos
			
			//for (int i=0;i<vectors_e->files;i++) printf("nlinks : %d\n",flexibility_params[i].n_links);
			//exit(0);
			generate_positions_cpp_vs_flex (param,vectors_e,metaheuristic,flexibility_params,flexibility_conformations);
			tgenerarposi_t += wtime() - tgenerarposi;
			//for (int i=0;i<vectors_e->nconformations;i++) printf("id %d m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f\n",i,vectors_e->move_x[i],vectors_e->move_y[i],vectors_e->move_z[i],vectors_e->quat_x[i],vectors_e->quat_y[i],vectors_e->quat_z[i]);
			//exit(0);
			tenergycpu = wtime();//tomamos tiempos para calculo		
			//printf("energia \n");	
			ForcesCpuSolver_vs(nlinks,vectors_e->nlig,vectors_e->files,param,proteina,ligand_params,vectors_e->conformations_x,vectors_e->conformations_y,vectors_e->conformations_z,vectors_e->move_x,vectors_e->move_y,vectors_e->move_z,vectors_e->quat_x,vectors_e->quat_y,vectors_e->quat_z,vectors_e->quat_w,vectors_e->ligtype,vectors_e->ql,vectors_e->bonds,vectors_e->energy.energy,vectors_e->energy.n_conformation,vectors_e->weights,vectors_e->f_params,vectors_e->nconformations,metaheuristic);
			energy_internal_conformations_cpu_vs (vectors_e->nconformations,vectors_e->nlig,vectors_e->files,param,metaheuristic,vectors_e->conformations_x,vectors_e->conformations_y,vectors_e->conformations_z,ligand_params,flexibility_params,vectors_e->weights,vectors_e->f_params,internal_energy,vectors_e->ql,vectors_e->ligtype);
			energy_total (vectors_e->nconformations,param,metaheuristic,vectors_e->energy.energy,internal_energy);
			tenergycpu_t += wtime() - tenergycpu;
			//for (int i=0;i<vectors_e->nconformations;i++) printf("id %d m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f energy %f\n",i,vectors_e->move_x[i],vectors_e->move_y[i],vectors_e->move_z[i],vectors_e->quat_x[i],vectors_e->quat_y[i],vectors_e->quat_z[i], vectors_e->energy.energy[i]);
			//exit(0);
		
			omp_set_num_threads (th1);
			#pragma omp parallel for									
			for (unsigned int i = 0; i < vectors_e->nconformations; i += metaheuristic.NEIIni)							
			    thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + metaheuristic.NEIIni), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());			
			//colocar_vs_flex(param,vectors_e,metaheuristic);
			#pragma omp parallel for
                        for (unsigned int i = 0; i < vectors_e->files; i++)
                                colocar_vs_flex_by_step (param,metaheuristic,vectors_e->nlig,vectors_e->conformations_x + (metaheuristic.NEIIni*i*vectors_e->nlig),vectors_e->conformations_y + (metaheuristic.NEIIni*i*vectors_e->nlig),vectors_e->conformations_z + (metaheuristic.NEIIni*i*vectors_e->nlig),vectors_e->move_x + (metaheuristic.NEIIni*i),vectors_e->move_y + (metaheuristic.NEIIni*i),vectors_e->move_z + (metaheuristic.NEIIni*i),vectors_e->quat_x + (metaheuristic.NEIIni*i),vectors_e->quat_y + (metaheuristic.NEIIni*i),vectors_e->quat_z + (metaheuristic.NEIIni*i),vectors_e->quat_w + (metaheuristic.NEIIni*i),vectors_e->energy.energy + (metaheuristic.NEIIni*i),vectors_e->energy.n_conformation + (metaheuristic.NEIIni*i),metaheuristic.NEIIni,i);
                        thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			
			//for (int i=0;i<vectors_e->nconformations;i++) printf("id %d m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f energy %f\n",i,vectors_e->move_x[i],vectors_e->move_y[i],vectors_e->move_z[i],vectors_e->quat_x[i],vectors_e->quat_y[i],vectors_e->quat_z[i], vectors_e->energy.energy[i]);
                        //exit(0);
			if (metaheuristic.PEMIni > 0)
			{		
				//printf("MEJORANDO %d\n");	
				tmejorainicpu = wtime(); //tomamos tiempos de mejora
				seleccionar_inicializar_vs(vectors_e->nlig,vectors_e,&vectors_selec,param,metaheuristic);
				if (metaheuristic.NEEImp > 0)
				{
					if (param.montecarlo)
                                                montecarlo_cpp_vs_flex_environment (proteina,ligand_params,param,vectors_selec.weights, vectors_selec.f_params, vectors_selec.conformations_x, vectors_selec.conformations_y, vectors_selec.conformations_z, vectors_selec.move_x, vectors_selec.move_y, vectors_selec.move_z, vectors_selec.quat_x, vectors_selec.quat_y, vectors_selec.quat_z, vectors_selec.quat_w, vectors_selec.energy.energy, vectors_selec.energy.n_conformation,vectors_selec.ql,vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.nlig,vectors_selec.files,vectors_selec.nconformations,metaheuristic,flexibility_params,flexibility_conformations,0);
                                        else
                                                mejorar_vs_flex_environment (proteina,ligand_params,param,vectors_selec.weights, vectors_selec.f_params, vectors_selec.conformations_x, vectors_selec.conformations_y, vectors_selec.conformations_z, vectors_selec.move_x, vectors_selec.move_y, vectors_selec.move_z, vectors_selec.quat_x, vectors_selec.quat_y, vectors_selec.quat_z, vectors_selec.quat_w, vectors_selec.energy.energy, vectors_selec.energy.n_conformation,vectors_selec.ql,vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.nlig,vectors_selec.files,vectors_selec.nconformations,metaheuristic,flexibility_params,flexibility_conformations,0);
				}
				else
				{	
					if (param.montecarlo)		
						montecarlo_cpp_vs_flex(proteina,ligand_params,param,vectors_selec.weights, vectors_selec.f_params, vectors_selec.conformations_x, vectors_selec.conformations_y, vectors_selec.conformations_z, vectors_selec.move_x, vectors_selec.move_y, vectors_selec.move_z, vectors_selec.quat_x, vectors_selec.quat_y, vectors_selec.quat_z, vectors_selec.quat_w, vectors_selec.energy.energy, vectors_selec.energy.n_conformation,vectors_selec.ql,vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.nlig,vectors_selec.files,vectors_selec.nconformations,metaheuristic,flexibility_params,flexibility_conformations,0);						
					else
						mejorar_vs_flex(proteina,ligand_params,param,vectors_selec.weights, vectors_selec.f_params, vectors_selec.conformations_x, vectors_selec.conformations_y, vectors_selec.conformations_z, vectors_selec.move_x, vectors_selec.move_y, vectors_selec.move_z, vectors_selec.quat_x, vectors_selec.quat_y, vectors_selec.quat_z, vectors_selec.quat_w, vectors_selec.energy.energy, vectors_selec.energy.n_conformation,vectors_selec.ql,vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.nlig,vectors_selec.files,vectors_selec.nconformations,metaheuristic,flexibility_params,flexibility_conformations,0);
				}
				//for (int i=0;i<vectors_selec.nconformations;i++) printf("id %d m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f energy %f\n",i,vectors_selec.move_x[i],vectors_selec.move_y[i],vectors_selec.move_z[i],vectors_selec.quat_x[i],vectors_selec.quat_y[i],vectors_selec.quat_z[i], vectors_selec.energy.energy[i]);
                        	//exit(0);
				tmejorainicpu_t += wtime() - tmejorainicpu;	
				//incluir_gpu_vs_flex (vectors_e->nlig,&vectors_selec,vectors_e,param,metaheuristic);
				stride_e = vectors_selec.nconformations / vectors_selec.files;
                                stride_s = vectors_e->nconformations / vectors_e->files;
				#pragma omp parallel for
                                for (unsigned int i = 0; i < vectors_e->files; i++)
				{
					struct vectors_t v;
                                        incluir_gpu_vs_flex_by_step (vectors_selec.nlig,vectors_selec.conformations_x + (stride_e*vectors_selec.nlig*i), vectors_selec.conformations_y + (stride_e*vectors_selec.nlig*i), vectors_selec.conformations_z + (stride_e*vectors_selec.nlig*i), vectors_selec.move_x + (stride_e*i), vectors_selec.move_y + (stride_e*i), vectors_selec.move_z + (stride_e*i), vectors_selec.quat_x + (stride_e*i), vectors_selec.quat_y + (stride_e*i), vectors_selec.quat_z + (stride_e*i), vectors_selec.quat_w + (stride_e*i), vectors_selec.energy.energy + (stride_e*i), vectors_selec.energy.n_conformation + (stride_e*i), (vectors_e->conformations_x + stride_s*vectors_e->nlig*i), vectors_e->conformations_y + (stride_s*vectors_e->nlig*i), vectors_e->conformations_z + (stride_s*vectors_e->nlig*i), vectors_e->move_x + (stride_s*i),  vectors_e->move_y + (stride_s*i),  vectors_e->move_z + (stride_s*i),  vectors_e->quat_x + (stride_s*i),  vectors_e->quat_y + (stride_s*i),  vectors_e->quat_z + (stride_s*i),  vectors_e->quat_w + (stride_s*i),  vectors_e->energy.energy + (stride_s*i),  vectors_e->energy.n_conformation + (stride_s*i), stride_e, stride_s, i,param,metaheuristic);
				}
				thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
				//omp_set_num_threads (th1);
				//#pragma omp parallel for									
				//for (unsigned int i = 0; i < vectors_e->nconformations; i += metaheuristic.NEIIni)			
				//	thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + metaheuristic.NEIIni), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());										
				//colocar_vs_flex(param,vectors_e,metaheuristic);
				//#pragma omp parallel for
				//for (unsigned int i = 0; i < vectors_e->files; i++)
				//	colocar_vs_flex_by_step (param,metaheuristic,vectors_e->nlig,vectors_e->conformations_x + (metaheuristic.NEIIni*i*vectors_e->nlig),vectors_e->conformations_y + (metaheuristic.NEIIni*i*vectors_e->nlig),vectors_e->conformations_z + (metaheuristic.NEIIni*i*vectors_e->nlig),vectors_e->move_x + (metaheuristic.NEIIni*i),vectors_e->move_y + (metaheuristic.NEIIni*i),vectors_e->move_z + (metaheuristic.NEIIni*i),vectors_e->quat_x + (metaheuristic.NEIIni*i),vectors_e->quat_y + (metaheuristic.NEIIni*i),vectors_e->quat_z + (metaheuristic.NEIIni*i),vectors_e->quat_w + (metaheuristic.NEIIni*i),vectors_e->energy.energy + (metaheuristic.NEIIni*i),vectors_e->energy.n_conformation + (metaheuristic.NEIIni*i),metaheuristic.NEIIni,i);
				//thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
											
				//for(int k=0;k<160;k++)
				//		printf("STP %d Mejorar VS-number %d, m_x %f, m_y %f, m_z %f, energy %f\n",steps,k,vectors_s->move_x[k],vectors_s->move_y[k],vectors_s->move_z[k],vectors_s->energy.energy[k]);
		
			}	
			//for (int i=0;i<vectors_e->nconformations;i++) printf("id %d m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f en %f\n",i,vectors_e->move_x[i],vectors_e->move_y[i],vectors_e->move_z[i],vectors_e->quat_x[i],vectors_e->quat_y[i],vectors_e->quat_z[i],vectors_e->energy.energy[i]);
			//printf("FIN\n");
			//exit(0);
			
			seleccionar_NEFIni_vs(vectors_e->nlig,vectors_e,vectors_s,metaheuristic,param);
			printf("\nFases INICILIZAR:\n");
			printf("\tTiempo Generacion posiciones: %lf seg\n",tgenerarposi_t);				
			printf("\tTiempo ES+VDW+HB potencial: %lf seg\n",tenergycpu_t);	
			printf("\tTiempo Ordenar: %lf seg\n",tordenarcpu_t);
			if (metaheuristic.PEMIni > 0) printf("\tTiempo Mejorar: %lf seg\n",tmejorainicpu_t);	
			printf("\tTiempo Colocar: %lf seg\n",tcolocarcpu_t);
			
			break;
			
		 case 3:
			//MULTIGPU SELECTION
			int n_devices,k;
			//NEItemp	= metaheuristic.NEIIni;
			//NEItemp = getPadding(NEItemp,16);
			//metaheuristic.NEIIni += NEItemp;
			NEI_selec_temp = (unsigned int)ceil((metaheuristic.NEIIni * metaheuristic.PEMIni) / 100);
			metaheuristic.NEIIni_selec = NEI_selec_temp;// + getPadding(NEI_selec_temp,16);

			vectors_e->nconformations = vectors_e->files * metaheuristic.NEIIni;
			vectors_e->conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->ql = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->ligtype = (char *)malloc(sizeof(char)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->subtype = (char *)malloc(sizeof(char)*vectors_e->nlig*SUBTYPEMAXLEN*vectors_e->nconformations);
			vectors_e->bonds = (unsigned int *)malloc(sizeof(unsigned int)*vectors_e->nlig*MAXBOND*vectors_e->nconformations);
			vectors_e->nbonds = (char *)malloc(sizeof(char)*vectors_e->nlig*vectors_e->nconformations);
			
			vectors_e->move_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->move_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->move_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_e->nconformations);
			vectors_e->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			//for (int i=0;i<20;i++) printf("i %d, eps %f, sig %f\n",i,vectors_e->vdw_params[i].epsilon,vectors_e->vdw_params[i].sigma);
			internal_energy = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			//flexibility_conformations = (flexibility_data_t *)malloc(sizeof(flexibility_data_t)*vectors_e->nconformations);
			//printf("Rellenando..\n");
			//exit(0);
			fill_conformations_vs(param,metaheuristic,c_x,c_y,c_z,ql,ligtype,subtype,nbonds,bonds,vectors_e);
			tgenerarposi = wtime();//tomamos tiempos
			//for (int i=0;i<vectors_e->nconformations*vectors_e->nlig;i++) printf("id %d c_x %f c_y %f c_z %f \n",i/vectors_e->nlig,vectors_e->conformations_x[i],vectors_e->conformations_y[i],vectors_e->conformations_z[i]);
			//exit(0);
			//printf("Generando...\n");
			//exit(0);
			generate_positions_vs_flex (vectors_e->nlig,ligand_params,param,vectors_e,metaheuristic,devices,devices->id[0],flexibility_params,0);
			//exit(0);
			tgenerarposi_t += wtime() - tgenerarposi;
			//for (int i=0;i<vectors_e->nconformations;i++) printf("id %d m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f\n",i,vectors_e->move_x[i],vectors_e->move_y[i],vectors_e->move_z[i],vectors_e->quat_x[i],vectors_e->quat_y[i],vectors_e->quat_z[i]);
			//exit(0);
			
			tenergycpu = wtime();//tomamos tiempos para calculo L-J
			multigpuSolver_vs_flex (vectors_e->files,vectors_e->nlig,vectors_e->ligtype,vectors_e->bonds,vectors_e->ql,ligand_params,flexibility_params,proteina,vectors_e->conformations_x,vectors_e->conformations_y,vectors_e->conformations_z,vectors_e->move_x,vectors_e->move_y,vectors_e->move_z,vectors_e->quat_x,vectors_e->quat_y,vectors_e->quat_z,vectors_e->quat_w,vectors_e->energy,vectors_e->weights,vectors_e->f_params,vectors_e->nconformations,param,metaheuristic,devices);
			
			tenergycpu_t += wtime() - tenergycpu;
			//exit(0);	
			tordenarcpu = wtime();
			omp_set_num_threads (metaheuristic.Threads1Ini);
			#pragma omp parallel for			
			for (unsigned int i = 0; i < vectors_e->nconformations; i += metaheuristic.NEIIni)							
			    thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + metaheuristic.NEIIni), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());			
			//colocar_vs_flex(param,vectors_e,metaheuristic);
			#pragma omp parallel for
                        for (unsigned int i = 0; i < vectors_e->files; i++)
                                colocar_vs_flex_by_step (param,metaheuristic,vectors_e->nlig,vectors_e->conformations_x + (metaheuristic.NEIIni*i*vectors_e->nlig),vectors_e->conformations_y + (metaheuristic.NEIIni*i*vectors_e->nlig),vectors_e->conformations_z + (metaheuristic.NEIIni*i*vectors_e->nlig),vectors_e->move_x + (metaheuristic.NEIIni*i),vectors_e->move_y + (metaheuristic.NEIIni*i),vectors_e->move_z + (metaheuristic.NEIIni*i),vectors_e->quat_x + (metaheuristic.NEIIni*i),vectors_e->quat_y + (metaheuristic.NEIIni*i),vectors_e->quat_z + (metaheuristic.NEIIni*i),vectors_e->quat_w + (metaheuristic.NEIIni*i),vectors_e->energy.energy + (metaheuristic.NEIIni*i),vectors_e->energy.n_conformation + (metaheuristic.NEIIni*i),metaheuristic.NEIIni,i);
                        thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			tordenarcpu_t += wtime() - tordenarcpu;
			
			if (metaheuristic.PEMIni > 0)
			{
				tmejorainicpu = wtime(); //tomamos tiempos de mejora
				seleccionar_inicializar_vs(vectors_e->nlig,vectors_e,&vectors_selec,param,metaheuristic);	
				multigpuSolver_mejora_vs_flex (vectors_selec.files,vectors_selec.nlig,vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.ql,ligand_params,flexibility_params,proteina,vectors_selec.conformations_x,vectors_selec.conformations_y,vectors_selec.conformations_z,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z,vectors_selec.quat_x,vectors_selec.quat_y,vectors_selec.quat_z,vectors_selec.quat_w,vectors_selec.energy.energy,vectors_selec.energy.n_conformation,vectors_selec.weights,vectors_selec.f_params,vectors_selec.nconformations,param,metaheuristic,devices,0);
				//incluir_gpu_vs_flex (vectors_e->nlig,&vectors_selec,vectors_e,param,metaheuristic);
				stride_e = vectors_selec.nconformations / vectors_selec.files;
                                stride_s = vectors_e->nconformations / vectors_e->files;
                                #pragma omp parallel for
                                for (unsigned int i = 0; i < vectors_e->files; i++)
				{
					struct vectors_t v;
                                        incluir_gpu_vs_flex_by_step (vectors_selec.nlig,vectors_selec.conformations_x + (stride_e*vectors_selec.nlig*i), vectors_selec.conformations_y + (stride_e*vectors_selec.nlig*i), vectors_selec.conformations_z + (stride_e*vectors_selec.nlig*i), vectors_selec.move_x + (stride_e*i), vectors_selec.move_y + (stride_e*i), vectors_selec.move_z + (stride_e*i), vectors_selec.quat_x + (stride_e*i), vectors_selec.quat_y + (stride_e*i), vectors_selec.quat_z + (stride_e*i), vectors_selec.quat_w + (stride_e*i), vectors_selec.energy.energy + (stride_e*i), vectors_selec.energy.n_conformation + (stride_e*i), vectors_e->conformations_x + (stride_s*vectors_e->nlig*i), vectors_e->conformations_y + (stride_s*vectors_e->nlig*i), vectors_e->conformations_z + (stride_s*vectors_e->nlig*i), vectors_e->move_x + (stride_s*i),  vectors_e->move_y + (stride_s*i),  vectors_e->move_z + (stride_s*i),  vectors_e->quat_x + (stride_s*i),  vectors_e->quat_y + (stride_s*i),  vectors_e->quat_z + (stride_s*i),  vectors_e->quat_w + (stride_s*i),  vectors_e->energy.energy + (stride_s*i),  vectors_e->energy.n_conformation + (stride_s*i), stride_e, stride_s, i,param,metaheuristic);
				}
                                thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);	
				tmejorainicpu_t += wtime() - tmejorainicpu;				

				//#pragma omp parallel for
				//for (unsigned int i = 0; i < vectors_e->nconformations; i += metaheuristic.NEIIni)			
				//	thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + metaheuristic.NEIIni), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());										
				//colocar_vs_flex(param,vectors_e,metaheuristic);						
				//#pragma omp parallel for
				//for (unsigned int i = 0; i < vectors_e->files; i++)
				//	colocar_vs_flex_by_step (param,metaheuristic,vectors_e->nlig,vectors_e->conformations_x + (metaheuristic.NEIIni*i*vectors_e->nlig),vectors_e->conformations_y + (metaheuristic.NEIIni*i*vectors_e->nlig),vectors_e->conformations_z + (metaheuristic.NEIIni*i*vectors_e->nlig),vectors_e->move_x + (metaheuristic.NEIIni*i),vectors_e->move_y + (metaheuristic.NEIIni*i),vectors_e->move_z + (metaheuristic.NEIIni*i),vectors_e->quat_x + (metaheuristic.NEIIni*i),vectors_e->quat_y + (metaheuristic.NEIIni*i),vectors_e->quat_z + (metaheuristic.NEIIni*i),vectors_e->quat_w + (metaheuristic.NEIIni*i),vectors_e->energy.energy + (metaheuristic.NEIIni*i),vectors_e->energy.n_conformation + (metaheuristic.NEIIni*i),metaheuristic.NEIIni,i);
				//thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			
			}
			seleccionar_NEFIni_vs(vectors_e->nlig,vectors_e,vectors_s,metaheuristic,param);
			printf("\nFases INICILIZAR:\n");
			printf("\tTiempo Generacion posiciones: %lf seg\n",tgenerarposi_t);				
			printf("\tTiempo ES+VDW+HB potencial: %lf seg\n",tenergycpu_t);	
			printf("\tTiempo Ordenar: %lf seg\n",tordenarcpu_t);
			if (metaheuristic.PEMIni > 0) printf("\tTiempo Mejorar: %lf seg\n",tmejorainicpu_t);	
			printf("\tTiempo Colocar: %lf seg\n",tcolocarcpu_t);
			
			break;
	}
	
	if (metaheuristic.PEMIni > 0)
	{	
		free(vectors_selec.conformations_x);
		free(vectors_selec.conformations_y);
		free(vectors_selec.conformations_z);
		free(vectors_selec.move_x);
		free(vectors_selec.move_y);
		free(vectors_selec.move_z);
		free(vectors_selec.quat_x);
		free(vectors_selec.quat_y);
		free(vectors_selec.quat_z);
		free(vectors_selec.quat_w);
		free(vectors_selec.energy.energy);
		free(vectors_selec.energy.n_conformation);
		free(vectors_selec.weights);
	}
}


