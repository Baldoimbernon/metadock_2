#include <omp.h>
#include "wtime.h"
#include "type.h"
#include "energy_struct.h"
#include "energy_mutation_vs.h"
#include "energy_CpuSolver_vs.h"
#include "metaheuristic_ini_vs.h"
#include "energy_mutation_cpp_vs.h"
#include "energy_CpuSolver_common.h"
#include "montecarlo_vs.h"
#include "rotation.h"


extern void mutation_cpp_vs (struct receptor_t proteina, struct param_t param, struct metaheuristic_t metaheuristic, struct ligand_params_t *ligand_params, type_data *weights, struct force_field_param_t *f_params,type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int *bonds, char *ligtype, type_data *ql, unsigned int nlig, unsigned int files, unsigned int nconformations)
{
	unsigned int *n_update;
	n_update = (unsigned int *)malloc(sizeof(unsigned int)*nconformations);
	unsigned int *nlinks = (unsigned int *)calloc(files,sizeof(unsigned int));
	mutation_random_cpp_vs (metaheuristic,nconformations,param,n_update);
	mutation_calculation_cpp_vs (param,metaheuristic,n_update,param.seed,param.automatic_seed,param.max_desp,param.rotation,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,nconformations);

	ForcesCpuSolver_vs(nlinks,nlig,files,param,proteina,ligand_params,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,ligtype,ql,bonds,energy,nconfs,weights,f_params,nconformations,metaheuristic);

	if (metaheuristic.IMEMUCom > 0)
        {
                if (param.montecarlo)
					montecarlo_cpp_vs(proteina,ligand_params,param,weights,f_params,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,ql,ligtype,bonds,nlig,files,nconformations,metaheuristic,2);
                else
					mejorar_vs(proteina,ligand_params,param,weights,f_params,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,ql,ligtype,bonds,nlig,files,nconformations,metaheuristic,2);
        }

	free(n_update);
	free(nlinks);

}
extern void mutation_calculation_cpp_vs (struct param_t param, struct metaheuristic_t metaheuristic, unsigned int *n_update, int seed, int automatic_seed, type_data max_desp, type_data rotation, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, unsigned int nconformations)
{
 	unsigned int th1;
        type_data angle, eje[3];
        type_data local_x, quat_tmp_x, local_y, quat_tmp_y, local_z, quat_tmp_z, local_w, quat_tmp_w ;

        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 0:
                case 3:
                case 2:
                        th1 = metaheuristic.Threads1Ini;
                        break;
        }
        omp_set_num_threads (th1);
	#pragma omp parallel for private(quat_tmp_x,quat_tmp_y,quat_tmp_z,quat_tmp_w,eje,angle,local_x,local_y,local_z,local_w) 
        for (unsigned int id_conformation=0; id_conformation < nconformations; id_conformation++)
        {
                switch (n_update[id_conformation])
                {
                	case 0:
                        	//MOVE X
                        	move_x[id_conformation] += getRealRandomNumber_r(max_desp,automatic_seed,seed,id_conformation+seed); //Desplazamiento entre -max_desp y max_desp
                        	break;
                 	case 1:
                        	//MOVE Y
                                move_y[id_conformation] += getRealRandomNumber_r(max_desp,automatic_seed,seed,id_conformation+seed); //Desplazamiento entre -max_desp y max_desp
                                break;
			case 2:
                                //MOVE Z
                                move_z[id_conformation] += getRealRandomNumber_r(max_desp,automatic_seed,seed,id_conformation+seed); //Desplazamiento entre -max_desp y max_desp
                                break;
                        case 3:
                                //ROTATION
				quat_tmp_x = quat_x[id_conformation];
                		quat_tmp_y = quat_y[id_conformation];
                		quat_tmp_z = quat_z[id_conformation];
                		quat_tmp_w = quat_w[id_conformation];
                		angle = getRealRandomNumber(param.rotation);
                		eje[0] = getRealRandomNumber (1);
                		eje[1] = getRealRandomNumber (1);
                		eje[2] = getRealRandomNumber (1);
                		setRotation_cpp (&local_x, &local_y, &local_z, &local_w, angle, eje);
                		composeRotation_cpp (&local_x, &local_y, &local_z, &local_w, &quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w, &quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w);
                		normalize_cpp(&quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w );
                		quat_x[id_conformation] = quat_tmp_x;
                		quat_y[id_conformation] = quat_tmp_y;
                		quat_z[id_conformation] = quat_tmp_z;
                		quat_w[id_conformation] = quat_tmp_w;
				break;
		}	
	}

}

extern void mutation_random_cpp_vs (struct metaheuristic_t metaheuristic, unsigned int nconformations, struct param_t param, unsigned int *n_update)
{
        unsigned int th1;

        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 0:
                case 3:
                case 2:
                        th1 = metaheuristic.Threads1Ini;
                        break;
        }
        omp_set_num_threads (th1);
        #pragma omp parallel 
        for (unsigned int id_conformation=0; id_conformation < nconformations; id_conformation++)
        {
                n_update[id_conformation] =  getIntNumber(10000) % 4;
        }
}

extern void mutation_multigpu_vs (struct receptor_t proteina, struct ligand_params_t *ligand_params, unsigned int files, unsigned int nlig, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *weights, struct force_field_param_t *f_params, type_data *ql, char *ligtype, unsigned int *bonds, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int fase)
{

	unsigned int  pc_temp, stride_temp,ttotal = 0, dev;
        //pc_temp = nconformations / surface;
        devices->stride[0] = 0;
        //stride_temp = (int)((devices->trabajo[0] * 0.01)*surface);
        //devices->conformaciones[0] = stride_temp * pc_temp;
        //ttotal = stride_temp;

        for (unsigned int i=0;i < devices->n_devices;i++)
        {
                stride_temp = floor((devices->trabajo[i] * 0.01)*nconformations);
                devices->conformaciones[i] = stride_temp;
                ttotal += stride_temp;
        }
        unsigned int diferencia = nconformations - ttotal;
        for (unsigned int i=0;i<diferencia;i++)
        {
                dev = i % devices->n_devices;
                devices->conformaciones[dev] ++;
        }
        for (unsigned int i=(devices->n_devices-1);i > 0;i--)
        {
                devices->stride[i] = nconformations - devices->conformaciones[i];
                nconformations = nconformations - devices->conformaciones[i];
        }

	
        omp_set_num_threads(devices->n_devices);

        #pragma omp parallel for
        for (unsigned int j=0;j<devices->n_devices;j++)
        {
                //unsigned int th = omp_get_thread_num();
                unsigned int stride_d = devices->stride[j];
		mutation_gpu_vs (proteina,param,ligand_params,metaheuristic,devices,weights,f_params,conformations_x+(nlig*stride_d),conformations_y+(nlig*stride_d),conformations_z+(nlig*stride_d),move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,nconfs+stride_d,ql+(nlig*stride_d),ligtype+(nlig*stride_d),bonds+(nlig*stride_d),nlig,files,devices->conformaciones[j],j,stride_d);
	}

}
