#include "energy_cuda.h"
#include "energy_common.h"
#include "energy_CpuSolver_vs.h"
#include "energy_GpuSolver_vs.h"
#include "metaheuristic_inc_vs.h"
#include "energy_positions_vs.h"
#include "montecarlo_vs.h"
#include "energy_montecarlo_vs.h"
#include "metaheuristic_mgpu_vs.h"
#include "wtime.h"
#include <thrust/sequence.h>
#include <thrust/sort.h>
#include <thrust/functional.h>
#include <omp.h>

using namespace std;

extern void incluir_gpu_vs_by_step (unsigned int nlig, type_data *e_move_x, type_data *e_move_y, type_data *e_move_z, type_data *e_quat_x, type_data *e_quat_y, type_data *e_quat_z, type_data *e_quat_w, type_data *e_energy, unsigned int *e_nconfs, type_data *s_move_x, type_data *s_move_y, type_data *s_move_z, type_data *s_quat_x, type_data *s_quat_y, type_data *s_quat_z, type_data *s_quat_w, type_data *s_energy, unsigned int *s_nconfs, unsigned int stride_e, unsigned int stride_s, unsigned int pos,struct param_t param, struct metaheuristic_t metaheuristic)
{

	struct vectors_t *vtmp = (struct vectors_t *)malloc(sizeof(struct vectors_t));
	vtmp->nconformations = stride_e + stride_s;
	vtmp->move_x = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->move_y = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->move_z = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->quat_x = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->quat_y = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->quat_z = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->quat_w = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
	vtmp->energy.energy = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
        vtmp->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vtmp->nconformations);
	thrust::sequence(vtmp->energy.n_conformation,vtmp->energy.n_conformation + vtmp->nconformations);
        
	thrust::stable_sort_by_key(e_energy,e_energy + stride_e, e_nconfs, thrust::less<type_data>());

	colocar_vs_by_step (param,metaheuristic,nlig,e_move_x,e_move_y,e_move_z,e_quat_x,e_quat_y,e_quat_z,e_quat_w,e_energy,e_nconfs,stride_e,pos);

	memcpy(vtmp->move_x, e_move_x, stride_e * sizeof(type_data));
        memcpy(vtmp->move_x + stride_e, s_move_x, stride_s * sizeof(type_data));

        memcpy(vtmp->move_y, e_move_y, stride_e * sizeof(type_data));
        memcpy(vtmp->move_y + stride_e, s_move_y, stride_s * sizeof(type_data));

        memcpy(vtmp->move_z, e_move_z, stride_e * sizeof(type_data));
        memcpy(vtmp->move_z + stride_e, s_move_z, stride_s * sizeof(type_data));

        memcpy(vtmp->quat_x, e_quat_x, stride_e * sizeof(type_data));
	memcpy(vtmp->quat_x + stride_e, s_quat_x, stride_s * sizeof(type_data));
	
        memcpy(vtmp->quat_y, e_quat_y, stride_e * sizeof(type_data));
	memcpy(vtmp->quat_y + stride_e, s_quat_y, stride_s * sizeof(type_data));

	memcpy(vtmp->quat_z, e_quat_z, stride_e * sizeof(type_data));
        memcpy(vtmp->quat_z + stride_e, s_quat_z, stride_s * sizeof(type_data));

	memcpy(vtmp->quat_w, e_quat_w, stride_e * sizeof(type_data));
        memcpy(vtmp->quat_w + stride_e, s_quat_w, stride_s * sizeof(type_data));

        memcpy(vtmp->energy.energy, e_energy, stride_e * sizeof(type_data));
	memcpy(vtmp->energy.energy + stride_e, s_energy, stride_s * sizeof(type_data));
        
	thrust::stable_sort_by_key(vtmp->energy.energy,vtmp->energy.energy + vtmp->nconformations, vtmp->energy.n_conformation, thrust::less<type_data>());
	colocar_vs_by_step (param,metaheuristic,nlig,vtmp->move_x,vtmp->move_y,vtmp->move_z,vtmp->quat_x,vtmp->quat_y,vtmp->quat_z,vtmp->quat_w,vtmp->energy.energy,vtmp->energy.n_conformation,vtmp->nconformations,0);

        memcpy(s_move_x, vtmp->move_x, stride_s * sizeof(type_data));
	memcpy(s_move_y, vtmp->move_y, stride_s * sizeof(type_data));
	memcpy(s_move_z, vtmp->move_z, stride_s * sizeof(type_data));
	
	memcpy(s_quat_x, vtmp->quat_x, stride_s * sizeof(type_data));
	memcpy(s_quat_y, vtmp->quat_y, stride_s * sizeof(type_data));
	memcpy(s_quat_z, vtmp->quat_z, stride_s * sizeof(type_data));
	memcpy(s_quat_w, vtmp->quat_w, stride_s * sizeof(type_data));

        memcpy(s_energy, vtmp->energy.energy, stride_s * sizeof(type_data));

	free(vtmp->move_x);
        free(vtmp->move_y);
        free(vtmp->move_z);
        free(vtmp->quat_x);
        free(vtmp->quat_y);
        free(vtmp->quat_z);
        free(vtmp->quat_w);
        free(vtmp->energy.energy);
        free(vtmp->energy.n_conformation);
}
/*extern void incluir_gpu_vs (unsigned int nlig, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic)
{
	struct vectors_t *vtmp = (struct vectors_t *)malloc(sizeof(struct vectors_t));
	
	unsigned int th1;
	unsigned int stride_p_e = vectors_e->nconformations / vectors_e->files;	
	unsigned int stride_p = metaheuristic.NEIIni_selec +  metaheuristic.NEIIni; 	
	vtmp->files = vectors_e->files;	
	vtmp->nconformations = vectors_e->nconformations + vectors_s->nconformations;
	vtmp->move_x = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
	vtmp->move_y = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
	vtmp->move_z = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
	vtmp->quat_x = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);	
	vtmp->quat_y = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);	
	vtmp->quat_z = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);	
	vtmp->quat_w = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);	
	vtmp->energy.energy = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
	vtmp->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vtmp->nconformations);
	thrust::sequence(vtmp->energy.n_conformation,vtmp->energy.n_conformation + vtmp->nconformations);
	//printf("conformationes totales %d, stride_p %d\n",vtmp->nconformations,stride_p);
	//exit(0);
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 0:
		case 2:
		case 3:
			th1 = metaheuristic.Threads1Inc;
			break;
	}
	omp_set_num_threads (th1);
	#pragma omp parallel for
	for (unsigned int i = 0; i < vectors_e->nconformations; i += stride_p_e)			
		thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + stride_p_e), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());			
	colocar_vs(nlig,param,vectors_e,metaheuristic);		
		
	
	for (unsigned int i = 0;i < vectors_e->files;i++)
	{	
		
		memcpy(vtmp->move_x + i*stride_p, vectors_s->move_x + i*metaheuristic.NEIIni, metaheuristic.NEIIni * sizeof(type_data)); 
		memcpy(vtmp->move_x + i*stride_p + metaheuristic.NEIIni, vectors_e->move_x + i*metaheuristic.NEIIni_selec, metaheuristic.NEIIni_selec * sizeof(type_data));  			
		memcpy(vtmp->move_y + i*stride_p, vectors_s->move_y + i*metaheuristic.NEIIni, metaheuristic.NEIIni * sizeof(type_data)); 
		memcpy(vtmp->move_y + i*stride_p + metaheuristic.NEIIni, vectors_e->move_y + i*metaheuristic.NEIIni_selec, metaheuristic.NEIIni_selec * sizeof(type_data));  			
		memcpy(vtmp->move_z + i*stride_p, vectors_s->move_z + i*metaheuristic.NEIIni, metaheuristic.NEIIni * sizeof(type_data)); 
		memcpy(vtmp->move_z + i*stride_p + metaheuristic.NEIIni, vectors_e->move_z + i*metaheuristic.NEIIni_selec, metaheuristic.NEIIni_selec * sizeof(type_data));  			
		memcpy(vtmp->quat_x + i*stride_p, vectors_s->quat_x + i*metaheuristic.NEIIni, metaheuristic.NEIIni * sizeof(type_data)); 
		memcpy(vtmp->quat_y + i*stride_p, vectors_s->quat_y + i*metaheuristic.NEIIni, metaheuristic.NEIIni * sizeof(type_data)); 
		memcpy(vtmp->quat_z + i*stride_p, vectors_s->quat_z + i*metaheuristic.NEIIni, metaheuristic.NEIIni * sizeof(type_data)); 
		memcpy(vtmp->quat_w + i*stride_p, vectors_s->quat_w + i*metaheuristic.NEIIni, metaheuristic.NEIIni * sizeof(type_data)); 
		memcpy(vtmp->quat_x + i*stride_p + metaheuristic.NEIIni, vectors_e->quat_x + i*metaheuristic.NEIIni_selec, metaheuristic.NEIIni_selec * sizeof(type_data));  			
		memcpy(vtmp->quat_y + i*stride_p + metaheuristic.NEIIni, vectors_e->quat_y + i*metaheuristic.NEIIni_selec, metaheuristic.NEIIni_selec * sizeof(type_data));  			
		memcpy(vtmp->quat_z + i*stride_p + metaheuristic.NEIIni, vectors_e->quat_z + i*metaheuristic.NEIIni_selec, metaheuristic.NEIIni_selec * sizeof(type_data));  			
		memcpy(vtmp->quat_w + i*stride_p + metaheuristic.NEIIni, vectors_e->quat_w + i*metaheuristic.NEIIni_selec, metaheuristic.NEIIni_selec * sizeof(type_data));  			
		memcpy(vtmp->energy.energy + i*stride_p, vectors_s->energy.energy + i*metaheuristic.NEIIni, metaheuristic.NEIIni * sizeof(type_data)); 
		memcpy(vtmp->energy.energy + i*stride_p + metaheuristic.NEIIni, vectors_e->energy.energy + i*metaheuristic.NEIIni_selec, metaheuristic.NEIIni_selec * sizeof(type_data));  					
	}
	//printf("files %d %d %d\n",vtmp->files,vectors_e->nconformations,vectors_s->nconformations);	
	//exit(0);
	#pragma omp parallel for
	for (unsigned int i = 0; i < vtmp->nconformations; i += stride_p)			
		thrust::stable_sort_by_key((vtmp->energy.energy + i),(vtmp->energy.energy + i + stride_p), (vtmp->energy.n_conformation + i), thrust::less<type_data>());			
	colocar_vs(nlig,param,vtmp,metaheuristic);
	//exit(0);	
	#pragma omp parallel for
	for (unsigned int i = 0;i < vectors_s->files;i++)
	{
		memcpy(vectors_s->move_x + i*metaheuristic.NEIIni, vtmp->move_x + i*stride_p,metaheuristic.NEIIni * sizeof(type_data));
		memcpy(vectors_s->move_y + i*metaheuristic.NEIIni, vtmp->move_y + i*stride_p,metaheuristic.NEIIni * sizeof(type_data)); 
		memcpy(vectors_s->move_z + i*metaheuristic.NEIIni, vtmp->move_z + i*stride_p,metaheuristic.NEIIni * sizeof(type_data)); 		
		memcpy(vectors_s->quat_x + i*metaheuristic.NEIIni, vtmp->quat_x + i*stride_p,metaheuristic.NEIIni * sizeof(type_data)); 		
		memcpy(vectors_s->quat_y + i*metaheuristic.NEIIni, vtmp->quat_y + i*stride_p,metaheuristic.NEIIni * sizeof(type_data)); 		
		memcpy(vectors_s->quat_z + i*metaheuristic.NEIIni, vtmp->quat_z + i*stride_p,metaheuristic.NEIIni * sizeof(type_data)); 		
		memcpy(vectors_s->quat_w + i*metaheuristic.NEIIni, vtmp->quat_w + i*stride_p,metaheuristic.NEIIni * sizeof(type_data)); 		
		memcpy(vectors_s->energy.energy + i*metaheuristic.NEIIni, vtmp->energy.energy + i*stride_p,metaheuristic.NEIIni * sizeof(type_data)); 		
	}
	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
	//exit(0);
	free(vtmp->move_x);
	free(vtmp->move_y);
	free(vtmp->move_z);
	free(vtmp->quat_x);
	free(vtmp->quat_y);
	free(vtmp->quat_z);
	free(vtmp->quat_w);
	free(vtmp->energy.energy);
	free(vtmp->energy.n_conformation);
}*/

extern void seleccionar_mutar_vs (unsigned int nlig, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic)
{
        unsigned int v1_tmp,l,th1;
        type_data v2_tmp, v3_tmp;
        v1_tmp = vectors_e->nconformations / vectors_e->files;
        metaheuristic.PEMUCom_selec = (v1_tmp * metaheuristic.PEMUCom)/100;
        //v3_tmp = getPadding(metaheuristic.PEMSel_selec,16);
        //metaheuristic.PEMSel_selec += v3_tmp;
        //printf("NEISelec %d %d\n",metaheuristic.PEMSel_selec,vectors_e->nconformations);

        free(vectors_s->conformations_x);
        free(vectors_s->conformations_y);
        free(vectors_s->conformations_z);
        free(vectors_s->ql);
        free(vectors_s->ligtype);
        free(vectors_s->subtype);
        free(vectors_s->bonds);
        free(vectors_s->nbonds);

        free(vectors_s->move_x);
        free(vectors_s->move_y);
        free(vectors_s->move_z);
        free(vectors_s->quat_x);
        free(vectors_s->quat_y);
        free(vectors_s->quat_z);
        free(vectors_s->quat_w);
        free(vectors_s->energy.energy);
        free(vectors_s->energy.n_conformation);
	free(vectors_s->weights);

        vectors_s->files = vectors_e->files;
        vectors_s->nlig = vectors_e->nlig;
        vectors_s->nconformations = vectors_e->files * metaheuristic.PEMUCom_selec;
        vectors_s->conformations_x = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);
        vectors_s->conformations_y = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);
        vectors_s->conformations_z = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);

 	vectors_s->ql = (type_data *)malloc(sizeof(type_data)*vectors_s->nlig*vectors_s->nconformations);
        vectors_s->ligtype = (char *)malloc(sizeof(char)*vectors_s->nlig*vectors_s->nconformations);
        vectors_s->subtype = (char *)malloc(sizeof(char)*vectors_s->nlig*SUBTYPEMAXLEN*vectors_s->nconformations);
        vectors_s->bonds = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nlig*MAXBOND*vectors_s->nconformations);
        vectors_s->nbonds = (char *)malloc(sizeof(char)*vectors_s->nlig*vectors_s->nconformations);

        vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
        vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
        vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
        vectors_s->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
        vectors_s->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
        vectors_s->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
        vectors_s->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
        vectors_s->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nconformations);
        vectors_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->weights = (type_data *)malloc(SCORING_TERMS * sizeof(type_data));
	memcpy(vectors_s->f_params, vectors_e->f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_s->weights, vectors_e->weights,SCORING_TERMS * sizeof(type_data));

        thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
        //exit(0);
        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 0:
                case 2:
                case 3:
                        th1 = metaheuristic.Threads1Sel;
                        break;
        }
        omp_set_num_threads (th1);
		#pragma omp parallel for
        for (unsigned int i = 0; i < vectors_e->files; i++)
        {
                memcpy(vectors_s->conformations_x + i*nlig*metaheuristic.PEMUCom_selec, vectors_e->conformations_x + i*nlig*v1_tmp, metaheuristic.PEMUCom_selec * nlig * sizeof(type_data));
                memcpy(vectors_s->conformations_y + i*nlig*metaheuristic.PEMUCom_selec, vectors_e->conformations_y + i*nlig*v1_tmp, metaheuristic.PEMUCom_selec * nlig * sizeof(type_data));
                memcpy(vectors_s->conformations_z + i*nlig*metaheuristic.PEMUCom_selec, vectors_e->conformations_z + i*nlig*v1_tmp, metaheuristic.PEMUCom_selec * nlig * sizeof(type_data));

                memcpy(vectors_s->ql + i*nlig*metaheuristic.PEMUCom_selec, vectors_e->ql + i*nlig*v1_tmp, metaheuristic.PEMUCom_selec*nlig*sizeof(type_data));
                memcpy(vectors_s->ligtype + i*nlig*metaheuristic.PEMUCom_selec, vectors_e->ligtype + i*nlig*v1_tmp, metaheuristic.PEMUCom_selec*nlig*sizeof(char));
                memcpy(vectors_s->subtype + i*nlig*SUBTYPEMAXLEN*metaheuristic.PEMUCom_selec, vectors_e->subtype + i*nlig*SUBTYPEMAXLEN*v1_tmp, metaheuristic.PEMUCom_selec * nlig * SUBTYPEMAXLEN * sizeof(char));
                memcpy(vectors_s->bonds + i*nlig*MAXBOND*metaheuristic.PEMUCom_selec, vectors_e->bonds + i*nlig*MAXBOND*v1_tmp, metaheuristic.PEMUCom_selec * nlig * MAXBOND * sizeof(unsigned int));
                memcpy(vectors_s->nbonds + i*nlig*metaheuristic.PEMUCom_selec, vectors_e->nbonds + i*nlig*v1_tmp, metaheuristic.PEMUCom_selec * nlig * sizeof(char));

                memcpy(vectors_s->move_x + i*metaheuristic.PEMUCom_selec , vectors_e->move_x + i*v1_tmp, metaheuristic.PEMUCom_selec * sizeof(type_data));
                memcpy(vectors_s->move_y + i*metaheuristic.PEMUCom_selec, vectors_e->move_y + i*v1_tmp, metaheuristic.PEMUCom_selec * sizeof(type_data));
                memcpy(vectors_s->move_z + i*metaheuristic.PEMUCom_selec, vectors_e->move_z + i*v1_tmp, metaheuristic.PEMUCom_selec * sizeof(type_data));
                memcpy(vectors_s->quat_x + i*metaheuristic.PEMUCom_selec, vectors_e->quat_x + i*v1_tmp, metaheuristic.PEMUCom_selec * sizeof(type_data));
                memcpy(vectors_s->quat_y + i*metaheuristic.PEMUCom_selec, vectors_e->quat_y + i*v1_tmp, metaheuristic.PEMUCom_selec * sizeof(type_data));
                memcpy(vectors_s->quat_z + i*metaheuristic.PEMUCom_selec, vectors_e->quat_z + i*v1_tmp, metaheuristic.PEMUCom_selec * sizeof(type_data));
                memcpy(vectors_s->quat_w + i*metaheuristic.PEMUCom_selec, vectors_e->quat_w + i*v1_tmp, metaheuristic.PEMUCom_selec * sizeof(type_data));
                memcpy((vectors_s->energy.energy + i*metaheuristic.PEMUCom_selec), (vectors_e->energy.energy + i*v1_tmp), metaheuristic.PEMUCom_selec * sizeof(type_data));
        }
}

extern void seleccionar_mejorar_vs (unsigned int nlig, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic)
{
	unsigned int v1_tmp,l,th1;
	type_data v2_tmp, v3_tmp;
	v1_tmp = vectors_e->nconformations / vectors_e->files;
	metaheuristic.PEMSel_selec = (v1_tmp * metaheuristic.PEMSel)/100;
	//v3_tmp = getPadding(metaheuristic.PEMSel_selec,16);
	//metaheuristic.PEMSel_selec += v3_tmp; 
	//printf("NEISelec %d %d\n",metaheuristic.PEMSel_selec,vectors_e->nconformations);	
	
	free(vectors_s->conformations_x);
	free(vectors_s->conformations_y);
	free(vectors_s->conformations_z);
	free(vectors_s->ql);
	free(vectors_s->ligtype);
	free(vectors_s->subtype);
	free(vectors_s->bonds);
	free(vectors_s->nbonds);
	
	free(vectors_s->move_x);
	free(vectors_s->move_y);
	free(vectors_s->move_z);
	free(vectors_s->quat_x);
	free(vectors_s->quat_y);
	free(vectors_s->quat_z);
	free(vectors_s->quat_w);
	free(vectors_s->energy.energy);
	free(vectors_s->energy.n_conformation);
	free(vectors_s->weights);
		
	vectors_s->files = vectors_e->files;
	vectors_s->nlig = vectors_e->nlig;
	vectors_s->nconformations = vectors_e->files * metaheuristic.PEMSel_selec;
	vectors_s->conformations_x = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);
	vectors_s->conformations_y = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);
	vectors_s->conformations_z = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);
	
	vectors_s->ql = (type_data *)malloc(sizeof(type_data)*vectors_s->nlig*vectors_s->nconformations);
	vectors_s->ligtype = (char *)malloc(sizeof(char)*vectors_s->nlig*vectors_s->nconformations);
	vectors_s->subtype = (char *)malloc(sizeof(char)*vectors_s->nlig*SUBTYPEMAXLEN*vectors_s->nconformations);
	vectors_s->bonds = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nlig*MAXBOND*vectors_s->nconformations);
	vectors_s->nbonds = (char *)malloc(sizeof(char)*vectors_s->nlig*vectors_s->nconformations);

	vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nconformations);
	vectors_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);	
	vectors_s->weights = (type_data *)malloc(SCORING_TERMS * sizeof(type_data));
        memcpy(vectors_s->f_params, vectors_e->f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_s->weights, vectors_e->weights,SCORING_TERMS * sizeof(type_data));

	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
	//exit(0);
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;			
		case 0:
		case 2:
		case 3:
			th1 = metaheuristic.Threads1Sel;
			break;
	}
	omp_set_num_threads (th1);
	#pragma omp parallel for 	
	for (unsigned int i = 0; i < vectors_e->files; i++)
	{
		memcpy(vectors_s->conformations_x + i*nlig*metaheuristic.PEMSel_selec, vectors_e->conformations_x + i*nlig*v1_tmp, metaheuristic.PEMSel_selec * nlig * sizeof(type_data));  	
		memcpy(vectors_s->conformations_y + i*nlig*metaheuristic.PEMSel_selec, vectors_e->conformations_y + i*nlig*v1_tmp, metaheuristic.PEMSel_selec * nlig * sizeof(type_data)); 
		memcpy(vectors_s->conformations_z + i*nlig*metaheuristic.PEMSel_selec, vectors_e->conformations_z + i*nlig*v1_tmp, metaheuristic.PEMSel_selec * nlig * sizeof(type_data));  
		
		memcpy(vectors_s->ql + i*nlig*metaheuristic.PEMSel_selec, vectors_e->ql + i*nlig*v1_tmp, metaheuristic.PEMSel_selec*nlig*sizeof(type_data));
		memcpy(vectors_s->ligtype + i*nlig*metaheuristic.PEMSel_selec, vectors_e->ligtype + i*nlig*v1_tmp, metaheuristic.PEMSel_selec*nlig*sizeof(char));
		memcpy(vectors_s->subtype + i*nlig*SUBTYPEMAXLEN*metaheuristic.PEMSel_selec, vectors_e->subtype + i*nlig*SUBTYPEMAXLEN*v1_tmp, metaheuristic.PEMSel_selec * nlig * SUBTYPEMAXLEN * sizeof(char));
		memcpy(vectors_s->bonds + i*nlig*MAXBOND*metaheuristic.PEMSel_selec, vectors_e->bonds + i*nlig*MAXBOND*v1_tmp, metaheuristic.PEMSel_selec * nlig * MAXBOND * sizeof(unsigned int));
		memcpy(vectors_s->nbonds + i*nlig*metaheuristic.PEMSel_selec, vectors_e->nbonds + i*nlig*v1_tmp, metaheuristic.PEMSel_selec * nlig * sizeof(char));
  
		memcpy(vectors_s->move_x + i*metaheuristic.PEMSel_selec , vectors_e->move_x + i*v1_tmp, metaheuristic.PEMSel_selec * sizeof(type_data));  	
		memcpy(vectors_s->move_y + i*metaheuristic.PEMSel_selec, vectors_e->move_y + i*v1_tmp, metaheuristic.PEMSel_selec * sizeof(type_data)); 
		memcpy(vectors_s->move_z + i*metaheuristic.PEMSel_selec, vectors_e->move_z + i*v1_tmp, metaheuristic.PEMSel_selec * sizeof(type_data));  
		memcpy(vectors_s->quat_x + i*metaheuristic.PEMSel_selec, vectors_e->quat_x + i*v1_tmp, metaheuristic.PEMSel_selec * sizeof(type_data));
		memcpy(vectors_s->quat_y + i*metaheuristic.PEMSel_selec, vectors_e->quat_y + i*v1_tmp, metaheuristic.PEMSel_selec * sizeof(type_data));
		memcpy(vectors_s->quat_z + i*metaheuristic.PEMSel_selec, vectors_e->quat_z + i*v1_tmp, metaheuristic.PEMSel_selec * sizeof(type_data));
		memcpy(vectors_s->quat_w + i*metaheuristic.PEMSel_selec, vectors_e->quat_w + i*v1_tmp, metaheuristic.PEMSel_selec * sizeof(type_data));
		memcpy((vectors_s->energy.energy + i*metaheuristic.PEMSel_selec), (vectors_e->energy.energy + i*v1_tmp), metaheuristic.PEMSel_selec * sizeof(type_data));		
	} 	
}


/*extern void incluir_gpu_mejorar_vs (unsigned int nlig, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic)
{
	struct vectors_t *vtmp = (struct vectors_t *)malloc(sizeof(struct vectors_t));
	unsigned int th1;	
	unsigned int stride_p_e = vectors_e->nconformations / vectors_e->files; 	
	unsigned int stride_p_s = vectors_s->nconformations / vectors_s->files;
	vtmp->nlig = vectors_e->nlig; 	
	vtmp->files = vectors_e->files;	
	vtmp->nconformations = vectors_e->nconformations + vectors_s->nconformations;
	vtmp->conformations_x = (type_data *)malloc(sizeof(type_data)*vtmp->nlig*vtmp->nconformations);
	vtmp->conformations_y = (type_data *)malloc(sizeof(type_data)*vtmp->nlig*vtmp->nconformations);
	vtmp->conformations_z = (type_data *)malloc(sizeof(type_data)*vtmp->nlig*vtmp->nconformations);	
	vtmp->move_x = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
	vtmp->move_y = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
	vtmp->move_z = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
	vtmp->quat_x = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);	
	vtmp->quat_y = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);	
	vtmp->quat_z = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);	
	vtmp->quat_w = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);	
	vtmp->energy.energy = (type_data *)malloc(sizeof(type_data)*vtmp->nconformations);
	vtmp->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vtmp->nconformations);
	thrust::sequence(vtmp->energy.n_conformation,vtmp->energy.n_conformation + vtmp->nconformations);
	//printf("conformationes totales %d, stride_p %d\n",vtmp->nconformations,stride_p);
	//exit(0);
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 0:
		case 2:
		case 3:
			th1 = metaheuristic.Threads1Inc;
			break;
	}
	omp_set_num_threads (th1);
	#pragma omp parallel for	
	for (unsigned int i = 0; i < vectors_e->nconformations; i += stride_p_e)			
		thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + stride_p_e), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());			
	colocar_vs(nlig,param,vectors_e,metaheuristic);
	
	#pragma omp parallel for	
	for (unsigned int i = 0;i < vectors_s->files;i++)
	{	
		memcpy(vtmp->conformations_x + i*nlig*(stride_p_e + stride_p_s), vectors_s->conformations_x + i*nlig*stride_p_s, stride_p_s * nlig * sizeof(type_data)); 
		memcpy(vtmp->conformations_y + i*nlig*(stride_p_e + stride_p_s), vectors_s->conformations_y + i*nlig*stride_p_s, stride_p_s * nlig * sizeof(type_data)); 
		memcpy(vtmp->conformations_z + i*nlig*(stride_p_e + stride_p_s), vectors_s->conformations_z + i*nlig*stride_p_s, stride_p_s * nlig * sizeof(type_data)); 
		memcpy(vtmp->conformations_x + i*nlig*(stride_p_e + stride_p_s) + nlig*stride_p_s, vectors_e->conformations_x + i*nlig*stride_p_e, stride_p_e * nlig * sizeof(type_data));  			
		memcpy(vtmp->conformations_y + i*nlig*(stride_p_e + stride_p_s) + nlig*stride_p_s, vectors_e->conformations_y + i*nlig*stride_p_e, stride_p_e * nlig * sizeof(type_data));  			
		memcpy(vtmp->conformations_z + i*nlig*(stride_p_e + stride_p_s) + nlig*stride_p_s, vectors_e->conformations_z + i*nlig*stride_p_e, stride_p_e * nlig * sizeof(type_data));  			
		
		memcpy(vtmp->move_x + i*(stride_p_e + stride_p_s), vectors_s->move_x + i*stride_p_s, stride_p_s * sizeof(type_data)); 
		memcpy(vtmp->move_x + i*(stride_p_e + stride_p_s) + stride_p_s, vectors_e->move_x + i*stride_p_e, stride_p_e * sizeof(type_data));  			
		memcpy(vtmp->move_y + i*(stride_p_e + stride_p_s), vectors_s->move_y + i*stride_p_s, stride_p_s * sizeof(type_data)); 
		memcpy(vtmp->move_y + i*(stride_p_e + stride_p_s) + stride_p_s, vectors_e->move_y + i*stride_p_e, stride_p_e * sizeof(type_data));  			
		memcpy(vtmp->move_z + i*(stride_p_e + stride_p_s), vectors_s->move_z + i*stride_p_s, stride_p_s * sizeof(type_data)); 
		memcpy(vtmp->move_z + i*(stride_p_e + stride_p_s) + stride_p_s, vectors_e->move_z + i*stride_p_e, stride_p_e * sizeof(type_data));  			
		memcpy(vtmp->quat_x + i*(stride_p_e + stride_p_s), vectors_s->quat_x + i*stride_p_s, stride_p_s * sizeof(type_data)); 
		memcpy(vtmp->quat_y + i*(stride_p_e + stride_p_s), vectors_s->quat_y + i*stride_p_s, stride_p_s * sizeof(type_data)); 
		memcpy(vtmp->quat_z + i*(stride_p_e + stride_p_s), vectors_s->quat_z + i*stride_p_s, stride_p_s * sizeof(type_data)); 
		memcpy(vtmp->quat_w + i*(stride_p_e + stride_p_s), vectors_s->quat_w + i*stride_p_s, stride_p_s * sizeof(type_data)); 
		memcpy(vtmp->quat_x + i*(stride_p_e + stride_p_s) + stride_p_s, vectors_e->quat_x + i*stride_p_e, stride_p_e * sizeof(type_data));  			
		memcpy(vtmp->quat_y + i*(stride_p_e + stride_p_s) + stride_p_s, vectors_e->quat_y + i*stride_p_e, stride_p_e * sizeof(type_data));  			
		memcpy(vtmp->quat_z + i*(stride_p_e + stride_p_s) + stride_p_s, vectors_e->quat_z + i*stride_p_e, stride_p_e * sizeof(type_data));  			
		memcpy(vtmp->quat_w + i*(stride_p_e + stride_p_s) + stride_p_s, vectors_e->quat_w + i*stride_p_e, stride_p_e * sizeof(type_data));  			
		memcpy(vtmp->energy.energy + i*(stride_p_e + stride_p_s), vectors_s->energy.energy + i*stride_p_s, stride_p_s * sizeof(type_data)); 
		memcpy(vtmp->energy.energy + i*(stride_p_e + stride_p_s) + stride_p_s, vectors_e->energy.energy + i*stride_p_e, stride_p_e * sizeof(type_data));  					
	}
	//exit(0);
	#pragma omp parallel for
	for (unsigned int i = 0; i < vtmp->nconformations; i += (stride_p_e + stride_p_s))			
		thrust::stable_sort_by_key((vtmp->energy.energy + i),(vtmp->energy.energy + i + (stride_p_e + stride_p_s)), (vtmp->energy.n_conformation + i), thrust::less<type_data>());			
	colocar_vs(nlig,param,vtmp,metaheuristic);			
	
	#pragma omp parallel for
	for (unsigned int i = 0;i < vectors_s->files;i++)
	{
		memcpy(vectors_s->conformations_x + i*nlig*stride_p_s, vtmp->conformations_x + i*nlig*(stride_p_e + stride_p_s),stride_p_s * nlig * sizeof(type_data));
		memcpy(vectors_s->conformations_y + i*nlig*stride_p_s, vtmp->conformations_y + i*nlig*(stride_p_e + stride_p_s),stride_p_s * nlig * sizeof(type_data));
		memcpy(vectors_s->conformations_z + i*nlig*stride_p_s, vtmp->conformations_z + i*nlig*(stride_p_e + stride_p_s),stride_p_s * nlig * sizeof(type_data));
		
		memcpy(vectors_s->move_x + i*stride_p_s, vtmp->move_x + i*(stride_p_e + stride_p_s),stride_p_s * sizeof(type_data));
		memcpy(vectors_s->move_y + i*stride_p_s, vtmp->move_y + i*(stride_p_e + stride_p_s),stride_p_s * sizeof(type_data)); 
		memcpy(vectors_s->move_z + i*stride_p_s, vtmp->move_z + i*(stride_p_e + stride_p_s),stride_p_s * sizeof(type_data)); 		
		memcpy(vectors_s->quat_x + i*stride_p_s, vtmp->quat_x + i*(stride_p_e + stride_p_s),stride_p_s * sizeof(type_data)); 		
		memcpy(vectors_s->quat_y + i*stride_p_s, vtmp->quat_y + i*(stride_p_e + stride_p_s),stride_p_s * sizeof(type_data)); 		
		memcpy(vectors_s->quat_z + i*stride_p_s, vtmp->quat_z + i*(stride_p_e + stride_p_s),stride_p_s * sizeof(type_data)); 		
		memcpy(vectors_s->quat_w + i*stride_p_s, vtmp->quat_w + i*(stride_p_e + stride_p_s),stride_p_s * sizeof(type_data)); 		
		memcpy(vectors_s->energy.energy + i*stride_p_s, vtmp->energy.energy + i*(stride_p_e + stride_p_s),stride_p_s * sizeof(type_data)); 		
	}
	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
	free(vtmp->conformations_x);
	free(vtmp->conformations_y);
	free(vtmp->conformations_z);
	free(vtmp->move_x);
	free(vtmp->move_y);
	free(vtmp->move_z);
	free(vtmp->quat_x);
	free(vtmp->quat_y);
	free(vtmp->quat_z);
	free(vtmp->quat_w);
	free(vtmp->energy.energy);
	free(vtmp->energy.n_conformation);
}*/


extern void incluir_mejorar_inicializar_vs_environment (unsigned int nlig, struct vectors_t vectors_e, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int nconformations, struct metaheuristic_t metaheuristic, struct param_t param)
{
	unsigned int id_conformation,th1;
        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 0:
                case 3:
                case 2:
                        th1 = metaheuristic.Threads1Inc;
                        break;
        }
        omp_set_num_threads (th1);
        #pragma omp parallel for //private (id_conformation)
        for (unsigned int i = 0;i < nconformations;i++)
	{
        	//id_conformation = vectors_e.energy.n_conformation[i];
                for (unsigned int j = 0; j < metaheuristic.NEEImp; j++)
		{
                	if (vectors_e.energy.energy[i*metaheuristic.NEEImp + j] < energy[i])
                        {
                               	move_x[i] = vectors_e.move_x[i*metaheuristic.NEEImp + j];
                               	move_y[i] = vectors_e.move_y[i*metaheuristic.NEEImp + j];
                               	move_z[i] = vectors_e.move_z[i*metaheuristic.NEEImp + j];
                               	quat_x[i] = vectors_e.quat_x[i*metaheuristic.NEEImp + j];
                               	quat_y[i] = vectors_e.quat_y[i*metaheuristic.NEEImp + j];
                               	quat_z[i] = vectors_e.quat_z[i*metaheuristic.NEEImp + j];
                               	quat_w[i] = vectors_e.quat_w[i*metaheuristic.NEEImp + j];
                               	energy[i] = vectors_e.energy.energy[i*metaheuristic.NEEImp + j];
                        }
                	else
                        {
                                vectors_e.move_x[i*metaheuristic.NEEImp + j] = move_x[i];
                                vectors_e.move_y[i*metaheuristic.NEEImp + j] = move_y[i];
                                vectors_e.move_z[i*metaheuristic.NEEImp + j] = move_z[i];
                                vectors_e.quat_x[i*metaheuristic.NEEImp + j] = quat_x[i];
                                vectors_e.quat_y[i*metaheuristic.NEEImp + j] = quat_y[i];
                                vectors_e.quat_z[i*metaheuristic.NEEImp + j] = quat_z[i];
                                vectors_e.quat_w[i*metaheuristic.NEEImp + j] = quat_w[i];
                                vectors_e.energy.energy[i*metaheuristic.NEEImp + j] = energy[i];
                        }
		}
        }
}

extern void incluir_mejorar_inicializar_vs (unsigned int nlig, struct vectors_t vectors_e, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, struct metaheuristic_t metaheuristic, struct param_t param)
{	
	unsigned int id_conformation;
	unsigned int th1;
	
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 2:
			th1 = metaheuristic.Threads1Inc;
			break;
	}
	omp_set_num_threads (th1);
	#pragma omp parallel for private (id_conformation)
	for (unsigned int i = 0;i < (vectors_e.nconformations);i++)
	{
		id_conformation = vectors_e.energy.n_conformation[i];				
		if (vectors_e.energy.energy[i] < energy[id_conformation])
			{
				move_x[id_conformation] = vectors_e.move_x[id_conformation];
				move_y[id_conformation] = vectors_e.move_y[id_conformation];
				move_z[id_conformation] = vectors_e.move_z[id_conformation];
				quat_x[id_conformation] = vectors_e.quat_x[id_conformation];
				quat_y[id_conformation] = vectors_e.quat_y[id_conformation];
				quat_z[id_conformation] = vectors_e.quat_z[id_conformation];
				quat_w[id_conformation] = vectors_e.quat_w[id_conformation];
				energy[i] = vectors_e.energy.energy[i];
			}
		else
			{
				vectors_e.move_x[id_conformation] = move_x[id_conformation];
				vectors_e.move_y[id_conformation] = move_y[id_conformation];
				vectors_e.move_z[id_conformation] = move_z[id_conformation];
				vectors_e.quat_x[id_conformation] = quat_x[id_conformation];
				vectors_e.quat_y[id_conformation] = quat_y[id_conformation];
				vectors_e.quat_z[id_conformation] = quat_z[id_conformation];
				vectors_e.quat_w[id_conformation] = quat_w[id_conformation];
				vectors_e.energy.energy[id_conformation] = energy[id_conformation];	
			}
	}			
		
}

extern void mejorar_vs_environment (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct param_t param, type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *ql, char *ligtype, unsigned int *bonds, unsigned int nlig, unsigned int files, unsigned int nconformations, struct metaheuristic_t metaheuristic, unsigned int opt)
{
        struct vectors_t vectors_mejora;
        unsigned int steps,moveType=MOVE,bucle,fase;
        double tenergymejora;
        double tenergymejora_t;
        unsigned int tam = nconformations / files;
	unsigned int *nlinks = (unsigned int *)calloc(files,sizeof(unsigned int));

        vectors_mejora.files = files;
        vectors_mejora.nlig = nlig;
        vectors_mejora.nconformations = nconformations * metaheuristic.NEEImp;

        vectors_mejora.conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations*nlig);
	vectors_mejora.conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations*nlig);
	vectors_mejora.conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations*nlig);

	vectors_mejora.ql = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations*nlig);
	vectors_mejora.ligtype = (char *)malloc(sizeof(char)*vectors_mejora.nconformations*nlig);
	vectors_mejora.bonds = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations*nlig*MAXBOND);

	vectors_mejora.move_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.move_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.move_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        vectors_mejora.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations);
        vectors_mejora.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
        //printf("Memoria para mejora creada....\n");
        vectors_mejora.weights = (type_data *)malloc(SCORING_TERMS * sizeof(type_data));
        memcpy(vectors_mejora.f_params, f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_mejora.weights, weights,SCORING_TERMS * sizeof(type_data));

        fill_conformations_environment_vs (vectors_mejora.nlig,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,ligtype,ql,bonds,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,vectors_mejora.ligtype,vectors_mejora.ql,vectors_mejora.bonds,nconformations,param,metaheuristic);
        //memset(vectors_mejora.energy.energy,0,vectors_s->nconformations * sizeof(type_data));

        //for(int k=0;k<160;k++)
        //              printf("STP %d Mejorar VS-number %d, m_x %f, m_y %f, m_z %f, energy %f\n",steps,k,vectors_s->move_x[k],vectors_s->move_y[k],vectors_s->move_z[k],vectors_s->energy.energy[k]);
        //printf("*******\n");
        //exit(0);
        steps = 0;
	
        if (opt == 0)
        {
                bucle = metaheuristic.IMEIni;
        }
        if (opt == 1)
        {
                bucle = metaheuristic.IMEImp;
        }
        if (opt == 2)
        {
                bucle = metaheuristic.IMEMUCom;
        }

        moveType = MOVE;
        //printf("Voy a mejorar....\n");
        while (steps < bucle){
                if (moveType == MOVE){
                        move_mejora_vs(vectors_mejora.files,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,param,tam,metaheuristic);
                        moveType = ROTATE;
                        //printf("He movido\n");
                }
                if (moveType == ROTATE) {
                        rotate_vs(vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,param,vectors_mejora.nconformations,metaheuristic);
                        moveType = MOVE;
                }
                //printf("Voy a calcular la energia...\n");
                tenergymejora = wtime();
                ForcesCpuSolver_vs(nlinks,vectors_mejora.nlig,vectors_mejora.files,param,proteina,ligand_params,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,vectors_mejora.ligtype,vectors_mejora.ql,vectors_mejora.bonds,vectors_mejora.energy.energy,vectors_mejora.energy.n_conformation, vectors_mejora.weights,vectors_mejora.f_params,vectors_mejora.nconformations,metaheuristic);
                
		incluir_mejorar_inicializar_vs_environment (vectors_mejora.nlig,vectors_mejora,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconformations,metaheuristic, param);
		fill_conformations_environment_vs (vectors_mejora.nlig,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,ligtype,ql,bonds,vectors_mejora.conformations_x,vectors_mejora.conformations_y,vectors_mejora.conformations_z,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,vectors_mejora.ligtype,vectors_mejora.ql,vectors_mejora.bonds,nconformations,param,metaheuristic);

                steps++;
	}
	
	free(vectors_mejora.move_x);
        free(vectors_mejora.move_y);
        free(vectors_mejora.move_z);
        free(vectors_mejora.quat_x);
        free(vectors_mejora.quat_y);
        free(vectors_mejora.quat_z);
        free(vectors_mejora.quat_w);
        free(vectors_mejora.energy.energy);
        free(vectors_mejora.energy.n_conformation);
        free(vectors_mejora.weights);
        free(nlinks);
}
extern void mejorar_vs (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct param_t param, type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *ql, char *ligtype, unsigned int *bonds, unsigned int nlig, unsigned int files, unsigned int nconformations, struct metaheuristic_t metaheuristic, unsigned int opt)
{
	struct vectors_t vectors_mejora;
	unsigned int steps,moveType=MOVE,bucle,fase;	
	double tenergymejora;
	double tenergymejora_t;
	unsigned int tam = nconformations / files;
	unsigned int *nlinks = (unsigned int *)calloc(files,sizeof(unsigned int));
	
	vectors_mejora.files = files;
	vectors_mejora.nlig = nlig;
	vectors_mejora.nconformations = nconformations;

	vectors_mejora.move_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.move_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.move_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);
	vectors_mejora.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_mejora.nconformations);
	vectors_mejora.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_mejora.nconformations);	
 	//printf("Memoria para mejora creada....\n");
	vectors_mejora.weights = (type_data *)malloc(SCORING_TERMS * sizeof(type_data));
        memcpy(vectors_mejora.f_params, f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_mejora.weights, weights,SCORING_TERMS * sizeof(type_data));
	
	memcpy(vectors_mejora.move_x, move_x, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.move_y, move_y, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.move_z, move_z, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_x, quat_x, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_y, quat_y, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_z, quat_z, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.quat_w, quat_w, vectors_mejora.nconformations * sizeof(type_data));  
	
	//memcpy(vectors_mejora.energy.energy, vectors_s->energy.energy, vectors_mejora.nconformations * sizeof(type_data));  
	memcpy(vectors_mejora.energy.n_conformation, nconfs,vectors_mejora.nconformations * sizeof(unsigned int));

	thrust::sequence(vectors_mejora.energy.n_conformation,vectors_mejora.energy.n_conformation + vectors_mejora.nconformations);	
	//memset(vectors_mejora.energy.energy,0,vectors_s->nconformations * sizeof(type_data));
	
	//for(int k=0;k<160;k++)
	//		printf("STP %d Mejorar VS-number %d, m_x %f, m_y %f, m_z %f, energy %f\n",steps,k,vectors_s->move_x[k],vectors_s->move_y[k],vectors_s->move_z[k],vectors_s->energy.energy[k]);
	//printf("*******\n");
	//exit(0);
	steps = 0;
	if (opt == 0) 
	{
		bucle = metaheuristic.IMEIni;
	}
	if (opt == 1)
	{	
		bucle = metaheuristic.IMEImp;			
	}
	if (opt == 2)
	{
		bucle = metaheuristic.IMEMUCom;
	}
	
	moveType = MOVE;
	//printf("Voy a mejorar....\n");
	while (steps < bucle){		
		if (moveType == MOVE){
			move_mejora_vs(vectors_mejora.files,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,param,tam,metaheuristic);
			moveType = ROTATE;
			//printf("He movido\n");
		}
		if (moveType == ROTATE) {
			rotate_vs(vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,param,vectors_mejora.nconformations,metaheuristic);
			moveType = MOVE;
		}				
		//printf("Voy a calcular la energia...\n");
		tenergymejora = wtime();		
		ForcesCpuSolver_vs(nlinks,vectors_mejora.nlig,vectors_mejora.files,param,proteina,ligand_params,conformations_x,conformations_y,conformations_z,vectors_mejora.move_x,vectors_mejora.move_y,vectors_mejora.move_z,vectors_mejora.quat_x,vectors_mejora.quat_y,vectors_mejora.quat_z,vectors_mejora.quat_w,ligtype,ql,bonds,vectors_mejora.energy.energy,vectors_mejora.energy.n_conformation, vectors_mejora.weights,vectors_mejora.f_params,vectors_mejora.nconformations,metaheuristic);
		incluir_mejorar_inicializar_vs(vectors_mejora.nlig,vectors_mejora,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,metaheuristic, param);	
		steps++;
		//for(int k=0;k<64;k++)
		//	printf("STP %d Mejorar VS-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",steps,k,vectors_s->conformations_x[k],vectors_s->conformations_y[k],vectors_s->conformations_z[k],vectors_s->move_x[k/ligando.nlig],vectors_s->move_y[k/ligando.nlig],vectors_s->move_z[k/ligando.nlig],vectors_s->energy.energy[k/ligando.nlig]);
	
	}
	//if (opt==0)
	//	printf("\tCALCULO ENERGIA L-J EN FASE MEJORA INICIALIZAR: %lf seg\n",tenergymejora_t);
	//else
	//	printf("\tCALCULO ENERGIA L-J EN FASE MEJORAR: %lf seg\n",tenergymejora_t);
	//for(int k=0;k<160;k++)
		//	printf("STP %d Mejorar VS-number %d, m_x %f, m_y %f, m_z %f, energy %f\n",steps,k,vectors_s->move_x[k],vectors_s->move_y[k],vectors_s->move_z[k],vectors_s->energy.energy[k]);
		
	free(vectors_mejora.move_x);
	free(vectors_mejora.move_y);
	free(vectors_mejora.move_z);
	free(vectors_mejora.quat_x);
	free(vectors_mejora.quat_y);
	free(vectors_mejora.quat_z);
	free(vectors_mejora.quat_w);
	free(vectors_mejora.energy.energy);
	free(vectors_mejora.energy.n_conformation);
	free(vectors_mejora.weights);
	free(nlinks);
	//for(int k=0;k<64;k++)
	//	printf("STP %d Mejorar VS-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",steps,k,vectors_s->conformations_x[k],vectors_s->conformations_y[k],vectors_s->conformations_z[k],vectors_s->move_x[k/ligando.nlig],vectors_s->move_y[k/ligando.nlig],vectors_s->move_z[k/ligando.nlig],vectors_s->energy.energy[k/ligando.nlig]);
	//exit(0);		
	
}

extern void fill_conformations_vs (struct param_t param, struct metaheuristic_t metaheuristic, type_data *c_x, type_data *c_y, type_data *c_z, type_data *ql, char *ligtype, char *subtype, char *nbonds, unsigned int *bonds,  struct vectors_t *vectors_e) 
{
	unsigned int tam = vectors_e->nconformations / vectors_e->files;
	unsigned int th1;
	//printf("tam %d\n",tam);
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 0:
		case 2:
		case 3:
			th1 = metaheuristic.Threads1Ini;
			break;
	}
	//omp_set_num_threads (th1);
	//#pragma omp parallel for 	
	for (unsigned int i = 0;i < vectors_e->files;i++)				
		for (unsigned int j = 0;j < tam;j++)
		{
			memcpy(vectors_e->conformations_x + (i*tam*vectors_e->nlig) + (j*vectors_e->nlig),c_x + (i*vectors_e->nlig),vectors_e->nlig*sizeof(type_data));
			memcpy(vectors_e->conformations_y + (i*tam*vectors_e->nlig) + (j*vectors_e->nlig),c_y + (i*vectors_e->nlig),vectors_e->nlig*sizeof(type_data));
			memcpy(vectors_e->conformations_z + (i*tam*vectors_e->nlig) + (j*vectors_e->nlig),c_z + (i*vectors_e->nlig),vectors_e->nlig*sizeof(type_data));
			
			memcpy(vectors_e->ql + (i*tam*vectors_e->nlig) + (j*vectors_e->nlig),ql + (i*vectors_e->nlig),vectors_e->nlig*sizeof(type_data));
			memcpy(vectors_e->ligtype + (i*tam*vectors_e->nlig) + (j*vectors_e->nlig),ligtype + (i*vectors_e->nlig),vectors_e->nlig*sizeof(char));
			memcpy(vectors_e->subtype + (i*tam*SUBTYPEMAXLEN*vectors_e->nlig) + (j*SUBTYPEMAXLEN*vectors_e->nlig),subtype + (i*SUBTYPEMAXLEN*vectors_e->nlig),vectors_e->nlig*SUBTYPEMAXLEN*sizeof(char));
			memcpy(vectors_e->bonds + (i*tam*MAXBOND*vectors_e->nlig) + (j*MAXBOND*vectors_e->nlig),bonds + (i*MAXBOND*vectors_e->nlig),vectors_e->nlig*MAXBOND*sizeof(unsigned int));
			memcpy(vectors_e->nbonds + (i*tam*vectors_e->nlig) + (j*vectors_e->nlig),nbonds + (i*vectors_e->nlig),vectors_e->nlig*sizeof(char));					
		}
	//for (unsigned int k=(3482308*vectors_e->nlig*MAXBOND);k<(vectors_e->nlig*3482508*MAXBOND);k++) printf("conf %d, bonds %d\n",k/(vectors_e->nlig*MAXBOND),vectors_e->bonds[k]);
	//for (unsigned int k=0;k<vectors_e->files*vectors_e->nlig*MAXBOND;k++) printf("file %d, bonds %d\n",k/(vectors_e->nlig*MAXBOND),bonds[k]);
	//exit(0);
}

extern void seleccionar_inicializar_vs (unsigned int nlig, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic)
{
	unsigned int v1_tmp,l;
	type_data v2_tmp, v3_tmp;
	unsigned int th1;
	//exit(0);	
	v1_tmp = vectors_e->nconformations / vectors_e->files;
	metaheuristic.NEIIni_selec = (v1_tmp * metaheuristic.PEMIni)/100;
	//v3_tmp = getPadding(metaheuristic.NEIIni_selec,16);
	//metaheuristic.NEIIni_selec += v3_tmp; 
	//exit(0);
	vectors_s->files = vectors_e->files;
	vectors_s->nlig = vectors_e->nlig;
	
	vectors_s->nconformations = vectors_e->files * metaheuristic.NEIIni_selec;
	vectors_s->conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nlig*vectors_s->nconformations);
	vectors_s->conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nlig*vectors_s->nconformations);
	vectors_s->conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nlig*vectors_s->nconformations);	
	//exit(0);
	vectors_s->ql = (type_data *)malloc(sizeof(type_data)*vectors_s->nlig*vectors_s->nconformations);
	vectors_s->ligtype = (char *)malloc(sizeof(char)*vectors_s->nlig*vectors_s->nconformations);
	vectors_s->subtype = (char *)malloc(sizeof(char)*vectors_s->nlig*SUBTYPEMAXLEN*vectors_s->nconformations);
	vectors_s->bonds = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nlig*MAXBOND*vectors_s->nconformations);
	vectors_s->nbonds = (char *)malloc(sizeof(char)*vectors_s->nlig*vectors_s->nconformations);
		
	vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nconformations);
	vectors_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);	
	vectors_s->weights = (type_data *)malloc(SCORING_TERMS * sizeof(type_data));
        memcpy(vectors_s->f_params, vectors_e->f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_s->weights, vectors_e->weights,SCORING_TERMS * sizeof(type_data));
	
	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
	//exit(0);	
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 0:
		case 2:
		case 3:
			th1 = metaheuristic.Threads1Sel;
			break;
	}	
	omp_set_num_threads (th1);
	#pragma omp parallel for 	
	for (unsigned int i = 0; i < vectors_e->files; i++)
	{
		memcpy(vectors_s->conformations_x + i*vectors_s->nlig*metaheuristic.NEIIni_selec , vectors_e->conformations_x + i*vectors_s->nlig*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * vectors_s->nlig * sizeof(type_data));  	
		memcpy(vectors_s->conformations_y + i*vectors_s->nlig*metaheuristic.NEIIni_selec, vectors_e->conformations_y + i*vectors_s->nlig*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * vectors_s->nlig * sizeof(type_data)); 
		memcpy(vectors_s->conformations_z + i*vectors_s->nlig*metaheuristic.NEIIni_selec, vectors_e->conformations_z + i*vectors_s->nlig*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * vectors_s->nlig * sizeof(type_data));

		memcpy((vectors_s->ql + i*vectors_s->nlig*metaheuristic.NEIIni_selec), (vectors_e->ql + i*vectors_s->nlig*metaheuristic.NEIIni), metaheuristic.NEIIni_selec * vectors_s->nlig * sizeof(type_data));
		memcpy((vectors_s->ligtype + i*vectors_s->nlig*metaheuristic.NEIIni_selec), (vectors_e->ligtype + i*vectors_s->nlig*metaheuristic.NEIIni), metaheuristic.NEIIni_selec * vectors_s->nlig * sizeof(char));
		memcpy((vectors_s->subtype + i*vectors_s->nlig*SUBTYPEMAXLEN*metaheuristic.NEIIni_selec), (vectors_e->subtype + i*vectors_s->nlig*SUBTYPEMAXLEN*metaheuristic.NEIIni), metaheuristic.NEIIni_selec * vectors_s->nlig * SUBTYPEMAXLEN * sizeof(char));
		memcpy((vectors_s->bonds + i*vectors_s->nlig*MAXBOND*metaheuristic.NEIIni_selec), (vectors_e->bonds + i*vectors_s->nlig*MAXBOND*metaheuristic.NEIIni), metaheuristic.NEIIni_selec * vectors_s->nlig * MAXBOND * sizeof(unsigned int));
		memcpy((vectors_s->nbonds + i*vectors_s->nlig*metaheuristic.NEIIni_selec), (vectors_e->nbonds + i*vectors_s->nlig*metaheuristic.NEIIni), metaheuristic.NEIIni_selec * vectors_s->nlig * sizeof(char));  
			
		memcpy(vectors_s->move_x + i*metaheuristic.NEIIni_selec , vectors_e->move_x + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));  	
		memcpy(vectors_s->move_y + i*metaheuristic.NEIIni_selec, vectors_e->move_y + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data)); 
		memcpy(vectors_s->move_z + i*metaheuristic.NEIIni_selec, vectors_e->move_z + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));  
		memcpy(vectors_s->quat_x + i*metaheuristic.NEIIni_selec, vectors_e->quat_x + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));
		memcpy(vectors_s->quat_y + i*metaheuristic.NEIIni_selec, vectors_e->quat_y + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));
		memcpy(vectors_s->quat_z + i*metaheuristic.NEIIni_selec, vectors_e->quat_z + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));
		memcpy(vectors_s->quat_w + i*metaheuristic.NEIIni_selec, vectors_e->quat_w + i*metaheuristic.NEIIni, metaheuristic.NEIIni_selec * sizeof(type_data));
		memcpy((vectors_s->energy.energy + i*metaheuristic.NEIIni_selec), (vectors_e->energy.energy + i*metaheuristic.NEIIni), metaheuristic.NEIIni_selec * sizeof(type_data));		
	}
			
	//for(int k=0;k<64;k++)
	//	printf("SIni e-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",k,vectors_e->conformations_x[k],vectors_e->conformations_y[k],vectors_e->conformations_z[k],vectors_e->move_x[k/nlig],vectors_e->move_y[k/nlig],vectors_e->move_z[k/nlig],vectors_e->energy.energy[k/nlig]);
	//printf("***\n");
	//for(int k=0;k<64;k++)
	//	printf("SIni s-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",k,vectors_s->conformations_x[k],vectors_s->conformations_y[k],vectors_s->conformations_z[k],vectors_s->move_x[k/nlig],vectors_s->move_y[k/nlig],vectors_s->move_z[k/nlig],vectors_s->energy.energy[k/nlig]);
	
	//exit(0);	
}


extern void seleccionar_NEFIni_vs (unsigned int nlig, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param)
{	
	unsigned int stride_s, stride_e;
	unsigned int th1;
	stride_s = metaheuristic.NEFMIni_selec;
	stride_e = metaheuristic.NEIIni - metaheuristic.NEFPIni_selec;	
	
	vectors_s->files = vectors_e->files;
	vectors_s->nlig = vectors_e->nlig;
	vectors_s->nconformations = vectors_e->files * metaheuristic.NEFIni;
	//printf("conformaciones por file %d\n",metaheuristic.NEFIni);
	//printf("Número de conformaciones: %d\n",vectors_s->nconformations);
	vectors_s->conformations_x = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);
	vectors_s->conformations_y = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);
	vectors_s->conformations_z = (type_data *)malloc(sizeof(type_data)*nlig*vectors_s->nconformations);
	vectors_s->move_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->move_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);
	vectors_s->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);

	vectors_s->ql = (type_data *)malloc(sizeof(type_data)*vectors_s->nlig*vectors_s->nconformations);
	vectors_s->ligtype = (char *)malloc(sizeof(char)*vectors_s->nlig*vectors_s->nconformations);
	vectors_s->subtype = (char *)malloc(sizeof(char)*vectors_s->nlig*SUBTYPEMAXLEN*vectors_s->nconformations);
	vectors_s->bonds = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nlig*MAXBOND*vectors_s->nconformations);
	vectors_s->nbonds = (char *)malloc(sizeof(char)*vectors_s->nlig*vectors_s->nconformations);
			
	
	vectors_s->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s->nconformations);
	vectors_s->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s->nconformations);	
	vectors_s->ac_x = vectors_e->ac_x;
	vectors_s->ac_y = vectors_e->ac_y;
	vectors_s->ac_z = vectors_e->ac_z;

	vectors_s->weights = (type_data *)malloc(SCORING_TERMS * sizeof(type_data));
        memcpy(vectors_s->f_params, vectors_e->f_params,MAXTYPES * sizeof(struct force_field_param_t));
        memcpy(vectors_s->weights, vectors_e->weights,SCORING_TERMS * sizeof(type_data));

	
	thrust::sequence(vectors_s->energy.n_conformation,vectors_s->energy.n_conformation + vectors_s->nconformations);
	
	switch (param.mode) {
		case 1:
			th1 = 1;
			break;
		case 0:
		case 2:
		case 3:
			th1 = metaheuristic.Threads1Ini;
			break;
	}	
	omp_set_num_threads (metaheuristic.Threads1Ini);
	#pragma omp parallel for 	
	for (unsigned int i = 0;i < vectors_s->files;i++)
	{				
		memcpy((vectors_s->conformations_x + i*nlig*metaheuristic.NEFIni), (vectors_e->conformations_x + i*nlig*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * nlig * sizeof(type_data));  	
		memcpy((vectors_s->conformations_y + i*nlig*metaheuristic.NEFIni), (vectors_e->conformations_y + i*nlig*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * nlig * sizeof(type_data)); 
		memcpy((vectors_s->conformations_z + i*nlig*metaheuristic.NEFIni), (vectors_e->conformations_z + i*nlig*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * nlig * sizeof(type_data));
		memcpy((vectors_s->ql + i*nlig*metaheuristic.NEFIni), (vectors_e->ql + i*nlig*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * nlig * sizeof(type_data));
		memcpy((vectors_s->ligtype + i*nlig*metaheuristic.NEFIni), (vectors_e->ligtype + i*nlig*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * nlig * sizeof(char));
		memcpy((vectors_s->subtype + i*nlig*SUBTYPEMAXLEN*metaheuristic.NEFIni), (vectors_e->subtype + i*nlig*SUBTYPEMAXLEN*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * nlig * SUBTYPEMAXLEN * sizeof(char));
		memcpy((vectors_s->bonds + i*nlig*MAXBOND*metaheuristic.NEFIni), (vectors_e->bonds + i*nlig*MAXBOND*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * nlig * MAXBOND * sizeof(unsigned int));
		memcpy((vectors_s->nbonds + i*nlig*metaheuristic.NEFIni), (vectors_e->nbonds + i*nlig*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * nlig * sizeof(char));
	
		memcpy((vectors_s->move_x + i*metaheuristic.NEFIni), (vectors_e->move_x + i*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * sizeof(type_data));  	
		memcpy((vectors_s->move_y + i*metaheuristic.NEFIni), (vectors_e->move_y + i*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * sizeof(type_data)); 
		memcpy((vectors_s->move_z + i*metaheuristic.NEFIni), (vectors_e->move_z + i*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * sizeof(type_data));
		memcpy((vectors_s->quat_x + i*metaheuristic.NEFIni), (vectors_e->quat_x + i*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * sizeof(type_data));
		memcpy((vectors_s->quat_y + i*metaheuristic.NEFIni), (vectors_e->quat_y + i*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * sizeof(type_data));
		memcpy((vectors_s->quat_z + i*metaheuristic.NEFIni), (vectors_e->quat_z + i*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * sizeof(type_data));
		memcpy((vectors_s->quat_w + i*metaheuristic.NEFIni), (vectors_e->quat_w + i*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * sizeof(type_data));
		memcpy((vectors_s->energy.energy + i*metaheuristic.NEFIni), (vectors_e->energy.energy + i*metaheuristic.NEIIni), metaheuristic.NEFMIni_selec * sizeof(type_data));
		//memcpy((vectors_s->energy.n_conformation + i*metaheuristic.NEFIni), (vectors_e->energy.n_conformation + i*metaheuristic.NEIIni),metaheuristic.NEFMIni_selec * sizeof(int));				
				
		memcpy((vectors_s->conformations_x + i*nlig*metaheuristic.NEFIni + stride_s*nlig), (vectors_e->conformations_x + i*nlig*metaheuristic.NEIIni + stride_e*nlig), metaheuristic.NEFPIni_selec * nlig * sizeof(type_data));  	
		memcpy((vectors_s->conformations_y + i*nlig*metaheuristic.NEFIni + stride_s*nlig), (vectors_e->conformations_y + i*nlig*metaheuristic.NEIIni + stride_e*nlig), metaheuristic.NEFPIni_selec * nlig * sizeof(type_data)); 
		memcpy((vectors_s->conformations_z + i*nlig*metaheuristic.NEFIni + stride_s*nlig), (vectors_e->conformations_z + i*nlig*metaheuristic.NEIIni + stride_e*nlig), metaheuristic.NEFPIni_selec * nlig * sizeof(type_data));
				
		memcpy((vectors_s->ql + i*nlig*metaheuristic.NEFIni + stride_s*nlig), (vectors_e->ql + i*nlig*metaheuristic.NEIIni + stride_e*nlig), metaheuristic.NEFPIni_selec * nlig * sizeof(type_data));
		memcpy((vectors_s->ligtype + i*nlig*metaheuristic.NEFIni + stride_s*nlig), (vectors_e->ligtype + i*nlig*metaheuristic.NEIIni+ stride_e*nlig), metaheuristic.NEFPIni_selec * nlig * sizeof(char));
		memcpy((vectors_s->subtype + i*nlig*SUBTYPEMAXLEN*metaheuristic.NEFIni + stride_s*nlig*SUBTYPEMAXLEN), (vectors_e->subtype + i*nlig*SUBTYPEMAXLEN*metaheuristic.NEIIni+ stride_e * nlig * SUBTYPEMAXLEN), metaheuristic.NEFPIni_selec * nlig * SUBTYPEMAXLEN * sizeof(char));
		memcpy((vectors_s->bonds + i*nlig*MAXBOND*metaheuristic.NEFIni + stride_s*nlig*MAXBOND), (vectors_e->bonds + i*nlig*MAXBOND*metaheuristic.NEIIni + stride_e * nlig * MAXBOND), metaheuristic.NEFPIni_selec * nlig * MAXBOND * sizeof(unsigned int));
		memcpy((vectors_s->nbonds + i*nlig*metaheuristic.NEFIni + stride_s*nlig), (vectors_e->nbonds + i*nlig*metaheuristic.NEIIni + stride_e * nlig), metaheuristic.NEFPIni_selec * nlig * sizeof(char));
	
		memcpy((vectors_s->move_x + i*metaheuristic.NEFIni + stride_s), (vectors_e->move_x + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(type_data));  	
		memcpy((vectors_s->move_y + i*metaheuristic.NEFIni + stride_s), (vectors_e->move_y + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(type_data)); 
		memcpy((vectors_s->move_z + i*metaheuristic.NEFIni + stride_s), (vectors_e->move_z + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(type_data));						
		memcpy((vectors_s->quat_x + i*metaheuristic.NEFIni + stride_s), (vectors_e->quat_x + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(type_data));
		memcpy((vectors_s->quat_y + i*metaheuristic.NEFIni + stride_s), (vectors_e->quat_y + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(type_data));
		memcpy((vectors_s->quat_z + i*metaheuristic.NEFIni + stride_s), (vectors_e->quat_z + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(type_data));
		memcpy((vectors_s->quat_w + i*metaheuristic.NEFIni + stride_s), (vectors_e->quat_w + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(type_data));
		memcpy((vectors_s->energy.energy + i*metaheuristic.NEFIni + stride_s), (vectors_e->energy.energy + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(type_data));
		//memcpy((vectors_s->energy.n_conformation + i*metaheuristic.NEFIni + stride_s), (vectors_e->energy.n_conformation + i*metaheuristic.NEIIni + stride_e), metaheuristic.NEFPIni_selec * sizeof(int));									
	}
	//for(int k=0;k<64;k++)
	//	printf("SelEF e-number %d, x %f, y %f, z %f m_x %f, m_y %f, m_z %f, energy %f\n",k,vectors_e->conformations_x[k],vectors_e->conformations_y[k],vectors_e->conformations_z[k],vectors_e->move_x[k/nlig],vectors_e->move_y[k/nlig],vectors_e->move_z[k/nlig],vectors_e->energy.energy[k/nlig]);
	//printf("***\n");
	//for (int l=0;l<1;l++)
	//{
	//for(int k=0;k<64;k++)
	//	printf("file %d SelEF s-number %d, m_x %f, m_y %f, m_z %f, energy %f\n",l,k,vectors_s->move_x[l*metaheuristic.NEFIni + k],vectors_s->move_y[l*metaheuristic.NEFIni + k],vectors_s->move_z[l*metaheuristic.NEFIni + k],vectors_s->energy.energy[l*metaheuristic.NEFIni + k]);
	//}
	//exit(0);
}

extern void inicializar_vs (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, type_data *c_x, type_data *c_y, type_data *c_z, type_data *ql, char *ligtype, char *subtype, char *nbonds, unsigned int *bonds)
{
	
	//CONFIGURING MEMORY
	unsigned int NEItemp, NEI_selec_temp;
	unsigned int th1;
	type_data *internal_energy;
	unsigned int max_conformaciones, stride_e, stride_s;
	double tgenerarposi, tenergycpu, tordenarcpu, tcolocarcpu, tmejorainicpu;
	double tgenerarposi_t, tenergycpu_t, tordenarcpu_t, tcolocarcpu_t, tmejorainicpu_t;	
	struct vectors_t vectors_selec;
	unsigned int *nlinks = (unsigned int *)calloc(vectors_e->files,sizeof(unsigned int));
	
	switch (param.mode) {
		case 0:
			//GPU SELECTION
			/*int *device_current;
			cudaDeviceProp *prop;
			prop = new cudaDeviceProp();	
			device_current = new int;
			cudaSetDevice(devices->id[0]);
			cudaGetDevice(device_current);
			cudaGetDeviceProperties (prop, *device_current);
			printf("\n%s\n",prop->name);*/
			//NEItemp	= metaheuristic.NEIIni;
			//NEItemp = getPadding(NEItemp,16);
			//metaheuristic.NEIIni += NEItemp;
			NEI_selec_temp = (int)ceil((metaheuristic.NEIIni * metaheuristic.PEMIni) / 100);
			metaheuristic.NEIIni_selec = NEI_selec_temp;// + getPadding(NEI_selec_temp,16);
			vectors_e->nconformations = vectors_e->files * metaheuristic.NEIIni;
			//printf("vectors_e->nconformations %d\n",vectors_e->nconformations);
			vectors_e->conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->ql = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->ligtype = (char *)malloc(sizeof(char)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->subtype = (char *)malloc(sizeof(char)*vectors_e->nlig*SUBTYPEMAXLEN*vectors_e->nconformations);
			vectors_e->bonds = (unsigned int *)malloc(sizeof(unsigned int)*vectors_e->nlig*MAXBOND*vectors_e->nconformations);
			vectors_e->nbonds = (char *)malloc(sizeof(char)*vectors_e->nlig*vectors_e->nconformations);
			
			vectors_e->move_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->move_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->move_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_e->nconformations);
			vectors_e->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			//for (int i=0;i<20;i++) printf("i %d, eps %f, sig %f\n",i,vectors_e->vdw_params[i].epsilon,vectors_e->vdw_params[i].sigma);
			fill_conformations_vs(param,metaheuristic,c_x,c_y,c_z,ql,ligtype,subtype,nbonds,bonds,vectors_e);
			tgenerarposi = wtime();//tomamos tiempos
			generate_positions_vs (param,vectors_e,metaheuristic,devices,0,0);			
			tgenerarposi_t += wtime() - tgenerarposi;
			
			//for (int i=0;i<vectors_e->nconformations*vectors_e->nlig;i++) printf("id %d c_x %f c_y %f c_z %f m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f\n",i,vectors_e->conformations_x[i], vectors_e->conformations_y[i], vectors_e->conformations_z[i],vectors_e->move_x[i/vectors_e->nlig],vectors_e->move_y[i/vectors_e->nlig],vectors_e->move_z[i/vectors_e->nlig],vectors_e->quat_x[i/vectors_e->nlig],vectors_e->quat_y[i/vectors_e->nlig],vectors_e->quat_z[i/vectors_e->nlig]);
			//exit(0);		
			
			tenergycpu = wtime();//tomamos tiempos para calculo
			max_conformaciones = ((devices->propiedades[0].maxGridSize[0] * devices->hilos[0*metaheuristic.num_kernels]) / WARP_SIZE) * 8;
			if (vectors_e->nconformations < max_conformaciones)
				gpuSolver_vs (metaheuristic.NEIIni,vectors_e->files,vectors_e->nlig,vectors_e->ligtype,vectors_e->bonds,vectors_e->ql,ligand_params,proteina,param,metaheuristic,devices,vectors_e->conformations_x, vectors_e->conformations_y, vectors_e->conformations_z,vectors_e->move_x,vectors_e->move_y,vectors_e->move_z,vectors_e->quat_x,vectors_e->quat_y,vectors_e->quat_z,vectors_e->quat_w,vectors_e->energy.energy,vectors_e->energy.n_conformation,vectors_e->weights,vectors_e->f_params,vectors_e->nconformations,0,0);		
			else
			{
				//DIVIDIR EN TROZOS
				while((vectors_e->nconformations % max_conformaciones) != 0) max_conformaciones --;		
				//printf("va a trocear\n");				
				for (unsigned int desplazamiento=0;desplazamiento<vectors_e->nconformations;desplazamiento+=max_conformaciones)
				{
					gpuSolver_vs (metaheuristic.NEIIni,vectors_e->files,vectors_e->nlig,vectors_e->ligtype,vectors_e->bonds,vectors_e->ql,ligand_params,proteina,param,metaheuristic,devices,vectors_e->conformations_x+(desplazamiento*vectors_e->nlig), vectors_e->conformations_y+(desplazamiento*vectors_e->nlig),vectors_e->conformations_z+(desplazamiento*vectors_e->nlig), vectors_e->move_x+desplazamiento,vectors_e->move_y+desplazamiento,vectors_e->move_z+desplazamiento,vectors_e->quat_x+desplazamiento,vectors_e->quat_y+desplazamiento,vectors_e->quat_z+desplazamiento,vectors_e->quat_w+desplazamiento,vectors_e->energy.energy+desplazamiento,vectors_e->energy.n_conformation+desplazamiento,vectors_e->weights,vectors_e->f_params,max_conformaciones,0,desplazamiento);
					//printf("Device %d con trozo %d\n",devices->id[th],i);					
				}
			}			
			tenergycpu_t += wtime() - tenergycpu;
			//for (int i=0;i<vectors_e->nconformations;i++) printf("id %d m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f energy %f\n",i,vectors_e->move_x[i],vectors_e->move_y[i],vectors_e->move_z[i],vectors_e->quat_x[i],vectors_e->quat_y[i],vectors_e->quat_z[i], vectors_e->energy.energy[i]);
			//exit(0);
			tordenarcpu = wtime();
			//exit(0);
			omp_set_num_threads (metaheuristic.Threads1Ini);
			#pragma omp parallel for			
			for (unsigned int i = 0; i < vectors_e->nconformations; i += metaheuristic.NEIIni)							
			    thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + metaheuristic.NEIIni), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());			
			//colocar_vs(vectors_e->nlig,param,vectors_e,metaheuristic);	
			#pragma omp parallel for
                        for (unsigned int i = 0; i < vectors_e->files; i++)	
				colocar_vs_by_step (param,metaheuristic,vectors_e->nlig,vectors_e->move_x + (metaheuristic.NEIIni*i),vectors_e->move_y + (metaheuristic.NEIIni*i),vectors_e->move_z + (metaheuristic.NEIIni*i),vectors_e->quat_x + (metaheuristic.NEIIni*i),vectors_e->quat_y + (metaheuristic.NEIIni*i),vectors_e->quat_z + (metaheuristic.NEIIni*i),vectors_e->quat_w + (metaheuristic.NEIIni*i),vectors_e->energy.energy + (metaheuristic.NEIIni*i),vectors_e->energy.n_conformation + (metaheuristic.NEIIni*i),metaheuristic.NEIIni,i);
			thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			
			tordenarcpu_t += wtime() - tordenarcpu;		

			//exit(0);
			if (metaheuristic.PEMIni > 0)
			{
				tmejorainicpu = wtime(); //tomamos tiempos de mejora
				seleccionar_inicializar_vs(vectors_e->nlig,vectors_e,&vectors_selec,param,metaheuristic);
				if (metaheuristic.NEEImp > 0)
				{
					if (param.montecarlo)
                                                montecarlo_vs_environment(proteina,ligand_params,param,&vectors_selec,metaheuristic,devices,0,0);
                                        else
                                                gpu_mejorar_vs_environment (proteina,ligand_params,param,&vectors_selec,metaheuristic,devices,0,vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.ql,0);
				}
				else
				{	
					if (param.montecarlo)
						montecarlo_vs(proteina,ligand_params,param,&vectors_selec,metaheuristic,devices,0,0);
					else
						gpu_mejorar_vs (proteina,ligand_params,param,&vectors_selec,metaheuristic,devices,0,vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.ql,0);						
				}
				//incluir_gpu_vs (vectors_e->nlig,&vectors_selec,vectors_e,param,metaheuristic);
				stride_e = vectors_selec.nconformations / vectors_selec.files;
				stride_s = vectors_e->nconformations / vectors_e->files;
				#pragma omp parallel for
	                        for (unsigned int i = 0; i < vectors_e->files; i++)
					incluir_gpu_vs_by_step (vectors_e->nlig,vectors_selec.move_x + (stride_e*i), vectors_selec.move_y + (stride_e*i), vectors_selec.move_z + (stride_e*i), vectors_selec.quat_x + (stride_e*i), vectors_selec.quat_y + (stride_e*i), vectors_selec.quat_z + (stride_e*i), vectors_selec.quat_w + (stride_e*i), vectors_selec.energy.energy + (stride_e*i), vectors_selec.energy.n_conformation + (stride_e*i), vectors_e->move_x + (stride_s*i),  vectors_e->move_y + (stride_s*i),  vectors_e->move_z + (stride_s*i),  vectors_e->quat_x + (stride_s*i),  vectors_e->quat_y + (stride_s*i),  vectors_e->quat_z + (stride_s*i),  vectors_e->quat_w + (stride_s*i),  vectors_e->energy.energy + (stride_s*i),  vectors_e->energy.n_conformation + (stride_s*i), stride_e, stride_s, i,param,metaheuristic);
				thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);	
				tmejorainicpu_t += wtime() - tmejorainicpu;				

				//#pragma omp parallel for
				//for (unsigned int i = 0; i < vectors_e->nconformations; i += metaheuristic.NEIIni)			
				//	thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + metaheuristic.NEIIni), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());										
				//colocar_vs(vectors_e->nlig,param,vectors_e,metaheuristic);						
				//#pragma omp parallel for
				//for (unsigned int i = 0; i < vectors_e->files; i++)
				//	colocar_vs_by_step (param,metaheuristic,vectors_e->nlig,vectors_e->move_x + (metaheuristic.NEIIni*i),vectors_e->move_y + (metaheuristic.NEIIni*i),vectors_e->move_z + (metaheuristic.NEIIni*i),vectors_e->quat_x + (metaheuristic.NEIIni*i),vectors_e->quat_y + (metaheuristic.NEIIni*i),vectors_e->quat_z + (metaheuristic.NEIIni*i),vectors_e->quat_w + (metaheuristic.NEIIni*i),vectors_e->energy.energy + (metaheuristic.NEIIni*i),vectors_e->energy.n_conformation + (metaheuristic.NEIIni*i),metaheuristic.NEIIni,i);
				//thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			}
			//for (int i=0;i<vectors_e->nconformations;i++) printf("id %d m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f energy %f\n",i,vectors_e->move_x[i],vectors_e->move_y[i],vectors_e->move_z[i],vectors_e->quat_x[i],vectors_e->quat_y[i],vectors_e->quat_z[i], vectors_e->energy.energy[i]);
			//exit(0);
			
			seleccionar_NEFIni_vs(vectors_e->nlig,vectors_e,vectors_s,metaheuristic,param);
			printf("\nFases INICILIZAR:\n");
			printf("\tTiempo Generacion posiciones: %lf seg\n",tgenerarposi_t);				
			printf("\tTiempo ES+VDW+HB potencial: %lf seg\n",tenergycpu_t);	
			printf("\tTiempo Ordenar: %lf seg\n",tordenarcpu_t);
			if (metaheuristic.PEMIni > 0) printf("\tTiempo Mejorar: %lf seg\n",tmejorainicpu_t);	
			printf("\tTiempo Colocar: %lf seg\n",tcolocarcpu_t);		
			//exit(0);
			break;			
					
		case 1:
		case 2:
			switch (param.mode) {
				case 1:
					th1 = 1;
					break;
				case 2:
					th1 = metaheuristic.Threads1Ini;
					break;
			}			
			//OPEN-MP SELECTION
			vectors_e->nconformations = vectors_e->files * metaheuristic.NEIIni;
			vectors_e->conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->ql = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->ligtype = (char *)malloc(sizeof(char)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->subtype = (char *)malloc(sizeof(char)*vectors_e->nlig*SUBTYPEMAXLEN*vectors_e->nconformations);
			vectors_e->bonds = (unsigned int *)malloc(sizeof(unsigned int)*vectors_e->nlig*MAXBOND*vectors_e->nconformations);
			vectors_e->nbonds = (char *)malloc(sizeof(char)*vectors_e->nlig*vectors_e->nconformations);
			
			vectors_e->move_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->move_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->move_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_e->nconformations);
			vectors_e->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			
			fill_conformations_vs(param,metaheuristic,c_x,c_y,c_z,ql,ligtype,subtype,nbonds,bonds,vectors_e);
				
			tgenerarposi = wtime();//tomamos tiempos
			generate_positions_cpp_vs (param,vectors_e,metaheuristic);
			tgenerarposi_t += wtime() - tgenerarposi;
			//for (int i=0;i<vectors_e->nconformations;i++) printf("id %d m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f\n",i,vectors_e->move_x[i],vectors_e->move_y[i],vectors_e->move_z[i],vectors_e->quat_x[i],vectors_e->quat_y[i],vectors_e->quat_z[i]);
			//exit(0);
			tenergycpu = wtime();//tomamos tiempos para calculo		
			//printf("energia \n");	
			ForcesCpuSolver_vs(nlinks,vectors_e->nlig,vectors_e->files,param,proteina,ligand_params,vectors_e->conformations_x,vectors_e->conformations_y,vectors_e->conformations_z,vectors_e->move_x,vectors_e->move_y,vectors_e->move_z,vectors_e->quat_x,vectors_e->quat_y,vectors_e->quat_z,vectors_e->quat_w,vectors_e->ligtype,vectors_e->ql,vectors_e->bonds,vectors_e->energy.energy,vectors_e->energy.n_conformation,vectors_e->weights,vectors_e->f_params,vectors_e->nconformations,metaheuristic);
			tenergycpu_t += wtime() - tenergycpu;
			omp_set_num_threads(th1);
			#pragma omp parallel for
			for (unsigned int i = 0; i < vectors_e->nconformations; i += metaheuristic.NEIIni)							
			    thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + metaheuristic.NEIIni), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());			
			//colocar_vs(vectors_e->nlig,param,vectors_e,metaheuristic);
			#pragma omp parallel for
                        for (unsigned int i = 0; i < vectors_e->files; i++)
                                colocar_vs_by_step (param,metaheuristic,vectors_e->nlig,vectors_e->move_x + (metaheuristic.NEIIni*i),vectors_e->move_y + (metaheuristic.NEIIni*i),vectors_e->move_z + (metaheuristic.NEIIni*i),vectors_e->quat_x + (metaheuristic.NEIIni*i),vectors_e->quat_y + (metaheuristic.NEIIni*i),vectors_e->quat_z + (metaheuristic.NEIIni*i),vectors_e->quat_w + (metaheuristic.NEIIni*i),vectors_e->energy.energy + (metaheuristic.NEIIni*i),vectors_e->energy.n_conformation + (metaheuristic.NEIIni*i),metaheuristic.NEIIni,i);
                        thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			
			
			if (metaheuristic.PEMIni > 0)
			{		
				//printf("MEJORANDO %d\n");	
				tmejorainicpu = wtime(); //tomamos tiempos de mejora
				seleccionar_inicializar_vs(vectors_e->nlig,vectors_e,&vectors_selec,param,metaheuristic);
				if (metaheuristic.NEEImp > 0)
				{
					if (param.montecarlo)
                                                montecarlo_cpp_vs_environment(proteina,ligand_params,param,vectors_selec.weights,vectors_selec.f_params, vectors_selec.conformations_x,vectors_selec.conformations_y,vectors_selec.conformations_z,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z,vectors_selec.quat_x,vectors_selec.quat_y,vectors_selec.quat_z,vectors_selec.quat_w,vectors_selec.energy.energy,vectors_selec.energy.n_conformation,vectors_selec.ql,vectors_selec.ligtype,vectors_selec.bonds, vectors_selec.nlig, vectors_selec.files, vectors_selec.nconformations,metaheuristic,0);
                                        else
                                                mejorar_vs_environment(proteina,ligand_params,param,vectors_selec.weights,vectors_selec.f_params,vectors_selec.conformations_x,vectors_selec.conformations_y,vectors_selec.conformations_z,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z,vectors_selec.quat_x,vectors_selec.quat_y,vectors_selec.quat_z,vectors_selec.quat_w,vectors_selec.energy.energy,vectors_selec.energy.n_conformation,vectors_selec.ql,vectors_selec.ligtype,vectors_selec.bonds, vectors_selec.nlig, vectors_selec.files, vectors_selec.nconformations,metaheuristic,0);
				}
				else
				{	
					if (param.montecarlo)		
						montecarlo_cpp_vs(proteina,ligand_params,param,vectors_selec.weights,vectors_selec.f_params, vectors_selec.conformations_x,vectors_selec.conformations_y,vectors_selec.conformations_z,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z,vectors_selec.quat_x,vectors_selec.quat_y,vectors_selec.quat_z,vectors_selec.quat_w,vectors_selec.energy.energy,vectors_selec.energy.n_conformation,vectors_selec.ql,vectors_selec.ligtype,vectors_selec.bonds, vectors_selec.nlig, vectors_selec.files, vectors_selec.nconformations,metaheuristic,0);						
					else
						mejorar_vs(proteina,ligand_params,param,vectors_selec.weights,vectors_selec.f_params,vectors_selec.conformations_x,vectors_selec.conformations_y,vectors_selec.conformations_z,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z,vectors_selec.quat_x,vectors_selec.quat_y,vectors_selec.quat_z,vectors_selec.quat_w,vectors_selec.energy.energy,vectors_selec.energy.n_conformation,vectors_selec.ql,vectors_selec.ligtype,vectors_selec.bonds, vectors_selec.nlig, vectors_selec.files, vectors_selec.nconformations,metaheuristic,0);				
				}
				//incluir_gpu_vs (vectors_e->nlig,&vectors_selec,vectors_e,param,metaheuristic);
				stride_e = vectors_selec.nconformations / vectors_selec.files;
                                stride_s = vectors_e->nconformations / vectors_e->files;
                                #pragma omp parallel for
                                for (unsigned int i = 0; i < vectors_e->files; i++)
                                        incluir_gpu_vs_by_step (vectors_e->nlig,vectors_selec.move_x + (stride_e*i), vectors_selec.move_y + (stride_e*i), vectors_selec.move_z + (stride_e*i), vectors_selec.quat_x + (stride_e*i), vectors_selec.quat_y + (stride_e*i), vectors_selec.quat_z + (stride_e*i), vectors_selec.quat_w + (stride_e*i), vectors_selec.energy.energy + (stride_e*i), vectors_selec.energy.n_conformation + (stride_e*i), vectors_e->move_x + (stride_s*i),  vectors_e->move_y + (stride_s*i),  vectors_e->move_z + (stride_s*i),  vectors_e->quat_x + (stride_s*i),  vectors_e->quat_y + (stride_s*i),  vectors_e->quat_z + (stride_s*i),  vectors_e->quat_w + (stride_s*i),  vectors_e->energy.energy + (stride_s*i),  vectors_e->energy.n_conformation + (stride_s*i), stride_e, stride_s, i,param,metaheuristic);
                                thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
				//omp_set_num_threads(metaheuristic.Threads1Ini);
				//#pragma omp parallel for
				//for (unsigned int i = 0; i < vectors_e->nconformations; i += metaheuristic.NEIIni)			
				//	thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + metaheuristic.NEIIni), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());										
				//colocar_vs(vectors_e->nlig,param,vectors_e,metaheuristic);
				//#pragma omp parallel for
                                //for (unsigned int i = 0; i < vectors_e->files; i++)
                                //      colocar_vs_by_step (param,metaheuristic,vectors_e->nlig,vectors_e->move_x + (metaheuristic.NEIIni*i),vectors_e->move_y + (metaheuristic.NEIIni*i),vectors_e->move_z + (metaheuristic.NEIIni*i),vectors_e->quat_x + (metaheuristic.NEIIni*i),vectors_e->quat_y + (metaheuristic.NEIIni*i),vectors_e->quat_z + (metaheuristic.NEIIni*i),vectors_e->quat_w + (metaheuristic.NEIIni*i),vectors_e->energy.energy + (metaheuristic.NEIIni*i),vectors_e->energy.n_conformation + (metaheuristic.NEIIni*i),metaheuristic.NEIIni,i);
                                //thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
				tmejorainicpu_t += wtime() - tmejorainicpu;							
				//for(int k=0;k<160;k++)
				//		printf("STP %d Mejorar VS-number %d, m_x %f, m_y %f, m_z %f, energy %f\n",steps,k,vectors_s->move_x[k],vectors_s->move_y[k],vectors_s->move_z[k],vectors_s->energy.energy[k]);
		
			}	
			//for (int i=0;i<vectors_e->nconformations;i++) printf("id %d m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f en %f\n",i,vectors_e->move_x[i],vectors_e->move_y[i],vectors_e->move_z[i],vectors_e->quat_x[i],vectors_e->quat_y[i],vectors_e->quat_z[i],vectors_e->energy.energy[i]);
			//exit(0);
			
			seleccionar_NEFIni_vs(vectors_e->nlig,vectors_e,vectors_s,metaheuristic,param);
			printf("\nFases INICILIZAR:\n");
			printf("\tTiempo Generacion posiciones: %lf seg\n",tgenerarposi_t);				
			printf("\tTiempo ES+VDW+HB potencial: %lf seg\n",tenergycpu_t);	
			printf("\tTiempo Ordenar: %lf seg\n",tordenarcpu_t);
			if (metaheuristic.PEMIni > 0) printf("\tTiempo Mejorar: %lf seg\n",tmejorainicpu_t);	
			printf("\tTiempo Colocar: %lf seg\n",tcolocarcpu_t);
			break;
			
		case 3:
			//MULTIGPU SELECTION
			/*int n_devices,k,*device_current_m;
			cudaDeviceProp *prop_m;
			device_current_m = new int;
			prop_m = new cudaDeviceProp();	
			cudaSetDevice(param.ngpu);
			cudaGetDevice(device_current_m);
			cudaGetDeviceProperties (prop_m, *device_current_m);
			printf("\n%s\n",prop_m->name);*/
			//NEItemp	= metaheuristic.NEIIni;
			//NEItemp = getPadding(NEItemp,16);
			//metaheuristic.NEIIni += NEItemp;
			NEI_selec_temp = (int)ceil((metaheuristic.NEIIni * metaheuristic.PEMIni) / 100);
			metaheuristic.NEIIni_selec = NEI_selec_temp;// + getPadding(NEI_selec_temp,16);
			vectors_e->nconformations = vectors_e->files * metaheuristic.NEIIni;
			vectors_e->conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->ql = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->ligtype = (char *)malloc(sizeof(char)*vectors_e->nlig*vectors_e->nconformations);
			vectors_e->subtype = (char *)malloc(sizeof(char)*vectors_e->nlig*SUBTYPEMAXLEN*vectors_e->nconformations);
			vectors_e->bonds = (unsigned int *)malloc(sizeof(unsigned int)*vectors_e->nlig*MAXBOND*vectors_e->nconformations);
			vectors_e->nbonds = (char *)malloc(sizeof(char)*vectors_e->nlig*vectors_e->nconformations);
			
			vectors_e->move_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->move_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->move_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			vectors_e->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_e->nconformations);
			vectors_e->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
			thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			//for (int i=0;i<20;i++) printf("i %d, eps %f, sig %f\n",i,vectors_e->vdw_params[i].epsilon,vectors_e->vdw_params[i].sigma);
			fill_conformations_vs(param,metaheuristic,c_x,c_y,c_z,ql,ligtype,subtype,nbonds,bonds,vectors_e);
			tgenerarposi = wtime();//tomamos tiempos
			generate_positions_vs (param,vectors_e,metaheuristic,devices,0,0);			
			tgenerarposi_t += wtime() - tgenerarposi;
			//for (int i=0;i<vectors_e->nconformations*vectors_e->nlig;i++) printf("id %d c_x %f c_y %f c_z %f m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f\n",i/vectors_e->nlig,vectors_e->conformations_x[i], vectors_e->conformations_y[i], vectors_e->conformations_z[i],vectors_e->move_x[i/vectors_e->nlig],vectors_e->move_y[i/vectors_e->nlig],vectors_e->move_z[i/vectors_e->nlig],vectors_e->quat_x[i/vectors_e->nlig],vectors_e->quat_y[i/vectors_e->nlig],vectors_e->quat_z[i/vectors_e->nlig]);
			//exit(0);		
						
			tenergycpu = wtime();//tomamos tiempos para calculo L-J
			multigpuSolver_vs (vectors_e->files,vectors_e->nlig, vectors_e->ligtype, vectors_e->bonds, vectors_e->ql, ligand_params, proteina, vectors_e->conformations_x, vectors_e->conformations_y, vectors_e->conformations_z, vectors_e->move_x, vectors_e->move_y, vectors_e->move_z, vectors_e->quat_x, vectors_e->quat_y, vectors_e->quat_z, vectors_e->quat_w, vectors_e->energy, vectors_e->weights, vectors_e->f_params, vectors_e->nconformations, param, metaheuristic, devices);			
			tenergycpu_t += wtime() - tenergycpu;
			//for (int i=0;i<vectors_e->nconformations;i++) printf("id %d m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f en %f number %d\n",i,vectors_e->move_x[i],vectors_e->move_y[i],vectors_e->move_z[i],vectors_e->quat_x[i],vectors_e->quat_y[i],vectors_e->quat_z[i],vectors_e->energy.energy[i],vectors_e->energy.n_conformation[i]);
			//exit(0);
			omp_set_num_threads (metaheuristic.Threads1Ini);
			#pragma omp parallel for			
			for (unsigned int i = 0; i < vectors_e->nconformations; i += metaheuristic.NEIIni)							
			    thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + metaheuristic.NEIIni), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());			
			//colocar_vs(vectors_e->nlig,param,vectors_e,metaheuristic);	
			#pragma omp parallel for
                        for (unsigned int i = 0; i < vectors_e->files; i++)
                                colocar_vs_by_step (param,metaheuristic,vectors_e->nlig,vectors_e->move_x + (metaheuristic.NEIIni*i),vectors_e->move_y + (metaheuristic.NEIIni*i),vectors_e->move_z + (metaheuristic.NEIIni*i),vectors_e->quat_x + (metaheuristic.NEIIni*i),vectors_e->quat_y + (metaheuristic.NEIIni*i),vectors_e->quat_z + (metaheuristic.NEIIni*i),vectors_e->quat_w + (metaheuristic.NEIIni*i),vectors_e->energy.energy + (metaheuristic.NEIIni*i),vectors_e->energy.n_conformation + (metaheuristic.NEIIni*i),metaheuristic.NEIIni,i);
                        thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			tordenarcpu_t += wtime() - tordenarcpu;		
			
			if (metaheuristic.PEMIni > 0)
			{
				tmejorainicpu = wtime(); //tomamos tiempos de mejora
				seleccionar_inicializar_vs(vectors_e->nlig,vectors_e,&vectors_selec,param,metaheuristic);
				if (metaheuristic.NEEImp > 0)
				{
					multigpuSolver_mejora_vs_environment (vectors_selec.files,vectors_selec.nlig, vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.ql,ligand_params,proteina,vectors_selec.conformations_x,vectors_selec.conformations_y,vectors_selec.conformations_z,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z,vectors_selec.quat_x,vectors_selec.quat_y,vectors_selec.quat_z,vectors_selec.quat_w,vectors_selec.energy.energy,vectors_selec.energy.n_conformation,vectors_selec.weights,vectors_selec.f_params,vectors_selec.nconformations,param,metaheuristic,devices,0);
				}
				else
				{	
					multigpuSolver_mejora_vs (vectors_selec.files,vectors_selec.nlig, vectors_selec.ligtype,vectors_selec.bonds,vectors_selec.ql,ligand_params,proteina,vectors_selec.conformations_x,vectors_selec.conformations_y,vectors_selec.conformations_z,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z,vectors_selec.quat_x,vectors_selec.quat_y,vectors_selec.quat_z,vectors_selec.quat_w,vectors_selec.energy.energy,vectors_selec.energy.n_conformation,vectors_selec.weights,vectors_selec.f_params,vectors_selec.nconformations,param,metaheuristic,devices,0);
				}
				//incluir_gpu_vs (vectors_e->nlig,&vectors_selec,vectors_e,param,metaheuristic);
				stride_e = vectors_selec.nconformations / vectors_selec.files;
                                stride_s = vectors_e->nconformations / vectors_e->files;
                                #pragma omp parallel for
                                for (unsigned int i = 0; i < vectors_e->files; i++)
                                        incluir_gpu_vs_by_step (vectors_e->nlig,vectors_selec.move_x + (stride_e*i), vectors_selec.move_y + (stride_e*i), vectors_selec.move_z + (stride_e*i), vectors_selec.quat_x + (stride_e*i), vectors_selec.quat_y + (stride_e*i), vectors_selec.quat_z + (stride_e*i), vectors_selec.quat_w + (stride_e*i), vectors_selec.energy.energy + (stride_e*i), vectors_selec.energy.n_conformation + (stride_e*i), vectors_e->move_x + (stride_s*i),  vectors_e->move_y + (stride_s*i),  vectors_e->move_z + (stride_s*i),  vectors_e->quat_x + (stride_s*i),  vectors_e->quat_y + (stride_s*i),  vectors_e->quat_z + (stride_s*i),  vectors_e->quat_w + (stride_s*i),  vectors_e->energy.energy + (stride_s*i),  vectors_e->energy.n_conformation + (stride_s*i), stride_e, stride_s, i,param,metaheuristic);
                                thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
				tmejorainicpu_t += wtime() - tmejorainicpu;				

				//#pragma omp parallel for
				//for (unsigned int i = 0; i < vectors_e->nconformations; i += metaheuristic.NEIIni)			
				//	thrust::stable_sort_by_key((vectors_e->energy.energy + i),(vectors_e->energy.energy + i + metaheuristic.NEIIni), (vectors_e->energy.n_conformation + i), thrust::less<type_data>());										
				//colocar_vs(vectors_e->nlig,param,vectors_e,metaheuristic);						
				//#pragma omp parallel for
                                //for (unsigned int i = 0; i < vectors_e->files; i++)
                                //        colocar_vs_by_step (param,metaheuristic,vectors_e->nlig,vectors_e->move_x + (metaheuristic.NEIIni*i),vectors_e->move_y + (metaheuristic.NEIIni*i),vectors_e->move_z + (metaheuristic.NEIIni*i),vectors_e->quat_x + (metaheuristic.NEIIni*i),vectors_e->quat_y + (metaheuristic.NEIIni*i),vectors_e->quat_z + (metaheuristic.NEIIni*i),vectors_e->quat_w + (metaheuristic.NEIIni*i),vectors_e->energy.energy + (metaheuristic.NEIIni*i),vectors_e->energy.n_conformation + (metaheuristic.NEIIni*i),metaheuristic.NEIIni,i);
                                //thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
			}
			//for (int i=0;i<vectors_e->nconformations;i++) printf("id %d m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f energy %f\n",i,vectors_e->move_x[i],vectors_e->move_y[i],vectors_e->move_z[i],vectors_e->quat_x[i],vectors_e->quat_y[i],vectors_e->quat_z[i], vectors_e->energy.energy[i]);
			//exit(0);
			
			seleccionar_NEFIni_vs(vectors_e->nlig,vectors_e,vectors_s,metaheuristic,param);
			printf("\nFases INICILIZAR:\n");
			printf("\tTiempo Generacion posiciones: %lf seg\n",tgenerarposi_t);				
			printf("\tTiempo ES+VDW+HB potencial: %lf seg\n",tenergycpu_t);	
			printf("\tTiempo Ordenar: %lf seg\n",tordenarcpu_t);
			if (metaheuristic.PEMIni > 0) printf("\tTiempo Mejorar: %lf seg\n",tmejorainicpu_t);	
			printf("\tTiempo Colocar: %lf seg\n",tcolocarcpu_t);		
			//exit(0);
			break;			
	}
	//printf("ANTES DE LOS FREE\n");	
	if (metaheuristic.PEMIni > 0)
	{	
		free(vectors_selec.conformations_x);
		free(vectors_selec.conformations_y);
		free(vectors_selec.conformations_z);
		free(vectors_selec.move_x);
		free(vectors_selec.move_y);
		free(vectors_selec.move_z);
		free(vectors_selec.quat_x);
		free(vectors_selec.quat_y);
		free(vectors_selec.quat_z);
		free(vectors_selec.quat_w);
		free(vectors_selec.energy.energy);
		free(vectors_selec.energy.n_conformation);
		free(vectors_selec.weights);
		free(vectors_selec.ql);
		free(vectors_selec.ligtype);
		free(vectors_selec.subtype);
		free(vectors_selec.bonds);
		free(vectors_selec.nbonds);			
	}
	//printf("FIN\n");
}


