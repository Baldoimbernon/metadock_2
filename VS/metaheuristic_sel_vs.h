#ifndef __METAHEU_SEL_VS_H__
#define __METAHEU_SEL_VS_H__

extern void seleccionar_vs (unsigned int nlig, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param);

#endif
