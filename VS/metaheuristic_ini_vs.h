#ifndef __METAHEU_INI_VS_H__
#define __METAHEU_INI_VS_H__

#include "energy_struct.h"

extern void incluir_gpu_vs_by_step (unsigned int nlig, type_data *e_move_x, type_data *e_move_y, type_data *e_move_z, type_data *e_quat_x, type_data *e_quat_y, type_data *e_quat_z, type_data *e_quat_w, type_data *e_energy, unsigned int *e_nconfs, type_data *s_move_x, type_data *s_move_y, type_data *s_move_z, type_data *s_quat_x, type_data *s_quat_y, type_data *s_quat_z, type_data *s_quat_w, type_data *s_energy, unsigned int *s_nconfs, unsigned int stride_e, unsigned int stride_s, unsigned int pos,struct param_t param, struct metaheuristic_t metaheuristic
);
extern void seleccionar_mejorar_vs (unsigned int nlig, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic);
extern void fill_conformations_vs (struct param_t param, struct metaheuristic_t metaheuristic, type_data *c_x, type_data *c_y, type_data *c_z, type_data *ql, char *ligtype, char *subtype, char *nbonds, unsigned int *bonds,  struct vectors_t *vectors_e); 
extern void seleccionar_NEFIni_vs (unsigned int nlig, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct metaheuristic_t metaheuristic, struct param_t param);
extern void seleccionar_inicializar_vs (unsigned int nlig, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic);
extern void mejorar_vs (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct param_t param, type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *ql, char *ligtype, unsigned int *bonds, unsigned int nlig, unsigned int files, unsigned int nconformations, struct metaheuristic_t metaheuristic, unsigned int opt);

extern void incluir_mejorar_inicializar_vs (unsigned int nlig, struct vectors_t vectors_e, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, struct metaheuristic_t metaheuristic, struct param_t param);
extern void incluir_mejorar_inicializar_vs_environment (unsigned int nlig, struct vectors_t vectors_e, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int nconformations, struct metaheuristic_t metaheuristic, struct param_t param);

extern void mejorar_vs_environment (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct param_t param, type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *ql, char *ligtype, unsigned int *bonds, unsigned int nlig, unsigned int files, unsigned int nconformations, struct metaheuristic_t metaheuristic, unsigned int opt);

extern void inicializar_vs (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, type_data *c_x, type_data *c_y, type_data *c_z, type_data *ql, char *ligtype, char *subtype, char *nbonds, unsigned int *bonds);
extern void seleccionar_mutar_vs (unsigned int nlig, struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, struct metaheuristic_t metaheuristic);

#endif
