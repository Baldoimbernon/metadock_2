#include "energy_cuda.h"
#include "energy_moving.h"
#include "energy_common-gpu.h"
#include "energy_struct.h"
#include "energy_kernel.h"
#include "energy_common.h"

#define PI 3.141592653589793

extern __device__ void rotate3D_flexibility_vs (type_data  q_x, type_data  q_y, type_data  q_z, type_data  q_w, type_data* src, type_data * dst){

	type_data x,y,z;

	x = ((q_w*q_w + q_x*q_x - q_y*q_y - q_z*q_z)*src[0]) + (2*(q_x*q_y - q_w*q_z)*src[1]) + (2*(q_x*q_z + q_w*q_y)*src[2]);
	y = (2*(q_y*q_x + q_w*q_z))*src[0] + ((q_w * q_w - q_x*q_x + q_y*q_y - q_z*q_z)*src[1]) + (2*(q_y*q_z - q_w*q_x)*src[2]);
	z = (2*(q_z*q_x - q_w*q_y))*src[0] + (2*(q_z*q_y + q_w*q_x)*src[1]) + ((q_w*q_w - q_x*q_x - q_y*q_y + q_z*q_z)*src[2]);
	dst[0] = x;
 	dst[1] = y;
	dst[2] = z;

}

extern __device__ void setRotation_flexibility_vs (type_data *q_x, type_data *q_y, type_data *q_z, type_data *q_w, type_data angle, type_data * vector){
	angle = angle/2;
	type_data module_v = sqrt((vector[0]*vector[0]) + (vector[1]*vector[1]) + (vector[2]*vector[2]));
	*q_w = cos (angle);
	*q_x = (vector[0] * sin (angle))/module_v;
	*q_y = (vector[1] * sin (angle))/module_v;
	*q_z = (vector[2] * sin (angle))/module_v;
}

extern __device__ void rotateLigandFragment_vs(int *f, unsigned int size_f, type_data* lig_x, type_data* lig_y, type_data* lig_z, type_data qeje_x, type_data qeje_y, type_data qeje_z, type_data qeje_w, type_data p1[3]) {

	type_data punto1[3], punto[3];

	for (unsigned int i1 = 0; i1 < size_f;i1++)
	{
		punto[0] = lig_x[(f[i1]-1)]   - p1[0];
		punto[1] = lig_y[(f[i1]-1)] - p1[1];
		punto[2] = lig_z[(f[i1]-1)] - p1[2];

		//ROTAMOS UN PUNTO (PUNTO) ALREDEDOR DE UN EJE MARCADO POR LOS PUNTOS B - A
		rotate3D_flexibility_vs(qeje_x,qeje_y,qeje_z,qeje_w,punto,punto1);
		//PUNTO GIRADO EN (PUNTO1)


		lig_x[(f[i1]-1)]   = punto1[0] + p1[0];
		lig_y[(f[i1]-1)] = punto1[1] + p1[1];
		lig_z[(f[i1]-1)] = punto1[2] + p1[2];
	}
}

__global__ void flexibilityKernel_vs (type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, curandState_t * states, struct flexibility_data_t * flexibility_conformations, struct flexibility_params_t *flexibility_params, unsigned int nlig, unsigned int max_angle_flex, unsigned int stride_d, unsigned int conf_point, unsigned int max)
{
	type_data a[3],b[3],eje[3],p1[3];
	type_data ang;
	unsigned int num_point, frag_act, position_link, file;
	unsigned int rot_point, link;
	type_data qeje_x, qeje_y, qeje_z, qeje_w;

	//OBTENEMOS LA CONFORMACION
	unsigned int tid = threadIdx.x + threadIdx.y * blockDim.x;
	unsigned int blockID = blockIdx.x + blockIdx.y * gridDim.x;
	unsigned int id_conformation = blockID * (blockDim.x * blockDim.y) + (threadIdx.y*blockDim.x) + threadIdx.x;
	//file = (id_conformation+stride_d) / conf_point;
	//printf("File %d id %d conf %d\n",file,id_conformation,conf_point);
	//int id_conformation;
	if (id_conformation < max)
	{
		file = (id_conformation+stride_d) / conf_point;
		//printf("File %d id %d conf %d\n",file,id_conformation,conf_point);	
		ang = flexibility_conformations[id_conformation].ang;
		link = flexibility_conformations[id_conformation].link;
		rot_point = flexibility_conformations[id_conformation].p_rot;
		//printf("id %d ang %f link %d rot_point %d\n",id_conformation,ang,link,rot_point);
		//NOS SITUAMOS EN EL VECTOR DE CONFORMACIONES Y DE ENLACES
		unsigned int position_conformation = id_conformation * nlig;

		position_link = link;
		
		//EJE DE GIRO
		a[0] = conformations_x[position_conformation + (flexibility_params[file].links[2 * position_link] - 1)];
		a[1] = conformations_y[position_conformation + (flexibility_params[file].links[2 * position_link] - 1)];
		a[2] = conformations_z[position_conformation + (flexibility_params[file].links[2 * position_link] - 1)];
		b[0] = conformations_x[position_conformation + (flexibility_params[file].links[(2 * position_link)+1] - 1)];
		b[1] = conformations_y[position_conformation + (flexibility_params[file].links[(2 * position_link)+1] - 1)];
		b[2] = conformations_z[position_conformation + (flexibility_params[file].links[(2 * position_link)+1] - 1)];

		if (rot_point == 0){
			eje[0] = b[0] - a[0];
			eje[1] = b[1] - a[1];
			eje[2] = b[2] - a[2];
			p1[0] = conformations_x[position_conformation + (flexibility_params[file].links[2 * position_link] - 1)];
			p1[1] = conformations_y[position_conformation + (flexibility_params[file].links[2 * position_link] - 1)];
			p1[2] = conformations_z[position_conformation + (flexibility_params[file].links[2 * position_link] - 1)];
			num_point = (2 * position_link) + 1;
		}else {
			eje[0] = a[0] - b[0];
			eje[1] = a[1] - b[1];
			eje[2] = a[2] - b[2];
			p1[0] = conformations_x[position_conformation + (flexibility_params[file].links[(2 * position_link)+1] - 1)];
			p1[1] = conformations_y[position_conformation + (flexibility_params[file].links[(2 * position_link)+1] - 1)];
			p1[2] = conformations_z[position_conformation + (flexibility_params[file].links[(2 * position_link)+1] - 1)];
			num_point = 2 * position_link;
		}
		
		//CONFIGURACION EJE DE ROTACION COMO QUATERNION
		setRotation_flexibility_vs(&qeje_x,&qeje_y,&qeje_z,&qeje_w,ang,eje);

		//int j2 = 0;
		unsigned int shift_fragment = 0;
		//ROTAMOS TODOS LOS FRAGMENTOS QUE TENGAN COMO ORIGEN EL PUNTO DE ROTACION
		//while ((frag_act = links_fragments_d[(num_point * n_fragments) + j2]) != 0)
		frag_act = flexibility_params[file].links_fragments[num_point];
		//{
		
		for (unsigned int i2 = 0; i2 < (frag_act - 1); i2++) 
			shift_fragment = shift_fragment + flexibility_params[file].fragments_tam[i2];
		rotateLigandFragment_vs((flexibility_params[file].fragments + shift_fragment),flexibility_params[file].fragments_tam[frag_act-1],(conformations_x + position_conformation), (conformations_y + position_conformation), (conformations_z + position_conformation), qeje_x, qeje_y, qeje_z, qeje_w, p1);
		
		
	}
}

extern void flexibility_function_vs(struct ligand_params_t *ligand_params, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, struct flexibility_data_t * flexibility_conformations,struct flexibility_params_t *flexibility_params, unsigned int nconformations, unsigned int orden_device)
{
	cudaError_t cudaStatus;
	curandState_t *states_d;
	
	/*int *links_d, *links_fragments_d, *fragments_d, *fragments_tam_d;
	int blk,max;
	type_data *conformations_x_d, *conformations_y_d, *conformations_z_d;
	struct flexibility_data_t *flexibility_conformations_d;
	
	unsigned int confSize = nconformations * ligando.nlig * sizeof(type_data);
	
	cudaSetDevice(devices->id[orden_device]);	
	cudaDeviceReset();
	
	dataToGPUConformations(nconformations,ligando.nlig,conformations_x,conformations_x_d,conformations_y,conformations_y_d,conformations_z,conformations_z_d);
	dataToGPUFlexibilityParameters(nconformations,ligando.atoms,links_d,flexibility_params->links,links_fragments_d,flexibility_params->links_fragments,fragments_d,flexibility_params->fragments,fragments_tam_d,flexibility_params->fragments_tam,flexibility_conformations,flexibility_conformations_d,flexibility_params->n_links,flexibility_params->n_fragments);	
	cudaStatus = cudaMalloc((void**)&states_d,nconformations*sizeof(curandState_t));
	save_params(vdw_params,sasa_params,hbond_params);
	 //Inicializa el generador de numeros aleatorios y el cuaternion de cada simulacion
	setupCurandState <<< (nconformations/devices->hilos[orden_device*metaheuristic.num_kernels+4])+1, devices->hilos[orden_device*metaheuristic.num_kernels+4] >>>  (states_d, param.automatic_seed,param.seed,nconformations);

	// check if kernel execution generated and error
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");

	blk = ceil(nconformations / devices->hilos[orden_device*metaheuristic.num_kernels])+1;
	max = nconformations;	
	dim3 hilos (devices->hilos[orden_device*metaheuristic.num_kernels]/16,16);
	dim3 grid  (ceil(blk/16)+1, 16);
	
	angulations_conformations <<< grid,hilos >>> (states_d, flexibility_conformations_d,flexibility_params->n_links,param.flex_angle,nconformations);
	flexibilityKernel  <<< grid,hilos>>>(conformations_x_d,conformations_y_d,conformations_z_d,states_d,flexibility_conformations_d,ligando.nlig,flexibility_params->n_links,flexibility_params->n_fragments,links_d,links_fragments_d,fragments_d,fragments_tam_d,param.flex_angle,nconformations);									
	
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernels antes de copia fallo!\n");
	
	cudaStatus = cudaMemcpy(conformations_x, conformations_x_d, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values conformations_x_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(conformations_y, conformations_y_d, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values conformations_y_d GPUtoHost failed!");
	cudaStatus = cudaMemcpy(conformations_z, conformations_z_d, confSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values conformations_z_d GPUtoHost failed!");	
	*/	
}
