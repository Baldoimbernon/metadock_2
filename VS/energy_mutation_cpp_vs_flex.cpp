#include <omp.h>
#include "wtime.h"
#include "type.h"
#include "energy_struct.h"
#include "energy_mutation_vs_flex.h"
#include "energy_CpuSolver_flex.h"
#include "energy_CpuSolver_vs.h"
#include "energy_CpuSolver_vs_flex.h"
#include "metaheuristic_ini_vs_flex.h"
#include "energy_mutation_cpp_vs_flex.h"
#include "energy_mutation_vs_flex.h"
#include "energy_CpuSolver_common.h"
#include "montecarlo_vs.h"
#include "flexibility_vs.h"
#include "rotation.h"


extern void mutation_cpp_vs_flex (struct receptor_t proteina, struct param_t param, struct metaheuristic_t metaheuristic, struct ligand_params_t *ligand_params, struct flexibility_params_t *flexibility_params, struct flexibility_data_t *flexibility_conformations, type_data *weights, struct force_field_param_t *f_params, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, unsigned int *bonds, char *ligtype, type_data *ql, unsigned int nlig, unsigned int files, unsigned int nconformations)
{
	unsigned int *n_update;
	unsigned int *nlinks = (unsigned int *)calloc(files,sizeof(unsigned int));
	type_data *internal_energy;

	n_update = (unsigned int *)malloc(sizeof(unsigned int)*nconformations);
	internal_energy = (type_data *)malloc(sizeof(type_data)*nconformations);
	
	for (int i=0; i < files; i++) nlinks[i]=flexibility_params[i].n_links;
	
	mutation_random_cpp_vs_flex (metaheuristic,nconformations,param,n_update);
	//for (int k=0;k<nconformations;k++) printf("n_update[%d] = %d\n",k,n_update[k]);
	//exit(0);
	mutation_calculation_cpp_vs_flex (param,metaheuristic,flexibility_params,n_update,param.seed,param.automatic_seed,param.max_desp,param.rotation,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,nlig,files,nconformations);
	//exit(0);
	ForcesCpuSolver_vs(nlinks,nlig,files,param,proteina,ligand_params,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,ligtype,ql,bonds,energy,nconfs,weights,f_params,nconformations,metaheuristic);
	energy_internal_conformations_cpu_vs (nconformations,nlig,files,param,metaheuristic,conformations_x,conformations_y,conformations_z,ligand_params,flexibility_params,weights,f_params,internal_energy,ql,ligtype);
        energy_total (nconformations,param,metaheuristic,energy,internal_energy);
	//exit(0);
	if (metaheuristic.IMEMUCom > 0)
        {
                if (param.montecarlo)
			montecarlo_cpp_vs_flex (proteina,ligand_params,param,weights,f_params,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,ql,ligtype,bonds,nlig,files,nconformations,metaheuristic,flexibility_params,flexibility_conformations,2);
                else
			mejorar_vs_flex (proteina,ligand_params,param,weights,f_params,conformations_x,conformations_y,conformations_z,move_x,move_y,move_z,quat_x,quat_y,quat_z,quat_w,energy,nconfs,ql,ligtype,bonds,nlig,files,nconformations,metaheuristic,flexibility_params,flexibility_conformations,2);
        }

	free(n_update);
	free(internal_energy);
	free(nlinks);

}
extern void mutation_calculation_cpp_vs_flex (struct param_t param, struct metaheuristic_t metaheuristic, struct flexibility_params_t *flexibility_params, unsigned int *n_update, int seed, int automatic_seed, type_data max_desp, type_data rotation, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, unsigned int nlig, unsigned int files, unsigned int nconformations)
{
 	unsigned int th1;
        type_data a[3],b[3],eje[3],p1[3];
        type_data ang;
        unsigned int num_point, frag_act, position_link;
        unsigned int rot_point, link;
        unsigned int shift_fragment, position_conformation;
	unsigned int k = nconformations / files;
        type_data qeje_x, qeje_y, qeje_z, qeje_w;
        type_data angle,t_p;
        type_data local_x, quat_tmp_x, local_y, quat_tmp_y, local_z, quat_tmp_z, local_w, quat_tmp_w ;

        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 0:
                case 3:
                case 2:
                        th1 = metaheuristic.Threads1Ini;
                        break;
        }
        omp_set_num_threads (th1);
	#pragma omp parallel for private(position_conformation,position_link,ang,link,rot_point,a,b,p1,num_point,shift_fragment,frag_act,qeje_x,qeje_y,qeje_z,qeje_w,quat_tmp_x,quat_tmp_y,quat_tmp_z,quat_tmp_w,eje,angle,local_x,local_y,local_z,local_w) 
        for (unsigned int id_conformation=0; id_conformation < nconformations; id_conformation++)
        {
                switch (n_update[id_conformation])
                {
                	case 0:
                        	//MOVE X
                        	move_x[id_conformation] += getRealRandomNumber_r(max_desp,automatic_seed,seed,id_conformation+seed); //Desplazamiento entre -max_desp y max_desp
                        	break;
                 	case 1:
                        	//MOVE Y
                                move_y[id_conformation] += getRealRandomNumber_r(max_desp,automatic_seed,seed,id_conformation+seed); //Desplazamiento entre -max_desp y max_desp
                                break;
			case 2:
                                //MOVE Z
                                move_z[id_conformation] += getRealRandomNumber_r(max_desp,automatic_seed,seed,id_conformation+seed); //Desplazamiento entre -max_desp y max_desp
                                break;
                        case 3:
                                //ROTATION
				quat_tmp_x = quat_x[id_conformation];
                		quat_tmp_y = quat_y[id_conformation];
                		quat_tmp_z = quat_z[id_conformation];
                		quat_tmp_w = quat_w[id_conformation];
                		angle = getRealRandomNumber(param.rotation);
                		eje[0] = getRealRandomNumber (1);
                		eje[1] = getRealRandomNumber (1);
                		eje[2] = getRealRandomNumber (1);
                		setRotation_cpp (&local_x, &local_y, &local_z, &local_w, angle, eje);
                		composeRotation_cpp (&local_x, &local_y, &local_z, &local_w, &quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w, &quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w);
                		normalize_cpp(&quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w );
                		quat_x[id_conformation] = quat_tmp_x;
                		quat_y[id_conformation] = quat_tmp_y;
                		quat_z[id_conformation] = quat_tmp_z;
                		quat_w[id_conformation] = quat_tmp_w;
				break;
			case 4:
				//FLEXIBILITY
                                 //OBTENEMOS EL ANGULO EN RADIANES ALEATORIO ENTRE UN MAXIMO.
				unsigned int file = id_conformation / k;
                                type_data t_p =  (getRealRandomNumber(rotation) * PI) / 180 ;
                                //ENLACE Y FRAGMENTO A GIRAR ALEATORIO
                                unsigned int link =  getIntNumber(10000) % flexibility_params[file].n_links;
                                //OBTENEMOS EL PUNTO DEL ENLACE DESDE DONDE ROTAR
                                unsigned int rot_point = getIntNumber(10000) % 2;

                                //NOS SITUAMOS EN EL VECTOR DE CONFORMACIONES Y DE ENLACES
		            	position_conformation = id_conformation * nlig;
                		//int k = id_conformation / files;
                		position_link = link;
                		//printf("principio id_conf %d, pos %d ang %f link %d rot %d pos %d\n",id_conformation,position_conformation,ang,link,rot_point,position_conformation);
				//printf("id_conf %d links %d link %d\n",id_conformation,flexibility_params[file].n_links,position_link);
                		//printf("id_conf %d link %d flexibility[%d].links[2*%d]=%d **** flexibility[%d].links[(2*%d)+1]=%d\n",id_conformation,position_link,k,position_link,flexibility_params[k].links[2*position_link],k,position_link,flexibility_params[k].links[(2*position_link)+1]);

                		//EJE DE GIRO
                		a[0] = conformations_x[position_conformation + (flexibility_params[file].links[2 * position_link] - 1)];
                		a[1] = conformations_y[position_conformation + (flexibility_params[file].links[2 * position_link] - 1)];
                		a[2] = conformations_z[position_conformation + (flexibility_params[file].links[2 * position_link] - 1)];
                		b[0] = conformations_x[position_conformation + (flexibility_params[file].links[(2 * position_link)+1] - 1)];
                		b[1] = conformations_y[position_conformation + (flexibility_params[file].links[(2 * position_link)+1] - 1)];
                		b[2] = conformations_z[position_conformation + (flexibility_params[file].links[(2 * position_link)+1] - 1)];
				//printf(" %f %f %f b %f %f %f\n",a[0],a[1],a[2],b[0],b[1],b[2]);
					
                		if (rot_point == 0){
                        		eje[0] = b[0] - a[0];
                        		eje[1] = b[1] - a[1];
                        		eje[2] = b[2] - a[2];
                        		p1[0] = conformations_x[position_conformation + (flexibility_params[file].links[2 * position_link] - 1)];
                        		p1[1] = conformations_y[position_conformation + (flexibility_params[file].links[2 * position_link] - 1)];
                        		p1[2] = conformations_z[position_conformation + (flexibility_params[file].links[2 * position_link] - 1)];
                        		num_point = (2 * position_link) + 1;
                		}else {
                        		eje[0] = a[0] - b[0];
                        		eje[1] = a[1] - b[1];
                        		eje[2] = a[2] - b[2];
                        		p1[0] = conformations_x[position_conformation + (flexibility_params[file].links[(2 * position_link)+1] - 1)];
                        		p1[1] = conformations_y[position_conformation + (flexibility_params[file].links[(2 * position_link)+1] - 1)];
                        		p1[2] = conformations_z[position_conformation + (flexibility_params[file].links[(2 * position_link)+1] - 1)];
                        		num_point = 2 * position_link;
                		}
					
                		//printf("id_conf %d, pos %d ang %f link %d rot %d k %d qxyzw %f %f %f %f ejexyz %f %f %f \n",id_conformation,position_conformation,ang,link,rot_point,k,qeje_x,qeje_y,qeje_z,qeje_w,eje[0],eje[1],eje[2]);
                		//CONFIGURACION EJE DE ROTACION COMO QUATERNION
                		setRotation_flexibility_cpp_vs (&qeje_x,&qeje_y,&qeje_z,&qeje_w,ang,eje);
                		//printf("FIN id_conf %d\n",id_conformation);
		                //int j2 = 0;
                		shift_fragment = 0;
                		//ROTAMOS TODOS LOS FRAGMENTOS QUE TENGAN COMO ORIGEN EL PUNTO DE ROTACION
                		//while ((frag_act = links_fragments_d[(num_point * n_fragments) + j2]) != 0)
                		frag_act = flexibility_params[file].links_fragments[num_point];
		               
		                for (unsigned int i2 = 0; i2 < (frag_act - 1); i2++)
                 		       shift_fragment = shift_fragment + flexibility_params[file].fragments_tam[i2];
		                rotateLigandFragment_cpp_vs((flexibility_params[file].fragments + shift_fragment),flexibility_params[file].fragments_tam[frag_act-1],(conformations_x + position_conformation), (conformations_y + position_conformation), (conformations_z + position_conformation), qeje_x, qeje_y, qeje_z, qeje_w, p1);
				break;
		}	
	}

}

extern void mutation_random_cpp_vs_flex (struct metaheuristic_t metaheuristic, unsigned int nconformations, struct param_t param, unsigned int *n_update)
{
        unsigned int th1;

        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 0:
                case 3:
                case 2:
                        th1 = metaheuristic.Threads1Ini;
                        break;
        }
        omp_set_num_threads (th1);
        #pragma omp parallel 
        for (unsigned int id_conformation=0; id_conformation < nconformations; id_conformation++)
        {
                n_update[id_conformation] =  getIntNumber(10000) % 5;
        }
}

extern void mutation_multigpu_vs_flex (struct receptor_t proteina, struct flexibility_params_t *flexibility_params, struct ligand_params_t *ligand_params, unsigned int files, unsigned int nlig, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, type_data *energy, unsigned int *nconfs, type_data *weights, struct force_field_param_t *f_params, type_data *ql, char *ligtype, unsigned int *bonds, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int fase)
{

	unsigned int  pc_temp, stride_temp,ttotal = 0, dev;
        //pc_temp = nconformations / surface;
        devices->stride[0] = 0;
        //stride_temp = (int)((devices->trabajo[0] * 0.01)*surface);
        //devices->conformaciones[0] = stride_temp * pc_temp;
        //ttotal = stride_temp;

        for (unsigned int i=0;i < devices->n_devices;i++)
        {
                stride_temp = floor((devices->trabajo[i] * 0.01)*nconformations);
                devices->conformaciones[i] = stride_temp;
                ttotal += stride_temp;
        }
        unsigned int diferencia = nconformations - ttotal;
        for (unsigned int i=0;i<diferencia;i++)
        {
                dev = i % devices->n_devices;
                devices->conformaciones[dev] ++;
        }
        for (unsigned int i=(devices->n_devices-1);i > 0;i--)
        {
                devices->stride[i] = nconformations - devices->conformaciones[i];
                nconformations = nconformations - devices->conformaciones[i];
        }

	
        omp_set_num_threads(devices->n_devices);

        #pragma omp parallel for
        for (unsigned int j=0;j<devices->n_devices;j++)
        {
                //unsigned int th = omp_get_thread_num();
                unsigned int stride_d = devices->stride[j];
		
		mutation_gpu_vs_flex (proteina,param,ligand_params,flexibility_params,metaheuristic,devices,weights,f_params,conformations_x+(nlig*stride_d),conformations_y+(nlig*stride_d),conformations_z+(nlig*stride_d),move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy+stride_d,nconfs+stride_d,ql+(nlig*stride_d),ligtype+(nlig*stride_d),bonds+(nlig*stride_d),nlig,files,devices->conformaciones[j],j,stride_d);
		
	}

}
