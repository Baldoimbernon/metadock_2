#include "energy_common.h"
#include <thrust/sequence.h>
#include <thrust/functional.h>
#include <omp.h>

extern void seleccionar_vs_warm_up (struct vectors_t *vectors_e, struct param_t param, unsigned int hilos)
{
	struct vectors_t vectors_s;
	type_data tmp_1 = param.por_conf_warm_up_sel * param.conf_warm_up_cpu * 0.01;
	unsigned int tmp = (unsigned int)tmp_1;
	//printf("%d\n",tmp);
	//exit(0);	
	vectors_s.files = vectors_e->files;
	vectors_s.files = vectors_e->nlig;
	vectors_s.nconformations = vectors_s.files * tmp;
	vectors_s.conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_s.nlig*vectors_s.nconformations);
        vectors_s.conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_s.nlig*vectors_s.nconformations);
        vectors_s.conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_s.nlig*vectors_s.nconformations);

	vectors_s.ql = (type_data *)malloc(sizeof(type_data)*vectors_s.nlig*vectors_s.nconformations);
        vectors_s.ligtype = (char *)malloc(sizeof(char)*vectors_s.nlig*vectors_s.nconformations);
        vectors_s.subtype = (char *)malloc(sizeof(char)*vectors_s.nlig*SUBTYPEMAXLEN*vectors_s.nconformations);
        vectors_s.bonds = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s.nlig*MAXBOND*vectors_s.nconformations);
        vectors_s.nbonds = (char *)malloc(sizeof(char)*vectors_s.nlig*vectors_s.nconformations);

	vectors_s.move_x = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.move_y = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.move_z = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s.nconformations);
	vectors_s.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	omp_set_num_threads (hilos);
	#pragma omp parallel for 	
	for (unsigned int i = 0;i < vectors_s.files;i++)
	{	
		memcpy(vectors_s.conformations_x + i*vectors_s.nlig*tmp, vectors_e->conformations_x + i*vectors_e->nlig*param.conf_warm_up_cpu, tmp * vectors_s.nlig * sizeof(type_data));
                memcpy(vectors_s.conformations_y + i*vectors_s.nlig*tmp, vectors_e->conformations_y + i*vectors_e->nlig*param.conf_warm_up_cpu, tmp * vectors_s.nlig * sizeof(type_data));
                memcpy(vectors_s.conformations_z + i*vectors_s.nlig*tmp, vectors_e->conformations_z + i*vectors_e->nlig*param.conf_warm_up_cpu, tmp * vectors_s.nlig * sizeof(type_data));

		memcpy(vectors_s.ql + i*vectors_s.nlig*tmp, vectors_e->ql + i*vectors_e->nlig*param.conf_warm_up_cpu, tmp * vectors_s.nlig * sizeof(type_data));
                memcpy(vectors_s.ligtype + i*vectors_s.nlig*tmp, vectors_e->ligtype + i*vectors_e->nlig*param.conf_warm_up_cpu, tmp * vectors_s.nlig * sizeof(char));
                memcpy(vectors_s.subtype + i*vectors_s.nlig*SUBTYPEMAXLEN*tmp, vectors_e->subtype + i*vectors_e->nlig*SUBTYPEMAXLEN*param.conf_warm_up_cpu, tmp  * vectors_s.nlig * SUBTYPEMAXLEN * sizeof(char));
                memcpy(vectors_s.bonds + i*vectors_s.nlig*MAXBOND*tmp, vectors_e->bonds + i*vectors_e->nlig*MAXBOND*param.conf_warm_up_cpu, tmp * vectors_s.nlig * MAXBOND * sizeof(unsigned int));
                memcpy(vectors_s.nbonds + i*vectors_s.nlig*tmp, vectors_e->nbonds + i*vectors_e->nlig*param.conf_warm_up_cpu, tmp * vectors_s.nlig * sizeof(char));

		memcpy(vectors_s.move_x + i*tmp, vectors_e->move_x + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));  	
		memcpy(vectors_s.move_y + i*tmp, vectors_e->move_y + i*param.conf_warm_up_cpu, tmp * sizeof(type_data)); 
		memcpy(vectors_s.move_z + i*tmp, vectors_e->move_z + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.quat_x + i*tmp, vectors_e->quat_x + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.quat_y + i*tmp, vectors_e->quat_y + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.quat_z + i*tmp, vectors_e->quat_z + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.quat_w + i*tmp, vectors_e->quat_w + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.energy.energy + i*tmp, vectors_e->energy.energy + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));

		memcpy(vectors_s.conformations_x + i*vectors_s.nlig*tmp, vectors_e->conformations_x + i*vectors_e->nlig*param.conf_warm_up_cpu, tmp * vectors_s.nlig * sizeof(type_data));
                memcpy(vectors_s.conformations_y + i*vectors_s.nlig*tmp, vectors_e->conformations_y + i*vectors_e->nlig*param.conf_warm_up_cpu, tmp * vectors_s.nlig * sizeof(type_data));
                memcpy(vectors_s.conformations_z + i*vectors_s.nlig*tmp, vectors_e->conformations_z + i*vectors_e->nlig*param.conf_warm_up_cpu, tmp * vectors_s.nlig * sizeof(type_data));

		memcpy(vectors_s.ql + i*vectors_s.nlig*tmp, vectors_e->ql + i*vectors_e->nlig*param.conf_warm_up_cpu, tmp * vectors_s.nlig * sizeof(type_data));
                memcpy(vectors_s.ligtype + i*vectors_s.nlig*tmp, vectors_e->ligtype + i*vectors_e->nlig*param.conf_warm_up_cpu, tmp * vectors_s.nlig * sizeof(char));
                memcpy(vectors_s.subtype + i*vectors_s.nlig*SUBTYPEMAXLEN*tmp, vectors_e->subtype + i*vectors_e->nlig*SUBTYPEMAXLEN*param.conf_warm_up_cpu, tmp  * vectors_s.nlig * SUBTYPEMAXLEN * sizeof(char));
                memcpy(vectors_s.bonds + i*vectors_s.nlig*MAXBOND*tmp, vectors_e->bonds + i*vectors_e->nlig*MAXBOND*param.conf_warm_up_cpu, tmp * vectors_s.nlig * MAXBOND * sizeof(unsigned int));
                memcpy(vectors_s.nbonds + i*vectors_s.nlig*tmp, vectors_e->nbonds + i*vectors_e->nlig*param.conf_warm_up_cpu, tmp * vectors_s.nlig * sizeof(char));

		memcpy(vectors_s.move_x + i*tmp, vectors_e->move_x + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));  	
		memcpy(vectors_s.move_y + i*tmp, vectors_e->move_y + i*param.conf_warm_up_cpu, tmp * sizeof(type_data)); 
		memcpy(vectors_s.move_z + i*tmp, vectors_e->move_z + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.quat_x + i*tmp, vectors_e->quat_x + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.quat_y + i*tmp, vectors_e->quat_y + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.quat_z + i*tmp, vectors_e->quat_z + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.quat_w + i*tmp, vectors_e->quat_w + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.energy.energy + i*tmp, vectors_e->energy.energy + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));						
	}
	thrust::sequence(vectors_s.energy.n_conformation,vectors_s.energy.n_conformation + vectors_s.nconformations);
	
	free(vectors_s.conformations_x);
	free(vectors_s.conformations_y);
	free(vectors_s.conformations_z);
	free(vectors_s.ql);
	free(vectors_s.ligtype);
	free(vectors_s.subtype);
	free(vectors_s.bonds);
	free(vectors_s.nbonds);
	free(vectors_s.move_x);
	free(vectors_s.move_y);
	free(vectors_s.move_z);
	free(vectors_s.quat_x);
	free(vectors_s.quat_y);
	free(vectors_s.quat_z);
	free(vectors_s.quat_w);	
	free(vectors_s.energy.energy);
	free(vectors_s.energy.n_conformation);
	
	
}	
