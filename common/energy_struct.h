#ifndef _STRUCT_H_
#define _STRUCT_H_
#include "definitions.h"
#include "vector_types.h"
#include "energy_cuda.h"


//Agrupa la informacion del ligando
typedef char mystring[50];

typedef struct ligand_t {

	unsigned int nlig;		//Numero de atomos
	unsigned int atoms;
	type_data * lig_x;		//Posicion x
	type_data * lig_y;		//Posicion y
	type_data * lig_z;		//Posicion z
	type_data * ql;		//Carga
	char * ligtype;		//Tipo: número asignado al tipo (carbono=0, hidrogeno=1,...)
	char * subtype;		//Cadena del tipo obtenida del fichero mol2
	char **type_json;
    unsigned int *bonds;
    char *nbonds;
}ligand;

typedef struct receptor_t {

	unsigned int nrec;
	unsigned int atoms;
	type_data * rec_x; // Posicion x
	type_data * rec_y; // Posicion y
	type_data * rec_z; // Posicion z
	type_data * qr;
	char * rectype;
    unsigned int *bonds;
    char * nbonds;
}receptor;

typedef struct Protein_entry_t{
	unsigned int atomID;
	mystring symbol;
	mystring symbol2;
	mystring sgName;
	mystring remark;
	type_data pos[3];
	type_data charge;
	type_data eps;
	type_data sig;
	unsigned int vdwType;
	unsigned int nBond;
	unsigned int bond[MAXBOND];
	unsigned int sgID;
	unsigned int externalSg;
} Protein_entry;

typedef struct Bond_tp_t{
	unsigned int a;
	unsigned int b;
	mystring type;
	unsigned int dum1;
	unsigned int dum2;
	unsigned int dum3;
} Bond_tp;

typedef struct param_t {
	char ligand[FILENAME];
	char input_dir_l[FILENAME];
	char input_dir_p[FILENAME];
	char input_ligs[FILENAME];
	char input_params[FILENAME];
	char output_dir[FILENAME];
	char protein[FILENAME];
	char openmp[FILENAME];
	char gpu[FILENAME];
	char multigpu[FILENAME];
	char forcefield[FILENAME];
	char pdbqt[FILENAME];
	int auto_flex;
	int mode;
	int ngpu;
	int tipo_computo;
	int tipo_dispositivo_computar;
	int scoring_function_type;
	int optimizacion;
	int conf_warm_up_gpu;
	int conf_warm_up_cpu;
	int por_conf_warm_up_sel;
	int por_conf_warm_up_inc;
	int limit;
	int steps_warm_up_gpu;	
	type_data max_desp;
	int steps;
	int rotation;
	int rotation_mu;
	int alarma;
	int duracion_alarma;
	int reparto_carga;
	int seed;
	int automatic_seed;
	int montecarlo;
	int max_ligand_selection;
	char ligand_energy[FILENAME];
	int level_bonds_VDW;
	int level_bonds_ES;
	int flex_angle;
	int work_type;
	int auto_max_threads;
	int rot_type;
} param;

typedef struct metaheuristic_t {	
	unsigned int NEIIni;
	unsigned int NEIIni_selec;
	unsigned int DQNIni;
	unsigned int NEFIni;
	int NEFMIni;
	int NEFPIni;
	unsigned int NEFMIni_selec;
	unsigned int NEFPIni_selec;
	int NEMSel;
	int NEPSel;
	unsigned int NEMSel_selec;
	unsigned int NEPSel_selec;
	int NMMCom;
	int NMPCom;
	int NPPCom;
	unsigned int NMMCom_selec;
	unsigned int NMPCom_selec;
	unsigned int NPPCom_selec;
	unsigned int NCOMtotal;
	int PEMIni;
	int IMEIni;
	int NEEImp;	
	int PEMUCom;
	unsigned int PEMUCom_selec;
	int IMEMUCom;
	int PEMSel;
	unsigned int PEMSel_selec;
	int IMEImp;
	int NEMInc;
	unsigned int NEPInc;
	unsigned int NEMInc_selec;
	unsigned int NEPInc_selec;
	int NIRFin;
	int NMIFin;
	int IMEFlex;
	int Threads1Ini;
	int Threads1Fit;
	int Threads2Fit;
	int Threads1Sel;
	int Threads1Com;
	int Threads2Com;
	int Threads1Imp;	
	int Threads1Inc;
	int ThreadsperblockFit;
	int ThreadsperblockMoveIni;
	int ThreadsperblockRotIni;
	int ThreadsperblockQuatIni;
	int ThreadsperblockRandomIni;
	int ThreadsperblockIncImp;
	int ThreadsperblockMoveImp;	
	int num_kernels;
 
} metaheuristic;

typedef struct energy_t {
	unsigned int *n_conformation;
	type_data *energy;
}energy;

typedef struct force_field_param_t {
        type_data epsilon;
        type_data sigma;
        type_data vol;
        type_data asp;
        type_data epsilon_h;
        type_data sigma_h;
} force_field_param;

typedef struct vectors_t {
	unsigned int num_surface;
	unsigned int nconformations;
	unsigned int nlig;
	unsigned int files;
	type_data ac_x;
	type_data ac_y;
	type_data ac_z;
	type_data *ql;
	unsigned int *bonds;
	char *subtype;
	char *ligtype;
	char *nbonds;
	struct force_field_param_t f_params[MAXTYPES];
	type_data *weights;
	type_data *conformations_x;
	type_data *conformations_y;
	type_data *conformations_z;
	type_data *surface_x;
	type_data *surface_y;
	type_data *surface_z;
	type_data *move_x;
	type_data *move_y;
	type_data *move_z;
	type_data *quat_x;
	type_data *quat_y;
	type_data *quat_z;
	type_data *quat_w;
	struct energy_t energy;
}vectors;

typedef struct device_t {
	unsigned int n_devices;
	unsigned int *id;
	cudaDeviceProp *propiedades;
	double *tiempo;
	unsigned int *trabajo;
	unsigned int *hilos_shared;
	unsigned int *hilos_sinshared;
	unsigned int *hilos;
	unsigned int *stride;
	unsigned int *conformaciones;
} devices_t;

typedef struct flexibility_data_t {
	type_data ang;
	unsigned int link;
	unsigned int p_rot;
}flexibility_data_t;

typedef struct flexibility_params_t {
	unsigned int n_links;
	unsigned int n_fragments;
	int *links;
	int *fragments;
	int *links_fragments;
	int *fragments_tam;
	bool *individual_bonds_ES;
	bool *individual_bonds_VDW;
}flexibility_param_t;

typedef struct ligand_params_t {
	char file[FILENAME];
	unsigned int atoms;
	char **type_json;	
}ligand_param_t;

#endif
