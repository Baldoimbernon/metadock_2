#ifndef _LIGAND_H_
#define _LIGAND_H_

#include "energy_common.h"
#include "energy_struct.h"

extern void ligand_babel (struct param_t param, char *filename,struct flexibility_params_t *flexibility_params, unsigned int level_bonds_VDW, unsigned int level_bonds_ES);
extern unsigned int rotable_ligand (char *filename);
extern void convert_into_mol2 (char *filename,char *filename_1);
extern void divide_ligands (char *input_dir,char *output_dir);
extern void lig2mol2 (unsigned int nAtom, char *ligand_original, char *ligand_generate, char *ligand_output, unsigned int zone);
extern void writeLigand (char *filename, struct ligand_t *ligando);
extern type_data writeLigandEnergies (unsigned int flex, unsigned int shift, char *filename, struct receptor_t proteina, struct ligand_t ligando, struct vectors_t *vectors_ini, type_data *q_x, type_data *q_y, type_data *q_z, type_data *q_w, type_data *move_x, type_data *move_y, type_data *move_z, type_data *weights, struct force_field_param_t *f_params, struct param_t param, char *cadena_json,char **type_json, unsigned int ball);
extern type_data writeLigandEnergies_type1 (unsigned int tor, unsigned int flex, unsigned int shift, char *filename, struct receptor_t proteina, struct ligand_t ligando, struct vectors_t *vectors_ini, type_data *q_x, type_data *q_y, type_data *q_z, type_data *q_w, type_data* move_x, type_data* move_y, type_data* move_z, type_data *weights, struct force_field_param_t *f_params, struct param_t param, char *cadena_json, char **type_json, unsigned int ball);
extern type_data writeLigandEnergies_type2 (unsigned int tor, unsigned int flex, unsigned int shift, char *filename, struct receptor_t proteina, struct ligand_t ligando, struct vectors_t *vectors_ini, type_data *q_x, type_data *q_y, type_data *q_z, type_data *q_w, type_data* move_x, type_data* move_y, type_data* move_z, type_data *weights, struct force_field_param_t *f_params, struct param_t param, char *cadena_json, char **type_json, unsigned int ball);

extern void create_json(struct param_t param, char *nombre_ligando_final, char *cadena_json, char *temp_json, type_data s_x, type_data s_y, type_data s_z, char *protein , char *ligand, int scoring_function_type, type_data total_energies);

extern void completarLigando_vs(struct ligand_t &ligando, struct vectors_t *vectors_ini, unsigned int act_c, unsigned int tam);
//extern void generarLigando (unsigned int auto_flex, unsigned int shift, struct ligand_t ligando, struct ligand_t *ligout, struct vectors_t *vectors_ini, float4 *q, type_data* move_x, type_data* move_y, type_data* move_z);
extern type_data rmsd (type_data * src1, type_data * src2, unsigned int n);

extern void configurar_ligandos_finales(struct receptor_t proteina, struct ligand_t ligando, struct ligand_t *ligtemp, struct vectors_t *vectors_ini,struct param_t param,struct metaheuristic_t metaheuristic, struct flexibility_params_t *flexibility_params);
extern void configurar_ligandos_finales_vs (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct vectors_t *vectors_ini,struct param_t param,struct metaheuristic_t metaheuristic, struct flexibility_params_t *flexibility_params);

extern void nombre_fichero_completo (char *cadena_in, char *cadena_out);
extern void ruta (char *cadena_in, char *cadena_ruta);
extern void nombre_fichero (char *cadena_in, char *cadena_out);
extern void nombre_dir(char *cadena_in, char *cadena_out);
#endif
