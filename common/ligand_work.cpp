#include "energy_common.h"
#include "energy_CpuSolver.h"
#include "energy_CpuSolver_common.h"
#include "rotation.h"
#include "ligand.h"
#include <thrust/sort.h>

using namespace std;

void extract_name_protein (char *input,char *output)
{
        int c=0,i=0;
        int len = strlen(input);
	c = len;
        while((c > 0) && (input[c] != '/')) c--;
	c++;
	while ((c < len) && (input[c] != '.'))
        {
                output[i] = input[c];
                i++;c++;
        }
        output[i]='\0';
}

extern void create_json(struct param_t param, char *nombre_ligando_final, char *cadena_json, char *temp_json, type_data s_x, type_data s_y, type_data s_z, char *protein , char *ligand, int scoring_function_type, type_data total_energies)
{
	char cadena[500];
	char base[20];
	char ejecucion[500];
	//char name_protein[500];

	strcpy(base,"rm -rf ");
	sprintf(ejecucion,"%s%s",base,temp_json);
	//extract_name_protein(param.protein,name_protein);

	ofstream output_l(cadena_json, ios::out | ios::app);

        if (!output_l.good()) {
                cout << "can't open file " << output_l << endl;
                cout << "Will not create file json" << endl;
                exit (1);
        }

	output_l << "{" << endl;
	output_l << "\t\"chain_protein\": \"N/A\"," << endl;
	output_l << "\t\"coords\": [" << endl;
	output_l << "\t\t\"" << 10.0*s_x << "\"," << endl;
	output_l << "\t\t\"" << 10.0*s_y << "\"," << endl;
	output_l << "\t\t\"" << 10.0*s_z << "\"" << endl;
	output_l << "\t]," << endl;
	output_l << "\t\"file_ori_query\": \"" << param.ligand << "\"," << endl;
	output_l << "\t\"file_ori_target\": \"" << param.protein << "\"," << endl;
	output_l << "\t\"file_result\": \"" << nombre_ligando_final << "\"," << endl;
	output_l << "\t\"global_score\": \"" << total_energies << "\"," << endl;
	output_l << "\t\"graph_atoms_color\": [" << endl;
	
	output_l << "\t\t\"b\"," << endl;
        output_l << "\t\t\"g\"," << endl;
        output_l << "\t\t\"#E92424\"," << endl;
        output_l << "\t\t\"c\"," << endl;
        output_l << "\t\t\"m\"" << endl;
	output_l << "\t]," << endl;
	
	output_l << "\t\"graph_atoms_field\": [" << endl;	
	output_l << "\t\t\"Electrostatic\"," << endl;
        output_l << "\t\t\"Van der Waal\"," << endl;
        output_l << "\t\t\"Hydrogen_Bonds\"," << endl;
        output_l << "\t\t\"Desolvation\"," << endl;
        output_l << "\t\t\"Torsion\"" << endl;
	output_l << "\t]," << endl;

	output_l << "\t\"graph_atoms_score\": [" << endl;
	//exit(0);
	ifstream original_l(temp_json, ios::in);	
	if (!original_l.good()) {
                cout << "can't open file " << original_l << endl;
                cout << "Will not read temp_json file" << endl;
                exit (1);
        }

	original_l.getline(cadena,100);

        while (!original_l.eof())
        {
                output_l << cadena << endl;
                original_l.getline(cadena,100);
                //printf("cadena %s\n",cadena);
        }
        output_l << cadena << endl;

	system(ejecucion);
        original_l.close();
	output_l.close();

}

extern void completarLigando_vs(struct ligand_t &ligando, struct vectors_t *vectors_ini, unsigned int conf, unsigned int tam) {
	ligando.ligtype = (char*) malloc (ligando.nlig * sizeof(char));
	ligando.subtype = (char*) malloc (ligando.nlig * SUBTYPEMAXLEN * sizeof(char));
	ligando.ql = (type_data*) malloc (ligando.nlig * sizeof(type_data));
	ligando.bonds = (unsigned int*) malloc (ligando.nlig * MAXBOND * sizeof(unsigned int));

	memcpy(ligando.ligtype, &(vectors_ini->ligtype[conf*ligando.nlig]), ligando.atoms * sizeof(char));
	memcpy(ligando.subtype, &(vectors_ini->subtype[conf*SUBTYPEMAXLEN*ligando.nlig]), ligando.atoms * SUBTYPEMAXLEN * sizeof(char));
	memcpy(ligando.ql, &(vectors_ini->ql[conf*ligando.nlig]), ligando.atoms * sizeof(type_data));
	memcpy(ligando.bonds, &(vectors_ini->bonds[conf*MAXBOND*ligando.nlig]), ligando.atoms * MAXBOND * sizeof(unsigned int));
}

extern void generarLigando (unsigned int flex, unsigned int shift, struct ligand_t ligando, struct ligand_t *ligout, struct vectors_t *vectors_ini, type_data *q_x, type_data *q_y, type_data *q_z, type_data *q_w, type_data* move_x, type_data* move_y, type_data* move_z){

	type_data pos[3];
	for (unsigned int j=0; j < ligando.atoms; j++){
		switch (flex) {
			case 0:
				rotate3DPoint_cpp(q_x, q_y, q_z, q_w, (ligando.lig_x + j), (ligando.lig_y + j), (ligando.lig_z + j),pos);
				//printf("atom %d, q_x %f ,  pos %f %f %f, move %f %f %f\n",j,q_x[0],pos[0],pos[1],pos[2],move_x[0],move_y[0],move_z[0]);
				break;
				
			case 1:
				rotate3DPoint_cpp(q_x, q_y, q_z, q_w, (vectors_ini->conformations_x + shift + j), (vectors_ini->conformations_y + shift + j), (vectors_ini->conformations_z + shift + j),pos);
				 //printf("atom %d, shift %d q_x %f ,  pos %f %f %f, move %f %f %f\n",j,shift,q_x[0],pos[0],pos[1],pos[2],move_x[0],move_y[0],move_z[0]);
				break;
	}
		

		ligout->lig_x[j] = move_x[0] + pos[0];
		ligout->lig_y[j] = move_y[0] + pos[1];
		ligout->lig_z[j] = move_z[0] + pos[2];
	}
}

extern void generarLigando_vs (unsigned int flex, unsigned int shift, struct ligand_t ligando, struct vectors_t *vectors_ini, type_data *q_x, type_data *q_y, type_data *q_z, type_data *q_w, type_data* move_x, type_data* move_y, type_data* move_z){

	type_data pos[3];
	//int shift = conf*tam*ligando.nlig;
	//printf("Atomos ligando: %d\n",ligando.atoms);
	for (unsigned int j=0; j < ligando.atoms; j++){
		switch (flex) {
			case 0:
			case 1:
				rotate3DPoint_cpp(q_x, q_y, q_z, q_w, (vectors_ini->conformations_x + shift + j), (vectors_ini->conformations_y + shift + j), (vectors_ini->conformations_z + shift + j),pos);
				//printf("atom %d, q_x %f ,  pos %f %f %f, move %f %f %f\n",j,ligando.ql[j],q_x[0],pos[0],pos[1],pos[2],move_x[0],move_y[0],move_z[0]);
				break;
		}
		

		ligando.lig_x[j] = move_x[0] + pos[0];
		ligando.lig_y[j] = move_y[0] + pos[1];
		ligando.lig_z[j] = move_z[0] + pos[2];
	}
}

//Calcula la medida de distancia RMSD entre dos ligandos
//@param src1 posicion de los atomos del primer ligando
//@param src2 posicion de los atomo del segundo ligando
//@param n número de atomos
extern type_data rmsd (type_data * src1, type_data * src2, unsigned int n){

	type_data res = 0, aux;

	for (unsigned int i=0; i < n; i++){
		aux = src1[i] - src2[i];
		aux *= aux;
		res += aux;
	}
	res /= (type_data)n;

	return sqrt(res);
}


extern void lig2mol2 (unsigned int nAtom,char *ligand_original, char *ligand_generate, char *ligand_output, unsigned int zone)
{
	char cadena[100];
	char aux[10],aux1[10],type[10], aux_generate[10], bond1[10], bond2[10], bond3[10], bond4[10];
	unsigned int natom, subtype, dum1, dum2, dum3;
	unsigned int nBond, number;
	type_data aux_coord, coord_x, coord_y, coord_z, value, aux_generate_t;

	ofstream output_l(ligand_output, ios::out | ios::app);
	ifstream original_l(ligand_original, ios::in);

	FILE *generate_l;
	//if ((original_l =  fopen(ligand_original,"r")) == NULL) {
	//	printf("ReadInput(): Can't open file \"%s\"\n", original_l);
	//	exit(1);
	//}
	if ((generate_l =  fopen(ligand_generate,"r")) == NULL) {
		printf("ReadInput(): Can't open file \"%s\"\n", generate_l);
		exit(1);
	}
	if (!original_l.good()) {
                cout << "can't open file " << original_l << endl;
                cout << "Will not read ligand file" << endl;
                exit (1);
        }
	if (!output_l.good()) {
		cout << "can't open file " << output_l << endl;
		cout << "Will not read ligand file" << endl;
		exit (1);
	}
	//exit(0);
	//int kk=0;
	original_l.getline(cadena,100);
	//printf("cadena %s\n",cadena);
	//exit(0);
	//int k=0;
	while (strstr(cadena,"<TRIPOS>ATOM") == NULL)
	{
		output_l << cadena << endl;
		original_l.getline(cadena,100);
		//printf("cadena %s\n",cadena);	
		//k++;
	}
	//exit(0);
	output_l << cadena << endl;
	//printf("nAtom %d\n",nAtom);
	//exit(0);
	for(unsigned int i=0; i < nAtom; i++) {
		//fscanf(original_l,"%d %s %f %f %f %s %d %s %f", &natom, type, &aux_coord, &aux_coord, &aux_coord, &aux, &number, &aux1, &value);
		original_l >> natom >> type >> aux_coord >> aux_coord >> aux_coord >> aux >> number >> aux1 >> value; 
		//original_l.ignore(50);
		fscanf(generate_l,"%s %s %f %f %f %s %s %s", &aux_generate, aux_generate, &coord_x, &coord_y, &coord_z, &aux_generate, &aux_generate, &aux_generate);
		output_l << natom << " " << type << " " << coord_x << " " << coord_y << " " << coord_z << " " << aux << " " << number << " " << aux1 << " " << value << " " << endl;
	}
	original_l.getline(cadena,100);
	
        while (!original_l.eof())
        {
                output_l << cadena << endl;
                original_l.getline(cadena,100);
		//printf("cadena %s\n",cadena);
        }
        output_l << cadena << endl;
	

	original_l.close();
	fclose(generate_l);
	output_l.close();
}

extern void writeLigand (char *filename, struct ligand_t *ligando, int scoring_type){

	char cadena[4];
	ofstream esfile(filename, ios::out | ios::trunc);

	if (!esfile.good()) {
		cout << "can't open file " << filename << endl;
		cout << "Will not write ligand file" << endl;
		return;
	}

	for (unsigned int i=0; i < ligando->atoms; i++){		

		esfile << i+1 << " " << getTypeString(ligando->ligtype[i],cadena) << " ";
		if (scoring_type != S_AD4)
			esfile << ligando->lig_x[i]*10.0 << " " << ligando->lig_y[i]*10.0 << " " << ligando->lig_z[i]*10.0 << " ";
		else
			esfile << ligando->lig_x[i] << " " << ligando->lig_y[i] << " " << ligando->lig_z[i] << " ";
		esfile << &(ligando->subtype[SUBTYPEMAXLEN*i]) << " **** " << ligando->ql[i] << endl;

	}
	
	esfile.close();

}

extern type_data writeLigandEnergies_type2 (unsigned int tor, unsigned int flex, unsigned int shift, char *filename, struct receptor_t proteina, struct ligand_t ligando, struct vectors_t *vectors_ini, type_data *q_x, type_data *q_y, type_data *q_z, type_data *q_w, type_data* move_x, type_data* move_y, type_data* move_z, type_data *weights, struct force_field_param_t *f_params, struct param_t param, char *temp_json, char **type_json, unsigned int ball)
{
	type_data term6, temp_sqrt=0,temp_vdw=0, temp_es = 0, temp_hbond = 0, temp_solv = 0, eps, sig, miatomo[3], v[3], w[3], e_es, e_vdw, e_hbond, e_solv, pre_calc, lig_map,tmp_e_es=0,tmp_e_vdw=0,tmp_e_hbond=0,tmp_e_solv=0, term12,es_atom,vdw_atom,hbond_atom,solv_atom, pos[3], aux1, aux2, aux3, rij, rij2;
        unsigned int j,i,k,id_conformation;
        unsigned int th1;
        unsigned int total;
        char ind1, ind2;
	char cadena[4];
	char name_protein[500];
	char totaltemp[500];
        type_data qaux_x,qaux_y,qaux_z,qaux_w;
        double tmpconst,tmpconst_h,cA,cB,cC,cD,eps_hb,sig_hb,rA,rB,dxA,dxB,rC,rD,dxC,dxD;
        type_data solv_asp_1, solv_asp_2, solv_vol_1, solv_vol_2,solv_qasp_1,solv_qasp_2;
        double minus_inv_two_sigma_sqd = -0.5L / (sigma_autodock * sigma_autodock);
        type_data exponencial;

        type_data difx,dify,difz;
        type_data  mod2x, mod2y, mod2z;

	extract_name_protein(param.protein,name_protein);
        ofstream output_l(temp_json, ios::out | ios::app);

        if (!output_l.good()) {
                cout << "can't open file " << output_l << endl;
                cout << "Will not create file temp_json" << endl;
                exit (1);
        }

	/*ofstream esfile(filename, ios::out | ios::trunc);

        if (!esfile.good()) {
                cout << "can't open file " << filename << endl;
                cout << "Will not write ligand energies file" << endl;
        	exit (1);
	}*/

        for (unsigned int j=0; j < ligando.atoms; j++){

                //rotate3DPoint_cpp(q_x, q_y, q_z, q_w, &(ligando.lig_x[j]), &(ligando.lig_y[j]), &(ligando.lig_z[j]),pos);
                //printf("atom %d, pos %f %f %f, move %f %f %f\n",j,pos[0],pos[1],pos[2],move_x[0],move_y[0],move_z[0]);

                pos[0] = ligando.lig_x[j];
                pos[1] = ligando.lig_y[j];
                pos[2] = ligando.lig_z[j];
                //printf("pos0 %f , pos1 %f , pos2 %f ql_j %f \n",pos[0],pos[1],pos[2],ligando.ql[j]);

                ind1 = ligando.ligtype[j];
                solv_asp_1 = f_params[ind1].asp;
        	solv_vol_1 = f_params[ind1].vol;
                //nenlaces= 0;

                for (unsigned int i=0; i < proteina.nrec;i++)
                {
                        e_vdw = 0;
                        e_hbond = 0;
                        e_es = 0;
                        e_solv = 0;

                        aux1=proteina.rec_x[i]-pos[0];
                        aux2=proteina.rec_y[i]-pos[1];
                        aux3=proteina.rec_z[i]-pos[2];
                        aux1 *= aux1;
                        aux2 *= aux2;
                        aux3 *= aux3;
                        rij2=aux1+aux2+aux3; //rij al cuadrado
                        rij = sqrtf(rij2);
	
			ind2 = proteina.rectype[i];
                        solv_asp_2 = f_params[ind2].asp;
            		solv_vol_2 = f_params[ind2].vol;
                        exponencial = exp(minus_inv_two_sigma_sqd * rij2) * weights[3];

			double dato_eij = calc_ddd_Mehler_Solmajer(rij,APPROX_ZERO);
                        //dato_eij = -0.01465;
                        //printf("e(rij) = %f\n",dato_eij);
                        eps = sqrt(f_params[ind1].epsilon * f_params[ind2].epsilon);
                        sig = ((f_params[ind1].sigma + f_params[ind2].sigma))*0.5L;
                        eps_hb =  (f_params[ind1].epsilon_h +  f_params[ind2].epsilon_h);//  * a_scoring.p_hbond;
                        sig_hb = ((f_params[ind1].sigma_h + f_params[ind2].sigma_h));//*0.5L;
                        //dist = clamp(dist, (RMIN_ELEC*RMIN_ELEC));

                        tmpconst = eps / (double)(xA - xB);
                        tmpconst_h = eps_hb / (double)(xC - xD);

                        cA =  tmpconst * pow( (double)sig, (double)xA ) * xB;
                        cB =  tmpconst * pow( (double)sig, (double)xB ) * xA;

                        cC =  tmpconst_h * pow( (double)sig_hb, (double)xC ) * xD;
                        cD =  tmpconst_h * pow( (double)sig_hb, (double)xD ) * xC;

                        dxA = (double) xA;
                        dxB = (double) xB;
                        dxC = (double) xC;
                        dxD = (double) xD;

                        rA = pow( rij, dxA);
                        rB = pow( rij, dxB);
                        rC = pow( rij, dxC);
                        rD = pow( rij, dxD);

                        term6 = cB / rB;//pow((double)dist, (double)xB);
                        term12 = cA / rA;//pow((double)dist, (double)xA);

                        e_es = (1.0 / (dato_eij * rij)) * proteina.qr[i] * ELECSCALE * weights[2];
                        //if (dist < 7.0)
                        e_vdw = term12 - term6;

			if (rij < 8)
                        {
                        	pre_calc = ((solv_asp_1 * solv_vol_2)  + ((solv_asp_2 + qsolpar * fabs(proteina.qr[i])) * solv_vol_1)) * exponencial;// * a_scoring.p_desolv;
                                lig_map = qsolpar * solv_vol_2 * exponencial;
                                e_solv = pre_calc + lig_map;
                        }

                        if ((rij > 0) && (rij < 8) && (ind1 == OXI || ind1 == NIT2 || ind1 == AZU)){
                        	if (ind2 == HBOND){
                                	//printf("Hola\n");
					sub_h(&pos[0], &pos[1], &pos[2],  &proteina.rec_x[i], &proteina.rec_y[i], &proteina.rec_z[i], v);
                                        sub_h(&(proteina.rec_x[proteina.bonds[MAXBOND * i]]), &(proteina.rec_y[proteina.bonds[MAXBOND * i]]),&(proteina.rec_z[proteina.bonds[MAXBOND * i]]),&proteina.rec_x[i], &proteina.rec_y[i], &proteina.rec_z[i], w);
                                        double cosT = cosTheta_h (v, w);

                                        if ( cosT < 0.0 ){
                                        	double sinT = sqrt(1.0-cosT*cosT);
                                                e_hbond = sinT * ((cC / rC) - (cD / rD) + cosT);
                                                //printf("e_b %f\n",e_hbond);
                             		}
                                        // printf("e_b %f\n",e_hbond);
                       		}
                 	}
                        if ((rij > 0) && (rij < 8) &&  ind1 == HBOND){
                        	//printf("Hola\n");
                                if (ind2 == OXI || ind2 == NIT2 || ind2 == AZU){
                                	//printf("Hola\n");
                                        sub_h(&ligando.lig_x[ligando.bonds[MAXBOND*j]], &ligando.lig_y[ligando.bonds[MAXBOND*j]], &ligando.lig_z[ligando.bonds[MAXBOND*j]], &pos[0], &pos[1], &pos[2], v);
                                        sub_h(&proteina.rec_x[i], &proteina.rec_y[i], &proteina.rec_z[i],&pos[0], &pos[1], &pos[2], w);
                                       	double cosT = cosTheta_h (v, w);

                                        if ( cosT < 0.0 ){
                                        	double sinT = sqrtf(1.0-cosT*cosT);
                                                e_hbond += sinT * ((cC / rC) - (cD / rD) + cosT);
                                                //printf("e_b %f\n",e_hbond);
                                      	}
                          	}
                     	}
			tmp_e_es += e_es;
                        tmp_e_vdw += e_vdw;
                        tmp_e_hbond += e_hbond;
                        tmp_e_solv += e_solv;
		}
		es_atom = (tmp_e_es * ligando.ql[j]);
		vdw_atom = tmp_e_vdw;
		hbond_atom = tmp_e_hbond;
		solv_atom = (tmp_e_solv * fabs(ligando.ql[j]) );
		//solv_atom = 0;

		vdw_atom *= weights[0];
		hbond_atom *= weights[1];
 
		temp_es += es_atom;
                temp_vdw += vdw_atom;
                temp_hbond += hbond_atom;
                temp_solv += solv_atom;

                //esfile << j+1 << " " << getTypeString(ligando.ligtype[j],cadena) << " "  << ligando.lig_x[j] << " " << ligando.lig_y[j] << " " << ligando.lig_z[j] << " "  << ligando.ql[j] << " " << es_atom << " " << vdw_atom << " " << hbond_atom << " " << solv_atom;
		//esfile << endl;

		output_l << "\t\t[" << endl;
                output_l << "\t\t\t\"" << es_atom << "\"," << endl;
                output_l << "\t\t\t\"" << vdw_atom << "\"," << endl;
                output_l << "\t\t\t\"" << hbond_atom << "\"," << endl;
                output_l << "\t\t\t\"" << solv_atom << "\"," << endl;
                output_l << "\t\t\t\"" << 0 << "\"" << endl;
                if ((j + 1) != ligando.atoms) output_l << "\t\t]," << endl; else output_l << "\t\t]" << endl;

                tmp_e_es=0;
                tmp_e_vdw=0;
                tmp_e_hbond=0;
                tmp_e_solv=0;
		es_atom=0;vdw_atom=0;hbond_atom=0;solv_atom=0;
	}
	output_l << "\t]," << endl;
	//esfile << endl;
        //esfile.close();
        //esfile << es_total*ES_CONSTANT << "\t" << vdw_total << "\t" << hbond_total <<  "\t" << (es_total*ES_CONSTANT) + vdw_total + hbond_total << endl;
        type_data tor_total = tor*weights[4];
        //strcpy(totaltemp,filename);
        //strcat(totaltemp,".tot");
        //strcat(totaltemp,'\0');
        //printf("totaltemp %s \n",totaltemp);
        //ofstream totalesfile (totaltemp, ios::out | ios::trunc);
        //totalesfile << temp_es << "     " << temp_vdw << "     " << temp_hbond << "     " << temp_solv << "     " << tor_total << endl;
        //totalesfile << "# " << temp_es + temp_vdw + temp_hbond + temp_solv + tor_total << endl;
        //totalesfile.close();

	output_l << "\t\"graph_atoms_type\": [" << endl;

        int a;
        for (a=0; a < ligando.atoms; a++)
                output_l << "\t\t\"" << type_json[a] << "\"," << endl;

        output_l << "\t\t\"" << "" << "\"" << endl;
        output_l << "\t]," << endl;

        output_l << "\t\"graph_global_color\": [" << endl;

        output_l << "\t\t\"b\"," << endl;
        output_l << "\t\t\"g\"," << endl;
        output_l << "\t\t\"r\"," << endl;
        output_l << "\t\t\"c\"," << endl;
        output_l << "\t\t\"m\"," << endl;
        output_l << "\t\t\"k\"" << endl;
        output_l << "\t]," << endl;

        output_l << "\t\"graph_global_field\": [" << endl;
        output_l << "\t\t\"Electrostatic\"," << endl;
        output_l << "\t\t\"Van der Waal\"," << endl;
        output_l << "\t\t\"Hydrogen_Bonds\"," << endl;
        output_l << "\t\t\"Desolvation\"," << endl;
        output_l << "\t\t\"Torsion\"," << endl;
        output_l << "\t\t\"Total_Affinity\"" << endl;
        output_l << "\t]," << endl;

        output_l << "\t\"graph_global_score\": [" << endl;
        output_l << "\t\t\"" << temp_es << "\"," << endl;
        output_l << "\t\t\"" << temp_vdw << "\"," << endl;
        output_l << "\t\t\"" << temp_hbond << "\"," << endl;
        output_l << "\t\t\"" << temp_solv << "\"," << endl;
        output_l << "\t\t\"" << tor_total << "\"," << endl;
        output_l << "\t\t\"" << temp_es + temp_vdw + temp_hbond + temp_solv + tor_total << "\"" << endl;
        output_l << "\t]," << endl;

        output_l << "\t\"name\": \"" << name_protein << "\"," << endl;
        output_l << "\t\"num_aminoacid\": \"" << ball << "\"," << endl;
        output_l << "\t\"num_execution\": \"" << ball << "\"" << endl;
        output_l << "}" << endl;

        output_l.close();

	return (temp_es + temp_vdw + temp_hbond + temp_solv + tor_total);
}

extern type_data writeLigandEnergies_type1 (unsigned int tor, unsigned int flex, unsigned int shift, char *filename, struct receptor_t proteina, struct ligand_t ligando, struct vectors_t *vectors_ini, type_data *q_x, type_data *q_y, type_data *q_z, type_data *q_w, type_data* move_x, type_data* move_y, type_data* move_z, type_data *weights, struct force_field_param_t *f_params, struct param_t param, char *temp_json, char **type_json, unsigned int ball){

type_data pos[3], aux1, aux2, aux3, e_vdw,es_atom,vdw_atom, hbond_atom, esolv_atom, e_solv, e_hbond, e_es, eps, sig, rij, rij2;
    	type_data v[3], w[3], vdw_total, hbond_total, es_total, esolv_total;
	
	type_data solv_asp_1, solv_asp_2, solv_vol_1, solv_vol_2,solv_qasp_1,solv_qasp_2,pre_calc,lig_map;
    double minus_inv_two_sigma_sqd = -0.5L / (sigma_autodock * sigma_autodock);
    type_data exponencial;
	
	unsigned int ind1, ind2, x,y,z, nenlaces;
	unsigned int index;
	unsigned int enlaces[MAXBOND];
	char totaltemp[500];
	char name_protein[500];
	char cadena[4];

	extract_name_protein(param.protein,name_protein);
        ofstream output_l(temp_json, ios::out | ios::app);

        if (!output_l.good()) {
                cout << "can't open file " << output_l << endl;
                cout << "Will not create file temp_json" << endl;
                exit (1);
        }

	//ofstream esfile(filename, ios::out | ios::trunc);

	/*if (!esfile.good()) {
		cout << "can't open file " << filename << endl;
		cout << "Will not write ligand energies file" << endl;
		exit (1);
	}*/

	vdw_total = 0;
	hbond_total=0;
	es_total=0;
	esolv_total = 0;

	for (unsigned int j=0; j < ligando.atoms; j++){
		
		//rotate3DPoint_cpp(q_x, q_y, q_z, q_w, &(ligando.lig_x[j]), &(ligando.lig_y[j]), &(ligando.lig_z[j]),pos);
		//printf("atom %d, pos %f %f %f, move %f %f %f\n",j,pos[0],pos[1],pos[2],move_x[0],move_y[0],move_z[0]);

		pos[0] = ligando.lig_x[j];
		pos[1] = ligando.lig_y[j];
		pos[2] = ligando.lig_z[j];
		//printf("pos0 %f , pos1 %f , pos2 %f ql_j %f \n",pos[0],pos[1],pos[2],ligando.ql[j]);
		
		vdw_atom = 0;
		hbond_atom= 0;
		es_atom = 0;
		esolv_atom = 0;

		ind1 = ligando.ligtype[j];
		solv_asp_1 = f_params[ind1].asp;
        solv_vol_1 = f_params[ind1].vol;
		nenlaces= 0;
		
		for (unsigned int i=0; i < proteina.nrec;i++)
		{
			e_vdw = 0;
		    	e_hbond = 0;
			e_es = 0;
			e_solv = 0;

			aux1=proteina.rec_x[i]-pos[0];
			aux2=proteina.rec_y[i]-pos[1];
			aux3=proteina.rec_z[i]-pos[2];
			aux1 *= aux1;
			aux2 *= aux2;
			aux3 *= aux3;
			rij2=aux1+aux2+aux3; //rij al cuadrado		        
			rij = sqrtf(rij2);
			//if (j==10)
			//{
			//	printf("Soy el atomo %d, vecino %d, y mi rij = %f",j+1,vecino,rij);
			//}
			ind2 = proteina.rectype[i];
			solv_asp_2 = f_params[ind2].asp;
            solv_vol_2 = f_params[ind2].vol;
			exponencial = exp(minus_inv_two_sigma_sqd * rij2) * weights[3];
			//eps = sqrtf(vdw_params[ind1].epsilon * vdw_params[ind2].epsilon);
			//sig = vdw_params[ind1].sigma * vdw_params[ind2].sigma;
			eps = sqrtf(f_params[ind1].epsilon * f_params[ind2].epsilon);
			sig = f_params[ind1].sigma * f_params[ind2].sigma;
			//if (i==0) printf("eps %f, sig %f\n",eps,sig);
			aux1 = sig / rij2;
			aux1 = aux1 * aux1 * aux1; //elevado a 3
			//aux1 *= aux1;		   //elevado a 6
			e_vdw = 4.0 * eps * aux1 * (aux1 - 1.0);
			e_es = proteina.qr[i]*(1/rij)*ligando.ql[j];		
			pre_calc = ((solv_asp_1 * solv_vol_2)  + ((solv_asp_2 + qsolpar * fabs(proteina.qr[i])) * solv_vol_1)) * exponencial;
            lig_map = qsolpar * solv_vol_2 * exponencial;
            e_solv = pre_calc + lig_map;
			//if (j==10) printf(", aux1 = %f, temporal = %f\n",aux1,e_vdw);	
			
			if (rij > 0.18 && rij < 0.39 && (ind1 == OXI || ind1 == NIT2 || ind1 == AZU)){
				if (ind2 == HBOND){
					sub_h(&pos[0], &pos[1], &pos[2],  &proteina.rec_x[i], &proteina.rec_y[i], &proteina.rec_z[i], v);
					sub_h(&(proteina.rec_x[proteina.bonds[MAXBOND * i]]), &(proteina.rec_y[proteina.bonds[MAXBOND * i]]),&(proteina.rec_z[proteina.bonds[MAXBOND * i]]),&proteina.rec_x[i], &proteina.rec_y[i], &proteina.rec_z[i], w);
					type_data cosT = cosTheta_h (v, w);

					if ( cosT < 0.0 ){
						type_data sinT = sqrtf(1.0-cosT*cosT);
						e_vdw *= sinT;
						aux2 = rij2 * rij2 * rij2 * rij2 * rij2 ;
						//if (j == 12) printf("cosT %f, disty %f, distx %f\n",cosT,aux2,rij2);
						//e_hbond = -cosT * ( hbond_params[ind1].c12 / (aux2*rij2)  -  hbond_params[ind1].c10 / aux2) ;
						e_hbond = -cosT * ( f_params[ind1].epsilon_h / (aux2*rij2)  -  f_params[ind1].sigma_h / aux2) ;
						//enlaces[nenlaces] = i;
						//nenlaces++;
					}	
				}

			}
			
			if (rij > 0.18 && rij < 0.39 &&  ind1 == HBOND){
				if (ind2 == OXI || ind2 == NIT2 || ind2 == AZU){
					sub_h(&ligando.lig_x[ligando.bonds[MAXBOND*j]], &ligando.lig_y[ligando.bonds[MAXBOND*j]], &ligando.lig_z[ligando.bonds[MAXBOND*j]], &pos[0], &pos[1], &pos[2], v);		
					sub_h(&proteina.rec_x[i], &proteina.rec_y[i], &proteina.rec_z[i],&pos[0], &pos[1], &pos[2], w);
					
					type_data cosT = cosTheta_h (v, w);

					if ( cosT < 0.0 ){
						type_data sinT = sqrtf(1.0-cosT*cosT);
						e_vdw *= sinT;
						aux2 = rij2 * rij2 * rij2 * rij2 * rij2;
						//if (j == 12) printf("ligx %f, ligy %f, ligz %f, cosT %f, disty %f, distx %f\n",pos[0],pos[1],pos[2],cosT,aux2,rij2);
						//e_hbond = -cosT * (hbond_params[ind2].c12 / (aux2*rij2) - hbond_params[ind2].c10 / aux2 );
						e_hbond = -cosT * (f_params[ind2].epsilon_h / (aux2*rij2) - f_params[ind2].sigma_h / aux2 );
						//enlaces[nenlaces] = i;
						//nenlaces++;
					}
				}

			}
			vdw_atom += (e_vdw * weights[0]);
			hbond_atom += (e_hbond * weights[1]);
			es_atom += (e_es * weights[2]);
			esolv_atom += (e_solv );
			
		}
		esolv_atom *= fabs(ligando.ql[j]);
		//esfile << j+1 << " " << getTypeString(ligando.ligtype[j],cadena) << " "  << ligando.lig_x[j]*10 << " " << ligando.lig_y[j]*10 << " " << ligando.lig_z[j]*10 << " "  << ligando.ql[j] << " " << es_atom*ES_CONSTANT << " " << vdw_atom << " " << hbond_atom << " " << esolv_atom;

		//for (int k= 0; k < nenlaces; k++){
		//	esfile << " " << enlaces[k]+1 ;

		//}
	
        	//esfile << endl;

	  	vdw_total += vdw_atom;
		hbond_total += hbond_atom;
		es_total += es_atom;
		esolv_total += esolv_atom;

		output_l << "\t\t[" << endl;
                output_l << "\t\t\t\"" << es_atom*ES_CONSTANT << "\"," << endl;
                output_l << "\t\t\t\"" << vdw_atom << "\"," << endl;
                output_l << "\t\t\t\"" << hbond_atom << "\"," << endl;
                output_l << "\t\t\t\"" << esolv_atom << "\"," << endl;
                output_l << "\t\t\t\"" << 0 << "\"" << endl;
                if ((j + 1) != ligando.atoms) output_l << "\t\t]," << endl; else output_l << "\t\t]" << endl;
	}
	output_l << "\t]," << endl;
	//esfile << endl;
	//esfile.close();
	//esfile << es_total*ES_CONSTANT << "\t" << vdw_total << "\t" << hbond_total <<  "\t" << (es_total*ES_CONSTANT) + vdw_total + hbond_total << endl;
	type_data tor_total = tor*weights[4];	
	//strcpy(totaltemp,filename);
	//strcat(totaltemp,".tot");
	//strcat(totaltemp,'\0');
	//printf("totaltemp %s \n",totaltemp);
	//ofstream totalesfile (totaltemp, ios::out | ios::trunc);
	//totalesfile << es_total*ES_CONSTANT << "     " << vdw_total << "     " << hbond_total << "     " << esolv_total << "     "<< tor_total << endl;
	//totalesfile << "# " << (es_total*ES_CONSTANT) + vdw_total + hbond_total + esolv_total + tor_total << endl;
	//totalesfile.close();
	output_l << "\t\"graph_atoms_type\": [" << endl;

        int a;
        for (a=0; a < ligando.atoms; a++)
                output_l << "\t\t\"" << type_json[a] << "\"," << endl;

        output_l << "\t\t\"" << "" << "\"" << endl;
        output_l << "\t]," << endl;

        output_l << "\t\"graph_global_color\": [" << endl;

        output_l << "\t\t\"b\"," << endl;
        output_l << "\t\t\"g\"," << endl;
        output_l << "\t\t\"r\"," << endl;
        output_l << "\t\t\"c\"," << endl;
        output_l << "\t\t\"m\"," << endl;
        output_l << "\t\t\"k\"" << endl;
        output_l << "\t]," << endl;

        output_l << "\t\"graph_global_field\": [" << endl;
        output_l << "\t\t\"Electrostatic\"," << endl;
        output_l << "\t\t\"Van der Waal\"," << endl;
        output_l << "\t\t\"Hydrogen_Bonds\"," << endl;
        output_l << "\t\t\"Desolvation\"," << endl;
        output_l << "\t\t\"Torsion\"," << endl;
        output_l << "\t\t\"Total_Affinity\"" << endl;
        output_l << "\t]," << endl;

        output_l << "\t\"graph_global_score\": [" << endl;
        output_l << "\t\t\"" << es_total*ES_CONSTANT << "\"," << endl;
        output_l << "\t\t\"" << vdw_total << "\"," << endl;
        output_l << "\t\t\"" << hbond_total << "\"," << endl;
        output_l << "\t\t\"" << esolv_total << "\"," << endl;
        output_l << "\t\t\"" << tor_total << "\"," << endl;
        output_l << "\t\t\"" << (es_total*ES_CONSTANT) + vdw_total + hbond_total + esolv_total + tor_total << "\"" << endl;
        output_l << "\t]," << endl;

        output_l << "\t\"name\": \"" << name_protein << "\"," << endl;
        output_l << "\t\"num_aminoacid\": \"" << ball << "\"," << endl;
        output_l << "\t\"num_execution\": \"" << ball << "\"" << endl;
        output_l << "}" << endl;

        output_l.close();
	return ((es_total*ES_CONSTANT) + vdw_total + hbond_total + esolv_total + tor_total);
	
}
			
		

extern type_data writeLigandEnergies (unsigned int flex, unsigned int shift, char *filename, struct receptor_t proteina, struct ligand_t ligando, struct vectors_t *vectors_ini, type_data *q_x, type_data *q_y, type_data *q_z, type_data *q_w, type_data* move_x, type_data* move_y, type_data* move_z, type_data *weights, struct force_field_param_t *f_params, struct param_t param, char *temp_json, char **type_json, unsigned int ball){

	type_data pos[3], aux1, aux2, aux3, e_vdw,es_atom,vdw_atom, hbond_atom, e_hbond, e_es, eps, sig, rij, rij2;
    	type_data v[3], w[3], vdw_total, hbond_total, es_total;
	unsigned int ind1, ind2, x,y,z, nenlaces;
	unsigned int index;
	unsigned int enlaces[MAXBOND];
	char totaltemp[500];
	char name_protein[500];
	char cadena[4];

	extract_name_protein(param.protein,name_protein);
	ofstream output_l(temp_json, ios::out | ios::app);

        if (!output_l.good()) {
                cout << "can't open file " << output_l << endl;
                cout << "Will not create file temp_json" << endl;
                exit (1);
        }

	//ofstream esfile(filename, ios::out | ios::trunc);

	/*if (!esfile.good()) {
		cout << "can't open file " << filename << endl;
		cout << "Will not write ligand energies file" << endl;
		exit (1);
	}*/

	vdw_total = 0;
	hbond_total=0;
	es_total=0;

	for (unsigned int j=0; j < ligando.atoms; j++){
		
		//rotate3DPoint_cpp(q_x, q_y, q_z, q_w, &(ligando.lig_x[j]), &(ligando.lig_y[j]), &(ligando.lig_z[j]),pos);
		//printf("atom %d, pos %f %f %f, move %f %f %f\n",j,pos[0],pos[1],pos[2],move_x[0],move_y[0],move_z[0]);

		pos[0] = ligando.lig_x[j];
		pos[1] = ligando.lig_y[j];
		pos[2] = ligando.lig_z[j];
		//printf("pos0 %f , pos1 %f , pos2 %f ql_j %f \n",pos[0],pos[1],pos[2],ligando.ql[j]);
		
		vdw_atom = 0;
		hbond_atom= 0;
		es_atom = 0;

		ind1 = ligando.ligtype[j];

		nenlaces= 0;
		
		for (unsigned int i=0; i < proteina.nrec;i++)
		{
			e_vdw = 0;
		    	e_hbond = 0;
			e_es = 0;

			aux1=proteina.rec_x[i]-pos[0];
			aux2=proteina.rec_y[i]-pos[1];
			aux3=proteina.rec_z[i]-pos[2];
			aux1 *= aux1;
			aux2 *= aux2;
			aux3 *= aux3;
			rij2=aux1+aux2+aux3; //rij al cuadrado		        
			rij = sqrtf(rij2);
			//if (j==10)
			//{
			//	printf("Soy el atomo %d, vecino %d, y mi rij = %f",j+1,vecino,rij);
			//}
			ind2 = proteina.rectype[i];
			//eps = sqrtf(vdw_params[ind1].epsilon * vdw_params[ind2].epsilon);
			//sig = vdw_params[ind1].sigma * vdw_params[ind2].sigma;
			eps = sqrtf(f_params[ind1].epsilon * f_params[ind2].epsilon);
			sig = f_params[ind1].sigma * f_params[ind2].sigma;
			//if (i==0) printf("eps %f, sig %f\n",eps,sig);
			aux1 = sig / rij2;
			aux1 = aux1 * aux1 * aux1; //elevado a 3
			//aux1 *= aux1;		   //elevado a 6
			e_vdw = 4.0 * eps * aux1 * (aux1 - 1.0);
			e_es = proteina.qr[i]*(1/rij)*ligando.ql[j];		
			//if (j==10) printf(", aux1 = %f, temporal = %f\n",aux1,e_vdw);			
			if (rij > 0.18 && rij < 0.39 && (ind1 == OXI || ind1 == NIT2 || ind1 == AZU)){
				if (ind2 == HBOND){
					sub_h(&pos[0], &pos[1], &pos[2],  &proteina.rec_x[i], &proteina.rec_y[i], &proteina.rec_z[i], v);
					sub_h(&(proteina.rec_x[proteina.bonds[MAXBOND * i]]), &(proteina.rec_y[proteina.bonds[MAXBOND * i]]),&(proteina.rec_z[proteina.bonds[MAXBOND * i]]),&proteina.rec_x[i], &proteina.rec_y[i], &proteina.rec_z[i], w);
					type_data cosT = cosTheta_h (v, w);

					if ( cosT < 0.0 ){
						type_data sinT = sqrtf(1.0-cosT*cosT);
						e_vdw *= sinT;
						aux2 = rij2 * rij2 * rij2 * rij2 * rij2 ;
						//if (j == 12) printf("cosT %f, disty %f, distx %f\n",cosT,aux2,rij2);
						//e_hbond = -cosT * ( hbond_params[ind1].c12 / (aux2*rij2)  -  hbond_params[ind1].c10 / aux2) ;
						e_hbond = -cosT * ( f_params[ind1].epsilon_h / (aux2*rij2)  -  f_params[ind1].sigma_h / aux2) ;
						//enlaces[nenlaces] = i;
						//nenlaces++;
					}	
				}

			}
			
			if (rij > 0.18 && rij < 0.39 &&  ind1 == HBOND){
				if (ind2 == OXI || ind2 == NIT2 || ind2 == AZU){
					sub_h(&ligando.lig_x[ligando.bonds[MAXBOND*j]], &ligando.lig_y[ligando.bonds[MAXBOND*j]], &ligando.lig_z[ligando.bonds[MAXBOND*j]], &pos[0], &pos[1], &pos[2], v);		
					sub_h(&proteina.rec_x[i], &proteina.rec_y[i], &proteina.rec_z[i],&pos[0], &pos[1], &pos[2], w);
					
					type_data cosT = cosTheta_h (v, w);

					if ( cosT < 0.0 ){
						type_data sinT = sqrtf(1.0-cosT*cosT);
						e_vdw *= sinT;
						aux2 = rij2 * rij2 * rij2 * rij2 * rij2;
						//if (j == 12) printf("ligx %f, ligy %f, ligz %f, cosT %f, disty %f, distx %f\n",pos[0],pos[1],pos[2],cosT,aux2,rij2);
						//e_hbond = -cosT * (hbond_params[ind2].c12 / (aux2*rij2) - hbond_params[ind2].c10 / aux2 );
						e_hbond = -cosT * (f_params[ind2].epsilon_h / (aux2*rij2) - f_params[ind2].sigma_h / aux2 );
						//enlaces[nenlaces] = i;
						//nenlaces++;
					}
				}

			}

			vdw_atom += (e_vdw * weights[0]);
			hbond_atom += (e_hbond * weights[1]);
			es_atom += (e_es * weights[2]);
		}

		//esfile << j+1 << " " << getTypeString(ligando.ligtype[j],cadena) << " "  << ligando.lig_x[j]*10 << " " << ligando.lig_y[j]*10 << " " << ligando.lig_z[j]*10 << " "  << ligando.ql[j] << " " << es_atom*ES_CONSTANT << " " << vdw_atom << " " << hbond_atom << " " << 0;

		//for (int k= 0; k < nenlaces; k++){
		//	esfile << " " << enlaces[k]+1 ;

		//}
	
        	//esfile << endl;

	  	vdw_total += vdw_atom;
		hbond_total += hbond_atom;
		es_total += es_atom;

		output_l << "\t\t[" << endl;
		output_l << "\t\t\t\"" << es_atom*ES_CONSTANT << "\"," << endl;
        	output_l << "\t\t\t\"" << vdw_atom << "\"," << endl;
        	output_l << "\t\t\t\"" << hbond_atom << "\"," << endl;
		output_l << "\t\t\t\"" << 0 << "\"," << endl;
		output_l << "\t\t\t\"" << 0 << "\"" << endl;
        	if ((j + 1) != ligando.atoms) output_l << "\t\t]," << endl; else output_l << "\t\t]" << endl;
	}
	output_l << "\t]," << endl;
	//esfile << endl;
	//esfile.close();
	//esfile << es_total*ES_CONSTANT << "\t" << vdw_total << "\t" << hbond_total <<  "\t" << (es_total*ES_CONSTANT) + vdw_total + hbond_total << endl;
	
	//strcpy(totaltemp,filename);
	//strcat(totaltemp,".tot");
	//strcat(totaltemp,'\0');
	//printf("totaltemp %s \n",totaltemp);
	//ofstream totalesfile (totaltemp, ios::out | ios::trunc);
	//totalesfile << es_total*ES_CONSTANT << "     " << vdw_total << "     " << hbond_total << "     " << "0" << "     "<< "0" << endl;
	//totalesfile << "# " << (es_total*ES_CONSTANT) + vdw_total + hbond_total << endl;
	//totalesfile.close();

	output_l << "\t\"graph_atoms_type\": [" << endl;
	
	int a;
	for (a=0; a < ligando.atoms; a++)
        	output_l << "\t\t\"" << type_json[a] << "\"," << endl;

	output_l << "\t\t\"" << "" << "\"" << endl;
	output_l << "\t]," << endl;

	output_l << "\t\"graph_global_color\": [" << endl;

        output_l << "\t\t\"b\"," << endl;
        output_l << "\t\t\"g\"," << endl;
        output_l << "\t\t\"r\"," << endl;
        output_l << "\t\t\"c\"," << endl;
        output_l << "\t\t\"m\"," << endl;
	output_l << "\t\t\"k\"" << endl;
        output_l << "\t]," << endl;
        
        output_l << "\t\"graph_global_field\": [" << endl;
        output_l << "\t\t\"Electrostatic\"," << endl;
        output_l << "\t\t\"Van der Waal\"," << endl;
        output_l << "\t\t\"Hydrogen_Bonds\"," << endl;
        output_l << "\t\t\"Desolvation\"," << endl;
        output_l << "\t\t\"Torsion\"," << endl;
	output_l << "\t\t\"Total_Affinity\"" << endl;
        output_l << "\t]," << endl;

        output_l << "\t\"graph_global_score\": [" << endl;
	output_l << "\t\t\"" << es_total*ES_CONSTANT << "\"," << endl;
        output_l << "\t\t\"" << vdw_total << "\"," << endl;
        output_l << "\t\t\"" << hbond_total << "\"," << endl;
        output_l << "\t\t\"" << 0 << "\"," << endl;
        output_l << "\t\t\"" << 0 << "\"," << endl;
	output_l << "\t\t\"" << (es_total*ES_CONSTANT) + vdw_total + hbond_total << "\"" << endl;
        output_l << "\t]," << endl;

	output_l << "\t\"name\": \"" << name_protein << "\"," << endl;
	output_l << "\t\"num_aminoacid\": \"" << ball << "\"," << endl;
	output_l << "\t\"num_execution\": \"" << ball << "\"" << endl;
	output_l << "}" << endl;

	output_l.close();
	return ((es_total*ES_CONSTANT) + vdw_total + hbond_total);
	
}

void results_conformations (struct vectors_t *vectors_ini, unsigned int *conformations_result, unsigned int max_l, unsigned int *order_balls)
{
	type_data *energy = (type_data *)malloc(sizeof(type_data)*vectors_ini->num_surface);	
	unsigned int *n_conf = (unsigned int *)malloc(sizeof(unsigned int)*vectors_ini->num_surface);
	unsigned int tam_v = vectors_ini->nconformations / vectors_ini->num_surface;
	
	for (unsigned int k = 0; k < vectors_ini->num_surface;k++)
	{
		energy[k] = vectors_ini->energy.energy[k*tam_v];
		n_conf[k] = vectors_ini->energy.n_conformation[k*tam_v];
		//printf("energy[%d] %f n_conf[%d] %d\n",k,energy[k],k,n_conf[k]);
	}
	thrust::stable_sort_by_key(energy, energy + vectors_ini->num_surface, n_conf, thrust::less<type_data>());
	//printf("max_l %u\n",max_l);
	//exit(0);
	for (unsigned int k = 0;k < max_l;k++)
		conformations_result[k] = n_conf[k];
	for (unsigned int k = 0;k < vectors_ini->num_surface;k++)
		order_balls[k] = n_conf[k];
}

extern void configurar_ligandos_finales(struct receptor_t proteina, struct ligand_t ligando, struct ligand_t *ligtemp, struct vectors_t *vectors_ini,struct param_t param,struct metaheuristic_t metaheuristic, struct flexibility_params_t *flexibility_params)
{
	char cadena[500];
	char cadena_json[500];
	char temp_json[500];
	char cadena_ligand[500];
	char cadena_ligand_mol2[500];
	char cadena_output[500];
	char nom_dir[500];
	char orden[200];
	char orden_1[500];
	type_data q_x, q_y, q_z, q_w, m_x, m_y, m_z, m_w, com[3];

	param.max_ligand_selection = vectors_ini->num_surface;
	unsigned int k_id;	
	unsigned int tam_v = vectors_ini->nconformations / vectors_ini->num_surface;
	unsigned int n_c = 0, tmp, shift_conf, act_c;
	unsigned int *conformations_result = (unsigned int *)malloc(param.max_ligand_selection*sizeof(unsigned int));
	unsigned int *order_balls = (unsigned int *)malloc(vectors_ini->num_surface*sizeof(unsigned int));

	strcpy(orden,"rm -rf ");
	sprintf(cadena_output,"%s/output-0",param.output_dir);
	ofstream fp (cadena_output, ios::out | ios::trunc);
	printf("\n*** EXTRAER CONFORMACIONES ***\n");
	results_conformations(vectors_ini,conformations_result,param.max_ligand_selection,order_balls);
	//printf("conf %d\n",conformations_result[0]);	
	*ligtemp = ligando;
	ligtemp->nlig = ligando.atoms;
	ligtemp->lig_x = (type_data *)malloc(sizeof(type_data)*ligando.nlig);
	ligtemp->lig_y = (type_data *)malloc(sizeof(type_data)*ligando.nlig);
	ligtemp->lig_z = (type_data *)malloc(sizeof(type_data)*ligando.nlig);
	nombre_dir(param.output_dir,nom_dir);	
	//printf("nom_dir %s\n",nom_dir);
		
	for (unsigned int k = 0; k < vectors_ini->num_surface;k++)
	{
		act_c = order_balls[k];
		q_x = vectors_ini->quat_x[act_c];	
		q_y = vectors_ini->quat_y[act_c];
		q_z = vectors_ini->quat_z[act_c];
		q_w = vectors_ini->quat_w[act_c];
		m_x = vectors_ini->move_x[act_c];
		m_y = vectors_ini->move_y[act_c];
		m_z = vectors_ini->move_z[act_c];
		/*m_x = 3.734138;	m_y = 2.435627;	m_z = 3.818065;
		q_x = 0.720024; q_y = -0.286111; q_z = 0.562299; q_w = -0.289008;*/
		/*if (k==0)
		{
			printf("act_c %d en %f m_x %f, m_y %f, m_z %f, q_x %f, q_y %f, q_z %f, q_w %f\n",act_c,vectors_ini->energy.energy[act_c],m_x, m_y, m_z, q_x, q_y, q_z, q_w);
			for (int l=0;l<64;l++) printf("atom %f %f %f\n",vectors_ini->conformations_x[act_c*ligando.nlig+l],vectors_ini->conformations_y[act_c*ligando.nlig+l],vectors_ini->conformations_z[act_c*ligando.nlig+l]);
		}*/
		
		k_id = act_c % vectors_ini->num_surface;
		shift_conf = act_c * ligando.nlig;
			
		generarLigando (param.auto_flex,shift_conf,ligando, ligtemp, vectors_ini, &q_x, &q_y, &q_z, &q_w, &m_x, &m_y, &m_z);					
		//completarLigando(ligando,*ligtemp);	
		getCOM(ligtemp->lig_x,ligtemp->lig_y,ligtemp->lig_z,ligando.atoms,com);
		//printf("0 %f 1 %f 2 %f\n",com[0],com[1],com[2]);
		//ligtemp.nlig = nAtom;
		//for (int k=0;k < param.max_ligand_selection;k++)
		//{
		//exit(0);	
		//printf("max_l %d %f %f %f %d %f\n",param.max_ligand_selection,com[0],com[1],com[2],k*tam_v,vectors_ini->surface_x[0]);
		//if ((n_c < param.max_ligand_selection) && (conformations_result[n_c] == act_c))
		//{					
			//printf("\tBest conformation: %d\n",conformations_result[n_c]);
		
		if (param.scoring_function_type != S_AD4)
		{
			sprintf(cadena,"%smolecules/%d_%s_%.3f_%.3f_%.3f",param.output_dir,n_c,nom_dir,10.0*vectors_ini->surface_x[k_id],10.0*vectors_ini->surface_y[k_id],10.0*vectors_ini->surface_z[k_id]);
			writeLigand(cadena, ligtemp,param.scoring_function_type);
			sprintf(cadena_ligand_mol2,"%smolecules/%d_%s_%.3f_%.3f_%.3f.mol2",param.output_dir,n_c,nom_dir,10.0*vectors_ini->surface_x[k_id],10.0*vectors_ini->surface_y[k_id],10.0*vectors_ini->surface_z[k_id]);
			sprintf(orden_1,"%s%s",orden,cadena);	
			//exit(0);
			lig2mol2(ligando.atoms,param.ligand,cadena,cadena_ligand_mol2,1);
			convert_into_mol2(cadena_ligand_mol2,cadena_ligand_mol2);
			//sprintf(cadena, "%senergies/%d-%s_%.3f_%.3f_%.3f.txt",param.output_dir,n_c,nom_dir,10.0*vectors_ini->surface_x[k],10.0*vectors_ini->surface_y[k],10.0*vectors_ini->surface_z[k]);
			sprintf(cadena_json, "%senergies/%d_%s_%.3f_%.3f_%.3f.json",param.output_dir,n_c,nom_dir,10.0*vectors_ini->surface_x[k_id],10.0*vectors_ini->surface_y[k_id],10.0*vectors_ini->surface_z[k_id]);
			sprintf(temp_json, "%senergies/%d_%s_%.3f_%.3f_%.3f_temp.json",param.output_dir,n_c,nom_dir,vectors_ini->surface_x[k_id],vectors_ini->surface_y[k_id],vectors_ini->surface_z[k_id]);
		}
		else
		{
			sprintf(cadena,"%smolecules/%d_%s_%.3f_%.3f_%.3f",param.output_dir,n_c,nom_dir,vectors_ini->surface_x[k_id],vectors_ini->surface_y[k_id],vectors_ini->surface_z[k_id]);
                        writeLigand(cadena, ligtemp,param.scoring_function_type);
                        sprintf(cadena_ligand_mol2,"%smolecules/%d_%s_%.3f_%.3f_%.3f.mol2",param.output_dir,n_c,nom_dir,vectors_ini->surface_x[k_id],vectors_ini->surface_y[k_id],vectors_ini->surface_z[k_id]);
                        sprintf(orden_1,"%s%s",orden,cadena);
                        lig2mol2(ligando.atoms,param.ligand,cadena,cadena_ligand_mol2,1);
                        convert_into_mol2(cadena_ligand_mol2,cadena_ligand_mol2);
                        //sprintf(cadena, "%senergies/%d-%s_%.3f_%.3f_%.3f.txt",param.output_dir,n_c,nom_dir,vectors_ini->surface_x[k],vectors_ini->surface_y[k],vectors_ini->surface_z[k]);
			sprintf(cadena_json, "%senergies/%d_%s_%.3f_%.3f_%.3f.json",param.output_dir,n_c,nom_dir,vectors_ini->surface_x[k_id],vectors_ini->surface_y[k_id],vectors_ini->surface_z[k_id]);
			sprintf(temp_json, "%senergies/%d_%s_%.3f_%.3f_%.3f_temp.json",param.output_dir,n_c,nom_dir,vectors_ini->surface_x[k_id],vectors_ini->surface_y[k_id],vectors_ini->surface_z[k_id]);
		}
		//exit(0);	
		//printf("cadena %s\n",cadena);
		int links;
		type_data total_energies;
		if (param.auto_flex) links = flexibility_params->n_links; else links = 0;
		switch (param.scoring_function_type)
		{
		case 0:
			total_energies = writeLigandEnergies (param.auto_flex, shift_conf, cadena, proteina, *ligtemp, vectors_ini, &q_x, &q_y, &q_z, &q_w, &m_x, &m_y, &m_z,vectors_ini->weights,vectors_ini->f_params,param,temp_json,ligando.type_json,k_id);
			break;
		case 1:
			total_energies = writeLigandEnergies_type1 (links,param.auto_flex, shift_conf, cadena, proteina, *ligtemp, vectors_ini, &q_x, &q_y, &q_z, &q_w, &m_x, &m_y, &m_z,vectors_ini->weights,vectors_ini->f_params,param,temp_json,ligando.type_json,k_id);
                        break;
		case 2:
			total_energies = writeLigandEnergies_type2 (links,param.auto_flex, shift_conf, cadena, proteina, *ligtemp, vectors_ini, &q_x, &q_y, &q_z, &q_w, &m_x, &m_y, &m_z,vectors_ini->weights,vectors_ini->f_params,param,temp_json,ligando.type_json,k_id);
                        break;
		}
		create_json(param,cadena_ligand_mol2,cadena_json,temp_json,vectors_ini->surface_x[k_id],vectors_ini->surface_y[k_id],vectors_ini->surface_z[k_id],param.protein,param.ligand,param.scoring_function_type,total_energies);
		if (param.scoring_function_type != S_AD4)
		{
			//sprintf(cadena,"%stxt/%d-%s_%.3f_%.3f_%.3f.txt",param.output_dir,n_c,nom_dir,10.0*vectors_ini->surface_x[k],10.0*vectors_ini->surface_y[k],10.0*vectors_ini->surface_z[k]);
			//ofstream fp_1 (cadena, ios::out | ios::trunc);
			//fp_1 << vectors_ini->energy.energy[act_c] << " " << 10.0*com[0] << " " << 10.0*com[1] << " " << 10.0*com[2] << " " << param.ligand << " " << n_c << "-" << endl;
			//fp_1.close();
			system(orden_1);
			//n_c++;
			//}
		
			fp << k_id << " " << 10.0*vectors_ini->surface_x[k_id] << " " << 10.0*vectors_ini->surface_y[k_id] << " " << 10.0*vectors_ini->surface_z[k_id] << " " << vectors_ini->energy.energy[act_c] << " " << 10.0*com[0] << " " << 10.0*com[1] << " " << 10.0*com[2] << " " << endl;
		}
		else
		{
			//sprintf(cadena,"%stxt/%d-%s_%.3f_%.3f_%.3f.txt",param.output_dir,n_c,nom_dir,vectors_ini->surface_x[k],vectors_ini->surface_y[k],vectors_ini->surface_z[k]);
                        //ofstream fp_1 (cadena, ios::out | ios::trunc);
                        //fp_1 << vectors_ini->energy.energy[act_c] << " " << com[0] << " " << com[1] << " " << com[2] << " " << param.ligand << " " << n_c << "-" << endl;
                        //fp_1.close();
                        system(orden_1);
                        //n_c++;
                        //}

                        fp << k_id << " " << vectors_ini->surface_x[k_id] << " " << vectors_ini->surface_y[k_id] << " " << vectors_ini->surface_z[k_id] << " " << vectors_ini->energy.energy[act_c] << " " << com[0] << " " << com[1] << " " << com[2] << " " << endl;

		}
		n_c++;
	}
	fp.close();			
}

extern void configurar_ligandos_finales_vs (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct vectors_t *vectors_ini,struct param_t param,struct metaheuristic_t metaheuristic, struct flexibility_params_t *flexibility_params)
{
	char cadena[500];
	char cadena_json[500];
	char temp_json[500];
	char nombre_f[500];
	char cadena_ligand[500];
	char cadena_output[500];
	struct ligand_t ligando;
	type_data q_x, q_y, q_z, q_w, m_x, m_y, m_z, m_w, com[3];
	char base[500];
	char tmp_file[500];
	char tmp_name[500];
	char orden[500];
        char orden_1[500];
	char **tmp_1;
	
	strcpy(orden,"rm -rf ");
	unsigned int tam_v = vectors_ini->nconformations / vectors_ini->files;
	//printf("tam_v %d conf %d files %d\n",tam_v,vectors_ini->nconformations,vectors_ini->files);
	unsigned int n_c = 0, tmp, shift_conf, act_c;

	nombre_fichero_completo(param.protein,tmp_file);
	nombre_fichero(tmp_file,tmp_name);

	sprintf(base,"VS_MT_%s_",tmp_name);
	//printf("base %s\n",base);
	//ofstream fp (cadena_output, ios::out | ios::trunc);
	printf("\n*** EXTRAER CONFORMACIONES VS ***\n");
	
	ligando.nlig = vectors_ini->nlig;
	for (unsigned int i = 0; i < vectors_ini->files;i++)
	{	
		ligando.atoms = ligand_params[i].atoms;
		ligando.lig_x = (type_data *)malloc(sizeof(type_data)*ligando.nlig);
		ligando.lig_y = (type_data *)malloc(sizeof(type_data)*ligando.nlig);
		ligando.lig_z = (type_data *)malloc(sizeof(type_data)*ligando.nlig);
		
		act_c = vectors_ini->energy.n_conformation[i*tam_v];
		q_x = vectors_ini->quat_x[act_c];
		q_y = vectors_ini->quat_y[act_c];
		q_z = vectors_ini->quat_z[act_c];
		q_w = vectors_ini->quat_w[act_c];
		m_x = vectors_ini->move_x[act_c];
		m_y = vectors_ini->move_y[act_c];
		m_z = vectors_ini->move_z[act_c];
		/*m_x = 3.734138;	m_y = 2.435627;	m_z = 3.818065;
		q_x = 0.720024; q_y = -0.286111; q_z = 0.562299; q_w = -0.289008;*/
		//printf("act %d m_x %f, m_y %f, m_z %f, q_x %f, q_y %f, q_z %f, q_w %f en %f\n",act_c, m_x, m_y, m_z, q_x, q_y, q_z, q_w,vectors_ini->energy.energy[act_c]);
		//if (act_c == 0)
		//{
		//	for (int i=0;i<64;i++) printf("conf %d, xyz %f %f %f m %f %f %f, q %f %f %f, energy %f %d\n",i,vectors_ini->conformations_x[i],vectors_ini->conformations_y[i], vectors_ini->conformations_z[i],vectors_ini->move_x[i/vectors_ini->nlig],vectors_ini->move_y[i/vectors_ini->nlig],vectors_ini->move_z[i/vectors_ini->nlig],vectors_ini->quat_x[i/vectors_ini->nlig],vectors_ini->quat_y[i/vectors_ini->nlig],vectors_ini->quat_z[i/vectors_ini->nlig],vectors_ini->energy.energy[i/vectors_ini->nlig],vectors_ini->energy.n_conformation[i/vectors_ini->nlig]);
		//}	
		shift_conf = act_c * ligando.nlig;
		//printf("act_c %d\n",act_c);
		//if (i==1) exit(0);	
		completarLigando_vs(ligando,vectors_ini,act_c,tam_v);
		//for (int i=0;i<ligando.atoms;i++) printf("atom %d ql %f ligtype %d\n",i,ligando.ql[i],ligando.ligtype[i]);	
			
		generarLigando_vs (param.auto_flex,shift_conf,ligando, vectors_ini, &q_x, &q_y, &q_z, &q_w, &m_x, &m_y, &m_z);					
		getCOM(ligando.lig_x,ligando.lig_y,ligando.lig_z,ligando.atoms,com);
			
		nombre_fichero_completo(ligand_params[i].file,cadena);
		//printf("Nombre fichero completo %s\n",cadena);
		nombre_fichero(cadena,nombre_f);
		//printf("cadena %s\n",cadena);
		//sprintf(cadena, "%s/%s/%s-%d",param.output_dir,nombre_f,nombre_f,i);
		//printf("cadena: %s\n",cadena);
		if (param.scoring_function_type != S_AD4)
                {
			sprintf(cadena,"%smolecules/%s%s_%.3f_%.3f_%.3f",param.output_dir,base,nombre_f,10.0*vectors_ini->ac_x,10.0*vectors_ini->ac_y,10.0*vectors_ini->ac_z);
			writeLigand(cadena, &ligando, param.scoring_function_type);
			//exit(0);	
			sprintf(cadena_ligand,"%s.mol2",cadena);	
			//sprintf(tmp_cadena_mol2,"%s_1.mol2",cadena);

			sprintf(orden_1,"%s%s",orden,cadena);
			//printf("original %s cadena %s cadena_ligand %s\n",ligand_params[i].file,cadena,cadena_ligand);
			lig2mol2(ligand_params[i].atoms,ligand_params[i].file,cadena,cadena_ligand,1);
			convert_into_mol2(cadena_ligand,cadena_ligand);
			//exit(0);
			//sprintf(cadena, "%s/%s/%s-%d.txt",param.output_dir,nombre_f,nombre_f,i);
			//sprintf(cadena, "%senergies/%s%s_%.3f_%.3f_%.3f.txt",param.output_dir,base,nombre_f,10.0*vectors_ini->ac_x,10.0*vectors_ini->ac_y,10.0*vectors_ini->ac_z);
			sprintf(cadena_json, "%senergies/%s%s_%.3f_%.3f_%.3f.json",param.output_dir,base,nombre_f,10.0*vectors_ini->ac_x,10.0*vectors_ini->ac_y,10.0*vectors_ini->ac_z);
                        sprintf(temp_json, "%senergies/%s%s_%.3f_%.3f_%.3f_temp.json",param.output_dir,base,nombre_f,10.0*vectors_ini->ac_x,10.0*vectors_ini->ac_y,10.0*vectors_ini->ac_z);
			//printf("cadena %s\n",cadena);
		}
		else
		{
			sprintf(cadena,"%smolecules/%s%s_%.3f_%.3f_%.3f",param.output_dir,base,nombre_f,vectors_ini->ac_x,vectors_ini->ac_y,vectors_ini->ac_z);
                        writeLigand(cadena, &ligando, param.scoring_function_type);
                        //exit(0);
                        sprintf(cadena_ligand,"%s.mol2",cadena);
                        //sprintf(tmp_cadena_mol2,"%s_1.mol2",cadena);

                        sprintf(orden_1,"%s%s",orden,cadena);
                        //printf("original %s cadena %s cadena_ligand %s\n",ligand_params[i].file,cadena,cadena_ligand);
                        lig2mol2(ligand_params[i].atoms,ligand_params[i].file,cadena,cadena_ligand,1);
                        convert_into_mol2(cadena_ligand,cadena_ligand);
                        //exit(0);
                        //sprintf(cadena, "%s/%s/%s-%d.txt",param.output_dir,nombre_f,nombre_f,i);
			sprintf(cadena_json, "%senergies/%s%s_%.3f_%.3f_%.3f.json",param.output_dir,base,nombre_f,10.0*vectors_ini->ac_x,10.0*vectors_ini->ac_y,10.0*vectors_ini->ac_z);
                        sprintf(temp_json, "%senergies/%s%s_%.3f_%.3f_%.3f_temp.json",param.output_dir,base,nombre_f,10.0*vectors_ini->ac_x,10.0*vectors_ini->ac_y,10.0*vectors_ini->ac_z);
			//exit(0);
		}
		int links;
		type_data total_energies;
		if (param.auto_flex) links = flexibility_params[i].n_links; else links = 0;
		switch (param.scoring_function_type)
                {
                	case 0:
				total_energies = writeLigandEnergies (param.auto_flex, shift_conf, cadena, proteina, ligando, vectors_ini, &q_x, &q_y, &q_z, &q_w, &m_x, &m_y, &m_z, vectors_ini->weights,vectors_ini->f_params,param,cadena_json,ligand_params[i].type_json,i);
				break;
			case 1:
				total_energies = writeLigandEnergies_type1 (links,param.auto_flex, shift_conf, cadena, proteina, ligando, vectors_ini, &q_x, &q_y, &q_z, &q_w, &m_x, &m_y, &m_z, vectors_ini->weights,vectors_ini->f_params,param,cadena_json,ligand_params[i].type_json,i);
				break;
			case 2:
				total_energies = writeLigandEnergies_type2 (links,param.auto_flex, shift_conf, cadena, proteina, ligando, vectors_ini, &q_x, &q_y, &q_z, &q_w, &m_x, &m_y, &m_z, vectors_ini->weights,vectors_ini->f_params,param,cadena_json,ligand_params[i].type_json,i);
                                break;
		}
		create_json(param,cadena_ligand,cadena_json,temp_json,vectors_ini->ac_x,vectors_ini->ac_y,vectors_ini->ac_z,param.protein,ligand_params[i].file,param.scoring_function_type,total_energies);
		//exit(0);
		/*if (param.scoring_function_type != S_AD4)
		{
			sprintf(cadena,"%stxt/%s%s_%.3f_%.3f_%.3f.txt",param.output_dir,base,nombre_f,10.0*vectors_ini->ac_x,10.0*vectors_ini->ac_y,10.0*vectors_ini->ac_z);
                	ofstream fp_1 (cadena, ios::out | ios::trunc);
                	fp_1 << vectors_ini->energy.energy[act_c] << "\t\t" << 10.0*com[0] << " " << 10.0*com[1] << " " << 10.0*com[2] << " " << ligand_params[i].file << endl;
			fp_1.close();
		}
		else
		{
			sprintf(cadena,"%stxt/%s%s_%.3f_%.3f_%.3f.txt",param.output_dir,base,nombre_f,vectors_ini->ac_x,vectors_ini->ac_y,vectors_ini->ac_z);
                        ofstream fp_1 (cadena, ios::out | ios::trunc);
                        fp_1 << vectors_ini->energy.energy[act_c] << "\t\t" << com[0] << " " << com[1] << " " << com[2] << " " << ligand_params[i].file << endl;
			fp_1.close();
		}*/
		
		system(orden_1);

		free(ligando.lig_x);
		free(ligando.lig_y);
		free(ligando.lig_z);
		free(ligando.ligtype);	
		free(ligando.bonds);
		free(ligando.subtype);
		free(ligando.ql);
		//exit(0);
	}
					
}
