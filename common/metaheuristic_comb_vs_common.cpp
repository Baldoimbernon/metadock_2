#include <omp.h>
#include <thrust/sequence.h>
#include "energy_common.h"
#include "energy_CpuSolver_common.h"
#include "rotation.h"

////Devuelve un número aleatorio entre 1 y max
extern unsigned int getRandomNumber_modified_vs_warm (unsigned int n, unsigned int act)
{
	type_data tmp = act;
	while ((tmp == act ) || (tmp >= n))
	 tmp = (unsigned int)((n*2) * frand());
	return tmp;
}

extern void combinar_individual_vs_warm_up (unsigned int nlig, unsigned int stride_e, unsigned int stride_s, type_data *e_move_x, type_data *e_move_y, type_data *e_move_z, type_data *e_quat_x, type_data *e_quat_y, type_data *e_quat_z, type_data *e_quat_w, type_data *s_move_x, type_data *s_move_y, type_data *s_move_z, type_data *s_quat_x, type_data *s_quat_y, type_data *s_quat_z, type_data *s_quat_w , struct param_t param, unsigned int i, unsigned int j, unsigned int k, unsigned int l)
{
        type_data local_x, local_y, local_z, local_w, quat_tmp_x, quat_tmp_y, quat_tmp_z, quat_tmp_w;
        type_data angle,eje[3];

        unsigned int pos_o = i*stride_e + j;
        unsigned int pos_d = i*stride_e + k;
        unsigned int pos_s = i*stride_s + l;

        s_move_x[pos_s] = (e_move_x[pos_o] + e_move_x[pos_d])/2;
        s_move_y[pos_s] = (e_move_y[pos_o] + e_move_y[pos_d])/2;
        s_move_z[pos_s] = (e_move_z[pos_o] + e_move_z[pos_d])/2;

        quat_tmp_x = e_quat_x[pos_o];
        quat_tmp_y = e_quat_y[pos_o];
        quat_tmp_z = e_quat_z[pos_o];
        quat_tmp_w = e_quat_w[pos_o];
        angle = getRealRandomNumber(param.rotation);
        eje[0] = getRealRandomNumber (1);
        eje[1] = getRealRandomNumber (1);
        eje[2] = getRealRandomNumber (1);
        setRotation_cpp (&local_x, &local_y, &local_z, &local_w, angle, eje);
        composeRotation_cpp (&local_x, &local_y, &local_z, &local_w, &quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w, &quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w);
        normalize_cpp(&quat_tmp_x, &quat_tmp_y, &quat_tmp_z, &quat_tmp_w);
        s_quat_x[pos_s] = quat_tmp_x;
        s_quat_y[pos_s] = quat_tmp_y;
        s_quat_z[pos_s] = quat_tmp_z;
        s_quat_w[pos_s] = quat_tmp_w;
}


extern void combinar_vs_warm_up (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct vectors_t *vectors_e, struct param_t param, unsigned int hilos)
{

	struct vectors_t vectors_s;
	unsigned int mejores, peores, mejorespeores, stride_e,stride_s;
	unsigned int l = 0;
	unsigned int ind_m,ind_p,k,j;
	unsigned int base = 0;
	unsigned int div_m = param.conf_warm_up_cpu - 1;
	unsigned int div_p = param.conf_warm_up_cpu - 1;
	unsigned int div_mp = param.conf_warm_up_cpu - 1;
	
	mejores = param.conf_warm_up_cpu * (param.conf_warm_up_cpu - 1);	
	mejorespeores = param.conf_warm_up_cpu * (param.conf_warm_up_cpu - 1);	
	peores = param.conf_warm_up_cpu * (param.conf_warm_up_cpu - 1);
	stride_s = mejores + mejorespeores + peores;
	stride_e = param.conf_warm_up_cpu;
	
	vectors_s.files = vectors_e->files;

	vectors_s.nconformations = vectors_e->files * stride_s;
        vectors_s.conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_s.nlig*vectors_s.nconformations);
        vectors_s.conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_s.nlig*vectors_s.nconformations);
        vectors_s.conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_s.nlig*vectors_s.nconformations);

        vectors_s.ql = (type_data *)malloc(sizeof(type_data)*vectors_s.nlig*vectors_s.nconformations);
        vectors_s.ligtype = (char *)malloc(sizeof(char)*vectors_s.nlig*vectors_s.nconformations);
        vectors_s.subtype = (char *)malloc(sizeof(char)*vectors_s.nlig*SUBTYPEMAXLEN*vectors_s.nconformations);
        vectors_s.bonds = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s.nlig*MAXBOND*vectors_s.nconformations);
        vectors_s.nbonds = (char *)malloc(sizeof(char)*vectors_s.nlig*vectors_s.nconformations);

	vectors_s.move_x = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.move_y = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.move_z = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.energy.n_conformation = (unsigned int *)calloc(sizeof(unsigned int),vectors_s.nconformations);
	vectors_s.energy.energy = (type_data *)calloc(sizeof(type_data),vectors_s.nconformations);
	//thrust::sequence(vectors_s.energy.n_conformation,vectors_s.energy.n_conformation + vectors_s.nconformations);

	//printf("mejores %d peores %d mejorespeores %d\n",mejores,peores,mejorespeores);
	//exit(0);
	//omp_set_nested(1);

	omp_set_num_threads(hilos);
        #pragma omp parallel for
        for (unsigned int j=0;j<vectors_s.files;j++)
                for (unsigned int k=0;k<stride_s;k++)
                {
                        memcpy(vectors_s.conformations_x + j*stride_s*vectors_s.nlig + k*vectors_s.nlig,vectors_e->conformations_x + j*stride_e*vectors_s.nlig,vectors_s.nlig*sizeof(type_data));
                        memcpy(vectors_s.conformations_y + j*stride_s*vectors_s.nlig + k*vectors_s.nlig,vectors_e->conformations_y + j*stride_e*vectors_s.nlig,vectors_s.nlig*sizeof(type_data));
                        memcpy(vectors_s.conformations_z + j*stride_s*vectors_s.nlig + k*vectors_s.nlig,vectors_e->conformations_z + j*stride_e*vectors_s.nlig,vectors_s.nlig*sizeof(type_data));

                        memcpy(vectors_s.ql + j*stride_s*vectors_s.nlig + k*vectors_s.nlig,vectors_e->ql + j*stride_e*vectors_s.nlig,vectors_s.nlig*sizeof(type_data));
                        memcpy(vectors_s.ligtype + j*stride_s*vectors_s.nlig + k*vectors_s.nlig,vectors_e->ligtype + j*stride_e*vectors_s.nlig,vectors_s.nlig*sizeof(char));
                        memcpy(vectors_s.subtype + j*stride_s*SUBTYPEMAXLEN*vectors_s.nlig + k*vectors_s.nlig*SUBTYPEMAXLEN, vectors_e->subtype + j*stride_e*vectors_s.nlig*SUBTYPEMAXLEN, vectors_s.nlig*SUBTYPEMAXLEN*sizeof(char));
                        memcpy(vectors_s.bonds + j*stride_s*MAXBOND*vectors_s.nlig + k*vectors_s.nlig*MAXBOND, vectors_e->bonds + j*stride_e*vectors_s.nlig*MAXBOND, vectors_s.nlig*MAXBOND*sizeof(unsigned int));
                        memcpy(vectors_s.nbonds + j*stride_s*vectors_s.nlig + k*vectors_s.nlig, vectors_e->nbonds + j*stride_e*vectors_s.nlig, vectors_s.nlig*sizeof(char));
                }

        thrust::sequence(vectors_s.energy.n_conformation,vectors_s.energy.n_conformation + vectors_s.nconformations);

	omp_set_num_threads(hilos);
	#pragma omp parallel for
	for (unsigned int i = 0; i < vectors_s.files;i++)	
	{				
						
		//omp_set_num_threads(n2);
		//#pragma omp parallel for private(j,k)
		for (unsigned int m = base; m < (mejores + base); m++)
		{
			j = m / div_m;
			k = (div_m * (j+1)) - m;
			if (k <= j) k--;
			//printf("l = %d, j = %d, k = %d\n",m,j,k);			
			combinar_individual_vs_warm_up (vectors_s.nlig,stride_e,stride_s,vectors_e->move_x,vectors_e->move_y,vectors_e->move_z,vectors_e->quat_x,vectors_e->quat_y,vectors_e->quat_z,vectors_e->quat_w,vectors_s.move_x,vectors_s.move_y,vectors_s.move_z,vectors_s.quat_x,vectors_s.quat_y,vectors_s.quat_z,vectors_s.quat_w,param,i,j,k,m);
		}
		
		base =  param.conf_warm_up_cpu * ( param.conf_warm_up_cpu - 1);				
		//#pragma omp parallel for private(j,k)
		 for (unsigned int j = 0; j < param.conf_warm_up_cpu; j++)
                         for (unsigned int k = 0; k < param.conf_warm_up_cpu; k++)
                                if (k != j)
                                {
					combinar_individual_vs_warm_up (vectors_s.nlig,stride_e,stride_s,vectors_e->move_x,vectors_e->move_y,vectors_e->move_z,vectors_e->quat_x,vectors_e->quat_y,vectors_e->quat_z,vectors_e->quat_w,vectors_s.move_x,vectors_s.move_y,vectors_s.move_z,vectors_s.quat_x,vectors_s.quat_y,vectors_s.quat_z,vectors_s.quat_w,param,i,j+ param.conf_warm_up_cpu,k+ param.conf_warm_up_cpu,base);
					base++;
				}
		
		base = (param.conf_warm_up_cpu * ( param.conf_warm_up_cpu - 1)) + (param.conf_warm_up_cpu * ( param.conf_warm_up_cpu - 1));
		
		ind_m = getRandomNumber_modified_vs_warm((param.conf_warm_up_cpu-1),0);
		ind_p = getRandomNumber_modified_vs_warm((param.conf_warm_up_cpu-1),0);
	
		//#pragma omp parallel for private(j,k)
		for (unsigned int m = base; m < (mejorespeores + base); m++)
		{					
			combinar_individual_vs_warm_up (vectors_s.nlig,stride_e,stride_s,vectors_e->move_x,vectors_e->move_y,vectors_e->move_z,vectors_e->quat_x,vectors_e->quat_y,vectors_e->quat_z,vectors_e->quat_w,vectors_s.move_x,vectors_s.move_y,vectors_s.move_z,vectors_s.quat_x,vectors_s.quat_y,vectors_s.quat_z,vectors_s.quat_w,param,i,ind_m,ind_p+ param.conf_warm_up_cpu,m);
			ind_m = (ind_m + 1) %  param.conf_warm_up_cpu;
			ind_p = (ind_p + 1) %  param.conf_warm_up_cpu;
                        if ((ind_p % param.conf_warm_up_cpu) == 0) ind_p++;
			//printf("ind_m %d, ind_p %d, l: %d\n",ind_m,ind_p,l);
			//l++;						
		}		
					
	}
	free(vectors_s.move_x);
	free(vectors_s.move_y);
	free(vectors_s.move_z);
	free(vectors_s.quat_x);
	free(vectors_s.quat_y);
	free(vectors_s.quat_z);
	free(vectors_s.quat_w);
			
}
