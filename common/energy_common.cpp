#include <omp.h>
#include "energy_common.h"
#include "energy_struct.h"
#include "definitions.h"
#include "math.h"
using namespace std;

extern void replace_for_dqn(type_data **dqn_data, struct vectors_t *vectors_e, struct metaheuristic_t metaheuristic)
{
	type_data s_x, s_y, s_z;
	int pos_k;
	int pos = -1, pos_conformation;
	int tam = (metaheuristic.NEIIni * metaheuristic.DQNIni) / 100;
	int stride = metaheuristic.NEIIni - tam;

	for (int k = 0; k < (vectors_e->num_surface*tam);k+=tam)
	{
		s_x = dqn_data[k][0];
		s_y = dqn_data[k][1];
		s_z = dqn_data[k][2];
		pos = k/tam;
		for (int j = 0; j < vectors_e->num_surface; j++)
		{
			if (((vectors_e->surface_x[j]*10) == s_x) && ((vectors_e->surface_y[j]*10) == s_y) && ((vectors_e->surface_z[j]*10) == s_z)) 
			{
				pos = j;
				printf("encontrado\n");
				break;
			}
		}
		//if (pos != -1)
		//{
		pos_k = k;	
		pos_conformation = pos * metaheuristic.NEIIni + stride;
		for (int i = 0; i < tam; i++)
		{
			vectors_e->move_x[pos_conformation + i] = dqn_data[pos_k][3];	
			vectors_e->move_y[pos_conformation + i] = dqn_data[pos_k][4];
			vectors_e->move_z[pos_conformation + i] = dqn_data[pos_k][5];
			vectors_e->quat_x[pos_conformation + i] = dqn_data[pos_k][6];
			vectors_e->quat_y[pos_conformation + i] = dqn_data[pos_k][7];
			vectors_e->quat_z[pos_conformation + i] = dqn_data[pos_k][8];
			vectors_e->quat_w[pos_conformation + i] = dqn_data[pos_k][9];
			pos_k++;
		}
		//}		
	}

}
extern type_data **readDQNfile(char *dqnfile, struct vectors_t *vectors_e, struct metaheuristic_t metaheuristic)
{
	type_data **tmp;
	type_data s_x,s_y,s_z;
	FILE *fp;
	char cadena[500];
	int tam = (metaheuristic.NEIIni * metaheuristic.DQNIni) / 100;
	//printf("tam %d\n",tam);
        //printf("total %s\n",total);
	tmp = (type_data **)malloc((vectors_e->num_surface+1)*tam*sizeof(type_data *));
        for (int i = 0; i < ((vectors_e->num_surface+1)*tam); i++)
                tmp[i] = (type_data *)calloc(10,sizeof(type_data));

        if ((fp = fopen(dqnfile,"r")) == NULL) {
                printf("ReadInput(): Can't open dqnfile \"%s\"\n", dqnfile);
                exit(1);
        }
	//fscanf(fp,"%s", cadena);
	//for (int k=0; k < 1;k++)
	for (int k=0; k < vectors_e->num_surface;k++)
	{
		fscanf(fp,"%f %f %f", &s_x, &s_y, &s_z);
		//printf("%f %f %f\n",s_x,s_y,s_z);
		//exit(0);
		for (int l=0; l < tam; l++)
		{
			tmp[k*tam + l][0] = s_x; tmp[k*tam + l][1] = s_y; tmp[k*tam + l][2] = s_z;
	        	fscanf(fp,"%f %f %f %f %f %f %f", &tmp[k*tam + l][3], &tmp[k*tam + l][4], &tmp[k*tam + l][5], &tmp[k*tam + l][6], &tmp[k*tam + l][7],  &tmp[k*tam + l][8], &tmp[k*tam + l][9]);
		}
	}
	//printf("Fin\n");
	return tmp;
	
}
extern double calc_ddd_Mehler_Solmajer( type_data distance, double approx_zero )
{
    const double lambda = 0.003627L;
    const double epsilon0 = 78.4L;
    const double A = -8.5525L;
    const double B = epsilon0 - A;
    const double rk= 7.7839L;
    const double lambda_B = -lambda * B;

    const double epsilon = A + B / (1.0L + rk*exp(lambda_B * distance));

    if (epsilon < approx_zero) {
        return 1.0L;
    }
    else {
        return epsilon;
    }
}

extern void fill_conformations_flex (unsigned int nlig, type_data *e_conformations_x, type_data *e_conformations_y, type_data *e_conformations_z, type_data *e_move_x, type_data *e_move_y, type_data *e_move_z, type_data *e_quat_x, type_data *e_quat_y, type_data *e_quat_z, type_data *e_quat_w, type_data *s_conformations_x, type_data *s_conformations_y, type_data *s_conformations_z, type_data *s_move_x, type_data *s_move_y, type_data *s_move_z, type_data *s_quat_x, type_data *s_quat_y, type_data *s_quat_z, type_data *s_quat_w, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic)
{
        unsigned int th1;
        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 0:
                case 3:
                case 2:
                        th1 = metaheuristic.Threads1Ini;
                        break;
        }
        omp_set_num_threads (th1);
        #pragma omp parallel for
        for (unsigned int i = 0;i < nconformations; i++)
        {
                        memcpy(s_conformations_x + i*nlig, e_conformations_x + i*nlig, nlig * sizeof(type_data));
                        memcpy(s_conformations_y + i*nlig, e_conformations_y + i*nlig, nlig * sizeof(type_data));
                        memcpy(s_conformations_z + i*nlig, e_conformations_z + i*nlig, nlig * sizeof(type_data));
                        memcpy(s_move_x + i, e_move_x + i,sizeof(type_data));
                        memcpy(s_move_y + i, e_move_y + i,sizeof(type_data));
                        memcpy(s_move_z + i, e_move_z + i,sizeof(type_data));
                        memcpy(s_quat_x + i, e_quat_x + i,sizeof(type_data));
                        memcpy(s_quat_y + i, e_quat_y + i,sizeof(type_data));
                        memcpy(s_quat_z + i, e_quat_z + i,sizeof(type_data));
                        memcpy(s_quat_w + i, e_quat_w + i,sizeof(type_data));
        }
}

extern void fill_conformations_environment_flex (unsigned int nlig, type_data *e_conformations_x, type_data *e_conformations_y, type_data *e_conformations_z, type_data *e_move_x, type_data *e_move_y, type_data *e_move_z, type_data *e_quat_x, type_data *e_quat_y, type_data *e_quat_z, type_data *e_quat_w, type_data *s_conformations_x, type_data *s_conformations_y, type_data *s_conformations_z, type_data *s_move_x, type_data *s_move_y, type_data *s_move_z, type_data *s_quat_x, type_data *s_quat_y, type_data *s_quat_z, type_data *s_quat_w, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic)
{
        unsigned int th1;
        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 0:
                case 3:
                case 2:
                        th1 = metaheuristic.Threads1Ini;
                        break;
        }
        omp_set_num_threads (th1);
        #pragma omp parallel for
        for (unsigned int i = 0;i < nconformations; i++)
        {
                for (int j=0;j<metaheuristic.NEEImp;j++)
                {
			memcpy(s_conformations_x + i*metaheuristic.NEEImp*nlig + j*nlig, e_conformations_x + i*nlig, nlig * sizeof(type_data));
			memcpy(s_conformations_y + i*metaheuristic.NEEImp*nlig + j*nlig, e_conformations_y + i*nlig, nlig * sizeof(type_data));
			memcpy(s_conformations_z + i*metaheuristic.NEEImp*nlig + j*nlig, e_conformations_z + i*nlig, nlig * sizeof(type_data));
                        memcpy(s_move_x + i*metaheuristic.NEEImp + j, e_move_x + i,sizeof(type_data));
                        memcpy(s_move_y + i*metaheuristic.NEEImp + j, e_move_y + i,sizeof(type_data));
                        memcpy(s_move_z + i*metaheuristic.NEEImp + j, e_move_z + i,sizeof(type_data));
                        memcpy(s_quat_x + i*metaheuristic.NEEImp + j, e_quat_x + i,sizeof(type_data));
                        memcpy(s_quat_y + i*metaheuristic.NEEImp + j, e_quat_y + i,sizeof(type_data));
                        memcpy(s_quat_z + i*metaheuristic.NEEImp + j, e_quat_z + i,sizeof(type_data));
                        memcpy(s_quat_w + i*metaheuristic.NEEImp + j, e_quat_w + i,sizeof(type_data));
                }
        }
}

extern void fill_conformations_environment (type_data *e_move_x, type_data *e_move_y, type_data *e_move_z, type_data *e_quat_x, type_data *e_quat_y, type_data *e_quat_z, type_data *e_quat_w, type_data *s_move_x, type_data *s_move_y, type_data *s_move_z, type_data *s_quat_x, type_data *s_quat_y, type_data *s_quat_z, type_data *s_quat_w, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic)
{
        unsigned int th1;
        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 0:
                case 3:
                case 2:
                        th1 = metaheuristic.Threads1Ini;
                        break;
        }
        omp_set_num_threads (th1);
        #pragma omp parallel for
        for (unsigned int i = 0;i < nconformations; i++)
        {
		for (unsigned int j=0;j<metaheuristic.NEEImp;j++)
		{
			memcpy(s_move_x + i*metaheuristic.NEEImp + j, e_move_x + i,sizeof(type_data));                
			memcpy(s_move_y + i*metaheuristic.NEEImp + j, e_move_y + i,sizeof(type_data));
			memcpy(s_move_z + i*metaheuristic.NEEImp + j, e_move_z + i,sizeof(type_data));
			memcpy(s_quat_x + i*metaheuristic.NEEImp + j, e_quat_x + i,sizeof(type_data));
			memcpy(s_quat_y + i*metaheuristic.NEEImp + j, e_quat_y + i,sizeof(type_data));
			memcpy(s_quat_z + i*metaheuristic.NEEImp + j, e_quat_z + i,sizeof(type_data));
			memcpy(s_quat_w + i*metaheuristic.NEEImp + j, e_quat_w + i,sizeof(type_data));
		}
        }
}

extern void fill_conformations_environment_vs (unsigned int nlig, type_data *e_conformations_x, type_data *e_conformations_y, type_data *e_conformations_z, type_data *e_move_x, type_data *e_move_y, type_data *e_move_z, type_data *e_quat_x, type_data *e_quat_y, type_data *e_quat_z, type_data *e_quat_w, char *e_ligtype, type_data *e_ql, unsigned int *e_bonds, type_data *s_conformations_x, type_data *s_conformations_y, type_data *s_conformations_z, type_data *s_move_x, type_data *s_move_y, type_data *s_move_z, type_data *s_quat_x, type_data *s_quat_y, type_data *s_quat_z, type_data *s_quat_w,  char *s_ligtype, type_data *s_ql, unsigned int *s_bonds, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic)
{
        unsigned int th1;
        switch (param.mode) {
                case 1:
                        th1 = 1;
                        break;
                case 0:
                case 3:
                case 2:
                        th1 = metaheuristic.Threads1Ini;
                        break;
        }
        omp_set_num_threads (th1);
        #pragma omp parallel for
        for (unsigned int i = 0;i < nconformations; i++)
        {
                for (unsigned int j=0;j<metaheuristic.NEEImp;j++)
                {
			memcpy(s_conformations_x + (i * metaheuristic.NEEImp * nlig) + (j * nlig), e_conformations_x + (i * nlig),nlig * sizeof(type_data));
			memcpy(s_conformations_y + (i * metaheuristic.NEEImp * nlig) + (j * nlig), e_conformations_y + (i * nlig),nlig * sizeof(type_data));
			memcpy(s_conformations_z + (i * metaheuristic.NEEImp * nlig) + (j * nlig), e_conformations_z + (i * nlig),nlig * sizeof(type_data));
                        memcpy(s_move_x + i*metaheuristic.NEEImp + j, e_move_x + i,sizeof(type_data));
                        memcpy(s_move_y + i*metaheuristic.NEEImp + j, e_move_y + i,sizeof(type_data));
                        memcpy(s_move_z + i*metaheuristic.NEEImp + j, e_move_z + i,sizeof(type_data));
                        memcpy(s_quat_x + i*metaheuristic.NEEImp + j, e_quat_x + i,sizeof(type_data));
                        memcpy(s_quat_y + i*metaheuristic.NEEImp + j, e_quat_y + i,sizeof(type_data));
                        memcpy(s_quat_z + i*metaheuristic.NEEImp + j, e_quat_z + i,sizeof(type_data));
                        memcpy(s_quat_w + i*metaheuristic.NEEImp + j, e_quat_w + i,sizeof(type_data));
			memcpy(s_ql + (i * metaheuristic.NEEImp * nlig) + (j * nlig), e_ql + (i * nlig),nlig * sizeof(type_data));
			memcpy(s_ligtype + (i * metaheuristic.NEEImp * nlig) + (j * nlig), e_ligtype + (i * nlig),nlig * sizeof(char));
			memcpy(s_bonds + (i * metaheuristic.NEEImp * nlig * MAXBOND) + (j * nlig * MAXBOND), e_bonds + (i * nlig * MAXBOND), nlig * MAXBOND * sizeof(unsigned int));
                }
        }
}

extern void comprobation_params (struct param_t Param, struct metaheuristic_t Metaheuristic)
{
	bool exit_p = false;
	if ((Param.auto_flex < 0) || (Param.auto_flex > 1)) { exit_p=true; printf("Error al seleccionar RIGIDO o FLEXIBLE\n"); }
	if ((Param.tipo_computo < 0) || (Param.tipo_computo > 1)) { exit_p=true; printf("Error al seleccionar Tipo de computo\n"); }
	if ((Param.ngpu < 0)) { exit_p=true; printf("Error al seleccionar número de DEVICE\n"); }
	if ((Param.optimizacion < 0) || (Param.optimizacion > 1)) { exit_p=true; printf("Error al seleccionar Optimizacion\n"); }
	if ((Param.reparto_carga < 0) || (Param.reparto_carga > 1)) { exit_p=true; printf("Error al seleccionar Reparto de carga\n"); }
	if ((Param.conf_warm_up_gpu < 0) || (Param.conf_warm_up_cpu < 0)) { exit_p=true; printf("Error al seleccionar conformaciones para warm-up en GPU o CPU\n"); }
	if ((Param.por_conf_warm_up_sel < 0) || (Param.por_conf_warm_up_sel > 100)) { exit_p=true; printf("Error al seleccionar conformaciones para warm-up en fase Seleccion\n"); }
	if ((Param.por_conf_warm_up_inc < 0) || (Param.por_conf_warm_up_inc > 100)) { exit_p=true; printf("Error al seleccionar conformaciones para warm-up en fase de Inclusion\n"); }
		if ((Param.limit < 64) || (Param.limit > 1024)) { exit_p=true; printf("Error al seleccionar limite de tamaño de bloque.\n"); }
	if (Param.steps_warm_up_gpu < 0) { exit_p=true; printf("Error al seleccionar número de pasadas para calentamiento\n"); }
	if ((strlen(Param.openmp) == 0) || (strlen(Param.gpu) == 0) || (strlen(Param.multigpu) == 0)) { exit_p=true; printf("Error al escribir nombre de ficheros para guardar optimizaciones.\n"); }
	if ((Param.alarma < 0) || (Param.alarma > 1)) { exit_p=true; printf("Error al seleccionar fin por tiempo.\n"); }
	if (Param.alarma == 1) if (Param.duracion_alarma < 1) { exit_p=true; printf("Error al seleccionar tiempo de alarma.\n"); }
	if ((Param.scoring_function_type < 0) || (Param.scoring_function_type > 2)) { exit_p=true; printf("Error al seleccionar el tipo de función de scoring.\n"); }
	//if ((strlen(Param.vdw_p) == 0) || (strlen(Param.sasa_p) == 0) || (strlen(Param.hbond_p) == 0)) { exit_p=true; printf("Error al introducir ficheros de parametros.\n"); }
	//printf("Param.level_bonds_VDW %d\n",Param.level_bonds_VDW);
	if (Param.level_bonds_VDW < 0.0) { exit_p=true; printf("Error al especificar separaciones de enlaces para energia interna. \n"); }
	if ((Param.flex_angle < 1) || (Param.flex_angle > 360)) { exit_p=true; printf("Error al especificar angulo para flexibilidad.\n"); }
	if ((Param.automatic_seed < 0) || (Param.automatic_seed > 1)) { exit_p=true; printf("Error al especificar tipo de semilla.\n"); }
	if ((Param.automatic_seed == 0) && (Param.seed < 1)) { exit_p=true; printf("Error al especificar la semilla.\n"); }
	if ((Param.max_desp < 0) || (Param.max_desp > 0.5)) { exit_p=true; printf("Error al especificar rango de movimientos para generar o mejorar conformaciones\n"); }
	if ((Param.rotation < 1) || (Param.rotation > 360)) { exit_p=true; printf("Error al especificar angulo de rotación de conformaciones.\n"); }	
	if ((Param.rotation_mu < 1) || (Param.rotation_mu > 360)) { exit_p=true; printf("Error al seleccionar angulo de rotacion para mutacion.\n"); }
	if ((Param.montecarlo < 0) || (Param.montecarlo > 1)) { exit_p=true; printf("Error al especificar tipo de mejora.\n"); }

	if ((Metaheuristic.NEEImp < 0) && (Metaheuristic.NEEImp > 10)) { exit_p=true; printf("Error en parametro de número de elementos para entorno en mejoras.\n"); }
	if ((Metaheuristic.IMEFlex < 0) || (Metaheuristic.IMEFlex > 1000)) { exit_p=true; printf("Error en parametro de número de intensificación de flexibilidad exclusiva.\n"); }
	if ((Metaheuristic.NEFMIni < 0) || (Metaheuristic.NEFMIni > 100)) { exit_p=true; printf("Error en parametro de NEFMIni.\n"); }
        if ((Metaheuristic.NEFPIni < 0) || (Metaheuristic.NEFPIni > 100)) { exit_p=true; printf("Error en parametro de NEFPIni.\n"); }
	if (Metaheuristic.NEIIni < 10) { exit_p=true; printf("Error en conformaciones iniciales insuficientes.\n"); }
	if ((Metaheuristic.IMEIni < 0) ||(Metaheuristic.IMEIni > 1000)) { exit_p=true; printf("Error en parametro de IMEIni.\n"); }
	if ((Metaheuristic.IMEImp < 0) ||(Metaheuristic.IMEImp > 1000)) { exit_p=true; printf("Error en parametro de IMEImp.\n"); }
	if ((Metaheuristic.IMEMUCom < 0) ||(Metaheuristic.IMEMUCom > 1000)) { exit_p=true; printf("Error en parametro de IMEMUCom.\n"); }
	if (Metaheuristic.NEFIni > Metaheuristic.NEIIni) { exit_p=true; printf("Error en parametro de NEFIni.\n"); }
	if ((Metaheuristic.PEMIni < 0) || (Metaheuristic.PEMIni > 100)) { exit_p=true; printf("Error en parametro de PEMIni.\n"); }
	if ((Metaheuristic.NEMSel < 0) || (Metaheuristic.NEMSel > 100)) { exit_p=true; printf("Error en parametro de NEMSel.\n"); }
	if ((Metaheuristic.NEPSel < 0) || (Metaheuristic.NEPSel > 100)) { exit_p=true; printf("Error en parametro de NEPSel.\n"); }
	if ((Metaheuristic.NMMCom < 0) || (Metaheuristic.NMMCom > 100)) { exit_p=true; printf("Error en parametro de NMMCom.\n"); }
	if ((Metaheuristic.NMPCom < 0) || (Metaheuristic.NMPCom > 100)) { exit_p=true; printf("Error en parametro de NMPCom.\n"); }
	if ((Metaheuristic.NPPCom < 0) || (Metaheuristic.NPPCom > 100)) { exit_p=true; printf("Error en parametro de NPPCom.\n"); }
	if ((Metaheuristic.PEMUCom < 0) || (Metaheuristic.PEMUCom > 100)) { exit_p=true; printf("Error en parametro de PEMUCom.\n"); }
	if ((Metaheuristic.PEMSel < 0) || (Metaheuristic.PEMSel > 100)) { exit_p=true; printf("Error en parametro de PEMImp.\n"); }
	if ((Metaheuristic.NEMInc < 0) || (Metaheuristic.NEMInc > 100)) { exit_p=true; printf("Error en parametro de NEMInc.\n"); }
	if ((Metaheuristic.NIRFin < 0) || (Metaheuristic.NMIFin < 0)) { exit_p=true; printf("Error en parametro de NIRFin o NMIFin.\n"); }

	if ((Metaheuristic.Threads1Ini < 1) || ( Metaheuristic.Threads1Sel < 1) || (Metaheuristic.Threads1Fit < 1) || (Metaheuristic.Threads1Com < 1) || (Metaheuristic.Threads2Com < 1) || (Metaheuristic.Threads1Imp < 1) || (Metaheuristic.Threads1Inc < 1)) if (Metaheuristic.IMEIni < 0) { exit_p=true; printf("Error en parametros de Hilos en OMP.\n"); }
	if ((Param.mode == 0) || (Param.mode == 3))
	{
	if ((Metaheuristic.ThreadsperblockFit < 64) || (Metaheuristic.ThreadsperblockFit > 1024)) { exit_p=true; printf("Valor incorrecto en Hilos/Bloques de Fitness.\n"); }
	if ((Metaheuristic.ThreadsperblockMoveIni < 64) || (Metaheuristic.ThreadsperblockMoveIni > 1024)) { exit_p=true; printf("Valor incorrecto en Hilos/Bloques de Move.\n"); }
	if ((Metaheuristic.ThreadsperblockRotIni < 64) || (Metaheuristic.ThreadsperblockRotIni > 1024)) { exit_p=true; printf("Valor incorrecto en Hilos/Bloques de Rot.\n"); }
	if ((Metaheuristic.ThreadsperblockQuatIni < 64) || (Metaheuristic.ThreadsperblockQuatIni > 1024)) { exit_p=true; printf("Valor incorrecto en Hilos/Bloques de Quat.\n"); }
	if ((Metaheuristic.ThreadsperblockRandomIni < 64) || (Metaheuristic.ThreadsperblockRandomIni > 1024)) { exit_p=true; printf("Valor incorrecto en Hilos/Bloques de Random.\n"); }
	if ((Metaheuristic.ThreadsperblockMoveImp < 64) || (Metaheuristic.ThreadsperblockMoveImp > 1024)) { exit_p=true; printf("Valor incorrecto en Hilos/Bloques de Move Imp.\n"); }
	if ((Param.auto_max_threads < 0) || (Param.auto_max_threads > 1)) { exit_p=true; printf("Valor incorrecto en Eleccion automatica de hilos.\n"); }
	}
	if (((Metaheuristic.NMMCom_selec < 2) && (Metaheuristic.NMMCom > 0))  || ((Metaheuristic.NMPCom_selec < 2) && (Metaheuristic.NMPCom > 0))  || ((Metaheuristic.NPPCom_selec < 2) && (Metaheuristic.NPPCom > 0))  && (Metaheuristic.NMIFin > 0)) { exit_p=true; printf("Error COMBINACION SELECCIONADA y NO es posible combinar.\n"); }

	if (exit_p)
	{
		printf("\n\t******ERROR IN RANKS OF PARAMS******\n");
		printf("\t******EXIT PROGRAM******\n");
		exit(1);
	} 
	
}
int search (char *filename,char *total)
{
	char tmp[5];
	unsigned int nAtom, nBond, aux;
        //Auxiliar variables to read useless information
	type_data lx,ly,lz,ch;
        char cadena[20],cadena1[20],cadena2[20],auxtipo[5];

	FILE *fp;
	//printf("total %s\n",total);
        if ((fp = fopen(filename,"r")) == NULL) {
                printf("ReadInput(): Can't open file \"%s\"\n", filename);
                exit(1);
        }

	 if (skip_to_token(fp,"@<TRIPOS>MOLECULE")) {
                exit(1);
                return (-1);
        }
        fscanf(fp,"%s", cadena);
        fscanf(fp,"%u %u", &nAtom, &nBond);


        if (skip_to_token(fp,"@<TRIPOS>ATOM")) {
                exit(1);
                return (-1);
        }

        for (unsigned int i=0; i < nAtom; i++){
                //Read type of atom
                fscanf(fp, "%u %s %f %f %f %s %s %s %f",&aux,auxtipo,&lx,&ly,&lz,cadena,cadena1,cadena2,&ch);
		if (strcmp(auxtipo,total) == 0)
			return (aux);
	}
	fclose(fp);
	printf("ERROR search rotatable bonds\n");
	exit(-1);
}	

void extract_number_type (char *input,char *type_atom_number)
{
	int c=0,i=0;
        int len = strlen(input);

	while((c < len) && (input[c] != '_'))
        {
                type_atom_number[i] = input[c];
                i++;c++;
        }
        type_atom_number[i]='\0';
}

int extract_link (char *input)
{
	int c=0,i=0;
	int len = strlen(input);
	char tmp[5];
	while (input[c] != '_') c++;
	c++;
	//printf("c %d %d\n",c,len);
	while(c < len)
	{
		tmp[i] = input[c];
		i++;c++;
	}
	tmp[i]='\0';
	//printf("tmp %s\n",tmp);
	return (atoi(tmp));
}

extern void read_pdbqt (struct param_t param, char *filename, unsigned int *nlinks, int *&n_links)
{
        ifstream file;
	FILE *fp;
        char cadena[200], cadena1[50], cadena2[50], cadena3[50], cadena4[50], cadena5[50], cadena6[50], cadena7[50];
	char nombre[100], total[10];
        char tmp[7];

        unsigned int n_l = 0;
	bool enlaces = false;
        int i,c,l1,l2;
        char ext[6]="pdbqt";
        char new_file[200];

	strcpy(new_file,param.pdbqt);

        int len = strlen(filename);
        c = len - 1;
        while (filename[c] != '/') c--;
        c++;
        i = 0;
        while (filename[c] != '.') { nombre[i] = filename[c]; i++;c++; }
	nombre[i]='.';
	i++;
	nombre[i]='\0';
        strcat(nombre,ext);
        strcat(nombre,"\0");
	strcat(new_file,nombre);
	strcat(new_file,"\0");

	
        //if ((fp =  fopen(new_file,"r")) == NULL) {
        //        printf("ReadInput(): Can't open file \"%s\"\n",new_file);
        //        exit(1);
        //}
        //printf("new_file %s\n",new_file);
        //exit(0);
	

	file.open(new_file, ios::in);
	//fscanf(fp,"%s %s %s",cadena,cadena1,cadena2);
	//printf("cadena %s cadena1 %s cadena2 %s\n",cadena,cadena1,cadena2);
	file >> cadena >> cadena1 >> cadena2;
	//printf("cadena %s \n",cadena);
	file.ignore(200,'\n');
        while( (!file.eof()) && (!enlaces))
        {
                //printf("tmp %s\n",tmp);
                if (strcmp(cadena2,"active") == 0)
		{
			n_l = atoi(cadena1);
			enlaces = true;
		}
		//fscanf(fp,"%s %s %s",cadena,cadena1,cadena2);
		file >> cadena >> cadena1 >> cadena2;
		file.ignore(200,'\n');
		//printf("cadena %s cadena1 %s cadena2 %s\n",cadena,cadena1,cadena2);
		//printf("cadena %s \n",cadena);
        }
        //printf("nlinks %d\n",n_l);
        n_links = (int *)calloc(n_l * 2,sizeof(int));
	//exit(0);	
	//fscanf(fp,"%s %s %s %s %s %s %s %s \n",cadena,cadena1,cadena2,cadena3,cadena4,cadena5,cadena6,cadena7);
	//printf("cadena %s cadena1 %s cadena2 %s\n",cadena,cadena1,cadena2);

	int h=0;
	int count = 1;
	int tmp_count;
	int num;

	file >> cadena >> cadena1;// >> cadena2 >> cadena3 >> cadena4 >> cadena5 >> cadena6 >> cadena7;
	while ( (!file.eof()) && (count != (n_l+1)))
        {
		if (strcmp(cadena1,"I") != 0)
			tmp_count = atoi(cadena1);
			//printf("cadena1 %s\n",cadena1);
			if (tmp_count == count)
			{
				file >> cadena2 >> cadena3 >> cadena4 >> cadena5 >> cadena6 >> cadena7;
				//printf("cadena5 %s cadena7 %s\n",cadena5,cadena7);
				//num = extract_link(cadena5);
				//printf("num %d\n",num);
				extract_number_type(cadena5,total);
				//num = search(filename,total);
				//printf("num %d ",num);
				n_links[h] = search(filename,total); //extract_link(cadena5);
				//n_links[h] = extract_link(cadena5);
				h++;
				extract_number_type(cadena7,total);
                        	//num = search(filename,total);
				//printf(" %d \n",num);
				n_links[h] = search(filename,total); //extract_link(cadena7);
			        //n_links[h] = extract_link(cadena7);
				h++;count++;
				//printf("count %d\n",count);
			}
			else
			{
				file.ignore(200,'\n');
			}
			file >> cadena >> cadena1;// >> cadena2 >> cadena3 >> cadena4 >> cadena5 >> cadena6 >> cadena7;
	}
	//for (int k=0;k < n_l * 2;k+=2) printf("enlace %d con %d \n",n_links[k],n_links[k+1]); 
        file.close();
	*nlinks = n_l;

        //exit(0);
}

extern void force_field_read (char *input, type_data *&weights, struct force_field_param_t * f_params, int scoring_type) {

        float f1,f2,f3,f4,f5,f6,f7;
        int type,factor=1;
        char header1[100];

        FILE *fp;
        if ((fp =  fopen(input,"r")) == NULL) {
                printf("ReadInput(): Can't open file \"%s\"\n", input);
                exit(1);
        }
	
	if (scoring_type != S_AD4) factor=1000;
	weights = (type_data *)calloc(SCORING_TERMS,sizeof(type_data));

        for (int i1=0;i1 < 22;i1++) {
                f_params[i1].sigma = 0;
                f_params[i1].epsilon = 0;
                f_params[i1].asp = 0;
                f_params[i1].vol = 0;
                f_params[i1].epsilon_h = 0;
                f_params[i1].sigma_h = 0;
        }
	
	if (skip_to_token(fp,"@<WEIGHTS>")) {
                exit(1);
                return;
        }

        fscanf(fp,"%s %f", header1,&f1);
        fscanf(fp,"%s %f", header1,&f2);
        fscanf(fp,"%s %f", header1,&f3);
        fscanf(fp,"%s %f", header1,&f4);
        fscanf(fp,"%s %f", header1,&f5);

        weights[0] = f1;
        weights[1] = f2;
        weights[2] = f3;
        weights[3] = f4;
        weights[4] = f5;

	//printf("p_vdw %f , p_hbond %f , p_estat %f , p_desolv %f , p_tor %f",weights[0],weights[1],weights[2],weights[3],weights[4]);

	if (skip_to_token(fp,"@<ATOMS>")) {
                exit(1);
                return;
        }
        while (!feof(fp))
        {
                fscanf(fp,"%s %f %f %f %f %f %f", header1,&f1,&f2,&f3,&f4,&f5,&f6);
                type = getTypeNumber(header1);
                //printf("atom type %s %d\n",header1,type);
                f_params[type].sigma = f1;
                f_params[type].epsilon = f2;
                f_params[type].vol = f3/factor;
                f_params[type].asp = f4/factor;
                f_params[type].sigma_h = f5;
                f_params[type].epsilon_h = f6;
        }

        f_params[HBOND] = f_params[HID];
        f_params[NIT2] = f_params[NIT];
	fclose(fp);
}

extern void secuenciar (unsigned int *n_conformations, unsigned int n)
{
	for (unsigned int i=0;i < n;i++)
		n_conformations[i] = i;
}


char* getTypeString (char tipo, char * string){

	switch(tipo){

		case CAR: strcpy(string,"C");
			break;
		case HID: strcpy(string,"H");
			break;
		case OXI: strcpy(string,"O");
			break;
		case AZU: strcpy(string,"S");
			break;
		case NIT: strcpy(string,"N");
			break;
		case NIT2:strcpy(string,"N2");
			break;
		case HBOND: strcpy(string,"H"); //Se pierde el tipo
			break;
		case IRI: strcpy(string,"I");
			break;
		case CLO: strcpy(string,"Cl");
			break;
		case FLU: strcpy(string,"F");
			break;
		case BRO: strcpy(string,"Br");
			break;
		case LIT: strcpy(string,"Li");
			break;
		case SOD: strcpy(string,"Na");
			break;
		case POT: strcpy(string,"K");
			break;
		case MAG: strcpy(string,"Mg");
			break;
		case CAL: strcpy(string,"Ca");
			break;
		case ZIN: strcpy(string,"Zn");
			break;
		case AR: strcpy(string,"C.ar");
                        break;
                case FER: strcpy(string,"Fe");
                        break;
                case PH: strcpy(string,"P");
                        break;
		case DUM: strcpy(string,"C");
			break;
	}
	return string;
}

extern int getTypeNumber (char* cadena){

	if ((cadena[0] == 'H' || cadena[0] == 'h')	//HIDROGENO
		&& (cadena[1] != 'G' && cadena[1] != 'g')){ //No es mercurio
		return HID;
	}
	else if (cadena[0] == 'C' || cadena[0] == 'c'){

                if (cadena[1] == 'L' || cadena[1] == 'l')
                        return CLO;     //CLORO
                else if (cadena[1] == 'A' || cadena[1] == 'a')
                        return CAL;     //CALCIO
                else if (cadena[1] == '.' && cadena[2] == 'a')
                        {
                        //printf("AR\n");
                        return AR;
                        }
                return CAR;
        }
        else if (cadena[0] == 'A' || cadena[0] == 'a')
                return AR;
	else if (cadena[0] == 'O' || cadena[0] == 'o') //OXIGENO
		return OXI;
	else if (cadena[0] == 'S' || cadena[0] == 's') //AZUFRE
		return AZU;
	else if (cadena[0] == 'N' || cadena[0] == 'n'){ //NITROGENO
		if (cadena[1] == 'A' || cadena[1] == 'a')
			return SOD;
		return NIT;
		}
	else if (cadena[0] == 'I' || cadena[0] == 'I') //IRIDIO
		return IRI;
	 else if (cadena[0] == 'F' || cadena[0] == 'f') {
                if (cadena[1] == 'e' || cadena[1] == 'E')
                        return FER;//HIERRO
                return FLU; //FLUOR
		}
	else if ((cadena[0] == 'B' || cadena[0] == 'b') && //BROMO
		(cadena[1] == 'R' || cadena[1] == 'r'))
		return BRO;
	else if ((cadena[0] == 'L' || cadena[0] == 'l') && //LITIO
		(cadena[1] == 'I' || cadena[1] == 'i'))
		return LIT;

	else if (cadena[0] == 'K' || cadena[0] == 'k') //POTASIO
		return POT;
	else if ((cadena[0] == 'M' || cadena[0] == 'm') && //MAGNESIO
		(cadena[1] == 'G' || cadena[1] == 'g'))
		return MAG;
	else if ((cadena[0] == 'Z' || cadena[0] == 'z') && //ZINC
		(cadena[1] == 'N' || cadena[1] == 'n'))
		return ZIN;
	else if ((cadena[0] == 'P' || cadena[0] == 'p'))
                return PH;
	else{
		return DUM; //DUMMY
	}

}

/*extern void read_params (char *filename_w, char *filename_s, char *filename_h, struct vdw_param_t * vdw_params, struct sasa_param_t * sasa_params, struct hbond_param_t * hbond_params){

	char cadena[50];
	unsigned int type;

	ifstream file;
	file.open (filename_w, ifstream::in );

	if (!file.good()) {
		cout << "can't open file " << filename_w << endl;
		cout << "Will not read vdw parameters file" << endl;
	}
	for (unsigned int i1=0;i1 < 20;i1++) {
		vdw_params[i1].sigma = 0;
		vdw_params[i1].epsilon = 0;
		sasa_params[i1].radius = 0;
		sasa_params[i1].sigma = 0;
		hbond_params[i1].c10 = 0;
		hbond_params[i1].c12 = 0;
	}
	file.getline(cadena,50);
	file >> cadena;

	while(!file.eof()){
		type = getTypeNumber(cadena);
		file >> vdw_params[type].sigma >> vdw_params[type].epsilon;
		//printf("vdw_params %d eps %f sig %f\n",type,vdw_params[type].epsilon,vdw_params[type].sigma);
		file >> cadena;
	}
	file.close();
	
	file.open (filename_s, ifstream::in );	
	
	if (!file.good()) {
		cout << "can't open file " << filename_s << endl;
		cout << "Will not read sasa parameters file" << endl;
	}	
	file.getline(cadena,50);
	file >> cadena;
	
	while(!file.eof()){

		type = getTypeNumber(cadena);
		file >> sasa_params[type].radius >> sasa_params[type].sigma;
		file >> cadena;
	}
	file.close();
	
	vdw_params[HBOND] = vdw_params[HID];
	sasa_params[HBOND] = sasa_params[HID];
	vdw_params[NIT2] = vdw_params[NIT];
	sasa_params[NIT2] = sasa_params[NIT];
	
	file.open (filename_h, ifstream::in );
	
	if (!file.good()) {
		cout << "can't open file " << filename_h << endl;
		cout << "Will not read hbond parameters file" << endl;

	}
	
	file.getline(cadena,50);
	file >> cadena;

	while(!file.eof()){

		type = getTypeNumber(cadena);
		if (type == NIT) type = NIT2;
		file >> hbond_params[type].c10 >> hbond_params[type].c12;
		file >> cadena;
	}
	file.close();
	//for (int i=0;i<20;i++) printf("hb.c10[%d] %f hb.c12[%d] %f\n",i,hbond_params[i].c10,i,hbond_params[i].c12);
}*/

//Calcula la media de las posiciones de los atomos del ligando
extern void getCOM (type_data * lig_x, type_data * lig_y, type_data * lig_z, unsigned int nAtom, type_data *com){

	com[0] = 0;
	com[1] = 0;
	com[2] = 0;

	for (unsigned int i=0; i < nAtom; i++){
		//printf("lig_y[%d] %f\n",i,lig_y[i]);
		com[0] += lig_x[i];
		com[1] += lig_y[i];
		com[2] += lig_z[i];
	}

	com[0] /= nAtom;
	com[1] /= nAtom;
	com[2] /= nAtom;
}


//Devuelve el número de atomos que se deben añadir a la proteina para ser multiplo de blockSize
extern unsigned int getPadding (unsigned int nrec, unsigned int blockSize){

	if (nrec % blockSize == 0)
		return 0;

	return (nrec / blockSize + 1) * blockSize - nrec;

}


void fillPadding (struct receptor_t * proteina, unsigned int padding){
	proteina->atoms = proteina->nrec;

	for (int i=proteina->nrec ; i < proteina->nrec + padding ; i++){
		proteina->qr[i] = 0.0;
		proteina->rectype[i] = 18;
		proteina->rec_x[i]  = 0.0;
		proteina->rec_y[i]= 0.0;
		proteina->rec_z[i]= 0.0;
	}
	proteina->nrec += padding;

}


//Extrae información de la proteina (carga, posición, tipo de átomo,...)
void splitProtein (Protein_entry* Protein, struct receptor_t &proteina, unsigned int nrec){

     proteina.nrec = nrec;
	unsigned int tipo_enlace;

	for (unsigned int i=0; i < nrec; i++){
		proteina.qr[i] = Protein[i].charge;
		proteina.rec_x[i] = Protein[i].pos[0];
		proteina.rec_y[i] = Protein[i].pos[1];
		proteina.rec_z[i] = Protein[i].pos[2];
		proteina.rectype[i] = Protein[i].vdwType;

          proteina.nbonds[i] = Protein[i].nBond;
          for(unsigned int j=0; j < Protein[i].nBond; j++)
               proteina.bonds[MAXBOND*i + j] = Protein[i].bond[j];

		tipo_enlace = proteina.rectype[proteina.bonds[MAXBOND*i]];
          if (proteina.rectype[i] == HID &&
			(tipo_enlace == OXI || tipo_enlace == NIT2 || tipo_enlace == AZU))
				proteina.rectype[i] = HBOND;
	}

}


extern void initProtein(char* protname, Protein_entry* &Protein, Bond_tp * &bond, unsigned int &nBond, unsigned int &nAtom, int scoring_function)
{

	unsigned int count, countBond, dum1, dum2, dum3, ind1, ind2;
	char *header2, *header3;
	char header1[100];

	type_data tot_charge = 0.0;
	unsigned int bonds[8];
	//ader1 = new char[HEADERLENGTH];
	//header2 = new char[HEADERLENGTH];
	//header3 = new char[HEADERLENGTH];


	//-----------------------------------------------------------
	// Definition of the object 'mol2file' of class ifstream,
	// ios::in means 'read only', ios::nocreate 'do not create
	// if not already there'
	//-----------------------------------------------------------


	FILE *fp;

	if ((fp = fopen(protname,"r")) == NULL) {
		printf("ReadInput(): Can't open file \"%s\"\n", protname);
		exit(1);
	}

//
	if (skip_to_token(fp,"@<TRIPOS>MOLECULE")) {
		exit(1);
		return;
	}
	fscanf(fp,"%s", header1);		
	fscanf(fp,"%u %u %u %u %u", &nAtom, &nBond, &dum1, &dum2,&dum3);


	Protein = (Protein_entry*) malloc (nAtom * sizeof(Protein_entry));
	bond = (Bond_tp*) malloc (nBond * sizeof(Bond_tp));

	if (skip_to_token(fp,"@<TRIPOS>ATOM")) {
		exit(1);
		return;
	}


	for (unsigned int k = 0; k < nAtom; k++) {
		//int k = i;
		fscanf(fp, "%u %s %f %f %f %s %u %s %f",
				&Protein[k].atomID , Protein[k].symbol, &Protein[k].pos[0],
				&Protein[k].pos[1],&Protein[k].pos[2],
				Protein[k].symbol2, &Protein[k].sgID,
				Protein[k].sgName, &Protein[k].charge);		
		if (scoring_function != S_AD4)
		{				
			Protein[k].pos[0] /= 10.0;
			Protein[k].pos[1] /= 10.0;
			Protein[k].pos[2] /= 10.0;
		}

		//-----------------------------------------------------------------
		//   Define the vdw-force field constants:
		//-----------------------------------------------------------------
		if (Protein[k].symbol2[0] == 'H' || Protein[k].symbol2[0] == 'h') {
			Protein[k].vdwType = HID;

		}
		else if (Protein[k].symbol2[0] == 'C' || Protein[k].symbol2[0] == 'c') {
			Protein[k].vdwType = CAR;
			if (Protein[k].symbol2[1] == 'L' || Protein[k].symbol2[1] == 'l')
				Protein[k].vdwType = CLO;
			else if (Protein[k].symbol2[1] == 'A' || Protein[k].symbol2[1] == 'a')
				Protein[k].vdwType = CAL;
		}

		else if (Protein[k].symbol2[0] == 'O' || Protein[k].symbol2[0] == 'o') {
			Protein[k].vdwType = OXI;

		}
		else if (Protein[k].symbol2[0] == 'S' || Protein[k].symbol2[0] == 's') {
			Protein[k].vdwType = AZU;

		}
		else if (Protein[k].symbol2[0] == 'N' || Protein[k].symbol2[0] == 'n') {
			Protein[k].vdwType = NIT;

		}
		else if (Protein[k].symbol2[0] == 'I' || Protein[k].symbol2[0] == 'i') {
			Protein[k].vdwType = IRI;

		}
		else if (Protein[k].symbol2[0] == 'F' || Protein[k].symbol2[0] == 'f') {
			Protein[k].vdwType = FLU;

		}
		else if (Protein[k].symbol2[0] == 'K' || Protein[k].symbol2[0] == 'k') {
			Protein[k].vdwType = POT;

		}
		else if ( (Protein[k].symbol2[0] == 'M' || Protein[k].symbol2[0] == 'm') &&
				 (Protein[k].symbol2[1] == 'G' || Protein[k].symbol2[1] == 'g')){
			Protein[k].vdwType = MAG;

		}
		else if ( (Protein[k].symbol2[0] == 'Z' || Protein[k].symbol2[0] == 'z') &&
				 (Protein[k].symbol2[1] == 'N' || Protein[k].symbol2[1] == 'n')){
			Protein[k].vdwType = ZIN;

		}
		else if ( (Protein[k].symbol2[0] == 'L' || Protein[k].symbol2[0] == 'l') &&
				 (Protein[k].symbol2[1] == 'I' || Protein[k].symbol2[1] == 'i')){
			Protein[k].vdwType = LIT;

		}
		else if ( (Protein[k].symbol2[0] == 'M' || Protein[k].symbol2[0] == 'm') &&
				 (Protein[k].symbol2[1] == 'G' || Protein[k].symbol2[1] == 'g')){
			Protein[k].vdwType = MAG;

		}
		else if ( (Protein[k].symbol2[0] == 'B' || Protein[k].symbol2[0] == 'b') &&
				 (Protein[k].symbol2[1] == 'R' || Protein[k].symbol2[1] == 'r')){
			Protein[k].vdwType = BRO;

		}
		else if ( (Protein[k].symbol2[0] == 'N' || Protein[k].symbol2[0] == 'n') &&
				 (Protein[k].symbol2[1] == 'A' || Protein[k].symbol2[1] == 'a')){
			Protein[k].vdwType = SOD;

		}
		else {
			Protein[k].vdwType = DUM;

		}

		//cout <<Protein[k].atomID <<"\t"<< Protein[k].symbol<<"\t"<< Protein[k].pos<< Protein[k].symbol2<<"\t"<<Protein[k].sgID<<"\t"<<Protein[k].sgName<<"\t"<<Protein[k].charge<<endl;
		if (skip_to_eol(fp)) {
			exit(1);
			return;
		}
		//for consistency with older versions
		strcpy(Protein[k].remark,"****");

		tot_charge += Protein[k].charge;
		Protein[k].externalSg = 0;
	}

	fscanf(fp,"%s", header1);
	
	for (unsigned int k = 0; k < nBond; k++) {
		fscanf(fp, "%u %u %u %s",
				&count, &bond[k].a, &bond[k].b,
				bond[k].type);
		
		if (skip_to_eol(fp)) {
			return;
		}
		//cout <<count<<"\t"<<bond[k].a<<"\t"<<bond[k].b<<"\t"<<bond[k].type<<endl;
	}

	fclose(fp);
	

	for (unsigned int j = 0; j < nAtom; j++) Protein[j].nBond = 0; //PUESTO POR MI
	//int ind1, ind2;

	unsigned int * enlaces_nit = (unsigned int*) calloc (nAtom, sizeof(unsigned int));

    	for (unsigned int k = 0; k < nBond; k++){

		ind1 = bond[k].a -1;
		ind2 = bond[k].b -1;

		Protein[ind1].bond[Protein[ind1].nBond] = ind2;
		Protein[ind2].bond[Protein[ind2].nBond] = ind1;

		Protein[ind1].nBond ++;
		Protein[ind2].nBond ++;

		//Calcula el número de enlaces de cada nitrógeno
		if (Protein[ind1].vdwType == NIT && isdigit(bond[k].type[0])){
			enlaces_nit[ind1] += atoi(bond[k].type);
		}

		if (Protein[ind2].vdwType == NIT && isdigit(bond[k].type[0])){
			enlaces_nit[ind2] += atoi(bond[k].type);
		}

     	}

	for (unsigned int l = 0; l < nAtom; l++){
		if(Protein[l].vdwType == NIT && enlaces_nit[l] <= 4)
			Protein[l].vdwType = NIT2;
	}

	free(enlaces_nit);

	for (unsigned int m = 0; m < nAtom; m++) {
		// cout << n <<"\t"<< Protein[n].symbol<<"\t"<<Protein[n].nBond<<endl;
		if (Protein[m].vdwType == HID){
			int bondTo = Protein[m].bond[0];
			int type2 = Protein[bondTo].vdwType;
			if((type2 == OXI) || (type2 == NIT2) || (type2 == AZU))
				Protein[m].vdwType = HBOND; // H-bond!
		}
	}

	for (unsigned int n = 0; n < nAtom; n++) {
		Protein[n].charge -= tot_charge/nAtom;
	}

}


extern void readProtein (Protein_entry * Protein, struct receptor_t &proteina, unsigned int &qrSize, unsigned int &recSize){

	unsigned int padding = getPadding (proteina.nrec, BLOCK_SIZE);

	qrSize = (proteina.nrec + padding) * sizeof(type_data);
	recSize = (proteina.nrec + padding)*sizeof(type_data);

	proteina.qr = (type_data*) malloc (qrSize);
	proteina.rec_x = (type_data*) malloc (recSize);
	proteina.rec_y = (type_data*) malloc (recSize);
	proteina.rec_z = (type_data*) malloc (recSize);
	proteina.rectype = (char *) malloc ((proteina.nrec + padding) * sizeof(char));
	proteina.bonds = (unsigned int*) malloc (proteina.nrec * MAXBOND * sizeof(unsigned int));
    	proteina.nbonds = (char*) malloc (proteina.nrec * sizeof(char));

	//Extrae de Protein, la informacion que necesitamos
	//splitProtein (Protein, proteina.qr, proteina.rec, proteina.rectype, proteina.nrec);
	splitProtein (Protein, proteina, proteina.nrec);

	fillPadding (&proteina, padding);
}


extern int skip_to_token(FILE* file,char* token)
{
	char c;
	unsigned int count=0;

	while ((c = fgetc(file)) != EOF)
	{
		if (c==token[count])
			count++;
		else
			count=0;
		if (count == strlen(token))
			return 0;
	}
	return 1;
}

extern int skip_to_eol(FILE* file)
{
	unsigned int c;
	unsigned int nl = '\n';

	while ((c = fgetc(file)) != EOF)
		if (c == nl) return 0;
	return 1;
}

extern void readLigand (char *filename, struct ligand_t *ligando, int scoring_function){

	unsigned int nAtom, nBond, aux,ind1, ind2,dum1,dum2,dum3;
     	type_data com[3];
	//Auxiliar variables to read useless information
	char cadena[20],cadena1[20],auxtipo[10];
	unsigned int * enlaces_nit;
	FILE *fp;

        if ((fp = fopen(filename,"r")) == NULL) {
                printf("ReadInput(): Can't open file \"%s\"\n", filename);
                exit(1);
        }

        if (skip_to_token(fp,"@<TRIPOS>MOLECULE")) {
                exit(1);
                return;
        }
        fscanf(fp,"%s", cadena);
        fscanf(fp,"%u %u", &nAtom, &nBond);

	ligando->type_json = (char **)malloc(nAtom*sizeof(char *));
	for (int i = 0; i < nAtom; i++) 
		ligando->type_json[i] = (char *)malloc(11*sizeof(char));
	//file >> cadena;
	//Read number of atoms
	//file >> nAtom >> nBond;
	//file >>  aux >> aux >> aux;
	//file >> cadena;
	//file >> cadena;
	//file >> cadena;
	
	//printf("Atom %d\n",nAtom);		
	
	ligando->nlig = nAtom;
	ligando->atoms = nAtom;

	ligando->nlig += getPadding(ligando->nlig, 64);

	//printf("nlig %d\n",ligando.nlig);
	ligando->ql = (type_data*) malloc(ligando->nlig * sizeof(type_data));
	ligando->lig_x = (type_data*) malloc(ligando->nlig * sizeof(type_data));
	ligando->lig_y = (type_data*) malloc(ligando->nlig * sizeof(type_data));
	ligando->lig_z = (type_data*) malloc(ligando->nlig * sizeof(type_data));
	ligando->ligtype = (char*) malloc (ligando->nlig * sizeof(char));
	ligando->subtype = (char*) malloc (ligando->nlig * SUBTYPEMAXLEN * sizeof(char));
	ligando->bonds = (unsigned int*) malloc (ligando->nlig * MAXBOND * sizeof(unsigned int));
    ligando->nbonds = (char*) calloc (ligando->nlig , sizeof(char));
	
	enlaces_nit = (unsigned int*) calloc (nAtom, sizeof(unsigned int));
	
	if (skip_to_token(fp,"@<TRIPOS>ATOM")) {
                exit(1);
                return;
        }
	
	for (unsigned int i=0; i < nAtom; i++){
		//Read type of atom
		fscanf(fp, "%u %s %f %f %f %s %s %s %f",&aux,auxtipo,&ligando->lig_x[i],&ligando->lig_y[i],&ligando->lig_z[i],&ligando->subtype[SUBTYPEMAXLEN*i],cadena,cadena1,&ligando->ql[i]);
		strcpy(ligando->type_json[i],auxtipo);
		//file >> aux >> auxtipo;
		//Read 3D position
		//file >> ligando->lig_x[i] >> ligando->lig_y[i] >> ligando->lig_z[i];
		//file >> &(ligando->subtype[SUBTYPEMAXLEN*i]) >> cadena >> cadena;
		//Read charge
		//file >> ligando->ql[i];
		//printf("x %f\n",ligando->lig_x[i]);
		if (scoring_function != S_AD4)
		{
			ligando->lig_x[i] /= 10.0;
			ligando->lig_y[i] /= 10.0;
			ligando->lig_z[i] /= 10.0;
		}
		ligando->ligtype[i] = getTypeNumber(auxtipo);
		//ligando->ligtype[i] = getTypeNumber(auxtipo);

	}
	//for (int i=0;i<nAtom;i++) printf("%s\n",ligando->type_json[i]);	
	//printf("x %f\n",ligando->lig_x[0]);
	//exit(0);
	//file.close();
	//if (scoring_function != S_AD4)
	//{
		getCOM (ligando->lig_x,ligando->lig_y,ligando->lig_z, nAtom, com);

		for (unsigned int i=0; i < nAtom; i++){
			ligando->lig_x[i] -= com[0];
			ligando->lig_y[i] -= com[1];
			ligando->lig_z[i] -= com[2];
		}
	//}

	//file >> cadena;
	// fscanf(fp,"%s", cadena1);
	if (skip_to_token(fp,"@<TRIPOS>BOND")) {
                exit(1);
                return;
        }
     
     for (unsigned int i=0; i < nBond; i++){

          //file >> aux >> ind1 >> ind2 >> cadena;
          fscanf(fp, "%u %u %u %s",&aux, &ind1, &ind2,cadena);

          if (skip_to_eol(fp)) {
               return;
          }

          ind1--;
          ind2--;

          ligando->bonds[MAXBOND*ind1 + ligando->nbonds[ind1]] = ind2;
          ligando->bonds[MAXBOND*ind2 + ligando->nbonds[ind2]] = ind1;
          ligando->nbonds[ind1]++;
          ligando->nbonds[ind2]++;

		//Calcula el número de enlaces de cada nitrógeno
		if (ligando->ligtype[ind1] == NIT && isdigit(cadena[0])){
			enlaces_nit[ind1] += atoi(cadena);
		}

		if (ligando->ligtype[ind2] == NIT && isdigit(cadena[0])){
			enlaces_nit[ind2] += atoi(cadena);
		}

     }

	

	for (unsigned int i=0; i < nAtom; i++){
		if(ligando->ligtype[i] == NIT && enlaces_nit[i] < 4)
			ligando->ligtype[i] = NIT2;
	}

	for (unsigned int i=0; i < nAtom; i++){
		unsigned int tipo_enlace = ligando->ligtype[ligando->bonds[MAXBOND*i]];
		if (ligando->ligtype[i] == HID &&
			(tipo_enlace == OXI || tipo_enlace == NIT2 || tipo_enlace == AZU))
				ligando->ligtype[i] = HBOND;
	}
	for (unsigned int i=nAtom ; i < ligando->nlig ; i++){
		ligando->ql[i] = 0.0;
		ligando->ligtype[i] = 18;
		ligando->lig_x[i]  = 0.0;
		ligando->lig_y[i]= 0.0;
		ligando->lig_z[i]= 0.0;
	}
	//printf("ok readligand\n");
	//exit(0);
	
	fclose(fp);
	//printf("natom %d\n",nAtom);
	//for (unsigned int k=0;k<nAtom;k++) printf("%s x %f y %f z %f q %f\n",filename,ligando->lig_x[k],ligando->lig_y[k],ligando->lig_z[k],ligando->ql[k]);
	
}

extern void extraction_txt (char *input, char *output){
	unsigned int i=0;
	while ((input[i] != ' ') && (input[i] != '\t') && (input[i] != '\n')) {
		output[i]=input[i];
		i++;
	}
	output[i]='\0';
}


extern void readConfigFile(char *file, struct param_t &Param, struct metaheuristic_t &Metaheuristic) {

	FILE *fp;
	char  *colp, *valp, txtline[FILENAME], stmp[FILENAME];
	unsigned int   colon = ':';
	unsigned int   itmp;
	double ftmp, ftmp1, ftmp2, ftmp3;
	int l;

	 if ((fp = fopen(file,"r")) == NULL) {
                printf("ReadInput(): No se puede abrir el fichero \"%s\"\n", file);
                exit(1);
        }

	printf("\n\t\tCONFIG FILE: %s \n",file);
	printf("\n");
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	/////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.auto_flex = atoi(stmp);
	printf("Flexibilidad:  %d  ",Param.auto_flex);	//flex
	if (Param.auto_flex == 1) printf("FLEXIBILIDAD AUTOMATICO\n"); else printf("RIGIDO\n"); 
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.mode = atoi(stmp);
	printf("Modo:  %d\n",Param.mode);	//Modo
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.tipo_computo = atoi(stmp);
	printf("Tipo de Computo:  %d ",Param.tipo_computo);	//Tipo de computo
	if (Param.tipo_computo == 0) printf("HOMOGENEO\n"); else printf("HETEROGENEO\n"); 
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.ngpu = atoi(stmp);
	printf("Identificador de dispositivo:  %d\n",Param.ngpu);	//Modo
	///////////////////////////////////////////////////////////		
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	///////////////////////////////////////////////////////////		
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.optimizacion = atoi(stmp);
	printf("Fase de Warm_UP:  %d\n",Param.optimizacion);	//Fase de warm up
	///////////////////////////////////////////////////////////		
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.reparto_carga = atoi(stmp);
	printf("Fase de reparto de carga optimo:  %d\n",Param.reparto_carga);	//Fase de warm up
	///////////////////////////////////////////////////////////		
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.conf_warm_up_gpu = atoi(stmp);
	printf("Conformaciones GPU(warm_up):  %d \n",Param.conf_warm_up_gpu);	//Conformaciones para entrenamiento	GPU
	///////////////////////////////////////////////////////////		
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.conf_warm_up_cpu = atoi(stmp);
	printf("Conformaciones Multicore CPU(warm_up):  %d \n",Param.conf_warm_up_cpu);	//Conformaciones para entrenamiento	CPU
	///////////////////////////////////////////////////////////		
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.por_conf_warm_up_sel = atoi(stmp);
	printf("Porcentaje de Conformaciones Multicore para Seleccion(warm_up):  %d \n",Param.por_conf_warm_up_sel);	//Porcentaje Conformaciones entrenamiento fase seleccion
	///////////////////////////////////////////////////////////		
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.por_conf_warm_up_inc = atoi(stmp);
	printf("Porcentaje de Conformaciones Multicore para Incluir(warm_up):  %d \n",Param.por_conf_warm_up_inc);	//Porcentaje Conformaciones para entrenamiento fase Incluir
	///////////////////////////////////////////////////////////		
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.limit = atoi(stmp);
	printf("Limite tamaño de bloque (warm_up):  %d \n",Param.limit);	//Limite tamaño de bloque (warm_up)
	///////////////////////////////////////////////////////////			
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.steps_warm_up_gpu = atoi(stmp);
	printf("Pasadas optimizacion tamaño de bloque (warm_up):  %d \n",Param.steps_warm_up_gpu);	//Pasadas optimizacion tamaño de bloque SIN Shared (warm_up)
	/////////////////////////////////////////////////////	
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	strcpy(Param.openmp, ".");
	strcat(Param.openmp, route_bar);
	strcat(Param.openmp, stmp);	
	printf("Fichero configuracion OPEN-MP:  %s\n",Param.openmp);	// Configuracion openmp
	/////////////////////////////////////////////////////	
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	strcpy(Param.gpu, ".");
	strcat(Param.gpu, route_bar);
	strcat(Param.gpu, stmp);	
	printf("Fichero configuracion GPU:  %s\n",Param.gpu);	// Configuracion gpu
	///////////////////////////////////////////////////////////	
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	strcpy(Param.multigpu, ".");
	strcat(Param.multigpu, route_bar);
	strcat(Param.multigpu, stmp);	
	printf("Fichero configuracion multiGPU:  %s\n",Param.multigpu);	// Configuracion multigpu
	///////////////////////////////////////////////////////////			
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	///////////////////////////////////////////////////////////		
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.alarma = atoi(stmp);
	printf("Alarma activada: ",Param.alarma);	//Alarma
	if (Param.alarma) printf("¡¡¡SI!!!\n"); else printf("NO\n");
	///////////////////////////////////////////////////////////		
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.duracion_alarma = atoi(stmp);
	printf("Duración de la alarma: %d minutos)\n",Param.duracion_alarma);	//Duracion Alarma
	/////////////////////////////////////////////////////	
/*	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	/////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	//strcpy(Param.vdw_p, Param.input_dir_p);
	//strcat(Param.vdw_p, route_bar);
	strcpy(Param.forcefield, stmp);
	printf("Force Field Params:  %s\n",Param.forcefield);	// force_field*/
	 if (fgets(txtline, FILENAME, fp) == NULL) {
                printf("ReadInput: Input error in line:\n");
                printf("%s\n",txtline);
                exit(1);
        }
        /////////////////////////////////////////////////////////
        if (fgets(txtline, FILENAME, fp) == NULL) {
                printf("ReadInput: Input error in line:\n");
                printf("%s\n",txtline);
                exit(1);
        }
        extraction_txt(txtline,stmp);
        Param.rot_type = atoi(stmp);
        printf("Modo obtencion enlaces rotables: %d \n",Param.rot_type);
	/////////////////////////////////////////////////////
        if (fgets(txtline, FILENAME, fp) == NULL) {
                printf("ReadInput: Input error in line:\n");
                printf("%s\n",txtline);
                exit(1);
        }
        /////////////////////////////////////////////////////////
        if (fgets(txtline, FILENAME, fp) == NULL) {
                printf("ReadInput: Input error in line:\n");
                printf("%s\n",txtline);
                exit(1);
        }
        extraction_txt(txtline,stmp);
	Param.scoring_function_type = atoi(stmp);
	switch (Param.scoring_function_type) {
		case 0:
			printf("Función de scoring: %d (ES+VDW+HBonds)\n",Param.scoring_function_type);   //Tipo 0
			break;
		case 1:
                        printf("Función de scoring: %d (ES+VDW+HBonds+Desolv)\n",Param.scoring_function_type);   //Tipo 1
			break;
		case 2:
                        printf("Función de scoring: %d (Autodock 4 scoring function)\n",Param.scoring_function_type);   //Tipo 2
			break;
		default:
			printf("Función de scoring: %d. ERROR\n");
			break;
	}
	/////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.level_bonds_VDW = atoi(stmp);
	printf("Niveles para evitar el cálculo de la Energía Interna en VDW:  %d\n ",Param.level_bonds_VDW);	// Levels avoid VDW
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);	
	Param.level_bonds_ES = atoi(stmp);
	printf("Niveles para evitar el cálculo de la Energía Interna en ES:  %d\n ",Param.level_bonds_ES);	// Levels avoid ES
	/////////////////////////////////////////////////////
		if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);	
	Param.flex_angle = atoi(stmp);
	printf("Angulo máximo de flexibilidad:  %d\n ",Param.flex_angle);	// Levels avoid ES
	/////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.automatic_seed = atoi(stmp);
	printf("Semilla Automatica:  %d ",Param.automatic_seed);	// Levels avoid ES
	if (Param.automatic_seed) printf("¡SEMILLA AUTOMATICA!\n"); else printf("NO\n");
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.seed = atoi(stmp);
	printf("Semilla:  %d\n",Param.seed);	// Semilla manual
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.max_desp = atof(stmp);
	printf("Shift:  %f\n",Param.max_desp);	// max desp
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.rotation = atoi(stmp);
	printf("Rotation angle:  %d\n",Param.rotation);	// Rotation angle
	/////////////////////////////////////////////////////
	 if (fgets(txtline, FILENAME, fp) == NULL) {
                printf("ReadInput: Input error in line:\n");
                printf("%s\n",txtline);
                exit(1);
        }
        extraction_txt(txtline,stmp);
        Param.rotation_mu = atoi(stmp);
        printf("Rotation angle Mutation:  %d\n",Param.rotation_mu); // Rotation angle mutation
	/////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Param.montecarlo = atoi(stmp);
	printf("Tipo de Mejora:  %d ",Param.montecarlo);	// Mejora montecarlo
	if (Param.montecarlo) printf("MONTECARLO\n"); else printf("LOCAL SEARCH\n");
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.NEEImp = atoi(stmp);
	printf("NEEImp:  %d\n",Metaheuristic.NEEImp); // Numero de puntos en el entorno en mejora
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.IMEFlex = atoi(stmp);
	printf("IMEFlex:  %d\n",Metaheuristic.IMEFlex);	// Intensificacion para flexibilidad
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.NEIIni = atoi(stmp);
	printf("NEIIni:  %d\n",Metaheuristic.NEIIni);	// numero de elementos iniciales
	 ///////////////////////////////////////////////////////////
        if (fgets(txtline, FILENAME, fp) == NULL) {
                printf("ReadInput: Input error in line:\n");
                printf("%s\n",txtline);
                exit(1);
        }
        extraction_txt(txtline,stmp);
        Metaheuristic.DQNIni = atoi(stmp);
        printf("DQNIni:  %d\n",Metaheuristic.DQNIni);   // numero de elementos iniciales DQN
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.PEMIni = atoi(stmp);
	printf("PEMIni:  %d\n",Metaheuristic.PEMIni);	// porcentaje de mejora
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.IMEIni = atoi(stmp);
	printf("IMEIni:  %d\n",Metaheuristic.IMEIni);	//Intensificacion de mejora
	Metaheuristic.NEIIni_selec = (unsigned int)ceil((Metaheuristic.NEIIni * Metaheuristic.PEMIni) / 100);
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.NEFMIni = atoi(stmp);
	printf("NEFMIni:  %d\n",Metaheuristic.NEFMIni);	// PORCENTAJE de elementos MEJORES finales
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.NEFPIni = atoi(stmp);
	printf("NEFPIni:  %d\n",Metaheuristic.NEFPIni);	// PORCENTAJE de elementos PEORES finales
	Metaheuristic.NEFMIni_selec = (unsigned int)((Metaheuristic.NEIIni * Metaheuristic.NEFMIni) / 100);
	Metaheuristic.NEFPIni_selec = (unsigned int)((Metaheuristic.NEIIni * Metaheuristic.NEFPIni) / 100);	
	Metaheuristic.NEFIni = Metaheuristic.NEFMIni_selec + Metaheuristic.NEFPIni_selec;
	if (Metaheuristic.NEFIni > Metaheuristic.NEIIni) {l=0; while (Metaheuristic.NEFIni > Metaheuristic.NEIIni) { if (l%2==0) Metaheuristic.NEFPIni_selec--; else Metaheuristic.NEFMIni_selec--;l++;}}
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.NEMSel = atoi(stmp);
	printf("NEMSel:  %d\n",Metaheuristic.NEMSel);	// NEMSel
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.NEPSel = atoi(stmp);
	printf("NEPSel:  %d\n",Metaheuristic.NEPSel);	// NEMPel	
	Metaheuristic.NEMSel_selec = (unsigned int)(((Metaheuristic.NEFMIni_selec + Metaheuristic.NEFPIni_selec) * Metaheuristic.NEMSel) / 100);
	Metaheuristic.NEPSel_selec = (unsigned int)(((Metaheuristic.NEFMIni_selec + Metaheuristic.NEFPIni_selec) * Metaheuristic.NEPSel) / 100);
	//printf("%d %d\n", Metaheuristic.NEMSel_selec, Metaheuristic.NEPSel_selec);
	if ((Metaheuristic.NEMSel_selec + Metaheuristic.NEPSel_selec) > Metaheuristic.NEFIni) {l=0; while ((Metaheuristic.NEMSel_selec + Metaheuristic.NEPSel_selec) > Metaheuristic.NEFIni) { if (l%2==0) Metaheuristic.NEPSel_selec--; else Metaheuristic.NEMSel_selec--;l++;}}
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.NMMCom = atoi(stmp);
	printf("NMMCom:  %d\n",Metaheuristic.NMMCom);	// PORCENTAJE de elementos a combinar MEJORES con MEJORES
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.NMPCom = atoi(stmp);
	printf("NMPCom:  %d\n",Metaheuristic.NMPCom);	// PORCENTAJE de elementos a combinar MEJORES con PEORES
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.NPPCom = atoi(stmp);
	printf("NPPCom:  %d\n",Metaheuristic.NPPCom);	// PORCENTAJE de elementos a combinar PEORES con PEORES
	Metaheuristic.NMMCom_selec = (unsigned int)floor((Metaheuristic.NEMSel_selec  * Metaheuristic.NMMCom) / 100);
	Metaheuristic.NMPCom_selec = (unsigned int)floor(((Metaheuristic.NEMSel_selec + Metaheuristic.NEPSel_selec) * Metaheuristic.NMPCom) / 100);
	Metaheuristic.NPPCom_selec = (unsigned int)floor((Metaheuristic.NEPSel_selec  * Metaheuristic.NPPCom) / 100);
	Metaheuristic.NCOMtotal = (Metaheuristic.NMMCom_selec * (Metaheuristic.NMMCom_selec - 1)) + (Metaheuristic.NMPCom_selec * (Metaheuristic.NMPCom_selec - 1)) + (Metaheuristic.NPPCom_selec * (Metaheuristic.NPPCom_selec - 1));	
	/////////////////////////////////////////////////////
	 if (fgets(txtline, FILENAME, fp) == NULL) {
                printf("ReadInput: Input error in line:\n");
                printf("%s\n",txtline);
                exit(1);
        }
        extraction_txt(txtline,stmp);
        Metaheuristic.PEMUCom = atoi(stmp);	
	printf("PEMUCom:  %d\n",Metaheuristic.PEMUCom);   // PORCENTAJE de elementos a mutar
	/////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
                printf("ReadInput: Input error in line:\n");
                printf("%s\n",txtline);
                exit(1);
        }
        extraction_txt(txtline,stmp);
        Metaheuristic.IMEMUCom = atoi(stmp); 
        printf("IMEMUCom:  %d\n",Metaheuristic.IMEMUCom);   // Intensificación de los elementos a mutar
	/////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.PEMSel = atoi(stmp);
	printf("PEMSel:  %d\n",Metaheuristic.PEMSel);	// porcentaje de elementos mejores para mejorar
	Metaheuristic.PEMSel_selec = (unsigned int)ceil((Metaheuristic.NEFIni * Metaheuristic.PEMSel) / 100);	
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.IMEImp = atoi(stmp);
	printf("IMEImp:  %d\n",Metaheuristic.IMEImp);	// Intensificacion de la mejora
	/////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);	
	Metaheuristic.NEMInc = atoi(stmp);
	Metaheuristic.NEPInc = 100 - Metaheuristic.NEMInc;
	printf("NEMInc:  %d\n",Metaheuristic.NEMInc);	// Mejores a incluir
	
	/////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	Metaheuristic.NEMInc_selec = (unsigned int)floor((Metaheuristic.NEFIni  * Metaheuristic.NEMInc) / 100);
	Metaheuristic.NEPInc_selec = (unsigned int)floor((Metaheuristic.NEFIni  * Metaheuristic.NEPInc) / 100);
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.NIRFin = atoi(stmp);
	printf("NIRFin:  %d\n",Metaheuristic.NIRFin);	// Pasadas sin mejora
	///////////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.NMIFin = atoi(stmp);
	printf("NMIFin:  %d\n",Metaheuristic.NMIFin);	// Pasadas máximas
	/////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	if ((Param.mode == 0) || (Param.mode == 3))
	{
			Metaheuristic.num_kernels = 7;
			 //////////////////////////////////////////////////////
                        if (fgets(txtline, FILENAME, fp) == NULL) {
                                printf("ReadInput: Input error in line:\n");
                                printf("%s\n",txtline);
                                exit(1);
                        }
                        extraction_txt(txtline,stmp);
                        Param.auto_max_threads = atoi(stmp);
                        if (Param.auto_max_threads == 1)
                                printf("Automatic threads:  %d AUTOMATIC\n",Param.auto_max_threads);    // Configuracion automatica threads
                        else
                                printf("Automatic threads:  %d MANUAL\n",Param.auto_max_threads);       // Configuracion automatica threads
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads1Ini = atoi(stmp);
			printf("Hilos primer nivel Inicializar:  %d\n",Metaheuristic.Threads1Ini);	// Hilos primer nivel Inicializar
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads1Sel = atoi(stmp);
			printf("Hilos primer nivel Seleccionar:  %d\n",Metaheuristic.Threads1Sel);	// Hilos primer nivel Seleccionar
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.ThreadsperblockFit = atoi(stmp);
			printf("Numero de Hilos por bloque función Cálculo de FITNESS:  %d\n",Metaheuristic.ThreadsperblockFit);	//Numero de Hilos por bloque función FITNESS			
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.ThreadsperblockMoveIni = atoi(stmp);
			printf("Numero de Hilos por bloque función MOVE Inicializar:  %d\n",Metaheuristic.ThreadsperblockMoveIni);	//Numero de Hilos por bloque funcion MOVE Ini
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.ThreadsperblockRotIni = atoi(stmp);
			printf("Numero de Hilos por bloque función ROTACION Inicializar:  %d\n",Metaheuristic.ThreadsperblockRotIni);	//Numero de Hilos por bloque funcion ROTACION Ini
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.ThreadsperblockQuatIni = atoi(stmp);
			printf("Numero de Hilos por bloque función QUAT Inicializar:  %d\n",Metaheuristic.ThreadsperblockQuatIni);	//Numero de Hilos por bloque funcion QUAT Ini
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.ThreadsperblockRandomIni = atoi(stmp);
			printf("Numero de Hilos por bloque función CURAND Inicializar:  %d\n",Metaheuristic.ThreadsperblockRandomIni);	//Numero de Hilos por bloque funcion CURAND Ini
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads1Com = atoi(stmp);
			printf("Hilos primer nivel Combinar:  %d\n",Metaheuristic.Threads1Com);	// Hilos primer nivel Combinar
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads2Com = atoi(stmp);
			printf("Hilos segundo nivel Combinar:  %d\n",Metaheuristic.Threads1Com);	// Hilos segundo nivel Combinar
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.ThreadsperblockMoveImp = atoi(stmp);
			printf("Numero de Hilos por bloque función MOVE mejorar:  %d\n",Metaheuristic.ThreadsperblockMoveImp);	//Numero de Hilos por bloque funcion MOVE mejorar
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.ThreadsperblockIncImp = atoi(stmp);
			printf("Numero de Hilos por bloque función INCLUIR mejorar:  %d\n",Metaheuristic.ThreadsperblockIncImp);	//Numero de Hilos por bloque funcion INCLUIR mejorar			
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads1Inc = atoi(stmp);
			printf("Hilos primer nivel incluir:  %d\n",Metaheuristic.Threads1Inc);	// Hilos primer nivel Incluir
			if (Param.auto_max_threads == 1)
                        {
                                Metaheuristic.Threads1Fit = omp_get_num_procs();
                                Metaheuristic.Threads2Fit = 1;
                                Metaheuristic.Threads1Ini = omp_get_num_procs();
                                Metaheuristic.Threads1Sel = omp_get_num_procs();
                                Metaheuristic.Threads1Com = omp_get_num_procs();
                                Metaheuristic.Threads2Com = 1;
                                Metaheuristic.Threads1Imp = omp_get_num_procs();
                                Metaheuristic.Threads1Inc = omp_get_num_procs();
                                printf("Automatic Threads first level: %d\n", Metaheuristic.Threads1Fit);

                        }
	}
	if (Param.mode == 2)
	{
			for (int i = 0; i < 14;i++)
			{
				/////////////////////////////////////////////////////
				if (fgets(txtline, FILENAME, fp) == NULL) {
					printf("ReadInput: Input error in line:\n");
					printf("%s\n",txtline);
					exit(1);
				}
			}
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Param.auto_max_threads = atoi(stmp);
			if (Param.auto_max_threads == 1)
				printf("Automatic threads:  %d AUTOMATIC\n",Param.auto_max_threads);	// Configuracion automatica threads
			else
				printf("Automatic threads:  %d MANUAL\n",Param.auto_max_threads);	// Configuracion automatica threads
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads1Ini = atoi(stmp);
			printf("Hilos primer nivel Inicializar:  %d\n",Metaheuristic.Threads1Ini);	// Hilos primer nivel Inicializar
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads1Fit = atoi(stmp);
			printf("Hilos primer nivel Calculo Fitness:  %d\n",Metaheuristic.Threads1Fit);	// Hilos segundo nivel Inicializar
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads2Fit = atoi(stmp);
			printf("Hilos segundo nivel Calculo Fitness:  %d\n",Metaheuristic.Threads2Fit);	// Hilos primer nivel Seleccionar
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads1Sel = atoi(stmp);
			printf("Hilos primer nivel Seleccionar:  %d\n",Metaheuristic.Threads1Sel);	// Hilos primer nivel Combinar
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads1Com = atoi(stmp);
			printf("Hilos primer nivel Combinar:  %d\n",Metaheuristic.Threads1Com);	// Hilos segundo nivel Combinar
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads2Com = atoi(stmp);
			printf("Hilos segundo nivel Combinar:  %d\n",Metaheuristic.Threads2Com);	// Hilos primer nivel Mejorar
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads1Imp = atoi(stmp);
			printf("Hilos primer nivel Mejorar:  %d\n",Metaheuristic.Threads1Imp);	// Hilos segundo nivel Mejorar
			//////////////////////////////////////////////////////
			if (fgets(txtline, FILENAME, fp) == NULL) {
				printf("ReadInput: Input error in line:\n");
				printf("%s\n",txtline);
				exit(1);
			}
			extraction_txt(txtline,stmp);
			Metaheuristic.Threads1Inc = atoi(stmp);
			printf("Hilos primer nivel incluir:  %d\n",Metaheuristic.Threads1Inc);	// Hilos primer nivel Incluir
			if (Param.auto_max_threads == 1)
			{
				Metaheuristic.Threads1Fit = omp_get_num_procs();
				Metaheuristic.Threads2Fit = 1;
				Metaheuristic.Threads1Ini = omp_get_num_procs();
				Metaheuristic.Threads1Sel = omp_get_num_procs();
				Metaheuristic.Threads1Com = omp_get_num_procs();
				Metaheuristic.Threads2Com = 1;  
				Metaheuristic.Threads1Imp = omp_get_num_procs();
				Metaheuristic.Threads1Inc = omp_get_num_procs();
				printf("Automatic Threads first level: %d\n", Metaheuristic.Threads1Fit);
				
			}
	}
}

extern void readOpenmpOptimizationFile(char *file, struct metaheuristic_t &Metaheuristic, unsigned int *error) {

	FILE *fp;
	char  txtline[FILENAME], stmp[FILENAME];
	
	if ((fp = fopen(file,"r")) == NULL) {
		printf("ReadInput(): No se puede abrir el fichero \"%s\"\n", file);
		exit(1);
	}
	printf("\n\t\FICHERO OPTIMIZACION: %s \n",file);
	printf("\n");
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("Error formato fichero. Se va a regenerar.....\n");
		*error = 1;
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.Threads1Ini = atoi(stmp);
	printf("Hilos primer nivel Inicializar:  %d\n",Metaheuristic.Threads1Ini);	// Hilos primer nivel Inicializar
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("Error formato fichero. Se va a regenerar.....\n");
		*error = 1;
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.Threads1Fit = atoi(stmp);
	printf("Hilos primer nivel Calculo Fitness:  %d\n",Metaheuristic.Threads1Fit);	// Hilos segundo nivel Inicializar
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("Error formato fichero. Se va a regenerar.....\n");
		*error = 1;
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.Threads2Fit = atoi(stmp);
	printf("Hilos segundo nivel Calculo Fitness:  %d\n",Metaheuristic.Threads2Fit);	// Hilos primer nivel Seleccionar
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("Error formato fichero. Se va a regenerar.....\n");
		*error = 1;
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.Threads1Sel = atoi(stmp);
	printf("Hilos primer nivel Seleccionar:  %d\n",Metaheuristic.Threads1Sel);	// Hilos primer nivel Combinar
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("Error formato fichero. Se va a regenerar.....\n");
		*error = 1;
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.Threads1Com = atoi(stmp);
	printf("Hilos primer nivel Combinar:  %d\n",Metaheuristic.Threads1Com);	// Hilos segundo nivel Combinar
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("Error formato fichero. Se va a regenerar.....\n");
		*error = 1;
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.Threads2Com = atoi(stmp);
	printf("Hilos segundo nivel Combinar:  %d\n",Metaheuristic.Threads2Com);	// Hilos primer nivel Mejorar
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("Error formato fichero. Se va a regenerar.....\n");
		*error = 1;
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.Threads1Imp = atoi(stmp);
	printf("Hilos primer nivel Mejorar:  %d\n",Metaheuristic.Threads1Imp);	// Hilos segundo nivel Mejorar
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("Error formato fichero. Se va a regenerar.....\n");
		*error = 1;
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.Threads1Inc = atoi(stmp);
	printf("Hilos primer nivel incluir:  %d\n",Metaheuristic.Threads1Inc);	// Hilos primer nivel Incluir
}

extern void readGpuOptimizationFile (char *file, unsigned int device, struct metaheuristic_t &Metaheuristic, struct device_t *devices, unsigned int *error) 
{
	FILE *fp;
	char  txtline[FILENAME], stmp[FILENAME];
	unsigned int gpu;
	
	if ((fp = fopen(file,"r")) == NULL) {
		printf("ReadInput(): No se puede abrir el fichero \"%s\"\n", file);
		exit(1);
	}
	printf("\n\t\FICHERO OPTIMIZACION: %s \n",file);
	printf("\n");
	
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}	
	Metaheuristic.num_kernels = 7;
	printf("\n");
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
	printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	gpu = atoi(stmp);
	//printf("gpu %d device %d\n",gpu,device);
	if (gpu != device)
	{
		printf("Error formato fichero. Se va a regenerar.....\n");
		*error = 1;
	}
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
	printf("ReadInput: Input error in line:\n");
		printf("Error formato fichero. Se va a regenerar.....\n");
		*error = 1;
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.Threads1Ini = atoi(stmp);
	printf("Hilos primer nivel Inicializar:  %d\n",Metaheuristic.Threads1Ini);	// Hilos primer nivel Inicializar
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {		
		printf("Error formato fichero. Se va a regenerar.....\n");
		*error = 1;
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.Threads1Sel = atoi(stmp);
	printf("Hilos primer nivel Seleccionar:  %d\n",Metaheuristic.Threads1Sel);	// Hilos primer nivel Inicializar
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("Error formato fichero. Se va a regenerar.....\n");
		*error = 1;
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.ThreadsperblockFit = atoi(stmp);
	devices-> hilos[0] = Metaheuristic.ThreadsperblockFit;
	printf("Numero de Hilos por bloque función Cálculo de FITNESS:  %d\n",devices-> hilos[0]);	//Numero de Hilos por bloque función FITNESS			
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("Error formato fichero. Se va a regenerar.....\n");
		*error = 1;
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.ThreadsperblockMoveIni = atoi(stmp);
	devices-> hilos[1] = Metaheuristic.ThreadsperblockMoveIni;
	printf("Numero de Hilos por bloque función MOVE Inicializar:  %d\n",devices-> hilos[1]);	//Numero de Hilos por bloque funcion MOVE Ini
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("Error formato fichero. Se va a regenerar.....\n");
		*error = 1;
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.ThreadsperblockRotIni = atoi(stmp);
	devices-> hilos[2] = Metaheuristic.ThreadsperblockRotIni;
	printf("Numero de Hilos por bloque función ROTACION Inicializar:  %d\n",devices-> hilos[2] );	//Numero de Hilos por bloque funcion ROTACION Ini
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("Error formato fichero. Se va a regenerar.....\n");
		*error = 1;
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.ThreadsperblockQuatIni = atoi(stmp);
	devices-> hilos[3] =  Metaheuristic.ThreadsperblockQuatIni;
	printf("Numero de Hilos por bloque función QUAT Inicializar:  %d\n",devices-> hilos[3]);	//Numero de Hilos por bloque funcion QUAT Ini
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("Error formato fichero. Se va a regenerar.....\n");
		*error = 1;
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.ThreadsperblockRandomIni = atoi(stmp);
	devices-> hilos[4] = Metaheuristic.ThreadsperblockRandomIni;
	printf("Numero de Hilos por bloque función CURAND Inicializar:  %d\n",devices-> hilos[4]);	//Numero de Hilos por bloque funcion CURAND Ini
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("Error formato fichero. Se va a regenerar.....\n");
		*error = 1;
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.Threads1Com = atoi(stmp);
	printf("Hilos primer nivel Combinar:  %d\n",Metaheuristic.Threads1Com);	// Hilos primer nivel Combinar
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("Error formato fichero. Se va a regenerar.....\n");
		*error = 1;
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.Threads2Com = atoi(stmp);
	printf("Hilos segundo nivel Combinar:  %d\n",Metaheuristic.Threads2Com);	// Hilos segundo nivel Combinar
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("Error formato fichero. Se va a regenerar.....\n");
		*error = 1;
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.ThreadsperblockMoveImp = atoi(stmp);
	devices-> hilos[5] =  Metaheuristic.ThreadsperblockMoveImp;
	printf("Numero de Hilos por bloque función MOVE mejorar:  %d\n",devices-> hilos[5]);	//Numero de Hilos por bloque funcion MOVE mejorar
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("Error formato fichero. Se va a regenerar.....\n");
		*error = 1;
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.ThreadsperblockIncImp = atoi(stmp);
	devices-> hilos[6] = Metaheuristic.ThreadsperblockIncImp;
	printf("Numero de Hilos por bloque función INCLUIR mejorar:  %d\n",devices-> hilos[6]);	//Numero de Hilos por bloque funcion INCLUIR mejorar			
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("Error formato fichero. Se va a regenerar.....\n");
		*error = 1;
	}
	extraction_txt(txtline,stmp);
	Metaheuristic.Threads1Inc = atoi(stmp);
	printf("Hilos primer nivel incluir:  %d\n",Metaheuristic.Threads1Inc);	// Hilos primer nivel Incluir
}

extern void readmultiGpuOptimizationFile (char *file, unsigned int tipo_computo, unsigned int opc, struct metaheuristic_t &Metaheuristic, struct device_t *devices, unsigned int *error) 
{
	FILE *fp;
	char  txtline[FILENAME], stmp[FILENAME];
	unsigned int gpus,modo,opc_f,num_kernels;
	
	if ((fp = fopen(file,"r")) == NULL) {
		printf("ReadInput(): No se puede abrir el fichero \"%s\"\n", file);
		exit(1);
	}
	printf("\n\t\FICHERO OPTIMIZACION: %s \n",file);
	printf("\n");
	
	if (fgets(txtline, FILENAME, fp) == NULL) {
		printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}	
	Metaheuristic.num_kernels = 7;
	num_kernels = Metaheuristic.num_kernels;
	printf("\n");
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
	printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	gpus = atoi(stmp);
	//printf("gpu %d device %d\n",gpu,device);
	if (gpus != devices->n_devices)
	{
		printf("Error formato fichero. Se va a regenerar.....\n");
		*error = 1;
	}
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
	printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	modo = atoi(stmp);
	//printf("gpu %d device %d\n",gpu,device);
	if (modo != tipo_computo)
	{
		printf("Error formato fichero. Se va a regenerar.....\n");
		*error = 1;
	}
	//////////////////////////////////////////////////////
	if (fgets(txtline, FILENAME, fp) == NULL) {
	printf("ReadInput: Input error in line:\n");
		printf("%s\n",txtline);
		exit(1);
	}
	extraction_txt(txtline,stmp);
	opc_f = atoi(stmp);
	//printf("gpu %d device %d\n",gpu,device);
	if (modo == 0)
	{		
		if (opc_f != opc)
		{
			printf("Error formato fichero. Se va a regenerar.....\n");
			*error = 1;
		}
	}
	for (unsigned int i = 0; i < devices->n_devices;i++)
	{
		if (fgets(txtline, FILENAME, fp) == NULL) {
			printf("ReadInput: Input error in line:\n");
			printf("%s\n",txtline);
			exit(1);
		}
		extraction_txt(txtline,stmp);
		devices->id[i] = atoi(stmp);
		printf("Device %d\n",devices->id[i]);
		//////////////////////////////////////////////////////
		if (fgets(txtline, FILENAME, fp) == NULL) {			
			printf("Error formato fichero. Se va a regenerar.....\n");
			*error = 1;
		}
		extraction_txt(txtline,stmp);
		Metaheuristic.Threads1Ini = atoi(stmp);
		printf("Hilos primer nivel Inicializar:  %d\n",Metaheuristic.Threads1Ini);	// Hilos primer nivel Inicializar
		//////////////////////////////////////////////////////
		if (fgets(txtline, FILENAME, fp) == NULL) {		
			printf("Error formato fichero. Se va a regenerar.....\n");
			*error = 1;
		}
		extraction_txt(txtline,stmp);
		Metaheuristic.Threads1Sel = atoi(stmp);
		printf("Hilos primer nivel Seleccionar:  %d\n",Metaheuristic.Threads1Sel);	// Hilos primer nivel Inicializar
		//////////////////////////////////////////////////////
		if (fgets(txtline, FILENAME, fp) == NULL) {
			printf("Error formato fichero. Se va a regenerar.....\n");
			*error = 1;
		}
		extraction_txt(txtline,stmp);
		Metaheuristic.ThreadsperblockFit = atoi(stmp);
		devices-> hilos[i*num_kernels] = Metaheuristic.ThreadsperblockFit;
		printf("Numero de Hilos por bloque función Cálculo de FITNESS:  %d\n",devices-> hilos[i*num_kernels]);	//Numero de Hilos por bloque función FITNESS			
		//////////////////////////////////////////////////////
		if (fgets(txtline, FILENAME, fp) == NULL) {
			printf("Error formato fichero. Se va a regenerar.....\n");
			*error = 1;
		}
		extraction_txt(txtline,stmp);
		Metaheuristic.ThreadsperblockMoveIni = atoi(stmp);
		devices-> hilos[i*num_kernels+1] = Metaheuristic.ThreadsperblockMoveIni;
		printf("Numero de Hilos por bloque función MOVE Inicializar:  %d\n",devices-> hilos[i*num_kernels+1]);	//Numero de Hilos por bloque funcion MOVE Ini
		//////////////////////////////////////////////////////
		if (fgets(txtline, FILENAME, fp) == NULL) {
			printf("Error formato fichero. Se va a regenerar.....\n");
			*error = 1;
		}
		extraction_txt(txtline,stmp);
		Metaheuristic.ThreadsperblockRotIni = atoi(stmp);
		devices-> hilos[i*num_kernels+2] = Metaheuristic.ThreadsperblockRotIni;
		printf("Numero de Hilos por bloque función ROTACION Inicializar:  %d\n",devices-> hilos[i*num_kernels+2] );	//Numero de Hilos por bloque funcion ROTACION Ini
		//////////////////////////////////////////////////////
		if (fgets(txtline, FILENAME, fp) == NULL) {
			printf("Error formato fichero. Se va a regenerar.....\n");
			*error = 1;
		}
		extraction_txt(txtline,stmp);
		Metaheuristic.ThreadsperblockQuatIni = atoi(stmp);
		devices-> hilos[i*num_kernels+3] =  Metaheuristic.ThreadsperblockQuatIni;
		printf("Numero de Hilos por bloque función QUAT Inicializar:  %d\n",devices-> hilos[i*num_kernels+3]);	//Numero de Hilos por bloque funcion QUAT Ini
		//////////////////////////////////////////////////////
		if (fgets(txtline, FILENAME, fp) == NULL) {
			printf("Error formato fichero. Se va a regenerar.....\n");
			*error = 1;
		}
		extraction_txt(txtline,stmp);
		Metaheuristic.ThreadsperblockRandomIni = atoi(stmp);
		devices-> hilos[i*num_kernels+4] = Metaheuristic.ThreadsperblockRandomIni;
		printf("Numero de Hilos por bloque función CURAND Inicializar:  %d\n",devices-> hilos[i*num_kernels+4]);	//Numero de Hilos por bloque funcion CURAND Ini
		//////////////////////////////////////////////////////
		if (fgets(txtline, FILENAME, fp) == NULL) {
			printf("Error formato fichero. Se va a regenerar.....\n");
			*error = 1;
		}
		extraction_txt(txtline,stmp);
		Metaheuristic.Threads1Com = atoi(stmp);
		printf("Hilos primer nivel Combinar:  %d\n",Metaheuristic.Threads1Com);	// Hilos primer nivel Combinar
		//////////////////////////////////////////////////////
		if (fgets(txtline, FILENAME, fp) == NULL) {
			printf("Error formato fichero. Se va a regenerar.....\n");
			*error = 1;
		}
		extraction_txt(txtline,stmp);
		Metaheuristic.Threads2Com = atoi(stmp);
		printf("Hilos segundo nivel Combinar:  %d\n",Metaheuristic.Threads2Com);	// Hilos segundo nivel Combinar
		//////////////////////////////////////////////////////
		if (fgets(txtline, FILENAME, fp) == NULL) {
			printf("Error formato fichero. Se va a regenerar.....\n");
			*error = 1;
		}
		extraction_txt(txtline,stmp);
		Metaheuristic.ThreadsperblockMoveImp = atoi(stmp);
		devices-> hilos[i*num_kernels+5] =  Metaheuristic.ThreadsperblockMoveImp;
		printf("Numero de Hilos por bloque función MOVE mejorar:  %d\n",devices-> hilos[i*num_kernels+5]);	//Numero de Hilos por bloque funcion MOVE mejorar
		//////////////////////////////////////////////////////
		if (fgets(txtline, FILENAME, fp) == NULL) {
			printf("Error formato fichero. Se va a regenerar.....\n");
			*error = 1;
		}
		extraction_txt(txtline,stmp);
		Metaheuristic.ThreadsperblockIncImp = atoi(stmp);
		devices-> hilos[i*num_kernels+6] = Metaheuristic.ThreadsperblockIncImp;
		printf("Numero de Hilos por bloque función INCLUIR mejorar:  %d\n",devices-> hilos[i*num_kernels+6]);	//Numero de Hilos por bloque funcion INCLUIR mejorar			
		//////////////////////////////////////////////////////
		if (fgets(txtline, FILENAME, fp) == NULL) {
			printf("Error formato fichero. Se va a regenerar.....\n");
			*error = 1;
		}
		extraction_txt(txtline,stmp);
		Metaheuristic.Threads1Inc = atoi(stmp);
		printf("Hilos primer nivel incluir:  %d\n",Metaheuristic.Threads1Inc);	// Hilos primer nivel Incluir
	}
}


