#ifndef COMMON_H_
#define COMMON_H_
#include "definitions.h"
#include "energy_struct.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cstring>
#include <time.h>
#include <ostream>
#include <iomanip>

// includes, project
extern double calc_ddd_Mehler_Solmajer( type_data distance, double approx_zero );
extern void replace_for_dqn(type_data **dqn_data, struct vectors_t *vectors_e, struct metaheuristic_t metaheuristic);
extern type_data **readDQNfile(char *dqnfile, struct vectors_t *vectors_e, struct metaheuristic_t metaheuristic);
extern void read_pdbqt (struct param_t param, char *filename, unsigned int *nlinks, int *&n_links);
extern int skip_to_token(FILE* file,char* token);
extern int skip_to_eol(FILE* file);
extern int getTypeNumber (char* cadena);
char* getTypeString (char tipo, char * string);
extern unsigned int getPadding (unsigned int nrec, unsigned int blockSize);

extern void readLigand (char *filename, struct ligand_t *ligando, int scoring_type);

extern void initProtein(char* protname, Protein_entry* &Protein, Bond_tp * &bond, unsigned int &nBond, unsigned int &nAtom, int scoring_type);
extern void readProtein (Protein_entry * Protein, struct receptor_t &proteina, unsigned int &qrSize, unsigned int &recSize);

extern void force_field_read (char *input, type_data *&weights, struct force_field_param_t * f_params, int scoring_type);

extern void getCOM (type_data *ligx, type_data *ligy, type_data *ligz, unsigned int atom, type_data *com);

extern void extraction_txt (char *input, char *output);
extern void readConfigFile(char *file, struct param_t &Param, struct metaheuristic_t &Metaheuristic);
extern void readOpenmpOptimizationFile(char *file, struct metaheuristic_t &Metaheuristic, unsigned int *error);
extern void readGpuOptimizationFile (char *file, unsigned int device, struct metaheuristic_t &Metaheuristic, struct device_t *devices, unsigned int *error);
extern void readmultiGpuOptimizationFile (char *file, unsigned int tipo_computo, unsigned int opc, struct metaheuristic_t &Metaheuristic, struct device_t *devices, unsigned int *error);

extern void secuenciar (unsigned int *e_nconformations, unsigned int n);

extern void comprobation_params (struct param_t Param, struct metaheuristic_t Metaheuristic);

extern void fill_conformations_environment (type_data *e_move_x, type_data *e_move_y, type_data *e_move_z, type_data *e_quat_x, type_data *e_quat_y, type_data *e_quat_z, type_data *e_quat_w, type_data *s_move_x, type_data *s_move_y, type_data *s_move_z, type_data *s_quat_x, type_data *s_quat_y, type_data *s_quat_z, type_data *s_quat_w, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic);
extern void fill_conformations_environment_vs (unsigned int nlig,  type_data *e_conformations_x, type_data *e_conformations_y, type_data *e_conformations_z, type_data *e_move_x, type_data *e_move_y, type_data *e_move_z, type_data *e_quat_x, type_data *e_quat_y, type_data *e_quat_z, type_data *e_quat_w, char *e_ligtype, type_data *e_ql, unsigned int *e_bonds,  type_data *s_conformations_x, type_data *s_conformations_y, type_data *s_conformations_z, type_data *s_move_x, type_data *s_move_y, type_data *s_move_z, type_data *s_quat_x, type_data *s_quat_y, type_data *s_quat_z, type_data *s_quat_w,  char *s_ligtype, type_data *s_ql, unsigned int *s_bonds, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic);

extern void fill_conformations_environment_flex (unsigned int nlig, type_data *e_conformations_x, type_data *e_conformations_y, type_data *e_conformations_z, type_data *e_move_x, type_data *e_move_y, type_data *e_move_z, type_data *e_quat_x, type_data *e_quat_y, type_data *e_quat_z, type_data *e_quat_w, type_data *s_conformations_x, type_data *s_conformations_y, type_data *s_conformations_z, type_data *s_move_x, type_data *s_move_y, type_data *s_move_z, type_data *s_quat_x, type_data *s_quat_y, type_data *s_quat_z, type_data *s_quat_w, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic);
#endif /* COMMON_H_ */
