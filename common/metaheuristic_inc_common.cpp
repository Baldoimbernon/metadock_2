#include "energy_common.h"
#include <thrust/sequence.h>
#include <omp.h>

extern void limpiar (struct vectors_t *vectors_1, struct vectors_t *vectors_2)
{
	free(vectors_1->move_x);free(vectors_2->move_x);
	free(vectors_1->move_y);free(vectors_2->move_y);
	free(vectors_1->move_z);free(vectors_2->move_z);
	free(vectors_1->quat_x);//free(vectors_2->quat);
	free(vectors_1->quat_y);//free(vectors_2->quat);
	free(vectors_1->quat_z);//free(vectors_2->quat);
	free(vectors_1->quat_w);//free(vectors_2->quat);
	free(vectors_1->energy.energy);free(vectors_2->energy.energy);
	free(vectors_1->energy.n_conformation);free(vectors_2->energy.n_conformation);
}

extern void incluir_warm_up (struct vectors_t *vectors_e, struct param_t param, unsigned int hilos)
{
	struct vectors_t vectors_s;
	type_data tmp_1 = param.por_conf_warm_up_inc * param.conf_warm_up_cpu * 0.01;
	unsigned int tmp = (unsigned int) tmp_1;

	vectors_s.num_surface = vectors_e->num_surface;
	vectors_s.nconformations = vectors_s.num_surface * tmp;
	vectors_s.move_x = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.move_y = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.move_z = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.quat_x = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.quat_y = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.quat_z = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	vectors_s.quat_w = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	
	vectors_s.energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_s.nconformations);
	vectors_s.energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_s.nconformations);
	
	omp_set_num_threads (hilos);
	#pragma omp parallel for 	
	for (unsigned int i = 0;i < vectors_s.num_surface;i++)
	{	
		memcpy(vectors_s.move_x + i*tmp, vectors_e->move_x + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));  	
		memcpy(vectors_s.move_y + i*tmp, vectors_e->move_y + i*param.conf_warm_up_cpu, tmp * sizeof(type_data)); 
		memcpy(vectors_s.move_z + i*tmp, vectors_e->move_z + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.quat_x + i*tmp, vectors_e->quat_x + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.quat_y + i*tmp, vectors_e->quat_y + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.quat_z + i*tmp, vectors_e->quat_z + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.quat_w + i*tmp, vectors_e->quat_w + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.energy.energy + i*tmp, vectors_e->energy.energy + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));

		memcpy(vectors_s.move_x + i*tmp, vectors_e->move_x + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));  	
		memcpy(vectors_s.move_y + i*tmp, vectors_e->move_y + i*param.conf_warm_up_cpu, tmp * sizeof(type_data)); 
		memcpy(vectors_s.move_z + i*tmp, vectors_e->move_z + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.quat_x + i*tmp, vectors_e->quat_x + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.quat_y + i*tmp, vectors_e->quat_y + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.quat_z + i*tmp, vectors_e->quat_z + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.quat_w + i*tmp, vectors_e->quat_w + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));
		memcpy(vectors_s.energy.energy + i*tmp, vectors_e->energy.energy + i*param.conf_warm_up_cpu, tmp * sizeof(type_data));						
	}
	thrust::sequence(vectors_s.energy.n_conformation,vectors_s.energy.n_conformation + vectors_s.nconformations);
	
	free(vectors_s.move_x);
	free(vectors_s.move_y);
	free(vectors_s.move_z);
	free(vectors_s.quat_x);
	free(vectors_s.quat_y);
	free(vectors_s.quat_z);
	free(vectors_s.quat_w);
	free(vectors_s.energy.energy);
	free(vectors_s.energy.n_conformation);
}
