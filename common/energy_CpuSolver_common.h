#ifndef __CPUSOLVER_COMMON_H
#define __CPUSOLVER_COMMON_H

extern type_data frand();
extern double aleatorio (type_data minmax);
extern double getRandomNumber (type_data max);
extern double getRealRandomNumber (type_data minmax);
extern int getIntNumber(unsigned int max);
extern type_data frand_r(unsigned int auto_seed, unsigned int seed, unsigned int seed_r);
extern double getRandomNumber_r (type_data max,unsigned int auto_seed, unsigned int seed, unsigned int seed_r);
extern double getRealRandomNumber_r (type_data minmax,unsigned int auto_seed, unsigned int seed, unsigned int seed_r);
extern void init_quat(type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, unsigned int nconformations);

extern void sub_h (type_data *v0 , type_data *v1 ,type_data *v2 , type_data *w0, type_data *w1, type_data *w2, type_data * dst);
extern type_data norma_h (type_data *v);
extern type_data cosTheta_h (type_data * v, type_data * w);


#endif
