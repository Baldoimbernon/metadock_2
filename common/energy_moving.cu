#include "energy_moving.h"
#include "energy_common.h"

//Devuelve un numero entero aleatorio entre 0 y UINT_MAX
extern __device__ unsigned int getRandomNumber (curandState_t * state){
	return curand(state);
}

//Devuelve un numero en coma flotante aleatorio en el intervalo (0,1]
extern __device__ type_data getRandomNumber_double (curandState_t * state){
	return curand_uniform_double(state);
}

//Devuelve un numero en coma flotante aleatorio en el intervalo (-max_desp ,max_desp]
extern __device__ type_data getRandomNumber_double (curandState_t * state, type_data max_desp){
	return (2.0 * max_desp * getRandomNumber_double (state)) - max_desp;
}

extern __device__ unsigned int getRandomNumber_modified (curandState_t * state, unsigned int n, unsigned int act)
{
	type_data tmp = act;
	while ((tmp == act ) || (tmp >= n))
	 tmp = (unsigned int)((n*2) * getRandomNumber_double(state));
	return tmp;
}

__global__ void setupCurandState (curandState_t * state, unsigned int auto_seed, unsigned int seed,unsigned int max){

	unsigned int r_seed;
	unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;
	if (id < max)
	{	
		if (auto_seed)
			r_seed = (unsigned int) clock();
		else
			r_seed = (unsigned int) seed;
		curand_init (seed, id, 0, &state[id]);
	}
	//else
	//{
	//	printf("mayor que max %d\n",id);
	//}
}

__global__ void setup ( type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w,unsigned int max){


	unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;
	if (id < max)
	{
		quat_x[id] = 0;
		quat_y[id] = 0;
		quat_z[id] = 0;
		quat_w[id] = 1;
	}
}

__global__ void incluir (type_data *moves_x_d_selec, type_data *moves_y_d_selec, type_data *moves_z_d_selec, type_data *quat_d_selec_x, type_data *quat_d_selec_y, type_data *quat_d_selec_z, type_data *quat_d_selec_w, type_data *energy_d_selec, unsigned int* conformations_d_selec, type_data *moves_x_d, type_data *moves_y_d, type_data *moves_z_d, type_data *quat_d_x, type_data *quat_d_y, type_data *quat_d_z, type_data *quat_d_w, type_data *energy_d, unsigned int* conformations_d) 
{

	unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int id_conformation = conformations_d_selec[id];
	if (energy_d[id_conformation] >= energy_d_selec[id])
	{		
		moves_x_d[id_conformation] = moves_x_d_selec[id_conformation];
		moves_y_d[id_conformation] = moves_y_d_selec[id_conformation];
		moves_z_d[id_conformation] = moves_z_d_selec[id_conformation];
		quat_d_x[id_conformation] = quat_d_selec_x[id_conformation];
		quat_d_y[id_conformation] = quat_d_selec_y[id_conformation];
		quat_d_z[id_conformation] = quat_d_selec_z[id_conformation];
		quat_d_w[id_conformation] = quat_d_selec_w[id_conformation];
		energy_d[id_conformation] = energy_d_selec[id];
		conformations_d[id_conformation] = conformations_d_selec[id];
	}	
}

__global__ void incluir_mejorar(type_data *moves_x_d_mejora, type_data *moves_y_d_mejora, type_data *moves_z_d_mejora, type_data *quat_d_mejora_x, type_data *quat_d_mejora_y, type_data *quat_d_mejora_z, type_data *quat_d_mejora_w, type_data *energy_d_mejora, unsigned int* conformations_d_mejora, type_data *moves_x_d, type_data *moves_y_d, type_data *moves_z_d, type_data *quat_d_x, type_data *quat_d_y, type_data *quat_d_z, type_data *quat_d_w, type_data *energy_d, unsigned int stride_d, unsigned int max) {
	
	unsigned id = blockIdx.x * blockDim.x + threadIdx.x;
	if (id < max)
	{
		unsigned int id_conformation = conformations_d_mejora[id]-stride_d;

		if (energy_d[id] >= energy_d_mejora[id])
		{
			//printf("energy_d[%d] = %f %f\n",id,energy_d[id],energy_d_mejora[id]);
			moves_x_d[id_conformation] = moves_x_d_mejora[id_conformation];
			moves_y_d[id_conformation] = moves_y_d_mejora[id_conformation];
			moves_z_d[id_conformation] = moves_z_d_mejora[id_conformation];
			quat_d_x[id_conformation] = quat_d_mejora_x[id_conformation];
			quat_d_y[id_conformation] = quat_d_mejora_y[id_conformation];
			quat_d_z[id_conformation] = quat_d_mejora_z[id_conformation];
			quat_d_w[id_conformation] = quat_d_mejora_w[id_conformation];
			energy_d[id] = energy_d_mejora[id];
		}
		else
		{
			moves_x_d_mejora[id_conformation] = moves_x_d[id_conformation];
			moves_y_d_mejora[id_conformation] = moves_y_d[id_conformation];
			moves_z_d_mejora[id_conformation] = moves_z_d[id_conformation];
			quat_d_mejora_x[id_conformation] = quat_d_x[id_conformation];		
			quat_d_mejora_y[id_conformation] = quat_d_y[id_conformation];		
			quat_d_mejora_z[id_conformation] = quat_d_z[id_conformation];		
			quat_d_mejora_w[id_conformation] = quat_d_w[id_conformation];		
			energy_d_mejora[id_conformation] = energy_d[id_conformation];
		}
	}	
}

__global__ void incluir_mejorar_environment (int NEEImp, type_data *moves_x_d_mejora, type_data *moves_y_d_mejora, type_data *moves_z_d_mejora, type_data *quat_d_mejora_x, type_data *quat_d_mejora_y, type_data *quat_d_mejora_z, type_data *quat_d_mejora_w, type_data *energy_d_mejora, unsigned int* conformations_d_mejora, type_data *moves_x_d, type_data *moves_y_d, type_data *moves_z_d, type_data *quat_d_x, type_data *quat_d_y, type_data *quat_d_z, type_data *quat_d_w, type_data *energy_d, unsigned int stride_d, unsigned int max) {

        unsigned id = blockIdx.x * blockDim.x + threadIdx.x;
        if (id < max)
        {
                unsigned int id_conformation = conformations_d_mejora[id]-stride_d;
		unsigned int id_conformation_begin = id_conformation * NEEImp;
	
		for (int j=0; j < NEEImp; j++)
		{

                	if (energy_d[id] >= energy_d_mejora[id_conformation_begin + j])
                	{
                        	//printf("energy_d[%d] = %f\n",id,energy_d[id]);
                        	moves_x_d[id_conformation] = moves_x_d_mejora[id_conformation_begin + j];
                        	moves_y_d[id_conformation] = moves_y_d_mejora[id_conformation_begin + j];
                        	moves_z_d[id_conformation] = moves_z_d_mejora[id_conformation_begin + j];
                        	quat_d_x[id_conformation] = quat_d_mejora_x[id_conformation_begin + j];
                        	quat_d_y[id_conformation] = quat_d_mejora_y[id_conformation_begin + j];
                        	quat_d_z[id_conformation] = quat_d_mejora_z[id_conformation_begin + j];
                        	quat_d_w[id_conformation] = quat_d_mejora_w[id_conformation_begin + j];
                        	energy_d[id] = energy_d_mejora[id];
                	}
                	else
                	{
                        	moves_x_d_mejora[id_conformation_begin + j] = moves_x_d[id_conformation];
                        	moves_y_d_mejora[id_conformation_begin + j] = moves_y_d[id_conformation];
                        	moves_z_d_mejora[id_conformation_begin + j] = moves_z_d[id_conformation];
                        	quat_d_mejora_x[id_conformation_begin + j] = quat_d_x[id_conformation];
                        	quat_d_mejora_y[id_conformation_begin + j] = quat_d_y[id_conformation];
                        	quat_d_mejora_z[id_conformation_begin + j] = quat_d_z[id_conformation];
                        	quat_d_mejora_w[id_conformation_begin + j] = quat_d_w[id_conformation];
                        	energy_d_mejora[id_conformation_begin + j] = energy_d[id_conformation];
                	}
        	}
	}
}

__global__ void incluir_mejorar_environment_flex (int NEEImp,int nlig, type_data *conformations_x_d_mejora, type_data *conformations_y_d_mejora, type_data *conformations_z_d_mejora, type_data *moves_x_d_mejora, type_data *moves_y_d_mejora, type_data *moves_z_d_mejora, type_data *quat_d_mejora_x, type_data *quat_d_mejora_y, type_data *quat_d_mejora_z, type_data *quat_d_mejora_w, type_data *energy_d_mejora, unsigned int* conformations_d, type_data *conformations_x_d, type_data *conformations_y_d, type_data *conformations_z_d, type_data *moves_x_d, type_data *moves_y_d, type_data *moves_z_d, type_data *quat_d_x, type_data *quat_d_y, type_data *quat_d_z, type_data *quat_d_w, type_data *energy_d, unsigned int stride_d, unsigned int max) {

        unsigned id = blockIdx.x * blockDim.x + threadIdx.x;
        if (id < max)
        {
                //unsigned int id_conformation = conformations_d_mejora[id]-stride_d;
		unsigned int id_conformation = conformations_d[id]-stride_d;
                unsigned int id_conformation_begin = id_conformation * NEEImp;

                for (int j=0; j < NEEImp; j++)
                {

                        if (energy_d[id] >= energy_d_mejora[id_conformation_begin + j])
                        {
                                //printf("energy_d[%d] = %f\n",id,energy_d[id]);
				for (int i=0;i<nlig;i++) { conformations_x_d[id_conformation*nlig + i] = conformations_x_d_mejora[id_conformation_begin*nlig + i]; conformations_y_d[id_conformation*nlig + i] = conformations_y_d_mejora[id_conformation_begin*nlig + i]; conformations_z_d[id_conformation*nlig + i] = conformations_z_d_mejora[id_conformation_begin*nlig + i]; }
                                moves_x_d[id_conformation] = moves_x_d_mejora[id_conformation_begin + j];
                                moves_y_d[id_conformation] = moves_y_d_mejora[id_conformation_begin + j];
                                moves_z_d[id_conformation] = moves_z_d_mejora[id_conformation_begin + j];
                                quat_d_x[id_conformation] = quat_d_mejora_x[id_conformation_begin + j];
                                quat_d_y[id_conformation] = quat_d_mejora_y[id_conformation_begin + j];
                                quat_d_z[id_conformation] = quat_d_mejora_z[id_conformation_begin + j];
                                quat_d_w[id_conformation] = quat_d_mejora_w[id_conformation_begin + j];
                                energy_d[id] = energy_d_mejora[id];
                        }
                        else
                        {
				for (int i=0;i<nlig;i++) { conformations_x_d_mejora[id_conformation_begin*nlig + i] = conformations_x_d[id_conformation*nlig + i]; conformations_y_d_mejora[id_conformation_begin*nlig + i] = conformations_y_d[id_conformation*nlig + i]; conformations_z_d_mejora[id_conformation_begin*nlig + i] = conformations_z_d[id_conformation*nlig + i]; }
                                moves_x_d_mejora[id_conformation_begin + j] = moves_x_d[id_conformation];
                                moves_y_d_mejora[id_conformation_begin + j] = moves_y_d[id_conformation];
                                moves_z_d_mejora[id_conformation_begin + j] = moves_z_d[id_conformation];
                                quat_d_mejora_x[id_conformation_begin + j] = quat_d_x[id_conformation];
                                quat_d_mejora_y[id_conformation_begin + j] = quat_d_y[id_conformation];
                                quat_d_mejora_z[id_conformation_begin + j] = quat_d_z[id_conformation];
                                quat_d_mejora_w[id_conformation_begin + j] = quat_d_w[id_conformation];
                                energy_d_mejora[id_conformation_begin + j] = energy_d[id_conformation];
                        }
                }
        }
}

__global__ void rotation (curandState_t * states, type_data max_angle, type_data * quaternions_x, type_data * quaternions_y, type_data * quaternions_z, type_data * quaternions_w, unsigned int max){

	type_data local_x, local_y, local_z, local_w, quat_x, quat_y, quat_z, quat_w;
	type_data angle, eje[3];
	curandState_t random_state;
	unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;
	
	if (id < max)
	{	
		random_state = states[id];
	
		quat_x = quaternions_x[id];
		quat_y = quaternions_y[id];
		quat_z = quaternions_z[id];
		quat_w = quaternions_w[id];

		angle = getRandomNumber_double(&random_state, max_angle);
		eje[0] = getRandomNumber_double(&random_state, 1);
		eje[1] = getRandomNumber_double(&random_state, 1);
		eje[2] = getRandomNumber_double(&random_state, 1);

		setRotation (&local_x, &local_y, &local_z, &local_w, angle, eje);
		composeRotation (&local_x, &local_y, &local_z, &local_w, &quat_x, &quat_y, &quat_z, &quat_w, &quat_x, &quat_y, &quat_z, &quat_w);

		normalize(&quat_x,&quat_y,&quat_z,&quat_w);

		quaternions_x[id] = quat_x;
		quaternions_y[id] = quat_y;
		quaternions_z[id] = quat_z;
		quaternions_w[id] = quat_w;
		
		states[id] = random_state;
	}
}


__global__ void rotation_montecarlo (curandState_t * states, type_data max_angle, type_data * quaternions_x, type_data * quaternions_y, type_data * quaternions_z, type_data * quaternions_w, type_data *quat_x_m, type_data *quat_y_m, type_data *quat_z_m, type_data *quat_w_m, unsigned int max){

	type_data local_x, local_y, local_z, local_w, quat_x, quat_y, quat_z, quat_w;
	type_data angle, eje[3];
	curandState_t random_state;
	unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;
	
	if (id < max)
	{	
		random_state = states[id];
		
		quaternions_x[id] = quat_x_m[id];
		quaternions_y[id] = quat_y_m[id];
		quaternions_z[id] = quat_z_m[id];
		quaternions_w[id] = quat_w_m[id];
		
		quat_x = quat_x_m[id];
		quat_y = quat_y_m[id];
		quat_z = quat_z_m[id];
		quat_w = quat_w_m[id];

		angle = getRandomNumber_double(&random_state, max_angle);
		eje[0] = getRandomNumber_double(&random_state, 1);
		eje[1] = getRandomNumber_double(&random_state, 1);
		eje[2] = getRandomNumber_double(&random_state, 1);

		setRotation (&local_x, &local_y, &local_z, &local_w, angle, eje);
		composeRotation (&local_x, &local_y, &local_z, &local_w, &quat_x, &quat_y, &quat_z, &quat_w, &quat_x, &quat_y, &quat_z, &quat_w);

		normalize(&quat_x,&quat_y,&quat_z,&quat_w);

		quat_x_m[id] = quat_x;
		quat_y_m[id] = quat_y;
		quat_z_m[id] = quat_z;
		quat_w_m[id] = quat_w;
		
		states[id] = random_state;
	}
}

__global__ void rotation_montecarlo_environment (curandState_t * states, type_data max_angle, type_data *quat_x_m, type_data *quat_y_m, type_data *quat_z_m, type_data *quat_w_m, unsigned int max){

        type_data local_x, local_y, local_z, local_w, quat_x, quat_y, quat_z, quat_w;
        type_data angle, eje[3];
        curandState_t random_state;
        unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;

        if (id < max)
        {
                random_state = states[id];

                //quaternions_x[id] = quat_x_m[id];
                //quaternions_y[id] = quat_y_m[id];
                //quaternions_z[id] = quat_z_m[id];
                //quaternions_w[id] = quat_w_m[id];

                quat_x = quat_x_m[id];
                quat_y = quat_y_m[id];
                quat_z = quat_z_m[id];
                quat_w = quat_w_m[id];

                angle = getRandomNumber_double(&random_state, max_angle);
                eje[0] = getRandomNumber_double(&random_state, 1);
                eje[1] = getRandomNumber_double(&random_state, 1);
                eje[2] = getRandomNumber_double(&random_state, 1);

                setRotation (&local_x, &local_y, &local_z, &local_w, angle, eje);
                composeRotation (&local_x, &local_y, &local_z, &local_w, &quat_x, &quat_y, &quat_z, &quat_w, &quat_x, &quat_y, &quat_z, &quat_w);

                normalize(&quat_x,&quat_y,&quat_z,&quat_w);

                quat_x_m[id] = quat_x;
                quat_y_m[id] = quat_y;
                quat_z_m[id] = quat_z;
                quat_w_m[id] = quat_w;

                states[id] = random_state;
        }
}


__global__ void move_vs (curandState_t * states, type_data radius_cutoff, type_data * moves_x_d, type_data * moves_y_d, type_data * moves_z_d, type_data ac_x, type_data ac_y, type_data ac_z, unsigned int max){

	curandState_t random_state;

	unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;
	if (id < max)
	{
		//printf("s_id %d\n",s_id);
		random_state = states[id];
		moves_x_d[id] = ac_x + getRandomNumber_double(&random_state, radius_cutoff); //Desplazamiento entre -max_desp y max_desp
		moves_y_d[id] = ac_y + getRandomNumber_double(&random_state, radius_cutoff);
		moves_z_d[id] = ac_z + getRandomNumber_double(&random_state, radius_cutoff);
		//if (id == 0) printf("x %f y %f z %f * %f %f %f\n",ac_x,ac_y,ac_z,moves_x_d[id],moves_y_d[id],moves_z_d[id]);
		states[id] = random_state;	
		//if (id==0) printf("moves_x_d[0]	%f moves_y_d[0] %f moves_z_d[0] %f\n",moves_x_d[id],moves_y_d[id],moves_z_d[id]);
	}
}


__global__ void move(curandState_t * states, type_data *surface_x, type_data *surface_y, type_data *surface_z, type_data radius_cutoff, type_data * moves_x_d, type_data * moves_y_d, type_data * moves_z_d, unsigned int start_point, unsigned int pc, unsigned int max){

	curandState_t random_state;

	unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;
	if (id < max)
	{
		unsigned int s_id = id/pc + start_point;
		//printf("s_id %d\n",s_id);
		random_state = states[id];
		moves_x_d[id] = surface_x[s_id] + getRandomNumber_double(&random_state, radius_cutoff); //Desplazamiento entre -max_desp y max_desp
		moves_y_d[id] = surface_y[s_id] + getRandomNumber_double(&random_state, radius_cutoff);
		moves_z_d[id] = surface_z[s_id] + getRandomNumber_double(&random_state, radius_cutoff);
		states[id] = random_state;		
	}
}

__global__ void move_mpi(curandState_t * states, type_data *surface_x, type_data *surface_y, type_data *surface_z, type_data radius_cutoff, type_data * moves_x_d, type_data * moves_y_d, type_data * moves_z_d, unsigned int pc, unsigned int g, unsigned int surface_openmpi,unsigned int start_point, unsigned int opt){

	curandState_t random_state;
		
	unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int s_id;
	if (opt)
		s_id = id/pc + start_point;
	else	
		s_id = id/pc + g * surface_openmpi;
	
	
	random_state = states[id];
	moves_x_d[id] = surface_x[s_id] + getRandomNumber_double(&random_state, radius_cutoff); //Desplazamiento entre -max_desp y max_desp
	moves_y_d[id] = surface_y[s_id] + getRandomNumber_double(&random_state, radius_cutoff);
	moves_z_d[id] = surface_z[s_id] + getRandomNumber_double(&random_state, radius_cutoff);	
	states[id] = random_state;
}

__global__ void move_mejora(curandState_t * states, type_data radius_cutoff, type_data * moves_x_d, type_data * moves_y_d, type_data * moves_z_d, unsigned int max){

	curandState_t random_state;

	unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;
	if (id < max)
	{
		random_state = states[id];		
		moves_x_d[id] += getRandomNumber_double(&random_state, radius_cutoff); //Desplazamiento entre -max_desp y max_desp
		moves_y_d[id] += getRandomNumber_double(&random_state, radius_cutoff);
		moves_z_d[id] += getRandomNumber_double(&random_state, radius_cutoff);
		states[id] = random_state;
	}
}

__global__ void move_mejora_montecarlo(curandState_t * states, type_data radius_cutoff, type_data * moves_x_d, type_data * moves_y_d, type_data * moves_z_d, type_data * move_x_m, type_data * move_y_m, type_data * move_z_m, unsigned int max){

	curandState_t random_state;

	unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;
	if (id < max)
	{
		random_state = states[id];
		moves_x_d[id] = move_x_m[id];
		moves_y_d[id] = move_y_m[id];
		moves_z_d[id] = move_z_m[id];
		
		move_x_m[id] += getRandomNumber_double(&random_state, radius_cutoff); //Desplazamiento entre -max_desp y max_desp
		move_y_m[id] += getRandomNumber_double(&random_state, radius_cutoff);
		move_z_m[id] += getRandomNumber_double(&random_state, radius_cutoff);
		states[id] = random_state;
	}
}

__global__ void move_mejora_montecarlo_environment (curandState_t * states, type_data radius_cutoff, type_data * move_x_m, type_data * move_y_m, type_data * move_z_m, unsigned int max){

        curandState_t random_state;

        unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;
        if (id < max)
        {
                random_state = states[id];
                //moves_x_d[id] = move_x_m[id];
                //moves_y_d[id] = move_y_m[id];
                //moves_z_d[id] = move_z_m[id];

                move_x_m[id] += getRandomNumber_double(&random_state, radius_cutoff); //Desplazamiento entre -max_desp y max_desp
                move_y_m[id] += getRandomNumber_double(&random_state, radius_cutoff);
                move_z_m[id] += getRandomNumber_double(&random_state, radius_cutoff);
                states[id] = random_state;
        }
}

__global__ void updateRotation (curandState_t * states, type_data max_angle,  type_data *quaternions_x, type_data *quaternions_y, type_data *quaternions_z, type_data *quaternions_w, type_data *quat_x_m, type_data *quat_y_m, type_data *quat_z_m, type_data *quat_w_m, type_data * energy_sig, type_data* energy_ant, unsigned int max){

	curandState_t random_state;
	type_data angle, eje[3];
	type_data invRot_x, invRot_y, invRot_z, invRot_w, quat_x, quat_y, quat_z, quat_w;
	unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;
	
	if (id < max)
	{
		random_state = states[id];

		/*angle = getRandomNumber_double (&random_state, max_angle);
		eje[0] = getRandomNumber_double(&random_state, 1);
		eje[1] = getRandomNumber_double(&random_state, 1);
		eje[2] = getRandomNumber_double(&random_state, 1);*/


		type_data cond = (energy_sig[id] < energy_ant[id] || getRandomNumber_double(&random_state) <= exp(-(energy_sig[id] - energy_ant[id])) );
		//if ((energy_ant[id] > -116.7) && (energy_ant[id] < -116.5)) printf("ANT id %d cond %f aux %f q_x %f q_y %f q_z %f",id,cond,eje[0],quaternions_x[id],quaternions_y[id],quaternions_z[id]);

		//setRotation(&invRot_x,&invRot_y,&invRot_z,&invRot_w, angle * !cond, eje);		
		//inverse(&invRot_x,&invRot_y,&invRot_z,&invRot_w, &invRot_x,&invRot_y,&invRot_z,&invRot_w);*/
		if (cond)
		{
			quaternions_x[id] = quat_x_m[id] * cond + quaternions_x[id] * !cond;		
			quaternions_y[id] = quat_y_m[id] * cond + quaternions_y[id] * !cond;
			quaternions_z[id] = quat_z_m[id] * cond + quaternions_z[id] * !cond;
			quaternions_w[id] = quat_w_m[id] * cond + quaternions_w[id] * !cond;
		}
		else
		{
			quat_x_m[id] = quaternions_x[id] * !cond;
			quat_y_m[id] = quaternions_y[id] * !cond;
			quat_z_m[id] = quaternions_z[id] * !cond;
			quat_w_m[id] = quaternions_w[id] * !cond;
		}
		/*quat_x = quaternions_x[id];
		quat_y = quaternions_y[id];
		quat_z = quaternions_z[id];
		quat_w = quaternions_w[id];
		composeRotation (&invRot_x,&invRot_y,&invRot_z,&invRot_w, &quat_x, &quat_y, &quat_z, &quat_w, &quat_x, &quat_y, &quat_z, &quat_w);

		quaternions_x[id] = quat_x;
		quaternions_y[id] = quat_y;
		quaternions_z[id] = quat_z;
		quaternions_w[id] = quat_w;*/
		
		energy_ant[id] = energy_sig[id] * cond + energy_ant[id] * !cond;
		//if ((energy_ant[id] > -116.7) && (energy_ant[id] < -116.5)) printf("   Lo que queda q_x %f q_y %f q_z %f q_w %f energy %f \n",quaternions_x[id],quaternions_y[id],quaternions_z[id],quaternions_w[id],energy_ant[id]);
		states[id] = random_state;
		//if (id == 31) printf("UPD ROT id %d cond %f q_x %f q_y %f q_z %f energy_sig %f energy_ant %f\n",id,cond,quaternions_x[id],quaternions_y[id],quaternions_z[id], energy_sig[id], energy_ant[id]);
	}

}


__global__ void updateRotation_environment (curandState_t * states, type_data max_angle,  type_data *quaternions_x, type_data *quaternions_y, type_data *quaternions_z, type_data *quaternions_w, type_data *quat_x_m, type_data *quat_y_m, type_data *quat_z_m, type_data *quat_w_m, type_data * energy_sig, type_data* energy_ant, int NEEImp, unsigned int max){

        curandState_t random_state;
        type_data angle, eje[3];
        type_data invRot_x, invRot_y, invRot_z, invRot_w, quat_x, quat_y, quat_z, quat_w;
        unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;

        if (id < max)
        {
		for (int j = 0; j < NEEImp; j++)
		{
                	random_state = states[id*NEEImp + j];

                	/*angle = getRandomNumber_double (&random_state, max_angle);
                	eje[0] = getRandomNumber_double(&random_state, 1);
                	eje[1] = getRandomNumber_double(&random_state, 1);
                	eje[2] = getRandomNumber_double(&random_state, 1);*/


                	type_data cond = (energy_sig[id*NEEImp + j] < energy_ant[id] || getRandomNumber_double(&random_state) <= exp(-(energy_sig[id*NEEImp + j] - energy_ant[id])) );
                	//if ((energy_ant[id] > -116.7) && (energy_ant[id] < -116.5)) printf("ANT id %d cond %f aux %f q_x %f q_y %f q_z %f",id,cond,eje[0],quaternions_x[id],quaternions_y[id],quaternions_z[id]);

                	//setRotation(&invRot_x,&invRot_y,&invRot_z,&invRot_w, angle * !cond, eje);
                	//inverse(&invRot_x,&invRot_y,&invRot_z,&invRot_w, &invRot_x,&invRot_y,&invRot_z,&invRot_w);*/
                	if (cond)
                	{
                        	quaternions_x[id] = quat_x_m[id*NEEImp + j] * cond + quaternions_x[id] * !cond;
                        	quaternions_y[id] = quat_y_m[id*NEEImp + j] * cond + quaternions_y[id] * !cond;
                        	quaternions_z[id] = quat_z_m[id*NEEImp + j] * cond + quaternions_z[id] * !cond;
                        	quaternions_w[id] = quat_w_m[id*NEEImp + j] * cond + quaternions_w[id] * !cond;
                	}
                	else
                	{
                        	quat_x_m[id*NEEImp + j] = quaternions_x[id] * !cond;
                        	quat_y_m[id*NEEImp + j] = quaternions_y[id] * !cond;
                        	quat_z_m[id*NEEImp + j] = quaternions_z[id] * !cond;
                        	quat_w_m[id*NEEImp + j] = quaternions_w[id] * !cond;
                	}
			/*quat_x = quaternions_x[id];
                	quat_y = quaternions_y[id];
                	quat_z = quaternions_z[id];
                	quat_w = quaternions_w[id];
                	composeRotation (&invRot_x,&invRot_y,&invRot_z,&invRot_w, &quat_x, &quat_y, &quat_z, &quat_w, &quat_x, &quat_y, &quat_z, &quat_w);

                	quaternions_x[id] = quat_x;
                	quaternions_y[id] = quat_y;
                	quaternions_z[id] = quat_z;
                	quaternions_w[id] = quat_w;*/

                	energy_ant[id] = energy_sig[id*NEEImp + j] * cond + energy_ant[id] * !cond;
                	//if ((energy_ant[id] > -116.7) && (energy_ant[id] < -116.5)) printf("   Lo que queda q_x %f q_y %f q_z %f q_w %f energy %f \n",quaternions_x[id],quaternions_y[id],quaternions_z[id],quaternions_w[id],energy_ant[id]);
                	states[id*NEEImp + j] = random_state;
                	//if (id == 31) printf("UPD ROT id %d cond %f q_x %f q_y %f q_z %f energy_sig %f energy_ant %f\n",id,cond,quaternions_x[id],quaternions_y[id],quaternions_z[id], energy_sig[id], energy_ant[id]);
		}
        }

}


__global__ void updateMove(curandState_t * states, type_data max_desp,  type_data* moves_x_d, type_data* moves_y_d, type_data* moves_z_d, type_data* move_x_m, type_data* move_y_m, type_data* move_z_m, type_data* energy_sig, type_data* energy_ant, unsigned int max){

	curandState_t random_state;
	//type_data aux, aux2, aux3;
	unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;

	if (id < max)
	{
		random_state = states[id];

		/*aux  = getRandomNumber_double(&random_state, max_desp); //Desplazamiento entre -max_desp y max_desp
		aux2 = getRandomNumber_double(&random_state, max_desp);
		aux3 = getRandomNumber_double(&random_state, max_desp);*/

		type_data cond = (energy_sig[id] < energy_ant[id] || getRandomNumber_double(&random_state) <= exp(-(energy_sig[id] - energy_ant[id])) );
		//if ((energy_sig[id] > -115.8) && (energy_sig[id] < -115.5)) printf("SIG id %d cond %f aux %f, m_x %f m_y %f m_z %f energy %f  ",id,cond,aux,moves_x_d[id],moves_y_d[id],moves_z_d[id],energy_ant[id]);		
		//if ((energy_ant[id] > -115.8) && (energy_ant[id] < -115.5)) printf("ANT id %d cond %f m_x %f m_y %f m_z %f",id,cond,moves_x_d[id],moves_y_d[id],moves_z_d[id]);
		if (cond)
		{
			moves_x_d[id] = move_x_m[id] * cond + moves_x_d[id] * !cond;
			moves_y_d[id] = move_y_m[id] * cond + moves_y_d[id] * !cond;
			moves_z_d[id] = move_z_m[id] * cond + moves_z_d[id] * !cond;
		}
		else
		{
			move_x_m[id] = moves_x_d[id] * !cond;
			move_y_m[id] = moves_y_d[id] * !cond;
			move_z_m[id] = moves_z_d[id] * !cond;
		}
		energy_ant[id] = energy_sig[id] * cond + energy_ant[id] * !cond;
		//if ((energy_sig[id] > -115.7) && (energy_sig[id] < -115.5)) printf("SIG Se va con m_x %f m_y %f m_z %f energy %f \n",moves_x_d[id],moves_y_d[id],moves_z_d[id], energy_ant[id]);		
		//if ((energy_ant[id] > -115.7) && (energy_sig[id] < -115.5)) printf("ANT Se va con m_x %f m_y %f m_z %f energy %f \n",moves_x_d[id],moves_y_d[id],moves_z_d[id], energy_ant[id]);

		states[id] = random_state;
		//if (id == 2) printf("UPD MOVE id %d cond %d m_x %f m_y %f m_z %f energy_sig %f energy_ant %f\n",id,cond,moves_x_d[id],moves_y_d[id],moves_z_d[id], energy_sig[id], energy_ant[id]);
	}
}

__global__ void updateMove_environment(curandState_t * states, type_data max_desp,  type_data* moves_x_d, type_data* moves_y_d, type_data* moves_z_d, type_data* move_x_m, type_data* move_y_m, type_data* move_z_m, type_data* energy_sig, type_data* energy_ant, int NEEImp, unsigned int max){

        curandState_t random_state;
        //type_data aux, aux2, aux3;
        unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;

        if (id < max)
        {

		for (int j = 0; j < NEEImp; j++)
		{
                	random_state = states[id*NEEImp + j];

                	/*aux  = getRandomNumber_double(&random_state, max_desp); //Desplazamiento entre -max_desp y max_desp
                	aux2 = getRandomNumber_double(&random_state, max_desp);
                	aux3 = getRandomNumber_double(&random_state, max_desp);*/

                	type_data cond = (energy_sig[id*NEEImp + j] < energy_ant[id] || getRandomNumber_double(&random_state) <= exp(-(energy_sig[id*NEEImp + j] - energy_ant[id])) );
                //if ((energy_sig[id] > -115.8) && (energy_sig[id] < -115.5)) printf("SIG id %d cond %f aux %f, m_x %f m_y %f m_z %f energy %f  ",id,cond,aux,moves_x_d[id],moves_y_d[id],moves_z_d[id],energy_ant[id]);
                //if ((energy_ant[id] > -115.8) && (energy_ant[id] < -115.5)) printf("ANT id %d cond %f m_x %f m_y %f m_z %f",id,cond,moves_x_d[id],moves_y_d[id],moves_z_d[id]);
                	if (cond)
                	{
                        	moves_x_d[id] = move_x_m[id*NEEImp + j] * cond + moves_x_d[id] * !cond;
                        	moves_y_d[id] = move_y_m[id*NEEImp + j] * cond + moves_y_d[id] * !cond;
                        	moves_z_d[id] = move_z_m[id*NEEImp + j] * cond + moves_z_d[id] * !cond;
                	}
                	else
                	{
                        	move_x_m[id*NEEImp + j] = moves_x_d[id] * !cond;
                        	move_y_m[id*NEEImp + j] = moves_y_d[id] * !cond;
                        	move_z_m[id*NEEImp + j] = moves_z_d[id] * !cond;
                	}
                	energy_ant[id] = energy_sig[id*NEEImp + j] * cond + energy_ant[id] * !cond;
                //if ((energy_sig[id] > -115.7) && (energy_sig[id] < -115.5)) printf("SIG Se va con m_x %f m_y %f m_z %f energy %f \n",moves_x_d[id],moves_y_d[id],moves_z_d[id], energy_ant[id]);
                //if ((energy_ant[id] > -115.7) && (energy_sig[id] < -115.5)) printf("ANT Se va con m_x %f m_y %f m_z %f energy %f \n",moves_x_d[id],moves_y_d[id],moves_z_d[id], energy_ant[id]);

                	states[id*NEEImp + j] = random_state;
		}
                //if (id == 2) printf("UPD MOVE id %d cond %d m_x %f m_y %f m_z %f energy_sig %f energy_ant %f\n",id,cond,moves_x_d[id],moves_y_d[id],moves_z_d[id], energy_sig[id], energy_ant[id]);
        }
}

__global__ void angulations_conformations (curandState_t * states, struct flexibility_data_t * flexibility_conformations, unsigned int n_links, type_data max_angle_flex, unsigned int max)
{	
	//OBTENEMOS LA CONFORMACION
	unsigned int tid = threadIdx.x + threadIdx.y * blockDim.x;
	unsigned int blockID = blockIdx.x + blockIdx.y * gridDim.x;
	unsigned int id_conformation = blockID * (blockDim.x * blockDim.y) + (threadIdx.y*blockDim.x) + threadIdx.x;
	//int id_conformation;
	if (id_conformation < max)
	{

		//OBTENEMOS EL ANGULO EN RADIANES ALEATORIO ENTRE UN MAXIMO.
		type_data t_p = getRandomNumber_double (&states[id_conformation], max_angle_flex);
		//ENLACE Y FRAGMENTO A GIRAR ALEATORIO
		int link =  getRandomNumber(&states[id_conformation]) % n_links;
		//OBTENEMOS EL PUNTO DEL ENLACE DESDE DONDE ROTAR
		int rot_point = getRandomNumber(&states[id_conformation]) % 2;

		//GUARDAR LOS VALORES
		flexibility_conformations[id_conformation].ang = (t_p * PI) / 180;
		flexibility_conformations[id_conformation].link = link;
		flexibility_conformations[id_conformation].p_rot = rot_point;
	}

}

__global__ void update_angulations_conformations (curandState_t * states, struct flexibility_data_t * flexibility_conformations, type_data * energy_sig, type_data* energy_ant, unsigned int max)
{
	curandState_t random_state;
	type_data tmp;	
	unsigned int tid = threadIdx.x + threadIdx.y * blockDim.x;
	unsigned int blockID = blockIdx.x + blockIdx.y * gridDim.x;
	unsigned int id_conformation = blockID * (blockDim.x * blockDim.y) + (threadIdx.y*blockDim.x) + threadIdx.x;
	//int id_conformation;
	if (id_conformation < max)
	{
		random_state = states[id_conformation];
		type_data cond = (energy_sig[id_conformation] < energy_ant[id_conformation] || getRandomNumber_double(&random_state) <= exp(-(energy_sig[id_conformation] - energy_ant[id_conformation])) );

		tmp = flexibility_conformations[id_conformation].ang  * !cond * (-1);
		flexibility_conformations[id_conformation].ang = tmp;

		energy_ant[id_conformation] = energy_sig[id_conformation] * cond + energy_ant[id_conformation] * !cond;
		states[id_conformation] = random_state;
	}
}

__global__ void update_angulations_conformations_environment (unsigned int tam, curandState_t * states, struct flexibility_data_t * flexibility_conformations, type_data * energy_sig, type_data* energy_ant, unsigned int max)
{
        curandState_t random_state;
        type_data tmp;
        unsigned int tid = threadIdx.x + threadIdx.y * blockDim.x;
        unsigned int blockID = blockIdx.x + blockIdx.y * gridDim.x;
        unsigned int id_conformation = blockID * (blockDim.x * blockDim.y) + (threadIdx.y*blockDim.x) + threadIdx.x;
        //int id_conformation;
        if (id_conformation < max)
        {
                random_state = states[id_conformation];
                type_data cond = (energy_sig[id_conformation] < energy_ant[id_conformation/tam] || getRandomNumber_double(&random_state) <= exp(-(energy_sig[id_conformation] - energy_ant[id_conformation/tam])) );

                tmp = flexibility_conformations[id_conformation].ang  * !cond * (-1);
                flexibility_conformations[id_conformation].ang = tmp;

                energy_ant[id_conformation/tam] = energy_sig[id_conformation] * cond + energy_ant[id_conformation/tam] * !cond;
                states[id_conformation] = random_state;
        }
}

__global__ void update_angulations_conformations_tradicional (curandState_t * states, struct flexibility_data_t * flexibility_conformations, type_data * energy_sig, type_data* energy_ant, unsigned int max)
{
	curandState_t random_state;
	type_data tmp;	
	unsigned int tid = threadIdx.x + threadIdx.y * blockDim.x;
	unsigned int blockID = blockIdx.x + blockIdx.y * gridDim.x;
	unsigned int id_conformation = blockID * (blockDim.x * blockDim.y) + (threadIdx.y*blockDim.x) + threadIdx.x;	
	if (id_conformation < max)
	{
		random_state = states[id_conformation];
		type_data cond = (energy_sig[id_conformation] < energy_ant[id_conformation]);// || getRandomNumber_double(&random_state) <= exp(-(energy_sig[id_conformation] - energy_ant[id_conformation])) );

		tmp = flexibility_conformations[id_conformation].ang  * !cond * (-1);
		flexibility_conformations[id_conformation].ang = tmp;

		energy_ant[id_conformation] = energy_sig[id_conformation] * cond + energy_ant[id_conformation] * !cond;
		states[id_conformation] = random_state;
	}
}

__global__ void update_angulations_conformations_tradicional_environment (unsigned int tam, curandState_t * states, struct flexibility_data_t * flexibility_conformations, type_data * energy_sig, type_data* energy_ant, unsigned int max)
{
        curandState_t random_state;
        type_data tmp;
        unsigned int tid = threadIdx.x + threadIdx.y * blockDim.x;
        unsigned int blockID = blockIdx.x + blockIdx.y * gridDim.x;
        unsigned int id_conformation = blockID * (blockDim.x * blockDim.y) + (threadIdx.y*blockDim.x) + threadIdx.x;
        if (id_conformation < max)
        {
                random_state = states[id_conformation];
                type_data cond = (energy_sig[id_conformation] < energy_ant[id_conformation/tam]);// || getRandomNumber_double(&random_state) <= exp(-(energy_sig[id_conformation] - energy_ant[id_conformation])) );

                tmp = flexibility_conformations[id_conformation].ang  * !cond * (-1);
                flexibility_conformations[id_conformation].ang = tmp;

                energy_ant[id_conformation/tam] = energy_sig[id_conformation] * cond + energy_ant[id_conformation/tam] * !cond;
                states[id_conformation] = random_state;
        }
}

__global__ void angulations_conformations_vs (curandState_t * states, struct flexibility_data_t * flexibility_conformations, struct flexibility_params_t *flexibility_params, type_data max_angle_flex, unsigned int stride_d, unsigned int conf_point, unsigned int max)
{	
	//OBTENEMOS LA CONFORMACION
	unsigned int tid = threadIdx.x + threadIdx.y * blockDim.x;
	unsigned int blockID = blockIdx.x + blockIdx.y * gridDim.x;
	unsigned int id_conformation = blockID * (blockDim.x * blockDim.y) + (threadIdx.y*blockDim.x) + threadIdx.x;
	unsigned int file = (id_conformation+stride_d) / conf_point;
	unsigned int n_links;
	//int id_conformation;
	if (id_conformation < max)
	{
		n_links = flexibility_params[file].n_links;
		//OBTENEMOS EL ANGULO EN RADIANES ALEATORIO ENTRE UN MAXIMO.
		type_data t_p = getRandomNumber_double (&states[id_conformation], max_angle_flex);
		//ENLACE Y FRAGMENTO A GIRAR ALEATORIO
		int link =  getRandomNumber(&states[id_conformation]) % n_links;
		//OBTENEMOS EL PUNTO DEL ENLACE DESDE DONDE ROTAR
		int rot_point = getRandomNumber(&states[id_conformation]) % 2;
		
		//GUARDAR LOS VALORES
		flexibility_conformations[id_conformation].ang = (t_p * PI) / 180;
		flexibility_conformations[id_conformation].link = link;
		flexibility_conformations[id_conformation].p_rot = rot_point;
	}

}

