#include <dirent.h>
#include <iostream>
#include <vector>
#include <openbabel/obconversion.h>
#include <openbabel/mol.h>
#include <openbabel/bond.h>
#include <openbabel/obiter.h>
#include <openbabel/bitvec.h>
#include "energy_struct.h"
#include "energy_common.h"

using namespace OpenBabel;

extern unsigned int rotable_ligand (char *filename)
{
	
	OpenBabel::OBConversion conv;
	OpenBabel::OBMol mol;
	unsigned int n_links = 0;
	bool success = conv.SetInFormat("Mol2");
	unsigned int natoms;
	if (success)
	{
		bool readed = conv.ReadFile(&mol,filename);
		if (readed)
			n_links = mol.NumRotors();
	}
	return(n_links);
} 

extern void ruta (char *cadena_in, char *cadena_ruta)
{
	unsigned int l1 = strlen(cadena_in);
	char c;
	int count = 0, i = 0, k = 0;
	//printf("cadena_in %s\n",cadena_in);
	count = l1-1;
	c = cadena_in[count];
	while (c != '/')
	{
		count--;
		c = cadena_in[count];
	}
		
	while (i < count)
	{
		cadena_ruta[k] = cadena_in[i];
		i++; k++;
	}
	
	cadena_ruta[k] = '\0';
	//printf("cadena_ruta %s\n",cadena_ruta);
}

extern void nombre_dir(char *cadena_in, char *cadena_out)
{
	unsigned int l1 = strlen(cadena_in);
        char c;
        unsigned int count = 0, i = 0, k = 0;
        //printf("cadena_in %s\n",cadena_in);
        count = l1-1;
	i = count;

        c = cadena_in[count];
	if (c == '/') c--;
        while ((c != '/') && (count != 0))
        {
        	count--;
                c = cadena_in[count];
		//printf("%c\n",c);
        }
	if (count != 0) count++;
	//printf("count %d i %d\n",count,i);
	while ((count != i) && (cadena_in[count] != '/'))
        {
                c = cadena_in[count];
		//printf("%c\n",c);
		cadena_out[k] = c;
                count++; k++;
        }
	cadena_out[k]='\0';
}

extern void nombre_fichero_completo (char *cadena_in, char *cadena_out)
{
	unsigned int l1 = strlen(cadena_in);
	char c;
	unsigned int count = 0, i = 0;
	//printf("cadena_in %s\n",cadena_in);
	count = l1-1;
	c = cadena_in[count];
	while (c != '/')
	{
		count--;
		c = cadena_in[count];
	}
	count++;
	while (count < l1)
	{
		cadena_out[i] = cadena_in[count];
		i++; count++; 
	}
	cadena_out[i] = '/0';

	/*c = cadena_in[count];
	while (c != '/')
	{
		c = cadena_in[count];
		//printf("c %c \n",&c);
		count++;
	}
	c = cadena_in[count];
	while (c != '/')
	{
		c = cadena_in[count];
		//printf("c %c \n",&c);
		count++;
	}
	c = cadena_in[count];
	while (c != '/')
	{
		c = cadena_in[count];
		count++;
	}
	i=0;
	while ((count < l1) && (c != '.'))
	{
		
		if (c != '.')
		{
			cadena_out[i] = cadena_in [count];
			count++;
			i++;
		}
	}
	cadena_out[i] = '\0';*/
}
extern void nombre_fichero (char *cadena_in, char *cadena_out)
{
	unsigned int l1 = strlen(cadena_in);
	char c;
	unsigned int count_1 = 0,count=0;
	while ((count_1 < l1) && (c != '.'))
	{
		c = cadena_in[count_1];
		if (c != '.')
		{
			cadena_out[count] = cadena_in[count_1];
			count++;	
		}
		count_1++;
	}
	cadena_out[count] = '\0';
}
extern void divide_ligands (char *input_dir, char *output_dir)
{
	struct dirent *ent;
	char *cadena_e = (char *)malloc(sizeof(char)*80);
	char *nombre_f = (char *)malloc(sizeof(char)*80);
	char *dir_r = (char *)malloc(sizeof(char)*100);
	char *dir_f = (char *)malloc(sizeof(char)*100);
	char *orden = (char *)malloc(sizeof(char)*200);
	char c;
	unsigned int lnk;
	DIR *dir;
	
	//system("date");		
			
	dir = opendir(input_dir);
	if (dir == NULL)
	{
		printf("ERROR open directory %s\n",input);
		exit(1);
	}
	
	while ((ent = readdir(dir)) != NULL)
	{
		if ((strcmp(ent->d_name, ".") != 0) && (strcmp(ent->d_name, "..") != 0) && (strcmp(ent->d_name, "flex") != 0) && (strcmp(ent->d_name, "rigid") != 0))
		{		
			sprintf(cadena_e,"%s/%s",input_dir,ent->d_name);
			nombre_fichero(ent->d_name,nombre_f);
			lnk = rotable_ligand(cadena_e);			
			//printf("Ligando %s lnk %d\n",cadena_e,lnk);	
			if (lnk > 0)
			{
				//Ligando flexible 
				sprintf(dir_f,"%s/flex/%s",input_dir,ent->d_name);	
				sprintf(orden,"cp %s %s",cadena_e,dir_f);
				//printf("Orden F: %s\n",orden);
				system(orden);				
			}
			else
			{
				//ligando rigido
				sprintf(dir_r,"%s/rigid/%s-rig.mol2",input_dir,nombre_f);	
				sprintf(orden,"cp %s %s",cadena_e,dir_r);
				//printf("Orden R: %s\n",orden);
				system(orden);
			}
			//sprintf(orden,"mkdir -p %s/%s",output_dir,nombre_f);
			//system(orden);
		}
	}
	closedir(dir);
}

extern void convert_into_mol2 (char *filename,char *filename_1)
{
	OpenBabel::OBConversion conv;
        OpenBabel::OBMol mol;

	bool success = conv.SetInFormat("Mol2");
	if (success)
	{
		bool readed = conv.ReadFile(&mol,filename);
                if (readed)
                {
			success = conv.SetOutFormat("Mol2");
			if (!success) printf("NO exito para escritura");
			bool writed = conv.WriteFile(&mol,filename_1);
			if (!writed) printf("NO escrito\n");
		}
		else
			printf("NO leido\n");
	}
	else
		printf("NO exito para lectura\n");
}
extern void ligand_babel (struct param_t param, char *filename,struct flexibility_params_t *flexibility_params, unsigned int level_bonds_VDW,unsigned int level_bonds_ES)
{
	//std::vector<int> **f;
	//std::vector<int> f1,f2,f3,f4,f5,f6,f7,f8,f9,f10;
	
	int i = 0, j, k, l;

	OpenBabel::OBConversion conv;
	OpenBabel::OBMol mol;
	OpenBabel::OBAtom a,c,d,e;	
	OpenBabel::OBBond bond;
	OpenBabel::OBBond *b;
	OpenBabel::OBBitVec frag;

	bool success = conv.SetInFormat("Mol2");
	bool readed = conv.ReadFile(&mol,filename);
        int natoms;
        //printf("fichero %s\n",filename);
	//exit(0);
	if (success)
	{
	flexibility_params->n_links = mol.NumRotors();
	if (flexibility_params->n_links > 0)
	{
		if (param.rot_type == 1)
		{
			read_pdbqt(param,filename,&flexibility_params->n_links,flexibility_params->links);
			printf("Links %d\n",flexibility_params->n_links);
			for (j=0; j < flexibility_params->n_links*2;j+=2) printf("link %d to %d\n",flexibility_params->links[j],flexibility_params->links[j+1]);
			//exit(0);
		
		}	
		else
		{
			//bool readed = conv.ReadFile(&mol,filename);
			if (readed)
			{
				//MANUAL
				//n_links = 16;
				flexibility_params->n_links = mol.NumRotors();
				flexibility_params->links = (int *)calloc(flexibility_params->n_links * 2,sizeof(int));
			}
		}
	

		natoms = mol.NumAtoms();
		//flexibility_params->links = (int *)calloc(flexibility_params->n_links * 2,sizeof(int));
		flexibility_params->fragments = (int * )calloc(flexibility_params->n_links * (natoms - 2),sizeof(int));
		
		flexibility_params->n_fragments = flexibility_params->n_links * 2;
		flexibility_params->fragments_tam = (int *)calloc(flexibility_params->n_fragments,sizeof(int));
		flexibility_params->links_fragments = (int *)calloc(flexibility_params->n_fragments,sizeof(int));
		
		std::vector< std::vector<int> > f(flexibility_params->n_fragments);
					
		//GENERAR ENLACES
		flexibility_params->individual_bonds_VDW = (bool *)calloc(natoms * natoms,sizeof(bool));
		flexibility_params->individual_bonds_ES = (bool *)calloc(natoms * natoms,sizeof(bool));
		
		//exit(0);
		//int i = 0, j, k, l;
		FOR_ATOMS_OF_MOL (a, mol)
		{
			i = a->GetId();
			//printf("Atomo: %d\n",i+1);
			FOR_NBORS_OF_ATOM(c, *a)
			{
				j = c->GetId();
				flexibility_params->individual_bonds_VDW[i*natoms + j] = 1;
				if (level_bonds_ES > 1) 
					flexibility_params->individual_bonds_ES[i*natoms + j] = 1;
					if ((level_bonds_VDW > 2) || (level_bonds_ES > 2)) 
					{	
						FOR_NBORS_OF_ATOM(d,*c)
						{
							k = d->GetId();
							if (level_bonds_VDW > 2)
								flexibility_params->individual_bonds_VDW[i*natoms + k] = 1;
							if (level_bonds_ES > 2) 
								flexibility_params->individual_bonds_ES[i*natoms + k] = 1;
							
							if ((level_bonds_VDW > 3) || (level_bonds_ES > 3))
							{				
								FOR_NBORS_OF_ATOM(e,*d)
								{
									l = e->GetId();
									if (level_bonds_VDW > 3)
										flexibility_params->individual_bonds_VDW[i*natoms + l] = 1;
									if (level_bonds_ES > 3)
										flexibility_params->individual_bonds_ES[i*natoms + l] = 1;															
								}
							}
						}
					}
			}		
		}
	
	
		if (param.rot_type != 1)
		{
			i = 0;
			j = 2;
			///exit(0);		
			i = (flexibility_params->n_links * 2) - j;
			int ref = i;
			FOR_BONDS_OF_MOL (b,mol)
			{
				bool rotable = b->IsRotor();
				if (rotable)
				{
					if (i < 0)
					{					
						i = ref - 2;												
						ref = ref - 2;
					}
				
					flexibility_params->links[i]   = b->GetBeginAtomIdx();
					flexibility_params->links[i+1] = b->GetEndAtomIdx();
					i = i - 2*(flexibility_params->n_links/2);					
				}				
			}
		}
		//MANUALMENTE
		/*links_h[0] = 1; links_h[1] = 42;
		links_h[2] = 15; links_h[3] = 17;
		links_h[4] = 17; links_h[5] = 18;
		links_h[6] = 21; links_h[7] = 22;
		links_h[8] = 22; links_h[9] = 56;
		links_h[10] = 25; links_h[11] = 26;
		links_h[12] = 26; links_h[13] = 27;
		links_h[14] = 33; links_h[15] = 35;
		links_h[16] = 35; links_h[17] = 36;
		links_h[18] = 36; links_h[19] = 51;
		links_h[20] = 39; links_h[21] = 40;
		links_h[22] = 40; links_h[23] = 46;
		links_h[24] = 42; links_h[25] = 44;
		links_h[26] = 46; links_h[27] = 47;
		inks_h[28] = 51; links_h[29] = 52;
		links_h[30] = 56; links_h[31] = 57;*/			
		
		//CONFIGURAR FRAGMENTOS				
		//exit(0);
		for (j = 0; j < (flexibility_params->n_fragments-1);j=j+2)
		{
			mol.FindChildren(f[j],flexibility_params->links[j+1],flexibility_params->links[j]);
			mol.FindChildren(f[j+1],flexibility_params->links[j],flexibility_params->links[j+1]);
		}									
		int i1 = 0;
		for (j = 0; j < flexibility_params->n_fragments; j++)
		{
			for (std::vector<int>::iterator i = f[j].begin(); i != f[j].end(); i++)
			{
				flexibility_params->fragments[i1] = *i;
				i1 = i1 + 1;
			}				
		}			
		//CAPTURAR TAMAÑO FRAGMENTOS
		//exit(0);
		for (i = 0; i < flexibility_params->n_fragments;i++)
		{
			flexibility_params->fragments_tam[i] = (int)f[i].capacity();
			flexibility_params->links_fragments[i] = i+1;						
		}
	}
	else flexibility_params->n_links=0;
	}
	else
        {
                printf("Error la configuración del formato en OpenBabel.\n");
                exit (1);
        }
} 
