#ifndef ROTATION_H
#define ROTATION_H

#include <math.h>
//#include "vector_types.h"


extern void setRotation_cpp (type_data *q_x, type_data *q_y, type_data *q_z, type_data *q_w,  type_data angle, type_data * vector);
extern type_data normalize_cpp (type_data *q_x, type_data *q_y, type_data *q_z, type_data *q_w);
extern void composeRotation_cpp (type_data *p_x, type_data *p_y, type_data *p_z, type_data *p_w, type_data *q_x, type_data *q_y, type_data *q_z, type_data *q_w, type_data *dst_x, type_data *dst_y, type_data *dst_z, type_data *dst_w);
extern void inverse_cpp (type_data *src_x, type_data *src_y, type_data *src_z, type_data *src_w, type_data *dst_x, type_data *dst_y, type_data *dst_z, type_data *dst_w);
extern void rotate3DPoint_cpp (type_data *q_x, type_data *q_y, type_data *q_z, type_data *q_w, type_data * src1, type_data * src2, type_data * src3, type_data * dst);

#endif
