#include "definitions.h"
#include "wtime.h"
#include "energy_common.h"
#include "energy_struct.h"
#include "energy_GpuSolver_vs.h"
#include "energy_positions_vs.h"
#include "energy_positions_common.h"
#include "energy_montecarlo.h"
#include "metaheuristic_mgpu.h"
#include "metaheuristic_mgpu_common.h"
#include "metaheuristic_ini_vs.h"
#include "metaheuristic_mgpu_vs_common.h"
#include <thrust/sequence.h>
#include <thrust/sort.h>
#include <omp.h>

using namespace std;

extern void divide_charge_vs (unsigned int surface, unsigned int numprocess, unsigned int *points_by_process, unsigned int *points_by_begin, unsigned int *order_node, double *time_warm_all)
{
	/*double sum = 0;
	int *work;
	type_data *w_t, *w_t_1;
	type_data tmp, ttotal_1;
	int tmp_1, ttotal = 0,k, reparto,tmp_surface;
	char tmp_nodes[MPI_MAX_PROCESSOR_NAME];
	
	work = (int *)malloc(sizeof(int)*(numprocess-1));
	w_t = (type_data *)malloc(sizeof(type_data)*(numprocess-1));
	w_t_1 = (type_data *)calloc(sizeof(type_data),(numprocess-1));

	for (k=0; k < (numprocess-1);k++) printf("time %f node %d\n",time_warm_all[k],order_node[k]);
	quicksort(time_warm_all,order_node,0,(numprocess-1) - 1);

	//for (k=0; k < (numprocess-1);k++) printf("time %f node %d\n",time_warm_all[k],order_node[k]);
	//work[numprocess-2] = 0;
	w_t[numprocess-2] = 1;
	//printf("n_d %d\n",numprocess-2);
	for (k=0; k < (numprocess-2);k++)
	{
		tmp = time_warm_all[numprocess-2]/time_warm_all[k];
		//printf("tmp %f\n",tmp);
		w_t[k] = tmp;
		printf("w %f\n",w_t[k]);
		//work[k] = (int)(5*tmp);
		//ttotal += work[k];
	}

	for (k=0; k < (numprocess-1);k++) printf("w_t %f\n",w_t[k]); 		
	
	int count = 0;

	for (k = 0;k < (numprocess-1);k++) w_t_1[k]=1;
	while ((count < 200)&&(ttotal_1 < 100.0))
	{
		ttotal_1=0;
		w_t_1[count % (numprocess-1)] += w_t[count % (numprocess-1)]; 
		//w_t_1[numprocess-2] *= 1.01;
		//ttotal_1 *= w_t[count % (numprocess -1)];
		for (k=0;k<(numprocess-1);k++) ttotal_1+=w_t_1[k];
		//printf("ttotal_1 %f w_t_1[%d] = %f\n",ttotal_1,count % (numprocess-1),w_t_1[count % (numprocess-1)]);
		count++;
	}
	w_t_1[(count-1) % (numprocess-1)] -= w_t[(count-1) % (numprocess-1)]; 
	for (k=0; k < (numprocess-1);k++) printf("ttotal_1 %f w_1 %f\n",ttotal_1,w_t_1[k]); 		
	ttotal = 0;
	for (k=0; k < (numprocess-1);k++) 
	{
		work[k] = (int)w_t_1[k];
		ttotal += work[k];
	}  
	
	reparto = 100 - ttotal;
	for (k=0; k < reparto;k++) work[k%(numprocess-1)]++;
	for (k=0; k < (numprocess-1);k++) printf("w %f\n",work[k]); 		
	//work[0] = 70;
	//work[1] = 20;
	//work[2] = 10;
	
	ttotal = 0;
	for (k=0; k < (numprocess-1);k++)
	{
		tmp_surface = floor(work[k]*surface/100);
		points_by_process[k] = tmp_surface;
		ttotal += tmp_surface;
	}
	reparto = surface - ttotal;
	for (k=0;k<reparto;k++) points_by_process[k]++;
	points_by_begin[0] = 0;
	for (k=1; k < (numprocess-1);k++)
	{
		points_by_begin[k] = points_by_begin[k-1] + points_by_process[k-1];
	}
	for (int s=0;s<(numprocess-1);s++) printf("Proceso %d work %d Comienza %d Puntos %d\n",order_node[s],work[s],points_by_begin[s],points_by_process[s]);		*/
	/*double sum = 0;
	int *work, tmp, ttotal = 0,k, reparto,tmp_surface;
	//char tmp_nodes[MPI_MAX_PROCESSOR_NAME];
	
	work = (int *)malloc(sizeof(int)*(numprocess-1));
	//printf("Surface	%d\n",surface);

	for (k=0; k < (numprocess-1);k++)
		sum += time_warm_all[k];
	for (k=0; k < (numprocess-1);k++)
	{
		tmp = floor(time_warm_all[k]*100/sum);
		work[k] = tmp;
		ttotal += tmp;
	}	
	thrust::stable_sort_by_key(work,work + (numprocess-1), order_node, thrust::greater<int>());
	reparto = 100 - ttotal;	
	for (k=0;k<reparto;k++) work[k]++;

	for (k=0;k<((numprocess-1)/2);k++) 
	{
		tmp = order_node[k];
		order_node[k] = order_node[(numprocess-1)-k-1];			
		order_node[(numprocess-1)-k-1] = tmp;					
	}				
	printf("\n");
	
	ttotal = 0;
	for (k=0; k < (numprocess-1);k++)
	{
		tmp_surface = floor(work[k]*surface/100);
		points_by_process[k] = tmp_surface;
		ttotal += tmp_surface;
	}
	reparto = surface - ttotal;
	for (k=0;k<reparto;k++) points_by_process[k]++;
	points_by_begin[0] = 0;
	for (k=1; k < (numprocess-1);k++)
	{
		points_by_begin[k] = points_by_begin[k-1] + points_by_process[k-1];
	}
	for (int s=0;s<(numprocess-1);s++) printf("Proceso %d work %d Comienza %d Puntos %d\n",order_node[s],work[s],points_by_begin[s],points_by_process[s]);		*/			
	//exit(0);	
	
}

extern void multigpuSolver_warm_up_vs (unsigned int files, unsigned int nlig, char* ligtype, unsigned int *bonds, type_data *ql, struct ligand_params_t *ligand_params, struct receptor_t proteina, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, struct energy_t energy, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, double *tiempos, unsigned int stride)
{
	//cudaError_t cudaStatus;
	unsigned int i = 0;	
	unsigned int pc_temp = nconformations / files;
	unsigned int n_gpus = devices->n_devices;
	tiempos = (double *)malloc(sizeof(double)*n_gpus);
	printf("\n*** FASE REPARTO DE CARGA ***\n\n");	
	omp_set_num_threads(n_gpus);
	#pragma omp parallel for
	for (unsigned int j=0;j<n_gpus;j++)
	{
	
		double tiempo_i = wtime();
		//int stride_d = th * stride;
		unsigned int stride_d = j * stride;
		gpuSolver_vs (pc_temp,files,nlig,ligtype+stride_d*nlig,bonds+stride_d*nlig*MAXBOND,ql+stride_d*nlig,ligand_params,proteina,param,metaheuristic,devices,conformations_x+stride_d,conformations_y+stride_d,conformations_z+stride_d,move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy.energy+stride_d,energy.n_conformation+stride_d,weights,f_params,nconformations,j,stride_d);		
		
		double tiempo_f = wtime() - tiempo_i;
		tiempos[j] = tiempo_f;		
	}
	for (unsigned int k = 0;k<n_gpus;k++) devices->tiempo[k]=tiempos[k];
	for (unsigned int k = 0;k<n_gpus;k++) printf("\tDevice %d Modelo: %s Tiempo GPU: %f seg\n",devices->id[k],devices->propiedades[k].name,devices->tiempo[k]);
	
}


/*extern double calculo_nodo_multigpu_vs_mpi (struct ligand_t ligando, struct receptor_t proteina, struct vectors_t *vectors_e, struct device_t *devices, struct vdw_param_t *vdw_params, struct param_t param, struct metaheuristic_t metaheuristic, int nconformations,int *number_devices)
{
	int *device_current = new int;	
	cudaDeviceProp deviceProp;
	double *tiempos,total_warm,total_warm_t;
	
	cudaDeviceProp *prop = new cudaDeviceProp();	
	cudaSetDevice(param.ngpu);
	cudaGetDevice(device_current);
	cudaGetDeviceProperties (prop, *device_current);
	printf("\n%s\n",prop->name);
			
	vectors_e->nconformations = nconformations;
	vectors_e->move_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->move_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->move_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->energy.n_conformation = (int *)malloc(sizeof(int)*vectors_e->nconformations);
	vectors_e->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
	
	generate_positions (param,vectors_e,metaheuristic,devices,0,0);
	total_warm = wtime();
	//multigpuSolver (ligando,proteina,vectors_e->num_surface, vectors_e->move_x,vectors_e->move_y,vectors_e->move_z,vectors_e->quat_x, vectors_e->quat_y, vectors_e->quat_z, vectors_e->quat_w, vectors_e->energy,vectors_e->vdw_params, vectors_e->nconformations, param, metaheuristic, devices);
	total_warm_t = wtime() - total_warm;
		
	free(vectors_e->move_x);
	free(vectors_e->move_y);
	free(vectors_e->move_z);
	free(vectors_e->quat_x);
	free(vectors_e->quat_y);
	free(vectors_e->quat_z);
	free(vectors_e->quat_w);
	free(vectors_e->energy.energy);
	free(vectors_e->energy.n_conformation);	
	return(total_warm_t);
	
}*/

extern void warm_up_devices_vs (type_data *c_x, type_data *c_y, type_data *c_z, struct ligand_params_t *ligand_params, type_data *ql, char *ligtype, char *subtype, unsigned int *bonds, char *nbonds, struct receptor_t proteina, struct vectors_t *vectors_e, struct device_t *devices, type_data *weights, struct force_field_param_t *f_params, struct param_t param, struct metaheuristic_t metaheuristic, unsigned int nconformations,unsigned int *number_devices)
{
	int *device_current = new int;
	unsigned int k,*count,*tipos,d,e,t,opc,tmp=0,temp,error,reparto;
	int n_devices;
	cudaDeviceProp deviceProp;
	struct device_t devices_tmp;
	struct vectors_t vectors_o;
	double *tiempos,*trabajo_d,tmp_d;
	char str[2];
	FILE *fp;
		
	
	//cudaDeviceProp *prop = new cudaDeviceProp();	
	//cudaSetDevice(param.ngpu);
	//cudaGetDevice(device_current);
	//cudaGetDeviceProperties (prop, *device_current);
	//printf("\n%s\n",prop->name);
	
	cudaGetDeviceCount(&n_devices);
	//n_devices = n_devices - 4;	
	vectors_e->nconformations = vectors_e->files * 32 * n_devices;
	//vectors_e->nconformations = n_devices * nconformations;
	//if (n_devices == 2) param.tipo_computo = 1;
	vectors_e->conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
	vectors_e->conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
	vectors_e->conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
	vectors_e->ql = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
	vectors_e->ligtype = (char *)malloc(sizeof(char)*vectors_e->nlig*vectors_e->nconformations);
	vectors_e->subtype = (char *)malloc(sizeof(char)*vectors_e->nlig*SUBTYPEMAXLEN*vectors_e->nconformations);
	vectors_e->bonds = (unsigned int *)malloc(sizeof(unsigned int)*vectors_e->nlig*MAXBOND*vectors_e->nconformations);
	vectors_e->nbonds = (char *)malloc(sizeof(char)*vectors_e->nlig*vectors_e->nconformations);
		
	vectors_e->move_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->move_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->move_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_e->nconformations);
	vectors_e->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
	 						 
	count = (unsigned int *)calloc(sizeof(unsigned int),n_devices);
	tipos = (unsigned int *)calloc(sizeof(unsigned int),n_devices);		
	devices_tmp.id = (unsigned int *)malloc(sizeof(unsigned int) * n_devices);
	devices_tmp.propiedades = (cudaDeviceProp *)malloc (sizeof(cudaDeviceProp) * n_devices);
	devices_tmp.trabajo = (unsigned int *)malloc (sizeof(unsigned int) * n_devices);
	devices_tmp.tiempo = (double *)malloc (sizeof(double) * n_devices);
	devices_tmp.hilos = (unsigned int *)malloc(sizeof(unsigned int) * n_devices*metaheuristic.num_kernels);
	
	for (unsigned int d=0;d<n_devices;d++)
	{
		cudaGetDeviceProperties(&deviceProp,d);		
		devices_tmp.id[d] = d;		
		devices_tmp.propiedades[d] = deviceProp;
		devices_tmp.hilos[d*metaheuristic.num_kernels] = metaheuristic.ThreadsperblockFit;
		devices_tmp.hilos[d*metaheuristic.num_kernels+1] = metaheuristic.ThreadsperblockMoveIni;
		devices_tmp.hilos[d*metaheuristic.num_kernels+2] = metaheuristic.ThreadsperblockRotIni;
		devices_tmp.hilos[d*metaheuristic.num_kernels+3] = metaheuristic.ThreadsperblockQuatIni;
		devices_tmp.hilos[d*metaheuristic.num_kernels+4] = metaheuristic.ThreadsperblockRandomIni;
		devices_tmp.hilos[d*metaheuristic.num_kernels+5] = metaheuristic.ThreadsperblockMoveImp;
		devices_tmp.hilos[d*metaheuristic.num_kernels+6] = metaheuristic.ThreadsperblockIncImp;
		printf("Device %d name %s\n",devices_tmp.id[d],devices_tmp.propiedades[d].name);
	}
	//exit(0);
	fill_conformations_vs(param,metaheuristic,c_x,c_y,c_z,ql,ligtype,subtype,nbonds,bonds,vectors_e);
	//for (int i=0;i<vectors_e->nconformations*vectors_e->nlig;i++) printf("id %d c_x %f c_y %f c_z %f \n",i/vectors_e->nlig,vectors_e->conformations_x[i], vectors_e->conformations_y[i], vectors_e->conformations_z[i]);
	generate_positions_vs (param,vectors_e,metaheuristic,&devices_tmp,0,0);			
	//for (int i=0;i<vectors_e->nconformations*vectors_e->nlig;i++) printf("id %d c_x %f c_y %f c_z %f m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f\n",i,vectors_e->conformations_x[i], vectors_e->conformations_y[i], vectors_e->conformations_z[i],vectors_e->move_x[i/vectors_e->nlig],vectors_e->move_y[i/vectors_e->nlig],vectors_e->move_z[i/vectors_e->nlig],vectors_e->quat_x[i/vectors_e->nlig],vectors_e->quat_y[i/vectors_e->nlig],vectors_e->quat_z[i/vectors_e->nlig]);
	//exit(0);		
	//exit(0);
	//TIPO DE COMPUTACION
	switch (param.tipo_computo) {
		case 0: //HOMOGENEA
			for (d=0;d<n_devices;d++)
			{
				for (e=0;e<n_devices;e++)
				{
					if (strcmp(devices_tmp.propiedades[d].name,devices_tmp.propiedades[e].name) == 0)
					count[d]++;
				}
			}
			t = n_devices;
			d = 0;
			while (t > 0)
			{
				tipos[d] = count[d];
				t -= tipos[d];
				d++;
			}
			
			printf("\n** GPUs disponibles **");
			for (t = 0; t < d;t++)
				printf("\n\t%d. %s (DISPOSITIVOS: %d)\n",t,devices_tmp.propiedades[t].name,count[t]);
			printf("\nTipo de dispositivo para computar: ");
			//scanf("%d",&opc);
			opc = param.tipo_dispositivo_computar;
			// el tipo del dispositivo es opc			
			//exit(0);	
			devices->id = (unsigned int *)malloc (sizeof(unsigned int) * count[opc]);
			devices->propiedades = (cudaDeviceProp *)malloc (sizeof(cudaDeviceProp) * count[opc]);
			devices->trabajo = (unsigned int *)malloc (sizeof(unsigned int) * count[opc]);
			devices->tiempo = (double *)malloc (sizeof(double) * count[opc]);
			devices->stride = (unsigned int *)malloc (sizeof(unsigned int) * count[opc]);
			devices->conformaciones = (unsigned int *)malloc (sizeof(unsigned int) * count[opc]);
			devices->hilos = (unsigned int *)malloc(sizeof(unsigned int) * count[opc]*metaheuristic.num_kernels);
			e = 0;
			for (d=0;d<n_devices;d++)
			{
				if (strcmp(devices_tmp.propiedades[d].name,devices_tmp.propiedades[opc].name) == 0)
				{
					devices->id[e] = d;
					devices->propiedades[e] = devices_tmp.propiedades[d];	
					devices->hilos[e*metaheuristic.num_kernels] = metaheuristic.ThreadsperblockFit;
					devices->hilos[e*metaheuristic.num_kernels+1] = metaheuristic.ThreadsperblockMoveIni;
					devices->hilos[e*metaheuristic.num_kernels+2] = metaheuristic.ThreadsperblockRotIni;
					devices->hilos[e*metaheuristic.num_kernels+3] = metaheuristic.ThreadsperblockQuatIni;
					devices->hilos[e*metaheuristic.num_kernels+4] = metaheuristic.ThreadsperblockRandomIni;
					devices->hilos[e*metaheuristic.num_kernels+5] = metaheuristic.ThreadsperblockMoveImp;
					devices->hilos[e*metaheuristic.num_kernels+6] = metaheuristic.ThreadsperblockIncImp;					
					e++;
				}
			}
			*number_devices = tipos[opc];
			devices->n_devices = tipos[opc];			
			printf("\n** SE VA A COMPUTAR CON LOS DISPOSITIVOS **\n");
			error = 0;	
			sprintf(str,"%d",opc);				
			strcat(param.multigpu,"_");
			strcat(param.multigpu,str);
			strcat(param.multigpu,"\0");			
			if ((param.optimizacion) && ((fp = fopen(param.multigpu,"r")) == NULL))
			{
				copy_surface(vectors_e,&vectors_o);
				//warm_up_bloques_gpu (ligando,proteina,&vectors_o,vectors_e->vdw_params,vectors_e->sasa_params,vectors_e->hbond_params,param,metaheuristic,devices,0);
				warm_up_bloques_gpu_vs (vectors_e->conformations_x,vectors_e->conformations_y,vectors_e->conformations_z,ligand_params,vectors_e->ql,vectors_e->ligtype,vectors_e->subtype,vectors_e->bonds,vectors_e->nbonds,proteina,&vectors_o,devices,vectors_e->weights,vectors_e->f_params,param,metaheuristic,vectors_e->nconformations,0)	;
				limpiar_surface(&vectors_o);
				escribir_multigpu (param,&metaheuristic,devices,opc);
				//printf("Threadsperblock1Ini: %d\n",metaheuristic.Threadsperblock1Ini);
				devices->trabajo[0] = floor(100/(*number_devices));
				tmp = devices->trabajo[0];
				printf("Device %d, modelo %s\n",devices->id[0],devices->propiedades[0].name);
				for (d=1;d<(*number_devices);d++)
				{	
					printf("Device %d, modelo %s\n",devices->id[d],devices->propiedades[d].name);
					devices->hilos[d*metaheuristic.num_kernels] = devices->hilos[metaheuristic.num_kernels];
					devices->hilos[d*metaheuristic.num_kernels+1] = devices->hilos[metaheuristic.num_kernels+1];
					devices->hilos[d*metaheuristic.num_kernels+2] = devices->hilos[metaheuristic.num_kernels+2];
					devices->hilos[d*metaheuristic.num_kernels+3] = devices->hilos[metaheuristic.num_kernels+3];
					devices->hilos[d*metaheuristic.num_kernels+4] = devices->hilos[metaheuristic.num_kernels+4];
					devices->hilos[d*metaheuristic.num_kernels+5] = devices->hilos[metaheuristic.num_kernels+5];
					devices->hilos[d*metaheuristic.num_kernels+6] = devices->hilos[metaheuristic.num_kernels+6];
					devices->trabajo[d] = floor(100/(*number_devices));	
					tmp += devices->trabajo[d];	
				}
				
			}
			else
			{
				if (param.optimizacion)
				{
					readmultiGpuOptimizationFile(param.multigpu,0,param.tipo_dispositivo_computar,metaheuristic,devices,&error);
					if (error)
					{
						copy_surface(vectors_e,&vectors_o);
						//warm_up_bloques_gpu (ligando,proteina,&vectors_o,vectors_e->vdw_params,vectors_e->sasa_params,vectors_e->hbond_params,param,metaheuristic,devices,0);
						warm_up_bloques_gpu_vs (vectors_e->conformations_x,vectors_e->conformations_y,vectors_e->conformations_z,ligand_params,vectors_e->ql,vectors_e->ligtype,vectors_e->subtype,vectors_e->bonds,vectors_e->nbonds,proteina,&vectors_o,devices,vectors_e->weights,vectors_e->f_params,param,metaheuristic,vectors_e->nconformations,0)	;
						limpiar_surface(&vectors_o);
						//escribir_multigpu (param,&metaheuristic,devices,opc);
						for (d=1;d<(*number_devices);d++)
						{								
							devices->hilos[d*metaheuristic.num_kernels] = devices->hilos[metaheuristic.num_kernels];
							devices->hilos[d*metaheuristic.num_kernels+1] = devices->hilos[metaheuristic.num_kernels+1];
							devices->hilos[d*metaheuristic.num_kernels+2] = devices->hilos[metaheuristic.num_kernels+2];
							devices->hilos[d*metaheuristic.num_kernels+3] = devices->hilos[metaheuristic.num_kernels+3];
							devices->hilos[d*metaheuristic.num_kernels+4] = devices->hilos[metaheuristic.num_kernels+4];
							devices->hilos[d*metaheuristic.num_kernels+5] = devices->hilos[metaheuristic.num_kernels+5];
							devices->hilos[d*metaheuristic.num_kernels+6] = devices->hilos[metaheuristic.num_kernels+6];							
						}						
					}
					devices->trabajo[0] = floor(100/(*number_devices));
					tmp = devices->trabajo[0];
					printf("Device %d, modelo %s\n",devices->id[0],devices->propiedades[0].name);
					for (d=1;d<(*number_devices);d++)
					{	
						printf("Device %d, modelo %s\n",devices->id[d],devices->propiedades[d].name);
						devices->trabajo[d] = floor(100/(*number_devices));	
						tmp += devices->trabajo[d];	
					}
					devices->trabajo[0] += (100 - tmp);		
				}
				else
				{
					devices->trabajo[0] = floor(100/(*number_devices));
					tmp = devices->trabajo[0];
					printf("Device %d, modelo %s\n",devices->id[0],devices->propiedades[0].name);
					for (d=1;d<(*number_devices);d++)
					{	
						printf("Device %d, modelo %s\n",devices->id[d],devices->propiedades[d].name);
						devices->trabajo[d] = floor(100/(*number_devices));	
						tmp += devices->trabajo[d];	
					}
					devices->trabajo[0] += (100 - tmp);		
				}
			}
			break;	
		case 1: //HETEROGENEA
			trabajo_d = (double *)malloc (sizeof(double) * n_devices);
			devices->id = (unsigned int *)malloc (sizeof(unsigned int) * n_devices);
			devices->propiedades = (cudaDeviceProp *)malloc (sizeof(cudaDeviceProp) * n_devices);
			devices->trabajo = (unsigned int *)malloc (sizeof(unsigned int) * n_devices);
			devices->conformaciones = (unsigned int *)malloc(sizeof(unsigned int) * n_devices);
			devices->tiempo = (double *)malloc (sizeof(double) * n_devices);
			devices->hilos = (unsigned int *)malloc(sizeof(unsigned int) * n_devices * metaheuristic.num_kernels);
			devices->stride = (unsigned int *)malloc (sizeof(unsigned int) * n_devices);
			tiempos = (double *)malloc(sizeof(double) * n_devices);			
			devices->n_devices = n_devices;
			for (unsigned int d=0;d<n_devices;d++)
			{
				devices->id[d] = devices_tmp.id[d];		
				devices->propiedades[d] = devices_tmp.propiedades[d];
				devices->hilos[d*metaheuristic.num_kernels] = metaheuristic.ThreadsperblockFit;
				devices->hilos[d*metaheuristic.num_kernels+1] = metaheuristic.ThreadsperblockMoveIni;
				devices->hilos[d*metaheuristic.num_kernels+2] = metaheuristic.ThreadsperblockRotIni;
				devices->hilos[d*metaheuristic.num_kernels+3] = metaheuristic.ThreadsperblockQuatIni;
				devices->hilos[d*metaheuristic.num_kernels+4] = metaheuristic.ThreadsperblockRandomIni;
				devices->hilos[d*metaheuristic.num_kernels+5] = metaheuristic.ThreadsperblockMoveImp;
				devices->hilos[d*metaheuristic.num_kernels+6] = metaheuristic.ThreadsperblockIncImp;
				//printf("Device %d es %s tiene computabilidad %d y %d Mb de memoria device\n",devices->id[d],devices->propiedades[d].name,devices->propiedades[d].major,devices->propiedades[d].totalGlobalMem/1024);
			}
			//exit(0);
			if (param.reparto_carga)				
			{
				type_data *w_t = (type_data *)malloc(sizeof(type_data)*devices->n_devices);
				type_data *w_t_1 = (type_data *)calloc(sizeof(type_data),devices->n_devices);
				type_data ttotal_1,tmp;

				multigpuSolver_warm_up_vs (vectors_e->files,vectors_e->nlig,vectors_e->ligtype,vectors_e->bonds,vectors_e->ql,ligand_params,proteina,vectors_e->conformations_x,vectors_e->conformations_y,vectors_e->conformations_z,vectors_e->move_x,vectors_e->move_y,vectors_e->move_z,vectors_e->quat_x,vectors_e->quat_y,vectors_e->quat_z,vectors_e->quat_w,vectors_e->energy,weights,f_params,vectors_e->nconformations,param,metaheuristic,devices,tiempos,0);
				
				//exit(0);	
				*number_devices = n_devices;
				devices->n_devices = n_devices;
				double ttotal = 0;
				unsigned int ttrabajo = 0;
				
				thrust::stable_sort_by_key(devices->tiempo,devices->tiempo + n_devices, devices->id, thrust::less<type_data>());
				w_t[n_devices-1] = 1;
				//printf("n_d %d\n",numprocess-2);
				for (k=0; k < (n_devices-1);k++)
				{
					tmp = devices->tiempo[n_devices-1]/devices->tiempo[k];
					//printf("tmp %f\n",tmp);
					w_t[k] = tmp;
					//printf("w %f\n",w_t[k]);
					//work[k] = (int)(5*tmp);
					//ttotal += work[k];
				}
				//for (k=0; k < n_devices;k++) printf("w_t %f\n",w_t[k]); 		
	
				unsigned int count = 0;
				ttotal_1=0;
				for (k = 0;k < n_devices-1;k++) w_t_1[k]=1;
				while ((count < 200)&&(ttotal_1 < 100.0))
				{
					ttotal_1=0;
					w_t_1[count % n_devices] += w_t[count % n_devices]; 
					//w_t_1[numprocess-2] *= 1.01;
					//ttotal_1 *= w_t[count % (numprocess -1)];
					for (k=0;k<n_devices;k++) ttotal_1+=w_t_1[k];
					//printf("ttotal_1 %f w_t_1[%d] = %f\n",ttotal_1,count % (numprocess-1),w_t_1[count % (numprocess-1)]);
					count++;
				}
				w_t_1[(count-1) % n_devices] -= w_t[(count-1) % n_devices]; 
				//for (k=0; k < n_devices-1;k++) printf("ttotal_1 %f w_1 %f\n",ttotal_1,w_t_1[k]); 		
				ttotal = 0;
				for (k=0; k < n_devices;k++) 
				{
					devices->trabajo[k] = (int)w_t_1[k];
					ttotal += devices->trabajo[k];
				}  	
				reparto = 100 - ttotal;
				for (k=0; k < reparto;k++) devices->trabajo[k%(n_devices-1)]++;
				printf("\n");
				for (unsigned int s=0;s<n_devices;s++) printf("Device %d modelo: %s Carga de trabajo %d \n",devices->id[s],devices->propiedades[s].name,devices->trabajo[s]);					
				free(w_t);
				free(w_t_1);
			}
			else
			{
				printf("\n*** FASE REPARTO DE CARGA ***\n\n");	
				*number_devices = n_devices;
				devices->n_devices = n_devices;				
				reparto = (unsigned int)(100 / n_devices);				
				for (d=0;d<n_devices;d++) 
					devices->trabajo[d] = reparto;
				reparto = 100 % n_devices;
				for (d=0;d<reparto;d++)	devices->trabajo[d]++;
				for (d=0;d<n_devices;d++) printf("Device %d modelo: %s Carga de trabajo %d\n",devices->id[d],devices->propiedades[d].name,devices->trabajo[d]);
				//printf("Reparto equitativo\n");
			}
			if (param.optimizacion)
			{
				strcat(param.multigpu,"_h");				
				strcat(param.multigpu,"\0");					
				if ((fp = fopen(param.multigpu,"r")) == NULL)
				{
					//copy_surface(vectors_e,&vectors_o);
					//for (d=0;d<n_devices;d++)
					//	warm_up_bloques_gpu (ligando,proteina,&vectors_o,vectors_e->vdw_params,vectors_e->sasa_params,vectors_e->hbond_params,param,metaheuristic,devices,d);			
					//limpiar_surface(&vectors_o);
					//escribir_multigpu (param,&metaheuristic,devices,1);
				}
				else
				{
					readmultiGpuOptimizationFile(param.multigpu,1,0,metaheuristic,devices,&error);
					if (error)
					{
						//copy_surface(vectors_e,&vectors_o);
						//for (d=0;d<n_devices;d++)
						//	warm_up_bloques_gpu (ligando,proteina,&vectors_o,vectors_e->vdw_params,vectors_e->sasa_params,vectors_e->hbond_params,param,metaheuristic,devices,d);			
						//limpiar_surface(&vectors_o);
						//escribir_multigpu (param,&metaheuristic,devices,1);
					}
				}	
			}
			break;
	}
	free(vectors_e->move_x);
	free(vectors_e->move_y);
	free(vectors_e->move_z);
	free(vectors_e->quat_x);
	free(vectors_e->quat_y);
	free(vectors_e->quat_z);
	free(vectors_e->quat_w);
	free(vectors_e->energy.energy);
	free(vectors_e->energy.n_conformation);	
	free(vectors_e->conformations_x);
	free(vectors_e->conformations_y);
	free(vectors_e->conformations_z);
	free(vectors_e->ql);
	free(vectors_e->ligtype);
	free(vectors_e->subtype);
	free(vectors_e->bonds);
	free(vectors_e->nbonds);
	free(count);
	free(tipos);
	
}

extern void warm_up_bloques_gpu_vs (type_data *c_x, type_data *c_y, type_data *c_z, struct ligand_params_t *ligand_params, type_data *ql, char *ligtype, char *subtype, unsigned int *bonds, char *nbonds, struct receptor_t proteina, struct vectors_t *vectors_e, struct device_t *devices, type_data *weights, struct force_field_param_t *f_params, struct param_t param, struct metaheuristic_t metaheuristic, unsigned int nconformations,unsigned int orden_device)
{


        int *device_current = new int;
        cudaDeviceProp *prop = new cudaDeviceProp();
        double tiempo_i,tiempo_f, twarm_up, twarm_up_t;
        double tiempo = 1000.0;
        int valor_act;
        
        vectors_e->nconformations = 32 * param.conf_warm_up_gpu;
      
        vectors_e->conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
        vectors_e->conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
        vectors_e->conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
        vectors_e->ql = (type_data *)malloc(sizeof(type_data)*vectors_e->nlig*vectors_e->nconformations);
        vectors_e->ligtype = (char *)malloc(sizeof(char)*vectors_e->nlig*vectors_e->nconformations);
        vectors_e->subtype = (char *)malloc(sizeof(char)*vectors_e->nlig*SUBTYPEMAXLEN*vectors_e->nconformations);
        vectors_e->bonds = (unsigned int *)malloc(sizeof(unsigned int)*vectors_e->nlig*MAXBOND*vectors_e->nconformations);
        vectors_e->nbonds = (char *)malloc(sizeof(char)*vectors_e->nlig*vectors_e->nconformations);

        vectors_e->move_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
        vectors_e->move_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
        vectors_e->move_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
        vectors_e->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
        vectors_e->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
        vectors_e->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
        vectors_e->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
        vectors_e->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_e->nconformations);
        vectors_e->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
        thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);

	cudaSetDevice(devices->id[orden_device]);
        cudaGetDevice(device_current);
        cudaGetDeviceProperties (prop, *device_current);
        printf("\n%s\n",prop->name);

	//FITNESS
        twarm_up = wtime();
        for (int i=64;i<=param.limit;i+=64)
        {
                tiempo_i = wtime();
                cambia_param_blocks(devices,orden_device,metaheuristic.num_kernels,0,i);
                
		gpuSolver_vs (param.conf_warm_up_gpu,vectors_e->files,vectors_e->nlig,vectors_e->ligtype,vectors_e->bonds,vectors_e->ql,ligand_params,proteina,param,metaheuristic,devices,vectors_e->conformations_x, vectors_e->conformations_y, vectors_e->conformations_z,vectors_e->move_x,vectors_e->move_y,vectors_e->move_z,vectors_e->quat_x,vectors_e->quat_y,vectors_e->quat_z,vectors_e->quat_w,vectors_e->energy.energy,vectors_e->energy.n_conformation,weights,f_params,vectors_e->nconformations,orden_device,0);
                //exit(0);
                tiempo_f = wtime() - tiempo_i;
                if (tiempo_f < tiempo)
                {
                        valor_act = i;
                        tiempo = tiempo_f;
                }
                printf("Tiempo %f para tamaño de bloque CON %d\n",tiempo_f,i);
                //exit(0);
        }
        cambia_param_blocks(devices,orden_device,metaheuristic.num_kernels,0,valor_act);
	printf("Valor Optimizado HILOS/BLOQUE en Cálculo de FITNESS VS: %d\n",valor_act);

	//exit(0);
        for (int i=64;i<=param.limit;i+=64)
        {
                //tiempo_i = wtime();
                cambia_param_blocks(devices,orden_device,metaheuristic.num_kernels,1,i);
                //generate_positions_move_warm_up(param,vectors_e,metaheuristic,devices,orden_device,&tiempo_f);
                //tiempo_f = wtime() - tiempo_i;
                if (tiempo_f < tiempo)
                {
                        valor_act = i;
                        tiempo = tiempo_f;
                }
                printf("Tiempo %f para tamaño de bloque CON %d\n",tiempo_f,i);
        }
        cambia_param_blocks(devices,orden_device,metaheuristic.num_kernels,1,valor_act);
	cambia_param_blocks(devices,orden_device,metaheuristic.num_kernels,2,valor_act);
	 cambia_param_blocks(devices,orden_device,metaheuristic.num_kernels,5,valor_act);
        printf("Valor Optimizado HILOS/BLOQUE en MOVE/ROTATION en Inicializar: %d\n",valor_act);	
        //exit(0)
        for (int i=64;i<=param.limit;i+=64)
        {
                //tiempo_i = wtime();
                cambia_param_blocks(devices,orden_device,metaheuristic.num_kernels,3,i);
                //generate_positions_quat_warm_up(param,vectors_e,metaheuristic,devices,orden_device,&tiempo_f);
                //tiempo_f = wtime() - tiempo_i;
                if (tiempo_f < tiempo)
                {
                        valor_act = i;
                        tiempo = tiempo_f;
                }
                printf("Tiempo %f para tamaño de bloque CON %d\n",tiempo_f,i);
        }
        cambia_param_blocks(devices,orden_device,metaheuristic.num_kernels,3,valor_act);
	cambia_param_blocks(devices,orden_device,metaheuristic.num_kernels,4,valor_act);

        printf("Valor Optimizado HILOS/BLOQUE en QUAT/CURAND en Inicializar: %d\n",valor_act);
        //exit(0);
        for (int i=64;i<=param.limit;i+=64)
        {
                //tiempo_i = wtime();
                cambia_param_blocks(devices,orden_device,metaheuristic.num_kernels,6,i);
                //generate_positions_incl_mejora_warm_up (proteina,ligando,param,vectors_e,metaheuristic,devices,orden_device,&tiempo_f);
                //tiempo_f = wtime() - tiempo_i;
                if (tiempo_f < tiempo)
                {
                        valor_act = i;
                        tiempo = tiempo_f;
                }
                printf("Tiempo %f para tamaño de bloque CON %d\n",tiempo_f,i);
        }
        cambia_param_blocks(devices,orden_device,metaheuristic.num_kernels,6,valor_act);
        printf("Valor Optimizado HILOS/BLOQUE en INCLUIR en Mejorar: %d\n",valor_act);
        //exit(0);

        twarm_up_t = wtime() - twarm_up;
        printf("\n\t*****************\n");
        printf("\tWARM_UP: %f seg\n",twarm_up_t);
        printf("\t*****************\n");
        final_parametros_paralelos_gpu (devices,metaheuristic.num_kernels);

        //LIBERA MEMORIA
	free(vectors_e->move_x);
        free(vectors_e->move_y);
        free(vectors_e->move_z);
        free(vectors_e->quat_x);
        free(vectors_e->quat_y);
        free(vectors_e->quat_z);
        free(vectors_e->quat_w);
        free(vectors_e->energy.energy);
        free(vectors_e->energy.n_conformation);
        free(vectors_e->conformations_x);
        free(vectors_e->conformations_y);
        free(vectors_e->conformations_z);
        free(vectors_e->ql);
        free(vectors_e->ligtype);
        free(vectors_e->subtype);
        free(vectors_e->bonds);
        free(vectors_e->nbonds);
}

