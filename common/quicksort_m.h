#ifndef QUICKSORT_H
#define QUICKSORT_H

extern void quicksort(float *array, unsigned int *keys, unsigned int start, unsigned int end);

#endif
