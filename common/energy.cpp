#include <thrust/sequence.h>
#include "energy_common.h"
#include "definitions.h"
#include "energy_GpuSolver.h"
#include "energy_CpuSolver.h"
#include "energy_struct.h"
#include "energy_surface.h"
#include "energy_positions.h"
#include "energy_positions_flex.h"
#include "energy_positions_common.h"
#include "energy_flexibility.h"
#include "energy_montecarlo.h"
#include "montecarlo.h"
#include "energy_mutation.h"
#include "energy_mutation_cpp.h"
#include "energy_mutation_flex.h"
#include "energy_mutation_cpp_flex.h"
#include "metaheuristic_ini.h"
#include "metaheuristic_ini_flex.h"
#include "metaheuristic_sel_flex.h"
#include "metaheuristic_comb_flex.h"
#include "metaheuristic_mgpu_common.h"
#include "metaheuristic_mgpu_flex.h"
#include "metaheuristic_inc.h"
#include "metaheuristic_inc_flex.h"
#include "metaheuristic_inc_common.h"
#include "metaheuristic_sel.h"
#include "metaheuristic_comb.h"
#include "metaheuristic_mgpu.h"
#include "metaheuristic_cpu.h"
#include "ligand.h"
#include "wtime.h"
#include "energy_vs.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <omp.h>
#include <signal.h>

type_data max_alarm = 10000;

void maximo (struct vectors_t *v, type_data *max)
{	
	unsigned int stride = v->nconformations / v->num_surface;
	//printf("MAX_M %f\n",*max);
	for (unsigned int j = 0; j < v->num_surface; j++)			
		if (v->energy.energy[j*stride] < *max)	
			*max = v->energy.energy[j*stride];													
}

void salir_alarma (int)
{
	printf("\n\t***** ¡¡ FIN POR TIEMPO !! *****\n");
	exit(0);	
}

int main (int argc, char **argv){

	char c,configfile[FILENAME],configdqn[FILENAME],cadena[FILENAME],cadena_ligand[FILENAME],cadena1[FILENAME];
	char str[2];
	struct ligand_t ligando, ligtemp;
	struct receptor_t protein;
	struct receptor_t proteina;
	struct device_t devices;
	type_data **dqn_data;
	bool fin = false;	
	FILE *fp;
	//struct vdw_param_t vdw_params[MAXTYPES];
	Protein_entry * Protein;
	Bond_tp * Bond;	
	struct param_t param;
	struct flexibility_data_t flexibility_conformations;
	struct flexibility_params_t flexibility_params;
	struct vectors_t vectors, vectors_o, vectors_ini, vectors_selec, vectors_comb;
	struct metaheuristic_t metaheuristic;
	double twarm_up,tinicializar, tseleccion, tcombinar, tmejora, tincluir, tmut, ttotal;
	double twarm_up_t,tinicializar_t, tseleccion_t, tcombinar_t, tmejora_t, tincluir_t, tmut_t, ttotal_t;
	type_data tmp;		

	
	unsigned int device, err, nBond, nAtom, mode, improve = 0, count = 0, n_devices, number_devices, alarma = 60, error, conformacion;
	unsigned int qrSize, recSize,stride_e,stride_s;
	type_data max = 10000;
	cudaDeviceProp deviceProp;	
		
	system("date");		
	param.work_type = 0;
	while ((c = getopt (argc, argv, "a:q:v:i:p:o:l:D:f:c:r:d:e:x:y:z:h")) != -1) {
	  switch (c) {
		    	case 'v':
	     			printf("Virtual Screening MODE\n\n");
				param.work_type = 1;					
				break;		
			case 'o':
				strcpy(param.output_dir,optarg);
				break;
			case 'a':
				strcpy(param.input_ligs,optarg);
				divide_ligands(param.input_ligs,argv[3]);
				exit(0);
				break;
			case 'i':
				strcpy(param.input_ligs,optarg);
				break;
			case 'p':
                                strcpy(param.protein,optarg);
				break;
			case 'l':
				strcpy(param.ligand,optarg);
				break;
			case 'D':
				strcpy(param.forcefield,optarg);
				break;
			case 'q':
				strcpy(param.pdbqt,optarg);
				break;
			case 'f':
				param.auto_flex = atoi(optarg);	
				break;
			case 'c':
				strcpy(configfile,optarg);
				break;	
			case 'd':
				strcpy(configdqn,optarg);
				break;	
			case 'x':
				tmp = atof(optarg);
				vectors.ac_x = tmp / 10;	
				break;
			case 'y':
				tmp = atof(optarg);
				vectors.ac_y = tmp / 10;
				break;
			case 'z':
				tmp = atof(optarg);
				vectors.ac_z = tmp / 10;
				break;	
			case 'h':
			case '?':
	      		printf("Usage:\tenergy -c CONFIG-FILE.met [-h | -? HELP] \n");
				printf("\t<Params>\n");
		    	printf("\t\t-v\t\tOutput version information and exit\n");		    	
	    		return 0;
    	  }
 	}
	//printf("x %f y %f z %f\n",vectors.ac_x,vectors.ac_y,vectors.ac_z);	
	if (param.work_type != 0)
	{
		vs_type(&vectors,param,metaheuristic,configfile);
		exit(0);
	}

  	readConfigFile(configfile,param,metaheuristic);
	if ( param.scoring_function_type == S_AD4)
	{
		vectors.ac_x *= 10;
		vectors.ac_y *= 10;
		vectors.ac_z *= 10;
	}
	comprobation_params (param,metaheuristic);
	
	ruta (param.ligand,param.input_dir_l);
	ruta (param.protein,param.input_dir_p);
	
	//sprintf(cadena,"%s/%s",param.input_params,param.forcefield);
	//strcpy(param.forcefield,cadena);
	//printf("fichero %s\n",cadena);
	//exit(0);	
	adjust_param_GPU(&param.mode);
	//exit(0);	
	ligand_babel (param,param.ligand,&flexibility_params,param.level_bonds_VDW,param.level_bonds_ES);
	//exit(0);
	//IMPRIMIR ENLACES ROTABLES
	printf("\nRotable bonds: %d ",flexibility_params.n_links);
	if (flexibility_params.n_links == 0)
	{
		printf("LIGANDO RÍGIDO\n");
		param.auto_flex = 0;	
	}
	else
	{
		printf("LIGANDO FLEXIBLE\n");
		for (unsigned int i=0;i<(flexibility_params.n_links*2);i=i+2) printf("\t%d  %d\n",flexibility_params.links[i],flexibility_params.links[i+1]);	
		if (param.auto_flex== 1)
			param.auto_flex = 1;
	}

	//printf("%s\n",param.ligand);	
	//exit(0);
	readLigand (param.ligand,&ligando,param.scoring_function_type); 			
	//printf("x %f y %f z %f\n",ligando.lig_x[0],ligando.lig_y[0],ligando.lig_z[0]);	
	//exit(0);
	initProtein(param.protein, Protein, Bond, nBond, proteina.nrec,param.scoring_function_type);
	//printf("Px %f y %f z %f\n",ligando.lig_x[0],ligando.lig_y[0],ligando.lig_z[0]);
        //exit(0);	
	readProtein (Protein,proteina,qrSize,recSize);
	//printf("gg %s\n",param.ligand);
	//exit(0);
	count_surface (param.protein,proteina.atoms,&vectors);	
	//exit(0);
	energy_surface (param.protein,&vectors,param.scoring_function_type);
	//for (int i=0;i<vectors.num_surface;i++) printf("sx %f sy %f sz %f\n",vectors.surface_x[i],vectors.surface_y[i],vectors.surface_z[i]);
	
	//LEER FICHERO DQN Y GUARDAR EN MATRIZ.
	dqn_data = readDQNfile (configdqn,&vectors,metaheuristic);
	//exit(0);
	/*for (int i=0;i<64;i++)
		printf("%f %f %f %f %f %f %f %f %f %f\n",dqn_data[i][0],dqn_data[i][1],dqn_data[i][2],dqn_data[i][3],dqn_data[i][4],dqn_data[i][5],dqn_data[i][6],dqn_data[i][7],dqn_data[i][8],dqn_data[i][9]);
	printf("%f %f %f \n",vectors.surface_x[0]*10,vectors.surface_y[0]*10,vectors.surface_z[0]*10);*/
	//exit(0);
	//for (int i=0;i<10;i++)
	//	printf("surface_x %f, surface_y %f surface_z %f\n",vectors.surface_x[i],vectors.surface_y[i],vectors.surface_z[i]);
	//exit(0);
	//read_params(param.vdw_p,param.sasa_p,param.hbond_p,vectors.vdw_params,vectors.sasa_params,vectors.hbond_params);
	//printf("Hola\n");
	force_field_read (param.forcefield,vectors.weights,vectors.f_params,param.scoring_function_type);
	//printf("%f\n",vectors.weights[0]);	
	//metaheuristic.NMIFin = 1;
	//exit(0);
	//CONFIGURACION DE LA ALARMA
	//for (int i=0;i<proteina.nrec;i++)printf("atom %d x %f y %f z %f carga %f tipo %d \n",i,proteina.rec_x[i],proteina.rec_y[i],proteina.rec_z[i],proteina.qr[i],proteina.rectype[i]);
	//exit(0);
	if (param.alarma)	
	{
		alarma = param.duracion_alarma * 60;
		alarm(alarma);
		signal(SIGALRM,salir_alarma);
	}
	switch (param.mode) {
		case 0:
			printf("\nGPU SOLVER MODE...\n");
			printf("******************\n");					
			genera_gpu(&devices,param.ngpu,metaheuristic);
			/*error = 0;
			//printf("devices->id[0] %d\n",devices.id[0]);
			sprintf(str,"%d",param.ngpu);				
			strcat(param.gpu,"_");
			strcat(param.gpu,str);
			strcat(param.gpu,"\0");
			if ((param.optimizacion) && ((fp = fopen(param.gpu,"r")) == NULL))
			{
				twarm_up = wtime();
				warm_up_bloques_gpu (ligando,proteina,&vectors,vectors.weights,vectors.f_params,param,metaheuristic,&devices,0);			
				twarm_up_t = wtime() - twarm_up;	
				escribir_gpu (param,&metaheuristic,&devices);				
				//printf("Threadsperblock1Ini: %d\n",metaheuristic.Threadsperblock1Ini);
			}
			else
			{
				if (param.optimizacion)
				{
					readGpuOptimizationFile(param.gpu,param.ngpu,metaheuristic,&devices,&error);
					comprobation_params (param,metaheuristic);
					if (error)
					{
						twarm_up = wtime();
						warm_up_bloques_gpu (ligando,proteina,&vectors,vectors.weights,vectors.f_params,param,metaheuristic,&devices,0);
						twarm_up_t = wtime() - twarm_up;
						escribir_gpu (param,&metaheuristic,&devices);
					}
				}
			}*/
			//exit(0);
			break;	
		case 1:
			printf("\nSEQUENTIAL MODE...\n");
			printf("******************\n");
			break;			
		case 2:
			printf("\nOPEN-MP MODE...\n");
			printf("***************\n");
			error = 0;
			if ((param.optimizacion) && ((fp = fopen(param.openmp,"r")) == NULL))
			{
				twarm_up = wtime();
				warm_up_openmp (ligando,proteina,&vectors,&vectors_o,vectors.num_surface,vectors.f_params,vectors.weights,param.conf_warm_up_cpu,param,&metaheuristic);
				twarm_up_t = wtime() - twarm_up;
			}
			else
			{
				if (param.optimizacion)
				{
					readOpenmpOptimizationFile(param.openmp,metaheuristic,&error);
					comprobation_params (param,metaheuristic);
					if (error)
					{
						twarm_up = wtime();
						warm_up_openmp (ligando,proteina,&vectors,&vectors_o,vectors.num_surface,vectors.f_params,vectors.weights,param.conf_warm_up_cpu,param,&metaheuristic);
						twarm_up_t = wtime() - twarm_up;
					}
				}
			}
			//exit(0);
			break;
		case 3:
			printf("\nMULTI-GPU MODE...\n");
			printf("*****************\n");
			twarm_up = wtime();
			warm_up_devices (ligando,proteina,&vectors,&devices,vectors.weights,vectors.f_params,param,metaheuristic,param.conf_warm_up_gpu,&number_devices);	
			twarm_up_t = wtime() - twarm_up;
			break;	
	}
	
	
	switch (param.auto_flex) {
		case 0:
			switch (param.mode) {
				case 0:
				case 1:
				case 2:
				case 3:							
					ttotal = wtime();	
					tinicializar = wtime();
					inicializar(proteina,ligando,&vectors,&vectors_ini,param,metaheuristic,&devices,dqn_data);printf("Inicializado...\n");
					tinicializar_t = wtime() - tinicializar;
					//EXTRAER CONFORMACION
					/*max = 10000;
					int n_c,shift;
					shift = vectors_ini.nconformations / vectors_ini.num_surface;
					for (int i=0;i<vectors_ini.num_surface;i++)
					if (vectors_ini.energy.energy[i*shift] < max)
					{
						max = vectors_ini.energy.energy[i*shift];
						n_c = i*shift;
					}
					printf("MAX %f , conf %d\n",max,n_c);
					configurar_ligandos_finales(proteina,ligando,&ligtemp,&vectors_ini,param,metaheuristic,n_c);
					//maximo(&vectors_ini,&max);	
					exit(0);*/						
					while ((count < metaheuristic.NMIFin) && (improve < metaheuristic.NIRFin))
					{		
						printf("%d pass RIGID in progress....\n",count+1);			
						tseleccion = wtime();
						seleccionar(&vectors_ini,&vectors_selec,metaheuristic,param);printf("Seleccionado..\n");
						tseleccion_t += wtime() - tseleccion;
						
						tcombinar = wtime ();
						switch (param.mode) {
							case 0:
								combinar_gpu (proteina,ligando,&vectors_selec,&vectors_comb,metaheuristic,param,&devices,0);printf("Combinado..\n");
								//exit(0);
								break;
							case 1:
							case 2:
								combinar(proteina,ligando,&vectors_selec,&vectors_comb,metaheuristic,param);printf("Combinado..\n");
								break;
							case 3:
								combinar_multigpu (proteina,ligando,&vectors_selec,&vectors_comb,metaheuristic,param,&devices);printf("Combinado..\n");	//exit(0);							
								break;
						}
						tcombinar_t += wtime() - tcombinar;				
						tmut = wtime();
                        			if (metaheuristic.PEMUCom > 0)
			                        {
							seleccionar_mutar (&vectors_comb,&vectors_selec,param,metaheuristic);
                            				switch (param.mode) {
								case 0:
					        	               	mutation_gpu (proteina,ligando,param,vectors_selec.weights,vectors_selec.f_params,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z, vectors_selec.quat_x, vectors_selec.quat_y, vectors_selec.quat_z, vectors_selec.quat_w, vectors_selec.energy.energy,vectors_selec.energy.n_conformation,vectors_selec.nconformations,metaheuristic,&devices,param.ngpu,0);printf("Mutado...\n");
                                    					break;
                                				case 1:
                                				case 2:
					                              mutation_cpp (proteina,ligando,param,metaheuristic,vectors_selec.weights,vectors_selec.f_params,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z, vectors_selec.quat_x, vectors_selec.quat_y, vectors_selec.quat_z, vectors_selec.quat_w, vectors_selec.energy.energy,vectors_selec.energy.n_conformation,vectors_selec.num_surface,vectors_selec.nconformations);printf("Mutado...\n");
                                				       break;
                                				case 3:
                                    					mutation_multigpu (ligando,proteina,vectors_selec.num_surface,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z, vectors_selec.quat_x, vectors_selec.quat_y, vectors_selec.quat_z, vectors_selec.quat_w, vectors_selec.energy.energy,vectors_selec.energy.n_conformation,vectors_selec.weights,vectors_selec.f_params,vectors_selec.nconformations,param,metaheuristic,&devices,2);printf("Mutado...\n");
									break;
                            				}
                            				//incluir_gpu_mejorar (&vectors_selec,&vectors_comb,param,metaheuristic);
							stride_e = vectors_selec.nconformations / vectors_selec.num_surface;
                                			stride_s = vectors_comb.nconformations / vectors_comb.num_surface;
							#pragma omp parallel for
							for (unsigned int i = 0; i < vectors_comb.num_surface; i++)
								incluir_gpu_by_step (ligando.nlig,vectors_selec.move_x + (stride_e*i), vectors_selec.move_y + (stride_e*i), vectors_selec.move_z + (stride_e*i), vectors_selec.quat_x + (stride_e*i), vectors_selec.quat_y + (stride_e*i), vectors_selec.quat_z + (stride_e*i), vectors_selec.quat_w + (stride_e*i), vectors_selec.energy.energy + (stride_e*i), vectors_selec.energy.n_conformation + (stride_e*i), vectors_comb.move_x + (stride_s*i),  vectors_comb.move_y + (stride_s*i),  vectors_comb.move_z + (stride_s*i),  vectors_comb.quat_x + (stride_s*i),  vectors_comb.quat_y + (stride_s*i),  vectors_comb.quat_z + (stride_s*i),  vectors_comb.quat_w + (stride_s*i),  vectors_comb.energy.energy + (stride_s*i),  vectors_comb.energy.n_conformation + (stride_s*i), stride_e, stride_s, i,param,metaheuristic);
							thrust::sequence(vectors_comb.energy.n_conformation,vectors_comb.energy.n_conformation + vectors_comb.nconformations);
                        			}
                        			tmut_t += wtime() - tmut;
						//exit(0);	
						if (metaheuristic.PEMSel > 0)
						{
							seleccionar_mejorar (&vectors_comb,&vectors_selec,param,metaheuristic);
							tmejora = wtime();
							if (metaheuristic.NEEImp > 0)
							{
								 //printf("Conf para mejorar %d\n",vectors_selec.nconformations);
                                                                switch (param.mode) {
                                                                case 0:
									if (param.montecarlo)
                                                                        {
                                                                                montecarlo_environment (proteina,ligando,param,&vectors_selec,metaheuristic,&devices,0,1);printf("Mejorado...\n");
                                                                        }
                                                                        else
                                                                        {
                                                                                gpu_mejorar_environment (proteina,ligando,param,&vectors_selec,metaheuristic,&devices,0,1);printf("Mejorado...\n");
                                                                        }
                                                                        break;
								case 1:
								case 2:
									if (param.montecarlo)
                                                                        {
                                                                                montecarlo_cpp_environment(proteina,ligando,param,vectors_selec.num_surface,vectors_selec.weights,vectors_selec.f_params,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z,vectors_selec.quat_x,vectors_selec.quat_y,vectors_selec.quat_z,vectors_selec.quat_w,vectors_selec.energy.energy,vectors_selec.energy.n_conformation,vectors_selec.nconformations,metaheuristic,1);printf("Mejorado...\n");
                                                                        }
                                                                        else
                                                                        {
                                                                                mejorar_environment(proteina,ligando,param,vectors_selec.num_surface, vectors_selec.weights, vectors_selec.f_params,vectors_selec.move_x, vectors_selec.move_y, vectors_selec.move_z, vectors_selec.quat_x, vectors_selec.quat_y, vectors_selec.quat_z, vectors_selec.quat_w, vectors_selec.energy.energy, vectors_selec.energy.n_conformation, vectors_selec.nconformations,metaheuristic,1);printf("Mejorado..\n");
                                                                        }
									break;
								case 3:
									mejorar_multigpu_environment (proteina,ligando,param,&vectors_selec,metaheuristic,&devices,1);printf("Mejorado...\n");
									break;
								}
							}
							else
							{
								//printf("Conf para mejorar %d\n",vectors_selec.nconformations);
								switch (param.mode) {
								case 0:
									if (param.montecarlo)
									{
										montecarlo(proteina,ligando,param,&vectors_selec,metaheuristic,&devices,0,1);printf("Mejorado...\n");
									}
									else
									{
										gpu_mejorar (proteina,ligando,param,&vectors_selec,metaheuristic,&devices,0,1);printf("Mejorado...\n");
									}
									break;
								case 1:
								case 2:
									if (param.montecarlo)
									{
										montecarlo_cpp(proteina,ligando,param,vectors_selec.num_surface,vectors_selec.weights,vectors_selec.f_params,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z,vectors_selec.quat_x,vectors_selec.quat_y,vectors_selec.quat_z,vectors_selec.quat_w,vectors_selec.energy.energy,vectors_selec.energy.n_conformation,vectors_selec.nconformations,metaheuristic,1);printf("Mejorado...\n");
									}
									else
									{
										mejorar(proteina,ligando,param,vectors_selec.num_surface, vectors_selec.weights, vectors_selec.f_params,vectors_selec.move_x, vectors_selec.move_y, vectors_selec.move_z, vectors_selec.quat_x, vectors_selec.quat_y, vectors_selec.quat_z, vectors_selec.quat_w, vectors_selec.energy.energy, vectors_selec.energy.n_conformation, vectors_selec.nconformations,metaheuristic,1);printf("Mejorado..\n");
									}
									break;
								case 3:
										mejorar_multigpu (proteina,ligando,param,&vectors_selec,metaheuristic,&devices,1);printf("Mejorado...\n");
									break;
								}
							}
							//incluir_gpu_mejorar (&vectors_selec,&vectors_comb,param,metaheuristic);
							stride_e = vectors_selec.nconformations / vectors_selec.num_surface;
                                                        stride_s = vectors_comb.nconformations / vectors_comb.num_surface;
                                                        #pragma omp parallel for
                                                        for (unsigned int i = 0; i < vectors_comb.num_surface; i++)
                                                                incluir_gpu_by_step (ligando.nlig,vectors_selec.move_x + (stride_e*i), vectors_selec.move_y + (stride_e*i), vectors_selec.move_z + (stride_e*i), vectors_selec.quat_x + (stride_e*i), vectors_selec.quat_y + (stride_e*i), vectors_selec.quat_z + (stride_e*i), vectors_selec.quat_w + (stride_e*i), vectors_selec.energy.energy + (stride_e*i), vectors_selec.energy.n_conformation + (stride_e*i), vectors_comb.move_x + (stride_s*i),  vectors_comb.move_y + (stride_s*i),  vectors_comb.move_z + (stride_s*i),  vectors_comb.quat_x + (stride_s*i),  vectors_comb.quat_y + (stride_s*i),  vectors_comb.quat_z + (stride_s*i),  vectors_comb.quat_w + (stride_s*i),  vectors_comb.energy.energy + (stride_s*i),  vectors_comb.energy.n_conformation + (stride_s*i), stride_e, stride_s, i,param,metaheuristic);
                                                        thrust::sequence(vectors_comb.energy.n_conformation,vectors_comb.energy.n_conformation + vectors_comb.nconformations);
							tmejora_t += wtime() - tmejora;	
							//printf("Conformaciones combinadas %d\n",vectors_comb.nconformations);	
							//for (int l=0;l < vectors_comb.nconformations;l++)
							//	printf("Conformation: %d , move_x %f, move_y %f, move_z %f, quat.x %f, energy %f\n",vectors_comb.energy.n_conformation[l],vectors_comb.move_x[l], vectors_comb.move_y[l],vectors_comb.move_z[l],vectors_comb.quat[l].x,vectors_comb.energy.energy[l]);
							//exit(0);	
						}					
						tincluir = wtime();
						incluir(proteina,ligando,&devices,&vectors_comb,&vectors_ini,metaheuristic,param,&max,&improve,&conformacion);printf("Incluido..\n");
						tincluir_t += wtime() - tincluir;
						limpiar(&vectors_comb,&vectors_selec);
						printf("Max: %f, Improve: %d\n",max,improve);
						//for (int i=0;i<vectors_ini.nconformations;i++) printf("energy[%d]=%f\n",i,vectors_ini.energy.energy[i]);
						count++;								
					}
					ttotal_t = wtime() - ttotal;
					printf("\nPHASES ** :\n");
					if (!param.optimizacion)
					{
						printf("\t*****************\n");
						printf("\tWARM_UP: %f seg\n",twarm_up_t);
						printf("\t*****************\n");
					}
					printf("\tTime Inicializar: %f seg\n",tinicializar_t);
					printf("\tTime Seleccionar: %f seg\n",tseleccion_t);
					printf("\tTime Combinar: %f seg\n",tcombinar_t);
					if (metaheuristic.PEMSel > 0) printf("\tTime Mejorar: %lf seg\n",tmejora_t);
					if (metaheuristic.PEMUCom > 0) printf("\tTime Mutation: %lf seg\n",tmut_t);
					printf("\tTime Incluir: %f seg\n",tincluir_t);			
					printf("\tTime Total: %f seg\n",ttotal_t);
					break;							
				default:
					printf("Wrong mode type: %d.  Use -h for help.\n", mode);
					return 0;
			}	
			break;
		case 1:
			switch (param.mode) {
				case 0:
				case 1:
				case 2:
				case 3:
					ttotal = wtime();	
					tinicializar = wtime();
					//param.scoring_function_type = 0;
					inicializar_flex(proteina,ligando,&vectors,&vectors_ini,param,metaheuristic,&devices,&flexibility_params,&flexibility_conformations,dqn_data);
					tinicializar_t = wtime() - tinicializar;
					//EXTRAER CONFORMACION
					/*max = vectors_ini.energy.energy[0];
					int n_c,shift;
					shift = vectors_ini.nconformations / vectors_ini.num_surface;
					for (int i=0;i<vectors_ini.num_surface;i++)
					if (vectors_ini.energy.energy[i*shift] < max)
					{
						max = vectors_ini.energy.energy[i*shift];
						n_c = i*shift;
					}
					printf("MAX %f , conf %d\n",max,n_c);*/
					//configurar_ligandos_finales(proteina,ligando,&ligtemp,&vectors_ini,param,metaheuristic);
					//maximo(&vectors_ini,&max);	
					//exit(0);				
						
					max=0;
					while ((count < metaheuristic.NMIFin) && (improve < metaheuristic.NIRFin))
					{	
						printf("%d pass FLEX in progress....\n",count+1);			
						tseleccion = wtime();
						seleccionar_flex(ligando.nlig,&vectors_ini,&vectors_selec,metaheuristic,param);printf("Seleccionado..\n");			
						tseleccion_t += wtime() - tseleccion;
						//exit(0);
						//printf("\tTime Seleccionar: %lf seg\n",tseleccion_t);
						tcombinar = wtime ();
						switch (param.mode) {
							case 0:
								combinar_gpu_flex (proteina,ligando,&vectors_selec,&vectors_comb,metaheuristic,param,&devices,&flexibility_params,0);printf("Combinado..\n");
								//maximo(&vectors_comb,&max);
								//printf("MAX: %f\n",max);
								//max = 0;	
								break;
							case 1:
							case 2:
								combinar_flex (proteina,ligando,&vectors_selec,&vectors_comb,metaheuristic,param,&flexibility_conformations,&flexibility_params);printf("Combinado..\n");
								
								break;
							case 3:
								combinar_multigpu_flex (proteina,ligando,&vectors_selec,&vectors_comb,metaheuristic,param,&devices,&flexibility_params);printf("Combinado..\n");								
								break;
						}
						tcombinar_t += wtime() - tcombinar;				
						//exit(0);	
						 tmut = wtime();
                                                if (metaheuristic.PEMUCom > 0)
                                                {
                                                        //seleccionar_mutar_flex (ligando.nlig,&vectors_comb,&vectors_selec,param,metaheuristic);
                                                        switch (param.mode) {
                                                         	case 0:
									mutation_gpu_flex (proteina,ligando,param,vectors_selec.weights,vectors_selec.f_params,&flexibility_params,vectors_selec.conformations_x,vectors_selec.conformations_y,vectors_selec.conformations_z,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z,vectors_selec.quat_x,vectors_selec.quat_y,vectors_selec.quat_z,vectors_selec.quat_w,vectors_selec.energy.energy, vectors_selec.energy.n_conformation,vectors_selec.nconformations,metaheuristic,&devices,param.ngpu,0);
                                                                        break;
                                                                case 1:
                                                                case 2:
                                                                       	mutation_cpp_flex (proteina,ligando,param,metaheuristic,&flexibility_params, vectors_selec.weights,vectors_selec.f_params,vectors_selec.conformations_x,vectors_selec.conformations_y,vectors_selec.conformations_z,vectors_selec.move_x,vectors_selec.move_y,vectors_selec.move_z,vectors_selec.quat_x,vectors_selec.quat_y,vectors_selec.quat_z,vectors_selec.quat_w,vectors_selec.energy.energy, vectors_selec.energy.n_conformation, vectors_selec.num_surface, vectors_selec.nconformations);
                                                                       break;
                                                                case 3:
                                                                        mutation_multigpu_flex (ligando,proteina,vectors_selec.num_surface,&flexibility_params, vectors_selec.conformations_x, vectors_selec.conformations_y, vectors_selec.conformations_z, vectors_selec.move_x, vectors_selec.move_y, vectors_selec.move_z, vectors_selec.quat_x,  vectors_selec.quat_y,  vectors_selec.quat_z,  vectors_selec.quat_w,  vectors_selec.energy.energy,  vectors_selec.energy.n_conformation, vectors_selec.weights, vectors_selec.f_params, vectors_selec.nconformations, param, metaheuristic, &devices, 2);
                                                                        break;
                                                                }
                                                                //incluir_gpu_mejorar_flex (ligando.nlig,&vectors_selec,&vectors_comb,param,metaheuristic);
								stride_e = vectors_selec.nconformations / vectors_selec.num_surface;
                                				stride_s = vectors_comb.nconformations / vectors_comb.num_surface;
								#pragma omp parallel for
                                				for (unsigned int i = 0; i < vectors_comb.num_surface; i++)
									incluir_gpu_flex_by_step (ligando.nlig,vectors_selec.conformations_x + (stride_e*ligando.nlig*i), vectors_selec.conformations_y + (stride_e*ligando.nlig*i), vectors_selec.conformations_z + (stride_e*ligando.nlig*i), vectors_selec.move_x + (stride_e*i), vectors_selec.move_y + (stride_e*i), vectors_selec.move_z + (stride_e*i), vectors_selec.quat_x + (stride_e*i), vectors_selec.quat_y + (stride_e*i), vectors_selec.quat_z + (stride_e*i), vectors_selec.quat_w + (stride_e*i), vectors_selec.energy.energy + (stride_e*i), vectors_selec.energy.n_conformation + (stride_e*i), vectors_comb.conformations_x + (stride_s*ligando.nlig*i), vectors_comb.conformations_y + (stride_s*ligando.nlig*i), vectors_comb.conformations_z + (stride_s*ligando.nlig*i), vectors_comb.move_x + (stride_s*i),  vectors_comb.move_y + (stride_s*i),  vectors_comb.move_z + (stride_s*i),  vectors_comb.quat_x + (stride_s*i),  vectors_comb.quat_y + (stride_s*i),  vectors_comb.quat_z + (stride_s*i),  vectors_comb.quat_w + (stride_s*i),  vectors_comb.energy.energy + (stride_s*i),  vectors_comb.energy.n_conformation + (stride_s*i), stride_e, stride_s, i,param,metaheuristic);
								thrust::sequence(vectors_comb.energy.n_conformation,vectors_comb.energy.n_conformation + vectors_comb.nconformations);
                                                }
                                                tmut_t += wtime() - tmut;
						if (metaheuristic.PEMSel > 0)
						{
							seleccionar_mejorar_flex (ligando.nlig,&vectors_comb,&vectors_selec,param,metaheuristic);
							tmejora = wtime();
							if (metaheuristic.NEEImp > 0)
                                                        {
                                                                 //printf("Conf para mejorar %d\n",vectors_selec.nconformations);
                                                                switch (param.mode) {
                                                                	case 0:
										if (param.montecarlo)
                                                                                {
                                                                                        montecarlo_flex_environment (proteina,ligando,param,&vectors_selec,metaheuristic,&devices,&flexibility_params,0,1);printf("Mejorado..\n");
                                                                                }
                                                                                else
                                                                                {
                                                                                        gpu_mejorar_flex_environment (proteina,ligando,param,&vectors_selec,metaheuristic,&devices,0,&flexibility_params,1);printf("Mejorado..\n");
                                                                                }
                                                                                break;
                                                                	case 1:
                                                                	case 2:
										if (param.montecarlo)
                                                                                {
                                                                                        montecarlo_cpp_flex_environment(proteina,ligando,param,vectors_selec.num_surface, vectors_selec.weights, vectors_selec.f_params, vectors_selec.conformations_x, vectors_selec.conformations_y, vectors_selec.conformations_z, vectors_selec.move_x, vectors_selec.move_y, vectors_selec.move_z, vectors_selec.quat_x, vectors_selec.quat_y, vectors_selec.quat_z, vectors_selec.quat_w, vectors_selec.energy.energy, vectors_selec.energy.n_conformation, vectors_selec.nconformations,metaheuristic,&flexibility_params,&flexibility_conformations,1);printf("Mejorado..\n");
                                                                                }
                                                                                else
                                                                                {
                                                                                        mejorar_flex_environment(proteina,ligando,param,vectors_selec.weights,vectors_selec.f_params,vectors_selec.conformations_x, vectors_selec.conformations_y, vectors_selec.conformations_z, vectors_selec.move_x, vectors_selec.move_y, vectors_selec.move_z, vectors_selec.quat_x, vectors_selec.quat_y, vectors_selec.quat_z, vectors_selec.quat_w, vectors_selec.energy.energy, vectors_selec.energy.n_conformation, vectors_selec.num_surface, vectors_selec.nconformations, metaheuristic, &flexibility_params, &flexibility_conformations,1);printf("Mejorado..\n");
                                                                                }
										break;
									case 3:
										mejorar_multigpu_flex_environment (proteina,ligando,param,&vectors_selec,metaheuristic,&devices,1,&flexibility_params);printf("Mejorado..\n");
										break;
										
								}
							}
							else
							{
								//printf("Conf para mejorar %d\n",vectors_selec.nconformations);
								switch (param.mode) {
									case 0:
										if (param.montecarlo)
										{
											montecarlo_flex(proteina,ligando,param,&vectors_selec,metaheuristic,&devices,&flexibility_params,0,1);printf("Mejorado..\n");
										}
										else
										{										
											gpu_mejorar_flex (proteina,ligando,param,&vectors_selec,metaheuristic,&devices,0,&flexibility_params,1);printf("Mejorado..\n");															
										}
										break;									
									case 1:									
									case 2:
										if (param.montecarlo)		
										{
											montecarlo_cpp_flex(proteina,ligando,param,vectors_selec.num_surface, vectors_selec.weights, vectors_selec.f_params, vectors_selec.conformations_x, vectors_selec.conformations_y, vectors_selec.conformations_z, vectors_selec.move_x, vectors_selec.move_y, vectors_selec.move_z, vectors_selec.quat_x, vectors_selec.quat_y, vectors_selec.quat_z, vectors_selec.quat_w, vectors_selec.energy.energy, vectors_selec.energy.n_conformation, vectors_selec.nconformations,metaheuristic,&flexibility_params,&flexibility_conformations,1);printf("Mejorado..\n");						
										}
										else
										{
											mejorar_flex(proteina,ligando,param,vectors_selec.weights,vectors_selec.f_params,vectors_selec.conformations_x, vectors_selec.conformations_y, vectors_selec.conformations_z, vectors_selec.move_x, vectors_selec.move_y, vectors_selec.move_z, vectors_selec.quat_x, vectors_selec.quat_y, vectors_selec.quat_z, vectors_selec.quat_w, vectors_selec.energy.energy, vectors_selec.energy.n_conformation, vectors_selec.num_surface, vectors_selec.nconformations, metaheuristic, &flexibility_params, &flexibility_conformations,1);printf("Mejorado..\n");
										}				
										break;
									case 3:
											mejorar_multigpu_flex (proteina,ligando,param,&vectors_selec,metaheuristic,&devices,1,&flexibility_params);printf("Mejorado..\n");
										break;
								}
							}
							//incluir_gpu_mejorar_flex (ligando.nlig,&vectors_selec,&vectors_comb,param,metaheuristic);
							stride_e = vectors_selec.nconformations / vectors_selec.num_surface;
							stride_s = vectors_comb.nconformations / vectors_comb.num_surface;
                                                        #pragma omp parallel for
                                                        for (unsigned int i = 0; i < vectors_comb.num_surface; i++)
								incluir_gpu_flex_by_step (ligando.nlig,vectors_selec.conformations_x + (stride_e*ligando.nlig*i), vectors_selec.conformations_y + (stride_e*ligando.nlig*i), vectors_selec.conformations_z + (stride_e*ligando.nlig*i), vectors_selec.move_x + (stride_e*i), vectors_selec.move_y + (stride_e*i), vectors_selec.move_z + (stride_e*i), vectors_selec.quat_x + (stride_e*i), vectors_selec.quat_y + (stride_e*i), vectors_selec.quat_z + (stride_e*i), vectors_selec.quat_w + (stride_e*i), vectors_selec.energy.energy + (stride_e*i), vectors_selec.energy.n_conformation + (stride_e*i), vectors_comb.conformations_x + (stride_s*ligando.nlig*i), vectors_comb.conformations_y + (stride_s*ligando.nlig*i), vectors_comb.conformations_z + (stride_s*ligando.nlig*i), vectors_comb.move_x + (stride_s*i),  vectors_comb.move_y + (stride_s*i),  vectors_comb.move_z + (stride_s*i),  vectors_comb.quat_x + (stride_s*i),  vectors_comb.quat_y + (stride_s*i),  vectors_comb.quat_z + (stride_s*i),  vectors_comb.quat_w + (stride_s*i),  vectors_comb.energy.energy + (stride_s*i),  vectors_comb.energy.n_conformation + (stride_s*i), stride_e, stride_s, i,param,metaheuristic);
							thrust::sequence(vectors_comb.energy.n_conformation,vectors_comb.energy.n_conformation + vectors_comb.nconformations);
							tmejora_t += wtime() - tmejora;	
							//maximo(&vectors_comb,&max);
							//printf("MAX: %f\n",max);
							//exit(0);
							//printf("Conformaciones combinadas %d\n",vectors_comb.nconformations);	
							//for (int l=0;l < vectors_comb.nconformations;l++)
							//	printf("Conformation: %d , move_x %f, move_y %f, move_z %f, quat.x %f, energy %f\n",vectors_comb.energy.n_conformation[l],vectors_comb.move_x[l], vectors_comb.move_y[l],vectors_comb.move_z[l],vectors_comb.quat[l].x,vectors_comb.energy.energy[l]);
							//exit(0);	
						}					
						tincluir = wtime();
						incluir_flex(ligando.nlig,&vectors_comb,&vectors_ini,metaheuristic,param,&max,&improve,&conformacion);//printf("Incluido..\n");
						tincluir_t += wtime() - tincluir;
						limpiar(&vectors_comb,&vectors_selec);
						printf("Max: %f, Improve: %d\n",max,improve);
						//for (int i=0;i<vectors_ini.nconformations;i++) printf("energy[%d]=%f\n",i,vectors_ini.energy.energy[i]);
						count++;								
					}
					ttotal_t = wtime() - ttotal;
					printf("\nPHASES FLEXIBILYTY**:\n");
					if (!param.optimizacion)
					{
						printf("\t*****************\n");
						printf("\tWARM_UP: %f seg\n",twarm_up_t);
						printf("\t*****************\n");
					}
					printf("\tTime Inicializar FLEX: %f seg\n",tinicializar_t);
					printf("\tTime Seleccionar FLEX: %f seg\n",tseleccion_t);
					printf("\tTime Combinar FLEX: %f seg\n",tcombinar_t);
					if (metaheuristic.PEMSel > 0) printf("\tTime Mejorar FLEX: %lf seg\n",tmejora_t);
					if (metaheuristic.PEMUCom > 0) printf("\tTime Mutation: %lf seg\n",tmut_t);
					printf("\tTime Incluir FLEX: %f seg\n",tincluir_t);			
					printf("\tTime Total FLEX: %f seg\n",ttotal_t);
					break;
				default:
					printf("Wrong mode type: %d.  Use -h for help.\n", mode);
					return 0;					
			}
			break;
	}
	
			

			
	//EXTRAER CONFORMACION
/*for (int k = 0;k<vectors_ini.nconformations;k++)
                        {
                                if (vectors_ini.energy.energy[k] < -772)
                                {
                                        printf("k %d en %f m_x %f m_y %f m_z %f q_x %f q_y %f q_z %f q_w %f\n",k,vectors_ini.energy.energy[k],vectors_ini.move_x[k],vectors_ini.move_y[k],vectors_ini.move_z[k],vectors_ini.quat_x[k],vectors_ini.quat_y[k],vectors_ini.quat_z[k],vectors_ini.quat_w[k]);
                                        for (int l=0;l<64;l++) printf("atom %f %f %f\n",vectors_ini.conformations_x[k*ligando.nlig+l],vectors_ini.conformations_y[k*ligando.nlig+l],vectors_ini.conformations_z[k*ligando.nlig+l]);
                                }
                        }*/
	configurar_ligandos_finales(proteina,ligando,&ligtemp,&vectors_ini,param,metaheuristic,&flexibility_params);

	
	cudaDeviceReset();

	//printf("\nPrinting Energies...\n");
	//writeLigandEnergies (file_e,nconformations,energy_vdw_CPU,energy_vdw_GPU_full);
	printf("\n\n");
	system("date");
	return 0;	

}

