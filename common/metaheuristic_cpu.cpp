#include <thrust/sequence.h>
#include "energy_common.h" 
#include "energy_CpuSolver.h"
#include "energy_CpuSolver_vs.h"
#include "metaheuristic_mgpu_common.h"
#include "metaheuristic_sel_common.h"
#include "metaheuristic_sel_vs_common.h"
#include "metaheuristic_comb_common.h"
#include "metaheuristic_comb_vs_common.h"
#include "metaheuristic_inc_common.h"
#include "metaheuristic_inc_vs_common.h"
#include "metaheuristic_ini_vs.h"
#include "energy_struct.h"
#include "wtime.h"
#include <omp.h>

using namespace std;

extern void optimizar_parametros_ini (struct metaheuristic_t *metaheuristic,unsigned int hilo1_n1)
{
	metaheuristic->Threads1Ini = hilo1_n1;
}

extern void optimizar_parametros_inc (struct metaheuristic_t *metaheuristic,unsigned int hilo1_n1)
{
	metaheuristic->Threads1Inc = hilo1_n1;
}

extern void optimizar_parametros_imp (struct metaheuristic_t *metaheuristic,unsigned int hilo1_n1)
{
	metaheuristic->Threads1Imp = hilo1_n1;
}

extern void optimizar_parametros_sel (struct metaheuristic_t *metaheuristic,unsigned int hilo1_n1)
{
	metaheuristic->Threads1Sel = hilo1_n1;
}

extern void optimizar_parametros_comb (struct metaheuristic_t *metaheuristic, unsigned int hilo1_n1, double tiempo_act_hilos_n1, unsigned int hilos1_n1_n2, unsigned int hilos2_n1_n2, double tiempo_act_hilos_n1_n2)
{
	if (tiempo_act_hilos_n1 <= tiempo_act_hilos_n1_n2)
	{
		metaheuristic->Threads1Com = hilo1_n1;
		metaheuristic->Threads2Com = 1;
	}
	else
	{
		metaheuristic->Threads1Com = hilos1_n1_n2;
		metaheuristic->Threads2Com = hilos2_n1_n2;
	}
}

extern void optimizar_parametros_fit (struct metaheuristic_t *metaheuristic,unsigned int hilo1_n1, double tiempo_act_hilos_n1, unsigned int hilos1_n1_n2, unsigned int hilos2_n1_n2, double tiempo_act_hilos_n1_n2)
{
	if (tiempo_act_hilos_n1 <= tiempo_act_hilos_n1_n2)
	{
		metaheuristic->Threads1Fit = hilo1_n1;
		metaheuristic->Threads2Fit = 1;
		
		//printf("HACE bien\n");	
	}
	else
	{
		metaheuristic->Threads1Fit = hilos1_n1_n2;
		metaheuristic->Threads2Fit = hilos2_n1_n2;		
	}
}

extern void final_parametros_paralelos (struct metaheuristic_t *metaheuristic)
{
	printf("\Valores parámetros paralelos:\n");
	printf("\tNivel 1. Función Inicializar: %d\n",metaheuristic->Threads1Ini);
	printf("\tNivel 1. Función Seleccionar: %d\n",metaheuristic->Threads1Sel);
	printf("\tNivel 1. Función Combinar: %d\n",metaheuristic->Threads1Com);
	printf("\tNivel 2. Función Combinar: %d\n",metaheuristic->Threads2Com);
	printf("\tNivel 1. Función Mejorar: %d\n",metaheuristic->Threads1Imp);
	printf("\tNivel 1. Función Incluir: %d\n",metaheuristic->Threads1Inc);
	printf("\tNivel 1. Cálculo de Fitness: %d\n",metaheuristic->Threads1Fit);
	printf("\tNivel 2. Cálculo de Fitness: %d\n",metaheuristic->Threads2Fit);	
}

extern void escribir_openmp (struct param_t param, struct metaheuristic_t *metaheuristic)
{
	//ofstream *file;

	ofstream file(param.openmp, ios::out | ios::trunc);

	if (!file.good()) {
		cout << "No se puede abrir el fichero " << param.openmp << endl;		
		return;
	}
	file << "*** PARAMETROS DE OPTIMIZACION OPEN_MP ***" << endl;
	file << metaheuristic->Threads1Ini << endl;
	file << metaheuristic->Threads1Fit << endl;
	file << metaheuristic->Threads2Fit << endl;
	file << metaheuristic->Threads1Sel << endl;
	file << metaheuristic->Threads1Com << endl;
	file << metaheuristic->Threads2Com << endl;
	file << metaheuristic->Threads1Imp << endl;
	file << metaheuristic->Threads1Inc << endl;
	file.close();	
}
extern void warm_up_openmp (struct ligand_t ligando, struct receptor_t proteina,  struct vectors_t *vectors_e, struct vectors_t *vectors_o,unsigned int surface, struct force_field_param_t *f_params, type_data *weights, unsigned int nconformations_pc, struct param_t param, struct metaheuristic_t *metaheuristic)
{
	unsigned int total_hilos_n1,total_hilos_n2,hilo1_n1,hilos1_n1_n2,hilos2_n1_n2;
	double twarm_up, twarm_up_t;
	double tiempo_i, tiempo_f;
	double tiempo_act_hilos_n1 = 1000,tiempo_act_hilos_n1_n2 = 1000;
	copy_surface(vectors_e,vectors_o);
	vectors_o->nconformations = vectors_o->num_surface * nconformations_pc;
	vectors_o->move_x = (type_data *)malloc(sizeof(type_data)*vectors_o->nconformations);
	vectors_o->move_y = (type_data *)malloc(sizeof(type_data)*vectors_o->nconformations);
	vectors_o->move_z = (type_data *)malloc(sizeof(type_data)*vectors_o->nconformations);
	vectors_o->quat_x = (float *)malloc(sizeof(float)*vectors_o->nconformations);
	vectors_o->quat_y = (float *)malloc(sizeof(float)*vectors_o->nconformations);
	vectors_o->quat_z = (float *)malloc(sizeof(float)*vectors_o->nconformations);
	vectors_o->quat_w = (float *)malloc(sizeof(float)*vectors_o->nconformations);
	vectors_o->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int )*vectors_o->nconformations);
	vectors_o->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_o->nconformations);
	thrust::sequence(vectors_o->energy.n_conformation,vectors_o->energy.n_conformation + vectors_o->nconformations);
	generate_positions_cpp_warm_up (param,vectors_o,*metaheuristic);	
	
	//UN NIVEL
	twarm_up = wtime();
	printf("\n**Optimización de la fase de INICIALIZAR**\n");	
	total_hilos_n1 = omp_get_num_procs();
	while (total_hilos_n1 > 1)
	{
		tiempo_i = wtime();		
		move_cpp_warm_up(vectors_o->num_surface,vectors_o->move_x,vectors_o->move_y,vectors_o->move_z,vectors_o->surface_x,vectors_o->surface_y,vectors_o->surface_z,param,param.conf_warm_up_cpu,total_hilos_n1);
		tiempo_f = wtime() - tiempo_i;
		
		if (tiempo_f < tiempo_act_hilos_n1)
		{
			tiempo_act_hilos_n1 = tiempo_f;
			hilo1_n1 = total_hilos_n1;
		}				
		total_hilos_n1--;
		printf("Hilos N1: %d \tTiempo: %f\n",total_hilos_n1+1,tiempo_f);
		
	}
	printf("MEJOR Hilos N1 función INICIALIZAR: %d tiempo mejor: %f\n",hilo1_n1,tiempo_act_hilos_n1);	
	optimizar_parametros_ini (metaheuristic,hilo1_n1);
	
	tiempo_act_hilos_n1 = 1000;
	tiempo_act_hilos_n1_n2 = 1000;
	//UN NIVEL
	printf("\n**Optimización de la fase de SELECCIONAR**\n");	
	total_hilos_n1 = omp_get_num_procs();
	while (total_hilos_n1 > 1)
	{
		tiempo_i = wtime();		
		seleccionar_warm_up (vectors_o,param,total_hilos_n1);
		tiempo_f = wtime() - tiempo_i;
		
		if (tiempo_f < tiempo_act_hilos_n1)
		{
			tiempo_act_hilos_n1 = tiempo_f;
			hilo1_n1 = total_hilos_n1;
		}				
		total_hilos_n1--;
		printf("Hilos N1: %d \tTiempo: %f\n",total_hilos_n1+1,tiempo_f);
		
	}
	printf("MEJOR Hilos N1 función SELECCIONAR: %d tiempo mejor: %f\n",hilo1_n1,tiempo_act_hilos_n1);	
	optimizar_parametros_sel (metaheuristic,hilo1_n1);	
		
	tiempo_act_hilos_n1 = 1000;
	tiempo_act_hilos_n1_n2 = 1000;
	//UN NIVEL
	printf("\n**Optimización valores paralelos de la fase COMBINAR**\n");
	total_hilos_n1 = omp_get_num_procs();
	while (total_hilos_n1 > 1)
	{
		tiempo_i = wtime();		
		omp_set_num_threads(total_hilos_n1);
		combinar_warm_up (proteina,ligando,vectors_o,param,total_hilos_n1,1);
		tiempo_f = wtime() - tiempo_i;
		
		if (tiempo_f < tiempo_act_hilos_n1)
		{
			tiempo_act_hilos_n1 = tiempo_f;
			hilo1_n1 = total_hilos_n1;
		}				
		total_hilos_n1--;
		printf("Hilos N1: %d \tTiempo: %f\n",total_hilos_n1+1,tiempo_f);
		
	}
	printf("MEJOR Hilos N1 función COMBINAR: %d tiempo mejor: %f\n",hilo1_n1,tiempo_act_hilos_n1);	
	optimizar_parametros_comb (metaheuristic,hilo1_n1,tiempo_act_hilos_n1,hilo1_n1,1,tiempo_act_hilos_n1_n2);
	
	tiempo_act_hilos_n1 = 1000;
	tiempo_act_hilos_n1_n2 = 1000;
	//UN NIVEL
	printf("\n**Optimización de la fase de MEJORAR**\n");	
	total_hilos_n1 = omp_get_num_procs();
	while (total_hilos_n1 > 1)
	{
		tiempo_i = wtime();		
		move_mejora_cpp_warm_up(vectors_o->num_surface,vectors_o->move_x,vectors_o->move_y,vectors_o->move_z,param,param.conf_warm_up_cpu,total_hilos_n1);
		tiempo_f = wtime() - tiempo_i;
		
		if (tiempo_f < tiempo_act_hilos_n1)
		{
			tiempo_act_hilos_n1 = tiempo_f;
			hilo1_n1 = total_hilos_n1;
		}				
		total_hilos_n1--;
		printf("Hilos N1: %d \tTiempo: %f\n",total_hilos_n1+1,tiempo_f);
		
	}
	printf("MEJOR Hilos N1 función MEJORAR: %d tiempo mejor: %f\n",hilo1_n1,tiempo_act_hilos_n1);	
	optimizar_parametros_imp (metaheuristic,hilo1_n1);	
	
	tiempo_act_hilos_n1 = 1000;
	tiempo_act_hilos_n1_n2 = 1000;
	//UN NIVEL
	printf("\n**Optimización de la fase de INCLUIR**\n");	
	total_hilos_n1 = omp_get_num_procs();
	while (total_hilos_n1 > 1)
	{
		tiempo_i = wtime();		
		incluir_warm_up (vectors_o,param,total_hilos_n1);
		tiempo_f = wtime() - tiempo_i;
		
		if (tiempo_f < tiempo_act_hilos_n1)
		{
			tiempo_act_hilos_n1 = tiempo_f;
			hilo1_n1 = total_hilos_n1;
		}				
		total_hilos_n1--;
		printf("Hilos N1: %d \tTiempo: %f\n",total_hilos_n1+1,tiempo_f);
		
	}
	printf("MEJOR Hilos N1 función INCLUIR: %d tiempo mejor: %f\n",hilo1_n1,tiempo_act_hilos_n1);	
	optimizar_parametros_inc (metaheuristic,hilo1_n1);	
	
	tiempo_act_hilos_n1 = 1000;
	tiempo_act_hilos_n1_n2 = 1000;	
	//UN NIVEL
	printf("\n**Optimización valores paralelos CALCULO DE FITNESS**\n");
	total_hilos_n1 = omp_get_num_procs();
	while (total_hilos_n1 > 1)
	{
		tiempo_i = wtime();		
		omp_set_num_threads(total_hilos_n1);
		vdwforces_warm_up (param, proteina.atoms,ligando.atoms,ligando.nlig, proteina.rec_x,proteina.rec_y,proteina.rec_z,ligando.lig_x,ligando.lig_y,ligando.lig_z,vectors_o->move_x,vectors_o->move_y, vectors_o->move_z, vectors_o->quat_x, vectors_o->quat_y, vectors_o->quat_z, vectors_o->quat_w, proteina.rectype,ligando.ligtype, vectors_o->energy, weights, f_params, vectors_o->nconformations, total_hilos_n1,1);
		tiempo_f = wtime() - tiempo_i;
		
		if (tiempo_f < tiempo_act_hilos_n1)
		{
			tiempo_act_hilos_n1 = tiempo_f;
			hilo1_n1 = total_hilos_n1;
		}				
		total_hilos_n1--;
		printf("Hilos N1: %d \tTiempo: %f\n",total_hilos_n1+1,tiempo_f);
		
	}
	printf("MEJOR Hilos N1: %d tiempo mejor: %f\n",hilo1_n1,tiempo_act_hilos_n1);	
	
	optimizar_parametros_fit (metaheuristic,hilo1_n1,tiempo_act_hilos_n1,hilo1_n1,1,tiempo_act_hilos_n1_n2);
	
	twarm_up_t = wtime() - twarm_up;
	printf("\n\t*****************\n");
	printf("\tWARM_UP: %f seg\n",twarm_up_t);
	printf("\t*****************\n");
	final_parametros_paralelos (metaheuristic);
	escribir_openmp(param,metaheuristic);
	
	free(vectors_o->move_x);
	free(vectors_o->move_y);
	free(vectors_o->move_z);
	free(vectors_o->quat_x);
	free(vectors_o->quat_y);
	free(vectors_o->quat_z);
	free(vectors_o->quat_w);
	free(vectors_o->energy.energy);
	free(vectors_o->energy.n_conformation);
}

extern void warm_up_openmp_vs (type_data *c_x, type_data *c_y, type_data *c_z, struct ligand_params_t *ligand_params, type_data *ql, char *ligtype, char *subtype, unsigned int *bonds, char *nbonds, struct receptor_t proteina,  struct vectors_t *vectors_e, struct vectors_t *vectors_o, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations_pf, struct param_t param, struct metaheuristic_t *metaheuristic)
{
 	unsigned int total_hilos_n1,total_hilos_n2,hilo1_n1,hilos1_n1_n2,hilos2_n1_n2;
        double twarm_up, twarm_up_t;
        double tiempo_i, tiempo_f;
        double tiempo_act_hilos_n1 = 1000,tiempo_act_hilos_n1_n2 = 1000;
        //copy_surface(vectors_e,vectors_o);
        vectors_o->nconformations = 8 * nconformations_pf;
	vectors_o->files = 8;
	vectors_o->nlig = vectors_e->nlig;
	vectors_o->ac_x = vectors_e->ac_x;
	vectors_o->ac_y = vectors_e->ac_y;
	vectors_o->ac_z = vectors_e->ac_z;
	
	vectors_o->conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_o->nlig*vectors_o->nconformations);
        vectors_o->conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_o->nlig*vectors_o->nconformations);
        vectors_o->conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_o->nlig*vectors_o->nconformations);
        vectors_o->ql = (type_data *)malloc(sizeof(type_data)*vectors_o->nlig*vectors_o->nconformations);
        vectors_o->ligtype = (char *)malloc(sizeof(char)*vectors_o->nlig*vectors_o->nconformations);
        vectors_o->subtype = (char *)malloc(sizeof(char)*vectors_o->nlig*SUBTYPEMAXLEN*vectors_o->nconformations);
        vectors_o->bonds = (unsigned int *)malloc(sizeof(unsigned int)*vectors_o->nlig*MAXBOND*vectors_o->nconformations);
        vectors_o->nbonds = (char *)malloc(sizeof(char)*vectors_o->nlig*vectors_o->nconformations);

        vectors_o->move_x = (type_data *)malloc(sizeof(type_data)*vectors_o->nconformations);
        vectors_o->move_y = (type_data *)malloc(sizeof(type_data)*vectors_o->nconformations);
        vectors_o->move_z = (type_data *)malloc(sizeof(type_data)*vectors_o->nconformations);
        vectors_o->quat_x = (float *)malloc(sizeof(float)*vectors_o->nconformations);
        vectors_o->quat_y = (float *)malloc(sizeof(float)*vectors_o->nconformations);
        vectors_o->quat_z = (float *)malloc(sizeof(float)*vectors_o->nconformations);
        vectors_o->quat_w = (float *)malloc(sizeof(float)*vectors_o->nconformations);
        vectors_o->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int )*vectors_o->nconformations);
        vectors_o->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_o->nconformations);
        thrust::sequence(vectors_o->energy.n_conformation,vectors_o->energy.n_conformation + vectors_o->nconformations);
        
	fill_conformations_vs(param,*metaheuristic,c_x,c_y,c_z,ql,ligtype,subtype,nbonds,bonds,vectors_o);
	generate_positions_cpp_vs (param,vectors_o,*metaheuristic);

	twarm_up = wtime();
        printf("\n**Optimización de la fase de INICIALIZAR VS**\n");
        total_hilos_n1 = omp_get_num_procs();
        while (total_hilos_n1 > 1)
        {
                tiempo_i = wtime();
		move_vs_warm_up (vectors_o->files,vectors_o->move_x,vectors_o->move_y,vectors_o->move_z,vectors_o->ac_x,vectors_o->ac_y,vectors_o->ac_z,param,param.conf_warm_up_cpu,total_hilos_n1);
                tiempo_f = wtime() - tiempo_i;

                if (tiempo_f < tiempo_act_hilos_n1)
                {
                        tiempo_act_hilos_n1 = tiempo_f;
                        hilo1_n1 = total_hilos_n1;
                }
                total_hilos_n1--;
                printf("Hilos N1: %d \tTiempo: %f\n",total_hilos_n1+1,tiempo_f);

        }
        printf("MEJOR Hilos N1 función INICIALIZAR VS: %d tiempo mejor: %f\n",hilo1_n1,tiempo_act_hilos_n1);
        optimizar_parametros_ini (metaheuristic,hilo1_n1);
	optimizar_parametros_imp (metaheuristic,hilo1_n1);

        tiempo_act_hilos_n1 = 1000;
        tiempo_act_hilos_n1_n2 = 1000;
	//UN NIVEL
        printf("\n**Optimización de la fase de SELECCIONAR VS**\n");
        total_hilos_n1 = omp_get_num_procs();
        while (total_hilos_n1 > 1)
        {
                tiempo_i = wtime();
                seleccionar_vs_warm_up (vectors_o,param,total_hilos_n1);
                tiempo_f = wtime() - tiempo_i;

                if (tiempo_f < tiempo_act_hilos_n1)
                {
                        tiempo_act_hilos_n1 = tiempo_f;
                        hilo1_n1 = total_hilos_n1;
                }
                total_hilos_n1--;
                printf("Hilos N1: %d \tTiempo: %f\n",total_hilos_n1+1,tiempo_f);

        }
        printf("MEJOR Hilos N1 función SELECCIONAR VS: %d tiempo mejor: %f\n",hilo1_n1,tiempo_act_hilos_n1);
        optimizar_parametros_sel (metaheuristic,hilo1_n1);

        tiempo_act_hilos_n1 = 1000;
        tiempo_act_hilos_n1_n2 = 1000;

	//UN NIVEL
        printf("\n**Optimización valores paralelos de la fase COMBINAR VS**\n");
        total_hilos_n1 = omp_get_num_procs();
        while (total_hilos_n1 > 1)
        {
                tiempo_i = wtime();
                omp_set_num_threads(total_hilos_n1);
                combinar_vs_warm_up (proteina,ligand_params,vectors_o,param,total_hilos_n1);
                tiempo_f = wtime() - tiempo_i;

                if (tiempo_f < tiempo_act_hilos_n1)
                {
                        tiempo_act_hilos_n1 = tiempo_f;
                        hilo1_n1 = total_hilos_n1;
                }
                total_hilos_n1--;
                printf("Hilos N1: %d \tTiempo: %f\n",total_hilos_n1+1,tiempo_f);

        }
        printf("MEJOR Hilos N1 función COMBINAR VS: %d tiempo mejor: %f\n",hilo1_n1,tiempo_act_hilos_n1);
	optimizar_parametros_comb (metaheuristic,hilo1_n1,tiempo_act_hilos_n1,hilo1_n1,1,tiempo_act_hilos_n1_n2);
	//UN NIVEL
        printf("\n**Optimización de la fase de INCLUIR VS**\n");
        total_hilos_n1 = omp_get_num_procs();
        while (total_hilos_n1 > 1)
        {
                tiempo_i = wtime();
		incluir_vs_warm_up (vectors_o,param,total_hilos_n1);
                tiempo_f = wtime() - tiempo_i;

                if (tiempo_f < tiempo_act_hilos_n1)
                {
                        tiempo_act_hilos_n1 = tiempo_f;
                        hilo1_n1 = total_hilos_n1;
                }
                total_hilos_n1--;
                printf("Hilos N1: %d \tTiempo: %f\n",total_hilos_n1+1,tiempo_f);

        }
        printf("MEJOR Hilos N1 función INCLUIR VS: %d tiempo mejor: %f\n",hilo1_n1,tiempo_act_hilos_n1);
        optimizar_parametros_inc (metaheuristic,hilo1_n1);

        tiempo_act_hilos_n1 = 1000;
        tiempo_act_hilos_n1_n2 = 1000;
	 //UN NIVEL
        printf("\n**Optimización valores paralelos CALCULO DE FITNESS VS**\n");
        total_hilos_n1 = omp_get_num_procs();
        while (total_hilos_n1 > 1)
        {
                tiempo_i = wtime();
                omp_set_num_threads(total_hilos_n1);
                
		forces_CPU_vs_warm_up (vectors_o->files,param,proteina.atoms,ligand_params,vectors_o->nlig,proteina.rec_x,proteina.rec_y,proteina.rec_z,vectors_o->conformations_x,vectors_o->conformations_y,vectors_o->conformations_z,vectors_o->move_x,vectors_o->move_y,vectors_o->move_z,vectors_o->quat_x,vectors_o->quat_y,vectors_o->quat_z,vectors_o->quat_w,proteina.rectype,vectors_o->ligtype,vectors_o->ql,proteina.qr,proteina.bonds,vectors_o->bonds,vectors_o->energy.energy,vectors_o->energy.n_conformation,weights,f_params,vectors_o->nconformations,total_hilos_n1);
                tiempo_f = wtime() - tiempo_i;

                if (tiempo_f < tiempo_act_hilos_n1)
                {
                        tiempo_act_hilos_n1 = tiempo_f;
                        hilo1_n1 = total_hilos_n1;
                }
                total_hilos_n1--;
                printf("Hilos N1: %d \tTiempo: %f\n",total_hilos_n1+1,tiempo_f);

        }
        printf("MEJOR Hilos N1: %d tiempo mejor: %f\n",hilo1_n1,tiempo_act_hilos_n1);
	optimizar_parametros_fit (metaheuristic,hilo1_n1,tiempo_act_hilos_n1,hilo1_n1,1,tiempo_act_hilos_n1_n2);
	
	twarm_up_t = wtime() - twarm_up;
        printf("\n\t*****************\n");
        printf("\tWARM_UP VS: %f seg\n",twarm_up_t);
        printf("\t*****************\n");
        final_parametros_paralelos (metaheuristic);
        escribir_openmp(param,metaheuristic);

	free(vectors_o->conformations_x);
	free(vectors_o->conformations_y);
	free(vectors_o->conformations_z);
	free(vectors_o->ql);
	free(vectors_o->ligtype);
	free(vectors_o->subtype);
	free(vectors_o->bonds);
	free(vectors_o->nbonds);
	free(vectors_o->move_x);
        free(vectors_o->move_y);
        free(vectors_o->move_z);
        free(vectors_o->quat_x);
        free(vectors_o->quat_y);
        free(vectors_o->quat_z);
        free(vectors_o->quat_w);
        free(vectors_o->energy.energy);
        free(vectors_o->energy.n_conformation);

	
}

