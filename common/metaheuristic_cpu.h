#ifndef __METAHEURISTIC_CPU_H
#define __METAHEURISTIC_CPU_H

#include "energy_struct.h"
#include "vector_types.h"

extern void warm_up_openmp (struct ligand_t ligando, struct receptor_t proteina,  struct vectors_t *vectors_e, struct vectors_t *vectors_o,unsigned int surface, struct force_field_param_t *f_params, type_data *weights, unsigned int nconformations_pc, struct param_t param, struct metaheuristic_t *metaheuristic);

extern void warm_up_openmp_vs (type_data *c_x, type_data *c_y, type_data *c_z, struct ligand_params_t *ligand_params, type_data *ql, char *ligtype, char *subtype, unsigned int *bonds, char *nbonds, struct receptor_t proteina,  struct vectors_t *vectors_e, struct vectors_t *vectors_o, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations_pf, struct param_t param, struct metaheuristic_t *metaheuristic);

extern void optimizar_parametros_fit (struct metaheuristic_t *metaheuristic,unsigned int hilo1_n1, double tiempo_act_hilos_n1, unsigned int hilos1_n1_n2, unsigned int hilos2_n1_n2, double tiempo_act_hilos_n1_n2);
extern void optimizar_parametros_comb (struct metaheuristic_t *metaheuristic,unsigned int hilo1_n1, double tiempo_act_hilos_n1, unsigned int hilos1_n1_n2, unsigned int hilos2_n1_n2, double tiempo_act_hilos_n1_n2);
extern void optimizar_parametros_ini (struct metaheuristic_t *metaheuristic,unsigned int hilo1_n1);
extern void optimizar_parametros_sel (struct metaheuristic_t *metaheuristic,unsigned int hilo1_n1);
extern void optimizar_parametros_inc (struct metaheuristic_t *metaheuristic,unsigned int hilo1_n1);
extern void optimizar_parametros_imp (struct metaheuristic_t *metaheuristic,unsigned int hilo1_n1);
extern void final_parametros_paralelos (struct metaheuristic_t *metaheuristic);
extern void escribir_openmp (struct param_t param, struct metaheuristic_t *metaheuristic);
#endif
