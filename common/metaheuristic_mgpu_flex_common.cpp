#include "definitions.h"
#include "wtime.h"
#include "energy_common.h"
#include "energy_struct.h"
#include "energy_GpuSolver.h"
#include "energy_positions.h"
#include "energy_positions_common.h"
#include "energy_positions_flex.h"
#include "energy_montecarlo.h"
#include "metaheuristic_mgpu_common.h"
#include "metaheuristic_mgpu_flex_common.h"
#include "metaheuristic_mgpu_flex.h"
#include <thrust/sequence.h>
#include <thrust/sort.h>
#include <omp.h>

using namespace std;


extern void multigpuSolver_warm_up_flex (struct ligand_t ligando, struct receptor_t proteina, type_data *conf_x, type_data *conf_y, type_data *conf_z,type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, struct energy_t energy, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, double *tiempos, unsigned int stride, unsigned int n_gpus, struct flexibility_params_t *flexibility_params,struct flexibility_data_t *flexibility_conformations)
{
	cudaError_t cudaStatus;
	type_data kernels = 0;
	int i = 0;	
	
	tiempos = (double *)malloc(sizeof(double)*n_gpus);
	printf("\n*** FASE REPARTO DE CARGA ***\n\n");
	omp_set_num_threads(n_gpus);
	#pragma omp parallel for	
	for (unsigned int j=0;j<n_gpus;j++)
	{
	
		double tiempo_i = wtime();
		//int stride_d = th * stride;
		unsigned int stride_d = j * stride;
		type_data local_k = 0;
		gpuSolver_flex (ligando,proteina,param,metaheuristic,devices,conf_x+stride_d,conf_y+stride_d,conf_z+stride_d,move_x+stride_d,move_y+stride_d,move_z+stride_d,quat_x+stride_d,quat_y+stride_d,quat_z+stride_d,quat_w+stride_d,energy.energy+stride_d,energy.n_conformation+stride_d,weights,f_params,flexibility_params->n_links,nconformations,j,stride_d,flexibility_params->individual_bonds_VDW,flexibility_params->individual_bonds_ES,&local_k);
		double tiempo_f = wtime() - tiempo_i;
		//tiempos[j] = tiempo_f;
		tiempos[j] = local_k;		
	}
	for (unsigned int k = 0;k<n_gpus;k++) devices->tiempo[k]=tiempos[k];
	for (unsigned int k = 0;k<n_gpus;k++) printf("\tDevice %d Modelo: %s Tiempo GPU: %f seg\n",devices->id[k],devices->propiedades[k].name,devices->tiempo[k]);
	
}

extern void warm_up_devices_flex (struct ligand_t ligando, struct receptor_t proteina, struct vectors_t *vectors_e, struct device_t *devices,type_data *weights, struct force_field_param_t *f_params, struct param_t param, struct metaheuristic_t metaheuristic, unsigned int nconformations,unsigned int *number_devices, struct flexibility_params_t *flexibility_params,struct flexibility_data_t *flexibility_conformations)
{
	int *device_current = new int;
	unsigned int k,*count,*tipos,d,e,t,opc,tmp=0,temp,error,reparto;
	cudaDeviceProp deviceProp;
	int n_devices;
	struct device_t devices_tmp;
	struct vectors_t vectors_o;
	double *tiempos,*trabajo_d,tmp_d;
	char str[2];
	FILE *fp;
		
	
	cudaDeviceProp *prop = new cudaDeviceProp();	
	cudaSetDevice(param.ngpu);
	cudaGetDevice(device_current);
	cudaGetDeviceProperties (prop, *device_current);
	printf("\n%s\n",prop->name);
	
	cudaGetDeviceCount(&n_devices);
	//n_devices = n_devices - 4;	
	vectors_e->nconformations = n_devices * nconformations;
	vectors_e->conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->move_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->move_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->move_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_e->nconformations);
	vectors_e->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);
	 						 
	count = (unsigned int *)calloc(sizeof(unsigned int),n_devices);
	tipos = (unsigned int *)calloc(sizeof(unsigned int),n_devices);		
	devices_tmp.id = (unsigned int *)malloc(sizeof(unsigned int) * n_devices);
	devices_tmp.propiedades = (cudaDeviceProp *)malloc (sizeof(cudaDeviceProp) * n_devices);
	devices_tmp.trabajo = (unsigned int *)malloc (sizeof(unsigned int) * n_devices);
	devices_tmp.tiempo = (double *)malloc (sizeof(double) * n_devices);
	devices_tmp.hilos = (unsigned int *)malloc(sizeof(unsigned int) * n_devices*metaheuristic.num_kernels);
	
	for (unsigned int d=0;d<n_devices;d++)
	{
		cudaGetDeviceProperties(&deviceProp,d);		
		devices_tmp.id[d] = d;		
		devices_tmp.propiedades[d] = deviceProp;
		devices_tmp.hilos[d*metaheuristic.num_kernels] = metaheuristic.ThreadsperblockFit;
		devices_tmp.hilos[d*metaheuristic.num_kernels+1] = metaheuristic.ThreadsperblockMoveIni;
		devices_tmp.hilos[d*metaheuristic.num_kernels+2] = metaheuristic.ThreadsperblockRotIni;
		devices_tmp.hilos[d*metaheuristic.num_kernels+3] = metaheuristic.ThreadsperblockQuatIni;
		devices_tmp.hilos[d*metaheuristic.num_kernels+4] = metaheuristic.ThreadsperblockRandomIni;
		devices_tmp.hilos[d*metaheuristic.num_kernels+5] = metaheuristic.ThreadsperblockMoveImp;
		devices_tmp.hilos[d*metaheuristic.num_kernels+6] = metaheuristic.ThreadsperblockIncImp;
		printf("Device %d name %s\n",devices_tmp.id[d],devices_tmp.propiedades[d].name);
	}
	//exit(0);
	generate_positions (param,vectors_e,metaheuristic,&devices_tmp,0,0);
	//exit(0);
	//TIPO DE COMPUTACION
	switch (param.tipo_computo) {
		case 0: //HOMOGENEA
			for (d=0;d<n_devices;d++)
			{
				for (e=0;e<n_devices;e++)
				{
					if (strcmp(devices_tmp.propiedades[d].name,devices_tmp.propiedades[e].name) == 0)
					count[d]++;
				}
			}
			t = n_devices;
			d = 0;
			while (t > 0)
			{
				tipos[d] = count[d];
				t -= tipos[d];
				d++;
			}
			
			printf("\n** GPUs disponibles **");
			for (t = 0; t < d;t++)
				printf("\n\t%d. %s (DISPOSITIVOS: %d)\n",t,devices_tmp.propiedades[t].name,count[t]);
			printf("\nTipo de dispositivo para computar: ");
			//scanf("%d",&opc);
			opc = param.tipo_dispositivo_computar;
			// el tipo del dispositivo es opc			
			//devices->n_devices = count[opc];
			devices->id = (unsigned int *)malloc (sizeof(unsigned int) * count[opc]);
			devices->propiedades = (cudaDeviceProp *)malloc (sizeof(cudaDeviceProp) * count[opc]);
			devices->trabajo = (unsigned int *)malloc (sizeof(unsigned int) * count[opc]);
			devices->tiempo = (double *)malloc (sizeof(double) * count[opc]);
			devices->stride = (unsigned int *)malloc (sizeof(unsigned int) * count[opc]);
			devices->conformaciones = (unsigned int *)malloc (sizeof(unsigned int) * count[opc]);
			devices->hilos = (unsigned int *)malloc(sizeof(unsigned int) * count[opc]*metaheuristic.num_kernels);
			e = 0;
			for (d=0;d<n_devices;d++)
			{
				if (strcmp(devices_tmp.propiedades[d].name,devices_tmp.propiedades[opc].name) == 0)
				{
					devices->id[e] = d;
					devices->propiedades[e] = devices_tmp.propiedades[d];	
					devices->hilos[e*metaheuristic.num_kernels] = metaheuristic.ThreadsperblockFit;
					devices->hilos[e*metaheuristic.num_kernels+1] = metaheuristic.ThreadsperblockMoveIni;
					devices->hilos[e*metaheuristic.num_kernels+2] = metaheuristic.ThreadsperblockRotIni;
					devices->hilos[e*metaheuristic.num_kernels+3] = metaheuristic.ThreadsperblockQuatIni;
					devices->hilos[e*metaheuristic.num_kernels+4] = metaheuristic.ThreadsperblockRandomIni;
					devices->hilos[e*metaheuristic.num_kernels+5] = metaheuristic.ThreadsperblockMoveImp;
					devices->hilos[e*metaheuristic.num_kernels+6] = metaheuristic.ThreadsperblockIncImp;					
					e++;
				}
			}
			*number_devices = tipos[opc];
			devices->n_devices = tipos[opc];			
			printf("\n** SE VA A COMPUTAR CON LOS DISPOSITIVOS **\n");
			error = 0;	
			sprintf(str,"%d",opc);				
			strcat(param.multigpu,"_");
			strcat(param.multigpu,str);
			strcat(param.multigpu,"\0");			
			if ((param.optimizacion) && ((fp = fopen(param.multigpu,"r")) == NULL))
			{
				copy_surface(vectors_e,&vectors_o);
				warm_up_bloques_gpu_flex (ligando,proteina,&vectors_o,vectors_e->weights,vectors_e->f_params,param,metaheuristic,devices,0,flexibility_params,flexibility_conformations);
				limpiar_surface(&vectors_o);
				escribir_multigpu (param,&metaheuristic,devices,opc);
				//printf("Threadsperblock1Ini: %d\n",metaheuristic.Threadsperblock1Ini);
				devices->trabajo[0] = floor(100/(*number_devices));
				tmp = devices->trabajo[0];
				printf("Device %d, modelo %s\n",devices->id[0],devices->propiedades[0].name);
				for (d=1;d<(*number_devices);d++)
				{	
					printf("Device %d, modelo %s\n",devices->id[d],devices->propiedades[d].name);
					devices->hilos[d*metaheuristic.num_kernels] = devices->hilos[metaheuristic.num_kernels];
					devices->hilos[d*metaheuristic.num_kernels+1] = devices->hilos[metaheuristic.num_kernels+1];
					devices->hilos[d*metaheuristic.num_kernels+2] = devices->hilos[metaheuristic.num_kernels+2];
					devices->hilos[d*metaheuristic.num_kernels+3] = devices->hilos[metaheuristic.num_kernels+3];
					devices->hilos[d*metaheuristic.num_kernels+4] = devices->hilos[metaheuristic.num_kernels+4];
					devices->hilos[d*metaheuristic.num_kernels+5] = devices->hilos[metaheuristic.num_kernels+5];
					devices->hilos[d*metaheuristic.num_kernels+6] = devices->hilos[metaheuristic.num_kernels+6];
					devices->trabajo[d] = floor(100/(*number_devices));	
					tmp += devices->trabajo[d];	
				}
				
			}
			else
			{
				if (param.optimizacion)
				{
					readmultiGpuOptimizationFile(param.multigpu,0,param.tipo_dispositivo_computar,metaheuristic,devices,&error);
					if (error)
					{
						copy_surface(vectors_e,&vectors_o);
						warm_up_bloques_gpu_flex (ligando,proteina,&vectors_o,vectors_e->weights,vectors_e->f_params,param,metaheuristic,devices,0,flexibility_params,flexibility_conformations);
						limpiar_surface(&vectors_o);
						//escribir_multigpu (param,&metaheuristic,devices,opc);
						for (d=1;d<(*number_devices);d++)
						{								
							devices->hilos[d*metaheuristic.num_kernels] = devices->hilos[metaheuristic.num_kernels];
							devices->hilos[d*metaheuristic.num_kernels+1] = devices->hilos[metaheuristic.num_kernels+1];
							devices->hilos[d*metaheuristic.num_kernels+2] = devices->hilos[metaheuristic.num_kernels+2];
							devices->hilos[d*metaheuristic.num_kernels+3] = devices->hilos[metaheuristic.num_kernels+3];
							devices->hilos[d*metaheuristic.num_kernels+4] = devices->hilos[metaheuristic.num_kernels+4];
							devices->hilos[d*metaheuristic.num_kernels+5] = devices->hilos[metaheuristic.num_kernels+5];
							devices->hilos[d*metaheuristic.num_kernels+6] = devices->hilos[metaheuristic.num_kernels+6];							
						}						
					}
					devices->trabajo[0] = floor(100/(*number_devices));
					tmp = devices->trabajo[0];
					printf("Device %d, modelo %s\n",devices->id[0],devices->propiedades[0].name);
					for (d=1;d<(*number_devices);d++)
					{	
						printf("Device %d, modelo %s\n",devices->id[d],devices->propiedades[d].name);
						devices->trabajo[d] = floor(100/(*number_devices));	
						tmp += devices->trabajo[d];	
					}
					devices->trabajo[0] += (100 - tmp);		
				}
				else
				{
					devices->trabajo[0] = floor(100/(*number_devices));
					tmp = devices->trabajo[0];
					printf("Device %d, modelo %s\n",devices->id[0],devices->propiedades[0].name);
					for (d=1;d<(*number_devices);d++)
					{	
						printf("Device %d, modelo %s\n",devices->id[d],devices->propiedades[d].name);
						devices->trabajo[d] = floor(100/(*number_devices));	
						tmp += devices->trabajo[d];	
					}
					devices->trabajo[0] += (100 - tmp);		
				}
			}
			break;	
		case 1: //HETEROGENEA
			trabajo_d = (double *)malloc (sizeof(double) * n_devices);
			devices->id = (unsigned int *)malloc (sizeof(unsigned int) * n_devices);
			devices->propiedades = (cudaDeviceProp *)malloc (sizeof(cudaDeviceProp) * n_devices);
			devices->trabajo = (unsigned int *)malloc (sizeof(unsigned int) * n_devices);
			devices->conformaciones = (unsigned int *)malloc(sizeof(unsigned int) * n_devices);
			devices->tiempo = (double *)malloc (sizeof(double) * n_devices);
			devices->hilos = (unsigned int *)malloc(sizeof(unsigned int) * n_devices * metaheuristic.num_kernels);
			devices->stride = (unsigned int *)malloc (sizeof(unsigned int) * n_devices);
			tiempos = (double *)malloc(sizeof(double) * n_devices);			
			devices->n_devices = n_devices;
			for (unsigned int d=0;d<n_devices;d++)
			{
				devices->id[d] = devices_tmp.id[d];		
				devices->propiedades[d] = devices_tmp.propiedades[d];
				devices->hilos[d*metaheuristic.num_kernels] = metaheuristic.ThreadsperblockFit;
				devices->hilos[d*metaheuristic.num_kernels+1] = metaheuristic.ThreadsperblockMoveIni;
				devices->hilos[d*metaheuristic.num_kernels+2] = metaheuristic.ThreadsperblockRotIni;
				devices->hilos[d*metaheuristic.num_kernels+3] = metaheuristic.ThreadsperblockQuatIni;
				devices->hilos[d*metaheuristic.num_kernels+4] = metaheuristic.ThreadsperblockRandomIni;
				devices->hilos[d*metaheuristic.num_kernels+5] = metaheuristic.ThreadsperblockMoveImp;
				devices->hilos[d*metaheuristic.num_kernels+6] = metaheuristic.ThreadsperblockIncImp;
				//printf("Device %d es %s tiene computabilidad %d y %d Mb de memoria device\n",devices->id[d],devices->propiedades[d].name,devices->propiedades[d].major,devices->propiedades[d].totalGlobalMem/1024);
			}
			//exit(0);
			if (param.reparto_carga)				
			{
				type_data *w_t = (type_data *)malloc(sizeof(type_data)*devices->n_devices);
				type_data *w_t_1 = (type_data *)calloc(sizeof(type_data),devices->n_devices);
				type_data ttotal_1;

				multigpuSolver_warm_up_flex (ligando,proteina,vectors_e->conformations_x,vectors_e->conformations_y,vectors_e->conformations_z,vectors_e->move_x,vectors_e->move_y,vectors_e->move_z,vectors_e->quat_x,vectors_e->quat_y,vectors_e->quat_z,vectors_e->quat_w,vectors_e->energy,weights,f_params,nconformations,param,metaheuristic,devices,tiempos,vectors_e->nconformations/n_devices,n_devices,flexibility_params,flexibility_conformations);		
				//exit(0);	
				*number_devices = n_devices;
				devices->n_devices = n_devices;
				double ttotal = 0;
				unsigned int ttrabajo = 0;
				
				//thrust::stable_sort_by_key(devices->tiempo,devices->tiempo + n_devices, devices->id, thrust::less<float>());
				thrust::stable_sort_by_key(devices->tiempo,devices->tiempo + n_devices, devices->id, thrust::less<type_data>());
				for (int s=0;s<n_devices;s++) printf("id %d %f\n",devices->id[s],devices->tiempo[s]);
				w_t[0]=1;
                                for (k=1; k < n_devices;k++)
                                {
                                        tmp = devices->tiempo[0]/devices->tiempo[k];
                                        //printf("tmp %f\n",tmp);
                                        w_t[k] = tmp;
                                }
                                for (k=0; k < n_devices;k++) ttotal += w_t[k];

                                tmp = 100 / ttotal;

                                for (k = 0;k < n_devices;k++) w_t[k]*=tmp;

                                ttotal = 0;
                                for (k=0; k < n_devices;k++)
                                {
                                        devices->trabajo[k] = (int)w_t[k];
                                        ttotal += devices->trabajo[k];
                                }

				printf("\n");
				for (unsigned int s=0;s<n_devices;s++) printf("Device %d Carga de trabajo %d \n",devices->id[s],devices->trabajo[s]);					
				free(w_t);
				free(w_t_1);
			}
			else
			{
				printf("\n*** FASE REPARTO DE CARGA ***\n\n");	
				*number_devices = n_devices;
				devices->n_devices = n_devices;				
				reparto = (unsigned int)(100 / n_devices);				
				for (d=0;d<n_devices;d++) 
					devices->trabajo[d] = reparto;
				reparto = 100 % n_devices;
				for (d=0;d<reparto;d++)	devices->trabajo[d]++;
				for (d=0;d<n_devices;d++) printf("Device %d modelo: %s Carga de trabajo %d\n",devices->id[d],devices->propiedades[d].name,devices->trabajo[d]);
				//printf("Reparto equitativo\n");
			}
			if (param.optimizacion)
			{
				strcat(param.multigpu,"_h");	
				strcat(param.multigpu,"\0");					
				if ((fp = fopen(param.multigpu,"r")) == NULL)
				{
					copy_surface(vectors_e,&vectors_o);
					for (d=0;d<n_devices;d++)
						warm_up_bloques_gpu_flex (ligando,proteina,&vectors_o,vectors_e->weights,vectors_e->f_params,param,metaheuristic,devices,d,flexibility_params,flexibility_conformations);			
					limpiar_surface(&vectors_o);
					//escribir_multigpu (param,&metaheuristic,devices,1);
				}
				else
				{
					readmultiGpuOptimizationFile(param.multigpu,1,0,metaheuristic,devices,&error);
					if (error)
					{
						copy_surface(vectors_e,&vectors_o);
						for (d=0;d<n_devices;d++)
							warm_up_bloques_gpu_flex (ligando,proteina,&vectors_o,vectors_e->weights,vectors_e->f_params,param,metaheuristic,devices,d,flexibility_params,flexibility_conformations);			
						limpiar_surface(&vectors_o);
						//escribir_multigpu (param,&metaheuristic,devices,1);
					}
				}	
			}
			break;
	}
	free(vectors_e->conformations_x);
	free(vectors_e->conformations_y);
	free(vectors_e->conformations_z);
	free(vectors_e->move_x);
	free(vectors_e->move_y);
	free(vectors_e->move_z);
	free(vectors_e->quat_x);
	free(vectors_e->quat_y);
	free(vectors_e->quat_z);
	free(vectors_e->quat_w);
	free(vectors_e->energy.energy);
	free(vectors_e->energy.n_conformation);	
}

extern void warm_up_bloques_gpu_flex (struct ligand_t ligando, struct receptor_t proteina, struct vectors_t *vectors_e, type_data *weights, struct force_field_param_t *f_params,  struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int orden_device, struct flexibility_params_t *flexibility_params, struct flexibility_data_t *flexibility_conformations)

{
	int *device_current = new int;	
	cudaDeviceProp *prop = new cudaDeviceProp();
	double tiempo_i,tiempo_f, twarm_up, twarm_up_t;
	double tiempo = 1000.0;
	unsigned int valor_act;
	type_data kernels = 0;
	vectors_e->nconformations = param.conf_warm_up_gpu;
	vectors_e->conformations_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->conformations_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->conformations_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	
	vectors_e->move_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->move_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->move_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->quat_x = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->quat_y = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->quat_z = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->quat_w = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	vectors_e->energy.n_conformation = (unsigned int *)malloc(sizeof(unsigned int)*vectors_e->nconformations);
	vectors_e->energy.energy = (type_data *)malloc(sizeof(type_data)*vectors_e->nconformations);
	thrust::sequence(vectors_e->energy.n_conformation,vectors_e->energy.n_conformation + vectors_e->nconformations);	
	generate_positions (param,vectors_e,metaheuristic,devices,orden_device,1);
	//for (int i=0;i<vectors_e->nconformations;i++)printf("move_s[%d] = %f\n",i,vectors_e->move_x[i]);
	//exit(0);	
	
	cudaSetDevice(devices->id[orden_device]);
	cudaGetDevice(device_current);
	cudaGetDeviceProperties (prop, *device_current);
	printf("\n%s\n",prop->name);
	twarm_up = wtime();
	for (unsigned int i=64;i<=param.limit;i+=64)
	{
		tiempo_i = wtime();
		cambia_param_blocks(devices,orden_device,metaheuristic.num_kernels,0,i);
		
		gpuSolver_flex (ligando,proteina,param,metaheuristic,devices,vectors_e->conformations_x,vectors_e->conformations_y,vectors_e->conformations_z,vectors_e->move_x,vectors_e->move_y,vectors_e->move_z,vectors_e->quat_x,vectors_e->quat_y,vectors_e->quat_z,vectors_e->quat_w,vectors_e->energy.energy,vectors_e->energy.n_conformation,weights,f_params,flexibility_params->n_links,param.conf_warm_up_gpu,orden_device,0,flexibility_params->individual_bonds_VDW,flexibility_params->individual_bonds_ES,&kernels);
		//exit(0);				
		tiempo_f = wtime() - tiempo_i;
		if (tiempo_f < tiempo) 
		{	
			valor_act = i;
			tiempo = tiempo_f;
		}
		printf("Tiempo %f para tamaño de bloque CON %d\n",tiempo_f,i);
		//exit(0);
	}
	cambia_param_blocks(devices,orden_device,metaheuristic.num_kernels,0,valor_act);	
	printf("Valor Optimizado HILOS/BLOQUE en Cálculo de FITNESS: %d\n",valor_act);
	//exit(0);		
	for (unsigned int i=64;i<=param.limit;i+=64)
	{		
		//tiempo_i = wtime();
		cambia_param_blocks(devices,orden_device,metaheuristic.num_kernels,1,i);	
		generate_positions_move_warm_up(param,vectors_e,metaheuristic,devices,orden_device,&tiempo_f);
		//tiempo_f = wtime() - tiempo_i;
		if (tiempo_f < tiempo) 
		{
			valor_act = i;
			tiempo = tiempo_f;
		}
		printf("Tiempo %f para tamaño de bloque CON %d\n",tiempo_f,i);
	}
	cambia_param_blocks(devices,orden_device,metaheuristic.num_kernels,1,valor_act);	
	printf("Valor Optimizado HILOS/BLOQUE en MOVE en Inicializar: %d\n",valor_act);
	//exit(0);
	for (unsigned int i=64;i<=param.limit;i+=64)
	{
		//tiempo_i = wtime();
		cambia_param_blocks(devices,orden_device,metaheuristic.num_kernels,2,i);	
		generate_positions_rot_warm_up(param,vectors_e,metaheuristic,devices,orden_device,&tiempo_f);
		//tiempo_f = wtime() - tiempo_i;
		if (tiempo_f < tiempo) 
		{
			valor_act = i;
			tiempo = tiempo_f;
		}
		printf("Tiempo %f para tamaño de bloque CON %d\n",tiempo_f,i);
	}
	cambia_param_blocks(devices,orden_device,metaheuristic.num_kernels,2,valor_act);	
	printf("Valor Optimizado HILOS/BLOQUE en ROTACION en Inicializar: %d\n",valor_act);
	//exit(0)
	for (unsigned int i=64;i<=param.limit;i+=64)
	{		
		//tiempo_i = wtime();
		cambia_param_blocks(devices,orden_device,metaheuristic.num_kernels,3,i);	
		generate_positions_quat_warm_up(param,vectors_e,metaheuristic,devices,orden_device,&tiempo_f);
		//tiempo_f = wtime() - tiempo_i;
		if (tiempo_f < tiempo) 
		{
			valor_act = i;
			tiempo = tiempo_f;
		}
		printf("Tiempo %f para tamaño de bloque CON %d\n",tiempo_f,i);
	}
	cambia_param_blocks(devices,orden_device,metaheuristic.num_kernels,3,valor_act);	
	printf("Valor Optimizado HILOS/BLOQUE en QUAT en Inicializar: %d\n",valor_act);
	//exit(0);
	for (unsigned int i=64;i<=param.limit;i+=64)
	{		
		//tiempo_i = wtime();
		cambia_param_blocks(devices,orden_device,metaheuristic.num_kernels,4,i);	
		generate_positions_rand_warm_up(param,vectors_e,metaheuristic,devices,orden_device,&tiempo_f);
		//tiempo_f = wtime() - tiempo_i;
		if (tiempo_f < tiempo) 
		{
			valor_act = i;
			tiempo = tiempo_f;
		}
		printf("Tiempo %f para tamaño de bloque CON %d\n",tiempo_f,i);
	}
	cambia_param_blocks(devices,orden_device,metaheuristic.num_kernels,4,valor_act);	
	printf("Valor Optimizado HILOS/BLOQUE en CURAND en Inicializar: %d\n",valor_act);
	//exit(0);
	for (unsigned int i=64;i<=param.limit;i+=64)
	{		
		//tiempo_i = wtime();
		cambia_param_blocks(devices,orden_device,metaheuristic.num_kernels,5,i);	
		generate_positions_move_mejora_warm_up(param,vectors_e,metaheuristic,devices,orden_device,&tiempo_f);
		//tiempo_f = wtime() - tiempo_i;
		if (tiempo_f < tiempo) 
		{
			valor_act = i;
			tiempo = tiempo_f;
		}
		printf("Tiempo %f para tamaño de bloque CON %d\n",tiempo_f,i);
	}
	cambia_param_blocks(devices,orden_device,metaheuristic.num_kernels,5,valor_act);	
	printf("Valor Optimizado HILOS/BLOQUE en MOVE en Mejorar: %d\n",valor_act);
	//exit(0);
	for (unsigned int i=64;i<=param.limit;i+=64)
	{		
		//tiempo_i = wtime();
		cambia_param_blocks(devices,orden_device,metaheuristic.num_kernels,6,i);			
		generate_positions_incl_mejora_warm_up (proteina,ligando,param,vectors_e,metaheuristic,devices,orden_device,&tiempo_f);
		//tiempo_f = wtime() - tiempo_i;
		if (tiempo_f < tiempo) 
		{
			valor_act = i;
			tiempo = tiempo_f;
		}
		printf("Tiempo %f para tamaño de bloque CON %d\n",tiempo_f,i);
	}
	cambia_param_blocks(devices,orden_device,metaheuristic.num_kernels,6,valor_act);	
	printf("Valor Optimizado HILOS/BLOQUE en INCLUIR en Mejorar: %d\n",valor_act);
	//exit(0);
	
	twarm_up_t = wtime() - twarm_up;
	printf("\n\t*****************\n");
	printf("\tWARM_UP: %f seg\n",twarm_up_t);
	printf("\t*****************\n");
	final_parametros_paralelos_gpu (devices,metaheuristic.num_kernels);
	
	//LIBERA MEMORIA
	free(vectors_e->conformations_x);
	free(vectors_e->conformations_y);
	free(vectors_e->conformations_z);
	free(vectors_e->move_x);
	free(vectors_e->move_y);
	free(vectors_e->move_z);
	free(vectors_e->quat_x);
	free(vectors_e->quat_y);
	free(vectors_e->quat_z);
	free(vectors_e->quat_w);
	free(vectors_e->energy.energy);
	free(vectors_e->energy.n_conformation);
}
