#ifndef __METAHEU_COMB_VS_COMMON_H__
#define __METAHEU_COMB_VS_COMMON_H__

extern void combinar_vs_warm_up (struct receptor_t proteina, struct ligand_params_t *ligand_params, struct vectors_t *vectors_e, struct param_t param, unsigned int n1);
extern void combinar_individual_vs_warm_up (unsigned int nlig, unsigned int stride_e, unsigned int stride_s, type_data *e_move_x, type_data *e_move_y, type_data *e_move_z, type_data *e_quat_x, type_data *e_quat_y, type_data *e_quat_z, type_data *e_quat_w, type_data *s_move_x, type_data *s_move_y, type_data *s_move_z, type_data *s_quat_x, type_data *s_quat_y, type_data *s_quat_z, type_data *s_quat_w , struct param_t param, unsigned int i, unsigned int j, unsigned int k, unsigned int l);
extern unsigned int getRandomNumber_modified_vs_warm (unsigned int n, unsigned int act);

#endif
