#ifndef __METAHEU_COMB_COMMON_H__
#define __METAHEU_COMB_COMMON_H__

extern void combinar_warm_up (struct receptor_t proteina, struct ligand_t ligando, struct vectors_t *vectors_e, struct param_t param,unsigned int n1, unsigned int n2);
void combinar_individual_warm_up (struct vectors_t *vectors_e, struct vectors_t *vectors_s, struct param_t param, unsigned int stride_e, unsigned int stride_s, unsigned int i, unsigned int j, unsigned int k, unsigned int l);
extern unsigned int getRandomNumber_modified_warm (unsigned int n, unsigned int act);

#endif
