#include "energy_common.h"
#include "definitions.h"
#include "energy_kernel.h"
#include "energy_common-gpu.h"
#include "energy_moving.h"
#include "metaheuristic_inc.h"
#include "wtime.h"

extern void genera_gpu(struct device_t *devices,unsigned int device,struct metaheuristic_t metaheuristic)
{
	cudaDeviceProp deviceProp,*prop;
	int *device_current;
	cudaSetDevice(device);
	device_current = new int;
	prop = new cudaDeviceProp();
	cudaGetDevice(device_current);
    	cudaGetDeviceProperties (prop, *device_current);
    	printf("\n%d %s\n",*device_current,prop->name);
	cudaGetDeviceProperties(&deviceProp,device);	
	devices->n_devices = 1;
	devices->id = (unsigned int *)malloc (sizeof(unsigned int));
	devices->propiedades = (cudaDeviceProp *)malloc (sizeof(cudaDeviceProp));
	devices->trabajo = (unsigned int *)malloc (sizeof(unsigned int));
	devices->stride = (unsigned int *)malloc (sizeof(unsigned int));
	devices->tiempo = (double *)malloc (sizeof(double));
	devices->hilos_shared = (unsigned int *)malloc (sizeof(unsigned int));
	devices->hilos_sinshared = (unsigned int *)malloc (sizeof(unsigned int));
	devices->hilos = (unsigned int *)malloc (sizeof(unsigned int) * metaheuristic.num_kernels);
	devices->id[0] = device;
	devices->propiedades[0] = deviceProp;
	devices->stride[0] = 0;
	devices->hilos[0] = metaheuristic.ThreadsperblockFit;
	devices->hilos[1] = metaheuristic.ThreadsperblockMoveIni;
	devices->hilos[2] = metaheuristic.ThreadsperblockRotIni;
	devices->hilos[3] = metaheuristic.ThreadsperblockQuatIni;
	devices->hilos[4] = metaheuristic.ThreadsperblockRandomIni;
	devices->hilos[5] = metaheuristic.ThreadsperblockMoveImp;
	devices->hilos[6] = metaheuristic.ThreadsperblockIncImp;	
	//devices->hilos_sinshared[0] = metaheuristic.Threadsperblock1Ini;
	//devices->hilos_shared[0] = metaheuristic.Threadsperblock2Ini;
	
}

__global__ void fill_conformations_mejora_flex (int NEEImp,unsigned int nlig, type_data *conformations_x_d_mejora, type_data *conformations_y_d_mejora, type_data *conformations_z_d_mejora, type_data *moves_x_d_mejora, type_data *moves_y_d_mejora, type_data *moves_z_d_mejora, type_data *quat_d_x_mejora, type_data *quat_d_y_mejora, type_data *quat_d_z_mejora, type_data *quat_d_w_mejora, type_data *energy_d_mejora, type_data *conformations_x_d, type_data *conformations_y_d, type_data *conformations_z_d, type_data *moves_x_d, type_data *moves_y_d, type_data *moves_z_d, type_data *quat_d_x, type_data *quat_d_y, type_data *quat_d_z, type_data *quat_d_w, type_data *energy_d, unsigned int max)
{

	unsigned int id_conformation = blockIdx.x*blockDim.x + threadIdx.x;
        unsigned int id_conformation_begin = id_conformation * NEEImp;
        if (id_conformation < max)
        {
                for (int j=0; j < NEEImp; j++)
                {
			for (int i=0;i<nlig;i++) { conformations_x_d_mejora[id_conformation_begin*nlig + i] = conformations_x_d[id_conformation*nlig + i]; conformations_y_d_mejora[id_conformation_begin*nlig + i] = conformations_y_d[id_conformation*nlig + i]; conformations_z_d_mejora[id_conformation_begin*nlig + i] = conformations_z_d[id_conformation*nlig + i]; }
                        moves_x_d_mejora[id_conformation_begin + j] = moves_x_d[id_conformation];
                        moves_y_d_mejora[id_conformation_begin + j] = moves_y_d[id_conformation];
                        moves_z_d_mejora[id_conformation_begin + j] = moves_z_d[id_conformation];
                        quat_d_x_mejora[id_conformation_begin + j] = quat_d_x[id_conformation];
                        quat_d_y_mejora[id_conformation_begin + j] = quat_d_y[id_conformation];
                        quat_d_z_mejora[id_conformation_begin + j] = quat_d_z[id_conformation];
                        quat_d_w_mejora[id_conformation_begin + j] = quat_d_w[id_conformation];
                        energy_d_mejora[id_conformation_begin + j] = energy_d[id_conformation];
                }
        }
}
__global__ void fill_conformations_mejora  (int NEEImp, type_data *moves_x_d_mejora, type_data *moves_y_d_mejora, type_data *moves_z_d_mejora, type_data *quat_d_x_mejora, type_data *quat_d_y_mejora, type_data *quat_d_z_mejora, type_data *quat_d_w_mejora, type_data *energy_d_mejora, type_data *moves_x_d, type_data *moves_y_d, type_data *moves_z_d, type_data *quat_d_x, type_data *quat_d_y, type_data *quat_d_z, type_data *quat_d_w, type_data *energy_d, unsigned int max)

{
	unsigned int id_conformation = blockIdx.x*blockDim.x + threadIdx.x;
	unsigned int id_conformation_begin = id_conformation * NEEImp;
	if (id_conformation < max)
	{
		for (int j=0; j < NEEImp; j++)
		{
			moves_x_d_mejora[id_conformation_begin + j] = moves_x_d[id_conformation];
                	moves_y_d_mejora[id_conformation_begin + j] = moves_y_d[id_conformation];
                	moves_z_d_mejora[id_conformation_begin + j] = moves_z_d[id_conformation];
                	quat_d_x_mejora[id_conformation_begin + j] = quat_d_x[id_conformation];
                	quat_d_y_mejora[id_conformation_begin + j] = quat_d_y[id_conformation];
                	quat_d_z_mejora[id_conformation_begin + j] = quat_d_z[id_conformation];
                	quat_d_w_mejora[id_conformation_begin + j] = quat_d_w[id_conformation];
                	energy_d_mejora[id_conformation_begin + j] = energy_d[id_conformation];
		}
	}
}

void generate_positions_move_warm_up (struct param_t param, struct vectors_t *vectors_e_s, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int orden_device, double *ttotal) 
{

	curandState_t *states_d;
	type_data *moves_x_d, *moves_y_d, *moves_z_d, * surface_x_d, * surface_y_d, * surface_z_d;		
	type_data *quat_d_x, *quat_d_y, *quat_d_z, *quat_d_w;
	unsigned int steps,inicial;
	cudaError_t cudaStatus;
	double tini;
	unsigned long int quatSize = vectors_e_s->nconformations * sizeof(type_data);
	cudaSetDevice(devices->id[orden_device]);
	
	dataToGPU(vectors_e_s->nconformations, vectors_e_s->num_surface, moves_x_d, moves_y_d, moves_z_d, quat_d_x,quat_d_y,quat_d_z,quat_d_w, states_d, vectors_e_s->surface_x, vectors_e_s->surface_y, vectors_e_s->surface_z, surface_x_d, surface_y_d, surface_z_d);
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Error en memoria.\n");

     //Inicializa el generador de numeros aleatorios y el cuaternion de cada simulacion
	setupCurandState <<< (vectors_e_s->nconformations/devices->hilos[orden_device*metaheuristic.num_kernels+4])+1, devices->hilos[orden_device*metaheuristic.num_kernels+4] >>>  (states_d, param.automatic_seed, param.seed,vectors_e_s->nconformations);

	// check if kernel execution generated and error
	cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");	

	// check if kernel execution generated and error
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");
	steps = 0;	
	inicial = 1000;
	
	//printf("devices->hilos[orden_device*metaheuristic.num_kernels+1] %d %d\n",devices->hilos[orden_device*metaheuristic.num_kernels+1],param.steps_warm_up_gpu);	
	tini = wtime();
	while (steps < param.steps_warm_up_gpu)
	{
			move <<< (vectors_e_s->nconformations /devices->hilos[orden_device*metaheuristic.num_kernels+1])+1,devices->hilos[orden_device*metaheuristic.num_kernels+1] >>> (states_d, surface_x_d, surface_y_d, surface_z_d, param.max_desp, moves_x_d, moves_y_d, moves_z_d,inicial,vectors_e_s->nconformations,0);
			steps++;
	}
	*ttotal = wtime() - tini;

	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel MOVE failed!");
	
	
	cudaStatus = cudaMemcpy(vectors_e_s->move_x, moves_x_d, quatSize, cudaMemcpyDeviceToHost);	
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed1!");
	cudaStatus = cudaMemcpy(vectors_e_s->move_y, moves_y_d, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->move_z, moves_z_d, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	cudaStatus = cudaMemcpy(vectors_e_s->quat_x, quat_d_x, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->quat_y, quat_d_y, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->quat_z, quat_d_z, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->quat_w, quat_d_w, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");

	cudaFree(surface_x_d);
	cudaFree(surface_y_d);
	cudaFree(surface_z_d);
	cudaFree(moves_x_d);
	cudaFree(moves_y_d);
	cudaFree(moves_z_d);
	cudaFree(quat_d_x);
	cudaFree(quat_d_y);
	cudaFree(quat_d_z);
	cudaFree(quat_d_w);
	cudaFree(states_d);


}

void generate_positions_rot_warm_up (struct param_t param, struct vectors_t *vectors_e_s, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int orden_device, double *ttotal) 
{

	curandState_t *states_d;
	type_data *moves_x_d, *moves_y_d, *moves_z_d, * surface_x_d, * surface_y_d, * surface_z_d;		
	type_data *quat_d_x, *quat_d_y, *quat_d_z, *quat_d_w;
	unsigned int steps,inicial;
	cudaError_t cudaStatus;
	double tini;
	unsigned long int quatSize = vectors_e_s->nconformations * sizeof(type_data);
	cudaSetDevice(devices->id[orden_device]);
	
	dataToGPU(vectors_e_s->nconformations, vectors_e_s->num_surface, moves_x_d, moves_y_d, moves_z_d, quat_d_x,quat_d_y,quat_d_z,quat_d_w, states_d, vectors_e_s->surface_x, vectors_e_s->surface_y, vectors_e_s->surface_z, surface_x_d, surface_y_d, surface_z_d);
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Error en memoria.\n");

     //Inicializa el generador de numeros aleatorios y el cuaternion de cada simulacion
	setupCurandState <<< (vectors_e_s->nconformations/devices->hilos[orden_device*metaheuristic.num_kernels+4])+1, devices->hilos[orden_device*metaheuristic.num_kernels+4] >>>  (states_d, param.automatic_seed,param.seed,vectors_e_s->nconformations);

	// check if kernel execution generated and error
	cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");

	setup <<< (vectors_e_s->nconformations /devices->hilos[orden_device*metaheuristic.num_kernels+3])+1, devices->hilos[orden_device*metaheuristic.num_kernels+3] >>> (quat_d_x,quat_d_y,quat_d_z,quat_d_w,vectors_e_s->nconformations);

	// check if kernel execution generated and error
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");
	steps = 0;	
	inicial = 1000;
	
	tini = wtime();
	while (steps < param.steps_warm_up_gpu)
	{
			rotation <<< (vectors_e_s->nconformations /devices->hilos[orden_device*metaheuristic.num_kernels+2])+1, devices->hilos[orden_device*metaheuristic.num_kernels+2] >>> (states_d, param.rotation, quat_d_x,quat_d_y,quat_d_z,quat_d_w,vectors_e_s->nconformations);
			steps++;
	}
	*ttotal = wtime() - tini;

	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel ROT failed!");
		
	cudaStatus = cudaMemcpy(vectors_e_s->move_x, moves_x_d, quatSize, cudaMemcpyDeviceToHost);	
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed1!");
	cudaStatus = cudaMemcpy(vectors_e_s->move_y, moves_y_d, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->move_z, moves_z_d, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	cudaStatus = cudaMemcpy(vectors_e_s->quat_x, quat_d_x, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->quat_y, quat_d_y, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->quat_z, quat_d_z, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->quat_w, quat_d_w, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");

	cudaFree(surface_x_d);
	cudaFree(surface_y_d);
	cudaFree(surface_z_d);
	cudaFree(moves_x_d);
	cudaFree(moves_y_d);
	cudaFree(moves_z_d);
	cudaFree(quat_d_x);
	cudaFree(quat_d_y);
	cudaFree(quat_d_z);
	cudaFree(quat_d_w);
	cudaFree(states_d);


}

void generate_positions_quat_warm_up (struct param_t param, struct vectors_t *vectors_e_s, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int orden_device, double *ttotal) 
{

	curandState_t *states_d;
	type_data *moves_x_d, *moves_y_d, *moves_z_d, * surface_x_d, * surface_y_d, * surface_z_d;		
	type_data *quat_d_x, *quat_d_y, *quat_d_z, *quat_d_w;
	unsigned int steps,inicial;
	cudaError_t cudaStatus;
	double tini;
	unsigned long int quatSize = vectors_e_s->nconformations * sizeof(type_data);
	cudaSetDevice(devices->id[orden_device]);
	
	dataToGPU(vectors_e_s->nconformations, vectors_e_s->num_surface, moves_x_d, moves_y_d, moves_z_d, quat_d_x,quat_d_y,quat_d_z,quat_d_w, states_d, vectors_e_s->surface_x, vectors_e_s->surface_y, vectors_e_s->surface_z, surface_x_d, surface_y_d, surface_z_d);
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Error en memoria.\n");

     //Inicializa el generador de numeros aleatorios y el cuaternion de cada simulacion
	setupCurandState <<< (vectors_e_s->nconformations/devices->hilos[orden_device*metaheuristic.num_kernels+4])+1, devices->hilos[orden_device*metaheuristic.num_kernels+4] >>>  (states_d, param.automatic_seed,param.seed,vectors_e_s->nconformations);

	// check if kernel execution generated and error
	cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");

	setup <<< (vectors_e_s->nconformations /devices->hilos[orden_device*metaheuristic.num_kernels+3])+1, devices->hilos[orden_device*metaheuristic.num_kernels+3] >>> (quat_d_x,quat_d_y,quat_d_z,quat_d_w,vectors_e_s->nconformations);

	// check if kernel execution generated and error
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");
	steps = 0;	
	inicial = 1000;
	
	tini = wtime();
	while (steps < param.steps_warm_up_gpu)
	{
			setup <<< (vectors_e_s->nconformations /devices->hilos[orden_device*metaheuristic.num_kernels+3])+1, devices->hilos[orden_device*metaheuristic.num_kernels+3] >>> (quat_d_x,quat_d_y,quat_d_z,quat_d_w,vectors_e_s->nconformations);
			steps++;
	}
	*ttotal = wtime() - tini;
	
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel QUAT failed!");
	
	cudaStatus = cudaMemcpy(vectors_e_s->move_x, moves_x_d, quatSize, cudaMemcpyDeviceToHost);	
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed1!");
	cudaStatus = cudaMemcpy(vectors_e_s->move_y, moves_y_d, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->move_z, moves_z_d, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	cudaStatus = cudaMemcpy(vectors_e_s->quat_x, quat_d_x, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->quat_y, quat_d_y, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->quat_z, quat_d_z, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->quat_w, quat_d_w, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");

	cudaFree(surface_x_d);
	cudaFree(surface_y_d);
	cudaFree(surface_z_d);
	cudaFree(moves_x_d);
	cudaFree(moves_y_d);
	cudaFree(moves_z_d);
	cudaFree(quat_d_x);
	cudaFree(quat_d_y);
	cudaFree(quat_d_z);
	cudaFree(quat_d_w);
	cudaFree(states_d);


}

void generate_positions_rand_warm_up (struct param_t param, struct vectors_t *vectors_e_s, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int orden_device, double *ttotal) 
{

	curandState_t *states_d;
	type_data *moves_x_d, *moves_y_d, *moves_z_d, * surface_x_d, * surface_y_d, * surface_z_d;		
	type_data *quat_d_x, *quat_d_y, *quat_d_z, *quat_d_w;
	unsigned int steps,inicial;
	cudaError_t cudaStatus;
	double tini;
	unsigned long int quatSize = vectors_e_s->nconformations * sizeof(type_data);
	cudaSetDevice(devices->id[orden_device]);
	
	dataToGPU(vectors_e_s->nconformations, vectors_e_s->num_surface, moves_x_d, moves_y_d, moves_z_d,quat_d_x,quat_d_y,quat_d_z,quat_d_w, states_d, vectors_e_s->surface_x, vectors_e_s->surface_y, vectors_e_s->surface_z, surface_x_d, surface_y_d, surface_z_d);
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Error en memoria.\n");

     //Inicializa el generador de numeros aleatorios y el cuaternion de cada simulacion
	setupCurandState <<< (vectors_e_s->nconformations/devices->hilos[orden_device*metaheuristic.num_kernels+4])+1, devices->hilos[orden_device*metaheuristic.num_kernels+4] >>>  (states_d, param.automatic_seed,param.seed,vectors_e_s->nconformations);

	// check if kernel execution generated and error
	cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");

	setup <<< (vectors_e_s->nconformations /devices->hilos[orden_device*metaheuristic.num_kernels+3])+1, devices->hilos[orden_device*metaheuristic.num_kernels+3] >>> (quat_d_x,quat_d_y,quat_d_z,quat_d_w,vectors_e_s->nconformations);

	// check if kernel execution generated and error
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");
	steps = 0;	
	inicial = 1000;
	
	tini = wtime();
	while (steps < param.steps_warm_up_gpu)
	{
			setupCurandState <<< (vectors_e_s->nconformations/devices->hilos[orden_device*metaheuristic.num_kernels+4])+1, devices->hilos[orden_device*metaheuristic.num_kernels+4] >>>  (states_d, param.automatic_seed, param.seed,vectors_e_s->nconformations);
			steps++;
	}
	*ttotal = wtime() - tini;

	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel CURAND failed!");
		
	cudaStatus = cudaMemcpy(vectors_e_s->move_x, moves_x_d, quatSize, cudaMemcpyDeviceToHost);	
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed1!");
	cudaStatus = cudaMemcpy(vectors_e_s->move_y, moves_y_d, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->move_z, moves_z_d, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	cudaStatus = cudaMemcpy(vectors_e_s->quat_x, quat_d_x, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->quat_y, quat_d_y, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->quat_z, quat_d_z, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->quat_w, quat_d_w, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");

	cudaFree(surface_x_d);
	cudaFree(surface_y_d);
	cudaFree(surface_z_d);
	cudaFree(moves_x_d);
	cudaFree(moves_y_d);
	cudaFree(moves_z_d);
	cudaFree(quat_d_x);
	cudaFree(quat_d_y);
	cudaFree(quat_d_z);
	cudaFree(quat_d_w);
	cudaFree(states_d);


}

void generate_positions_move_mejora_warm_up (struct param_t param, struct vectors_t *vectors_e_s, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int orden_device, double *ttotal) 
{

	curandState_t *states_d;
	type_data *moves_x_d, *moves_y_d, *moves_z_d, * surface_x_d, * surface_y_d, * surface_z_d;		
	type_data *quat_d_x, *quat_d_y, *quat_d_z, *quat_d_w;
	unsigned int steps,inicial;
	cudaError_t cudaStatus;
	double tini;
	unsigned long int quatSize = vectors_e_s->nconformations * sizeof(type_data);
	cudaSetDevice(devices->id[orden_device]);
	
	dataToGPU(vectors_e_s->nconformations, vectors_e_s->num_surface, moves_x_d, moves_y_d, moves_z_d,quat_d_x,quat_d_y,quat_d_z,quat_d_w, states_d, vectors_e_s->surface_x, vectors_e_s->surface_y, vectors_e_s->surface_z, surface_x_d, surface_y_d, surface_z_d);
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Error en memoria.\n");

     //Inicializa el generador de numeros aleatorios y el cuaternion de cada simulacion
	setupCurandState <<< (vectors_e_s->nconformations/devices->hilos[orden_device*metaheuristic.num_kernels+4])+1, devices->hilos[orden_device*metaheuristic.num_kernels+4] >>>  (states_d, param.automatic_seed,param.seed,vectors_e_s->nconformations);

	// check if kernel execution generated and error
	cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");	

	// check if kernel execution generated and error
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");
	steps = 0;	
	inicial = 1000;
	
	//printf("devices->hilos[orden_device*metaheuristic.num_kernels+1] %d %d\n",devices->hilos[orden_device*metaheuristic.num_kernels+1],param.steps_warm_up_gpu);	
	tini = wtime();
	while (steps < param.steps_warm_up_gpu)
	{
			move_mejora <<< (vectors_e_s->nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+5])+1, devices->hilos[orden_device*metaheuristic.num_kernels+5] >>> (states_d,param.max_desp,moves_x_d,moves_y_d,moves_z_d,vectors_e_s->nconformations);
			steps++;
	}
	*ttotal = wtime() - tini;
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel move mejora failed!");

	cudaStatus = cudaMemcpy(vectors_e_s->move_x, moves_x_d, quatSize, cudaMemcpyDeviceToHost);	
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed1!");
	cudaStatus = cudaMemcpy(vectors_e_s->move_y, moves_y_d, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->move_z, moves_z_d, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values failed!");
	
	cudaStatus = cudaMemcpy(vectors_e_s->quat_x, quat_d_x, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->quat_y, quat_d_y, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->quat_z, quat_d_z, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->quat_w, quat_d_w, quatSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");

	cudaFree(surface_x_d);
	cudaFree(surface_y_d);
	cudaFree(surface_z_d);
	cudaFree(moves_x_d);
	cudaFree(moves_y_d);
	cudaFree(moves_z_d);
	cudaFree(quat_d_x);
	cudaFree(quat_d_y);
	cudaFree(quat_d_z);
	cudaFree(quat_d_w);
	cudaFree(states_d);


}

void generate_positions_incl_mejora_warm_up (struct receptor_t proteina, struct ligand_t ligando, struct param_t param, struct vectors_t *vectors_e_s, struct metaheuristic_t metaheuristic, struct device_t *devices, unsigned int orden_device, double *ttotal)
{
	curandState_t *states_d;
	type_data *moves_x_d, *moves_y_d, *moves_z_d, *energy_d;		
	type_data *quat_d_x, *quat_d_y, *quat_d_z, *quat_d_w, *quat_d_mejora_x, *quat_d_mejora_y, *quat_d_mejora_z, *quat_d_mejora_w;
	type_data *moves_x_d_mejora, *moves_y_d_mejora, *moves_z_d_mejora, *energy_d_mejora;
	unsigned int *energy_nconformation_d, *energy_nconformation_d_mejora;	
	
	type_data *rec_x_d, *rec_y_d, *rec_z_d, *qr_d;
	char *rectype_d, *ligtype_d;
	unsigned int *bondsr_d, *bonds_d;
	type_data *lig_x_d, *lig_y_d, *lig_z_d, *ql_d;
	
	double tini;	
	
	unsigned int steps, moveType=MOVE;
	cudaError_t cudaStatus;
	unsigned long int moveSize = vectors_e_s->nconformations * sizeof(type_data);
	cudaSetDevice(devices->id[orden_device]);	
	dataToGPU_multigpu_mejorar_inicializar(vectors_e_s->nconformations, energy_d, energy_nconformation_d, energy_d_mejora, energy_nconformation_d_mejora, moves_x_d, moves_y_d, moves_z_d, quat_d_x,quat_d_y,quat_d_z,quat_d_w, moves_x_d_mejora, moves_y_d_mejora, moves_z_d_mejora, quat_d_mejora_x, quat_d_mejora_y, quat_d_mejora_z, quat_d_mejora_w, states_d, vectors_e_s->move_x, vectors_e_s->move_y, vectors_e_s->move_z, vectors_e_s->quat_x,  vectors_e_s->quat_y,  vectors_e_s->quat_z,  vectors_e_s->quat_w, vectors_e_s->energy.energy, vectors_e_s->energy.n_conformation, metaheuristic);
	//commonDataToGPU (proteina, proteina_d, ligando, ligando_d);
	commonDataToGPU_WS (proteina,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,ligando,lig_x_d,lig_y_d,lig_z_d,ql_d,ligtype_d,bonds_d);
	save_params(vectors_e_s->weights,vectors_e_s->f_params);
	
	setupCurandState <<<(vectors_e_s->nconformations/devices->hilos[orden_device*metaheuristic.num_kernels+4])+1, devices->hilos[orden_device*metaheuristic.num_kernels+4] >>>  (states_d, param.automatic_seed,param.seed,vectors_e_s->nconformations);
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");

	setup <<< (vectors_e_s->nconformations /devices->hilos[orden_device*metaheuristic.num_kernels+3])+1, devices->hilos[orden_device*metaheuristic.num_kernels+3] >>> (quat_d_x,quat_d_y,quat_d_z,quat_d_w,vectors_e_s->nconformations);	
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel execution failed \n");
	
	
	move_mejora <<< (vectors_e_s->nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+5])+1,devices->hilos[orden_device*metaheuristic.num_kernels+5] >>> (states_d,param.max_desp,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,vectors_e_s->nconformations);
	unsigned int size = 5 * devices->hilos[orden_device*metaheuristic.num_kernels] * sizeof(type_data) + sizeof(char)*devices->hilos[orden_device*metaheuristic.num_kernels];
	unsigned int blk = ceil(vectors_e_s->nconformations*WARP_SIZE / devices->hilos[orden_device*metaheuristic.num_kernels])+1;	
	unsigned int max = vectors_e_s->nconformations* WARP_SIZE;	
	dim3 hilos (devices->hilos[orden_device*metaheuristic.num_kernels]/8,8);
	dim3 grid  (ceil(blk/8), 8);		                                                             
	Gpu_full_Kernel_Conformations_by_warp <<<grid,hilos,size>>> (proteina.nrec,ligando.atoms,ligando.nlig,rec_x_d,rec_y_d,rec_z_d,qr_d,rectype_d,bondsr_d,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_d_mejora_x, quat_d_mejora_y, quat_d_mejora_z, quat_d_mejora_w,lig_x_d,lig_y_d,lig_z_d,ql_d,ligtype_d,bonds_d,energy_d_mejora,energy_nconformation_d,0,devices->hilos[orden_device*metaheuristic.num_kernels],max);
	
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernel energy failed!");
	
	steps = 0;
	tini = wtime();
	while (steps < param.steps_warm_up_gpu)
	{
		incluir_mejorar <<<(vectors_e_s->nconformations / devices->hilos[orden_device*metaheuristic.num_kernels+6])+1, devices->hilos[orden_device*metaheuristic.num_kernels+6]>>> (moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_d_mejora_x, quat_d_mejora_y, quat_d_mejora_z, quat_d_mejora_w,energy_d_mejora,energy_nconformation_d_mejora,moves_x_d,moves_y_d,moves_z_d,quat_d_x,quat_d_y,quat_d_z,quat_d_w,energy_d,0,vectors_e_s->nconformations);	
		steps++;	
	}
	*ttotal = wtime() - tini;
	
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) fprintf(stderr, "Kernels antes de copia fallo!\n");
	//GPUtoHost (vectors_e_s->nconformations,vectors_e_s,moves_x_d_mejora,moves_y_d_mejora,moves_z_d_mejora,quat_d_mejora,energy_d_mejora);
	cudaStatus = cudaMemcpy(vectors_e_s->move_x, moves_x_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_x_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->move_y, moves_y_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_y_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->move_z, moves_z_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values moves_z_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->energy.energy, energy_d, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values energy_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->quat_x, quat_d_x, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->quat_y, quat_d_y, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->quat_z, quat_d_z, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	cudaStatus = cudaMemcpy(vectors_e_s->quat_w, quat_d_w, moveSize, cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) fprintf(stderr, "cudaMemcpy values quat_d_mejora GPUtoHost failed!");
	
	cudaFree(states_d);
	cudaFree(moves_x_d);
	cudaFree(moves_y_d);
	cudaFree(moves_z_d);
	cudaFree(quat_d_x);
	cudaFree(quat_d_y);
	cudaFree(quat_d_z);
	cudaFree(quat_d_w);
	cudaFree(rec_x_d);
	cudaFree(rec_y_d);
	cudaFree(rec_z_d);
	cudaFree(qr_d);
	cudaFree(bondsr_d);
	cudaFree(rectype_d);
	cudaFree(lig_x_d);
	cudaFree(lig_y_d);
	cudaFree(lig_z_d);
	cudaFree(ql_d);
	cudaFree(ligtype_d);
	cudaFree(bonds_d);
	cudaFree(energy_d);
	cudaFree(energy_d_mejora);
	cudaFree(energy_nconformation_d);
	cudaFree(energy_nconformation_d_mejora);
	cudaFree(moves_x_d_mejora);
	cudaFree(moves_y_d_mejora);
	cudaFree(moves_z_d_mejora);
	cudaFree(quat_d_mejora_x);
	cudaFree(quat_d_mejora_y);
	cudaFree(quat_d_mejora_z);
	cudaFree(quat_d_mejora_w);
	cudaDeviceReset();	
}

