#ifndef _DEFINITIONS_H
#define _DEFINITIONS_H
#include "type.h"

#define BLOCK_SIZE 64
#define BLOCK_64 64
#define BLOCK_SM 64
#define BLOCK_96 96
#define BLOCK_128 128
#define BLOCK_256 256 
#define BLOCK_512 512


#define xA 12.
#define xB 6.
#define xC 12.
#define xD 10.

#define S_EWH 0
#define S_EWHS 1
#define S_AD4 2

#define sigma_autodock 3.6L
#define qsolpar 0.01097L
#define ELECSCALE 332.0639L
#define APPROX_ZERO 1.0E-6
#define MAX_ENERGY -50000.0

#define FILENAME 256
#define SCORING_TERMS 5
#define MAXTYPES 22
#define WARP_SIZE 32
#define ES_CONSTANT 34.7361375
#define EPSILON 4.0
#define SUBTYPEMAXLEN 7		//Longitud maxima para el tipo de atomo

#define MAXBOND 6
#define PI 3.141592653589793 

#define NIT2 0		//N with valence less than 3
#define OXI 1		//O
#define AZU 2		//S
#define NIT 3		//N
#define CAR 4		//C
#define HID 5		//H
#define HBOND 6	//Enlace de H
#define IRI 7		//I
#define CLO 8		//Cl
#define FLU 9   	//F
#define BRO 10		//Br
#define LIT 11		//Li
#define SOD 12		//Na
#define POT 13		//K
#define MAG 14		//Mg
#define CAL 15		//Ca
#define ZIN 16		//Zn
#define DUM 17 		// Dummy: otros tipos
#define VOID 18		//Padding
#define AR 19		//Carbono aromatico
#define FER 20		//Hierro
#define PH 21		//Fósforo

#define MOVE 1
#define ROTATE 2
#define FLEX 3

#define INI 2222
#define COMB 1234
#define IMPR 4321
const char route_bar[] = "/";
const int HEADERLENGTH = 80;
#endif
