#include <math.h>
#include "energy_common.h"
#include "vector_types.h"

void setInitialState_cpp (type_data *q_x, type_data *q_y, type_data *q_z, type_data *q_w){

	*q_w = 1.0;
	*q_x = *q_y = *q_z = 0.0;
}


type_data dotProduct_cpp (type_data *src_x, type_data *src_y, type_data *src_z, type_data *src_w, type_data *src2_x, type_data *src2_y, type_data *src2_z, type_data *src2_w){
	return  *src_w * *src2_w + *src_x * *src2_x + *src_y * *src2_y + *src_z * *src2_z;
}

type_data norm_cpp (type_data *q_x, type_data *q_y, type_data *q_z, type_data *q_w){
	return sqrtf(dotProduct_cpp (q_x,q_y,q_z,q_w,q_x,q_y,q_z,q_w));
}

extern type_data normalize_cpp (type_data *q_x, type_data *q_y, type_data *q_z, type_data *q_w){

	type_data m = norm_cpp(q_x,q_y,q_z,q_w);
	*q_w /= m;
	*q_x /= m;
	*q_y /= m;
	*q_z /= m;
}

void setValues_cpp  (type_data *q_x, type_data *q_y, type_data *q_z, type_data *q_w, type_data w, type_data *vector1, type_data *vector2, type_data *vector3){

	*q_w = w;
	*q_x = *vector1;//[0];
	*q_y = *vector2;//[1];
	*q_z = *vector3;//[2];
}

extern void setRotation_cpp (type_data *q_x, type_data *q_y, type_data *q_z, type_data *q_w,  type_data angle, type_data * vector){
	angle = angle/2;
	*q_w = cos (angle);
	*q_x = vector[0] * sin (angle);
	*q_y = vector[1] * sin (angle);
	*q_z = vector[2] * sin (angle);
}

extern void composeRotation_cpp (type_data *p_x, type_data *p_y, type_data *p_z, type_data *p_w, type_data *q_x, type_data *q_y, type_data *q_z, type_data *q_w, type_data *dst_x, type_data *dst_y, type_data *dst_z, type_data *dst_w){

	type_data w, x, y, z;

	w = *p_w * *q_w - *p_x * *q_x - *p_y * *q_y - *p_z * *q_z;
	x = *p_w * *q_x + *p_x * *q_w + *p_y * *q_z - *p_z * *q_y;
	y = *p_w * *q_y - *p_x * *q_z + *p_y * *q_w + *p_z * *q_x;
	z = *p_w * *q_z + *p_x * *q_y - *p_y * *q_x + *p_z * *q_w;

	*dst_w = w;
	*dst_x = x;
	*dst_y = y;
	*dst_z = z;
}

extern void inverse_cpp (type_data *src_x, type_data *src_y, type_data *src_z, type_data *src_w, type_data *dst_x, type_data *dst_y, type_data *dst_z, type_data *dst_w){

	type_data w, x, y, z;
	type_data abs = norm_cpp (src_x,src_y,src_z,src_w);
	w = *src_w / abs;
	x = -*src_x / abs;
	y = -*src_y / abs;
	z = -*src_z / abs;

	*dst_w = w;
	*dst_x = x;
	*dst_y = y;
	*dst_z = z;
}

extern void rotate3DPoint_cpp (type_data *q_x, type_data *q_y, type_data *q_z, type_data *q_w, type_data * src1, type_data * src2, type_data * src3, type_data * dst){

	type_data point_x, point_y, point_z, point_w;
	type_data result_x, result_y, result_z, result_w;
	type_data inv_x, inv_y, inv_z, inv_w;
	
	result_x = *q_x;
	result_y = *q_y;
	result_z = *q_z;
	result_w = *q_w;

	setValues_cpp (&point_x, &point_y, &point_z, &point_w, 0, src1, src2, src3);

	inverse_cpp(q_x,q_y,q_z,q_w, &inv_x, &inv_y, &inv_z, &inv_w);

	composeRotation_cpp(&result_x, &result_y, &result_z, &result_w, &point_x, &point_y, &point_z, &point_w, &result_x, &result_y, &result_z, &result_w);

	composeRotation_cpp(&result_x, &result_y, &result_z, &result_w, &inv_x, &inv_y, &inv_z, &inv_w, &result_x, &result_y, &result_z, &result_w);

	dst[0] = result_x;
 	dst[1] = result_y;
	dst[2] = result_z;

}

