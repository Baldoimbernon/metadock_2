#include <stdio.h>
#include "type.h"

using namespace std;

// Función para dividir el array y hacer los intercambios
unsigned int divide(type_data *array, unsigned int *keys, unsigned int start, unsigned int end) 
{
	unsigned int left;
	unsigned int right;
    	type_data pivot;
    	type_data temp;
	unsigned int tmp_i;
 
	pivot = array[start];
	left = start;
	right = end;
 
	// Mientras no se cruzen los índices
	while (left < right) {
		while (array[right] > pivot) {
			right--;
		}
 
        	while ((left < right) && (array[left] <= pivot)) {
            		left++;
        	}

        	// Si todavía no se cruzan los indices seguimos intercambiando
        	if (left < right) {
            		temp = array[left];
            		array[left] = array[right];
            		array[right] = temp;
	    		tmp_i = keys[left];
	    		keys[left] = keys[right];
	    		keys[right] = tmp_i;
        	}
    	}
 
    	// Los índices ya se han cruzado, ponemos el pivot en el lugar que le corresponde
    	temp = array[right];
    	array[right] = array[start];
    	array[start] = temp;
	tmp_i = keys[right];
	keys[right] = keys[start];
	keys[start] = tmp_i;
 
	// La nueva posición del pivot
    	return right;
}
 
// Función recursiva para hacer el ordenamiento
extern void quicksort(type_data *array, unsigned int *keys, unsigned int start, unsigned int end)
{
	int pivot;
 
	if (start < end) {
        	pivot = divide(array, keys, start, end);
 
        	// Ordeno la lista de los menores
	        quicksort(array, keys, start, pivot - 1);
 
        	// Ordeno la lista de los mayores
        	quicksort(array, keys, pivot + 1, end);
    	}
}
