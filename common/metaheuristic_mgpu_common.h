#ifndef __METAHEURISTIC_MGPU_COMMON_H
#define __METAHEURISTIC_MGPU_COMMON_H

extern void divide_charge (unsigned int surface, unsigned int numprocess, unsigned int *points_by_process, unsigned int *points_by_begin, unsigned int *order_node, type_data *time_warm_all);
extern void multigpuSolver_warm_up (struct ligand_t ligando, struct receptor_t proteina, type_data *move_x, type_data *move_y, type_data *move_z, type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, struct energy_t energy, type_data *weights, struct force_field_param_t *f_params, unsigned int nconformations, struct param_t param, struct metaheuristic_t metaheuristic, struct device_t *devices, double *tiempos, unsigned int stride, unsigned int n_gpus);
extern void warm_up_devices (struct ligand_t ligando, struct receptor_t proteina, struct vectors_t *vectors_e, struct device_t *devices, type_data *weights, struct force_field_param_t *f_params, struct param_t param, struct metaheuristic_t metaheuristic, unsigned int nconformations,unsigned int *number_devices);
extern void warm_up_bloques_gpu (struct ligand_t ligando, struct receptor_t proteina, struct vectors_t *vectors_e, type_data *weights, struct force_field_param_t *f_params, struct param_t param, struct metaheuristic_t metaheuristic,  struct device_t *devices, unsigned int orden_device);
extern void copy_surface(struct vectors_t *vectors_e,struct vectors_t *vectors_o);
extern void limpiar_surface (struct vectors_t *vectors_o);
extern void final_parametros_paralelos_gpu (struct device_t *devices, unsigned int kernels);
extern void escribir_gpu (struct param_t param, struct metaheuristic_t *metaheuristic, struct device_t *devices);
extern void escribir_multigpu (struct param_t param, struct metaheuristic_t *metaheuristic, struct device_t *devices, unsigned int opc);
extern void cambia_param_blocks ( struct device_t *devices,unsigned int orden_device,unsigned int num_kernels,unsigned int kernel_num, unsigned int i);
extern void adjust_param_GPU (int *mode);
extern double calculo_nodo_multigpu_mpi (struct ligand_t ligando, struct receptor_t proteina, struct vectors_t *vectors_e, struct device_t *devices, type_data *weights, struct force_field_param_t *f_params, struct param_t param, struct metaheuristic_t metaheuristic, unsigned int nconformations,unsigned int *number_devices);
#endif
