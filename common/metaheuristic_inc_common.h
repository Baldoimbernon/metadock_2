#ifndef __METAHEU_INC_COMMON_H__
#define __METAHEU_INC_COMMON_H__

extern void limpiar (struct vectors_t *vectors_1, struct vectors_t *vectors_2);
extern void incluir_warm_up (struct vectors_t *vectors_e, struct param_t param, unsigned int hilos);

#endif
