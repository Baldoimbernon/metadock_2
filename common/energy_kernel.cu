#include "definitions.h"
#include "energy_common.h"
#include "energy_common-gpu.h"
#include "energy_struct.h"
#include "energy_rotation.h"

__constant__ struct force_field_param_t f_params_c [MAXTYPES];
__constant__ type_data weights_c[SCORING_TERMS];

__device__ inline type_data calc_ddd_Mehler_Solmajer_GPU( type_data distance, type_data approx_zero )
{
    const type_data lambda = 0.003627L;
    const type_data epsilon0 = 78.4L;
    const type_data A = -8.5525L;
    const type_data B = epsilon0 - A;
    const type_data rk= 7.7839L;
    const type_data lambda_B = -lambda * B;

    const type_data epsilon = A + B / (1.0L + rk*__expf(lambda_B * distance));

    if (epsilon < approx_zero) {
        return 1.0L;
    }
    else {
        return epsilon;
    }
}

__global__ void better (type_data *energy, int conformations,type_data *valor)
{
        int tid = threadIdx.x;
        //float max=10000;

        if (tid == 0)
        {
                float max = 10000;
                for (int j=0;j<conformations;j++)
                {
                        if (max > energy[j])
                        {
                                max = energy[j];
                                //printf("MAX GPU: %f\n",max);
                        }
                }
                printf("MAX GPU: %f %d\n",max,conformations);
        }

}
__global__ void f_example (unsigned int files, unsigned int *atoms_d, struct flexibility_params_t *flexibility_params_d)
{
	unsigned int id = blockIdx.x*blockDim.x+threadIdx.x;
	if (id < 8)
	{
		printf("id %d\n",id);
		printf("n_links %d\n",flexibility_params_d[id].n_links);
	}
}

extern void save_params (type_data *weights, struct force_field_param_t *f_params)
{
	cudaError_t cudaStatus;
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) printf("Antes de memoria constante %d\n",cudaStatus);
	cudaStatus = cudaMemcpyToSymbol(f_params_c, f_params, MAXTYPES * sizeof(struct force_field_param_t));
	 cudaDeviceSynchronize();
        cudaStatus = cudaGetLastError();
        if (cudaStatus != cudaSuccess) printf("Error memoria constante 1 %d\n",cudaStatus);
	cudaStatus = cudaMemcpyToSymbol(weights_c, weights, SCORING_TERMS * sizeof(type_data));
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) printf("Error memoria constante 2 %d\n",cudaStatus);
}


__device__ void sub (type_data *v , type_data * w, type_data * dst){

     dst[0] = v[0] - w[0];
     dst[1] = v[1] - w[1];
     dst[2] = v[2] - w[2];

}

__device__ void sub (type_data *v1, type_data *v2, type_data *v3, type_data *w1, type_data *w2, type_data *w3, type_data *dst) {
	
	dst[0] = *v1 - *w1;
	dst[1] = *v2 - *w2;
	dst[2] = *v3 - *w3;

}

__device__ type_data norma (type_data *v){

     return  sqrtf(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
}

__device__ type_data cosTheta (type_data * v, type_data * w){

     type_data scalar, cos_angle;
     type_data modv, modw;
     scalar = v[0] * w[0] + v[1] * w[1] + v[2] * w[2];

     modv = norma(v);
     modw = norma(w);
     cos_angle = scalar / (modv * modw);

     if (cos_angle > 1.0) return 1.0;

     if (cos_angle < -1.0) return -1.0;

     return cos_angle;
}

__device__ inline void reduction_shfl(type_data* prob, unsigned int n, unsigned int index){
  
#if  __CUDA_ARCH__ >= 350
    type_data aux;
    *prob = (index >= n) ? 0.0f : *prob;
  
     for(unsigned int s=WARP_SIZE>>1; s>0; s>>=1) {
         aux = __shfl_down((type_data)*prob, s);
          *prob+=aux;
      } 
#endif
}

/*__global__ void energyKernel_GPU_full(int nlig, int nrec, int natom, struct receptor_t* proteina_d, type_data *lig_x, type_data *lig_y, type_data *lig_z, type_data * moves_x_d, type_data *moves_y_d, type_data *moves_z_d, type_data *quat_d_x, type_data *quat_d_y, type_data *quat_d_z, type_data *quat_d_w, type_data *energy_d){

	__shared__ type_data proteina_x[128];
	__shared__ type_data proteina_y[128];
	__shared__ type_data proteina_z[128];
	__shared__ char proteina_type[128];
	__shared__ type_data proteina_qr[128];
	
	type_data miatomo[3],distx,disty,distz;
	type_data temp_vdw, temp_es, temp_total, eps, sig, epslig, siglig, term6;
	float4 qaux;
	int atom,id_atom,ind1,ind2;	
	int tid = threadIdx.x;
	int gid = blockIdx.x * blockDim.x + tid;
	int id_conformation = gid / WARP_SIZE;
	int lane_id = tid & (WARP_SIZE-1);
	int warp_id = tid>>5;
			
 	id_atom = id_conformation * nlig;
	temp_total = 0;
	temp_vdw = 0;
	temp_es = 0;
	qaux = quat_d[id_conformation];	
	
	for (int k = 0; k < nrec;k+=128)
	{
		proteina_x[tid] = proteina_d->rec_x[k + tid];
		proteina_y[tid] = proteina_d->rec_y[k + tid];
		proteina_z[tid] = proteina_d->rec_z[k + tid];
		proteina_type[tid] = proteina_d->rectype[k + tid];
		proteina_qr[tid] = proteina_d->qr[k + tid];
		
		__syncthreads();
			
		for (int i=lane_id;i<natom;i+=WARP_SIZE)
		{
					
			//rotation
			rotate3DPoint (&qaux,(lig_x + i), (lig_y + i), (lig_z + i), miatomo);
			//traslation
			miatomo[0] += moves_x_d[id_conformation];
			miatomo[1] += moves_y_d[id_conformation];
			miatomo[2] += moves_z_d[id_conformation];
		
			//if ((id_conformation == 0) && (atom == 0)) {
			//	printf("atom_x %f, atom_y %f, atom_z %f\n",miatomo[0], miatomo[1],miatomo[2]);
			//}

			ind2 = ligtype_c[i];
			epslig = vdw_params_c [ind2].epsilon;
			siglig = vdw_params_c [ind2].sigma;			
			
			//if ((id_conformation == 0) && (i == 0)) printf("siglig %f\n",siglig);
						
			for (int j = 0; j < 128; j++)
			{	
				
				distx = miatomo[0] - proteina_x[j];
				disty = miatomo[1] - proteina_y[j];
				distz = miatomo[2] - proteina_z[j];
				distx = distx*distx + disty*disty + distz*distz;
	  		    				
				ind2 = proteina_type[k+j];
				//if ((id_conformation == 0) && (printf("ind2 %d atomo %d\n",ind2,k+j);
				eps = sqrtf(vdw_params_c[ind2].epsilon * epslig);
				sig = vdw_params_c[ind2].sigma * siglig;
						
				term6 = sig / distx;
				term6 = term6 * term6 * term6; //^6
				
				temp_vdw += 4.0 * eps * term6 * (term6 - 1);
				temp_es += proteina_qr[k + tid] * rsqrtf(distx); 				
			}
			//id_atom += WARP_SIZE;
			temp_es *= ES_CONSTANT * ql_c[i];	
		}
		__syncthreads();	
	}
	temp_total = temp_vdw + temp_es;
	reduction_shfl(&temp_total,WARP_SIZE,warp_id);	
	if (lane_id == 0) energy_d[id_conformation] = temp_total;
}*/
__global__ void Gpu_full_Kernel_Conformations_vs_by_warp_type2 (unsigned int *nlinks, unsigned int nrec, unsigned int conf_point, unsigned int *atom, unsigned int nlig, type_data *rec_x_d, type_data *rec_y_d, type_data *rec_z_d, type_data *qr_d, char *rectype_d, unsigned int *bondsr_d, type_data *conformations_x_d, type_data *conformations_y_d, type_data *conformations_z_d, type_data *move_x_d, type_data *move_y_d, type_data *move_z_d, type_data *quat_d_x, type_data *quat_d_y, type_data *quat_d_z, type_data *quat_d_w, type_data *ql_d, char *ligtype_d, unsigned int *bonds_d, type_data *energy_d, unsigned int *nconformation, unsigned int stride, unsigned int tam, unsigned long int max)
{
	extern __shared__ type_data data[];
	type_data *proteina_x = (type_data *)data;
	type_data *proteina_y = (type_data *)&proteina_x[tam];
	type_data *proteina_z = (type_data *)&proteina_y[tam];
        type_data *proteina_qr = (type_data *)&proteina_z[tam];
        type_data *partial_energy = (type_data *)&proteina_qr[tam];
        char *proteina_type = (char *)&partial_energy[tam];

        type_data temp_vdw, temp_es, temp_hbond, temp_solv, e_es, e_vdw, e_hbond, e_solv, tmp_e_es=0,tmp_e_vdw=0,tmp_e_solv=0,tmp_e_hbond=0;
        type_data temp_total;
	type_data tmpconst,tmpconst_h,cA,cB,cC,cD,eps_hb,sig_hb,rA,rB,rC,rD;
        type_data ligx,ligy,ligz,distx,disty,distz,mx,my,mz,temp_sqrt;
        type_data eps, sig, epslig, siglig, term6,term12;
        type_data miatomo[3],v[3],w[3];
        
	type_data qaux_x,qaux_y,qaux_z,qaux_w;
        type_data solv_asp_1, solv_asp_2, solv_vol_1, solv_vol_2,solv_qasp_1,solv_qasp_2,pre_calc,lig_map;
     
        type_data minus_inv_two_sigma_sqd = -0.5L / (sigma_autodock * sigma_autodock);
        type_data exponencial;
        unsigned int ind1,ind2;
        unsigned int tid = threadIdx.x + threadIdx.y * blockDim.x;
        unsigned int blockID = blockIdx.x + blockIdx.y * gridDim.x;
        unsigned int long gid = blockID * (blockDim.x * blockDim.y) + (threadIdx.y*blockDim.x) + threadIdx.x;
        unsigned int long id_conformation, id_atom;

        int lane_id = tid & (WARP_SIZE-1);
        int warp_id = tid>>5;

	if (gid < max)
        {
                id_conformation = nconformation[gid / WARP_SIZE] - stride;
				id_atom = id_conformation * nlig;
                //if (id_conformation == 1) printf("id_conf %d tid %d\n",id_conformation,tid);
                //if (id_conformation > 449695) printf("MAYOR %d\n",id_conformation);
                qaux_x = quat_d_x[id_conformation];
                qaux_y = quat_d_y[id_conformation];
                qaux_z = quat_d_z[id_conformation];
                qaux_w = quat_d_w[id_conformation];
        //traslation
                mx = move_x_d[id_conformation];
                my = move_y_d[id_conformation];
                mz = move_z_d[id_conformation];
                //if (id_conformation == 449695) printf("%f %f %f %f %f %f %f\n",mx,my,mz,qaux_x,qaux_y,qaux_z,qaux_w);
        }

        temp_vdw = 0;
        temp_es = 0;
        temp_hbond = 0;
        temp_solv = 0;

         for (unsigned int k = 0; k < nrec;k+=tam)
        {

                proteina_x[tid] = rec_x_d[k + tid];
                proteina_y[tid] = rec_y_d[k + tid];
                proteina_z[tid] = rec_z_d[k + tid];
                proteina_qr[tid] = qr_d[k + tid];
                proteina_type[tid] = rectype_d[k + tid];

                __syncthreads();

                if (gid < max)
                {
                        //if (id_conformation ==0) printf("id_conf %d, %d\n",nconformation[gid/WARP_SIZE],atom[nconformation[gid / WARP_SIZE]/conf_point]);	
			for (unsigned int i=lane_id;i<atom[nconformation[gid / WARP_SIZE]/conf_point];i+=WARP_SIZE)
                        {
                                rotate3DPoint (&qaux_x,&qaux_y,&qaux_z,&qaux_w,(conformations_x_d + id_atom + i),(conformations_y_d + id_atom + i),(conformations_z_d + id_atom + i),miatomo);

                                ligx = miatomo[0] + mx;
                                ligy = miatomo[1] + my;
                                ligz = miatomo[2] + mz;
				//if ((id_conformation == 0) && (lane_id == 0)) printf("miatomo[0] %f miatomo[1] %f miatomo[2] %f ligx %f ligy %f ligz %f\n",miatomo[0],miatomo[1],miatomo[2],ligx,ligy,ligz);
                                ind1 = ligtype_d[id_atom + i];
                                solv_asp_1 = f_params_c[ind1].asp;
                                solv_vol_1 = f_params_c[ind1].vol;
                                //epslig = vdw_params_c [ind1].epsilon;
                                //siglig = vdw_params_c [ind1].sigma;
                                epslig = f_params_c [ind1].epsilon;
                                siglig = f_params_c [ind1].sigma;

                                for (unsigned int j = 0; j < tam; j++)
                                {
                                        if ((k + j) < nrec)
                                        {
                                                e_es = 0;
                                                e_vdw = 0;
                                                e_hbond = 0;
                                                e_solv = 0;
                                                distx = ligx - proteina_x[j];
                                                disty = ligy - proteina_y[j];
                                                distz = ligz - proteina_z[j];
                                                distx = distx*distx + disty*disty + distz*distz;
                                                temp_sqrt =  sqrtf(distx);

                                                exponencial = expf(minus_inv_two_sigma_sqd * distx) * weights_c[3];

                                                ind2 = proteina_type[j];
                                                solv_asp_2 = f_params_c[ind2].asp;
                                                solv_vol_2 = f_params_c[ind2].vol;
                                                //eps = sqrtf(vdw_params_c[ind2].epsilon * epslig);
                                                //sig = vdw_params_c[ind2].sigma * siglig;
						type_data dato_eij = calc_ddd_Mehler_Solmajer_GPU(temp_sqrt,APPROX_ZERO);

                                                eps =  sqrtf(f_params_c[ind2].epsilon * epslig);
                                                sig = ((f_params_c[ind2].sigma + siglig))*0.5L;
                                		eps_hb =  (f_params_c[ind1].epsilon_h +  f_params_c[ind2].epsilon_h);//  * a_scoring.p_hbond;
                                		sig_hb = ((f_params_c[ind1].sigma_h + f_params_c[ind2].sigma_h));//*0.5L;
						
						tmpconst = eps / (xA - xB);
                                		tmpconst_h = eps_hb / (xC - xD);

                                		cA =  tmpconst * powf( sig, xA ) * xB;
                                		cB =  tmpconst * powf( sig, xB ) * xA;

                                		cC =  tmpconst_h * powf( sig_hb, xC ) * xD;
                                		cD =  tmpconst_h * powf( sig_hb, xD ) * xC;

                                		//dxA = (double) xA;
                                		//dxB = (double) xB;
                                		//dxC = (double) xC;
                                		//dxD = (double) xD;

                                		rA = powf( temp_sqrt, xA);
                                		rB = powf( temp_sqrt, xB);
                                		rC = powf( temp_sqrt, xC);
                                		rD = powf( temp_sqrt, xD);

                                		term6 = cB / rB;//pow((double)dist, (double)xB);
                                		term12 = cA / rA;//pow((double)dist, (double)xA);

                                		e_es = (1.0 / (dato_eij * temp_sqrt)) * proteina_qr[j] * ELECSCALE * weights_c[2];
                                		//if (dist < 7.0)
                                		e_vdw = term12 - term6;

                                		if (temp_sqrt < 8)
                                		{
                                        		pre_calc = ((solv_asp_1 * solv_vol_2)  + ((solv_asp_2 + qsolpar * fabs(proteina_qr[j])) * solv_vol_1)) * exponencial;// * a_scoring.p_desolv;
                                        		lig_map = qsolpar * solv_vol_2 * exponencial;
                                         		e_solv = pre_calc + lig_map;
                                		}
						if ((temp_sqrt > 0) && (temp_sqrt < 8) && (ind1 == OXI || ind1 == NIT2 || ind1 == AZU)){
                                        		if (ind2 == HBOND){
                                                		//printf("Hola\n");
                                                		sub(&ligx,&ligy,&ligz, &proteina_x[j], &proteina_y[j], &proteina_z[j], v);
								sub(&(rec_x_d[bondsr_d[MAXBOND * (k + j)]]),&(rec_y_d[bondsr_d[MAXBOND * (k + j)]]),&(rec_z_d[bondsr_d[MAXBOND * (k + j)]]),&proteina_x[j], &proteina_y[j], &proteina_z[j], w);
								type_data cosT = cosTheta (v, w);
                                                		if ( cosT < 0.0 ){
                                                        		//if (ind1 == OXI || ind1 == AZU) cosT = pow(cosT,4);
                                                        		//if ((ind1 == NIT2)) cosT = pow(cosT,2);
                                                        		type_data sinT = sqrtf(1.0-cosT*cosT);
									//e_hbond = sinT*((cC / rC) - (cD / rD)) - cosT;
									e_hbond = sinT * ((cC / rC) - (cD / rD) + cosT);
                                                        		//e_hbond = sinT * ((cC / rC) - (cD / rD) + cosT);
                                                        		//printf("e_b %f\n",e_hbond);
                                                		}
                                                		// printf("e_b %f\n",e_hbond);
                                        		}
                                		}
                                		if ((temp_sqrt > 0) && (temp_sqrt < 8) &&  ind1 == HBOND){
                                         		//printf("Hola\n");
                                        		if (ind2 == OXI || ind2 == NIT2 || ind2 == AZU){
                                                		//printf("Hola\n");
								sub(conformations_x_d + id_atom + (bonds_d[id_conformation*nlig*MAXBOND + MAXBOND*i]),conformations_y_d + id_atom + (bonds_d[id_conformation*nlig*MAXBOND + MAXBOND*i]),conformations_z_d + id_atom + (bonds_d[id_conformation*nlig*MAXBOND + MAXBOND*i]),&ligx,&ligy,&ligz,v);		
								sub(&proteina_x[j], &proteina_y[j], &proteina_z[j], &ligx,&ligy,&ligz, w);
								type_data cosT = cosTheta (v, w);
                                                                if ( cosT < 0.0 ){
                                                                        //if (ind1 == OXI || ind1 == AZU) cosT = pow(cosT,4);
                                                                        //if ((ind1 == NIT2)) cosT = pow(cosT,2);
                                                                        type_data sinT = sqrtf(1.0-cosT*cosT);
                                                                        //e_hbond = sinT*((cC / rC) - (cD / rD)) - cosT;
									 e_hbond = sinT * ((cC / rC) - (cD / rD) + cosT);
								}
                                        		}
                                		}
                                		tmp_e_es += e_es;
                                		tmp_e_vdw += e_vdw;
                                		tmp_e_hbond += e_hbond;
                                		tmp_e_solv += e_solv;

                         		}
				}

                        	temp_es += (tmp_e_es * ql_d[id_atom + i]);
                        	temp_vdw += tmp_e_vdw;
				//if ((id_conformation==0) && (lane_id==0)) printf("temp_vdw %f\n",temp_vdw);
                        	temp_hbond += tmp_e_hbond;
                        	temp_solv += (tmp_e_solv * fabs(ql_d[id_atom + i]) );
				//temp_solv = 0;
                        	tmp_e_es=0;
                        	tmp_e_vdw=0;
                        	tmp_e_hbond=0;
                        	tmp_e_solv=0;
                		
                        }
                }
                __syncthreads();

        }
	 //__syncthreads();
        //if ((lane_id == 0) && (id_conformation == 1)) printf("temp_vdw %f quat_x %f quat_y %f move_x %f move_y %f move_z %f\n",temp_vdw,qaux_x,qaux_y,mx,my,mz);
        temp_total =   (temp_es) + (temp_hbond*weights_c[1]) + (temp_vdw*weights_c[0]) + (temp_solv);
	//if ((id_conformation==0)&&(lane_id==0)) printf("temp_total %f\n",temp_total);
	//CODIGO
	
	__syncthreads();
	if (gid < max)
	{
		#if  __CUDA_ARCH__ >= 350
			reduction_shfl(&temp_total,WARP_SIZE,warp_id);	
			if (lane_id == 0) { energy_d[id_conformation] = temp_total + (nlinks[nconformation[gid / WARP_SIZE]/conf_point]*weights_c[4]);}//printf("nlinks %d %f\n",nlinks[nconformation[gid / WARP_SIZE]/conf_point],temp_total);}
			//if ((id_conformation == 0) && (lane_id == 0)) printf(" 0 %f %f\n",temp_total, energy_d[id_conformation]);
		#else
			partial_energy[tid] = temp_total;		
			
			__syncthreads();
			
			 //Reduccion: suma de las energias de los atomos que pertenecen al mismo ligando
			for(unsigned int s=WARP_SIZE/2; s>0; s>>=1) {
					if (tid < s + warp_id*WARP_SIZE) {
							partial_energy[tid] += partial_energy[tid + s];
					}
					__syncthreads();
			}
	
			__syncthreads();
	
			if (tid % WARP_SIZE == 0){	
				energy_d[id_conformation] = partial_energy[tid] + (nlinks[nconformation[gid / WARP_SIZE]/conf_point]*weights_c[4]);	
									
			}
		#endif
	}
}

__global__ void Gpu_full_Kernel_Conformations_vs_by_warp_type1 (unsigned int *nlinks, unsigned int nrec, unsigned int conf_point, unsigned int *atom, unsigned int nlig, type_data *rec_x_d, type_data *rec_y_d, type_data *rec_z_d, type_data *qr_d, char *rectype_d, unsigned int *bondsr_d, type_data *conformations_x_d, type_data *conformations_y_d, type_data *conformations_z_d, type_data *move_x_d, type_data *move_y_d, type_data *move_z_d, type_data *quat_d_x, type_data *quat_d_y, type_data *quat_d_z, type_data *quat_d_w, type_data *ql_d, char *ligtype_d, unsigned int *bonds_d, type_data *energy_d, unsigned int *nconformation, unsigned int stride, unsigned int tam, unsigned long int max)
{
	extern __shared__ type_data data[];
	type_data *proteina_x = (type_data *)data;
	type_data *proteina_y = (type_data *)&proteina_x[tam];
	type_data *proteina_z = (type_data *)&proteina_y[tam];
	type_data *proteina_qr = (type_data *)&proteina_z[tam];
	type_data *partial_energy = (type_data *)&proteina_qr[tam];
	char *proteina_type = (char *)&partial_energy[tam];	
		
	type_data temp_vdw, temp_es, temp_hbond, temp_solv, temp_total, e_vdw, e_hbond, e_es, e_solv, pre_calc, lig_map;	
	type_data ligx,ligy,ligz,distx,disty,distz,mx,my,mz,temp_sqrt;
	type_data eps, sig, epslig, siglig, term6,term12;
	type_data miatomo[3],v[3],w[3];
	type_data qaux_x, qaux_y, qaux_z, qaux_w;
	type_data solv_asp_1, solv_asp_2, solv_vol_1, solv_vol_2,solv_qasp_1,solv_qasp_2,tmp_e_solv,temp_solv_total;
    	type_data minus_inv_two_sigma_sqd = -0.5L / (sigma_autodock * sigma_autodock);
    	type_data exponencial;
	int ind1,ind2;		
	unsigned int tid = threadIdx.x + threadIdx.y * blockDim.x;
	unsigned int blockID = blockIdx.x + blockIdx.y * gridDim.x;
	unsigned long int gid = blockID * (blockDim.x * blockDim.y) + (threadIdx.y*blockDim.x) + threadIdx.x;
	unsigned long int id_atom;
	unsigned long int id_conformation;
	
	int lane_id = tid & (WARP_SIZE-1);
	int warp_id = tid>>5;
	if (gid < max)
	{
		id_conformation = nconformation[gid / WARP_SIZE] - stride;
		//if (id_conformation == 241448) printf ("stride_globla: %d stride %d file %d \n",nconformation[gid / WARP_SIZE],stride,nconformation[gid / WARP_SIZE]/conf_point);
		id_atom = id_conformation * nlig;
	//rotation
		qaux_x = quat_d_x[id_conformation];			
		qaux_y = quat_d_y[id_conformation];			
		qaux_z = quat_d_z[id_conformation];			
		qaux_w = quat_d_w[id_conformation];			
	//traslation
		mx = move_x_d[id_conformation];
		my = move_y_d[id_conformation];
		mz = move_z_d[id_conformation];
				
	}	 	
	temp_vdw = 0;
	temp_es = 0;
	temp_hbond = 0;
	temp_solv = 0;
	temp_solv_total = 0;
	//if (id_conformation == 128)printf("max %d\n",max);	
	for (unsigned int k = 0; k < nrec;k+=tam)
	{
		proteina_x[tid] = rec_x_d[k + tid];
		proteina_y[tid] = rec_y_d[k + tid];
		proteina_z[tid] = rec_z_d[k + tid];
		proteina_qr[tid] = qr_d[k + tid];
		proteina_type[tid] = rectype_d[k + tid];		
		__syncthreads();
		
		if (gid < max)
		{		
			for (unsigned int i=lane_id;i<atom[nconformation[gid / WARP_SIZE]/conf_point];i+=WARP_SIZE)
			{
				rotate3DPoint (&qaux_x,&qaux_y,&qaux_z,&qaux_w,(conformations_x_d + id_atom + i), (conformations_y_d + id_atom + i), (conformations_z_d + id_atom + i), miatomo);
				ligx = miatomo[0] + mx;
				ligy = miatomo[1] + my;
				ligz = miatomo[2] + mz;
			
				//if ((id_conformation==0)&&(lane_id==0)) printf("miatomo[0] %f miatomo[1] %f miatomo[2] %f\n",miatomo[0],miatomo[1],miatomo[2]);
				ind1 = ligtype_d[id_atom + i];
				solv_asp_1 = f_params_c[ind1].asp;
                		solv_vol_1 = f_params_c[ind1].vol;
				//epslig = vdw_params_c [ind1].epsilon;
	    			//siglig = vdw_params_c [ind1].sigma;
				epslig = f_params_c[ind1].epsilon;
				siglig = f_params_c[ind1].sigma;
				//if (id_conformation == 0) printf("epslig %f siglig %f %d %d\n",vdw_params_c[ind1].epsilon,vdw_params_c[ind1].sigma,ind1, atom[id_conformation/conf_point]);					
				for (unsigned int j = 0; j < tam; j++)
				{
					if ((k+j) < nrec)
					{	
						e_es = 0;
						e_vdw = 0;
						e_hbond = 0;	
						e_solv = 0;
					
						distx = proteina_x[j] - ligx;
						disty = proteina_y[j] - ligy;
						distz = proteina_z[j] - ligz;
						distx = distx*distx + disty*disty + distz*distz;
						temp_sqrt =  sqrtf(distx);
					
						ind2 = proteina_type[j];
						solv_asp_2 = f_params_c[ind2].asp;
						solv_vol_2 = f_params_c[ind2].vol;						
						//eps = sqrtf(vdw_params_c[ind2].epsilon * epslig);
						//sig = vdw_params_c[ind2].sigma * siglig;
						eps =  sqrtf(f_params_c[ind2].epsilon * epslig);
						sig = f_params_c[ind2].sigma * siglig;
						
						term6 = sig / distx;
						term6 = term6 * term6 * term6; //^3				
				
						e_vdw = 4.0 * eps * term6 * (term6 - 1.0);
						e_es = proteina_qr[j] * (1/temp_sqrt) * ql_d[id_atom+i];
						//if (id_conformation == 0) printf("temp_vdw %f	temp_es %f\n",temp_vdw,temp_es);	
						pre_calc = ((solv_asp_1 * solv_vol_2)  + ((solv_asp_2 + qsolpar * fabs(qr_d[j])) * solv_vol_1)) * exponencial;
						lig_map = qsolpar * solv_vol_2 * exponencial;
						e_solv = pre_calc + lig_map;
						
						if (temp_sqrt > 0.18 && temp_sqrt < 0.39 && (ind1 == OXI || ind1 == NIT2 || ind1 == AZU)){
							if (ind2 == HBOND){
								sub(&ligx,&ligy,&ligz, &proteina_x[j], &proteina_y[j], &proteina_z[j], v);
								sub(&(rec_x_d[bondsr_d[MAXBOND * (k + j)]]),&(rec_y_d[bondsr_d[MAXBOND * (k + j)]]),&(rec_z_d[bondsr_d[MAXBOND * (k + j)]]),&proteina_x[j], &proteina_y[j], &proteina_z[j], w);
								type_data cosT = cosTheta (v, w);

								if ( cosT < 0.0 ){
									type_data sinT = sqrtf(1.0-cosT*cosT);
									e_vdw *= sinT;
									disty = distx *distx *distx *distx *distx ;
									//e_hbond = -cosT * ( hbond_params_c[ind1].c12 / (disty*distx)  -  hbond_params_c[ind1].c10 / disty) ;
									e_hbond = -cosT * ( f_params_c[ind1].epsilon_h / (disty*distx)  -  f_params_c[ind1].sigma_h / disty) ;
								}
							}
						}
						if (temp_sqrt > 0.18 && temp_sqrt < 0.39 &&  ind1 == HBOND){
							if (ind2 == OXI || ind2 == NIT2 || ind2 == AZU){	
								
								sub(conformations_x_d + id_atom + (bonds_d[id_conformation*nlig*MAXBOND + MAXBOND*i]),conformations_y_d + id_atom + (bonds_d[id_conformation*nlig*MAXBOND + MAXBOND*i]),conformations_z_d + id_atom + (bonds_d[id_conformation*nlig*MAXBOND + MAXBOND*i]),&ligx,&ligy,&ligz,v);		
								sub(&proteina_x[j], &proteina_y[j], &proteina_z[j], &ligx,&ligy,&ligz, w);
								type_data cosT = cosTheta (v, w);
								if ( cosT < 0.0 ){
									type_data sinT = sqrtf(1.0-cosT*cosT);
									e_vdw *= sinT;
									disty = distx *distx *distx *distx *distx;
									//e_hbond = -cosT * (hbond_params_c[ind2].c12 / (disty*distx) - hbond_params_c[ind2].c10 / disty );
									e_hbond = -cosT * (f_params_c[ind2].epsilon_h / (disty*distx) - f_params_c[ind2].sigma_h / disty );
												
								}
							}
						}
						temp_es += e_es;
						temp_vdw += e_vdw;
						temp_hbond += e_hbond;
						temp_solv += e_solv;
					}					
				}
				tmp_e_solv =  temp_solv * fabs(ql_d[id_atom + i]);	
				temp_solv_total += tmp_e_solv;	
				tmp_e_solv = 0;
				temp_solv = 0;
			}
		}
		__syncthreads();	
	}
	
	temp_total = ((temp_es*ES_CONSTANT)*weights_c[2]) + (temp_vdw*weights_c[0]) + (temp_hbond*weights_c[1]) + (temp_solv_total);
	//if (id_conformation == 0) printf("temp_total %f\n",temp_total);
	
	__syncthreads();
	if (gid < max)
	{	
		#if  __CUDA_ARCH__ >= 350
			reduction_shfl(&temp_total,WARP_SIZE,warp_id);	
			if (lane_id == 0) energy_d[id_conformation] = temp_total + (nlinks[nconformation[gid / WARP_SIZE]/conf_point] * weights_c[4]);	
			//if ((id_conformation == 0) && (lane_id == 0)) printf("id %d total %f\n",id_conformation,temp_total);	
		#else
			partial_energy[tid] = temp_total;		
			
			__syncthreads();
			
			 //Reduccion: suma de las energias de los atomos que pertenecen al mismo ligando
			for(unsigned int s=WARP_SIZE/2; s>0; s>>=1) {
					if (tid < s + warp_id*WARP_SIZE) {
							partial_energy[tid] += partial_energy[tid + s];
					}
					__syncthreads();
				}
	
			__syncthreads();
	
			if (tid % WARP_SIZE == 0){	
				energy_d[id_conformation] = partial_energy[tid] + (nlinks[nconformation[gid / WARP_SIZE]/conf_point] * weights_c[4]);	
									
			}
		#endif
	}
}
					

__global__ void Gpu_full_Kernel_Conformations_vs_by_warp (unsigned int nrec, unsigned int conf_point, unsigned int *atom, unsigned int nlig, type_data *rec_x_d, type_data *rec_y_d, type_data *rec_z_d, type_data *qr_d, char *rectype_d, unsigned int *bondsr_d, type_data *conformations_x_d, type_data *conformations_y_d, type_data *conformations_z_d, type_data *move_x_d, type_data *move_y_d, type_data *move_z_d, type_data *quat_d_x, type_data *quat_d_y, type_data *quat_d_z, type_data *quat_d_w, type_data *ql_d, char *ligtype_d, unsigned int *bonds_d, type_data *energy_d, unsigned int *nconformation, unsigned int stride, unsigned int tam, unsigned long int max)
{
	extern __shared__ type_data data[];
	type_data *proteina_x = (type_data *)data;
	type_data *proteina_y = (type_data *)&proteina_x[tam];
	type_data *proteina_z = (type_data *)&proteina_y[tam];
	type_data *proteina_qr = (type_data *)&proteina_z[tam];
	type_data *partial_energy = (type_data *)&proteina_qr[tam];
	char *proteina_type = (char *)&partial_energy[tam];	
		
	type_data temp_vdw, temp_es, temp_hbond, temp_total, e_vdw, e_hbond, e_es;	
	type_data ligx,ligy,ligz,distx,disty,distz,mx,my,mz,temp_sqrt;
	type_data eps, sig, epslig, siglig, term6,term12;
	type_data miatomo[3],v[3],w[3];
	type_data qaux_x, qaux_y, qaux_z, qaux_w;
	int ind1,ind2;		
	unsigned int tid = threadIdx.x + threadIdx.y * blockDim.x;
	unsigned int blockID = blockIdx.x + blockIdx.y * gridDim.x;
	unsigned long int gid = blockID * (blockDim.x * blockDim.y) + (threadIdx.y*blockDim.x) + threadIdx.x;
	unsigned long int id_atom;
	unsigned long int id_conformation;
	
	int lane_id = tid & (WARP_SIZE-1);
	int warp_id = tid>>5;
	if (gid < max)
	{
		id_conformation = nconformation[gid / WARP_SIZE] - stride;
		//if (id_conformation == 241448) printf ("stride_globla: %d stride %d file %d \n",nconformation[gid / WARP_SIZE],stride,nconformation[gid / WARP_SIZE]/conf_point);
		id_atom = id_conformation * nlig;
	//rotation
		qaux_x = quat_d_x[id_conformation];			
		qaux_y = quat_d_y[id_conformation];			
		qaux_z = quat_d_z[id_conformation];			
		qaux_w = quat_d_w[id_conformation];			
	//traslation
		mx = move_x_d[id_conformation];
		my = move_y_d[id_conformation];
		mz = move_z_d[id_conformation];
				
	}	 	
	temp_vdw = 0;
	temp_es = 0;
	temp_hbond = 0;
	//if (id_conformation == 128)printf("max %d\n",max);			
	for (unsigned int k = 0; k < nrec;k+=tam)
	{
		proteina_x[tid] = rec_x_d[k + tid];
		proteina_y[tid] = rec_y_d[k + tid];
		proteina_z[tid] = rec_z_d[k + tid];
		proteina_qr[tid] = qr_d[k + tid];
		proteina_type[tid] = rectype_d[k + tid];		
		__syncthreads();
		
		if (gid < max)
		{		
			for (unsigned int i=lane_id;i<atom[nconformation[gid / WARP_SIZE]/conf_point];i+=WARP_SIZE)
			{
				rotate3DPoint (&qaux_x,&qaux_y,&qaux_z,&qaux_w,(conformations_x_d + id_atom + i), (conformations_y_d + id_atom + i), (conformations_z_d + id_atom + i), miatomo);
				ligx = miatomo[0] + mx;
				ligy = miatomo[1] + my;
				ligz = miatomo[2] + mz;
			
				ind1 = ligtype_d[id_atom + i];
				//epslig = vdw_params_c [ind1].epsilon;
	    			//siglig = vdw_params_c [ind1].sigma;
				epslig = f_params_c[ind1].epsilon;
				siglig = f_params_c[ind1].sigma;
				//if (id_conformation == 0) printf("epslig %f siglig %f %d %d\n",vdw_params_c[ind1].epsilon,vdw_params_c[ind1].sigma,ind1, atom[id_conformation/conf_point]);					
				for (unsigned int j = 0; j < tam; j++)
				{
					if ((k+j) < nrec)
					{	
						e_es = 0;
						e_vdw = 0;
						e_hbond = 0;	
					
						distx = proteina_x[j] - ligx;
						disty = proteina_y[j] - ligy;
						distz = proteina_z[j] - ligz;
						distx = distx*distx + disty*disty + distz*distz;
						temp_sqrt =  sqrtf(distx);
					
						ind2 = proteina_type[j];						
						//eps = sqrtf(vdw_params_c[ind2].epsilon * epslig);
						//sig = vdw_params_c[ind2].sigma * siglig;
						eps =  sqrtf(f_params_c[ind2].epsilon * epslig);
						sig = f_params_c[ind2].sigma * siglig;
						
						term6 = sig / distx;
						term6 = term6 * term6 * term6; //^3				
				
						e_vdw = 4.0 * eps * term6 * (term6 - 1.0);
						e_es = proteina_qr[j] * (1/temp_sqrt) * ql_d[id_atom+i];
						//if (id_conformation == 0) printf("temp_vdw %f	temp_es %f\n",temp_vdw,temp_es);					
						
						if (temp_sqrt > 0.18 && temp_sqrt < 0.39 && (ind1 == OXI || ind1 == NIT2 || ind1 == AZU)){
							if (ind2 == HBOND){
								sub(&ligx,&ligy,&ligz, &proteina_x[j], &proteina_y[j], &proteina_z[j], v);
								sub(&(rec_x_d[bondsr_d[MAXBOND * (k + j)]]),&(rec_y_d[bondsr_d[MAXBOND * (k + j)]]),&(rec_z_d[bondsr_d[MAXBOND * (k + j)]]),&proteina_x[j], &proteina_y[j], &proteina_z[j], w);
								type_data cosT = cosTheta (v, w);

								if ( cosT < 0.0 ){
									type_data sinT = sqrtf(1.0-cosT*cosT);
									e_vdw *= sinT;
									disty = distx *distx *distx *distx *distx ;
									//e_hbond = -cosT * ( hbond_params_c[ind1].c12 / (disty*distx)  -  hbond_params_c[ind1].c10 / disty) ;
									e_hbond = -cosT * ( f_params_c[ind1].epsilon_h / (disty*distx)  -  f_params_c[ind1].sigma_h / disty) ;
								}
							}
						}
						if (temp_sqrt > 0.18 && temp_sqrt < 0.39 &&  ind1 == HBOND){
							if (ind2 == OXI || ind2 == NIT2 || ind2 == AZU){	
								
								sub(conformations_x_d + id_atom + (bonds_d[id_conformation*nlig*MAXBOND + MAXBOND*i]),conformations_y_d + id_atom + (bonds_d[id_conformation*nlig*MAXBOND + MAXBOND*i]),conformations_z_d + id_atom + (bonds_d[id_conformation*nlig*MAXBOND + MAXBOND*i]),&ligx,&ligy,&ligz,v);		
								sub(&proteina_x[j], &proteina_y[j], &proteina_z[j], &ligx,&ligy,&ligz, w);
								type_data cosT = cosTheta (v, w);
								if ( cosT < 0.0 ){
									type_data sinT = sqrtf(1.0-cosT*cosT);
									e_vdw *= sinT;
									disty = distx *distx *distx *distx *distx;
									//e_hbond = -cosT * (hbond_params_c[ind2].c12 / (disty*distx) - hbond_params_c[ind2].c10 / disty );
									e_hbond = -cosT * (f_params_c[ind2].epsilon_h / (disty*distx) - f_params_c[ind2].sigma_h / disty );
												
								}
							}
						}
						temp_es += e_es;
						temp_vdw += e_vdw;
						temp_hbond += e_hbond;
					}
				}				
			}
		}
		__syncthreads();	
	}
	
	temp_total = ((temp_es*ES_CONSTANT)*weights_c[2]) + (temp_vdw*weights_c[0]) + (temp_hbond*weights_c[1]);
	//if (id_conformation == 0) printf("temp_total %f\n",temp_total);
	__syncthreads();
	if (gid < max)
	{	
		#if  __CUDA_ARCH__ >= 350
			reduction_shfl(&temp_total,WARP_SIZE,warp_id);	
			if (lane_id == 0) energy_d[id_conformation] = temp_total;	
			//if ((id_conformation == 0) && (lane_id == 0)) printf("id %d total %f\n",id_conformation,temp_total);	
		#else
			partial_energy[tid] = temp_total;		
			
			__syncthreads();
			
			 //Reduccion: suma de las energias de los atomos que pertenecen al mismo ligando
			for(unsigned int s=WARP_SIZE/2; s>0; s>>=1) {
					if (tid < s + warp_id*WARP_SIZE) {
							partial_energy[tid] += partial_energy[tid + s];
					}
					__syncthreads();
				}
	
			__syncthreads();
	
			if (tid % WARP_SIZE == 0){	
				energy_d[id_conformation] = partial_energy[tid];	
									
			}
		#endif
	}
	
}

__global__ void Gpu_full_Kernel_Conformations_by_warp_type2 (unsigned int nrec, unsigned int natom, unsigned int nlig, type_data *rec_x_d, type_data *rec_y_d, type_data *rec_z_d, type_data *qr_d, char *rectype_d, unsigned int *bondsr_d, type_data *move_x_d, type_data *move_y_d, type_data *move_z_d, type_data *quat_d_x, type_data *quat_d_y, type_data *quat_d_z, type_data *quat_d_w, type_data *lig_x_d, type_data *lig_y_d, type_data *lig_z_d, type_data *ql_d, char *ligtype_d, unsigned int *bonds_d, type_data *energy_d, unsigned int *nconformation, unsigned int stride, unsigned int tam, unsigned long int max)
{
	extern __shared__ type_data data[];
	type_data *proteina_x = (type_data *)data;
	type_data *proteina_y = (type_data *)&proteina_x[tam];
	type_data *proteina_z = (type_data *)&proteina_y[tam];
        type_data *proteina_qr = (type_data *)&proteina_z[tam];
        type_data *partial_energy = (type_data *)&proteina_qr[tam];
        char *proteina_type = (char *)&partial_energy[tam];

        type_data temp_vdw, temp_es, temp_hbond, temp_solv, e_es, e_vdw, e_hbond, e_solv, tmp_e_es=0,tmp_e_vdw=0,tmp_e_solv=0,tmp_e_hbond=0;
        type_data temp_total;
	type_data tmpconst,tmpconst_h,cA,cB,cC,cD,eps_hb,sig_hb,rA,rB,rC,rD;
        type_data ligx,ligy,ligz,distx,disty,distz,mx,my,mz,temp_sqrt;
        type_data eps, sig, epslig, siglig, term6,term12;
        type_data miatomo[3],v[3],w[3];
        
	type_data qaux_x,qaux_y,qaux_z,qaux_w;
        type_data solv_asp_1, solv_asp_2, solv_vol_1, solv_vol_2,solv_qasp_1,solv_qasp_2,pre_calc,lig_map;
     
        type_data minus_inv_two_sigma_sqd = -0.5L / (sigma_autodock * sigma_autodock);
        type_data exponencial;
        unsigned int ind1,ind2;
        unsigned int tid = threadIdx.x + threadIdx.y * blockDim.x;
        unsigned int blockID = blockIdx.x + blockIdx.y * gridDim.x;
        unsigned int long gid = blockID * (blockDim.x * blockDim.y) + (threadIdx.y*blockDim.x) + threadIdx.x;
        unsigned int long id_conformation;

        int lane_id = tid & (WARP_SIZE-1);
        int warp_id = tid>>5;

	if (gid < max)
        {
                id_conformation = nconformation[gid / WARP_SIZE] - stride;
                //if (id_conformation == 1) printf("id_conf %d tid %d\n",id_conformation,tid);
                //if (id_conformation > 449695) printf("MAYOR %d\n",id_conformation);
                qaux_x = quat_d_x[id_conformation];
                qaux_y = quat_d_y[id_conformation];
                qaux_z = quat_d_z[id_conformation];
                qaux_w = quat_d_w[id_conformation];
        //traslation
                mx = move_x_d[id_conformation];
                my = move_y_d[id_conformation];
                mz = move_z_d[id_conformation];
                //if (id_conformation == 449695) printf("%f %f %f %f %f %f %f\n",mx,my,mz,qaux_x,qaux_y,qaux_z,qaux_w);
        }

        temp_vdw = 0;
        temp_es = 0;
        temp_hbond = 0;
        temp_solv = 0;

         for (unsigned int k = 0; k < nrec;k+=tam)
        {

                proteina_x[tid] = rec_x_d[k + tid];
                proteina_y[tid] = rec_y_d[k + tid];
                proteina_z[tid] = rec_z_d[k + tid];
                proteina_qr[tid] = qr_d[k + tid];
                proteina_type[tid] = rectype_d[k + tid];

                __syncthreads();

                if (gid < max)
                {
                        //if (id_conformation > 211582) printf("id_conf %d\n",nconformation[gid/WARP_SIZE]);	
			for (unsigned int i=lane_id;i<natom;i+=WARP_SIZE)
                        {
                                rotate3DPoint (&qaux_x,&qaux_y,&qaux_z,&qaux_w,(lig_x_d + i), (lig_y_d + i), (lig_z_d + i), miatomo);
                                ligx = miatomo[0] + mx;
                                ligy = miatomo[1] + my;
                                ligz = miatomo[2] + mz;

                                ind1 = ligtype_d[i];
                                solv_asp_1 = f_params_c[ind1].asp;
                                solv_vol_1 = f_params_c[ind1].vol;
                                //epslig = vdw_params_c [ind1].epsilon;
                                //siglig = vdw_params_c [ind1].sigma;
                                epslig = f_params_c [ind1].epsilon;
                                siglig = f_params_c [ind1].sigma;

                                for (unsigned int j = 0; j < tam; j++)
                                {
                                        if ((k + j) < nrec)
                                        {
                                                e_es = 0;
                                                e_vdw = 0;
                                                e_hbond = 0;
                                                e_solv = 0;
                                                distx = ligx - proteina_x[j];
                                                disty = ligy - proteina_y[j];
                                                distz = ligz - proteina_z[j];
                                                distx = distx*distx + disty*disty + distz*distz;
                                                temp_sqrt =  sqrtf(distx);

                                                exponencial = expf(minus_inv_two_sigma_sqd * distx) * weights_c[3];

                                                ind2 = proteina_type[j];
                                                solv_asp_2 = f_params_c[ind2].asp;
                                                solv_vol_2 = f_params_c[ind2].vol;
                                                //eps = sqrtf(vdw_params_c[ind2].epsilon * epslig);
                                                //sig = vdw_params_c[ind2].sigma * siglig;
						type_data dato_eij = calc_ddd_Mehler_Solmajer_GPU(temp_sqrt,APPROX_ZERO);

                                                eps =  sqrtf(f_params_c[ind2].epsilon * epslig);
                                                sig = ((f_params_c[ind2].sigma + siglig))*0.5L;
                                		eps_hb =  (f_params_c[ind1].epsilon_h +  f_params_c[ind2].epsilon_h);//  * a_scoring.p_hbond;
                                		sig_hb = ((f_params_c[ind1].sigma_h + f_params_c[ind2].sigma_h));//*0.5L;
						
						tmpconst = eps / (xA - xB);
                                		tmpconst_h = eps_hb / (xC - xD);

                                		cA =  tmpconst * powf( sig, xA ) * xB;
                                		cB =  tmpconst * powf( sig, xB ) * xA;

                                		cC =  tmpconst_h * powf( sig_hb, xC ) * xD;
                                		cD =  tmpconst_h * powf( sig_hb, xD ) * xC;

                                		//dxA = (double) xA;
                                		//dxB = (double) xB;
                                		//dxC = (double) xC;
                                		//dxD = (double) xD;

                                		rA = powf( temp_sqrt, xA);
                                		rB = powf( temp_sqrt, xB);
                                		rC = powf( temp_sqrt, xC);
                                		rD = powf( temp_sqrt, xD);

                                		term6 = cB / rB;//pow((double)dist, (double)xB);
                                		term12 = cA / rA;//pow((double)dist, (double)xA);

                                		e_es = (1.0 / (dato_eij * temp_sqrt)) * proteina_qr[j] * ELECSCALE * weights_c[2];
                                		//if (dist < 7.0)
                                		e_vdw = term12 - term6;

                                		if (temp_sqrt < 8)
                                		{
                                        		pre_calc = ((solv_asp_1 * solv_vol_2)  + ((solv_asp_2 + qsolpar * fabs(proteina_qr[j])) * solv_vol_1)) * exponencial;// * a_scoring.p_desolv;
                                        		lig_map = qsolpar * solv_vol_2 * exponencial;
                                         		e_solv = pre_calc + lig_map;
                                		}
						if ((temp_sqrt > 0) && (temp_sqrt < 8) && (ind1 == OXI || ind1 == NIT2 || ind1 == AZU)){
                                        		if (ind2 == HBOND){
                                                		//printf("Hola\n");
                                                		sub(&ligx,&ligy,&ligz, &proteina_x[j], &proteina_y[j], &proteina_z[j], v);
                                                                sub(&(rec_x_d[bondsr_d[MAXBOND * (k + j)]]),&(rec_y_d[bondsr_d[MAXBOND * (k + j)]]),&(rec_z_d[bondsr_d[MAXBOND * (k + j)]]),&proteina_x[j], &proteina_y[j], &proteina_z[j], w);
                                                		type_data cosT = cosTheta (v, w);

                                                		if ( cosT < 0.0 ){
                                                        		type_data sinT = sqrtf(1.0-cosT*cosT);
                                                        		e_hbond = sinT * ((cC / rC) - (cD / rD) + cosT);
                                                        		//printf("e_b %f\n",e_hbond);
                                                		}
                                                		// printf("e_b %f\n",e_hbond);
                                        		}
                                		}
                                		if ((temp_sqrt > 0) && (temp_sqrt < 8) &&  ind1 == HBOND){
                                         		//printf("Hola\n");
                                        		if (ind2 == OXI || ind2 == NIT2 || ind2 == AZU){
                                                		//printf("Hola\n");
                                                		sub(lig_x_d + (bonds_d[MAXBOND*i]),lig_y_d + (bonds_d[MAXBOND*i]),lig_z_d + (bonds_d[MAXBOND*i]),&ligx,&ligy,&ligz,v);
                                                                sub(&proteina_x[j], &proteina_y[j], &proteina_z[j], &ligx,&ligy,&ligz, w);
                                                		type_data cosT = cosTheta (v, w);

                                                		if ( cosT < 0.0 ){
                                                        		type_data sinT = sqrtf(1.0-cosT*cosT);
                                                        		e_hbond += sinT * ((cC / rC) - (cD / rD) + cosT);
                                                        		//printf("e_b %f\n",e_hbond);
                                                		}
                                        		}
                                		}
                                		tmp_e_es += e_es;
                                		tmp_e_vdw += e_vdw;
                                		tmp_e_hbond += e_hbond;
                                		tmp_e_solv += e_solv;

                         		}


                        		temp_es += (tmp_e_es * ql_d[i]);
                        		temp_vdw += tmp_e_vdw;
                        		temp_hbond += tmp_e_hbond;
                        		temp_solv += (tmp_e_solv * fabs(ql_d[i]) );
                        		tmp_e_es=0;
                        		tmp_e_vdw=0;
                        		tmp_e_hbond=0;
                        		tmp_e_solv=0;
                		 }
                        }
                }
                __syncthreads();

        }
        //if ((lane_id == 0) && (id_conformation == 1)) printf("temp_vdw %f quat_x %f quat_y %f move_x %f move_y %f move_z %f\n",temp_vdw,qaux_x,qaux_y,mx,my,mz);
        temp_total = (temp_es) + (temp_hbond*weights_c[1]) + (temp_vdw*weights_c[0]) + (temp_solv);

	//CODIGO
	__syncthreads();
        if (gid < max)
        {
                #if  __CUDA_ARCH__ >= 350
                        reduction_shfl(&temp_total,WARP_SIZE,warp_id);
                        if (lane_id == 0) energy_d[id_conformation] = temp_total;
                //if ((lane_id == 0) && (temp_total > 2460.3) && (temp_total < 2460.6)) printf("Conf %d Lane %d. m_x %f, m_y %f, m_z %f, q_x %f, q_y %f, q_z %f, q_w %f,  temp_vdw %f temp_total %f\n",id_conformation,lane_id,mx,my,mz,qaux_x,qaux_y,qaux_z,qaux_w,temp_vdw, temp_total);
                #else
                        partial_energy[tid] = temp_total;

                        __syncthreads();

                        ///Reduccion: suma de las energias de los atomos que pertenecen al mismo ligando
                        for(unsigned int s=WARP_SIZE/2; s>0; s>>=1) {
                                        if (tid < s + warp_id*WARP_SIZE) {
                                                        partial_energy[tid] += partial_energy[tid + s];
                                        }
                                        __syncthreads();
                                }

                        __syncthreads();

                        if (tid % WARP_SIZE == 0){
                                energy_d[id_conformation] = partial_energy[tid];
                        }
                #endif
        }
	
}

__global__ void Gpu_full_Kernel_Conformations_by_warp_type1 (unsigned int nrec, unsigned int natom, unsigned int nlig, type_data *rec_x_d, type_data *rec_y_d, type_data *rec_z_d, type_data *qr_d, char *rectype_d, unsigned int *bondsr_d, type_data *move_x_d, type_data *move_y_d, type_data *move_z_d, type_data *quat_d_x, type_data *quat_d_y, type_data *quat_d_z, type_data *quat_d_w, type_data *lig_x_d, type_data *lig_y_d, type_data *lig_z_d, type_data *ql_d, char *ligtype_d, unsigned int *bonds_d, type_data *energy_d, unsigned int *nconformation, unsigned int stride, unsigned int tam, unsigned long int max)
{
        extern __shared__ type_data data[];
        type_data *proteina_x = (type_data *)data;
        type_data *proteina_y = (type_data *)&proteina_x[tam];
        type_data *proteina_z = (type_data *)&proteina_y[tam];
        type_data *proteina_qr = (type_data *)&proteina_z[tam];
        type_data *partial_energy = (type_data *)&proteina_qr[tam];
        char *proteina_type = (char *)&partial_energy[tam];

        type_data temp_vdw, temp_es, temp_hbond, temp_solv, e_es, e_vdw, e_hbond, e_solv;
        type_data temp_total;
        type_data ligx,ligy,ligz,distx,disty,distz,mx,my,mz,temp_sqrt;
        type_data eps, sig, epslig, siglig, term6,term12;
        type_data miatomo[3],v[3],w[3];
        
	type_data qaux_x,qaux_y,qaux_z,qaux_w;
        type_data solv_asp_1, solv_asp_2, solv_vol_1, solv_vol_2,solv_qasp_1,solv_qasp_2,pre_calc,lig_map,temp_solv_total,tmp_e_solv;
        
        type_data minus_inv_two_sigma_sqd = -0.5L / (sigma_autodock * sigma_autodock);
        type_data exponencial;
        unsigned int ind1,ind2;
        unsigned int tid = threadIdx.x + threadIdx.y * blockDim.x;
        unsigned int blockID = blockIdx.x + blockIdx.y * gridDim.x;
        unsigned int long gid = blockID * (blockDim.x * blockDim.y) + (threadIdx.y*blockDim.x) + threadIdx.x;
        unsigned int long id_conformation;

        int lane_id = tid & (WARP_SIZE-1);
        int warp_id = tid>>5;
        //partial_energy[tid]=0;
        if (gid < max)
        {
                id_conformation = nconformation[gid / WARP_SIZE] - stride;
                //if (id_conformation == 1) printf("id_conf %d tid %d\n",id_conformation,tid);
                //if (id_conformation > 449695) printf("MAYOR %d\n",id_conformation);
                qaux_x = quat_d_x[id_conformation];
                qaux_y = quat_d_y[id_conformation];
                qaux_z = quat_d_z[id_conformation];
                qaux_w = quat_d_w[id_conformation];
        //traslation
                mx = move_x_d[id_conformation];
                my = move_y_d[id_conformation];
                mz = move_z_d[id_conformation];
                //if (id_conformation == 449695) printf("%f %f %f %f %f %f %f\n",mx,my,mz,qaux_x,qaux_y,qaux_z,qaux_w);
        }

        temp_vdw = 0;
        temp_es = 0;
        temp_hbond = 0;
	temp_solv = 0;
	temp_solv_total = 0;
	
	 for (unsigned int k = 0; k < nrec;k+=tam)
        {

                proteina_x[tid] = rec_x_d[k + tid];
                proteina_y[tid] = rec_y_d[k + tid];
                proteina_z[tid] = rec_z_d[k + tid];
                proteina_qr[tid] = qr_d[k + tid];
                proteina_type[tid] = rectype_d[k + tid];

                __syncthreads();

                if (gid < max)
                {
                        //if (id_conformation > 211582) printf("id_conf %d\n",nconformation[gid/WARP_SIZE]);

                        for (unsigned int i=lane_id;i<natom;i+=WARP_SIZE)
                        {
                                rotate3DPoint (&qaux_x,&qaux_y,&qaux_z,&qaux_w,(lig_x_d + i), (lig_y_d + i), (lig_z_d + i), miatomo);
                                ligx = miatomo[0] + mx;
                                ligy = miatomo[1] + my;
                                ligz = miatomo[2] + mz;

                                ind1 = ligtype_d[i];
				solv_asp_1 = f_params_c[ind1].asp;
                        	solv_vol_1 = f_params_c[ind1].vol;
                                //epslig = vdw_params_c [ind1].epsilon;
                                //siglig = vdw_params_c [ind1].sigma;
                                epslig = f_params_c [ind1].epsilon;
                                siglig = f_params_c [ind2].sigma;

                                for (unsigned int j = 0; j < tam; j++)
                                {
                                        if ((k + j) < nrec)
                                        {
                                                e_es = 0;
                                                e_vdw = 0;
                                                e_hbond = 0;
												e_solv = 0;
                                                distx = ligx - proteina_x[j];
                                                disty = ligy - proteina_y[j];
                                                distz = ligz - proteina_z[j];
                                                distx = distx*distx + disty*disty + distz*distz;
                                                temp_sqrt =  sqrtf(distx);

						exponencial = expf(minus_inv_two_sigma_sqd * distx) * weights_c[3];

                                                ind2 = proteina_type[j];
						solv_asp_2 = f_params_c[ind2].asp;
                        			solv_vol_2 = f_params_c[ind2].vol;
                                                //eps = sqrtf(vdw_params_c[ind2].epsilon * epslig);
                                                //sig = vdw_params_c[ind2].sigma * siglig;
                                                eps =  sqrtf(f_params_c[ind2].epsilon * epslig);
                                                sig = f_params_c[ind2].sigma * siglig;
			
						term6 = sig / distx;
                                                term6 = term6 * term6 * term6; //^3

                                                e_vdw = 4.0 * eps * term6 * (term6 - 1.0);
                                                e_es = proteina_qr[j] * (1/temp_sqrt) * ql_d[i];
						pre_calc = ((solv_asp_1 * solv_vol_2)  + ((solv_asp_2 + qsolpar * fabs(qr_d[j])) * solv_vol_1)) * exponencial;
						lig_map = qsolpar * solv_vol_2 * exponencial;
						e_solv = pre_calc + lig_map;

                                                if (temp_sqrt > 0.18 && temp_sqrt < 0.39 && (ind1 == OXI || ind1 == NIT2 || ind1 == AZU)){
                                                        if (ind2 == HBOND){
                                                                sub(&ligx,&ligy,&ligz, &proteina_x[j], &proteina_y[j], &proteina_z[j], v);
                                                                sub(&(rec_x_d[bondsr_d[MAXBOND * (k + j)]]),&(rec_y_d[bondsr_d[MAXBOND * (k + j)]]),&(rec_z_d[bondsr_d[MAXBOND * (k + j)]]),&proteina_x[j], &proteina_y[j], &proteina_z[j], w);
                                                                type_data cosT = cosTheta (v, w);

                                                                if ( cosT < 0.0 ){
                                                                        type_data sinT = sqrtf(1.0-cosT*cosT);
                                                                        e_vdw *= sinT;
                                                                        disty = distx *distx *distx *distx *distx ;
                                                                        //e_hbond = -cosT * ( hbond_params_c[ind1].c12 / (disty*distx)  -  hbond_params_c[ind1].c10 / disty) ;
                                                                        e_hbond = -cosT * ( f_params_c[ind1].epsilon_h / (disty*distx)  -  f_params_c[ind1].sigma_h / disty) ;
                                                                        //if ((id_conformation==13) && (lane_id==12)) printf("mx %f, cosT %f, disty %f %f distx %f\n",mx,cosT,disty,distx);
                                                                }
                                                        }
                                                }
                                                if (temp_sqrt > 0.18 && temp_sqrt < 0.39 &&  ind1 == HBOND){
                                                        if (ind2 == OXI || ind2 == NIT2 || ind2 == AZU){
                                                                sub(lig_x_d + (bonds_d[MAXBOND*i]),lig_y_d + (bonds_d[MAXBOND*i]),lig_z_d + (bonds_d[MAXBOND*i]),&ligx,&ligy,&ligz,v);
                                                                sub(&proteina_x[j], &proteina_y[j], &proteina_z[j], &ligx,&ligy,&ligz, w);
                                                                type_data cosT = cosTheta (v, w);
                                                                if ( cosT < 0.0 ){
                                                                        type_data sinT = sqrtf(1.0-cosT*cosT);
                                                                        e_vdw *= sinT;
                                                                        disty = distx *distx *distx *distx *distx;
                                                                        //if ((id_conformation==2) && (lane_id ==12)) printf("mx %f, qaux_x %f, ligx %f, ligy %f, ligz %f, costT %f, disty %f, distx %f\n",mx,qaux_x,ligx,ligy,ligz,cosT,disty,distx);
                                                                        //e_hbond += -cosT * (hbond_params_c[ind2].c12 / (disty*distx) - hbond_params_c[ind2].c10 / disty );
                                                                        e_hbond += -cosT * (f_params_c[ind2].epsilon_h / (disty*distx) - f_params_c[ind2].sigma_h / disty );
                                                                }
                                                        }
                                                }
                                                temp_es += e_es;
                                                temp_vdw += e_vdw;
                                                temp_hbond += e_hbond;
						temp_solv += e_solv;
					}
					
                                        //if ((id_conformation == 1000) && (stride>0)) printf("Temp_es %f k %d nrec %d\n",temp_es,k,nrec);
                                }
				tmp_e_solv = temp_solv * fabs(ql_d[i]);
				temp_solv_total += tmp_e_solv;
				tmp_e_solv = 0;
				temp_solv = 0;
                        }
                }
                __syncthreads();

        }
        //if ((lane_id == 0) && (id_conformation == 1)) printf("temp_vdw %f quat_x %f quat_y %f move_x %f move_y %f move_z %f\n",temp_vdw,qaux_x,qaux_y,mx,my,mz);
        temp_total = (temp_es*ES_CONSTANT*weights_c[2]) + (temp_hbond*weights_c[1]) + (temp_vdw*weights_c[0]) + (temp_solv_total);
     
	__syncthreads();
        if (gid < max)
        {
                #if  __CUDA_ARCH__ >= 350
                        reduction_shfl(&temp_total,WARP_SIZE,warp_id);
                        if (lane_id == 0) energy_d[id_conformation] = temp_total;
                //if ((lane_id == 0) && (temp_total > 2460.3) && (temp_total < 2460.6)) printf("Conf %d Lane %d. m_x %f, m_y %f, m_z %f, q_x %f, q_y %f, q_z %f, q_w %f,  temp_vdw %f temp_total %f\n",id_conformation,lane_id,mx,my,mz,qaux_x,qaux_y,qaux_z,qaux_w,temp_vdw, temp_total);
                #else
                        partial_energy[tid] = temp_total;

                        __syncthreads();

                        ///Reduccion: suma de las energias de los atomos que pertenecen al mismo ligando
                        for(unsigned int s=WARP_SIZE/2; s>0; s>>=1) {
                                        if (tid < s + warp_id*WARP_SIZE) {
                                                        partial_energy[tid] += partial_energy[tid + s];
                                        }
                                        __syncthreads();
                                }

                        __syncthreads();

                        if (tid % WARP_SIZE == 0){
                                energy_d[id_conformation] = partial_energy[tid];
                        }
                #endif
        }

}

__global__ void Gpu_full_Kernel_Conformations_by_warp (unsigned int nrec, unsigned int natom, unsigned int nlig, type_data *rec_x_d, type_data *rec_y_d, type_data *rec_z_d, type_data *qr_d, char *rectype_d, unsigned int *bondsr_d, type_data *move_x_d, type_data *move_y_d, type_data *move_z_d, type_data *quat_d_x, type_data *quat_d_y, type_data *quat_d_z, type_data *quat_d_w, type_data *lig_x_d, type_data *lig_y_d, type_data *lig_z_d, type_data *ql_d, char *ligtype_d, unsigned int *bonds_d, type_data *energy_d, unsigned int *nconformation, unsigned int stride, unsigned int tam, unsigned long int max)
{
	extern __shared__ type_data data[];
	type_data *proteina_x = (type_data *)data;
	type_data *proteina_y = (type_data *)&proteina_x[tam];
	type_data *proteina_z = (type_data *)&proteina_y[tam];
	type_data *proteina_qr = (type_data *)&proteina_z[tam];
	type_data *partial_energy = (type_data *)&proteina_qr[tam];
	char *proteina_type = (char *)&partial_energy[tam];	

	type_data temp_vdw, temp_es, temp_hbond, e_es, e_vdw, e_hbond;
	type_data temp_total;	
	type_data ligx,ligy,ligz,distx,disty,distz,mx,my,mz,temp_sqrt;
	type_data eps, sig, epslig, siglig, term6,term12;
	type_data miatomo[3],v[3],w[3];
	type_data qaux_x, qaux_y, qaux_z, qaux_w;
	unsigned int ind1,ind2;		
	unsigned int tid = threadIdx.x + threadIdx.y * blockDim.x;
	unsigned int blockID = blockIdx.x + blockIdx.y * gridDim.x;
	unsigned int long gid = blockID * (blockDim.x * blockDim.y) + (threadIdx.y*blockDim.x) + threadIdx.x;
	unsigned int long id_conformation;
		
	int lane_id = tid & (WARP_SIZE-1);
	int warp_id = tid>>5;
	//partial_energy[tid]=0;	
	if (gid < max)
	{
		id_conformation = nconformation[gid / WARP_SIZE] - stride;
		//if (id_conformation == 1) printf("id_conf %d tid %d\n",id_conformation,tid);
		//if (id_conformation > 449695) printf("MAYOR %d\n",id_conformation);
		qaux_x = quat_d_x[id_conformation];			
		qaux_y = quat_d_y[id_conformation];			
		qaux_z = quat_d_z[id_conformation];			
		qaux_w = quat_d_w[id_conformation];			
	//traslation
		mx = move_x_d[id_conformation];
		my = move_y_d[id_conformation];
		mz = move_z_d[id_conformation];		
		//if (id_conformation == 449695) printf("%f %f %f %f %f %f %f\n",mx,my,mz,qaux_x,qaux_y,qaux_z,qaux_w);
	}
	
	temp_vdw = 0;
	temp_es = 0;
	temp_hbond = 0;
	//if (gid < max)
	//{
	for (unsigned int k = 0; k < nrec;k+=tam)
	{

		proteina_x[tid] = rec_x_d[k + tid];
		proteina_y[tid] = rec_y_d[k + tid];
		proteina_z[tid] = rec_z_d[k + tid];
		proteina_qr[tid] = qr_d[k + tid];
		proteina_type[tid] = rectype_d[k + tid];
	
		__syncthreads();
		
		if (gid < max)
		{	
			//if (id_conformation > 211582) printf("id_conf %d\n",nconformation[gid/WARP_SIZE]);
	
			for (unsigned int i=lane_id;i<natom;i+=WARP_SIZE)
			{
				rotate3DPoint (&qaux_x,&qaux_y,&qaux_z,&qaux_w,(lig_x_d + i), (lig_y_d + i), (lig_z_d + i), miatomo);
				ligx = miatomo[0] + mx;
				ligy = miatomo[1] + my;
				ligz = miatomo[2] + mz;
			
				ind1 = ligtype_d[i];
				//epslig = vdw_params_c [ind1].epsilon;
    				//siglig = vdw_params_c [ind1].sigma;
				epslig = f_params_c [ind1].epsilon;
				siglig = f_params_c [ind1].sigma;
									
				for (unsigned int j = 0; j < tam; j++)
				{	
					if ((k + j) < nrec)
					{
						e_es = 0;
						e_vdw = 0;
						e_hbond = 0;
						distx = ligx - proteina_x[j];
						disty = ligy - proteina_y[j];
						distz = ligz - proteina_z[j];
						distx = distx*distx + disty*disty + distz*distz;
						temp_sqrt =  sqrtf(distx);
					
						ind2 = proteina_type[j];
						
						//eps = sqrtf(vdw_params_c[ind2].epsilon * epslig);
						//sig = vdw_params_c[ind2].sigma * siglig;
						eps =  sqrtf(f_params_c[ind2].epsilon * epslig);
						sig = f_params_c[ind2].sigma * siglig;
						//if (id_conformation == 0)  printf("sig %f atomo %d\n",siglig,k+j);
						
						term6 = sig / distx;
						term6 = term6 * term6 * term6; //^3				
			
						e_vdw = 4.0 * eps * term6 * (term6 - 1.0);
						e_es = proteina_qr[j] * (1/temp_sqrt) * ql_d[i];					
						
						if (temp_sqrt > 0.18 && temp_sqrt < 0.39 && (ind1 == OXI || ind1 == NIT2 || ind1 == AZU)){
							if (ind2 == HBOND){
								sub(&ligx,&ligy,&ligz, &proteina_x[j], &proteina_y[j], &proteina_z[j], v);
								sub(&(rec_x_d[bondsr_d[MAXBOND * (k + j)]]),&(rec_y_d[bondsr_d[MAXBOND * (k + j)]]),&(rec_z_d[bondsr_d[MAXBOND * (k + j)]]),&proteina_x[j], &proteina_y[j], &proteina_z[j], w);
								type_data cosT = cosTheta (v, w);

								if ( cosT < 0.0 ){
									type_data sinT = sqrtf(1.0-cosT*cosT);
									e_vdw *= sinT;
									disty = distx *distx *distx *distx *distx ;					
									//e_hbond = -cosT * ( hbond_params_c[ind1].c12 / (disty*distx)  -  hbond_params_c[ind1].c10 / disty) ;
									e_hbond = -cosT * ( f_params_c[ind1].epsilon_h / (disty*distx)  -  f_params_c[ind1].sigma_h / disty) ;
									//if ((id_conformation==13) && (lane_id==12)) printf("mx %f, cosT %f, disty %f %f distx %f\n",mx,cosT,disty,distx);
								}
							}
						}
						if (temp_sqrt > 0.18 && temp_sqrt < 0.39 &&  ind1 == HBOND){
							if (ind2 == OXI || ind2 == NIT2 || ind2 == AZU){
								sub(lig_x_d + (bonds_d[MAXBOND*i]),lig_y_d + (bonds_d[MAXBOND*i]),lig_z_d + (bonds_d[MAXBOND*i]),&ligx,&ligy,&ligz,v);		
								sub(&proteina_x[j], &proteina_y[j], &proteina_z[j], &ligx,&ligy,&ligz, w);
								type_data cosT = cosTheta (v, w);
								if ( cosT < 0.0 ){
									type_data sinT = sqrtf(1.0-cosT*cosT);
									e_vdw *= sinT;
									disty = distx *distx *distx *distx *distx;
									//if ((id_conformation==2) && (lane_id ==12)) printf("mx %f, qaux_x %f, ligx %f, ligy %f, ligz %f, costT %f, disty %f, distx %f\n",mx,qaux_x,ligx,ligy,ligz,cosT,disty,distx);
									//e_hbond += -cosT * (hbond_params_c[ind2].c12 / (disty*distx) - hbond_params_c[ind2].c10 / disty );
									e_hbond += -cosT * (f_params_c[ind2].epsilon_h / (disty*distx) - f_params_c[ind2].sigma_h / disty );
								}
							}
						}
						temp_es += e_es;
						temp_vdw += e_vdw;
						temp_hbond += e_hbond; 
						
					}					
					//if ((id_conformation == 1000) && (stride>0)) printf("Temp_es %f k %d nrec %d\n",temp_es,k,nrec);
				}					
			}
		}
		__syncthreads();	
		
	}
	//if ((lane_id == 0) && (id_conformation == 1)) printf("temp_vdw %f quat_x %f quat_y %f move_x %f move_y %f move_z %f\n",temp_vdw,qaux_x,qaux_y,mx,my,mz);
	temp_total = (temp_es*ES_CONSTANT*weights_c[2]) + (temp_hbond*weights_c[1]) + (temp_vdw*weights_c[0]);
	__syncthreads();	
	if (gid < max)
	{
		#if  __CUDA_ARCH__ >= 350
			reduction_shfl(&temp_total,WARP_SIZE,warp_id);	
			if (lane_id == 0) energy_d[id_conformation] = temp_total;				
		//if ((lane_id == 0) && (temp_total > 2460.3) && (temp_total < 2460.6)) printf("Conf %d Lane %d. m_x %f, m_y %f, m_z %f, q_x %f, q_y %f, q_z %f, q_w %f,  temp_vdw %f temp_total %f\n",id_conformation,lane_id,mx,my,mz,qaux_x,qaux_y,qaux_z,qaux_w,temp_vdw, temp_total);
		#else
			partial_energy[tid] = temp_total;		
		
			__syncthreads();
			
			///Reduccion: suma de las energias de los atomos que pertenecen al mismo ligando
			for(unsigned int s=WARP_SIZE/2; s>0; s>>=1) {
					if (tid < s + warp_id*WARP_SIZE) {
							partial_energy[tid] += partial_energy[tid + s];
					}
					__syncthreads();
				}
	
			__syncthreads();
			
			if (tid % WARP_SIZE == 0){			
				energy_d[id_conformation] = partial_energy[tid];		
			}
		#endif
	}
	
}

__global__ void Gpu_full_Kernel_Conformations_flex_by_warp_type2 (unsigned int tor, unsigned int nrec, unsigned int natom, unsigned int nlig, type_data *rec_x_d, type_data *rec_y_d, type_data *rec_z_d, type_data *qr_d, char *rectype_d, unsigned int *bondsr_d, type_data *conformations_x_d, type_data *conformations_y_d, type_data *conformations_z_d, type_data *move_x_d, type_data *move_y_d, type_data *move_z_d, type_data *quat_d_x, type_data *quat_d_y, type_data *quat_d_z, type_data *quat_d_w, type_data *ql_d, char *ligtype_d, unsigned int *bonds_d, type_data *energy_d, unsigned int *nconformation, unsigned int stride, unsigned int tam, unsigned long int max)
{
	extern __shared__ type_data data[];
	type_data *proteina_x = (type_data *)data;
	type_data *proteina_y = (type_data *)&proteina_x[tam];
	type_data *proteina_z = (type_data *)&proteina_y[tam];
        type_data *proteina_qr = (type_data *)&proteina_z[tam];
        type_data *partial_energy = (type_data *)&proteina_qr[tam];
        char *proteina_type = (char *)&partial_energy[tam];

        type_data temp_vdw, temp_es, temp_hbond, temp_solv, e_es, e_vdw, e_hbond, e_solv, tmp_e_es=0,tmp_e_vdw=0,tmp_e_solv=0,tmp_e_hbond=0;
        type_data temp_total;
	type_data tmpconst,tmpconst_h,cA,cB,cC,cD,eps_hb,sig_hb,rA,rB,rC,rD;
        type_data ligx,ligy,ligz,distx,disty,distz,mx,my,mz,temp_sqrt;
        type_data eps, sig, epslig, siglig, term6,term12;
        type_data miatomo[3],v[3],w[3];
        
	type_data qaux_x,qaux_y,qaux_z,qaux_w;
        type_data solv_asp_1, solv_asp_2, solv_vol_1, solv_vol_2,solv_qasp_1,solv_qasp_2,pre_calc,lig_map;
     
        type_data minus_inv_two_sigma_sqd = -0.5L / (sigma_autodock * sigma_autodock);
        type_data exponencial;
        unsigned int ind1,ind2;
        unsigned int tid = threadIdx.x + threadIdx.y * blockDim.x;
        unsigned int blockID = blockIdx.x + blockIdx.y * gridDim.x;
        unsigned int long gid = blockID * (blockDim.x * blockDim.y) + (threadIdx.y*blockDim.x) + threadIdx.x;
        unsigned int long id_conformation,id_atom;

        int lane_id = tid & (WARP_SIZE-1);
        int warp_id = tid>>5;

	if (gid < max)
        {
                id_conformation = nconformation[gid / WARP_SIZE] - stride;
				id_atom = id_conformation * nlig;
                //if (id_conformation == 1) printf("id_conf %d tid %d\n",id_conformation,tid);
                //if (id_conformation > 449695) printf("MAYOR %d\n",id_conformation);
                qaux_x = quat_d_x[id_conformation];
                qaux_y = quat_d_y[id_conformation];
                qaux_z = quat_d_z[id_conformation];
                qaux_w = quat_d_w[id_conformation];
        //traslation
                mx = move_x_d[id_conformation];
                my = move_y_d[id_conformation];
                mz = move_z_d[id_conformation];
                //if (id_conformation == 449695) printf("%f %f %f %f %f %f %f\n",mx,my,mz,qaux_x,qaux_y,qaux_z,qaux_w);
        }

        temp_vdw = 0;
        temp_es = 0;
        temp_hbond = 0;
        temp_solv = 0;

         for (unsigned int k = 0; k < nrec;k+=tam)
        {

                proteina_x[tid] = rec_x_d[k + tid];
                proteina_y[tid] = rec_y_d[k + tid];
                proteina_z[tid] = rec_z_d[k + tid];
                proteina_qr[tid] = qr_d[k + tid];
                proteina_type[tid] = rectype_d[k + tid];

                __syncthreads();

                if (gid < max)
                {
                        //if (id_conformation > 211582) printf("id_conf %d\n",nconformation[gid/WARP_SIZE]);	
			for (unsigned int i=lane_id;i<natom;i+=WARP_SIZE)
                        {
                                rotate3DPoint (&qaux_x,&qaux_y,&qaux_z,&qaux_w,(conformations_x_d + id_atom + i), (conformations_y_d + id_atom + i), (conformations_z_d + id_atom + i), miatomo);
                                ligx = miatomo[0] + mx;
                                ligy = miatomo[1] + my;
                                ligz = miatomo[2] + mz;

                                ind1 = ligtype_d[i];
                                solv_asp_1 = f_params_c[ind1].asp;
                                solv_vol_1 = f_params_c[ind1].vol;
                                //epslig = vdw_params_c [ind1].epsilon;
                                //siglig = vdw_params_c [ind1].sigma;
                                epslig = f_params_c [ind1].epsilon;
                                siglig = f_params_c [ind1].sigma;

                                for (unsigned int j = 0; j < tam; j++)
                                {
                                        if ((k + j) < nrec)
                                        {
                                                e_es = 0;
                                                e_vdw = 0;
                                                e_hbond = 0;
                                                e_solv = 0;
                                                distx = ligx - proteina_x[j];
                                                disty = ligy - proteina_y[j];
                                                distz = ligz - proteina_z[j];
                                                distx = distx*distx + disty*disty + distz*distz;
                                                temp_sqrt =  sqrtf(distx);

                                                exponencial = expf(minus_inv_two_sigma_sqd * distx) * weights_c[3];

                                                ind2 = proteina_type[j];
                                                solv_asp_2 = f_params_c[ind2].asp;
                                                solv_vol_2 = f_params_c[ind2].vol;
                                                //eps = sqrtf(vdw_params_c[ind2].epsilon * epslig);
                                                //sig = vdw_params_c[ind2].sigma * siglig;
						type_data dato_eij = calc_ddd_Mehler_Solmajer_GPU(temp_sqrt,APPROX_ZERO);

                                                eps =  sqrtf(f_params_c[ind2].epsilon * epslig);
                                                sig = ((f_params_c[ind2].sigma + siglig))*0.5L;
                                		eps_hb =  (f_params_c[ind1].epsilon_h +  f_params_c[ind2].epsilon_h);//  * a_scoring.p_hbond;
                                		sig_hb = ((f_params_c[ind1].sigma_h + f_params_c[ind2].sigma_h));//*0.5L;
						
						tmpconst = eps / (xA - xB);
                                		tmpconst_h = eps_hb / (xC - xD);

                                		cA =  tmpconst * powf( sig, xA ) * xB;
                                		cB =  tmpconst * powf( sig, xB ) * xA;

                                		cC =  tmpconst_h * powf( sig_hb, xC ) * xD;
                                		cD =  tmpconst_h * powf( sig_hb, xD ) * xC;

                                		//dxA = (double) xA;
                                		//dxB = (double) xB;
                                		//dxC = (double) xC;
                                		//dxD = (double) xD;

                                		rA = powf( temp_sqrt, xA);
                                		rB = powf( temp_sqrt, xB);
                                		rC = powf( temp_sqrt, xC);
                                		rD = powf( temp_sqrt, xD);

                                		term6 = cB / rB;//pow((double)dist, (double)xB);
                                		term12 = cA / rA;//pow((double)dist, (double)xA);

                                		e_es = (1.0 / (dato_eij * temp_sqrt)) * proteina_qr[j] * ELECSCALE * weights_c[2];
                                		//if (dist < 7.0)
                                		e_vdw = term12 - term6;

                                		if (temp_sqrt < 8)
                                		{
                                        		pre_calc = ((solv_asp_1 * solv_vol_2)  + ((solv_asp_2 + qsolpar * fabs(proteina_qr[j])) * solv_vol_1)) * exponencial;// * a_scoring.p_desolv;
                                        		lig_map = qsolpar * solv_vol_2 * exponencial;
                                         		e_solv = pre_calc + lig_map;
                                		}
						if ((temp_sqrt > 0) && (temp_sqrt < 8) && (ind1 == OXI || ind1 == NIT2 || ind1 == AZU)){
                                        		if (ind2 == HBOND){
                                                		//printf("Hola\n");
                                                		sub(&ligx,&ligy,&ligz, &proteina_x[j], &proteina_y[j], &proteina_z[j], v);
														sub(&(rec_x_d[bondsr_d[MAXBOND * (k + j)]]),&(rec_y_d[bondsr_d[MAXBOND * (k + j)]]),&(rec_z_d[bondsr_d[MAXBOND * (k + j)]]),&proteina_x[j], &proteina_y[j], &proteina_z[j], w);
														type_data cosT = cosTheta (v, w);

                                                		if ( cosT < 0.0 ){
                                                        		type_data sinT = sqrtf(1.0-cosT*cosT);
                                                        		e_hbond = sinT * ((cC / rC) - (cD / rD) + cosT);
                                                        		//printf("e_b %f\n",e_hbond);
                                                		}
                                                		// printf("e_b %f\n",e_hbond);
                                        		}
                                		}
                                		if ((temp_sqrt > 0) && (temp_sqrt < 8) &&  ind1 == HBOND){
                                         		//printf("Hola\n");
                                        		if (ind2 == OXI || ind2 == NIT2 || ind2 == AZU){
                                                		//printf("Hola\n");
														sub(conformations_x_d + id_atom + (bonds_d[MAXBOND*i]),conformations_y_d + id_atom + (bonds_d[MAXBOND*i]),conformations_z_d + id_atom + (bonds_d[MAXBOND*i]),&ligx,&ligy,&ligz,v);		
														sub(&proteina_x[j], &proteina_y[j], &proteina_z[j], &ligx,&ligy,&ligz, w);
                                                		type_data cosT = cosTheta (v, w);

                                                		if ( cosT < 0.0 ){
                                                        		type_data sinT = sqrtf(1.0-cosT*cosT);
                                                        		e_hbond += sinT * ((cC / rC) - (cD / rD) + cosT);
                                                        		//printf("e_b %f\n",e_hbond);
                                                		}
                                        		}
                                		}
                                		tmp_e_es += e_es;
                                		tmp_e_vdw += e_vdw;
                                		tmp_e_hbond += e_hbond;
                                		tmp_e_solv += e_solv;

                         		}


                        		temp_es += (tmp_e_es * ql_d[i]);
                        		temp_vdw += tmp_e_vdw;
                        		temp_hbond += tmp_e_hbond;
                        		temp_solv += (tmp_e_solv * fabs(ql_d[i]) );
                        		tmp_e_es=0;
                        		tmp_e_vdw=0;
                        		tmp_e_hbond=0;
                        		tmp_e_solv=0;
                		 }
                        }
                }
                __syncthreads();

        }
        //if ((lane_id == 0) && (id_conformation == 1)) printf("temp_vdw %f quat_x %f quat_y %f move_x %f move_y %f move_z %f\n",temp_vdw,qaux_x,qaux_y,mx,my,mz);
        temp_total = (temp_es) + (temp_hbond*weights_c[1]) + (temp_vdw*weights_c[0]) + (temp_solv);

	//CODIGO
	
	__syncthreads();
	if (gid < max)
	{
		#if  __CUDA_ARCH__ >= 350
			reduction_shfl(&temp_total,WARP_SIZE,warp_id);	
			if (lane_id == 0) energy_d[id_conformation] = temp_total + (tor*weights_c[4]);		
		#else
			partial_energy[tid] = temp_total;		
			
			__syncthreads();
			
			 //Reduccion: suma de las energias de los atomos que pertenecen al mismo ligando
			for(unsigned int s=WARP_SIZE/2; s>0; s>>=1) {
					if (tid < s + warp_id*WARP_SIZE) {
							partial_energy[tid] += partial_energy[tid + s];
					}
					__syncthreads();
			}
	
			__syncthreads();
	
			if (tid % WARP_SIZE == 0){	
				energy_d[id_conformation] = partial_energy[tid] + (tor*weights_c[4]);	
									
			}
		#endif
	}
}
__global__ void Gpu_full_Kernel_Conformations_flex_by_warp_type1 (unsigned int tor, unsigned int nrec, unsigned int natom, unsigned int nlig, type_data *rec_x_d, type_data *rec_y_d, type_data *rec_z_d, type_data *qr_d, char *rectype_d, unsigned int *bondsr_d, type_data *conformations_x_d, type_data *conformations_y_d, type_data *conformations_z_d, type_data *move_x_d, type_data *move_y_d, type_data *move_z_d, type_data *quat_d_x, type_data *quat_d_y, type_data *quat_d_z, type_data *quat_d_w, type_data *ql_d, char *ligtype_d, unsigned int *bonds_d, type_data *energy_d, unsigned int *nconformation, unsigned int stride, unsigned int tam, unsigned long int max)
{
	extern __shared__ type_data data[];
	type_data *proteina_x = (type_data *)data;
	type_data *proteina_y = (type_data *)&proteina_x[tam];
	type_data *proteina_z = (type_data *)&proteina_y[tam];
	type_data *proteina_qr = (type_data *)&proteina_z[tam];
	type_data *partial_energy = (type_data *)&proteina_qr[tam];
	char *proteina_type = (char *)&partial_energy[tam];	
		
	type_data temp_vdw, temp_es, temp_hbond, temp_solv, temp_total, e_es, e_vdw, e_hbond, e_solv, pre_calc, lig_map;	
	type_data ligx,ligy,ligz,distx,disty,distz,mx,my,mz,temp_sqrt;
	type_data eps, sig, epslig, siglig, term6,term12;
	type_data miatomo[3],v[3],w[3];
	type_data solv_asp_1, solv_asp_2, solv_vol_1, solv_vol_2,solv_qasp_1,solv_qasp_2,temp_solv_total,tmp_e_solv;
    double minus_inv_two_sigma_sqd = -0.5L / (sigma_autodock * sigma_autodock);
    type_data exponencial;
	
	type_data qaux_x, qaux_y, qaux_z, qaux_w;
	unsigned int ind1,ind2,id_atom;		
	unsigned int tid = threadIdx.x + threadIdx.y * blockDim.x;
	unsigned int blockID = blockIdx.x + blockIdx.y * gridDim.x;
	unsigned long int gid = blockID * (blockDim.x * blockDim.y) + (threadIdx.y*blockDim.x) + threadIdx.x;
	unsigned long int id_conformation;
	
	int lane_id = tid & (WARP_SIZE-1);
	int warp_id = tid>>5;
	if (gid < max)
	{
		id_conformation = nconformation[gid / WARP_SIZE] - stride;
		id_atom = id_conformation * nlig;
	//rotation
		qaux_x = quat_d_x[id_conformation];			
		qaux_y = quat_d_y[id_conformation];			
		qaux_z = quat_d_z[id_conformation];			
		qaux_w = quat_d_w[id_conformation];			
	//traslation
		mx = move_x_d[id_conformation];
		my = move_y_d[id_conformation];
		mz = move_z_d[id_conformation];
				
	}	 	
	temp_vdw = 0;
	temp_es = 0;
	temp_hbond = 0;
	temp_solv = 0;
	temp_solv_total = 0;
	
	for (unsigned int k = 0; k < nrec;k+=tam)
	{
		proteina_x[tid] = rec_x_d[k + tid];
		proteina_y[tid] = rec_y_d[k + tid];
		proteina_z[tid] = rec_z_d[k + tid];
		proteina_qr[tid] = qr_d[k + tid];
		proteina_type[tid] = rectype_d[k + tid];		
		__syncthreads();
		
		if (gid < max)
		{		
			for (unsigned int i=lane_id;i<natom;i+=WARP_SIZE)
			{
				rotate3DPoint (&qaux_x,&qaux_y,&qaux_z,&qaux_w,(conformations_x_d + id_atom + i), (conformations_y_d + id_atom + i), (conformations_z_d + id_atom + i), miatomo);
				ligx = miatomo[0] + mx;
				ligy = miatomo[1] + my;
				ligz = miatomo[2] + mz;
			
				ind1 = ligtype_d[i];
				//epslig = vdw_params_c [ind1].epsilon;
	    			//siglig = vdw_params_c [ind1].sigma;
				epslig = f_params_c[ind1].epsilon;
				siglig = f_params_c[ind1].sigma;
				solv_asp_1 = f_params_c[ind1].asp;
                solv_vol_1 = f_params_c[ind1].vol;
									
				for (unsigned int j = 0; j < tam; j++)
				{	
					if ((k+j) < nrec)
					{
						e_es = 0;
						e_vdw = 0;
						e_hbond = 0;
						distx = proteina_x[j] - ligx;
						disty = proteina_y[j] - ligy;
						distz = proteina_z[j] - ligz;
						distx = distx*distx + disty*disty + distz*distz;
						temp_sqrt = sqrtf(distx);
						
						exponencial = expf(minus_inv_two_sigma_sqd * distx) * weights_c[3];
						
						ind2 = proteina_type[j];						
						solv_asp_2 = f_params_c[ind2].asp;
                        solv_vol_2 = f_params_c[ind2].vol;
						//eps = __fsqrt_rn(vdw_params_c[ind2].epsilon * epslig);
						//sig = vdw_params_c[ind2].sigma * siglig;
						eps = sqrtf(f_params_c[ind2].epsilon * epslig);
						sig = f_params_c[ind2].sigma * siglig;
						
						
						term6 = sig / distx;
						term6 = term6 * term6 * term6; //^3				
				
						e_vdw = 4.0 * eps * term6 * (term6 - 1.0);
						e_es = proteina_qr[j] * (1/temp_sqrt) * ql_d[i];
						pre_calc = ((solv_asp_1 * solv_vol_2)  + ((solv_asp_2 + qsolpar * fabs(qr_d[j])) * solv_vol_1)) * exponencial;
						lig_map = qsolpar * solv_vol_2 * exponencial;
						e_solv = pre_calc + lig_map;
						
						if (temp_sqrt > 0.18 && temp_sqrt < 0.39 && (ind1 == OXI || ind1 == NIT2 || ind1 == AZU)){
							if (ind2 == HBOND){
								sub(&ligx,&ligy,&ligz, &proteina_x[j], &proteina_y[j], &proteina_z[j], v);
								sub(&(rec_x_d[bondsr_d[MAXBOND * (k + j)]]),&(rec_y_d[bondsr_d[MAXBOND * (k + j)]]),&(rec_z_d[bondsr_d[MAXBOND * (k + j)]]),&proteina_x[j], &proteina_y[j], &proteina_z[j], w);
								type_data cosT = cosTheta (v, w);

								if ( cosT < 0.0 ){
									type_data sinT = sqrtf(1.0-cosT*cosT);
									e_vdw *= sinT;
									disty = distx *distx *distx *distx *distx ;
									//e_hbond = -cosT * ( hbond_params_c[ind1].c12 / (disty*distx)  -  hbond_params_c[ind1].c10 / disty) ;
									e_hbond = -cosT * ( f_params_c[ind1].epsilon_h / (disty*distx)  -  f_params_c[ind1].sigma_h / disty) ;
								}
							}
						}
						if (temp_sqrt > 0.18 && temp_sqrt < 0.39 &&  ind1 == HBOND){
							if (ind2 == OXI || ind2 == NIT2 || ind2 == AZU){
								sub(conformations_x_d + id_atom + (bonds_d[MAXBOND*i]),conformations_y_d + id_atom + (bonds_d[MAXBOND*i]),conformations_z_d + id_atom + (bonds_d[MAXBOND*i]),&ligx,&ligy,&ligz,v);		
								sub(&proteina_x[j], &proteina_y[j], &proteina_z[j], &ligx,&ligy,&ligz, w);
								type_data cosT = cosTheta (v, w);
								if ( cosT < 0.0 ){
									type_data sinT = sqrtf(1.0-cosT*cosT);
									e_vdw *= sinT;
									disty = distx *distx *distx *distx *distx;
									//e_hbond += -cosT * (hbond_params_c[ind2].c12 / (disty*distx) - hbond_params_c[ind2].c10 / disty );
									e_hbond += -cosT * (f_params_c[ind2].epsilon_h / (disty*distx) - f_params_c[ind2].sigma_h / disty );
								}
							}
						}
                        temp_es += e_es;
                        temp_vdw += e_vdw;
                        temp_hbond += e_hbond;
						temp_solv += e_solv;
					}					
				}
				tmp_e_solv = temp_solv * fabs(ql_d[i]);
				temp_solv_total += tmp_e_solv;
				tmp_e_solv = 0;
				temp_solv = 0;
            }
        }
        __syncthreads();

    }
    //if ((lane_id == 0) && (id_conformation == 1)) printf("temp_vdw %f quat_x %f quat_y %f move_x %f move_y %f move_z %f\n",temp_vdw,qaux_x,qaux_y,mx,my,mz);
    temp_total = (temp_es*ES_CONSTANT*weights_c[2]) + (temp_hbond*weights_c[1]) + (temp_vdw*weights_c[0]) + (temp_solv);
     
	__syncthreads();
	if (gid < max)
	{
		#if  __CUDA_ARCH__ >= 350
			reduction_shfl(&temp_total,WARP_SIZE,warp_id);	
			if (lane_id == 0) energy_d[id_conformation] = temp_total + (tor*weights_c[4]);		
		#else
			partial_energy[tid] = temp_total;		
			
			__syncthreads();
			
			 //Reduccion: suma de las energias de los atomos que pertenecen al mismo ligando
			for(unsigned int s=WARP_SIZE/2; s>0; s>>=1) {
					if (tid < s + warp_id*WARP_SIZE) {
							partial_energy[tid] += partial_energy[tid + s];
					}
					__syncthreads();
			}
	
			__syncthreads();
	
			if (tid % WARP_SIZE == 0){	
				energy_d[id_conformation] = partial_energy[tid] + (tor*weights_c[4]);	
									
			}
		#endif
	}
}						
	
__global__ void Gpu_full_Kernel_Conformations_flex_by_warp (unsigned int nrec, unsigned int natom, unsigned int nlig, type_data *rec_x_d, type_data *rec_y_d, type_data *rec_z_d, type_data *qr_d, char *rectype_d, unsigned int *bondsr_d, type_data *conformations_x_d, type_data *conformations_y_d, type_data *conformations_z_d, type_data *move_x_d, type_data *move_y_d, type_data *move_z_d, type_data *quat_d_x, type_data *quat_d_y, type_data *quat_d_z, type_data *quat_d_w, type_data *ql_d, char *ligtype_d, unsigned int *bonds_d, type_data *energy_d, unsigned int *nconformation, unsigned int stride, unsigned int tam, unsigned long int max)
{
	extern __shared__ type_data data[];
	type_data *proteina_x = (type_data *)data;
	type_data *proteina_y = (type_data *)&proteina_x[tam];
	type_data *proteina_z = (type_data *)&proteina_y[tam];
	type_data *proteina_qr = (type_data *)&proteina_z[tam];
	type_data *partial_energy = (type_data *)&proteina_qr[tam];
	char *proteina_type = (char *)&partial_energy[tam];	
		
	type_data temp_vdw, temp_es, temp_hbond, temp_total, e_es, e_vdw, e_hbond;	
	type_data ligx,ligy,ligz,distx,disty,distz,mx,my,mz,temp_sqrt;
	type_data eps, sig, epslig, siglig, term6,term12;
	type_data miatomo[3],v[3],w[3];
	type_data qaux_x, qaux_y, qaux_z, qaux_w;
	unsigned int ind1,ind2,id_atom;		
	unsigned int tid = threadIdx.x + threadIdx.y * blockDim.x;
	unsigned int blockID = blockIdx.x + blockIdx.y * gridDim.x;
	unsigned long int gid = blockID * (blockDim.x * blockDim.y) + (threadIdx.y*blockDim.x) + threadIdx.x;
	unsigned long int id_conformation;
	
	int lane_id = tid & (WARP_SIZE-1);
	int warp_id = tid>>5;
	if (gid < max)
	{
		id_conformation = nconformation[gid / WARP_SIZE] - stride;
		id_atom = id_conformation * nlig;
	//rotation
		qaux_x = quat_d_x[id_conformation];			
		qaux_y = quat_d_y[id_conformation];			
		qaux_z = quat_d_z[id_conformation];			
		qaux_w = quat_d_w[id_conformation];			
	//traslation
		mx = move_x_d[id_conformation];
		my = move_y_d[id_conformation];
		mz = move_z_d[id_conformation];
				
	}	 	
	temp_vdw = 0;
	temp_es = 0;
	temp_hbond = 0;
						
	for (unsigned int k = 0; k < nrec;k+=tam)
	{
		proteina_x[tid] = rec_x_d[k + tid];
		proteina_y[tid] = rec_y_d[k + tid];
		proteina_z[tid] = rec_z_d[k + tid];
		proteina_qr[tid] = qr_d[k + tid];
		proteina_type[tid] = rectype_d[k + tid];		
		__syncthreads();
		
		if (gid < max)
		{		
			for (unsigned int i=lane_id;i<natom;i+=WARP_SIZE)
			{
				rotate3DPoint (&qaux_x,&qaux_y,&qaux_z,&qaux_w,(conformations_x_d + id_atom + i), (conformations_y_d + id_atom + i), (conformations_z_d + id_atom + i), miatomo);
				ligx = miatomo[0] + mx;
				ligy = miatomo[1] + my;
				ligz = miatomo[2] + mz;
			
				ind1 = ligtype_d[i];
				//epslig = vdw_params_c [ind1].epsilon;
	    			//siglig = vdw_params_c [ind1].sigma;
				epslig = f_params_c[ind1].epsilon;
				siglig = f_params_c[ind1].sigma;
									
				for (unsigned int j = 0; j < tam; j++)
				{	
					if ((k+j) < nrec)
					{
						e_es = 0;
						e_vdw = 0;
						e_hbond = 0;
						distx = proteina_x[j] - ligx;
						disty = proteina_y[j] - ligy;
						distz = proteina_z[j] - ligz;
						distx = distx*distx + disty*disty + distz*distz;
						temp_sqrt = sqrtf(distx);
					
						ind2 = proteina_type[j];						
						//eps = __fsqrt_rn(vdw_params_c[ind2].epsilon * epslig);
						//sig = vdw_params_c[ind2].sigma * siglig;
						eps = sqrtf(f_params_c[ind2].epsilon * epslig);
						sig = f_params_c[ind2].sigma * siglig;
						
						
						term6 = sig / distx;
						term6 = term6 * term6 * term6; //^3				
				
						e_vdw = 4.0 * eps * term6 * (term6 - 1.0);
						e_es = proteina_qr[j] * (1/temp_sqrt) * ql_d[i];					
						
						if (temp_sqrt > 0.18 && temp_sqrt < 0.39 && (ind1 == OXI || ind1 == NIT2 || ind1 == AZU)){
							if (ind2 == HBOND){
								sub(&ligx,&ligy,&ligz, &proteina_x[j], &proteina_y[j], &proteina_z[j], v);
								sub(&(rec_x_d[bondsr_d[MAXBOND * (k + j)]]),&(rec_y_d[bondsr_d[MAXBOND * (k + j)]]),&(rec_z_d[bondsr_d[MAXBOND * (k + j)]]),&proteina_x[j], &proteina_y[j], &proteina_z[j], w);
								type_data cosT = cosTheta (v, w);

								if ( cosT < 0.0 ){
									type_data sinT = sqrtf(1.0-cosT*cosT);
									e_vdw *= sinT;
									disty = distx *distx *distx *distx *distx ;
									//e_hbond = -cosT * ( hbond_params_c[ind1].c12 / (disty*distx)  -  hbond_params_c[ind1].c10 / disty) ;
									e_hbond = -cosT * ( f_params_c[ind1].epsilon_h / (disty*distx)  -  f_params_c[ind1].sigma_h / disty) ;
								}
							}
						}
						if (temp_sqrt > 0.18 && temp_sqrt < 0.39 &&  ind1 == HBOND){
							if (ind2 == OXI || ind2 == NIT2 || ind2 == AZU){
								sub(conformations_x_d + id_atom + (bonds_d[MAXBOND*i]),conformations_y_d + id_atom + (bonds_d[MAXBOND*i]),conformations_z_d + id_atom + (bonds_d[MAXBOND*i]),&ligx,&ligy,&ligz,v);		
								sub(&proteina_x[j], &proteina_y[j], &proteina_z[j], &ligx,&ligy,&ligz, w);
								type_data cosT = cosTheta (v, w);
								if ( cosT < 0.0 ){
									type_data sinT = sqrtf(1.0-cosT*cosT);
									e_vdw *= sinT;
									disty = distx *distx *distx *distx *distx;
									//e_hbond += -cosT * (hbond_params_c[ind2].c12 / (disty*distx) - hbond_params_c[ind2].c10 / disty );
									e_hbond += -cosT * (f_params_c[ind2].epsilon_h / (disty*distx) - f_params_c[ind2].sigma_h / disty );
								}
							}
						}
						temp_es += e_es;
						temp_vdw += e_vdw;
						temp_hbond += e_hbond; 
					}
				}				
			}
		}
		__syncthreads();	
	}
	temp_total = (temp_vdw * weights_c[0]) + (temp_es*ES_CONSTANT*weights_c[2]) + (temp_hbond * weights_c[1]);
	if (gid < max)
	{
		#if  __CUDA_ARCH__ >= 350
			reduction_shfl(&temp_total,WARP_SIZE,warp_id);	
			if (lane_id == 0) energy_d[id_conformation] = temp_total;		
		#else
			partial_energy[tid] = temp_total;		
			
			__syncthreads();
			
			 //Reduccion: suma de las energias de los atomos que pertenecen al mismo ligando
			for(unsigned int s=WARP_SIZE/2; s>0; s>>=1) {
					if (tid < s + warp_id*WARP_SIZE) {
							partial_energy[tid] += partial_energy[tid + s];
					}
					__syncthreads();
			}
	
			__syncthreads();
	
			if (tid % WARP_SIZE == 0){	
				energy_d[id_conformation] = partial_energy[tid];	
									
			}
		#endif
	}
}

__device__ type_data distance (type_data *lig_x, type_data *lig_y, type_data *lig_z, type_data atom[3], unsigned int j)
{
	type_data difx,dify,difz,mod2x, mod2y, mod2z, dist;

	difx = lig_x[j] - atom[0];
	dify = lig_y[j] - atom[1];
	difz = lig_z[j] - atom[2];

	mod2x = difx * difx;
	mod2y = dify * dify;
	mod2z = difz * difz;
	
	dist = mod2x + mod2y + mod2z;
	return (sqrtf(dist));

}

__global__ void energy_internal_conformations_type2 (type_data *ql_d, char *ligtype_d, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, unsigned int atoms, unsigned int nlig, type_data *energy,  unsigned int *nconformation, unsigned int stride, bool *individual_bonds_VDW_d, bool *individual_bonds_ES_d, unsigned int tam, unsigned long int max) {
	
	type_data temp_es, temp_vdw, temp_sqrt, eps, sig, term6, term12, temp;
	type_data partial_energy[128];	
	unsigned int tid = threadIdx.x + threadIdx.y * blockDim.x;
	unsigned int blockID = blockIdx.x + blockIdx.y * gridDim.x;
	unsigned int long gid = blockID * (blockDim.x * blockDim.y) + (threadIdx.y*blockDim.x) + threadIdx.x;	

	unsigned int long position_conformation;
	char ind1,ind2;
	type_data carga_i, params_i_eps, params_i_sig, atom[3], carga_j;
    	type_data difx,dify,difz,mod2x, mod2y, mod2z, dist, temp_es_total,tmp_e_es;
	type_data tmpconst,cA,cB,rA,rB;

	int lane_id = tid & (WARP_SIZE-1);
	int warp_id = tid>>5;	
	int ilig =  warp_id * WARP_SIZE;
	unsigned long int i_atom, id_conformation;

	temp = 0;
        temp_es = 0;
	temp_es_total = 0;
        temp_vdw = 0;	
	//CODIGO
	 if (gid < max)
        {
                id_conformation = nconformation[gid / WARP_SIZE]-stride;
                position_conformation = id_conformation * nlig;


                for (unsigned int i = lane_id; i < (atoms-1); i+=WARP_SIZE)
                {
                        atom[0] = *(conformations_x + position_conformation + i);
                        atom[1] = *(conformations_y + position_conformation + i);
                        atom[2] = *(conformations_z + position_conformation + i);
                        //charge[ilig + lane_id] = ql_d[i + lane_id];
                        //type[ilig + lane_id] = ligtype_d[i + lane_id];

                        ind1 = *(ligtype_d + i);
                        //carga_i = ql_d[i + lane_id];
                        carga_i = *(ql_d + i);
                        //params_i_eps = vdw_params_c[ind1].epsilon;
                        //params_i_sig = vdw_params_c[ind1].sigma;
                        params_i_eps = f_params_c[ind1].epsilon;
                        params_i_sig = f_params_c[ind1].sigma;

                        for (unsigned int j = i + 1;j < atoms; j++)
                        {
                                carga_j = *(ql_d + j);
                                difx = *(conformations_x + position_conformation + j) - atom[0];
                                dify = *(conformations_y + position_conformation + j) - atom[1];
                                difz = *(conformations_z + position_conformation + j) - atom[2];
                                mod2x = difx * difx;
                                mod2y = dify * dify;
                                mod2z = difz * difz;
                                dist = mod2x + mod2y + mod2z;
                                temp_sqrt = sqrtf(dist);

                                ind2 = *(ligtype_d + j);

				type_data dato_eij = calc_ddd_Mehler_Solmajer_GPU(temp_sqrt,APPROX_ZERO);

                                eps =  sqrtf(f_params_c[ind2].epsilon * params_i_eps);
                                sig = ((f_params_c[ind2].sigma + params_i_sig))*0.5L;
                                
				tmpconst = eps / (xA - xB);
				
                                cA =  tmpconst * powf( sig, xA ) * xB;
                                cB =  tmpconst * powf( sig, xB ) * xA;

                                //dxA = (double) xA;
                                //dxB = (double) xB;
                                
				rA = powf( temp_sqrt, xA);
                                rB = powf( temp_sqrt, xB);

                                term6 = cB / rB;//pow((double)dist, (double)xB);
                                term12 = cA / rA;//pow((double)dist, (double)xA);

				temp_es += (1.0 / (dato_eij * temp_sqrt)) * carga_j * ELECSCALE * weights_c[2] * !individual_bonds_ES_d[(j*atoms) + i];
                                temp_vdw += (term12 - term6)  * !individual_bonds_VDW_d[(j*atoms) + i]; 
                        }
			tmp_e_es = temp_es * carga_i;
			temp_es_total = tmp_e_es + carga_i;
			temp_es = 0;
			tmp_e_es = 0;

                }
        }
        __syncthreads();
        temp = (temp_es) + (temp_vdw*weights_c[0]);
	__syncthreads();
	if (gid < max)
	{
		#if  __CUDA_ARCH__ >= 350
			reduction_shfl(&temp,WARP_SIZE,warp_id);	
			if (lane_id == 0) energy[id_conformation] = temp;	
			//if (id_conformation==0) printf("0 %f\n",temp);	
		#else
			partial_energy[tid] = temp;		
			
			__syncthreads();
			
			 //Reduccion: suma de las energias de los atomos que pertenecen al mismo ligando
			for(unsigned int s=WARP_SIZE/2; s>0; s>>=1) {
					if (tid < s + warp_id*WARP_SIZE) {
							partial_energy[tid] += partial_energy[tid + s];
					}
					__syncthreads();
				}
	
			__syncthreads();
	
			if (tid % WARP_SIZE == 0){			
				energy[id_conformation] = partial_energy[tid];	
									
			}
		#endif
	}


}

__global__ void energy_internal_conformations (type_data *ql_d, char *ligtype_d, type_data *conformations_x, type_data *conformations_y, type_data *conformations_z, unsigned int atoms, unsigned int nlig, type_data *energy,  unsigned int *nconformation, unsigned int stride, bool *individual_bonds_VDW_d, bool *individual_bonds_ES_d, unsigned int tam, unsigned long int max) {
	
	type_data temp_es, temp_vdw, temp_dist, eps, sig, term6, term12, temp;
	type_data partial_energy[128];	
	//extern __shared__ type_data data[];
	//type_data *coord_x = (type_data *)data;
	//type_data *coord_y = (type_data *)&coord_x[tam];
	//type_data *coord_z = (type_data *)&coord_y[tam];
	//type_data *charge = (type_data *)&coord_z[tam];
	//type_data *partial_energy = (type_data *)&charge[tam];
	//char *type = (char *)&partial_energy[tam];
	
	unsigned int tid = threadIdx.x + threadIdx.y * blockDim.x;
	unsigned int blockID = blockIdx.x + blockIdx.y * gridDim.x;
	unsigned int long gid = blockID * (blockDim.x * blockDim.y) + (threadIdx.y*blockDim.x) + threadIdx.x;	

	unsigned int long position_conformation;
	char ind1,ind2;
	type_data carga_i, params_i_eps, params_i_sig, atom[3], carga_j;
        type_data difx,dify,difz,mod2x, mod2y, mod2z, dist;

	int lane_id = tid & (WARP_SIZE-1);
	int warp_id = tid>>5;	
	int ilig =  warp_id * WARP_SIZE;
	unsigned long int i_atom, id_conformation;
	temp = 0;
	temp_es = 0;
	temp_vdw = 0;

	if (gid < max)	
	{
		id_conformation = nconformation[gid / WARP_SIZE]-stride;
		position_conformation = id_conformation * nlig;				
		
	
		for (unsigned int i = lane_id; i < (atoms-1); i+=WARP_SIZE)
		{
			atom[0] = *(conformations_x + position_conformation + i);	
			atom[1] = *(conformations_y + position_conformation + i);
			atom[2] = *(conformations_z + position_conformation + i);
			//charge[ilig + lane_id] = ql_d[i + lane_id];
			//type[ilig + lane_id] = ligtype_d[i + lane_id];
	
			ind1 = *(ligtype_d + i);
			//carga_i = ql_d[i + lane_id];
			carga_i = *(ql_d + i);	
			//params_i_eps = vdw_params_c[ind1].epsilon;
			//params_i_sig = vdw_params_c[ind1].sigma;
			params_i_eps = f_params_c[ind1].epsilon;
			params_i_sig = f_params_c[ind1].sigma;
							
			for (unsigned int j = i + 1;j < atoms; j++) 
			{						
				carga_j = *(ql_d + j);
				difx = *(conformations_x + position_conformation + j) - atom[0];
				dify = *(conformations_y + position_conformation + j) - atom[1];
				difz = *(conformations_z + position_conformation + j) - atom[2];
				mod2x = difx * difx;
				mod2y = dify * dify;
				mod2z = difz * difz;
				dist = mod2x + mod2y + mod2z;
				temp_dist = sqrtf(dist);
				
				ind2 = *(ligtype_d + j);
				//eps = __fsqrt_rn(params_i_eps  * vdw_params_c[ind2].epsilon);
				//sig = __fsqrt_rn(params_i_sig  * vdw_params_c[ind2].sigma);
				eps = sqrtf(params_i_eps  * f_params_c[ind2].epsilon);
                                sig = sqrtf(params_i_sig  * f_params_c[ind2].sigma);
				term6 = sig / temp_dist;
				term6 = term6 * term6 * term6; // ^3
				term6 = term6 * term6; //^6
				term12 = term6 * term6; //^12
				temp_vdw += 4.0*eps*(term12 -term6) * !individual_bonds_VDW_d[(j*atoms) + i];	 
				temp_es += carga_i * (1/temp_dist) * carga_j * !individual_bonds_ES_d[(j*atoms) + i];			
				
			}
		
		}
	}			
	__syncthreads();	
	temp_es *= ES_CONSTANT;			
	temp = (temp_es*weights_c[2]) + (temp_vdw*weights_c[0]);	
	__syncthreads();
	if (gid < max)
	{
		#if  __CUDA_ARCH__ >= 350
			reduction_shfl(&temp,WARP_SIZE,warp_id);	
			if (lane_id == 0) energy[id_conformation] = temp;		
		#else
			partial_energy[tid] = temp;		
			
			__syncthreads();
			
			 //Reduccion: suma de las energias de los atomos que pertenecen al mismo ligando
			for(unsigned int s=WARP_SIZE/2; s>0; s>>=1) {
					if (tid < s + warp_id*WARP_SIZE) {
							partial_energy[tid] += partial_energy[tid + s];
					}
					__syncthreads();
				}
	
			__syncthreads();
	
			if (tid % WARP_SIZE == 0){			
				energy[id_conformation] = partial_energy[tid];	
									
			}
		#endif
	}
		
}

__global__ void energy_internal_conformations_vs_type2 (type_data *conformations_x,type_data *conformations_y,type_data *conformations_z,unsigned int *atoms,unsigned int nlig,struct flexibility_params_t *flexibility_params,type_data *internal_energy,unsigned int *n_conformations,type_data *ql, char *ligtype, unsigned int stride_conformations,unsigned int tam, unsigned long int max, unsigned int stride)
{
	type_data temp_es, temp_vdw, temp_sqrt, eps, sig, term6, term12, temp;
	type_data partial_energy[128];	
	unsigned int tid = threadIdx.x + threadIdx.y * blockDim.x;
	unsigned int blockID = blockIdx.x + blockIdx.y * gridDim.x;
	unsigned int long gid = blockID * (blockDim.x * blockDim.y) + (threadIdx.y*blockDim.x) + threadIdx.x;	

	unsigned int long position_conformation;
	char ind1,ind2;
	type_data carga_i, params_i_eps, params_i_sig, atom[3], carga_j;
    	type_data difx,dify,difz,mod2x, mod2y, mod2z, dist, temp_es_total,tmp_e_es;
	type_data tmpconst,cA,cB,rA,rB;

	int lane_id = tid & (WARP_SIZE-1);
	int warp_id = tid>>5;	
	int ilig =  warp_id * WARP_SIZE;
	unsigned long int i_atom, id_conformation;
	unsigned int file;
	unsigned int atoms_f;
	temp=0;
	temp_es=0;
	temp_es_total = 0;
	temp_vdw=0;	
	//xA = 12;
	//xB = 6;
	//CODIGO
	 if (gid < max)	
	{
		id_conformation = n_conformations[gid / WARP_SIZE]-stride_conformations;
		position_conformation = id_conformation * nlig;
		file = id_conformation / stride;
		atoms_f = atoms[file];
		//printf("id_c %d file %d atoms %d\n",id_conformation,file,atoms_f);	
		//if (id_conformation == 0)
		//{
	//		printf("Hola tu\n");
		//	for (int o=0;o<(atoms_f*atoms_f);o++) printf("bonds_ES %d\n",flexibility_params[file].individual_bonds_VDW[o]);
		//}
	
		for (unsigned int i = lane_id; i < (atoms_f-1); i+=WARP_SIZE)
		{
			atom[0] = *(conformations_x + position_conformation + i);	
			atom[1] = *(conformations_y + position_conformation + i);
			atom[2] = *(conformations_z + position_conformation + i);
			
			ind1 = *(ligtype + position_conformation + i);

			carga_i = *(ql + position_conformation + i);
			//params_i_eps = vdw_params_c[ind1].epsilon;
			//params_i_sig = vdw_params_c[ind1].sigma;
			params_i_eps = f_params_c[ind1].epsilon;
					params_i_sig = f_params_c[ind1].sigma;
			//i_atom = i + lane_id + 1;
								
			for (unsigned int j = i + 1;j < atoms_f; j++) 
			{	
				carga_j = *(ql + position_conformation + j);
				difx = *(conformations_x + position_conformation + j) - atom[0];
				dify = *(conformations_y + position_conformation + j) - atom[1];
				difz = *(conformations_z + position_conformation + j) - atom[2];
				mod2x = difx * difx;
				mod2y = dify * dify;
				mod2z = difz * difz;
				dist = mod2x + mod2y + mod2z;
				temp_sqrt = sqrtf(dist);
				
				ind2 = *(ligtype + position_conformation + j);
					
				type_data dato_eij = calc_ddd_Mehler_Solmajer_GPU(temp_sqrt,APPROX_ZERO);

                		eps =  sqrtf(f_params_c[ind2].epsilon * params_i_eps);
                		sig = ((f_params_c[ind2].sigma + params_i_sig))*0.5L;
                                
				tmpconst = eps / (xA - xB);
				
                		cA =  tmpconst * powf( sig, xA ) * xB;
                		cB =  tmpconst * powf( sig, xB ) * xA;

                //dxA = (double) xA;
                //dxB = (double) xB;
                                
				rA = powf( temp_sqrt, xA);
                		rB = powf( temp_sqrt, xB);

                		term6 = cB / rB;//pow((double)dist, (double)xB);
                		term12 = cA / rA;//pow((double)dist, (double)xA);

				temp_es += (1.0 / (dato_eij * temp_sqrt)) * carga_j * ELECSCALE * weights_c[2] * !flexibility_params[file].individual_bonds_VDW[(j*atoms_f) + i];
                		temp_vdw += (term12 - term6) * !flexibility_params[file].individual_bonds_VDW[(j*atoms_f) + i]; 
            		}
			tmp_e_es = temp_es * carga_i;
	    		temp_es_total += tmp_e_es;
			temp_es = 0;
			tmp_e_es = 0;

        	}
    	}
    	//__syncthreads();
    	temp = (temp_es) + (temp_vdw*weights_c[0]);
	__syncthreads();
	
	__syncthreads();
	if (gid < max)
	{
		#if  __CUDA_ARCH__ >= 350
			reduction_shfl(&temp,WARP_SIZE,warp_id);	
			if (lane_id == 0) internal_energy[id_conformation] = temp;		
			//if ((id_conformation == 0) && (lane_id==0))printf("0 %f\n",temp);	
		#else
			partial_energy[tid] = temp;		
			
			__syncthreads();
			
			 //Reduccion: suma de las energias de los atomos que pertenecen al mismo ligando
			for(unsigned int s=WARP_SIZE/2; s>0; s>>=1) {
					if (tid < s + warp_id*WARP_SIZE) {
							partial_energy[tid] += partial_energy[tid + s];
					}
				__syncthreads();
			}
			
			__syncthreads();
	
			if (tid % WARP_SIZE == 0){			
				internal_energy[id_conformation] = partial_energy[tid];	
									
			}
		#endif
	}
	
}

__global__ void energy_internal_conformations_vs (type_data *conformations_x,type_data *conformations_y,type_data *conformations_z,unsigned int *atoms,unsigned int nlig,struct flexibility_params_t *flexibility_params,type_data *internal_energy,unsigned int *n_conformations,type_data *ql, char *ligtype, unsigned int stride_conformations,unsigned int tam, unsigned long int max, unsigned int stride)
{
	
	type_data temp_es, temp_vdw, temp_dist, eps, sig, term6, term12, temp;
	
	//extern __shared__ type_data data[];
	//type_data *coord_x = (type_data *)data;
	//type_data *coord_y = (type_data *)&coord_x[tam];
	//type_data *coord_z = (type_data *)&coord_y[tam];
	//type_data *charge = (type_data *)&coord_z[tam];
	//type_data *partial_energy = (type_data *)&charge[tam];
	//char *type = (char *)&partial_energy[tam];
	type_data partial_energy[128];
	
	unsigned int tid = threadIdx.x + threadIdx.y * blockDim.x;
	unsigned int blockID = blockIdx.x + blockIdx.y * gridDim.x;
	unsigned long int gid = blockID * (blockDim.x * blockDim.y) + (threadIdx.y*blockDim.x) + threadIdx.x;	

	unsigned long int position_conformation;
	char ind1,ind2;
	type_data carga_i, params_i_eps, params_i_sig, atom[3], carga_j;
	type_data difx,dify,difz,mod2x, mod2y, mod2z, dist;

	int lane_id = tid & (WARP_SIZE-1);
	int warp_id = tid>>5;	
	int ilig =  warp_id * WARP_SIZE;
	unsigned int i_atom, id_conformation;
	unsigned int file;
	unsigned int atoms_f;
	temp = 0;
	temp_es = 0;
	temp_vdw = 0;

	if (gid < max)	
	{
		id_conformation = n_conformations[gid / WARP_SIZE]-stride_conformations;
		position_conformation = id_conformation * nlig;
		file = id_conformation / stride;
		atoms_f = atoms[file];
		//printf("id_c %d file %d atoms %d\n",id_conformation,file,atoms_f);	
		//if (id_conformation == 0)
		//{
	//		printf("Hola tu\n");
		//	for (int o=0;o<(atoms_f*atoms_f);o++) printf("bonds_ES %d\n",flexibility_params[file].individual_bonds_VDW[o]);
		//}
	
	for (unsigned int i = lane_id; i < (atoms_f-1); i+=WARP_SIZE)
	{
		atom[0] = *(conformations_x + position_conformation + i);	
		atom[1] = *(conformations_y + position_conformation + i);
		atom[2] = *(conformations_z + position_conformation + i);
		
		ind1 = *(ligtype + position_conformation + i);

		carga_i = *(ql + position_conformation + i);
		//params_i_eps = vdw_params_c[ind1].epsilon;
		//params_i_sig = vdw_params_c[ind1].sigma;
		params_i_eps = f_params_c[ind1].epsilon;
                params_i_sig = f_params_c[ind1].sigma;
		//i_atom = i + lane_id + 1;
							
		for (unsigned int j = i + 1;j < atoms_f; j++) 
		{	
			carga_j = *(ql + position_conformation + j);
			difx = *(conformations_x + position_conformation + j) - atom[0];
			dify = *(conformations_y + position_conformation + j) - atom[1];
			difz = *(conformations_z + position_conformation + j) - atom[2];
			mod2x = difx * difx;
			mod2y = dify * dify;
			mod2z = difz * difz;
			dist = mod2x + mod2y + mod2z;
			temp_dist = sqrtf(dist);
			
			ind2 = *(ligtype + position_conformation + j);
			//eps = __fsqrt_rn(params_i_eps  * vdw_params_c[ind2].epsilon);
                        //sig = __fsqrt_rn(params_i_sig  * vdw_params_c[ind2].sigma);
			eps = sqrtf(params_i_eps  * f_params_c[ind2].epsilon);
			sig = sqrtf(params_i_sig  * f_params_c[ind2].sigma);
			term6 = sig / temp_dist;
			term6 = term6 * term6 * term6; // ^3
			term6 = term6 * term6; //^6
			term12 = term6 * term6; //^12
			temp_vdw += 4.0*eps*(term12 -term6) * !flexibility_params[file].individual_bonds_VDW[(j*atoms_f) + i];	 
			temp_es += carga_i * (1/temp_dist) * carga_j * !flexibility_params[file].individual_bonds_ES[(j*atoms_f) + i];			
								
			}
		
		}
	}
	__syncthreads();		
	temp_es *= ES_CONSTANT;			
	temp = (temp_es*weights_c[2]) + (temp_vdw*weights_c[0]);
	//if (id_conformation == 0) printf("Hola\n");
	__syncthreads();
	if (gid < max)
	{
		#if  __CUDA_ARCH__ >= 350
			reduction_shfl(&temp,WARP_SIZE,warp_id);	
			if (lane_id == 0) internal_energy[id_conformation] = temp;		
		#else
			partial_energy[tid] = temp;		
			
			__syncthreads();
			
			 //Reduccion: suma de las energias de los atomos que pertenecen al mismo ligando
			for(unsigned int s=WARP_SIZE/2; s>0; s>>=1) {
					if (tid < s + warp_id*WARP_SIZE) {
							partial_energy[tid] += partial_energy[tid + s];
					}
				__syncthreads();
			}
			
			__syncthreads();
	
			if (tid % WARP_SIZE == 0){			
				internal_energy[id_conformation] = partial_energy[tid];	
									
			}
		#endif
	}

}

__global__ void energy_total (type_data *energy, type_data *internal_energy, unsigned long int max)
{
	//int tid = threadIdx.x + threadIdx.y * blockDim.x;
	unsigned int blockID = blockIdx.x + blockIdx.y * gridDim.x;
	unsigned int gid = blockID * (blockDim.x * blockDim.y) + (threadIdx.y*blockDim.x) + threadIdx.x;
	unsigned int long id_conformation;
	if (gid < max)
	{
		id_conformation = gid;// / WARP_SIZE;		
		energy[id_conformation] = internal_energy[id_conformation] + energy[id_conformation];
	}
}




