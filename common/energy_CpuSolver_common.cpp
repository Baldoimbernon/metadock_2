#include "definitions.h"
#include <sys/time.h>
#include "energy_common.h"
#include <stdio.h>
#include <math.h>

using namespace std;

extern double aleatorio (type_data minmax) 
{
  unsigned seed;
  unsigned int ti1,si;
  type_data alea;
  struct timeval *tv;
  struct timezone *tz;
  tv=(struct timeval *) malloc(sizeof(struct timeval));
  tz=(struct timezone *) malloc(sizeof(struct timezone));
  gettimeofday(tv,tz);
  si=(tv->tv_sec);
  ti1=(tv->tv_usec);
  ti1=si*1000000+ti1;

  srand (ti1);
  seed=rand()%10000;

  alea = 1.*rand_r(&seed)/RAND_MAX;

  free(tv);
  free(tz);

  return (2* minmax * alea - minmax);
}


extern void init_quat (type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, unsigned int nconformations)
{
	for (unsigned int i=0;i < nconformations;i++)
	{
		quat_x[i] = 0;quat_y[i] = 0;quat_z[i] = 0;quat_w[i] = 1;
	}
}

extern type_data frand()
{
	return(drand48());
}
//Devuelve un n�mero aleatorio entre 0 y 1
extern int getIntNumber (unsigned int max)
{
	return (max * frand());
}


extern type_data frand_r(unsigned int auto_seed, unsigned int seed, unsigned int seed_r)
{
	unsigned int i;
	type_data alea;
	/*int ti1,si;
	struct timeval *tv;
	struct timezone *tz;
	tv=(struct timeval *) malloc(sizeof(struct timeval));
	tz=(struct timezone *) malloc(sizeof(struct timezone));
	gettimeofday(tv,tz);
	si=(tv->tv_sec);
	ti1=(tv->tv_usec);
	ti1=si*1000000+ti1;

	srand (ti1);
	seed=rand()%10000;
	i = seed;	
	alea = 1.*rand_r(&i)/RAND_MAX;*/

	if (auto_seed)
	{

		i = rand()%10000;	
		//return(drand48());
		//i = (unsigned int)clock();
		alea = 1.*rand_r(&i)/RAND_MAX;
		return (alea);
	}
	else
	{
		//i = (unsigned int) seed;
		i = (unsigned int) seed_r;
	
		return(type_data(rand_r(&i))/32767.0)/100000;
	}
	//}
        
}

//Devuelve un n�mero aleatorio entre 0 y max
extern double getRandomNumber (type_data max){

	//return max * frand();
	return frand();
}

extern double getRandomNumber_r (type_data max,unsigned int auto_seed, unsigned int seed, unsigned int seed_r){

	//return max * frand_r(auto_seed,seed);
	return frand_r(auto_seed,seed,seed_r);
}

//Devuelve un n�mero aleatorio entre -minmax y minmax
extern double getRealRandomNumber (type_data minmax){

	//return 2* minmax * getRandomNumber(minmax) - minmax;
	return (2* minmax * getRandomNumber(minmax)) - minmax;
}

extern double getRealRandomNumber_r (type_data minmax,unsigned int auto_seed, unsigned int seed, unsigned int seed_r) {

	//return 2* minmax * getRandomNumber_r(minmax,auto_seed,seed) - minmax;
	return (2* minmax * getRandomNumber_r(minmax,auto_seed,seed,seed_r)) - minmax;
}

extern void sub_h (type_data *v0 , type_data *v1 ,type_data *v2 , type_data *w0, type_data *w1, type_data *w2, type_data * dst){

     dst[0] = *v0 - *w0;
     dst[1] = *v1 - *w1;
     dst[2] = *v2 - *w2;

}

extern type_data norma_h (type_data *v){

     return sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
}

extern type_data cosTheta_h (type_data * v, type_data * w){

     type_data scalar, cos_angle;
     type_data modv, modw;
     scalar = v[0] * w[0] + v[1] * w[1] + v[2] * w[2];

     modv = norma_h(v);
     modw = norma_h(w);
     cos_angle = scalar / (modv * modw);

     if (cos_angle > 1.0) return 1.0;

     if (cos_angle < -1.0) return -1.0;

     return cos_angle;
}


