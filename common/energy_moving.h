#ifndef __ENERGY_MOVING_H
#define __ENERGY_MOVING_H

#include "energy_cuda.h"
#include "energy_rotation.h"
#include "definitions.h"
#include "energy_struct.h"

#define PI 3.141592653589793

//Devuelve un numero entero aleatorio entre 0 y UINT_MAX
extern __device__ unsigned int getRandomNumber (curandState_t * state);
	
//Devuelve un numero en coma flotante aleatorio en el intervalo (0,1]
extern __device__ type_data getRandomNumber_double (curandState_t * state);
extern __device__ type_data getRandomNumber_double (curandState_t * state, type_data max_angle_flex);

extern __device__ unsigned int getRandomNumber_modified (curandState_t * state, unsigned int n, unsigned int act);

__global__ void setupCurandState (curandState_t * state, unsigned int auto_seed, unsigned int seed, unsigned int max);
__global__ void setup ( type_data *quat_x, type_data *quat_y, type_data *quat_z, type_data *quat_w, unsigned int max);
__global__ void rotation (curandState_t * states, type_data max_angle, type_data * quaternions_x, type_data * quaternions_y, type_data * quaternions_z, type_data * quaternions_w, unsigned int max);
__global__ void rotation_montecarlo (curandState_t * states, type_data max_angle, type_data * quaternions_x, type_data * quaternions_y, type_data * quaternions_z, type_data * quaternions_w, type_data *quat_x_m, type_data *quat_y_m, type_data *quat_z_m, type_data *quat_w_m, unsigned int max);
__global__ void rotation_montecarlo_environment (curandState_t * states, type_data max_angle, type_data *quat_x_m, type_data *quat_y_m, type_data *quat_z_m, type_data *quat_w_m, unsigned int max);
__global__ void move(curandState_t * states, type_data *surface_x, type_data *surface_y, type_data *surface_z, type_data radius_cutoff, type_data * moves_x_d, type_data * moves_y_d, type_data * moves_z_d, unsigned int start_point, unsigned int pc, unsigned int max);
__global__ void move_vs (curandState_t * states, type_data radius_cutoff, type_data * moves_x_d, type_data * moves_y_d, type_data * moves_z_d, type_data ac_x, type_data ac_y, type_data ac_z, unsigned int max);
__global__ void move_mejora(curandState_t * states, type_data radius_cutoff, type_data * moves_x_d, type_data * moves_y_d, type_data * moves_z_d, unsigned int max);
__global__ void move_mejora_montecarlo(curandState_t * states, type_data radius_cutoff, type_data * moves_x_d, type_data * moves_y_d, type_data * moves_z_d, type_data * moves_x_m, type_data * moves_y_m, type_data * moves_z_m, unsigned int max);

__global__ void move_mejora_montecarlo_environment (curandState_t * states, type_data radius_cutoff, type_data * move_x_m, type_data * move_y_m, type_data * move_z_m, unsigned int max);

__global__ void incluir_mejorar(type_data *moves_x_d_mejora, type_data *moves_y_d_mejora, type_data *moves_z_d_mejora, type_data *quat_d_mejora_x, type_data *quat_d_mejora_y, type_data *quat_d_mejora_z, type_data *quat_d_mejora_w, type_data *energy_d_mejora, unsigned int* conformations_d_mejora, type_data *moves_x_d, type_data *moves_y_d, type_data *moves_z_d, type_data *quat_d_x, type_data *quat_d_y, type_data *quat_d_z, type_data *quat_d_w, type_data *energy_d, unsigned int stride_d, unsigned int max);
__global__ void incluir_mejorar_environment (int NEEImp, type_data *moves_x_d_mejora, type_data *moves_y_d_mejora, type_data *moves_z_d_mejora, type_data *quat_d_mejora_x, type_data *quat_d_mejora_y, type_data *quat_d_mejora_z, type_data *quat_d_mejora_w, type_data *energy_d_mejora, unsigned int* conformations_d_mejora, type_data *moves_x_d, type_data *moves_y_d, type_data *moves_z_d, type_data *quat_d_x, type_data *quat_d_y, type_data *quat_d_z, type_data *quat_d_w, type_data *energy_d, unsigned int stride_d, unsigned int max);
__global__ void incluir_mejorar_environment_flex (int NEEImp,int nlig, type_data *conformations_x_d_mejora, type_data *conformations_y_d_mejora, type_data *conformations_z_d_mejora, type_data *moves_x_d_mejora, type_data *moves_y_d_mejora, type_data *moves_z_d_mejora, type_data *quat_d_mejora_x, type_data *quat_d_mejora_y, type_data *quat_d_mejora_z, type_data *quat_d_mejora_w, type_data *energy_d_mejora, unsigned int* conformations_d_mejora, type_data *conformations_x_d, type_data *conformations_y_d, type_data *conformations_z_d, type_data *moves_x_d, type_data *moves_y_d, type_data *moves_z_d, type_data *quat_d_x, type_data *quat_d_y, type_data *quat_d_z, type_data *quat_d_w, type_data *energy_d, unsigned int stride_d, unsigned int max);

__global__ void incluir (type_data *moves_x_d_selec, type_data *moves_y_d_selec, type_data *moves_z_d_selec, type_data *quat_d_selec_x, type_data *quat_d_selec_y, type_data *quat_d_selec_z, type_data *quat_d_selec_w, type_data *energy_d_selec, unsigned int* conformations_d_selec, type_data *moves_x_d, type_data *moves_y_d, type_data *moves_z_d, type_data *quat_d_x, type_data *quat_d_y, type_data *quat_d_z, type_data *quat_d_w, type_data *energy_d, unsigned int* conformations_d);

__global__ void updateMove(curandState_t * states, type_data max_desp,  type_data* moves_x_d, type_data* moves_y_d, type_data* moves_z_d, type_data* move_x_m, type_data* move_y_m, type_data* move_z_m, type_data* energy_sig, type_data* energy_ant, unsigned int max);
__global__ void updateMove_environment(curandState_t * states, type_data max_desp,  type_data* moves_x_d, type_data* moves_y_d, type_data* moves_z_d, type_data* move_x_m, type_data* move_y_m, type_data* move_z_m, type_data* energy_sig, type_data* energy_ant, int NEEImp, unsigned int max);
__global__ void updateRotation (curandState_t * states, type_data max_angle,  type_data *quaternions_x, type_data *quaternions_y, type_data *quaternions_z, type_data *quaternions_w, type_data *quat_x_m, type_data *quat_y_m, type_data *quat_z_m, type_data *quat_w_m, type_data * energy_sig, type_data* energy_ant, unsigned int max);
__global__ void updateRotation_environment (curandState_t * states, type_data max_angle,  type_data *quaternions_x, type_data *quaternions_y, type_data *quaternions_z, type_data *quaternions_w, type_data *quat_x_m, type_data *quat_y_m, type_data *quat_z_m, type_data *quat_w_m, type_data * energy_sig, type_data* energy_ant, int NEEImp, unsigned int max);

__global__ void angulations_conformations (curandState_t * states, struct flexibility_data_t * flexibility_conformations, unsigned int n_links, type_data max_angle_flex, unsigned int max);
__global__ void update_angulations_conformations (curandState_t * states, struct flexibility_data_t * flexibility_conformations, type_data * energy_sig, type_data* energy_ant, unsigned int max);
__global__ void update_angulations_conformations_environment (unsigned int tam, curandState_t * states, struct flexibility_data_t * flexibility_conformations, type_data * energy_sig, type_data* energy_ant, unsigned int max);

__global__ void update_angulations_conformations_tradicional (curandState_t * states, struct flexibility_data_t * flexibility_conformations, type_data * energy_sig, type_data* energy_ant, unsigned int max);
__global__ void update_angulations_conformations_tradicional_environment (unsigned int tam, curandState_t * states, struct flexibility_data_t * flexibility_conformations, type_data * energy_sig, type_data* energy_ant, unsigned int max);

__global__ void angulations_conformations_vs (curandState_t * states, struct flexibility_data_t * flexibility_conformations, struct flexibility_params_t *flexibility_params, type_data max_angle_flex, unsigned int stride_d, unsigned int conf_point, unsigned int max);

#endif
